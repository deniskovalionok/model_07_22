      Subroutine FDC40_l(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDC40_l.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      call lin_ext(REAL(R8_am,4),I2_id,R_(2),R_ed,R_ad)
      R8_ud=R_(2)
C FDC40_l.fgi( 268, 241):�������-�������� ������� (20)
      R8_od=R8_ud
C FDC40_l.fgi( 288, 247):������,UKG2_P
      call lin_ext(REAL(R8_am,4),I2_if,R_(3),R_ef,R_af)
      R0_ek=R_(3)
C FDC40_l.fgi( 249, 206):�������-�������� ������� (20)
      call lin_ext(REAL(R0_ek,4),I2_ak,R_(4),R_uf,R_of)
      R0_ok=R_(4)
C FDC40_l.fgi( 269, 206):�������-�������� ������� (20)
      R_(7) = R0_ok * R8_ik
C FDC40_l.fgi( 314, 211):����������
      call lin_ext(REAL(R0_ek,4),I2_u,R_(1),R_i,R_e)
      R0_o=R_(1)
C FDC40_l.fgi( 270, 188):�������-�������� ������� (20)
      R_(5) = R0_o * R8_ik
C FDC40_l.fgi( 315, 193):����������
      !��������� R_(6) = FDC40_lC?? /0.0/
      R_(6)=R0_uk
C FDC40_l.fgi( 330, 195):���������
      if(L_al) then
         R8_el=R_(5)
      else
         R8_el=R_(6)
      endif
C FDC40_l.fgi( 333, 193):���� RE IN LO CH7
      !��������� R_(8) = FDC40_lC?? /0.0/
      R_(8)=R0_il
C FDC40_l.fgi( 329, 213):���������
      if(L_ol) then
         R8_ul=R_(7)
      else
         R8_ul=R_(8)
      endif
C FDC40_l.fgi( 332, 211):���� RE IN LO CH7
      call lin_ext(REAL(R8_av,4),I2_ip,R_(10),R_ep,R_ap)
      R8_up=R_(10)
C FDC40_l.fgi(  75, 239):�������-�������� ������� (20)
      R8_op=R8_up
C FDC40_l.fgi(  94, 245):������,UKG1_P
      call lin_ext(REAL(R8_av,4),I2_ir,R_(11),R_er,R_ar)
      R0_es=R_(11)
C FDC40_l.fgi(  56, 204):�������-�������� ������� (20)
      call lin_ext(REAL(R0_es,4),I2_as,R_(12),R_ur,R_or)
      R0_os=R_(12)
C FDC40_l.fgi(  76, 204):�������-�������� ������� (20)
      R_(15) = R0_os * R8_is
C FDC40_l.fgi( 121, 209):����������
      call lin_ext(REAL(R0_es,4),I2_um,R_(9),R_im,R_em)
      R0_om=R_(9)
C FDC40_l.fgi(  77, 186):�������-�������� ������� (20)
      R_(13) = R0_om * R8_is
C FDC40_l.fgi( 122, 191):����������
      !��������� R_(14) = FDC40_lC?? /0.0/
      R_(14)=R0_us
C FDC40_l.fgi( 137, 193):���������
      if(L_at) then
         R8_et=R_(13)
      else
         R8_et=R_(14)
      endif
C FDC40_l.fgi( 140, 191):���� RE IN LO CH7
      !��������� R_(16) = FDC40_lC?? /0.0/
      R_(16)=R0_it
C FDC40_l.fgi( 136, 211):���������
      if(L_ot) then
         R8_ut=R_(15)
      else
         R8_ut=R_(16)
      endif
C FDC40_l.fgi( 139, 209):���� RE IN LO CH7
      End
