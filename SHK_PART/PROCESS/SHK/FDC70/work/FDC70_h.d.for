      subroutine FDC70_hData!
      implicit none
      include 'FDC70_h.fh'

      REAL*8 FlwTnSqFrom(8)
      COMMON/FDC70_h_nowrt/ FlwTnSqFrom
      REAL*8 FlwHTnFromUp(8)
      COMMON/FDC70_h_nowrt/ FlwHTnFromUp
      REAL*8 FlwTnSqTo(8)
      COMMON/FDC70_h_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromDo(8)
      COMMON/FDC70_h_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(8)
      COMMON/FDC70_h_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(8)
      COMMON/FDC70_h_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 INVPERe(8)
      COMMON/FDC70_h_nowrt/ INVPERe
      INTEGER*4 NZSUBe(48)
      COMMON/FDC70_h_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(9)
      COMMON/FDC70_h_nowrt/ INZSUBe
      INTEGER*4 ILNZe(9)
      COMMON/FDC70_h_nowrt/ ILNZe
      INTEGER*4 INVPERp(8)
      COMMON/FDC70_h_nowrt/ INVPERp
      INTEGER*4 NZSUBp(48)
      COMMON/FDC70_h_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(9)
      COMMON/FDC70_h_nowrt/ INZSUBp
      INTEGER*4 ILNZp(9)
      COMMON/FDC70_h_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=8)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=8)
      INTEGER*4 Nent
      PARAMETER (Nent=8)
      INTEGER*4 Npc
      PARAMETER (Npc=16)
      INTEGER*4 Npres
      PARAMETER (Npres=8)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(8)
      COMMON/FDC70_h_nowrt/ PIntIter
      LOGICAL*1 EIntIter(8)
      COMMON/FDC70_h_nowrt/ EIntIter
      LOGICAL*1 EextIter(8)
      COMMON/FDC70_h_nowrt/ EextIter
      LOGICAL*1 PExtIter(8)
      COMMON/FDC70_h_nowrt/ PExtIter
      !DEC$PSECT/FDC70_h_NOWRT/ NoWRT
      DATA FlwTnSqTo(1:8)/0,0,0,0,0,0,0,0/
      DATA FlwTnSqFrom(1:8)/0,0,0,0,0,0,0,0/
      DATA FlwHTnToDo(1:8)/0,0,0,0,0,0,0,0/
      DATA FlwHTnToUp(1:8)/0,0,0,0,0,0,0,0/
      DATA FlwHTnFromDo(1:8)/0,0,0,0,0,0,0,0/
      DATA FlwHTnFromUp(1:8)/0,0,0,0,0,0,0,0/
      DATA EextIter(1:8)/.false.,.true.,.true.,.false.,.true.,.true.,
     >.true.,.false./
      DATA PExtIter(1:8)/.false.,.true.,.true.,.false.,.true.,.true.,
     >.true.,.false./
      DATA EIntIter(1:8)/.false.,.false.,.false.,.false.,.false.,.false.
     >,.false.,.false./
      DATA PIntIter(1:8)/.false.,.false.,.false.,.false.,.false.,.false.
     >,.false.,.false./
      DATA INVPERe(1:8)/1,4,2,5,3,6,7,8/
      DATA NZSUBe(1:48)/2,3,7,6,7,8,1,1,8,1214343432,1661190084,0,0,0,
     >1214343448,1661190084,1666603948,1666603944,0,41,17568000,
     >1666604284,105976896,1214330588,1108610,1194692,1214330600,1108790
     >,1194692,1214330616,1111215,32,1666384032,1214330628,1110734,41,0,
     >1214330664,1106850,17568008,120,33,1,32,1666384032,1,32,1214330740
     >/
      DATA INZSUBe(1:9)/1,2,3,4,4,5,6,6,6/
      DATA ILNZe(1:9)/1,2,3,4,5,6,7,8,8/
      DATA INVPERp(1:8)/1,4,2,5,3,6,7,8/
      DATA NZSUBp(1:48)/2,3,7,6,7,8,1,1,8,1214343432,1661190084,0,0,0,
     >1214343448,1661190084,1666603948,1666603944,1666604084,1666604304,
     >1666604036,1666604284,105976896,105977856,105977536,105977344,
     >105986976,105987264,105987744,105989472,0,0,0,0,0,0,105988128,
     >105977280,105995344,105977664,105986880,0,0,203604272,1666604044,0
     >,0,0/
      DATA INZSUBp(1:9)/1,2,3,4,4,5,6,6,6/
      DATA ILNZp(1:9)/1,2,3,4,5,6,7,8,8/
      return
      end

