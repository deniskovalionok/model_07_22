      Subroutine FDB10_vent_l(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDB10_vent_l.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R_(54)=R8_ox
C FDB10_vent_log.fgi(  59, 260):pre: �������������� �����  
      R_(50)=R8_iv
C FDB10_vent_log.fgi( 135, 260):pre: �������������� �����  
      if(R8_ed.le.R0_u) then
         R8_ad=R0_o
      elseif(R8_ed.gt.R0_i) then
         R8_ad=R0_e
      else
         R8_ad=R0_o+(R8_ed-(R0_u))*(R0_e-(R0_o))/(R0_i-(R0_u
     &))
      endif
C FDB10_vent_log.fgi( 127,  56):��������������� ���������
      R_(1) = 60
C FDB10_vent_log.fgi( 126,  72):��������� (RE4) (�������)
      R_(2) = 1000
C FDB10_vent_log.fgi( 119,  75):��������� (RE4) (�������)
      R_(3) = R8_od * R_(2)
C FDB10_vent_log.fgi( 121,  77):����������
      if(R_(1).ge.0.0) then
         R8_id=R_(3)/max(R_(1),1.0e-10)
      else
         R8_id=R_(3)/min(R_(1),-1.0e-10)
      endif
C FDB10_vent_log.fgi( 130,  75):�������� ����������
      R_(4) = 60
C FDB10_vent_log.fgi( 126,  85):��������� (RE4) (�������)
      R_(5) = 1000
C FDB10_vent_log.fgi( 119,  88):��������� (RE4) (�������)
      R_(6) = R8_af * R_(5)
C FDB10_vent_log.fgi( 121,  90):����������
      if(R_(4).ge.0.0) then
         R8_ud=R_(6)/max(R_(4),1.0e-10)
      else
         R8_ud=R_(6)/min(R_(4),-1.0e-10)
      endif
C FDB10_vent_log.fgi( 130,  88):�������� ����������
      R_(7) = 60
C FDB10_vent_log.fgi( 126,  98):��������� (RE4) (�������)
      R_(8) = 1000
C FDB10_vent_log.fgi( 119, 101):��������� (RE4) (�������)
      R_(9) = R8_if * R_(8)
C FDB10_vent_log.fgi( 121, 103):����������
      if(R_(7).ge.0.0) then
         R8_ef=R_(9)/max(R_(7),1.0e-10)
      else
         R8_ef=R_(9)/min(R_(7),-1.0e-10)
      endif
C FDB10_vent_log.fgi( 130, 101):�������� ����������
      R_(10) = 60
C FDB10_vent_log.fgi( 126, 111):��������� (RE4) (�������)
      R_(11) = 1000
C FDB10_vent_log.fgi( 119, 114):��������� (RE4) (�������)
      R_(12) = R8_uf * R_(11)
C FDB10_vent_log.fgi( 121, 116):����������
      if(R_(10).ge.0.0) then
         R8_of=R_(12)/max(R_(10),1.0e-10)
      else
         R8_of=R_(12)/min(R_(10),-1.0e-10)
      endif
C FDB10_vent_log.fgi( 130, 114):�������� ����������
      R_(13) = 60
C FDB10_vent_log.fgi(  53, 111):��������� (RE4) (�������)
      R_(14) = 1000
C FDB10_vent_log.fgi(  46, 114):��������� (RE4) (�������)
      R_(15) = R8_ek * R_(14)
C FDB10_vent_log.fgi(  48, 116):����������
      if(R_(13).ge.0.0) then
         R8_ak=R_(15)/max(R_(13),1.0e-10)
      else
         R8_ak=R_(15)/min(R_(13),-1.0e-10)
      endif
C FDB10_vent_log.fgi(  57, 114):�������� ����������
      R8_ik = R8_ok
C FDB10_vent_log.fgi( 128, 127):��������
      R8_uk = R8_al
C FDB10_vent_log.fgi(  55, 127):��������
      R_(16) = 1e-3
C FDB10_vent_log.fgi( 128, 174):��������� (RE4) (�������)
      R_(17) = R8_il * R_(16)
C FDB10_vent_log.fgi( 131, 176):����������
      R8_el = R_(17)
C FDB10_vent_log.fgi( 135, 176):��������
      R_(18) = 1e-3
C FDB10_vent_log.fgi(  54, 174):��������� (RE4) (�������)
      R_(19) = R8_ul * R_(18)
C FDB10_vent_log.fgi(  57, 176):����������
      R8_ol = R_(19)
C FDB10_vent_log.fgi(  61, 176):��������
      R_(21) = R8_am + (-R8_at)
C FDB10_vent_log.fgi( 120, 142):��������
      R_(20) = 1e-3
C FDB10_vent_log.fgi( 128, 139):��������� (RE4) (�������)
      R_(22) = R_(21) * R_(20)
C FDB10_vent_log.fgi( 131, 141):����������
      R8_em = R_(22)
C FDB10_vent_log.fgi( 135, 141):��������
      R_(24) = R8_im + (-R8_at)
C FDB10_vent_log.fgi( 120, 152):��������
      R_(23) = 1e-3
C FDB10_vent_log.fgi( 128, 149):��������� (RE4) (�������)
      R_(25) = R_(24) * R_(23)
C FDB10_vent_log.fgi( 131, 151):����������
      R8_om = R_(25)
C FDB10_vent_log.fgi( 135, 151):��������
      R_(27) = R8_um + (-R8_at)
C FDB10_vent_log.fgi( 120, 160):��������
      R_(26) = 1e-3
C FDB10_vent_log.fgi( 128, 157):��������� (RE4) (�������)
      R_(28) = R_(27) * R_(26)
C FDB10_vent_log.fgi( 131, 159):����������
      R8_ap = R_(28)
C FDB10_vent_log.fgi( 135, 159):��������
      R_(30) = R8_ep + (-R8_at)
C FDB10_vent_log.fgi( 120, 169):��������
      R_(29) = 1e-3
C FDB10_vent_log.fgi( 128, 166):��������� (RE4) (�������)
      R_(31) = R_(30) * R_(29)
C FDB10_vent_log.fgi( 131, 168):����������
      R8_ip = R_(31)
C FDB10_vent_log.fgi( 135, 168):��������
      R_(33) = R8_op + (-R8_at)
C FDB10_vent_log.fgi(  46, 142):��������
      R_(32) = 1e-3
C FDB10_vent_log.fgi(  54, 139):��������� (RE4) (�������)
      R_(34) = R_(33) * R_(32)
C FDB10_vent_log.fgi(  57, 141):����������
      R8_up = R_(34)
C FDB10_vent_log.fgi(  61, 141):��������
      R_(36) = R8_ar + (-R8_at)
C FDB10_vent_log.fgi(  46, 152):��������
      R_(35) = 1e-3
C FDB10_vent_log.fgi(  54, 149):��������� (RE4) (�������)
      R_(37) = R_(36) * R_(35)
C FDB10_vent_log.fgi(  57, 151):����������
      R8_er = R_(37)
C FDB10_vent_log.fgi(  61, 151):��������
      R_(39) = R8_ir + (-R8_at)
C FDB10_vent_log.fgi(  46, 160):��������
      R_(38) = 1e-3
C FDB10_vent_log.fgi(  54, 157):��������� (RE4) (�������)
      R_(40) = R_(39) * R_(38)
C FDB10_vent_log.fgi(  57, 159):����������
      R8_or = R_(40)
C FDB10_vent_log.fgi(  61, 159):��������
      R_(42) = R8_ur + (-R8_at)
C FDB10_vent_log.fgi(  46, 169):��������
      R_(41) = 1e-3
C FDB10_vent_log.fgi(  54, 166):��������� (RE4) (�������)
      R_(43) = R_(42) * R_(41)
C FDB10_vent_log.fgi(  57, 168):����������
      R8_as = R_(43)
C FDB10_vent_log.fgi(  61, 168):��������
      R_(45) = R8_at + (-R8_es)
C FDB10_vent_log.fgi( 120, 193):��������
      R_(44) = 1e-3
C FDB10_vent_log.fgi( 128, 190):��������� (RE4) (�������)
      R_(46) = R_(45) * R_(44)
C FDB10_vent_log.fgi( 131, 192):����������
      R8_is = R_(46)
C FDB10_vent_log.fgi( 135, 192):��������
      R_(48) = R8_at + (-R8_os)
C FDB10_vent_log.fgi(  46, 193):��������
      R_(47) = 1e-3
C FDB10_vent_log.fgi(  54, 190):��������� (RE4) (�������)
      R_(49) = R_(48) * R_(47)
C FDB10_vent_log.fgi(  57, 192):����������
      R8_us = R_(49)
C FDB10_vent_log.fgi(  61, 192):��������
      R8_et = R8_it
C FDB10_vent_log.fgi(  48, 213):��������
      R8_ot = R8_ut
C FDB10_vent_log.fgi(  48, 228):��������
      !��������� R_(53) = FDB10_vent_logC?? /0.0/
      R_(53)=R0_ov
C FDB10_vent_log.fgi( 122, 261):���������
      !��������� R_(52) = FDB10_vent_logC?? /1.0/
      R_(52)=R0_uv
C FDB10_vent_log.fgi( 122, 259):���������
      if(L_ax) then
         R_(51)=R_(52)
      else
         R_(51)=R_(53)
      endif
C FDB10_vent_log.fgi( 125, 259):���� RE IN LO CH7
      R8_iv=(R0_av*R_(50)+deltat*R_(51))/(R0_av+deltat)
C FDB10_vent_log.fgi( 135, 260):�������������� �����  
      R8_ev=R8_iv
C FDB10_vent_log.fgi( 154, 260):������,F_FDB13CU001_G
      !��������� R_(57) = FDB10_vent_logC?? /0.0/
      R_(57)=R0_ux
C FDB10_vent_log.fgi(  44, 261):���������
      !��������� R_(56) = FDB10_vent_logC?? /1.0/
      R_(56)=R0_abe
C FDB10_vent_log.fgi(  44, 259):���������
      if(L_ebe) then
         R_(55)=R_(56)
      else
         R_(55)=R_(57)
      endif
C FDB10_vent_log.fgi(  47, 259):���� RE IN LO CH7
      R8_ox=(R0_ex*R_(54)+deltat*R_(55))/(R0_ex+deltat)
C FDB10_vent_log.fgi(  59, 260):�������������� �����  
      R8_ix=R8_ox
C FDB10_vent_log.fgi(  76, 260):������,F_FDB11AX001_G
      End
