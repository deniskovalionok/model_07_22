      Subroutine gr_parLinkData
      Call gr_parData
      End

****** Begin file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET41.fs'
c   ������ ��������, �������� � ��������� � ����                     
c   �� ���������, ������������� �.�.��������� 19.01.99

      subroutine gr_par(deltat)
c
      IMPLICIT NONE
  
      include 'gr_par.fh'
      REAL*8 FlwTnSqFrom(44)
      COMMON/gr_par_nowrt/ FlwTnSqFrom
      REAL*8 FlwTnSqTo(44)
      COMMON/gr_par_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromUp(44)
      COMMON/gr_par_nowrt/ FlwHTnFromUp
      REAL*8 FlwHTnFromDo(44)
      COMMON/gr_par_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(44)
      COMMON/gr_par_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(44)
      COMMON/gr_par_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tc2Dim_TMax
      PARAMETER (Tc2Dim_TMax=2)
      INTEGER*4 Tn3Dim_TMax
      PARAMETER (Tn3Dim_TMax=2)
      INTEGER*4 INVPERe(51)
      COMMON/gr_par_nowrt/ INVPERe
      INTEGER*4 NZSUBe(540)
      COMMON/gr_par_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(52)
      COMMON/gr_par_nowrt/ INZSUBe
      INTEGER*4 ILNZe(52)
      COMMON/gr_par_nowrt/ ILNZe
      INTEGER*4 INVPERp(43)
      COMMON/gr_par_nowrt/ INVPERp
      INTEGER*4 NZSUBp(324)
      COMMON/gr_par_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(44)
      COMMON/gr_par_nowrt/ INZSUBp
      INTEGER*4 ILNZp(44)
      COMMON/gr_par_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=129)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=65)
      INTEGER*4 Nent
      PARAMETER (Nent=51)
      INTEGER*4 Npc
      PARAMETER (Npc=86)
      INTEGER*4 Npres
      PARAMETER (Npres=43)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(43)
      COMMON/gr_par_nowrt/ PIntIter
      LOGICAL*1 EIntIter(51)
      COMMON/gr_par_nowrt/ EIntIter
      LOGICAL*1 PExtIter(43)
      COMMON/gr_par_nowrt/ PExtIter
      LOGICAL*1 EextIter(51)
      COMMON/gr_par_nowrt/ EextIter
      CHARACTER*16 ConFlOutNames(1)
      COMMON/gr_par_nowrt/ ConFlOutNames
      !DEC$PSECT/gr_par_NOWRT/ NoWRT
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13 

****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
      integer*4 Ncomp,CompLin(1),CompOin(1),CompGenTyp(1)
      parameter(Ncomp=0)
      real*8 CompQq(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 22 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 27 


****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'

c      real*8 FlwCondRegul(nv)
c      common/gr_parFlwL/ FlwCondRegul
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 38 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 73 
      integer*4 nfwv
      parameter(nfwv=0)
      integer*4 FwvFloWw(1),FwvFloWv(1)
      real*8 FwvRoFrom1(1),FwvRoFrom2(1),FwvRoTo1(1),FwvRoTo2(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 79 
      integer*4 nfwv1
      parameter(nfwv1=0)
      integer*4 Fwv1Flow(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 84 
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 100 
* ���������� ��� ��������������� ��������� ����������
      real*8 C0He2(NHe2) !����. ��� ������� ����������� ���� ��
      real*8 C1He2(NHe2) !����. ��� ������� ����������� ���� ��
      real*8 C2He2(NHe2) !����. ��� ������� ����������� ���� ��
      real*8 DTHe2
      real*8 C0He2b1(NHe2),C1He2b1(NHe2),C2He2b1(NHe2)
      real*8 C0He2b2(NHe2),C1He2b2(NHe2),C2He2b2(NHe2)
      real*8 He2T1t(2,NHe2),He2T2t(2,NHe2),He2decQt(NHe2)
      real*8 He2vvGa(NHe2)
      common/gr_parHE2/ C0He2,C1He2,C2He2,DTHe2,
     * C0He2b1,C1He2b1,C2He2b1,C0He2b2,C1He2b2,C2He2b2,
     * He2T1t,He2T2t,He2decQt,He2vvGa
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 114 
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 125 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 132 

      integer*4 NMtl
      parameter ( NMtl=0 )
      real*8 MtlTt(1),MtlMass(1),MtlCp(1),MtlAlCulc(1)
      real*8 Mtl_QqOut(1)  !��������� �������� ����� � ������� ��������
      real*8 MtlLamdC(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 140 

      integer*4 NMtlO,MtlOIndRoom(1)
      parameter ( NMtlO=0 )
      real*8 MtlOTt(1),MtlOMass(1),MtlOCp(1),MtlOAlCulcWl1(1)
      real*8 MtlO_QqOut(1)  !��������� �������� ����� � ������� ��������
      real*8 MtlOLamdC(1),MtlOLamd(1),MtlOQqEnv(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 148 
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'

      integer*4 NUCH
      parameter (NUCH=0)
      integer*4 CoalHConNodInd(1)
      real*8 CoalHNodMoistG(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 157 

      integer*4 NFlwWasteSet
      parameter(NFlwWasteSet=0)
      integer*4 FlwWasteSetConnInd(1),FlwWasteSetNum(1)
      real*8 FlwWasteSetWaste(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 164 

      integer*4 FlwConnFlwInd(1),NFlwConn
      parameter(NFlwConn=0)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 169 

      real*8 HC1PpFrom(NHC1),HC1PpTo(NHC1)
      common/gr_parConFlOut/ HC1PpFrom,HC1PpTo
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 174 

      integer*4 NHCO
      parameter(NHCO=0)
      integer*4  HCOFlMod(1),HCOIndFlw(1)
      real*8 HCOCfMod(1),HCOOFg(1),HCOONodVol(1),HCOONoddRodP(1)
      common/gr_parConFlOut/ HCOFlMod,HCOCfMod,HCOOFg,HCOONodVol,
     >HCOONoddRodP,HCOIndFlw
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 182 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 189 

      integer*4 nCrv
      parameter(nCrv=0)
      real*8 CrvRes(1)
      integer*4 CrvFlw(nCrv)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 196 

      integer*4 nWst
      parameter(nWst=0)
      integer*4 WstNode(1)
      real*8    WSTConsN2(1),WSTConsO2(1),WSTConsH2O(1),
     *          WSTConsCO2(1),WSTConsSoot(1),WSTConsAsh(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 204 

      integer*4 NXS
      parameter(NXS=0)
      integer*4 XSFlagSetCc(1),XSFlSet(30,1),XSIndSetCc(1),
     >XSIndSetCc0(1)
      real*8    XSCcstr(30,1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 211 

      integer*4 ntFrB
      parameter(ntFrB=0)
      integer*4 tFrBNdInd(1),tFrBi(1)
      real*8    tFrBCcOld(100,1),tFrBTau(1)
      real*8    tFrBGIn(1),tFrBAa(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 219 

      integer*4 np4
      parameter(np4=0)
      integer*4 Pm4Nb(1)
      real*8 Pm4Res(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 226 

c ��������� �����. ��� ������� � ���������������, ���� �� (�������) ���
      integer*4 np
      parameter(np=0)
      integer*4 VNKZ(1)
      real*8 PumRes(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 234 
      integer*4 np2
      parameter(np2=0)
      integer*4 Pm2Nb(1)
      real*8 Pm2Res(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 240 
      integer*4 nFan
      parameter(nFan=0)
      integer*4 FanFlw(1)
      real*8 FanRes(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 246 
      integer*4 nFan2
      parameter(nFan2=0)
      integer*4 Fan2Flw(1)
      real*8 Fan2Res(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 252 
      integer*4 np3
      parameter(np3=0)
      real*8 Pm3QqNod(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 257 
      integer*4 nfr1
      parameter(nfr1=0)
      integer*4 Fr1Flw(1)
      real*8 Fr1Bor(1)
      real*8 Fr1KkFullWst(1)
      real*8 Fr1Waste_T(100,1)
      real*8 Fr1WstMass_T(100,1)
      integer*4 Fr1SumMas_0(1),Fr1FlSumMas(1),Fr1F0SumMas(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 267 

      real*8 time_df1  !������� ��� ������� ������� (� �����)
      real*8 time_tmp

      real   deltat      !��� ����������
      real   deltat1      !��� ����������
      REAL*8 Atau        !1/dt
c      save Atau
      real*4 dtInt       !dt*Accel
      real*8 TimeSrv     !dt/Niter
      character*100 fn1  ! ��� ������������� .cbf ������
c      common/gr_parLocal1/ time_tmp,deltat,Atau,dtInt,TimeSrv,fn1
      common/gr_parLocal1/ Atau,time_tmp,deltat1,dtInt,TimeSrv,fn1
      real*8 VOL1(nu) !V/dt
      real*8 NodCfdE(nu) !����. � ��. ����. ��� ����� ���������
      real*8 NodTtk(nu)  !����������� ��������. � ������� ����� �� k-�� ��������
      real*8 RotVol(nu) !��������� ������
      real*8 RokVol(nu) !��������� ������
      real*8 NodVolk(nu)  !������������ ����� �� k-�� ��������
      real*8 NoddVdPk(nu) !����������� ������ � ���� �� �������� �� k-�� ����.

c ---  ������ �. (29.08.02) - ������
      real*8 NodSum1(nu)
      real*8 NodSum2(nu)
c ---  ������ �. (29.08.02) - �����

      real*8 NodQqPmAd(nu)

      common/gr_parNODE1/ VOL1,NodCfdE,NodTtk,RotVol,RokVol,
     * NodVolk,NoddVdPk,NodSum1,NodSum2,NodQqPmAd
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 299 

      real*8 C7(nv)   !��������� ������������
      real*8 Gk(nv)   !������� �� k-�� ��������
      real*8 dHLev(nv)!������ �� ����� �� ���� ������� � �������
      real*8 Rovk(nv)    !��. ��������� �� k-�� ��������
      real*8 AconRovk(nv)!��������� ��� ��������
c###### ��������� ####################################
      real*8 FlwGaVeSq(nv)  ! ��������� ������ ��� �������� ������������� �����
      real*8 flwbcon(nv)    ! �������� ��������� �������� �������� �� ����� 
      real*8 FlwC7work1(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work2(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work3(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work4(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work5(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work7(nv)
      real*8 FlwVsk    (nv)   ! �������� �� ����� �� ���������
      real*8 ComEeFrom  (nv)   ! ��������� ��������� ��������� � ��������
      real*8 ComEeTo    (nv)   ! ��������� ��������� ��������� � ��������  
      real*8 FlwSlGaFrom(nv)   ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaFrom0(nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaTo   (nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaTo0  (nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwCcFrom(nv) ! ��������� ������ ��� ������������ ���� �� ������ �����
      real*8 FlwCcTo(nv)   ! ��������� ������ ��� ������������ ���� �� ������ �����
      real*8 flwdhz0(nv)
      real*8 ComRoFrom(3,nv)
      real*8 ComRoTo  (3,nv)
c###### ��������� ####################################
      real*8 FlwKgp(nf,2,nv)    ! 
      real*8 FlwKge(nf,nf,nv)
      real*8 FlwKgg(3,nv)
      real*8 FlwKgpt(2,nv)
      real*8 FlwdPLevFrom(nv),FlwdPLevTo(nv)
      real*8 FlwCcGgWtJa(nv)
      real*8 FlwRes_Iter(nv)
      real*8 FlwGG_TMP(nv)
      real*8 FlwRes_Tmp
      real*8 FlwdCgDCcFrom(nv),FlwdCgDCcTo(nv)
c      real*8 frco0(nv)
      common/gr_parFLOW/ C7,Gk,dHLev,Rovk,AconRovk,FlwGaVeSq,
     * flwbcon,FlwC7work1,
     * FlwC7work2,FlwC7work3,FlwC7work4,FlwC7work5,FlwC7work7,
     * FlwVsk,ComEeFrom,ComEeTo,FlwSlGaFrom,FlwSlGaFrom0,
     * FlwSlGaTo,FlwSlGaTo0,FlwCcFrom,FlwCcTo,flwdhz0,ComRoFrom,
     * ComRoTo,FlwKgp,FlwKge,FlwKgg,FlwKgpt,
     * FlwdPLevFrom,FlwdPLevTo,FlwCcGgWtJa,
     * FlwRes_Iter,FlwRes_Tmp,FlwGG_TMP,FlwdCgDCcFrom,FlwdCgDCcTo
c    * ,frco0
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 349 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 354 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 359 


c      !������� ����. ����� ����� � ���. ��������� ��� ��������
       real*8 Dmp(Npres,Npres)
C       real*8 Dmpc(Npc,Npc) ! �������� 
c      !������� ����. ����� ����� � ������� ��������� ��� ���������
       real*8 Dmh(Nent,Nent)
       real*8 DmhG(Nent,Nent)
c      !������ ����� ��� ���������� ������� �� ����.
      Real*8 Yp(Npres)
c      Real*8 Ypc(Npc)
c      !������ ����� ��� ���������� ������� �� ���.
      real*8 Ye(Nent)
      real*8 YeG(Nent)
c      !������� �� ���������
      real*8 dP(Npres)
c      real*8 dPC(Npc)
c      !������� �� ����������
      real*8 dE(Nent)
      real*8 dEG(Nent)
c      !������ ������ �������� �� k-�� ��������
      real*8 Pk(Npres)
c      !������ ������ �������� �� (k+1)-�� ��������
      real*8 Pk1(Npres)
c      !������ ������ ��������� �� k-�� ��������
      real*8 Ek(Nent)
      real*8 EGk(Nent)
c      !������ ������ ��������� �� (k+1)-�� ��������
      real*8 Ek1(Nent)
      real*8 EGk1(Nent)
      real*8 Rok(Nu) !�����. �\� ����� � ������� ����� �� k-�� ����.
      !������ ������ ��������� �����. ����
      real*8 EeWtSatk(Nent)
      !������ ������ ��������� �����. ����
      real*8 EeVpSatk(Nent)
      real*8 MaxDevP     !����. �����. ����. �������� �� ��������
      real*8 MaxDevH     !����. �����. ����. ��������� �� ��������
      INTEGER*4 I,j     !���������� �����
      integer*4 IC        !������� ��������
      integer*4 ICInt !������� ���������� ��������
      common/gr_parLocal2/ Dmp,Dmh,Yp,Ye,dP,dE,Pk,Pk1,Ek,Ek1,Rok,
     *  EeWtSatk,EeVpSatk,MaxDevP,MaxDevH,IC,ICInt,DmhG,YeG,dEG,
     *  EGk,EGk1       !,dPC,Ypc,Dmpc <=������ ��������!!!!

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 407 

      INTEGER*4 nvlgpos !����� ����� � �������������� ���������� � ����������
      common/gr_parVlvGeo/ nvlgpos
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 412 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 417 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 422 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 427 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 432 

      integer*4 nsep1,Sep1FlwS(1),Sep1Node(1)
        parameter(nsep1=0)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 437 

      integer*4 nInWst,InWstNum(1),InWstNode(1),InWstNdTp(1)
      real*8    InWstdt(1)
      integer*4 InWstTyp(1)
      real*8    InWstCc(1)
      real*8 InWstExtCon(1),InWstExtWstdt(1)
      parameter(nInWst=0)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 446 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 451 


      LOGICAL*1 go,goint  !������� ����������� ��������
      DATA go/.true./     !����������� �������� �������. ��������
      common/gr_parLocal4/ go,goint
      real*8  RDTSC
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 490 
      real*8 NoddRodPk(nu) !dRo/dP ��� ������� ����� �� ����. ����.
      real*8 NoddRodEk(nu) !dRo/d� ��� ������� ����� �� ����. ����.
      real*8 NoddRodPk1(nu) !dRo/dP ��� ������� ����� �� ���. ����.
      real*8 NoddRodEk1(nu) !dRo/d� ��� ������� ����� �� ���. ����.
      real*8 nodrorelax(nu)
      real*8 NodRoGa0(nu)
      real*8 NodCcGa_Aeq0(nu) 
      real*8 NodCcGa_Deq0(nu) 
      real*8 NodCcGa_Beq (nu)
      real*8 NodCcGa_Beq0(nu)
      real*8 NodCcGa_Aeq (nu)
      real*8 NodCcGa_Deq (nu)
      real*8 NodXxk1(nu)    !���. �������. � ������� ����� �� ���. ����.
      real*8 NodXxBalk1(nu) !���. �������. � ������� ����� �� ���. ����.
      real*8 NodQqAd(nu)
      real*8 NodCfEeAsh(nu)
      real*8 NodEeWvt(nu)
      real*8 NodAlSqGaWv(nu),NodYe(nu),NodDmh(nu)
      real*8 NodPsw(nu),NodRoWvTmp(nu),NoddRdHGas(nu)
     *,NoddRdPWvTmp(nu),NoddRdHWvTmp(nu)
     *,NodTtWvTmp(nu),NodCpWvTmp(nu),NodXxTmp(nu)
      integer*4 Nod_ghm_flag(nu),NodPropRelaxFlag(nu)
      logical*1 EExtIter_(Nent)
      logical*1 PExtIter_(Npres)
      common /gr_parNODE/ NoddRodPk,NoddRodEk,NoddRodPk1,NoddRodEk1,
     *  nodrorelax,NodRoGa0,NodCcGa_Aeq0,NodCcGa_Deq0,NodCcGa_Beq,
     *  NodCcGa_Beq0,NodCcGa_Aeq,NodCcGa_Deq,NodXxk1,NodXxBalk1,
     *  NodQqAd,NodCfEeAsh,NodEeWvt,NodAlSqGaWv,NodYe,NodDmh,
     *  NodPsw,NodRoWvTmp,NoddRdHGas,NodTtWvTmp,NodCpWvTmp,NodXxTmp
      common /gr_parNODE_I/ Nod_ghm_flag,NodPropRelaxFlag
      common /gr_parNODE_L/ EExtIter_,PExtIter_
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 523 
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'

      integer NTc0
      parameter(NTc0=0)
      integer*4 Tc0IbnFro(1),Tc0NodFro(1),Tc0IbnTo(1),Tc0NodTo(1)
      real*8 Tc0zt(1),Tc0AlG23(1),Tc0AlG43(1)
     *,Tc0G43(1),Tc0G23(1),Tc0G34(1),Tc0CpWl(1)
     *,Tc0TtEnv(1),Tc0TtWlk(1),Tc0TtWl(1),Tc0AlSqEnv(1),Tc0AlEnv(1)
     *,Tc0AlSqPhWl(1,nf),Tc0MasCpWl(1),Tc0CfAlEnv(1)
     *,Tc0CfAlGaWl(1),Tc0CfAlWpUpWl(1)
     *,Tc0CfAlWtWl(1),Tc0CfAlWpDoWl(1)
     *,Tc0EvUpWl(1),Tc0EvDoWl(1)
     *,Tc0dG23dp(1),Tc0dG34dp(1),Tc0dG43dp(1)
     *,Tc0CfGgWpUpWt(1),Tc0CcGgWtWpDo(1),Tc0CfGgWpDoWt(1)
     *,Tc0G32(1),Tc0dG32dp(1),Tc0CfGgWt(1),Tc0SqWl(1),Tc0TettaQ(1,nf)
     *,Tc0QqIsp(1),Tc0AlIsp(1)
     *,Tc0dQIspdP(1),Tc0TtTnIsp(1),Tc0AlSqIsp(1),TC0ResWl(1)
     *,Tc0dQG23dp(1),Tc0TettaQ2(1,nf),Tc0DeltaTs(1)
     *,Tc0AlG23From(1),Tc0AlG43From(1)
     *,Tc0G23From(1),Tc0G34From(1),Tc0G32From(1),Tc0G43From(1)
     *,Tc0dG23dpFrom(1),Tc0dG34dpFrom(1),Tc0dG43dpFrom(1),
     >Tc0dG32dpFrom(1)
     *,Tc0dQIspdPFrom(1),Tc0TtTnIspFrom(1),Tc0AlSqIspFrom(1)
     *,Tc0CcGgWtWpDoFrom(1),Tc0CfGgWpUpWtFrom(1),Tc0CfGgWpDoWtFrom(1)
     *,Tc0QqIspFrom(1),Tc0AlIspFrom(1),Tc0CfGgWtFrom(1),Tc0TettaQFrom(1,
     >nf)
     *,Tc0AlSqPhWlFrom(1,nf),Tc0CfRelAlIsp(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 553 
C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 582 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 605 
      real*8 Tc2zt(NTc2),Tc2AlG23(NTc2),Tc2G23(NTc2),Tc2G34(NTc2)
     *,Tc2AlG43(NTc2),Tc2G43(NTc2),Tc2TtWlk(NTc2),Tc2AlPhWl(NTc2,nf)
     *,Tc2AlSqPhWl(NTc2,nf),Tc2dG23dp(NTc2),Tc2dG34dp(NTc2)
     *,Tc2dG43dp(NTc2),Tc2G32(Ntc2),Tc2dG32dp(Ntc2)
c     *,Tc2TettaQ(NTc2,nf)
     *,Tc2dQIspdP(NTc2),Tc2TtTnIsp(NTc2),Tc2AlSqIsp(NTc2)
      real*8 Tc2AlEnv_P(NTc2)
      common/gr_parTC2/ Tc2zt,Tc2AlG23,Tc2G23,Tc2G34
     *,Tc2AlG43,Tc2G43,Tc2TtWlk,Tc2AlPhWl
     *,Tc2AlSqPhWl,Tc2dG23dp,Tc2dG34dp,Tc2dG43dp,Tc2G32,Tc2dG32dp
c     *,Tc2TettaQ
     *,Tc2dQIspdP,Tc2TtTnIsp,Tc2AlSqIsp,Tc2AlEnv_P
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 619 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 629 

      integer*4 NTCR,TcRNode(1)
      parameter ( NTCR=0 )
      real*8 TcR_XQ(1),TcR_BQ(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 635 

      integer*4 Nwall2O
      parameter ( Nwall2O=0 )
      real*8  Wl2OTt(6,1),Wl2ORAw(1),Wl2OSqEnv(1),Wl2OQqBout(1),
     &        Wl2ODout(1)

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 643 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 649 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'

      integer NTen
      parameter(NTen=0)
      integer*4 TenIbn(1),TenNode(1)
      real*8 Tenzt(1),TenQqIn(1),TenAlG23(1),TenG23(1),TenG34(1)
     *,TenAlG43(1),TenG43(1),TenCpWl(1),TenTtWlk(1),TenTtWl(1)
     *,TenAlSqPhWl(1,nf),TenMasCpWl(1),TenCfAlGaWl(1),TenCfAlWpUpWl(1)
     *,TenCfAlWtWl(1),TenCfAlWpDoWl(1),TenEvUpWl(1),TenEvDoWl(1)
     *,TendG23dp(1),TendG34dp(1),TendG43dp(1)
     *,TenCfGgWpUpWt(1),TenCcGgWtWpDo(1),TenCfGgWpDoWt(1)
     *,TenG32(1),TendG32dp(1),TenCfGgWt(1),TenSqWl(1),TenTettaQ(1,nf)
     *,TenQqIsp(1),TenAlIsp(1),TenDeltaTs(1)
     *,TendQIspdP(1),TenTtTnIsp(1),TenAlSqIsp(1),TenResWl(1),
     >TenTcwAlCulc(1),TenTcwQq(1)
     *,TenLawL(1),TenQqInt(1),TenQqExt(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 672 
C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 686 
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 724 

c   ��������� ���������� ��� "����� �������"
*--- ����� ���������� ����������
      real*8 Tn3Pp(n03,Tn3Ne)    ! �������
      real*8 Tn3Ee(n03,Tn3Ne)    ! ���������
      real*8 Tn3Tt(n03,Tn3Ne)    ! �����������
      real*8 Tn3TtWl(n03,2)  ! ����������� ������
      real*8 Tn3TtEnv(n03,2) ! ����������� � ���������� ����������
      real*8 Tn3Mas(n03,Tn3Ne)   ! ����� ���
      real*8 Tn3rot(n03,Tn3Ne)   ! ��������� ���
      real*8 Tn3Volt(n03,Tn3Ne)  ! ������ ���
      real*8 Tn3Voldt(Tn3Ne,n03) ! ������ ���/dt
      real*8 Tn3MasGas(n03,3)! ����� ������������ �����
*---

      real*8 Tn3rok(n03,Tn3Ne)   ! ��������� �� ��������
      real*8 Tn3pk (n03,Tn3Ne)   ! �������� �� ��������
      real*8 Tn3ek (n03,Tn3Ne)   ! �������� �� ��������
      real*8 Tn3hw (n03,Tn3Ne)   ! ��������� ��������� ���� �� ��������
      real*8 Tn3hs (n03,Tn3Ne)   ! ��������� ��������� ���� �� ��������
      real*8 Tn3RoWtSatk(n03,Tn3Ne)   ! ��������� ��������� ���� �� ��������
      real*8 Tn3cp (n03,Tn3Ne)   ! ������������
      real*8 Tn3Volk(n03,Tn3Ne)  ! ������ ��� �� ��������
      real*8 Tn3TtWlk(n03,2) ! ����������� ������ �� ��������
      real*8 Tn3Mask(Tn3Ne,n03)  ! ����� ��� �� ���������
      real*8 Tn3MasGask(3,n03) ! ����� ������������ ����� �� ���������
      real*8 Tn3Volkdt(Tn3Ne,n03) ! ������ ���/dt �� ��������

      real*8 Tn3AlSqPhPh(Tn3Ne,Tn3Ne,n03) ! alfa*S ����� ������
      real*8 Tn3SqPhPh(Tn3Ne,Tn3Ne,n03) ! S ����� ������
      real*8 Tn3AlPhPh(Tn3Ne,Tn3Ne,n03) ! �-�� ������������� ����� ����� ������
      real*8 Tn3AlSqPhWl(n03,Tn3Ne)   ! �-�� ��������. ����� ������ � �������
      real*8 Tn3AlWlUpDo(n03)
      !����. ������� ��������. ����� ������ ����� ������ � ����� ������
      real*8 Tn3ActWlUpDo(n03)
      !�-�� ������������� ����� ������� � ����������
      real*8 Tn3AlSqWlEnv(n03,2)
      real*8 Tn3MasCpWl(n03,2)    ! �����*Cp ������

      real*8 Tn3drdp(n03,Tn3Ne)
      real*8 Tn3drdh(n03,Tn3Ne)
      real*8 Tn3dh(n03,Tn3Ne)

      real*8 Tn3G(Tn3Ne,Tn3Ne,n03) 
      real*8 Tn3dGdP(Tn3Ne,Tn3Ne,n03),Tn3dg32dp2(n03),Tn3dg32dp3(n03)
      real*8 Tn3Gw23(n03) 
      real*8 Tn3dGw23dP(n03) 
      real*8 Tn3Gw34(n03) 
      real*8 Tn3dGw34dP(n03) 
      real*8 Tn3Gw43(n03) 
      real*8 Tn3dGw43dP(n03) 
      real*8 Tn3Gw32(n03) 
      real*8 Tn3dGw32dP(n03) 
      real*8 Tn3G32Sum(n03)
      real*8 Tn3G34Sum(n03)
      real*8 Tn3G23Sum(n03)
      real*8 Tn3G43Sum(n03)

      real*8 Tn3G0(Tn3Ne,Tn3Ne,n03)
      real*8 Tn3CcGas(3,n03)
      real*8 Tn3AlGw23(n03)
      real*8 Tn3AlGw34(n03)
      real*8 Tn3AlGw43(n03)
      real*8 Tn3zd(n03)
      real*8 Tn3zu(n03)
      real*8 Tn3det(n03)
      real*8 Tn3zt(n03)
      real*8 Tn3Ts(n03,Tn3Ne)
      real*8 Tn3Ts_(n03,Tn3Ne)
      real*8 Tn3rv(Tn3Ne,n03)
      real*8 Tn3GSum(Tn3Ne,n03)
      real*8 Tn3MasgenMult(Tn3Ne,n03)
      real*8 Tn3Masgen(Tn3Ne,n03)
      real*8 Tn3AlGv23(n03)
      real*8 Tn3Gv23(n03)
      real*8 Tn3dGv23dP(n03)
 
      real*8 Tn3Pk2(n03,Tn3Ne)
      real*8 Tn3hw2(n03,Tn3Ne)
      real*8 Tn3hs2(n03,Tn3Ne)
      real*8 Tn3ts2(n03,Tn3Ne)
      real*8 Tn3ts2_(n03,Tn3Ne)
      real*8 Tn3Tt2(n03,Tn3Ne)
      real*8 Tn3ro2(n03,Tn3Ne)
      real*8 Tn3cp2(n03,Tn3Ne)
      real*8 Tn3hd2(n03,Tn3Ne)
      real*8 Tn3CcUp2(2,n03)
      real*8 Tn3Xxk(n03,Tn3Ne)
      logical*1 Tn3RelaxKG
      real*8 Tn3dp(n03,Tn3Ne)
      real*8 Tn3Mas0(n03,Tn3Ne)
c      real*8 Tn3CfMasGasMin ! /1.0d-7/
c      parameter (Tn3CfMasGasMin=1.0d-7)
      real*8 Tn3TettaQ(n03,Tn3Ne)
      real*8 Tn3Vk(n03,Tn3Ne)
      real*8 Tn3dTdH(n03,Tn3Ne)
      real*8 Tn3CcUp(2,n03)
      real*8 Tn3AVolDo(n03)
      real*8 Tn3TettaWl(n03)
      real*8 Tn3LevRelk(n03),Tn3LevMask(n03)
      real*8 Tn3QqPhPh(Tn3Ne,Tn3Ne,n03),Tn3QqGen(Tn3Ne,Tn3Ne,n03)
        real*8 Tn3y1(n03),Tn3y2(n03),Tn3z1(n03),Tn3z2(n03)
      real*8 Tn3MasGaRealk(n03),Tn3MasGaReal0(n03)
      real*8 Tn3TcwAlCulc(n03,2)
      real*8 Tn3La(n03,Tn3Ne)
      real*8 Tn3Cf42(n03)
      real*8 Tn3WlTtIsp(n03)
      real*8 Tn3HD(n03,Tn3Ne),Tn3Sigma(n03),Tn3Vel42(n03)
     *,Tn3Dd4(n03)
      real*8 Tn3FlwHUp(1,1)
      real*8 Tn3FlwHDo(1,1)
      real*8 Tn3VolNorm0(n03)
      real*8 Tn3dMLoc_tmp(Tn3Ne,n03)
      real*8 Tn3VolFull(n03)
         real*8 Tn3Pk_dev(n03)

      common/gr_parTn3/
     * Tn3Pp,Tn3Ee,Tn3Tt,Tn3TtWl,Tn3TtEnv,Tn3Mas,Tn3rot,Tn3Volt,Tn3Voldt
     * ,Tn3MasGas,Tn3rok,Tn3pk,Tn3ek,Tn3hw,Tn3hs,Tn3RoWtSatk,Tn3cp
     * ,Tn3Volk,Tn3TtWlk,Tn3Mask,Tn3MasGask,Tn3Volkdt,Tn3AlSqPhPh
     * ,Tn3SqPhPh,Tn3AlPhPh,Tn3AlSqPhWl,Tn3AlWlUpDo,Tn3ActWlUpDo
     * ,Tn3AlSqWlEnv,Tn3MasCpWl,Tn3drdp,Tn3drdh,Tn3dh,Tn3G,Tn3dGdP
     * ,Tn3dg32dp2,Tn3dg32dp3,Tn3Gw23,Tn3dGw23dP,Tn3Gw34,Tn3dGw34dP
     * ,Tn3Gw43,Tn3dGw43dP,Tn3Gw32,Tn3dGw32dP,Tn3G32Sum,Tn3G34Sum
     * ,Tn3G23Sum,Tn3G43Sum,Tn3G0,Tn3CcGas,Tn3AlGw23
     * ,Tn3AlGw34,Tn3AlGw43,Tn3zd,Tn3zu,Tn3det,Tn3zt,Tn3Ts,Tn3rv
     * ,Tn3GSum,Tn3MasgenMult,Tn3Masgen,Tn3AlGv23,Tn3Gv23,Tn3dGv23dP
     * ,Tn3Pk2,Tn3hw2,Tn3hs2,Tn3ts2,Tn3Tt2,Tn3ro2,Tn3cp2,Tn3hd2
     * ,Tn3CcUp2,Tn3Xxk,Tn3dp,Tn3Mas0,Tn3TettaQ,Tn3Vk,Tn3dTdH
     * ,Tn3CcUp,Tn3AVolDo,Tn3TettaWl,Tn3LevRelk,Tn3LevMask,Tn3QqPhPh
     * ,Tn3QqGen,Tn3y1,Tn3y2,Tn3z1,Tn3z2,Tn3MasGaRealk,Tn3MasGaReal0
     * ,Tn3TcwAlCulc,Tn3La,Tn3Cf42,Tn3WlTtIsp,Tn3HD,Tn3Sigma
     * ,Tn3Vel42,Tn3Dd4,Tn3FlwHUp,Tn3FlwHDo,Tn3VolNorm0,Tn3ts_,Tn3ts2_
     * ,Tn3dMLoc_tmp,Tn3VolFull,Tn3Pk_dev
      common/gr_parTn3L/ Tn3RelaxKG
      real*8 Tn3GSpCond(nv)
      real*8 Tn3QSpCond(nv)
      common/gr_parTn3Flow/ Tn3GSpCond,Tn3QSpCond
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 866 


****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'

      integer*4 n07,Tn7RelP(1,1),Tn7RelE(2,1),Tn7Homo(1),Tn7CcSetInd(1)
      real*8    Tn7RoGa(1),Tn7RoWt(1),Tn7Rok(1,2),Tn7XxWt(1),Tn7Xk(1,2),
     *          Tn7Lev(1),Tn7EeWt(1),Tn7VsGa(1),Tn7VsWt(1),Tn7PpGa(1),
     *          Tn7EeGa(1),Tn7EeWtSat(1),Tn7EeVpSat(1),Tn7LevRel(1),
     *          Tn7Hi(1),Tn7PpWt(1),Tn7TtWt(1),Tn7Ee(2,1),
     *          Tn7Ek(2,1),Tn7CcVolk(1),Tn7CcMask(1)
     *         ,Tn7XxBalWt(1),Tn7Levk(1),Tn7Lev_Rlx(1),Tn7Acon(1),
     *          Tn7Sq(1),Tn7CcBor(1),Tn7MasWt(1),Tn7MasGa(1),
     *          Tn7MasBor(1),Tn7GCorrWt(1),
     *          Tn7dRdPk(2,1),Tn7dRdP(2,1),Tn7dRdHk(2,1),Tn7dRdH(2,1),
     *          dP_Tn7(1),Tn7Dcon(1),Tn7LsMin(1),Tn7dMLoc(2,1),
     *          Tn7CfRoGaCorr(1),Tn7CfRoWtCorr(1),
     *          Tn7Waste_T(20,1),Tn7Rowttrue(1),Tn7RoWtSat(1),
     *          Tn7RoVpSat(1),Tn7Dmint(1),Tn7Gcorr(1),
     *          Tn7WlUpQqOut(1),Tn7WlDoQqOut(1),Tn7TcwAlCulc(1,1),
     *          Tn7TtWlUp(1),Tn7TtWlDo(1),Tn7LaWl(1),Tn7QqWlEnv(1),
     >Tn7RoomInd(1)
     *          ,Tn7EvDo(1),Tn7EvDo0(1),Tn7TtGa(1),Tn7AlSqPhWl(2,1),
     >Tn7AlWlUpDo(1)
     *          ,Tn7dMIntGa(1),Tn7dMIntWt(1),Tn7GCorrGa(1),
     >Tn7LevTunG(1),Tn7LevTunPpRegG(1)
      logical*1 Tn7_Flag_Mas_Deb(1),Tn7CfCorr_On(1)


      logical*1 Tn7CcBorLb(1)

      parameter(n07=0)
      real*8 Tn7Pk(2,1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 900 

C     common/gr_parC     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 957 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
      integer n08
      parameter(n08=0)
      integer*4 Tn8RelP(1,0),Tn8RelE(2,0),Tn8CcSetInd(1)
      real*8 Tn8RoWt(1),Tn8RoGa(1),Tn8GSumWt1(1),Tn8GSumWt2(1),
     *       Tn8Lev(1),Tn8RoWtk(1),Tn8PpWt(1),Tn8MasBor(1),Tn8ccBor(1),
     *       Tn8VolWt(1),Tn8XxWt(1),Tn8XxBalWt(1),
     *       Tn8MasWt(1),Tn8MasWtk(1),
     *       Tn8RoGak(1),Tn8Gsliv(1),Tn8EeGa(1),
     *       Tn8EeWt(1),Tn8PpGa(1),Tn8VsWt(1),Tn8VsGa(1),Tn8VsVp(1),
     *       Tn8EeWtSat(1),Tn8EeVpSat(1),Tn8LevRel(1),Tn8Hi(1),
     *       Tn8TtGa(1),Tn8TtWt(1),Tn8GgXx(1),
     *       Tn8dRdP1(1),Tn8dRdH1(1),Tn8dRdP2(1),Tn8dRdH2(1),
     *       Tn8Waste_T(20,1),Tn8RowtSat(1),Tn8RovpSat(1),
     *       Tn8QqEnv(1),Tn8RoomInd(1),Tn8TtMeDo(1)
     *       ,Tn8EvDo(1),Tn8EvDo0(1),Tn8GInOut(1),Tn8LevTunG(1)

      logical*1 Tn8CcBorLb(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 981 

C     common/gr_parC     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1018 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'

      integer n09
      parameter(n09=0)
      integer*4 Tn9RelP(2,1),Tn9RelE(Tn9Ne,1),Tn9CcSetInd(1)
      real*8 Tn9RoWt(1),Tn9Cp(1,1),Tn9Lev(1),
     *       Tn9Ek(1,1),Tn9Rok(1,Tn9Ne),Tn9PpWt(1),Tn9Pk(1,1),Tn9Rot(1,
     >Tn9Ne),
     *       Tn9Xxk(1,Tn9Ne),Tn9MasBor(1),Tn9ccBor(1),Tn9VolWt(1),
     *       Tn9dp(1,Tn9Ne),Tn9MasWt(1),Tn9GIspSum(1),Tn9Pp(1),
     *       Tn9GCondSum(1),Tn9G32sum(1),Tn9G34sum(1),Tn9G23sum(1),
     *       Tn9G43sum(1),Tn9TettaQ(1,Tn9Ne),Tn9Ev_t(1,1),Tn9Dim_t(1),
     *       Tn9Dim_tmax/1/,Tn9EvDo(1),Tn9EvDo0(1),Tn9FlwDdMin(1),
     >Tn9MasVpUp(1),
     *       Tn9MasVpDo(1),Tn9MasGa(1),Tn9LevMas(1),Tn9LevMask(1),
     >Tn9PpGa(1),
     *       Tn9PpVpUp(1),Tn9EeGa(1),Tn9EeVpUp(1),Tn9EeWt(1),
     *       Tn9EeVpDo(1),Tn9LevRelax(1),Tn9VsGa(1),Tn9VsVpUp(1),
     *       Tn9VsWt(1),Tn9VsVpDo(1),Tn9EeWtSatWt(1),Tn9EeVpSatWt(1),
     *       Tn9RoGa(1),Tn9ROVpUp(1),Tn9RoVpDo(1),Tn9LevRel(1),Tn9Hi(1),
     *       Tn9Volk(1,Tn9Ne),  ! ������ ��� �� ��������
     *       Tn9VsWtUp(1),Tn9VsWtJa(1),Tn9Ee(1,1),Tn9TtGa(1),
     *       Tn9TtVpUp(1),Tn9TtWt(1),Tn9TtVpDo(1),
     *       Tn9Mask(1,1),Tn9CcGas(1,1),
     *       Tn9CfMasGasMin(1)/7.5d-7/,
     *       Tn9CcUp(1,1),Tn9FlwHUp(1,1),Tn9FlwHDo(1,1),Tn9GSepSum(1),
     *       Tn9GFlwWtSum(1),Tn9MasGaRealk(1),Tn9dEInt(1,1),
     *       Tn9GCorr(1,1),Tn9drdp(1,1),Tn9drdh(1,1),Tn9vk(1,1),
     *       Tn9Waste_T(20,1),Tn9Dmint(1),Tn9MasWtUp(1),Tn9MasWtJa(1),
     >Tn9RoDo(1),
     *       Tn9WlUpQqOut(1),Tn9WlDoQqOut(1),Tn9TcwAlCulc(1,1),
     *       Tn9TtWlUp(1),Tn9TtWlDo(1),Tn9LaWl(1),
     *       Tn9QqWlEnvUp(1),Tn9QqWlEnvDo(1),Tn9RoomUpInd(1),
     >Tn9RoomDoInd(1),
     *       Tn9CcVpDo(1),Tn9pmax(1),Tn9TtUp(1),Tn9AlSqPhWl(6,1),
     >Tn9AlWlUpDo(1),
     *       Tn9TtSatWt_(1),Tn9Levadd(1),Tn9vol(1),Tn9VolCorrUp(1),
     >Tn9VolCorrDo(1),
     *       Tn9PressCorr(1),Tn9VolGa(1),Tn9FlwIntCoef(1),
     >Tn9VolVpUpLim(1),
     *       Tn9LevTunG(1),Tn9LevTunPpRegG(6,1)

      integer*4 Tn9FlwNum(1)
      logical*1 Tn9CcBorLb(1),Tn9VpInFlag(1),Tn9BorFl(1),
     >Tn9EntOut_Key(1),
     *          Tn9FlagGCorr(1),Tn9FlagGCorrDn(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1061 

C     common/gr_parC     common/gr_parC     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1205 


****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
      integer*4 Ntur1,Tr1Lin(1),tr2RLinTyp(1)
      parameter(ntur1=0)
      real*8    tr2FlwKsi1(1)
      real*8 Tr1Oin(1),Tr1Qsd(1),Tr1NdP(1),Tr1genTyp(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1217 
      integer*4 Ntur3
      parameter(Ntur3=0)
         integer*4 Tr3beaTyp(1),Tr3beaInd(1)
      real*8    Tr3Omdl(1),Tr3Qsd(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1223 
      integer*4 Ntur4
      parameter(Ntur4=0)
         integer*4 Tr4beaTyp(1),Tr4beaInd(1)
      real*8    Tr4Omdl(1),Tr4Qsd(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1229 



****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'

      real*8 C1serv(nwall) !����. ��� ������� ������. ������� ���
      real*8 C2serv(nwall) !����������� ���� � ���. ��. ����� ������
      real*8 wl1_QqOut(nwall)  !��������� �������� ����� � ������� ��������
      real*8 wl1AlCulcWl(nwall)   !��������� �������� ����� � ������� ��������
      real*8 wl1_Tt_t(nwall)
      real*8 wl1_XQ(nwall),wl1_BQ(nwall)  ! �-�� � ������� ���������
      common/gr_parWALL1/ C1serv,C2serv,wl1_QqOut,wl1AlCulcWl,
     *  wl1_Tt_t,wl1_XQ,wl1_BQ
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1249 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1263 

      integer*4 NRm1
      parameter ( NRm1=0 )
      real*8 Rm1Qq(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1269 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1289 

      integer*4 Nwall2
      parameter ( Nwall2=0 )
      real*8 wl2_QqOut(1)  !��������� �������� ����� � ������� ��������
      real*8 Wl2Tt_t(1)
      real*8 WL2mass(1),WL2Cp(1),WL2Tt(1)
      real*8 Wl2LamdC(1)
      integer*4 Wl2IndRoom(1),Wl2Ndet(1),Wl2Node(1)
      real*8 Wl2QqEnv(1),Wl2AlCulcWl2(1)
      real*8 Wl2Leff(1)
      real*8 Wl2Rb(1)
      real*8 Wl2Raw(1), Wl2SqEnv(1), Wl2QqBout(1), Wl2DOut(1)
      real*8 Wl2_XQ(1),Wl2_BQ(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1304 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      integer*4 Nwlcon2
      parameter ( Nwlcon2=0 )
      integer*4 WLC2Iwl(1)
      real*8 WLC2_Qq(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1313 




****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

!!      external gr_par_it,gr_par_end
!!      call spw_IterReg(gr_par_it,gr_par_end)
c
      ErrorNum(1)=-1
c
      deltat1=deltat

      if(StpFlag(1).eq.0) then
       StpFlag(1)=1
      else
       StpFlag(1)=0
      end if

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1337 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1343 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1348 

c --- �������� ������� ���������� ��� ����� � ��������
      time_tmp = time_df1()
      call gr_parconnIn
      tick(2,1) = time_df1()-time_tmp
      ErrorNum(1)=2
c
      do i=1,nu
        if( VOLUKZ(i).gt.VolNodMax(1)) VOLUKZ(i)=VolNodMax(1)
      enddo
      do i=1,nv
        if( FlwLnSq(i).gt.FlwLnSqMax(1)) FlwLnSq(i)=FlwLnSqMax(1)
      enddo
c
c uvk ��������� � net42
      call SetCfPp(Nodcfppup_,Tn3CfPp,Tn7CfPp,Tn8CfPp,Tn9CfPp,NodCfPp
     &            ,NodcfppLoc)
c     ��������� ��������� ������ ���������� ���������
      time_tmp = time_df1()
      call start_switch(flag_first(1),flag_start(1),accel(1),dtint,atau,
     &                 accelnew(1),maxiter(1),maxiternew(1),deltat,
     &                 nodcfppup(1), nodcfppup_(1))

      tick(3,1) = time_df1()-time_tmp
      ErrorNum(1)=3
C     ��������� ��������� ������ ���������� ���������

      TimeSrv=dtInt/MaxIternew(1)
      !���� ��������� ����� ��� �������� ������ ��� ���������
      if((Event(1).eq.3).or.(Event(1).eq.5)) then

c   ������ ���������� �������
        call PhConst             !����������� ���������� ����������
c        call spw_getfile(' ','input','airph_cbf',fn1)
c        call airfph(fn1)       !�������� ������ ��� ���������
                               !������� �������
c        call spw_getfile(' ','input','helph_cbf',fn1)
c        call helfph(fn1)       !�������� ������ ��� ���������
                               !������� �����
c        call spw_getfile(' ','input','nitrph_cbf',fn1)
c        call nitrfph(fn1)       !�������� ������ ��� ���������
                               !������� �����
c        call spw_getfile(' ','input','oxiph_cbf',fn1)
c        call oxifph(fn1)       !�������� ������ ��� ���������
                               !������� ���������     
      endif

      time_tmp = time_df1()
      call HydroCon_In(nv,NHC1,HC1IndGg,FlwType,Gz,
     *                 HC1Lb,HC1LbOld,HC1Type,FlwKeyVp,FlwWt,
     *                 ResCf1,FlwResCf,FlwRedGa,FlwLl,FlwDd,
     *                 FlwADd,FlwSq,FlwASq2,GzIni,ROVZ,
     *                 FlwResTyp,FlwPipeType,Kv1,Kv,FlwRes0,
     *                 FlwRes,FlwCon,FlwRg,FlwVs,FlwSqEnv,
     *                 FlwCfKv,FlwCfCorKv,FlwLnSq,FlwResDel,
     *                 ConFlOutNames,
     *                 FlwDdExt,FlwThi,FlwSqE,HFrom,HTo,
     *                 FlwPipeType,FlwKForm,FlwResFric,FlwResAcc,
     *                 FlwResLoc,FlwFlowType,FlwResl,FlwBndLam,
     *                 FlwBndTur,FlwVlvFlIni,FlwResTur,FlwResLam,
     *                 FlwKsi1,FlwKsi2,FlwRo0,FlwCfResl,FlwSq2,
     *                 HC1Gg,HC1GgOld,HC1LbAlone,HC1FlagInOut,
     *                 FlwCfTime,IFROKZ,ITONKZ,
     *                 HC1HFrom,HC1HTo,
     *                 HC1FlwCfResDel,
     *                 FlagCon(1)*DpMaxFlag(1),HC1CfTime,777)
      tick(4,1) = time_df1()-time_tmp
      ErrorNum(1)=4
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1418 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1452 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1486 
      time_tmp = time_df1()
      call clr_Y(Nent,Ye)  !��������� ������ ������ ��� ���������
      tick(5,1) = time_df1()-time_tmp
      ErrorNum(1)=5
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1497 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1500 
c   ��������������� ������� ��� ������� �����
      time_tmp = time_df1()
       call Node_pre(nu,VOLUKZ0,Atau,Vol1,
     &      NodGaType,SochiGasIn1,SochiGasIn2,SochiCfCpGas,
     &                    SochiGasIn10,
     &                    SochiGasIn20,SochiCfCpGas0,
     &                    PropsKey )

      tick(6,1) = time_df1()-time_tmp
      ErrorNum(1)=6
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1512 

c --- ���������� ��������� ������ � ������ �������� ---

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1519 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1554 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1588 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1597 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1603 
c
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1674 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1744 

****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1752 


****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'

      time_tmp = time_df1()
c      call Nod_Typ_Tnk(nv,nu,n03,n07,n08,n09,
c     *                 LinIBNfro,LinIBNto,IFROKZ,ITONKz,
c     *                 HFrom,HTo,
c     *                 Tn3Lev,Tn7Lev,Tn8Lev,Tn9Lev,
c     *                 NodTypTnk)
      call Flw_ForwEx(nv,Gk,GZ,Rovk,Rovz,FlwVsk,FlwVs,FlwRo,FlwRoFrc,
     >FlwRoFrom,FlwRoTo
     *,nfwv,FWVFLOWW,FWVFLOWV,FwvRoFrom1,FwvRoFrom2,FwvRoTo1,FwvRoTo2)
      tick(153,1) = time_df1()-time_tmp
      ErrorNum(1)=153
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1769 


****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1785 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1793 


****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'



      time_tmp = time_df1()

      call Node_ForwEx(nu,Npres,Nent,Pk,NodPp,Ek,NodEe,Rok,
     *                   Rozt,NodRo,EeWtSatk,NodEeWtSat,EeVpSatk,
     *                   NodEeVpSat,NodTtk,NodTt,NodCp,NodCp,
     *                   NoddRodPk,NoddRodPk1,NoddRodP,NoddRodEk,
     *                   NoddRodEk1,NoddRodE,nodEeGa,nodEeGat,
     *                   nodccgat,nodvvgat,nodccga,nodvvga,
     *                   VOLUKZ0,VOLUKZt,NodXxk1,
     *                   NodXx,NodXxBalk1,NodXxBal,
     *                   nodrotrue,nodrotrue0,NodFreez,
     *                   PExtIter,PExtIter_,invperP,EExtIter,EExtIter_,
     *                   invperE,NodTyp,
     *                   NodRoGa,NodRoGa0,
     *                   NodCgEe0,NodCgEe,NodRoomFl,NodTtenv,NodPpenv
     &                      ,LimTmax,LimTmin,dPMaxFlag
     *                      ,NodRoTot,NodRoTot0,
     *                       LimdMWmax,LimdMGmax,Masdt_Wv,Masdt_Ga,
     *                       NWall,Wl1Tt,LimTWmax,LimTWmin,
     *                       NodMasWvMax,NodMasGaMax,
     *                       LimddMWmax,LimddMGmax
     *           ,LimPmax,NODFREEZPH
     *           ,NodCfdPdt_main,NodCfdPdt_loc,NodCfdPdt_key,NodCfdPdt
     *           ,NodCfdPdt_loc1,NodEeWv,NodEeWvt,EGk,NodNumEe(1)
     *           ,NodEeWv_tmp,NodTe1Key,NumFlStop(1),NumFlagStop(1),
     *       OutLimPNodNum(1),OutLimTupNodNum(1),OutLimTdoNodNum(1),
     >volukz)

c      call Node_Vs(nu,NodPp,NodTt,NodVsVp,NodVsWt,
c     *             NodVsFr,NodXx,NodBodyTyp)
      tick(162,1) = time_df1()-time_tmp
      ErrorNum(1)=162


C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1839 

****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1861 

****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'

      time_tmp = time_df1()
      call Tc2_ForwEx(NTc2,Tc2TtWl,Tc2TtWlk
     *,Tc2MasCpWl,Tc2MasWl,Tc2CpWl
     *,Tc2ResWl,Tc2LlWl,Tc2LaWl,Tc2ResWl0
     *,Tc2AlEnv_P,Tc2AlEnv,Tc2QqEnv,Tc2QqEnv1,Tc2QqRelax
     *,Tc2SqWl,Tc2SqWl0,Tc2Sq_T,Tc2Sq_T0,Tc2Dim_T,Tc2Dim_Tmax
     *,Tc2SqWlNorm)
      tick(181,1) = time_df1()-time_tmp
      ErrorNum(1)=181
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1878 

****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1899 

****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

        if (TnkNormFlag(1)) then
          TnkNormFlag=.false.
        call Tn3_norm(n03,TnkNormFlag,Tn3VolGa,Tn3VolWpUp,Tn3VolWt
     *    ,Tn3VolWpDo,Tn3MasGa,Tn3MasWpUp,Tn3MasWt,Tn3MasWpDo,Tn3Ne,
     >Tn3Vol
     *    ,Tn3VolCorrUp,Tn3VolCorrDo,Tn3VolFull)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1912 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1917 
        endif

      time_tmp = time_df1()
      call Tn3_ForwEx(n03
     *,Tn3PpGa,Tn3PpWpUp,Tn3PpWt,Tn3PpWpDo,Tn3Pp
     *,Tn3EeGa,Tn3EeWpUp,Tn3EeWt,Tn3EeWpDo,Tn3Ee
     *,Tn3TtGa,Tn3TtWpUp,Tn3TtWt,Tn3TtWpDo,Tn3Tt
     *,Tn3TtWlUp,Tn3TtWlDo,Tn3TtWl
     *,Tn3TtEnvUp,Tn3TtEnvDo,Tn3TtEnv
     *,Tn3MasGa,Tn3MasWpUp,Tn3MasWt,Tn3MasWpDo,Tn3Mas
     *,Tn3RoGa,Tn3RoWpUp,Tn3RoWt,Tn3RoWpDo,Tn3Rot
     *,Tn3VolGa,Tn3VolWpUp,Tn3VolWt,Tn3VolWpDo,Tn3Volt
     *,Tn3Pk,Tn3TtWlk,Tn3Ek,Tn3Mask
     *,Tn3RelP,Tn3RelE,Pk,Ek,Npres,Nent,Tn3Volk
     *,Tn3EeWtSatWt,Tn3EeWpSatWt,EeWtSatk,EeVpSatk,Tn3Xxk
     *,Tn3AlPhPh,Tn3AlGaWpUp,Tn3AlGaWt
     *,Tn3AlGaWpDo,Tn3AlWpUpWt,Tn3AlWpUpWpDo
     *,Tn3AlWtWpDo,Tn3Ne,Tn3MasGaMin,Tn3CfMasGasMin
     *,Tn3MasGaReal,Tn3MasGaRealk
     *,Tn3QqPhPh,Tn3QqWtMi,Tn3QqVpUpMi,Tn3VolNorm)
      tick(202,1) = time_df1()-time_tmp
      ErrorNum(1)=202

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1942 



****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 1994 

****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 2017 


****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 2048 


****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 2060 




****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'

        call Wl1_ForwEx(nwall,wl1_QqOut,wl1_Tt_t,Wl1Tt
     &            ,WL1MatTyp,Wl1MatDepTyp
     &            ,Wl1Lamd,Wl1Cp,Wl1LamdC,Wl1CpC)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 2074 


****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 2088 


****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 2100 





****** End   section 'ForwEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

      FlwCfSum=FlwCfResL !�-� �������� �/�
      time_tmp = time_df1()
      call clr_FlwKge(FlwKge,nf,nv,LinIbnFro,LinIbnTo)
      call Flw_Tn_K(nv,LinIBNfro,LinIBNto,Gzz,Gk,
     *               FlwCfSum,FlwCfTnk,CfTnkFlw(1))  !!!!!!!!!!!!!!!!!!!!!!
      tick(7,1) = time_df1()-time_tmp
      ErrorNum(1)=7
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 2116 

****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 2327 


****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Tn9_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

* ��������� � "����� �������"
      time_tmp = time_df1()
      call Tn3_Prop(Tn3Pk,Tn3Ek,Tn3Tt,Tn3Rok,Tn3drdp,Tn3drdh,n03
     *,Tn3Mask,Tn3hw,Tn3hs,Tn3cp,Tn3Ts
     *,EeWtSatk,EeVpSatk,Tn3RelE,Nent,Tn3Pk2,Tn3hw2,Tn3hs2,Tn3ts2
     *,Tn3Tt2,Tn3Vk,Tn3dTdH,Tn3RoWtSatk,Tn3Ne,Tn3CcUp2,Tn3TtFr,Tn3La
     *,Tn3HD,Tn3ro2,Tn3cp2,Tn3hd2
     *,Tn3rot,Tn3LevMas,Tn3Levadd,Tn3ts_,Tn3Ts2_,Tn3Pk_dev)
      tick(203,1) = time_df1()-time_tmp
      ErrorNum(1)=203

* ������, ������, ����� �������� ������ � "����� �������" �� ��������
*          (��� ������� � ������)
      time_tmp = time_df1()
      call Tn3_Vol(Tn3rok,n03,Tn3LevRelk,Tn3LevMask,Tn3volt,Tn3Vol
     *,Tn3Mask,Tn3RoDo,Tn3Ev_T,Tn3Vol_T,Tn3Dim_TMax,Tn3Dim_T,Tn3RelE
     *,Tn3EvDo,Tn3Lev,Tn3MasWl,Tn3MasCpWl,Tn3CpWl,Tn3TettaQ
     *,Tn3Vk,Tn3CcUp,Tn3AVolDo,Tn3VolCorrUp,Tn3VolCorrDo,Tn3CcWpDo
     *,Tn3TettaWl,Tn3EvSq_T,Tn3Sq_T,Tn3Sq,Tn3Ts,Tn3SqPhPh,Tn3Ne
     *,Tn3La,Tn3LaFr,Tn3LaCf,Tn3Cf42,Tn3CfGgWpDoWpUp,Tn3G42TypCalc
     *,Tn3Vel42,Tn3Dd4,Tn3Sigma,Tn3VolDoVH,Tn3ficr,Tn3CfGgWpDo_tmp,
     >Tn3CfMasWl,Tn3VolNorm0
     *,Tn3Levadd,Tn3Ts_,Tn3MassInterpKey,Tn3Vlim1,Tn3Vlim2,Tn3VolFull)
      tick(204,1) = time_df1()-time_tmp
      ErrorNum(1)=204
      time_tmp = time_df1()
       call Tn3Tc2_Lev(n03,Tn3Lev,NTc2,Tc2Ibn,Tc2Node
     *,Tc2EvUpWl,Tc2EvDoWl,Tc2TettaQ,Tn3Volk,Tn3CcUp,Tn3AVolDo,Tn3Ne,nf
     *,Tc2Form,Tc2Ev_T,Tc2Sq_T,Tc2Dim_Tmax,Tc2Dim_T,Tc2SqWl,Tn3LevRelk,
     >Tc2CfVolWt,Tc2CfCcWpUp)
      tick(205,1) = time_df1()-time_tmp
      ErrorNum(1)=205

* ������ �-��� ������������� ��� �������
      time_tmp = time_df1()
      call Tn3_tp(n03,Tn3AlSqPhWl,Tn3AlSqPhPh,Tn3AlWlUpDo
     *,Tn3AlSqWlEnv,Tn3MasCpWl,Tn3ActWlUpDo
     *,Tn3CfAlGaWl,Tn3CfAlWpUpWl,Tn3CfAlWtWl,Tn3CfAlWpDoWl
     *,Tn3CfAlGaWpUp,Tn3CfAlGaWt,Tn3CfAlGaWpDo,Tn3CfAlWpUpWpDo
     *,Tn3CfAlWlUpEnv,Tn3CfAlWlDoEnv,Tn3CfAlWlUpDo
     *,Tn3TettaQ,Tn3SqWl
     *,Tn3Rok,Tn3AlPhPh,Tn3SqPhPh,Tn3Ne,Tn3CfAlEnvGr,Tn3AlWlEnv
     *,Tn3volt,Tn3vol
     *,Tn3AlGaWl,Tn3AlWpUpWl,Tn3AlWtWl,Tn3AlWpDoWl)
      tick(208,1) = time_df1()-time_tmp
      ErrorNum(1)=208
      time_tmp = time_df1()
      call Tn3Tc2_tp(NTc2,Tc2Ibn,Tc2Node
     *,Tc2AlSqEnv,Tc2AlSqPhWl,Tc2CfAlGaWl,Tc2CfAlWpUpWl
     *,Tc2CfAlWtWl,Tc2CfAlWpDoWl
     *,Tc2TettaQ,Tc2SqWl,Tc2AlPhWl,Tc2AlEnvMod
     *,Tc2ResWl,nf,Tc2LlWl,Tc2LaWl,Tc2ResWl0)
      tick(209,1) = time_df1()-time_tmp
      ErrorNum(1)=209

* ������� ��������� � ����������� � "����� �������"
      time_tmp = time_df1()
      call Tn3_isp_cond(Tn3Pk,Tn3Ek,Tn3Tt,n03,Tn3G,Tn3Mask
     *,Tn3hw,Tn3hs,Tn3dGdP,Tn3TtWlk,Tn3AlGw23,Tn3Gw23,Tn3dGw23dP
     *,Tn3AlGw34,Tn3Gw34,Tn3dGw34dP,Tn3AlGw43,Tn3Gw43,Tn3dGw43dP
     *,Tn3CfGgWpUpWt,Tn3CfGgWtWpUp,Tn3CfGgWpDoWt,Tn3CfGgWtWpDo
     *,Tn3CfGgWpDoWpUp,Tn3CfGgWlWpUpWt,Tn3CfGgWlWtWpDo,Tn3CfGgWlWpDoWt
     *,Tn3Ts,Tn3Pk2,Tn3hw2,Tn3hs2,Tn3ts2,Tn3Tt2,Tn3Gw32,Tn3DGw32dP
     *,Tc2AlG23,Tc2G23,Tc2dG23dp,Tc2G34,Tc2dG34dp,Tc2AlG43,Tc2G43
     *,Tc2dG43dp,Tc2CfGgWpUpWt,Tc2CfGgWt,Tc2CfGgWpDoWt,NTc2,Tc2TtWlk
     *,Tc2Ibn,Tc2Node,Tc2G32,Tc2dG32dp,Tc2CcGgWtWpDo,Tn3Volk,Tn3Vol
     *,Tn3AlSqPhWl,Tc2AlSqPhWl,Tc2SqWl,Tc2TettaQ,Tc2QqIsp,Tc2AlIsp,
     >Tc2DeltaTs
     *,Tn3AlGv23,Tn3CfGgGaWpUpWt,Tn3Gv23,Tn3dGv23dP,Tn3SqWl
     *,Tc0AlG23,Tc0G23,Tc0dG23dp,Tc0G34,Tc0dG34dp,Tc0AlG43,Tc0G43
     *,Tc0dG43dp,Tc0CfGgWpUpWt,Tc0CfGgWt,Tc0CfGgWpDoWt,NTc0,Tc0TtWlk
     *,Tc0IbnTo,Tc0NodTo,Tc0G32,Tc0dG32dp,Tc0CcGgWtWpDo
     *,Tc0AlSqPhWl,Tc0SqWl,Tc0TettaQ,Tc0QqIsp,Tc0AlIsp,Tc0DeltaTs
     *,TenAlG23,TenG23,TendG23dp,TenG34,TendG34dp,TenAlG43,TenG43
     *,TendG43dp,TenCfGgWpUpWt,TenCfGgWt,TenCfGgWpDoWt,NTen,TenTtWlk
     *,TenIbn,TenNode,TenG32,TendG32dp,TenCcGgWtWpDo
     *,TenAlSqPhWl,TenSqWl,TenTettaQ,TenQqIsp,TenAlIsp
     *,Tn3AlSqPhPh,Tn3QqPhPh,Tn3QqGen,Tn3SqPhPh,Tn3dg32dp2,Tn3dg32dp3
     *,Tn3Ne,dtInt,Tn3TettaQ,Tc2dQIspdP,Tc2TtTnIsp,Tc2AlSqIsp
     *,Tc0dQIspdP,Tc0TtTnIsp,Tc0AlSqIsp
     *,TendQIspdP,TenTtTnIsp,TenAlSqIsp,Tc2ResWl,Tc0ResWl,Tc0dQG23dp
     *,Tc0TettaQ2,Tn3CfVolVpUp,TenResWl,Tn3CfGgWtWpUp2
     *,nf,Tc0IbnFro,Tc0NodFro,Tc0CcGgWtWpDoFrom,Tc0AlG23From,
     >Tc0CfGgWpUpWtFrom
     *,Tc0AlSqPhWlFrom,Tc0G23From,Tc0G32From,Tc0G43From,Tc0G34From
     *,Tc0dG23dPFrom,Tc0AlG43From,Tc0CfGgWpDoWtFrom
     *,Tc0dG43dPFrom,Tc0TtTnIspFrom,Tc0QqIspFrom,Tc0AlIspFrom
     *,Tc0CfGgWtFrom,Tc0AlSqIspFrom,Tc0dG34dpFrom,Tc0dG32dpFrom
     *,Tc0dQIspdPFrom,Tc0TettaQFrom,Tn3Qq23CfRel
     *,Tn3Cf42
     *,Tn3WlTtIsp,Tn3WlQqIsp,Tn3WlAlIsp,Tn3WlAlSqIsp
     *,Tn3G42TypCalc,Tn3Al34TypCalc
     *,Tn3CcWpDo,Tn3Rok,Tn3La,Tn3HD,Tn3Cp,Tn3Sigma,Tn3VsWt,Tn3Vel42
     *,Tn3Dd4,Tn3ro2,Tn3cp2,Tn3hd2,Tn3Sq34,Tn3CfSq34,Tn3CfSqWl34
     *,Tn3CfGgWpDo_tmp,FirstStepLabel,Tc2CfRelAlIsp,Tc0CfRelAlIsp
     *,Tn3TtWtMi,Tn3dTWtMi
     *,Tn3Volt,Tn3Ts_,Tn3Ts2_,Tn3Hi,Tn3LevMas,Tn3VolVpUpLim,Tn3Pk_dev,
     >Tn3Qq34dec)
      tick(212,1) = time_df1()-time_tmp
      ErrorNum(1)=212

*    ������������ ��������� ������� ��� "����� �������"
      time_tmp = time_df1()
         call Tn39_Srv1(n03,Tn3Volt,Tn3Voldt,Atau,Tn3Ne)
      tick(213,1) = time_df1()-time_tmp
      ErrorNum(1)=213

* ������� ������������ ��� �������� �/�� "����� �������"
      Tn3RelaxKG=.false.
      time_tmp = time_df1()
      call Tn3_kG(Gz,Tn3Lev,FlwKgp,FlwKgg,FlwKge
     *,Tn3Mask,nv,ifrokz
     *,itonkz,n03,Tn3CcGas,LinIbnFro,LinIbnTo,nu
     *,NodBodyTyp,Tn3Pk,Tn3Rok
     *,Tn3LevMask,dtint,Tn3CcUp,FlwXxFrom,FlwXxTo,nf
     *,FlwHTnFromDo,FlwHTnFromUp,FlwHTnToDo,FlwHTnToUp
     *,Tn3LevRelax,Tn3EvDo
     *,Tn3Ne,n09,Tn9LevRelax
     *,Tn3Volk,FlwType
     *,ComGamFrom,ComGamTo,FlwRoFrom,FlwRoTo
     *,NodXx,Tn8XxWt,Tn7Xk,n08,n07
     *,FlwGaSlFrom,FlwGaSlTo,Tn3MasGaRealk
     *,Tn3drdp,FlwdRodPFrom,FlwdRodPTo,Tn3Vk,Tn3drdh,FlwCfAdFrom,
     >FlwCfAdTo
     *,FlwRoFromSt,FlwRoToSt,FlwCcGgWtJa
     *,FlwdRodPSndFrom,FlwdRodPSndTo
     *,FlwVpInFlagFrom,FlwVpInFlagTo
     *,FlwFromSoc,FlwToSoc,FlwSp)

      Tn3RelaxKG=.true.
      tick(214,1) = time_df1()-time_tmp
      ErrorNum(1)=214

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 2561 


****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Tn3_PreIt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 2641 

      time_tmp = time_df1()
      call HydroCon_Dens(nv,NHC1,HC1Type,HC1Lb,Rovz,HC1Rov,
     *                    HC1IndGg,HC1LbAlone)
      tick(10,1) = time_df1()-time_tmp
      ErrorNum(1)=10
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 2649 
c   ������ ������� ��������� � ����. ������ �������� �� ������
      time_tmp = time_df1()
        call Flw_Xx_v1(nv,n03,Nf,IFROKZ,ITONKZ,LinIBNfro,
     *                 LinIBNto,NodXxBal,Tn8XxBalWt,
     *                 Tn3Xxk,
     *                 FlwXxBalFrom,FlwXxBalTo,FlwXxFrom,FlwXxTo,
     *                 Tn7XxWt,n09,Tn9Xxk,ComGamFrom,
     *                 ComGamTo,nu,n07,n08,
     *                 NodXb_prev,NodXb_next,Nodxb_filter,Gz)
      tick(11,1) = time_df1()-time_tmp
      ErrorNum(1)=11
      time_tmp = time_df1()
        call Density_new(nv,nu,
     *               n07,n03,n08,n09,
     *               LinIBNfro,LinIBNto,FlwRoFrom,
     *               IFROKZ,ITONKZ,
     *               FlwRoTo,
     *               ROVz,Rovz,
     *               FlwLl,FlwSq,GZ,FlwRo,
     *               dtInt,FlwRoFrc,Hfrom,Hto,
     *               AconRovk,Tn7lev,Tn8lev,Tn3Lev,Tn9Lev,NodTypTnk,
     >FlwResTyp,
     *               FlwHTnToDo,FlwHTnToUp,
     *               FlwHTnFromDo,FlwHTnFromUp,
     *               FlwVsFrom,FlwVsTo,FlwVs,FlwVs,
     *               FlwRoFromSt,
     *               FlwRoToSt,NodLaFr,FlwLaFrc,FlwRoFlag,FlwCfdh,
     *               NodGgSumZero,NodGgSumZeroin,NodLaFr_cor(1),
     *                 FlwDensCfL,FlwRoC)
     
      tick(12,1) = time_df1()-time_tmp
      ErrorNum(1)=12
      time_tmp = time_df1()
        call Volume(nv,LinIBNFro,LinIBNTo,IFROKZ,ITONKZ,
     *              HFrom,HTo,
     *              Tn8Lev,Tn3LevRel,Tn9LevRel,
     *              Tn8RoWt,Tn3RoWt,Tn9RoWt,dHLev,
     *              FlwdPStat,
     *              FlwRoFrc,
     *              InternalIter,Tn3EvDo,
     *              Tn7Levk,Tn7RoWt,Tn9EvDo,
     *              Tn8RoGa,Tn3RoGa,Tn9RoGa,
     *              Tn7RoGa,Gz,aconRovk,
     *              FlwdPLevFrom,FlwdPLevTo,
     *              Flwdd,FlwAngFrom,FlwAngTo,Tn3RoDo,Tn9RoDo
     *                 ,FlwHTnFromUp,FlwHTnFromDo,FlwHTnToUp,FlwHTnToDo
     *                 ,FlwRoFrom,FlwRoTo,n08,n03,n07,n09,FlwKkdh,
     >FlwFlagm2,
     *              NodCcGa,NodRoWtSat,NodRoGa,Tn3RoWpUp,Tn9RoVpUp)
      tick(13,1) = time_df1()-time_tmp
      ErrorNum(1)=13
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 2700 
c---------------------------------------------------------------------
c   ������ ��������� �������� � ����
c   ���� ������ �������
      time_tmp = time_df1()
      call Clr_Y(nv,FlwdPdGPm)
c      call Clr_Y(nv,FlwdPPm)
      FlwdPPm=FlwdPdop
      tick(14,1) = time_df1()-time_tmp
      ErrorNum(1)=14
      time_tmp = time_df1()
      call Clr_Y(nu,NodQqPm)
      tick(15,1) = time_df1()-time_tmp
      ErrorNum(1)=15
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 2908 

****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 2974 

****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'TurPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'




c---------------------------------------------------------------
c     ���������� ������������� ������
*              MIKE2
      FlwSqMin=FlwSq
      call Clr_Y(nv,FlwRes)
      call Clr_Y(nv,FlwSumRes)
* ������ �/� ������
c      call FlwSub_Res(nv,Gz,FlwRoFrc,FlwLl,
      time_tmp = time_df1()
      call FlwSub_Res(nv,Gz,Rovk,FlwLl,
     &             FlwRg,FlwSq,FlwDd,FlwCon,
     &             FlwRes,FlwRes0,FlwResTyp,FlwResDel,
     &             FlwVs,FlwRoFrom,FlwRoTo,
     &             np2,Pm2Nb,Pm2Res,     
     &             np4,Pm4Nb,Pm4Res,     
     &             FlwSumRes,FlwResCf,FlwKeyVp,
     &             FlwXxBalFrom,FlwXxBalTo,
     &             nf,FlwRedGa,FlwADd,FlwASq2,
     &             Rov0,LbResRo,FlwType
     &           ,FlwSqMin
     *           ,ComGamFrom,ComGamTo
     &           )
      tick(24,1) = time_df1()-time_tmp
      ErrorNum(1)=24
c
* �������� Kv
      time_tmp = time_df1()
      call Flwsub_Kv(nv,Kv1,Kv,FlwResCf)
      tick(26,1) = time_df1()-time_tmp
      ErrorNum(1)=26
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3035 

* ��������� ��������� �������������
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3052 

* ��������� ���������
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3070 

      nvlgpos=maxval(VlgNT)
      call VlgSub_Res(nv,nvlg,VlgFlw,Gz,VlgGTCh,
     &            Rovk,FlwSq,Flwres,Flwcon,FlwSumRes,
     &            VlgPos,VlgRes,VlgResMax,VlgCon,VlgSm,
     &            VlgKk,VlgType,nvlgpos,VlgNT,VlgSm_T,VlgSs_T,
     &            VlgFlag,VlgSmt,VlgKSmin,VlgNSmin,VlgVid,
     &            VlgSq0Cor,VlgReSmSs,
     &            VlgTypeCor,FlwVs,VlgVs0,VlgPowCor,VlgKvmax,
     &            VlgSmOld,FlwRe,FlwRg,FlwDd,VlgSSm,FlwSqMin,
     &          VlgMf5Lb,VlgMf5Cf,VlgCfRes,VlgSSNul,VlgMfkSs)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3083 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3091 

* ��������� �������
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3107 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3113 

c   ����������� ������������� �������� ��������
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3156 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3162 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3168 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3197 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3203 

c   ����������� ������������� ��������
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3230 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3235 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3241 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3246 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3255 

*!!! ��� ����������� ����� ��������

      time_tmp = time_df1()
      call Clr_Y(nv,FlwWt)
      tick(38,1) = time_df1()-time_tmp
      ErrorNum(1)=38
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3264 

c   ����������� ����� ��� ��������
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3276 

c   ����������� ����� ��� �������� ��������
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3287 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3298 



      time_tmp = time_df1()
      call clr_Y(nu,NodGgSum)
      tick(45,1) = time_df1()-time_tmp
      ErrorNum(1)=45
c      call clr_Y(nu,NodGgSumIn)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3308 

c ������������� ������� ���������� ����� ������� ��������
      Pk1=Pk
      Ek1=Ek
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

c ������ ������� ������������� ������������� ����� �����������
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
      time_tmp = time_df1()
c      call SetMusor
      call He2_alfaSq(Nv,Nu,NHe2,He2MatTyp,He2MatDepTyp
     &        ,He2Nod1Int,He2Nod2Int,He2FlowInt
     &        ,He2Nod1Tub,He2Nod2Tub,He2FlowTub
     &        ,He2CulcTyp,He2AreaInt,He2AreaTub,He2Cf,He2PAIR
     &        ,He2Lamd,He2Thick,He2AlfaInt,He2AlfaTub,He2AlfaResI
     &        ,He2AlfInt,He2AlfTub
     &        ,He2SqInt,He2SqTub,He2DdInt,He2DdTub,He2DdTubOut
     &        ,He2QQH,He2QQC,He2Tt
     &        ,Gz,NodPp,NodTt,NodVsWt,NodVsVp,NodTtSat,NodXxBal
     &        ,He2CulcTypOut,He2CulcTypIn,He2Out_G0,He2In_G0
     &        ,He2AlfCulcInt,He2AlfCulcTub
     &        ,NodBodyTyp,TypeBut
     -  ,NODEEWV,NodEeWtSat,NodEeVPSat,NODRO,NodRoVpSat
     -  ,NODROWTSAT,NodRoGa,NodcpGa,NodCgGa,VOLUKZ
     -  ,NODFLOWREGIME,ALFAFLAG(1),HE2ALFAWLM1KEY
     &          ,FlwVs,He2RebroHeigth,He2RebroStep 
     &          ,He2RebroThin,He2TubeStep1
     &          ,He2TubeStep2,He2NLine,He2RebroForm
     &          ,He2TubeOrder
     &          ,He2AlfaIMin,He2AlfaIMax,He2AlfaTMin,He2AlfaTMax
     &          ,He2AlfaI,He2AlfaT,NodLaFr,He2Tet
     &          ,He2CfI,He2RCfI,NuLimReb(1),He2Ttb1,He2Ttb2
     &          ,ATYPPCRIT(1),He2alprev,He2alprev2,NodVsGa
     &          ,He2RadOn,He2RadAlType
     &          ,He2RAlfaIR0,He2RAlfaIR,He2RAw,He2RLs,He2RAg
     &          ,EradCfcorr,WasteNoGas,NodWaste_T
     &          ,RadIndSA(1),Dashes(1)
     &          ,He2QWsufI,He2QWsufT,He2TSurfI,He2BundRstep
     *          ,NodGgSumZeroIn,WasteNoGl)

      tick(157,1) = time_df1()-time_tmp
      ErrorNum(1)=157
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3360 

****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_ALFA' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      IC=0
      go=.true.
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3432 
      end
****** End   file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET41.fs'
****** Begin file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Look.fs'
c --------------------------------------------------------------------
c ��������� ������������ ����'� ��
c 
c --------------------------------------------------------------------
c
c � ���� ������������ ������������ ��� �������, ������������� 
c     � GIW'� �� ��������� ����.
c
c ��������: �������� �. �.
c
c ����� ���������: ������ �.�.
c
c �����������: ����� ���
c
c ---------------------------------------------------------------------
c
c
c     $LOOK:Look
      subroutine gr_parLook(arg1,arg2)
      
      implicit none

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3461 
      integer FirstStepErrFlag
      common/gr_parErrorFlags/ FirstStepErrFlag
      integer is_error

      include 'gr_par.fh'
      REAL*8 FlwTnSqFrom(44)
      COMMON/gr_par_nowrt/ FlwTnSqFrom
      REAL*8 FlwTnSqTo(44)
      COMMON/gr_par_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromUp(44)
      COMMON/gr_par_nowrt/ FlwHTnFromUp
      REAL*8 FlwHTnFromDo(44)
      COMMON/gr_par_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(44)
      COMMON/gr_par_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(44)
      COMMON/gr_par_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tc2Dim_TMax
      PARAMETER (Tc2Dim_TMax=2)
      INTEGER*4 Tn3Dim_TMax
      PARAMETER (Tn3Dim_TMax=2)
      INTEGER*4 INVPERe(51)
      COMMON/gr_par_nowrt/ INVPERe
      INTEGER*4 NZSUBe(540)
      COMMON/gr_par_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(52)
      COMMON/gr_par_nowrt/ INZSUBe
      INTEGER*4 ILNZe(52)
      COMMON/gr_par_nowrt/ ILNZe
      INTEGER*4 INVPERp(43)
      COMMON/gr_par_nowrt/ INVPERp
      INTEGER*4 NZSUBp(324)
      COMMON/gr_par_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(44)
      COMMON/gr_par_nowrt/ INZSUBp
      INTEGER*4 ILNZp(44)
      COMMON/gr_par_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=129)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=65)
      INTEGER*4 Nent
      PARAMETER (Nent=51)
      INTEGER*4 Npc
      PARAMETER (Npc=86)
      INTEGER*4 Npres
      PARAMETER (Npres=43)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(43)
      COMMON/gr_par_nowrt/ PIntIter
      LOGICAL*1 EIntIter(51)
      COMMON/gr_par_nowrt/ EIntIter
      LOGICAL*1 PExtIter(43)
      COMMON/gr_par_nowrt/ PExtIter
      LOGICAL*1 EextIter(51)
      COMMON/gr_par_nowrt/ EextIter
      CHARACTER*16 ConFlOutNames(1)
      COMMON/gr_par_nowrt/ ConFlOutNames
      !DEC$PSECT/gr_par_NOWRT/ NoWRT

      integer*4 arg1,arg2
      character*256 :: spjnam='gr_par'

c      character*80 msg
      
c      write(msg,*) 'gr_parLook: a1=',arg1,'    a2=',arg2
c      call wrcons(msg)

      select case(arg1)
      case(0)                 !�������� ������
         Event(1)=0
c         do i=1,nu
c           if((IBN(i)).and.(NodBodyType(i).eq.0) then   !���� 
c             if(NodEe.eq.0.d0) then
c               call gas_pt()
c             else
c               call gas_ph()
c             endif
c           endif
c         enddo
c****************************
c �������� ���������������� ������ ��� ������� ����� ����, ���� � ���� /�����/
c****************************
c        call partpre_initab
c****************************
      case(2)                 !����� ����� ��������� ������
c****************************
c ���������� ������, ������� ����������������� ��������� ������� /�����/
c****************************
c        call partpre_killtab
c****************************
      case(3)                 !����� ������
         Event(1)=3
c        is_error=FirstStepErrFlag
c         if(is_error.ne.0) then
c          write(*,*) 'ERROR! ������ � �������� ������. ���������'
c     * //' ������ ��������� ���� � ����������� ������!'
c          stop
c         end if
      case(4)                 !������� ������

      case(5)                 !�������� ���������
         Event(1)=5
         PropsKey(1) = 0
         FirstStepErrFlag=0
         TnkNormFlag=.true.
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3588 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3660 
         call gr_parconnIn
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3665 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3686 
      end select


* ���������� �������������� ���������� ������

      if(arg1.eq.5) then
         if(not_dic_state.eq.0) then ! 
            call gr_par_OnLoadDIC
         else if(not_cvt_state.eq.0) then
            call gr_par_OnLoadCvt
         else
            call gr_par_OnLoadNoncvt
         endif
c         call gr_par_OnLoadAny
         not_cvt_state=1
         not_dic_state=1
      endif

      end

*================================================
*-----------------------------------
      subroutine gr_par_OnLoadDIC
      implicit none
      include 'gr_par.fh'
      REAL*8 FlwTnSqFrom(44)
      COMMON/gr_par_nowrt/ FlwTnSqFrom
      REAL*8 FlwTnSqTo(44)
      COMMON/gr_par_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromUp(44)
      COMMON/gr_par_nowrt/ FlwHTnFromUp
      REAL*8 FlwHTnFromDo(44)
      COMMON/gr_par_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(44)
      COMMON/gr_par_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(44)
      COMMON/gr_par_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tc2Dim_TMax
      PARAMETER (Tc2Dim_TMax=2)
      INTEGER*4 Tn3Dim_TMax
      PARAMETER (Tn3Dim_TMax=2)
      INTEGER*4 INVPERe(51)
      COMMON/gr_par_nowrt/ INVPERe
      INTEGER*4 NZSUBe(540)
      COMMON/gr_par_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(52)
      COMMON/gr_par_nowrt/ INZSUBe
      INTEGER*4 ILNZe(52)
      COMMON/gr_par_nowrt/ ILNZe
      INTEGER*4 INVPERp(43)
      COMMON/gr_par_nowrt/ INVPERp
      INTEGER*4 NZSUBp(324)
      COMMON/gr_par_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(44)
      COMMON/gr_par_nowrt/ INZSUBp
      INTEGER*4 ILNZp(44)
      COMMON/gr_par_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=129)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=65)
      INTEGER*4 Nent
      PARAMETER (Nent=51)
      INTEGER*4 Npc
      PARAMETER (Npc=86)
      INTEGER*4 Npres
      PARAMETER (Npres=43)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(43)
      COMMON/gr_par_nowrt/ PIntIter
      LOGICAL*1 EIntIter(51)
      COMMON/gr_par_nowrt/ EIntIter
      LOGICAL*1 PExtIter(43)
      COMMON/gr_par_nowrt/ PExtIter
      LOGICAL*1 EextIter(51)
      COMMON/gr_par_nowrt/ EextIter
      CHARACTER*16 ConFlOutNames(1)
      COMMON/gr_par_nowrt/ ConFlOutNames
      !DEC$PSECT/gr_par_NOWRT/ NoWRT

        flag_start(1) = 2

      return
      end
*-----------------------------------
      subroutine gr_par_OnLoadCvt
      implicit none
      include 'gr_par.fh'
      REAL*8 FlwTnSqFrom(44)
      COMMON/gr_par_nowrt/ FlwTnSqFrom
      REAL*8 FlwTnSqTo(44)
      COMMON/gr_par_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromUp(44)
      COMMON/gr_par_nowrt/ FlwHTnFromUp
      REAL*8 FlwHTnFromDo(44)
      COMMON/gr_par_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(44)
      COMMON/gr_par_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(44)
      COMMON/gr_par_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tc2Dim_TMax
      PARAMETER (Tc2Dim_TMax=2)
      INTEGER*4 Tn3Dim_TMax
      PARAMETER (Tn3Dim_TMax=2)
      INTEGER*4 INVPERe(51)
      COMMON/gr_par_nowrt/ INVPERe
      INTEGER*4 NZSUBe(540)
      COMMON/gr_par_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(52)
      COMMON/gr_par_nowrt/ INZSUBe
      INTEGER*4 ILNZe(52)
      COMMON/gr_par_nowrt/ ILNZe
      INTEGER*4 INVPERp(43)
      COMMON/gr_par_nowrt/ INVPERp
      INTEGER*4 NZSUBp(324)
      COMMON/gr_par_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(44)
      COMMON/gr_par_nowrt/ INZSUBp
      INTEGER*4 ILNZp(44)
      COMMON/gr_par_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=129)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=65)
      INTEGER*4 Nent
      PARAMETER (Nent=51)
      INTEGER*4 Npc
      PARAMETER (Npc=86)
      INTEGER*4 Npres
      PARAMETER (Npres=43)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(43)
      COMMON/gr_par_nowrt/ PIntIter
      LOGICAL*1 EIntIter(51)
      COMMON/gr_par_nowrt/ EIntIter
      LOGICAL*1 PExtIter(43)
      COMMON/gr_par_nowrt/ PExtIter
      LOGICAL*1 EextIter(51)
      COMMON/gr_par_nowrt/ EextIter
      CHARACTER*16 ConFlOutNames(1)
      COMMON/gr_par_nowrt/ ConFlOutNames
      !DEC$PSECT/gr_par_NOWRT/ NoWRT

c        flag_start(1) = 1

      return
      end
*-----------------------------------
      subroutine gr_par_OnLoadNoncvt
      implicit none
      include 'gr_par.fh'
      REAL*8 FlwTnSqFrom(44)
      COMMON/gr_par_nowrt/ FlwTnSqFrom
      REAL*8 FlwTnSqTo(44)
      COMMON/gr_par_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromUp(44)
      COMMON/gr_par_nowrt/ FlwHTnFromUp
      REAL*8 FlwHTnFromDo(44)
      COMMON/gr_par_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(44)
      COMMON/gr_par_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(44)
      COMMON/gr_par_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tc2Dim_TMax
      PARAMETER (Tc2Dim_TMax=2)
      INTEGER*4 Tn3Dim_TMax
      PARAMETER (Tn3Dim_TMax=2)
      INTEGER*4 INVPERe(51)
      COMMON/gr_par_nowrt/ INVPERe
      INTEGER*4 NZSUBe(540)
      COMMON/gr_par_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(52)
      COMMON/gr_par_nowrt/ INZSUBe
      INTEGER*4 ILNZe(52)
      COMMON/gr_par_nowrt/ ILNZe
      INTEGER*4 INVPERp(43)
      COMMON/gr_par_nowrt/ INVPERp
      INTEGER*4 NZSUBp(324)
      COMMON/gr_par_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(44)
      COMMON/gr_par_nowrt/ INZSUBp
      INTEGER*4 ILNZp(44)
      COMMON/gr_par_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=129)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=65)
      INTEGER*4 Nent
      PARAMETER (Nent=51)
      INTEGER*4 Npc
      PARAMETER (Npc=86)
      INTEGER*4 Npres
      PARAMETER (Npres=43)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(43)
      COMMON/gr_par_nowrt/ PIntIter
      LOGICAL*1 EIntIter(51)
      COMMON/gr_par_nowrt/ EIntIter
      LOGICAL*1 PExtIter(43)
      COMMON/gr_par_nowrt/ PExtIter
      LOGICAL*1 EextIter(51)
      COMMON/gr_par_nowrt/ EextIter
      CHARACTER*16 ConFlOutNames(1)
      COMMON/gr_par_nowrt/ ConFlOutNames
      !DEC$PSECT/gr_par_NOWRT/ NoWRT

c        flag_start(1) = 0

      return
      end
****** End   file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Look.fs'
****** Begin file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mqnew.fs'
      subroutine gr_parMQNEW(dtInt1)
      IMPLICIT NONE
      include 'gr_par.fh'
      REAL*8 FlwTnSqFrom(44)
      COMMON/gr_par_nowrt/ FlwTnSqFrom
      REAL*8 FlwTnSqTo(44)
      COMMON/gr_par_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromUp(44)
      COMMON/gr_par_nowrt/ FlwHTnFromUp
      REAL*8 FlwHTnFromDo(44)
      COMMON/gr_par_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(44)
      COMMON/gr_par_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(44)
      COMMON/gr_par_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tc2Dim_TMax
      PARAMETER (Tc2Dim_TMax=2)
      INTEGER*4 Tn3Dim_TMax
      PARAMETER (Tn3Dim_TMax=2)
      INTEGER*4 INVPERe(51)
      COMMON/gr_par_nowrt/ INVPERe
      INTEGER*4 NZSUBe(540)
      COMMON/gr_par_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(52)
      COMMON/gr_par_nowrt/ INZSUBe
      INTEGER*4 ILNZe(52)
      COMMON/gr_par_nowrt/ ILNZe
      INTEGER*4 INVPERp(43)
      COMMON/gr_par_nowrt/ INVPERp
      INTEGER*4 NZSUBp(324)
      COMMON/gr_par_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(44)
      COMMON/gr_par_nowrt/ INZSUBp
      INTEGER*4 ILNZp(44)
      COMMON/gr_par_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=129)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=65)
      INTEGER*4 Nent
      PARAMETER (Nent=51)
      INTEGER*4 Npc
      PARAMETER (Npc=86)
      INTEGER*4 Npres
      PARAMETER (Npres=43)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(43)
      COMMON/gr_par_nowrt/ PIntIter
      LOGICAL*1 EIntIter(51)
      COMMON/gr_par_nowrt/ EIntIter
      LOGICAL*1 PExtIter(43)
      COMMON/gr_par_nowrt/ PExtIter
      LOGICAL*1 EextIter(51)
      COMMON/gr_par_nowrt/ EextIter
      CHARACTER*16 ConFlOutNames(1)
      COMMON/gr_par_nowrt/ ConFlOutNames
      !DEC$PSECT/gr_par_NOWRT/ NoWRT

****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
      integer*4 Ncomp,CompLin(1),CompOin(1),CompGenTyp(1)
      parameter(Ncomp=0)
      real*8 CompQq(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3748 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3753 


****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'

c      real*8 FlwCondRegul(nv)
c      common/gr_parFlwL/ FlwCondRegul
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3764 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3799 
      integer*4 nfwv
      parameter(nfwv=0)
      integer*4 FwvFloWw(1),FwvFloWv(1)
      real*8 FwvRoFrom1(1),FwvRoFrom2(1),FwvRoTo1(1),FwvRoTo2(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3805 
      integer*4 nfwv1
      parameter(nfwv1=0)
      integer*4 Fwv1Flow(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3810 
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3826 
* ���������� ��� ��������������� ��������� ����������
      real*8 C0He2(NHe2) !����. ��� ������� ����������� ���� ��
      real*8 C1He2(NHe2) !����. ��� ������� ����������� ���� ��
      real*8 C2He2(NHe2) !����. ��� ������� ����������� ���� ��
      real*8 DTHe2
      real*8 C0He2b1(NHe2),C1He2b1(NHe2),C2He2b1(NHe2)
      real*8 C0He2b2(NHe2),C1He2b2(NHe2),C2He2b2(NHe2)
      real*8 He2T1t(2,NHe2),He2T2t(2,NHe2),He2decQt(NHe2)
      real*8 He2vvGa(NHe2)
      common/gr_parHE2/ C0He2,C1He2,C2He2,DTHe2,
     * C0He2b1,C1He2b1,C2He2b1,C0He2b2,C1He2b2,C2He2b2,
     * He2T1t,He2T2t,He2decQt,He2vvGa
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3840 
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3851 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3858 

      integer*4 NMtl
      parameter ( NMtl=0 )
      real*8 MtlTt(1),MtlMass(1),MtlCp(1),MtlAlCulc(1)
      real*8 Mtl_QqOut(1)  !��������� �������� ����� � ������� ��������
      real*8 MtlLamdC(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3866 

      integer*4 NMtlO,MtlOIndRoom(1)
      parameter ( NMtlO=0 )
      real*8 MtlOTt(1),MtlOMass(1),MtlOCp(1),MtlOAlCulcWl1(1)
      real*8 MtlO_QqOut(1)  !��������� �������� ����� � ������� ��������
      real*8 MtlOLamdC(1),MtlOLamd(1),MtlOQqEnv(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3874 
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'

      integer*4 NUCH
      parameter (NUCH=0)
      integer*4 CoalHConNodInd(1)
      real*8 CoalHNodMoistG(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3883 

      integer*4 NFlwWasteSet
      parameter(NFlwWasteSet=0)
      integer*4 FlwWasteSetConnInd(1),FlwWasteSetNum(1)
      real*8 FlwWasteSetWaste(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3890 

      integer*4 FlwConnFlwInd(1),NFlwConn
      parameter(NFlwConn=0)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3895 

      real*8 HC1PpFrom(NHC1),HC1PpTo(NHC1)
      common/gr_parConFlOut/ HC1PpFrom,HC1PpTo
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3900 

      integer*4 NHCO
      parameter(NHCO=0)
      integer*4  HCOFlMod(1),HCOIndFlw(1)
      real*8 HCOCfMod(1),HCOOFg(1),HCOONodVol(1),HCOONoddRodP(1)
      common/gr_parConFlOut/ HCOFlMod,HCOCfMod,HCOOFg,HCOONodVol,
     >HCOONoddRodP,HCOIndFlw
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3908 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3915 

      integer*4 nCrv
      parameter(nCrv=0)
      real*8 CrvRes(1)
      integer*4 CrvFlw(nCrv)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3922 

      integer*4 nWst
      parameter(nWst=0)
      integer*4 WstNode(1)
      real*8    WSTConsN2(1),WSTConsO2(1),WSTConsH2O(1),
     *          WSTConsCO2(1),WSTConsSoot(1),WSTConsAsh(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3930 

      integer*4 NXS
      parameter(NXS=0)
      integer*4 XSFlagSetCc(1),XSFlSet(30,1),XSIndSetCc(1),
     >XSIndSetCc0(1)
      real*8    XSCcstr(30,1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3937 

      integer*4 ntFrB
      parameter(ntFrB=0)
      integer*4 tFrBNdInd(1),tFrBi(1)
      real*8    tFrBCcOld(100,1),tFrBTau(1)
      real*8    tFrBGIn(1),tFrBAa(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3945 

      integer*4 np4
      parameter(np4=0)
      integer*4 Pm4Nb(1)
      real*8 Pm4Res(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3952 

c ��������� �����. ��� ������� � ���������������, ���� �� (�������) ���
      integer*4 np
      parameter(np=0)
      integer*4 VNKZ(1)
      real*8 PumRes(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3960 
      integer*4 np2
      parameter(np2=0)
      integer*4 Pm2Nb(1)
      real*8 Pm2Res(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3966 
      integer*4 nFan
      parameter(nFan=0)
      integer*4 FanFlw(1)
      real*8 FanRes(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3972 
      integer*4 nFan2
      parameter(nFan2=0)
      integer*4 Fan2Flw(1)
      real*8 Fan2Res(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3978 
      integer*4 np3
      parameter(np3=0)
      real*8 Pm3QqNod(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3983 
      integer*4 nfr1
      parameter(nfr1=0)
      integer*4 Fr1Flw(1)
      real*8 Fr1Bor(1)
      real*8 Fr1KkFullWst(1)
      real*8 Fr1Waste_T(100,1)
      real*8 Fr1WstMass_T(100,1)
      integer*4 Fr1SumMas_0(1),Fr1FlSumMas(1),Fr1F0SumMas(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 3993 

      real*8 time_df1  !������� ��� ������� ������� (� �����)
      real*8 time_tmp

      real   deltat      !��� ����������
      real   deltat1      !��� ����������
      REAL*8 Atau        !1/dt
c      save Atau
      real*4 dtInt       !dt*Accel
      real*8 TimeSrv     !dt/Niter
      character*100 fn1  ! ��� ������������� .cbf ������
c      common/gr_parLocal1/ time_tmp,deltat,Atau,dtInt,TimeSrv,fn1
      common/gr_parLocal1/ Atau,time_tmp,deltat1,dtInt,TimeSrv,fn1
      real*8 VOL1(nu) !V/dt
      real*8 NodCfdE(nu) !����. � ��. ����. ��� ����� ���������
      real*8 NodTtk(nu)  !����������� ��������. � ������� ����� �� k-�� ��������
      real*8 RotVol(nu) !��������� ������
      real*8 RokVol(nu) !��������� ������
      real*8 NodVolk(nu)  !������������ ����� �� k-�� ��������
      real*8 NoddVdPk(nu) !����������� ������ � ���� �� �������� �� k-�� ����.

c ---  ������ �. (29.08.02) - ������
      real*8 NodSum1(nu)
      real*8 NodSum2(nu)
c ---  ������ �. (29.08.02) - �����

      real*8 NodQqPmAd(nu)

      common/gr_parNODE1/ VOL1,NodCfdE,NodTtk,RotVol,RokVol,
     * NodVolk,NoddVdPk,NodSum1,NodSum2,NodQqPmAd
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4025 

      real*8 C7(nv)   !��������� ������������
      real*8 Gk(nv)   !������� �� k-�� ��������
      real*8 dHLev(nv)!������ �� ����� �� ���� ������� � �������
      real*8 Rovk(nv)    !��. ��������� �� k-�� ��������
      real*8 AconRovk(nv)!��������� ��� ��������
c###### ��������� ####################################
      real*8 FlwGaVeSq(nv)  ! ��������� ������ ��� �������� ������������� �����
      real*8 flwbcon(nv)    ! �������� ��������� �������� �������� �� ����� 
      real*8 FlwC7work1(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work2(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work3(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work4(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work5(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work7(nv)
      real*8 FlwVsk    (nv)   ! �������� �� ����� �� ���������
      real*8 ComEeFrom  (nv)   ! ��������� ��������� ��������� � ��������
      real*8 ComEeTo    (nv)   ! ��������� ��������� ��������� � ��������  
      real*8 FlwSlGaFrom(nv)   ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaFrom0(nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaTo   (nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaTo0  (nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwCcFrom(nv) ! ��������� ������ ��� ������������ ���� �� ������ �����
      real*8 FlwCcTo(nv)   ! ��������� ������ ��� ������������ ���� �� ������ �����
      real*8 flwdhz0(nv)
      real*8 ComRoFrom(3,nv)
      real*8 ComRoTo  (3,nv)
c###### ��������� ####################################
      real*8 FlwKgp(nf,2,nv)    ! 
      real*8 FlwKge(nf,nf,nv)
      real*8 FlwKgg(3,nv)
      real*8 FlwKgpt(2,nv)
      real*8 FlwdPLevFrom(nv),FlwdPLevTo(nv)
      real*8 FlwCcGgWtJa(nv)
      real*8 FlwRes_Iter(nv)
      real*8 FlwGG_TMP(nv)
      real*8 FlwRes_Tmp
      real*8 FlwdCgDCcFrom(nv),FlwdCgDCcTo(nv)
c      real*8 frco0(nv)
      common/gr_parFLOW/ C7,Gk,dHLev,Rovk,AconRovk,FlwGaVeSq,
     * flwbcon,FlwC7work1,
     * FlwC7work2,FlwC7work3,FlwC7work4,FlwC7work5,FlwC7work7,
     * FlwVsk,ComEeFrom,ComEeTo,FlwSlGaFrom,FlwSlGaFrom0,
     * FlwSlGaTo,FlwSlGaTo0,FlwCcFrom,FlwCcTo,flwdhz0,ComRoFrom,
     * ComRoTo,FlwKgp,FlwKge,FlwKgg,FlwKgpt,
     * FlwdPLevFrom,FlwdPLevTo,FlwCcGgWtJa,
     * FlwRes_Iter,FlwRes_Tmp,FlwGG_TMP,FlwdCgDCcFrom,FlwdCgDCcTo
c    * ,frco0
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4075 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4080 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4085 


c      !������� ����. ����� ����� � ���. ��������� ��� ��������
       real*8 Dmp(Npres,Npres)
C       real*8 Dmpc(Npc,Npc) ! �������� 
c      !������� ����. ����� ����� � ������� ��������� ��� ���������
       real*8 Dmh(Nent,Nent)
       real*8 DmhG(Nent,Nent)
c      !������ ����� ��� ���������� ������� �� ����.
      Real*8 Yp(Npres)
c      Real*8 Ypc(Npc)
c      !������ ����� ��� ���������� ������� �� ���.
      real*8 Ye(Nent)
      real*8 YeG(Nent)
c      !������� �� ���������
      real*8 dP(Npres)
c      real*8 dPC(Npc)
c      !������� �� ����������
      real*8 dE(Nent)
      real*8 dEG(Nent)
c      !������ ������ �������� �� k-�� ��������
      real*8 Pk(Npres)
c      !������ ������ �������� �� (k+1)-�� ��������
      real*8 Pk1(Npres)
c      !������ ������ ��������� �� k-�� ��������
      real*8 Ek(Nent)
      real*8 EGk(Nent)
c      !������ ������ ��������� �� (k+1)-�� ��������
      real*8 Ek1(Nent)
      real*8 EGk1(Nent)
      real*8 Rok(Nu) !�����. �\� ����� � ������� ����� �� k-�� ����.
      !������ ������ ��������� �����. ����
      real*8 EeWtSatk(Nent)
      !������ ������ ��������� �����. ����
      real*8 EeVpSatk(Nent)
      real*8 MaxDevP     !����. �����. ����. �������� �� ��������
      real*8 MaxDevH     !����. �����. ����. ��������� �� ��������
      INTEGER*4 I,j     !���������� �����
      integer*4 IC        !������� ��������
      integer*4 ICInt !������� ���������� ��������
      common/gr_parLocal2/ Dmp,Dmh,Yp,Ye,dP,dE,Pk,Pk1,Ek,Ek1,Rok,
     *  EeWtSatk,EeVpSatk,MaxDevP,MaxDevH,IC,ICInt,DmhG,YeG,dEG,
     *  EGk,EGk1       !,dPC,Ypc,Dmpc <=������ ��������!!!!

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4133 

      INTEGER*4 nvlgpos !����� ����� � �������������� ���������� � ����������
      common/gr_parVlvGeo/ nvlgpos
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4138 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4143 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4148 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4153 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4158 

      integer*4 nsep1,Sep1FlwS(1),Sep1Node(1)
        parameter(nsep1=0)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4163 

      integer*4 nInWst,InWstNum(1),InWstNode(1),InWstNdTp(1)
      real*8    InWstdt(1)
      integer*4 InWstTyp(1)
      real*8    InWstCc(1)
      real*8 InWstExtCon(1),InWstExtWstdt(1)
      parameter(nInWst=0)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4172 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4177 


      LOGICAL*1 go,goint  !������� ����������� ��������
      DATA go/.true./     !����������� �������� �������. ��������
      common/gr_parLocal4/ go,goint
      real*8  RDTSC
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4216 
      real*8 NoddRodPk(nu) !dRo/dP ��� ������� ����� �� ����. ����.
      real*8 NoddRodEk(nu) !dRo/d� ��� ������� ����� �� ����. ����.
      real*8 NoddRodPk1(nu) !dRo/dP ��� ������� ����� �� ���. ����.
      real*8 NoddRodEk1(nu) !dRo/d� ��� ������� ����� �� ���. ����.
      real*8 nodrorelax(nu)
      real*8 NodRoGa0(nu)
      real*8 NodCcGa_Aeq0(nu) 
      real*8 NodCcGa_Deq0(nu) 
      real*8 NodCcGa_Beq (nu)
      real*8 NodCcGa_Beq0(nu)
      real*8 NodCcGa_Aeq (nu)
      real*8 NodCcGa_Deq (nu)
      real*8 NodXxk1(nu)    !���. �������. � ������� ����� �� ���. ����.
      real*8 NodXxBalk1(nu) !���. �������. � ������� ����� �� ���. ����.
      real*8 NodQqAd(nu)
      real*8 NodCfEeAsh(nu)
      real*8 NodEeWvt(nu)
      real*8 NodAlSqGaWv(nu),NodYe(nu),NodDmh(nu)
      real*8 NodPsw(nu),NodRoWvTmp(nu),NoddRdHGas(nu)
     *,NoddRdPWvTmp(nu),NoddRdHWvTmp(nu)
     *,NodTtWvTmp(nu),NodCpWvTmp(nu),NodXxTmp(nu)
      integer*4 Nod_ghm_flag(nu),NodPropRelaxFlag(nu)
      logical*1 EExtIter_(Nent)
      logical*1 PExtIter_(Npres)
      common /gr_parNODE/ NoddRodPk,NoddRodEk,NoddRodPk1,NoddRodEk1,
     *  nodrorelax,NodRoGa0,NodCcGa_Aeq0,NodCcGa_Deq0,NodCcGa_Beq,
     *  NodCcGa_Beq0,NodCcGa_Aeq,NodCcGa_Deq,NodXxk1,NodXxBalk1,
     *  NodQqAd,NodCfEeAsh,NodEeWvt,NodAlSqGaWv,NodYe,NodDmh,
     *  NodPsw,NodRoWvTmp,NoddRdHGas,NodTtWvTmp,NodCpWvTmp,NodXxTmp
      common /gr_parNODE_I/ Nod_ghm_flag,NodPropRelaxFlag
      common /gr_parNODE_L/ EExtIter_,PExtIter_
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4249 
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'

      integer NTc0
      parameter(NTc0=0)
      integer*4 Tc0IbnFro(1),Tc0NodFro(1),Tc0IbnTo(1),Tc0NodTo(1)
      real*8 Tc0zt(1),Tc0AlG23(1),Tc0AlG43(1)
     *,Tc0G43(1),Tc0G23(1),Tc0G34(1),Tc0CpWl(1)
     *,Tc0TtEnv(1),Tc0TtWlk(1),Tc0TtWl(1),Tc0AlSqEnv(1),Tc0AlEnv(1)
     *,Tc0AlSqPhWl(1,nf),Tc0MasCpWl(1),Tc0CfAlEnv(1)
     *,Tc0CfAlGaWl(1),Tc0CfAlWpUpWl(1)
     *,Tc0CfAlWtWl(1),Tc0CfAlWpDoWl(1)
     *,Tc0EvUpWl(1),Tc0EvDoWl(1)
     *,Tc0dG23dp(1),Tc0dG34dp(1),Tc0dG43dp(1)
     *,Tc0CfGgWpUpWt(1),Tc0CcGgWtWpDo(1),Tc0CfGgWpDoWt(1)
     *,Tc0G32(1),Tc0dG32dp(1),Tc0CfGgWt(1),Tc0SqWl(1),Tc0TettaQ(1,nf)
     *,Tc0QqIsp(1),Tc0AlIsp(1)
     *,Tc0dQIspdP(1),Tc0TtTnIsp(1),Tc0AlSqIsp(1),TC0ResWl(1)
     *,Tc0dQG23dp(1),Tc0TettaQ2(1,nf),Tc0DeltaTs(1)
     *,Tc0AlG23From(1),Tc0AlG43From(1)
     *,Tc0G23From(1),Tc0G34From(1),Tc0G32From(1),Tc0G43From(1)
     *,Tc0dG23dpFrom(1),Tc0dG34dpFrom(1),Tc0dG43dpFrom(1),
     >Tc0dG32dpFrom(1)
     *,Tc0dQIspdPFrom(1),Tc0TtTnIspFrom(1),Tc0AlSqIspFrom(1)
     *,Tc0CcGgWtWpDoFrom(1),Tc0CfGgWpUpWtFrom(1),Tc0CfGgWpDoWtFrom(1)
     *,Tc0QqIspFrom(1),Tc0AlIspFrom(1),Tc0CfGgWtFrom(1),Tc0TettaQFrom(1,
     >nf)
     *,Tc0AlSqPhWlFrom(1,nf),Tc0CfRelAlIsp(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4279 
C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4308 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4331 
      real*8 Tc2zt(NTc2),Tc2AlG23(NTc2),Tc2G23(NTc2),Tc2G34(NTc2)
     *,Tc2AlG43(NTc2),Tc2G43(NTc2),Tc2TtWlk(NTc2),Tc2AlPhWl(NTc2,nf)
     *,Tc2AlSqPhWl(NTc2,nf),Tc2dG23dp(NTc2),Tc2dG34dp(NTc2)
     *,Tc2dG43dp(NTc2),Tc2G32(Ntc2),Tc2dG32dp(Ntc2)
c     *,Tc2TettaQ(NTc2,nf)
     *,Tc2dQIspdP(NTc2),Tc2TtTnIsp(NTc2),Tc2AlSqIsp(NTc2)
      real*8 Tc2AlEnv_P(NTc2)
      common/gr_parTC2/ Tc2zt,Tc2AlG23,Tc2G23,Tc2G34
     *,Tc2AlG43,Tc2G43,Tc2TtWlk,Tc2AlPhWl
     *,Tc2AlSqPhWl,Tc2dG23dp,Tc2dG34dp,Tc2dG43dp,Tc2G32,Tc2dG32dp
c     *,Tc2TettaQ
     *,Tc2dQIspdP,Tc2TtTnIsp,Tc2AlSqIsp,Tc2AlEnv_P
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4345 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4355 

      integer*4 NTCR,TcRNode(1)
      parameter ( NTCR=0 )
      real*8 TcR_XQ(1),TcR_BQ(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4361 

      integer*4 Nwall2O
      parameter ( Nwall2O=0 )
      real*8  Wl2OTt(6,1),Wl2ORAw(1),Wl2OSqEnv(1),Wl2OQqBout(1),
     &        Wl2ODout(1)

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4369 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4375 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'

      integer NTen
      parameter(NTen=0)
      integer*4 TenIbn(1),TenNode(1)
      real*8 Tenzt(1),TenQqIn(1),TenAlG23(1),TenG23(1),TenG34(1)
     *,TenAlG43(1),TenG43(1),TenCpWl(1),TenTtWlk(1),TenTtWl(1)
     *,TenAlSqPhWl(1,nf),TenMasCpWl(1),TenCfAlGaWl(1),TenCfAlWpUpWl(1)
     *,TenCfAlWtWl(1),TenCfAlWpDoWl(1),TenEvUpWl(1),TenEvDoWl(1)
     *,TendG23dp(1),TendG34dp(1),TendG43dp(1)
     *,TenCfGgWpUpWt(1),TenCcGgWtWpDo(1),TenCfGgWpDoWt(1)
     *,TenG32(1),TendG32dp(1),TenCfGgWt(1),TenSqWl(1),TenTettaQ(1,nf)
     *,TenQqIsp(1),TenAlIsp(1),TenDeltaTs(1)
     *,TendQIspdP(1),TenTtTnIsp(1),TenAlSqIsp(1),TenResWl(1),
     >TenTcwAlCulc(1),TenTcwQq(1)
     *,TenLawL(1),TenQqInt(1),TenQqExt(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4398 
C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4412 
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4450 

c   ��������� ���������� ��� "����� �������"
*--- ����� ���������� ����������
      real*8 Tn3Pp(n03,Tn3Ne)    ! �������
      real*8 Tn3Ee(n03,Tn3Ne)    ! ���������
      real*8 Tn3Tt(n03,Tn3Ne)    ! �����������
      real*8 Tn3TtWl(n03,2)  ! ����������� ������
      real*8 Tn3TtEnv(n03,2) ! ����������� � ���������� ����������
      real*8 Tn3Mas(n03,Tn3Ne)   ! ����� ���
      real*8 Tn3rot(n03,Tn3Ne)   ! ��������� ���
      real*8 Tn3Volt(n03,Tn3Ne)  ! ������ ���
      real*8 Tn3Voldt(Tn3Ne,n03) ! ������ ���/dt
      real*8 Tn3MasGas(n03,3)! ����� ������������ �����
*---

      real*8 Tn3rok(n03,Tn3Ne)   ! ��������� �� ��������
      real*8 Tn3pk (n03,Tn3Ne)   ! �������� �� ��������
      real*8 Tn3ek (n03,Tn3Ne)   ! �������� �� ��������
      real*8 Tn3hw (n03,Tn3Ne)   ! ��������� ��������� ���� �� ��������
      real*8 Tn3hs (n03,Tn3Ne)   ! ��������� ��������� ���� �� ��������
      real*8 Tn3RoWtSatk(n03,Tn3Ne)   ! ��������� ��������� ���� �� ��������
      real*8 Tn3cp (n03,Tn3Ne)   ! ������������
      real*8 Tn3Volk(n03,Tn3Ne)  ! ������ ��� �� ��������
      real*8 Tn3TtWlk(n03,2) ! ����������� ������ �� ��������
      real*8 Tn3Mask(Tn3Ne,n03)  ! ����� ��� �� ���������
      real*8 Tn3MasGask(3,n03) ! ����� ������������ ����� �� ���������
      real*8 Tn3Volkdt(Tn3Ne,n03) ! ������ ���/dt �� ��������

      real*8 Tn3AlSqPhPh(Tn3Ne,Tn3Ne,n03) ! alfa*S ����� ������
      real*8 Tn3SqPhPh(Tn3Ne,Tn3Ne,n03) ! S ����� ������
      real*8 Tn3AlPhPh(Tn3Ne,Tn3Ne,n03) ! �-�� ������������� ����� ����� ������
      real*8 Tn3AlSqPhWl(n03,Tn3Ne)   ! �-�� ��������. ����� ������ � �������
      real*8 Tn3AlWlUpDo(n03)
      !����. ������� ��������. ����� ������ ����� ������ � ����� ������
      real*8 Tn3ActWlUpDo(n03)
      !�-�� ������������� ����� ������� � ����������
      real*8 Tn3AlSqWlEnv(n03,2)
      real*8 Tn3MasCpWl(n03,2)    ! �����*Cp ������

      real*8 Tn3drdp(n03,Tn3Ne)
      real*8 Tn3drdh(n03,Tn3Ne)
      real*8 Tn3dh(n03,Tn3Ne)

      real*8 Tn3G(Tn3Ne,Tn3Ne,n03) 
      real*8 Tn3dGdP(Tn3Ne,Tn3Ne,n03),Tn3dg32dp2(n03),Tn3dg32dp3(n03)
      real*8 Tn3Gw23(n03) 
      real*8 Tn3dGw23dP(n03) 
      real*8 Tn3Gw34(n03) 
      real*8 Tn3dGw34dP(n03) 
      real*8 Tn3Gw43(n03) 
      real*8 Tn3dGw43dP(n03) 
      real*8 Tn3Gw32(n03) 
      real*8 Tn3dGw32dP(n03) 
      real*8 Tn3G32Sum(n03)
      real*8 Tn3G34Sum(n03)
      real*8 Tn3G23Sum(n03)
      real*8 Tn3G43Sum(n03)

      real*8 Tn3G0(Tn3Ne,Tn3Ne,n03)
      real*8 Tn3CcGas(3,n03)
      real*8 Tn3AlGw23(n03)
      real*8 Tn3AlGw34(n03)
      real*8 Tn3AlGw43(n03)
      real*8 Tn3zd(n03)
      real*8 Tn3zu(n03)
      real*8 Tn3det(n03)
      real*8 Tn3zt(n03)
      real*8 Tn3Ts(n03,Tn3Ne)
      real*8 Tn3Ts_(n03,Tn3Ne)
      real*8 Tn3rv(Tn3Ne,n03)
      real*8 Tn3GSum(Tn3Ne,n03)
      real*8 Tn3MasgenMult(Tn3Ne,n03)
      real*8 Tn3Masgen(Tn3Ne,n03)
      real*8 Tn3AlGv23(n03)
      real*8 Tn3Gv23(n03)
      real*8 Tn3dGv23dP(n03)
 
      real*8 Tn3Pk2(n03,Tn3Ne)
      real*8 Tn3hw2(n03,Tn3Ne)
      real*8 Tn3hs2(n03,Tn3Ne)
      real*8 Tn3ts2(n03,Tn3Ne)
      real*8 Tn3ts2_(n03,Tn3Ne)
      real*8 Tn3Tt2(n03,Tn3Ne)
      real*8 Tn3ro2(n03,Tn3Ne)
      real*8 Tn3cp2(n03,Tn3Ne)
      real*8 Tn3hd2(n03,Tn3Ne)
      real*8 Tn3CcUp2(2,n03)
      real*8 Tn3Xxk(n03,Tn3Ne)
      logical*1 Tn3RelaxKG
      real*8 Tn3dp(n03,Tn3Ne)
      real*8 Tn3Mas0(n03,Tn3Ne)
c      real*8 Tn3CfMasGasMin ! /1.0d-7/
c      parameter (Tn3CfMasGasMin=1.0d-7)
      real*8 Tn3TettaQ(n03,Tn3Ne)
      real*8 Tn3Vk(n03,Tn3Ne)
      real*8 Tn3dTdH(n03,Tn3Ne)
      real*8 Tn3CcUp(2,n03)
      real*8 Tn3AVolDo(n03)
      real*8 Tn3TettaWl(n03)
      real*8 Tn3LevRelk(n03),Tn3LevMask(n03)
      real*8 Tn3QqPhPh(Tn3Ne,Tn3Ne,n03),Tn3QqGen(Tn3Ne,Tn3Ne,n03)
        real*8 Tn3y1(n03),Tn3y2(n03),Tn3z1(n03),Tn3z2(n03)
      real*8 Tn3MasGaRealk(n03),Tn3MasGaReal0(n03)
      real*8 Tn3TcwAlCulc(n03,2)
      real*8 Tn3La(n03,Tn3Ne)
      real*8 Tn3Cf42(n03)
      real*8 Tn3WlTtIsp(n03)
      real*8 Tn3HD(n03,Tn3Ne),Tn3Sigma(n03),Tn3Vel42(n03)
     *,Tn3Dd4(n03)
      real*8 Tn3FlwHUp(1,1)
      real*8 Tn3FlwHDo(1,1)
      real*8 Tn3VolNorm0(n03)
      real*8 Tn3dMLoc_tmp(Tn3Ne,n03)
      real*8 Tn3VolFull(n03)
         real*8 Tn3Pk_dev(n03)

      common/gr_parTn3/
     * Tn3Pp,Tn3Ee,Tn3Tt,Tn3TtWl,Tn3TtEnv,Tn3Mas,Tn3rot,Tn3Volt,Tn3Voldt
     * ,Tn3MasGas,Tn3rok,Tn3pk,Tn3ek,Tn3hw,Tn3hs,Tn3RoWtSatk,Tn3cp
     * ,Tn3Volk,Tn3TtWlk,Tn3Mask,Tn3MasGask,Tn3Volkdt,Tn3AlSqPhPh
     * ,Tn3SqPhPh,Tn3AlPhPh,Tn3AlSqPhWl,Tn3AlWlUpDo,Tn3ActWlUpDo
     * ,Tn3AlSqWlEnv,Tn3MasCpWl,Tn3drdp,Tn3drdh,Tn3dh,Tn3G,Tn3dGdP
     * ,Tn3dg32dp2,Tn3dg32dp3,Tn3Gw23,Tn3dGw23dP,Tn3Gw34,Tn3dGw34dP
     * ,Tn3Gw43,Tn3dGw43dP,Tn3Gw32,Tn3dGw32dP,Tn3G32Sum,Tn3G34Sum
     * ,Tn3G23Sum,Tn3G43Sum,Tn3G0,Tn3CcGas,Tn3AlGw23
     * ,Tn3AlGw34,Tn3AlGw43,Tn3zd,Tn3zu,Tn3det,Tn3zt,Tn3Ts,Tn3rv
     * ,Tn3GSum,Tn3MasgenMult,Tn3Masgen,Tn3AlGv23,Tn3Gv23,Tn3dGv23dP
     * ,Tn3Pk2,Tn3hw2,Tn3hs2,Tn3ts2,Tn3Tt2,Tn3ro2,Tn3cp2,Tn3hd2
     * ,Tn3CcUp2,Tn3Xxk,Tn3dp,Tn3Mas0,Tn3TettaQ,Tn3Vk,Tn3dTdH
     * ,Tn3CcUp,Tn3AVolDo,Tn3TettaWl,Tn3LevRelk,Tn3LevMask,Tn3QqPhPh
     * ,Tn3QqGen,Tn3y1,Tn3y2,Tn3z1,Tn3z2,Tn3MasGaRealk,Tn3MasGaReal0
     * ,Tn3TcwAlCulc,Tn3La,Tn3Cf42,Tn3WlTtIsp,Tn3HD,Tn3Sigma
     * ,Tn3Vel42,Tn3Dd4,Tn3FlwHUp,Tn3FlwHDo,Tn3VolNorm0,Tn3ts_,Tn3ts2_
     * ,Tn3dMLoc_tmp,Tn3VolFull,Tn3Pk_dev
      common/gr_parTn3L/ Tn3RelaxKG
      real*8 Tn3GSpCond(nv)
      real*8 Tn3QSpCond(nv)
      common/gr_parTn3Flow/ Tn3GSpCond,Tn3QSpCond
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4592 


****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'

      integer*4 n07,Tn7RelP(1,1),Tn7RelE(2,1),Tn7Homo(1),Tn7CcSetInd(1)
      real*8    Tn7RoGa(1),Tn7RoWt(1),Tn7Rok(1,2),Tn7XxWt(1),Tn7Xk(1,2),
     *          Tn7Lev(1),Tn7EeWt(1),Tn7VsGa(1),Tn7VsWt(1),Tn7PpGa(1),
     *          Tn7EeGa(1),Tn7EeWtSat(1),Tn7EeVpSat(1),Tn7LevRel(1),
     *          Tn7Hi(1),Tn7PpWt(1),Tn7TtWt(1),Tn7Ee(2,1),
     *          Tn7Ek(2,1),Tn7CcVolk(1),Tn7CcMask(1)
     *         ,Tn7XxBalWt(1),Tn7Levk(1),Tn7Lev_Rlx(1),Tn7Acon(1),
     *          Tn7Sq(1),Tn7CcBor(1),Tn7MasWt(1),Tn7MasGa(1),
     *          Tn7MasBor(1),Tn7GCorrWt(1),
     *          Tn7dRdPk(2,1),Tn7dRdP(2,1),Tn7dRdHk(2,1),Tn7dRdH(2,1),
     *          dP_Tn7(1),Tn7Dcon(1),Tn7LsMin(1),Tn7dMLoc(2,1),
     *          Tn7CfRoGaCorr(1),Tn7CfRoWtCorr(1),
     *          Tn7Waste_T(20,1),Tn7Rowttrue(1),Tn7RoWtSat(1),
     *          Tn7RoVpSat(1),Tn7Dmint(1),Tn7Gcorr(1),
     *          Tn7WlUpQqOut(1),Tn7WlDoQqOut(1),Tn7TcwAlCulc(1,1),
     *          Tn7TtWlUp(1),Tn7TtWlDo(1),Tn7LaWl(1),Tn7QqWlEnv(1),
     >Tn7RoomInd(1)
     *          ,Tn7EvDo(1),Tn7EvDo0(1),Tn7TtGa(1),Tn7AlSqPhWl(2,1),
     >Tn7AlWlUpDo(1)
     *          ,Tn7dMIntGa(1),Tn7dMIntWt(1),Tn7GCorrGa(1),
     >Tn7LevTunG(1),Tn7LevTunPpRegG(1)
      logical*1 Tn7_Flag_Mas_Deb(1),Tn7CfCorr_On(1)


      logical*1 Tn7CcBorLb(1)

      parameter(n07=0)
      real*8 Tn7Pk(2,1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4626 

C     common/gr_parC     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4683 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
      integer n08
      parameter(n08=0)
      integer*4 Tn8RelP(1,0),Tn8RelE(2,0),Tn8CcSetInd(1)
      real*8 Tn8RoWt(1),Tn8RoGa(1),Tn8GSumWt1(1),Tn8GSumWt2(1),
     *       Tn8Lev(1),Tn8RoWtk(1),Tn8PpWt(1),Tn8MasBor(1),Tn8ccBor(1),
     *       Tn8VolWt(1),Tn8XxWt(1),Tn8XxBalWt(1),
     *       Tn8MasWt(1),Tn8MasWtk(1),
     *       Tn8RoGak(1),Tn8Gsliv(1),Tn8EeGa(1),
     *       Tn8EeWt(1),Tn8PpGa(1),Tn8VsWt(1),Tn8VsGa(1),Tn8VsVp(1),
     *       Tn8EeWtSat(1),Tn8EeVpSat(1),Tn8LevRel(1),Tn8Hi(1),
     *       Tn8TtGa(1),Tn8TtWt(1),Tn8GgXx(1),
     *       Tn8dRdP1(1),Tn8dRdH1(1),Tn8dRdP2(1),Tn8dRdH2(1),
     *       Tn8Waste_T(20,1),Tn8RowtSat(1),Tn8RovpSat(1),
     *       Tn8QqEnv(1),Tn8RoomInd(1),Tn8TtMeDo(1)
     *       ,Tn8EvDo(1),Tn8EvDo0(1),Tn8GInOut(1),Tn8LevTunG(1)

      logical*1 Tn8CcBorLb(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4707 

C     common/gr_parC     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4744 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'

      integer n09
      parameter(n09=0)
      integer*4 Tn9RelP(2,1),Tn9RelE(Tn9Ne,1),Tn9CcSetInd(1)
      real*8 Tn9RoWt(1),Tn9Cp(1,1),Tn9Lev(1),
     *       Tn9Ek(1,1),Tn9Rok(1,Tn9Ne),Tn9PpWt(1),Tn9Pk(1,1),Tn9Rot(1,
     >Tn9Ne),
     *       Tn9Xxk(1,Tn9Ne),Tn9MasBor(1),Tn9ccBor(1),Tn9VolWt(1),
     *       Tn9dp(1,Tn9Ne),Tn9MasWt(1),Tn9GIspSum(1),Tn9Pp(1),
     *       Tn9GCondSum(1),Tn9G32sum(1),Tn9G34sum(1),Tn9G23sum(1),
     *       Tn9G43sum(1),Tn9TettaQ(1,Tn9Ne),Tn9Ev_t(1,1),Tn9Dim_t(1),
     *       Tn9Dim_tmax/1/,Tn9EvDo(1),Tn9EvDo0(1),Tn9FlwDdMin(1),
     >Tn9MasVpUp(1),
     *       Tn9MasVpDo(1),Tn9MasGa(1),Tn9LevMas(1),Tn9LevMask(1),
     >Tn9PpGa(1),
     *       Tn9PpVpUp(1),Tn9EeGa(1),Tn9EeVpUp(1),Tn9EeWt(1),
     *       Tn9EeVpDo(1),Tn9LevRelax(1),Tn9VsGa(1),Tn9VsVpUp(1),
     *       Tn9VsWt(1),Tn9VsVpDo(1),Tn9EeWtSatWt(1),Tn9EeVpSatWt(1),
     *       Tn9RoGa(1),Tn9ROVpUp(1),Tn9RoVpDo(1),Tn9LevRel(1),Tn9Hi(1),
     *       Tn9Volk(1,Tn9Ne),  ! ������ ��� �� ��������
     *       Tn9VsWtUp(1),Tn9VsWtJa(1),Tn9Ee(1,1),Tn9TtGa(1),
     *       Tn9TtVpUp(1),Tn9TtWt(1),Tn9TtVpDo(1),
     *       Tn9Mask(1,1),Tn9CcGas(1,1),
     *       Tn9CfMasGasMin(1)/7.5d-7/,
     *       Tn9CcUp(1,1),Tn9FlwHUp(1,1),Tn9FlwHDo(1,1),Tn9GSepSum(1),
     *       Tn9GFlwWtSum(1),Tn9MasGaRealk(1),Tn9dEInt(1,1),
     *       Tn9GCorr(1,1),Tn9drdp(1,1),Tn9drdh(1,1),Tn9vk(1,1),
     *       Tn9Waste_T(20,1),Tn9Dmint(1),Tn9MasWtUp(1),Tn9MasWtJa(1),
     >Tn9RoDo(1),
     *       Tn9WlUpQqOut(1),Tn9WlDoQqOut(1),Tn9TcwAlCulc(1,1),
     *       Tn9TtWlUp(1),Tn9TtWlDo(1),Tn9LaWl(1),
     *       Tn9QqWlEnvUp(1),Tn9QqWlEnvDo(1),Tn9RoomUpInd(1),
     >Tn9RoomDoInd(1),
     *       Tn9CcVpDo(1),Tn9pmax(1),Tn9TtUp(1),Tn9AlSqPhWl(6,1),
     >Tn9AlWlUpDo(1),
     *       Tn9TtSatWt_(1),Tn9Levadd(1),Tn9vol(1),Tn9VolCorrUp(1),
     >Tn9VolCorrDo(1),
     *       Tn9PressCorr(1),Tn9VolGa(1),Tn9FlwIntCoef(1),
     >Tn9VolVpUpLim(1),
     *       Tn9LevTunG(1),Tn9LevTunPpRegG(6,1)

      integer*4 Tn9FlwNum(1)
      logical*1 Tn9CcBorLb(1),Tn9VpInFlag(1),Tn9BorFl(1),
     >Tn9EntOut_Key(1),
     *          Tn9FlagGCorr(1),Tn9FlagGCorrDn(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4787 

C     common/gr_parC     common/gr_parC     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4931 


****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
      integer*4 Ntur1,Tr1Lin(1),tr2RLinTyp(1)
      parameter(ntur1=0)
      real*8    tr2FlwKsi1(1)
      real*8 Tr1Oin(1),Tr1Qsd(1),Tr1NdP(1),Tr1genTyp(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4943 
      integer*4 Ntur3
      parameter(Ntur3=0)
         integer*4 Tr3beaTyp(1),Tr3beaInd(1)
      real*8    Tr3Omdl(1),Tr3Qsd(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4949 
      integer*4 Ntur4
      parameter(Ntur4=0)
         integer*4 Tr4beaTyp(1),Tr4beaInd(1)
      real*8    Tr4Omdl(1),Tr4Qsd(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4955 



****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'

      real*8 C1serv(nwall) !����. ��� ������� ������. ������� ���
      real*8 C2serv(nwall) !����������� ���� � ���. ��. ����� ������
      real*8 wl1_QqOut(nwall)  !��������� �������� ����� � ������� ��������
      real*8 wl1AlCulcWl(nwall)   !��������� �������� ����� � ������� ��������
      real*8 wl1_Tt_t(nwall)
      real*8 wl1_XQ(nwall),wl1_BQ(nwall)  ! �-�� � ������� ���������
      common/gr_parWALL1/ C1serv,C2serv,wl1_QqOut,wl1AlCulcWl,
     *  wl1_Tt_t,wl1_XQ,wl1_BQ
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4975 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4989 

      integer*4 NRm1
      parameter ( NRm1=0 )
      real*8 Rm1Qq(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 4995 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5015 

      integer*4 Nwall2
      parameter ( Nwall2=0 )
      real*8 wl2_QqOut(1)  !��������� �������� ����� � ������� ��������
      real*8 Wl2Tt_t(1)
      real*8 WL2mass(1),WL2Cp(1),WL2Tt(1)
      real*8 Wl2LamdC(1)
      integer*4 Wl2IndRoom(1),Wl2Ndet(1),Wl2Node(1)
      real*8 Wl2QqEnv(1),Wl2AlCulcWl2(1)
      real*8 Wl2Leff(1)
      real*8 Wl2Rb(1)
      real*8 Wl2Raw(1), Wl2SqEnv(1), Wl2QqBout(1), Wl2DOut(1)
      real*8 Wl2_XQ(1),Wl2_BQ(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5030 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      integer*4 Nwlcon2
      parameter ( Nwlcon2=0 )
      integer*4 WLC2Iwl(1)
      real*8 WLC2_Qq(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5039 




****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      real*4 dtInt1
      call SYBMQNEW(dtInt1,nv,nu,nf,n03,n07,n08,n09,
     *           InpMass,InpMass0,
     *           IFROkz,ITONkz,LinIBNFro,LinIBNto,
     *           FlwGgWv,FlwGgGa,GZ,
     *           NodPp,NodTyp,NodFreez,NodGgSum,NodGgSum_Wv,
     *           NodGgSum_Ga,NodGgSumZeroIn,NodGgSumZero,NodGgComp,
     *           NodGgCompGas,NodGgCompCorGa,NodGgCompWvp,
     *           NodGgCompCorWv,Volukz0,NodRo,NodRotrue,Nodrotrue0,
     *           NodRoGa,nodvvga,nodccga,nodvvgat,
     *           Masdt,Masdt_Ga,Masdt_Wv,
     *           Volukzt,Rozt,nodccgat,NodMasWvMaxTmp,NodMasWvMax,
     *           NodMasGaMaxTmp,NodMasGaMax,Nodtyptnk,NodCfTime,
     *           Volukz,NodGgSumIn,NodCfGgComp,Nod_Flag_Mas_Deb,
     *           MasdtIntegr_Wv,MasdtIntegr_Ga,MasdtIntegr,
     *           Tn3MasGa,Tn3MasWt,Tn3MasWpUp,Tn3MasWpDo,Tn3dMInt,
     *           Tn3GCorr,
     *           Tn7MasWt,Tn7MasGa,Tn7dMIntGa,Tn7dMIntWt,Tn7GCorrGa,
     *           Tn7GCorrWt,
     *           Tn8Gsliv,Tn8GgXx,Tn8MasWt,
     *           Tn9MasVpDo,Tn9MasWt,Tn9MasVpUp,Tn9MasWtUp,Tn9MasWtJa,
     *           Tn9dMInt,Tn9GCorr,
     *           CoolerMass,GazMass,db_mass_max,
     *           NodCountMax,flag_Mas_deb_,alph1,nodalph1,
     *          flag_Mas_deb,flag_start,Maxiterint,ItIntStart,T_fist_in,
     *          flag_first,accelnew,accel,Maxiternew,Maxiter,Tn9MasGa,
     *          FirstStepLabel,CoolerMass0,CoolerMass00,CoolerMass11,
     *          GazMass0,KeyDel,Tn3dEInt,Tn9dEInt,NodTypCon,
     *          Maxiterint0(1),ItIntStart0(1),Nod_Flag_dM,NodRoMas,
     *          NodPropsKey,NodPropsKey1,NodePropsKey(1),NODFREEZPH,
     *          NUCH,CoalHConNodInd,CoalHNodMoistG,NodDbMasZero,
     *          NodalfaH,dbIntMassga,dbIntMassWv,dbIntMassdif,
     *          dCm2_sum,dCm_hist,dCmdt_Mean,step_num,DebMaxG,DebMaxWv,
     *          DebMaxFull,iDebMaxG,iDebMaxWv,tiDebMaxG,tiDebMaxWv,
     *          NodXxBal,GCorCf,iDebMaxSum,CoolerMassNod,pzt,
     *          ComGamFrom,ComGamTo,Tn3EvDo,Tn3LevRel,Tn3FlwDdMin,
     *          Tn9EvDo,Tn9LevRel,Tn9FlwDdMin,HFrom,HTo,Tn3FlagGCorr,
     *          Tn9FlagGCorr,Tn3FlagGCorrDn,Tn9FlagGCorrDn,
     *          Fast_Option_Deb,FlwDpPm,FlwDpPm0,NodtypDeb,
     *          Nod_flag_Mas_deb_tmp,nodcfppup_,nodcfppup_tmp,
     *          Tn3Hi,Tn9Hi,NodCfdPdt_loc,NodCfdPdt_loc1,NodTt,
     >CoolerType,
     *          Tn3LevTunG,Tn9LevTunG,Tn7LevTunG,Tn8LevTunG,
     *          Tn3LevTunPpRegG,Tn9LevTunPpRegG,Tn7LevTunPpRegG,
     *          NodPpMinWtDebCor)

                                                         


      return
      end
****** End   file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mqnew.fs'
****** Begin file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET42.fs'
      logical function gr_par_it(deltat)
      implicit none
      include 'gr_par.fh'
      REAL*8 FlwTnSqFrom(44)
      COMMON/gr_par_nowrt/ FlwTnSqFrom
      REAL*8 FlwTnSqTo(44)
      COMMON/gr_par_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromUp(44)
      COMMON/gr_par_nowrt/ FlwHTnFromUp
      REAL*8 FlwHTnFromDo(44)
      COMMON/gr_par_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(44)
      COMMON/gr_par_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(44)
      COMMON/gr_par_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tc2Dim_TMax
      PARAMETER (Tc2Dim_TMax=2)
      INTEGER*4 Tn3Dim_TMax
      PARAMETER (Tn3Dim_TMax=2)
      INTEGER*4 INVPERe(51)
      COMMON/gr_par_nowrt/ INVPERe
      INTEGER*4 NZSUBe(540)
      COMMON/gr_par_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(52)
      COMMON/gr_par_nowrt/ INZSUBe
      INTEGER*4 ILNZe(52)
      COMMON/gr_par_nowrt/ ILNZe
      INTEGER*4 INVPERp(43)
      COMMON/gr_par_nowrt/ INVPERp
      INTEGER*4 NZSUBp(324)
      COMMON/gr_par_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(44)
      COMMON/gr_par_nowrt/ INZSUBp
      INTEGER*4 ILNZp(44)
      COMMON/gr_par_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=129)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=65)
      INTEGER*4 Nent
      PARAMETER (Nent=51)
      INTEGER*4 Npc
      PARAMETER (Npc=86)
      INTEGER*4 Npres
      PARAMETER (Npres=43)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(43)
      COMMON/gr_par_nowrt/ PIntIter
      LOGICAL*1 EIntIter(51)
      COMMON/gr_par_nowrt/ EIntIter
      LOGICAL*1 PExtIter(43)
      COMMON/gr_par_nowrt/ PExtIter
      LOGICAL*1 EextIter(51)
      COMMON/gr_par_nowrt/ EextIter
      CHARACTER*16 ConFlOutNames(1)
      COMMON/gr_par_nowrt/ ConFlOutNames
      !DEC$PSECT/gr_par_NOWRT/ NoWRT
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5102 

****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
      integer*4 Ncomp,CompLin(1),CompOin(1),CompGenTyp(1)
      parameter(Ncomp=0)
      real*8 CompQq(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5111 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5116 


****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'

c      real*8 FlwCondRegul(nv)
c      common/gr_parFlwL/ FlwCondRegul
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5127 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5162 
      integer*4 nfwv
      parameter(nfwv=0)
      integer*4 FwvFloWw(1),FwvFloWv(1)
      real*8 FwvRoFrom1(1),FwvRoFrom2(1),FwvRoTo1(1),FwvRoTo2(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5168 
      integer*4 nfwv1
      parameter(nfwv1=0)
      integer*4 Fwv1Flow(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5173 
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5189 
* ���������� ��� ��������������� ��������� ����������
      real*8 C0He2(NHe2) !����. ��� ������� ����������� ���� ��
      real*8 C1He2(NHe2) !����. ��� ������� ����������� ���� ��
      real*8 C2He2(NHe2) !����. ��� ������� ����������� ���� ��
      real*8 DTHe2
      real*8 C0He2b1(NHe2),C1He2b1(NHe2),C2He2b1(NHe2)
      real*8 C0He2b2(NHe2),C1He2b2(NHe2),C2He2b2(NHe2)
      real*8 He2T1t(2,NHe2),He2T2t(2,NHe2),He2decQt(NHe2)
      real*8 He2vvGa(NHe2)
      common/gr_parHE2/ C0He2,C1He2,C2He2,DTHe2,
     * C0He2b1,C1He2b1,C2He2b1,C0He2b2,C1He2b2,C2He2b2,
     * He2T1t,He2T2t,He2decQt,He2vvGa
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5203 
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5214 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5221 

      integer*4 NMtl
      parameter ( NMtl=0 )
      real*8 MtlTt(1),MtlMass(1),MtlCp(1),MtlAlCulc(1)
      real*8 Mtl_QqOut(1)  !��������� �������� ����� � ������� ��������
      real*8 MtlLamdC(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5229 

      integer*4 NMtlO,MtlOIndRoom(1)
      parameter ( NMtlO=0 )
      real*8 MtlOTt(1),MtlOMass(1),MtlOCp(1),MtlOAlCulcWl1(1)
      real*8 MtlO_QqOut(1)  !��������� �������� ����� � ������� ��������
      real*8 MtlOLamdC(1),MtlOLamd(1),MtlOQqEnv(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5237 
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'

      integer*4 NUCH
      parameter (NUCH=0)
      integer*4 CoalHConNodInd(1)
      real*8 CoalHNodMoistG(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5246 

      integer*4 NFlwWasteSet
      parameter(NFlwWasteSet=0)
      integer*4 FlwWasteSetConnInd(1),FlwWasteSetNum(1)
      real*8 FlwWasteSetWaste(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5253 

      integer*4 FlwConnFlwInd(1),NFlwConn
      parameter(NFlwConn=0)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5258 

      real*8 HC1PpFrom(NHC1),HC1PpTo(NHC1)
      common/gr_parConFlOut/ HC1PpFrom,HC1PpTo
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5263 

      integer*4 NHCO
      parameter(NHCO=0)
      integer*4  HCOFlMod(1),HCOIndFlw(1)
      real*8 HCOCfMod(1),HCOOFg(1),HCOONodVol(1),HCOONoddRodP(1)
      common/gr_parConFlOut/ HCOFlMod,HCOCfMod,HCOOFg,HCOONodVol,
     >HCOONoddRodP,HCOIndFlw
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5271 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5278 

      integer*4 nCrv
      parameter(nCrv=0)
      real*8 CrvRes(1)
      integer*4 CrvFlw(nCrv)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5285 

      integer*4 nWst
      parameter(nWst=0)
      integer*4 WstNode(1)
      real*8    WSTConsN2(1),WSTConsO2(1),WSTConsH2O(1),
     *          WSTConsCO2(1),WSTConsSoot(1),WSTConsAsh(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5293 

      integer*4 NXS
      parameter(NXS=0)
      integer*4 XSFlagSetCc(1),XSFlSet(30,1),XSIndSetCc(1),
     >XSIndSetCc0(1)
      real*8    XSCcstr(30,1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5300 

      integer*4 ntFrB
      parameter(ntFrB=0)
      integer*4 tFrBNdInd(1),tFrBi(1)
      real*8    tFrBCcOld(100,1),tFrBTau(1)
      real*8    tFrBGIn(1),tFrBAa(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5308 

      integer*4 np4
      parameter(np4=0)
      integer*4 Pm4Nb(1)
      real*8 Pm4Res(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5315 

c ��������� �����. ��� ������� � ���������������, ���� �� (�������) ���
      integer*4 np
      parameter(np=0)
      integer*4 VNKZ(1)
      real*8 PumRes(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5323 
      integer*4 np2
      parameter(np2=0)
      integer*4 Pm2Nb(1)
      real*8 Pm2Res(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5329 
      integer*4 nFan
      parameter(nFan=0)
      integer*4 FanFlw(1)
      real*8 FanRes(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5335 
      integer*4 nFan2
      parameter(nFan2=0)
      integer*4 Fan2Flw(1)
      real*8 Fan2Res(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5341 
      integer*4 np3
      parameter(np3=0)
      real*8 Pm3QqNod(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5346 
      integer*4 nfr1
      parameter(nfr1=0)
      integer*4 Fr1Flw(1)
      real*8 Fr1Bor(1)
      real*8 Fr1KkFullWst(1)
      real*8 Fr1Waste_T(100,1)
      real*8 Fr1WstMass_T(100,1)
      integer*4 Fr1SumMas_0(1),Fr1FlSumMas(1),Fr1F0SumMas(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5356 

      real*8 time_df1  !������� ��� ������� ������� (� �����)
      real*8 time_tmp

      real   deltat      !��� ����������
      real   deltat1      !��� ����������
      REAL*8 Atau        !1/dt
c      save Atau
      real*4 dtInt       !dt*Accel
      real*8 TimeSrv     !dt/Niter
      character*100 fn1  ! ��� ������������� .cbf ������
c      common/gr_parLocal1/ time_tmp,deltat,Atau,dtInt,TimeSrv,fn1
      common/gr_parLocal1/ Atau,time_tmp,deltat1,dtInt,TimeSrv,fn1
      real*8 VOL1(nu) !V/dt
      real*8 NodCfdE(nu) !����. � ��. ����. ��� ����� ���������
      real*8 NodTtk(nu)  !����������� ��������. � ������� ����� �� k-�� ��������
      real*8 RotVol(nu) !��������� ������
      real*8 RokVol(nu) !��������� ������
      real*8 NodVolk(nu)  !������������ ����� �� k-�� ��������
      real*8 NoddVdPk(nu) !����������� ������ � ���� �� �������� �� k-�� ����.

c ---  ������ �. (29.08.02) - ������
      real*8 NodSum1(nu)
      real*8 NodSum2(nu)
c ---  ������ �. (29.08.02) - �����

      real*8 NodQqPmAd(nu)

      common/gr_parNODE1/ VOL1,NodCfdE,NodTtk,RotVol,RokVol,
     * NodVolk,NoddVdPk,NodSum1,NodSum2,NodQqPmAd
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5388 

      real*8 C7(nv)   !��������� ������������
      real*8 Gk(nv)   !������� �� k-�� ��������
      real*8 dHLev(nv)!������ �� ����� �� ���� ������� � �������
      real*8 Rovk(nv)    !��. ��������� �� k-�� ��������
      real*8 AconRovk(nv)!��������� ��� ��������
c###### ��������� ####################################
      real*8 FlwGaVeSq(nv)  ! ��������� ������ ��� �������� ������������� �����
      real*8 flwbcon(nv)    ! �������� ��������� �������� �������� �� ����� 
      real*8 FlwC7work1(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work2(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work3(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work4(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work5(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work7(nv)
      real*8 FlwVsk    (nv)   ! �������� �� ����� �� ���������
      real*8 ComEeFrom  (nv)   ! ��������� ��������� ��������� � ��������
      real*8 ComEeTo    (nv)   ! ��������� ��������� ��������� � ��������  
      real*8 FlwSlGaFrom(nv)   ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaFrom0(nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaTo   (nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaTo0  (nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwCcFrom(nv) ! ��������� ������ ��� ������������ ���� �� ������ �����
      real*8 FlwCcTo(nv)   ! ��������� ������ ��� ������������ ���� �� ������ �����
      real*8 flwdhz0(nv)
      real*8 ComRoFrom(3,nv)
      real*8 ComRoTo  (3,nv)
c###### ��������� ####################################
      real*8 FlwKgp(nf,2,nv)    ! 
      real*8 FlwKge(nf,nf,nv)
      real*8 FlwKgg(3,nv)
      real*8 FlwKgpt(2,nv)
      real*8 FlwdPLevFrom(nv),FlwdPLevTo(nv)
      real*8 FlwCcGgWtJa(nv)
      real*8 FlwRes_Iter(nv)
      real*8 FlwGG_TMP(nv)
      real*8 FlwRes_Tmp
      real*8 FlwdCgDCcFrom(nv),FlwdCgDCcTo(nv)
c      real*8 frco0(nv)
      common/gr_parFLOW/ C7,Gk,dHLev,Rovk,AconRovk,FlwGaVeSq,
     * flwbcon,FlwC7work1,
     * FlwC7work2,FlwC7work3,FlwC7work4,FlwC7work5,FlwC7work7,
     * FlwVsk,ComEeFrom,ComEeTo,FlwSlGaFrom,FlwSlGaFrom0,
     * FlwSlGaTo,FlwSlGaTo0,FlwCcFrom,FlwCcTo,flwdhz0,ComRoFrom,
     * ComRoTo,FlwKgp,FlwKge,FlwKgg,FlwKgpt,
     * FlwdPLevFrom,FlwdPLevTo,FlwCcGgWtJa,
     * FlwRes_Iter,FlwRes_Tmp,FlwGG_TMP,FlwdCgDCcFrom,FlwdCgDCcTo
c    * ,frco0
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5438 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5443 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5448 


c      !������� ����. ����� ����� � ���. ��������� ��� ��������
       real*8 Dmp(Npres,Npres)
C       real*8 Dmpc(Npc,Npc) ! �������� 
c      !������� ����. ����� ����� � ������� ��������� ��� ���������
       real*8 Dmh(Nent,Nent)
       real*8 DmhG(Nent,Nent)
c      !������ ����� ��� ���������� ������� �� ����.
      Real*8 Yp(Npres)
c      Real*8 Ypc(Npc)
c      !������ ����� ��� ���������� ������� �� ���.
      real*8 Ye(Nent)
      real*8 YeG(Nent)
c      !������� �� ���������
      real*8 dP(Npres)
c      real*8 dPC(Npc)
c      !������� �� ����������
      real*8 dE(Nent)
      real*8 dEG(Nent)
c      !������ ������ �������� �� k-�� ��������
      real*8 Pk(Npres)
c      !������ ������ �������� �� (k+1)-�� ��������
      real*8 Pk1(Npres)
c      !������ ������ ��������� �� k-�� ��������
      real*8 Ek(Nent)
      real*8 EGk(Nent)
c      !������ ������ ��������� �� (k+1)-�� ��������
      real*8 Ek1(Nent)
      real*8 EGk1(Nent)
      real*8 Rok(Nu) !�����. �\� ����� � ������� ����� �� k-�� ����.
      !������ ������ ��������� �����. ����
      real*8 EeWtSatk(Nent)
      !������ ������ ��������� �����. ����
      real*8 EeVpSatk(Nent)
      real*8 MaxDevP     !����. �����. ����. �������� �� ��������
      real*8 MaxDevH     !����. �����. ����. ��������� �� ��������
      INTEGER*4 I,j     !���������� �����
      integer*4 IC        !������� ��������
      integer*4 ICInt !������� ���������� ��������
      common/gr_parLocal2/ Dmp,Dmh,Yp,Ye,dP,dE,Pk,Pk1,Ek,Ek1,Rok,
     *  EeWtSatk,EeVpSatk,MaxDevP,MaxDevH,IC,ICInt,DmhG,YeG,dEG,
     *  EGk,EGk1       !,dPC,Ypc,Dmpc <=������ ��������!!!!

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5496 

      INTEGER*4 nvlgpos !����� ����� � �������������� ���������� � ����������
      common/gr_parVlvGeo/ nvlgpos
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5501 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5506 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5511 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5516 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5521 

      integer*4 nsep1,Sep1FlwS(1),Sep1Node(1)
        parameter(nsep1=0)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5526 

      integer*4 nInWst,InWstNum(1),InWstNode(1),InWstNdTp(1)
      real*8    InWstdt(1)
      integer*4 InWstTyp(1)
      real*8    InWstCc(1)
      real*8 InWstExtCon(1),InWstExtWstdt(1)
      parameter(nInWst=0)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5535 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5540 


      LOGICAL*1 go,goint  !������� ����������� ��������
      DATA go/.true./     !����������� �������� �������. ��������
      common/gr_parLocal4/ go,goint
      real*8  RDTSC
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5579 
      real*8 NoddRodPk(nu) !dRo/dP ��� ������� ����� �� ����. ����.
      real*8 NoddRodEk(nu) !dRo/d� ��� ������� ����� �� ����. ����.
      real*8 NoddRodPk1(nu) !dRo/dP ��� ������� ����� �� ���. ����.
      real*8 NoddRodEk1(nu) !dRo/d� ��� ������� ����� �� ���. ����.
      real*8 nodrorelax(nu)
      real*8 NodRoGa0(nu)
      real*8 NodCcGa_Aeq0(nu) 
      real*8 NodCcGa_Deq0(nu) 
      real*8 NodCcGa_Beq (nu)
      real*8 NodCcGa_Beq0(nu)
      real*8 NodCcGa_Aeq (nu)
      real*8 NodCcGa_Deq (nu)
      real*8 NodXxk1(nu)    !���. �������. � ������� ����� �� ���. ����.
      real*8 NodXxBalk1(nu) !���. �������. � ������� ����� �� ���. ����.
      real*8 NodQqAd(nu)
      real*8 NodCfEeAsh(nu)
      real*8 NodEeWvt(nu)
      real*8 NodAlSqGaWv(nu),NodYe(nu),NodDmh(nu)
      real*8 NodPsw(nu),NodRoWvTmp(nu),NoddRdHGas(nu)
     *,NoddRdPWvTmp(nu),NoddRdHWvTmp(nu)
     *,NodTtWvTmp(nu),NodCpWvTmp(nu),NodXxTmp(nu)
      integer*4 Nod_ghm_flag(nu),NodPropRelaxFlag(nu)
      logical*1 EExtIter_(Nent)
      logical*1 PExtIter_(Npres)
      common /gr_parNODE/ NoddRodPk,NoddRodEk,NoddRodPk1,NoddRodEk1,
     *  nodrorelax,NodRoGa0,NodCcGa_Aeq0,NodCcGa_Deq0,NodCcGa_Beq,
     *  NodCcGa_Beq0,NodCcGa_Aeq,NodCcGa_Deq,NodXxk1,NodXxBalk1,
     *  NodQqAd,NodCfEeAsh,NodEeWvt,NodAlSqGaWv,NodYe,NodDmh,
     *  NodPsw,NodRoWvTmp,NoddRdHGas,NodTtWvTmp,NodCpWvTmp,NodXxTmp
      common /gr_parNODE_I/ Nod_ghm_flag,NodPropRelaxFlag
      common /gr_parNODE_L/ EExtIter_,PExtIter_
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5612 
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'

      integer NTc0
      parameter(NTc0=0)
      integer*4 Tc0IbnFro(1),Tc0NodFro(1),Tc0IbnTo(1),Tc0NodTo(1)
      real*8 Tc0zt(1),Tc0AlG23(1),Tc0AlG43(1)
     *,Tc0G43(1),Tc0G23(1),Tc0G34(1),Tc0CpWl(1)
     *,Tc0TtEnv(1),Tc0TtWlk(1),Tc0TtWl(1),Tc0AlSqEnv(1),Tc0AlEnv(1)
     *,Tc0AlSqPhWl(1,nf),Tc0MasCpWl(1),Tc0CfAlEnv(1)
     *,Tc0CfAlGaWl(1),Tc0CfAlWpUpWl(1)
     *,Tc0CfAlWtWl(1),Tc0CfAlWpDoWl(1)
     *,Tc0EvUpWl(1),Tc0EvDoWl(1)
     *,Tc0dG23dp(1),Tc0dG34dp(1),Tc0dG43dp(1)
     *,Tc0CfGgWpUpWt(1),Tc0CcGgWtWpDo(1),Tc0CfGgWpDoWt(1)
     *,Tc0G32(1),Tc0dG32dp(1),Tc0CfGgWt(1),Tc0SqWl(1),Tc0TettaQ(1,nf)
     *,Tc0QqIsp(1),Tc0AlIsp(1)
     *,Tc0dQIspdP(1),Tc0TtTnIsp(1),Tc0AlSqIsp(1),TC0ResWl(1)
     *,Tc0dQG23dp(1),Tc0TettaQ2(1,nf),Tc0DeltaTs(1)
     *,Tc0AlG23From(1),Tc0AlG43From(1)
     *,Tc0G23From(1),Tc0G34From(1),Tc0G32From(1),Tc0G43From(1)
     *,Tc0dG23dpFrom(1),Tc0dG34dpFrom(1),Tc0dG43dpFrom(1),
     >Tc0dG32dpFrom(1)
     *,Tc0dQIspdPFrom(1),Tc0TtTnIspFrom(1),Tc0AlSqIspFrom(1)
     *,Tc0CcGgWtWpDoFrom(1),Tc0CfGgWpUpWtFrom(1),Tc0CfGgWpDoWtFrom(1)
     *,Tc0QqIspFrom(1),Tc0AlIspFrom(1),Tc0CfGgWtFrom(1),Tc0TettaQFrom(1,
     >nf)
     *,Tc0AlSqPhWlFrom(1,nf),Tc0CfRelAlIsp(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5642 
C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5671 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5694 
      real*8 Tc2zt(NTc2),Tc2AlG23(NTc2),Tc2G23(NTc2),Tc2G34(NTc2)
     *,Tc2AlG43(NTc2),Tc2G43(NTc2),Tc2TtWlk(NTc2),Tc2AlPhWl(NTc2,nf)
     *,Tc2AlSqPhWl(NTc2,nf),Tc2dG23dp(NTc2),Tc2dG34dp(NTc2)
     *,Tc2dG43dp(NTc2),Tc2G32(Ntc2),Tc2dG32dp(Ntc2)
c     *,Tc2TettaQ(NTc2,nf)
     *,Tc2dQIspdP(NTc2),Tc2TtTnIsp(NTc2),Tc2AlSqIsp(NTc2)
      real*8 Tc2AlEnv_P(NTc2)
      common/gr_parTC2/ Tc2zt,Tc2AlG23,Tc2G23,Tc2G34
     *,Tc2AlG43,Tc2G43,Tc2TtWlk,Tc2AlPhWl
     *,Tc2AlSqPhWl,Tc2dG23dp,Tc2dG34dp,Tc2dG43dp,Tc2G32,Tc2dG32dp
c     *,Tc2TettaQ
     *,Tc2dQIspdP,Tc2TtTnIsp,Tc2AlSqIsp,Tc2AlEnv_P
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5708 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5718 

      integer*4 NTCR,TcRNode(1)
      parameter ( NTCR=0 )
      real*8 TcR_XQ(1),TcR_BQ(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5724 

      integer*4 Nwall2O
      parameter ( Nwall2O=0 )
      real*8  Wl2OTt(6,1),Wl2ORAw(1),Wl2OSqEnv(1),Wl2OQqBout(1),
     &        Wl2ODout(1)

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5732 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5738 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'

      integer NTen
      parameter(NTen=0)
      integer*4 TenIbn(1),TenNode(1)
      real*8 Tenzt(1),TenQqIn(1),TenAlG23(1),TenG23(1),TenG34(1)
     *,TenAlG43(1),TenG43(1),TenCpWl(1),TenTtWlk(1),TenTtWl(1)
     *,TenAlSqPhWl(1,nf),TenMasCpWl(1),TenCfAlGaWl(1),TenCfAlWpUpWl(1)
     *,TenCfAlWtWl(1),TenCfAlWpDoWl(1),TenEvUpWl(1),TenEvDoWl(1)
     *,TendG23dp(1),TendG34dp(1),TendG43dp(1)
     *,TenCfGgWpUpWt(1),TenCcGgWtWpDo(1),TenCfGgWpDoWt(1)
     *,TenG32(1),TendG32dp(1),TenCfGgWt(1),TenSqWl(1),TenTettaQ(1,nf)
     *,TenQqIsp(1),TenAlIsp(1),TenDeltaTs(1)
     *,TendQIspdP(1),TenTtTnIsp(1),TenAlSqIsp(1),TenResWl(1),
     >TenTcwAlCulc(1),TenTcwQq(1)
     *,TenLawL(1),TenQqInt(1),TenQqExt(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5761 
C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5775 
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5813 

c   ��������� ���������� ��� "����� �������"
*--- ����� ���������� ����������
      real*8 Tn3Pp(n03,Tn3Ne)    ! �������
      real*8 Tn3Ee(n03,Tn3Ne)    ! ���������
      real*8 Tn3Tt(n03,Tn3Ne)    ! �����������
      real*8 Tn3TtWl(n03,2)  ! ����������� ������
      real*8 Tn3TtEnv(n03,2) ! ����������� � ���������� ����������
      real*8 Tn3Mas(n03,Tn3Ne)   ! ����� ���
      real*8 Tn3rot(n03,Tn3Ne)   ! ��������� ���
      real*8 Tn3Volt(n03,Tn3Ne)  ! ������ ���
      real*8 Tn3Voldt(Tn3Ne,n03) ! ������ ���/dt
      real*8 Tn3MasGas(n03,3)! ����� ������������ �����
*---

      real*8 Tn3rok(n03,Tn3Ne)   ! ��������� �� ��������
      real*8 Tn3pk (n03,Tn3Ne)   ! �������� �� ��������
      real*8 Tn3ek (n03,Tn3Ne)   ! �������� �� ��������
      real*8 Tn3hw (n03,Tn3Ne)   ! ��������� ��������� ���� �� ��������
      real*8 Tn3hs (n03,Tn3Ne)   ! ��������� ��������� ���� �� ��������
      real*8 Tn3RoWtSatk(n03,Tn3Ne)   ! ��������� ��������� ���� �� ��������
      real*8 Tn3cp (n03,Tn3Ne)   ! ������������
      real*8 Tn3Volk(n03,Tn3Ne)  ! ������ ��� �� ��������
      real*8 Tn3TtWlk(n03,2) ! ����������� ������ �� ��������
      real*8 Tn3Mask(Tn3Ne,n03)  ! ����� ��� �� ���������
      real*8 Tn3MasGask(3,n03) ! ����� ������������ ����� �� ���������
      real*8 Tn3Volkdt(Tn3Ne,n03) ! ������ ���/dt �� ��������

      real*8 Tn3AlSqPhPh(Tn3Ne,Tn3Ne,n03) ! alfa*S ����� ������
      real*8 Tn3SqPhPh(Tn3Ne,Tn3Ne,n03) ! S ����� ������
      real*8 Tn3AlPhPh(Tn3Ne,Tn3Ne,n03) ! �-�� ������������� ����� ����� ������
      real*8 Tn3AlSqPhWl(n03,Tn3Ne)   ! �-�� ��������. ����� ������ � �������
      real*8 Tn3AlWlUpDo(n03)
      !����. ������� ��������. ����� ������ ����� ������ � ����� ������
      real*8 Tn3ActWlUpDo(n03)
      !�-�� ������������� ����� ������� � ����������
      real*8 Tn3AlSqWlEnv(n03,2)
      real*8 Tn3MasCpWl(n03,2)    ! �����*Cp ������

      real*8 Tn3drdp(n03,Tn3Ne)
      real*8 Tn3drdh(n03,Tn3Ne)
      real*8 Tn3dh(n03,Tn3Ne)

      real*8 Tn3G(Tn3Ne,Tn3Ne,n03) 
      real*8 Tn3dGdP(Tn3Ne,Tn3Ne,n03),Tn3dg32dp2(n03),Tn3dg32dp3(n03)
      real*8 Tn3Gw23(n03) 
      real*8 Tn3dGw23dP(n03) 
      real*8 Tn3Gw34(n03) 
      real*8 Tn3dGw34dP(n03) 
      real*8 Tn3Gw43(n03) 
      real*8 Tn3dGw43dP(n03) 
      real*8 Tn3Gw32(n03) 
      real*8 Tn3dGw32dP(n03) 
      real*8 Tn3G32Sum(n03)
      real*8 Tn3G34Sum(n03)
      real*8 Tn3G23Sum(n03)
      real*8 Tn3G43Sum(n03)

      real*8 Tn3G0(Tn3Ne,Tn3Ne,n03)
      real*8 Tn3CcGas(3,n03)
      real*8 Tn3AlGw23(n03)
      real*8 Tn3AlGw34(n03)
      real*8 Tn3AlGw43(n03)
      real*8 Tn3zd(n03)
      real*8 Tn3zu(n03)
      real*8 Tn3det(n03)
      real*8 Tn3zt(n03)
      real*8 Tn3Ts(n03,Tn3Ne)
      real*8 Tn3Ts_(n03,Tn3Ne)
      real*8 Tn3rv(Tn3Ne,n03)
      real*8 Tn3GSum(Tn3Ne,n03)
      real*8 Tn3MasgenMult(Tn3Ne,n03)
      real*8 Tn3Masgen(Tn3Ne,n03)
      real*8 Tn3AlGv23(n03)
      real*8 Tn3Gv23(n03)
      real*8 Tn3dGv23dP(n03)
 
      real*8 Tn3Pk2(n03,Tn3Ne)
      real*8 Tn3hw2(n03,Tn3Ne)
      real*8 Tn3hs2(n03,Tn3Ne)
      real*8 Tn3ts2(n03,Tn3Ne)
      real*8 Tn3ts2_(n03,Tn3Ne)
      real*8 Tn3Tt2(n03,Tn3Ne)
      real*8 Tn3ro2(n03,Tn3Ne)
      real*8 Tn3cp2(n03,Tn3Ne)
      real*8 Tn3hd2(n03,Tn3Ne)
      real*8 Tn3CcUp2(2,n03)
      real*8 Tn3Xxk(n03,Tn3Ne)
      logical*1 Tn3RelaxKG
      real*8 Tn3dp(n03,Tn3Ne)
      real*8 Tn3Mas0(n03,Tn3Ne)
c      real*8 Tn3CfMasGasMin ! /1.0d-7/
c      parameter (Tn3CfMasGasMin=1.0d-7)
      real*8 Tn3TettaQ(n03,Tn3Ne)
      real*8 Tn3Vk(n03,Tn3Ne)
      real*8 Tn3dTdH(n03,Tn3Ne)
      real*8 Tn3CcUp(2,n03)
      real*8 Tn3AVolDo(n03)
      real*8 Tn3TettaWl(n03)
      real*8 Tn3LevRelk(n03),Tn3LevMask(n03)
      real*8 Tn3QqPhPh(Tn3Ne,Tn3Ne,n03),Tn3QqGen(Tn3Ne,Tn3Ne,n03)
        real*8 Tn3y1(n03),Tn3y2(n03),Tn3z1(n03),Tn3z2(n03)
      real*8 Tn3MasGaRealk(n03),Tn3MasGaReal0(n03)
      real*8 Tn3TcwAlCulc(n03,2)
      real*8 Tn3La(n03,Tn3Ne)
      real*8 Tn3Cf42(n03)
      real*8 Tn3WlTtIsp(n03)
      real*8 Tn3HD(n03,Tn3Ne),Tn3Sigma(n03),Tn3Vel42(n03)
     *,Tn3Dd4(n03)
      real*8 Tn3FlwHUp(1,1)
      real*8 Tn3FlwHDo(1,1)
      real*8 Tn3VolNorm0(n03)
      real*8 Tn3dMLoc_tmp(Tn3Ne,n03)
      real*8 Tn3VolFull(n03)
         real*8 Tn3Pk_dev(n03)

      common/gr_parTn3/
     * Tn3Pp,Tn3Ee,Tn3Tt,Tn3TtWl,Tn3TtEnv,Tn3Mas,Tn3rot,Tn3Volt,Tn3Voldt
     * ,Tn3MasGas,Tn3rok,Tn3pk,Tn3ek,Tn3hw,Tn3hs,Tn3RoWtSatk,Tn3cp
     * ,Tn3Volk,Tn3TtWlk,Tn3Mask,Tn3MasGask,Tn3Volkdt,Tn3AlSqPhPh
     * ,Tn3SqPhPh,Tn3AlPhPh,Tn3AlSqPhWl,Tn3AlWlUpDo,Tn3ActWlUpDo
     * ,Tn3AlSqWlEnv,Tn3MasCpWl,Tn3drdp,Tn3drdh,Tn3dh,Tn3G,Tn3dGdP
     * ,Tn3dg32dp2,Tn3dg32dp3,Tn3Gw23,Tn3dGw23dP,Tn3Gw34,Tn3dGw34dP
     * ,Tn3Gw43,Tn3dGw43dP,Tn3Gw32,Tn3dGw32dP,Tn3G32Sum,Tn3G34Sum
     * ,Tn3G23Sum,Tn3G43Sum,Tn3G0,Tn3CcGas,Tn3AlGw23
     * ,Tn3AlGw34,Tn3AlGw43,Tn3zd,Tn3zu,Tn3det,Tn3zt,Tn3Ts,Tn3rv
     * ,Tn3GSum,Tn3MasgenMult,Tn3Masgen,Tn3AlGv23,Tn3Gv23,Tn3dGv23dP
     * ,Tn3Pk2,Tn3hw2,Tn3hs2,Tn3ts2,Tn3Tt2,Tn3ro2,Tn3cp2,Tn3hd2
     * ,Tn3CcUp2,Tn3Xxk,Tn3dp,Tn3Mas0,Tn3TettaQ,Tn3Vk,Tn3dTdH
     * ,Tn3CcUp,Tn3AVolDo,Tn3TettaWl,Tn3LevRelk,Tn3LevMask,Tn3QqPhPh
     * ,Tn3QqGen,Tn3y1,Tn3y2,Tn3z1,Tn3z2,Tn3MasGaRealk,Tn3MasGaReal0
     * ,Tn3TcwAlCulc,Tn3La,Tn3Cf42,Tn3WlTtIsp,Tn3HD,Tn3Sigma
     * ,Tn3Vel42,Tn3Dd4,Tn3FlwHUp,Tn3FlwHDo,Tn3VolNorm0,Tn3ts_,Tn3ts2_
     * ,Tn3dMLoc_tmp,Tn3VolFull,Tn3Pk_dev
      common/gr_parTn3L/ Tn3RelaxKG
      real*8 Tn3GSpCond(nv)
      real*8 Tn3QSpCond(nv)
      common/gr_parTn3Flow/ Tn3GSpCond,Tn3QSpCond
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5955 


****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'

      integer*4 n07,Tn7RelP(1,1),Tn7RelE(2,1),Tn7Homo(1),Tn7CcSetInd(1)
      real*8    Tn7RoGa(1),Tn7RoWt(1),Tn7Rok(1,2),Tn7XxWt(1),Tn7Xk(1,2),
     *          Tn7Lev(1),Tn7EeWt(1),Tn7VsGa(1),Tn7VsWt(1),Tn7PpGa(1),
     *          Tn7EeGa(1),Tn7EeWtSat(1),Tn7EeVpSat(1),Tn7LevRel(1),
     *          Tn7Hi(1),Tn7PpWt(1),Tn7TtWt(1),Tn7Ee(2,1),
     *          Tn7Ek(2,1),Tn7CcVolk(1),Tn7CcMask(1)
     *         ,Tn7XxBalWt(1),Tn7Levk(1),Tn7Lev_Rlx(1),Tn7Acon(1),
     *          Tn7Sq(1),Tn7CcBor(1),Tn7MasWt(1),Tn7MasGa(1),
     *          Tn7MasBor(1),Tn7GCorrWt(1),
     *          Tn7dRdPk(2,1),Tn7dRdP(2,1),Tn7dRdHk(2,1),Tn7dRdH(2,1),
     *          dP_Tn7(1),Tn7Dcon(1),Tn7LsMin(1),Tn7dMLoc(2,1),
     *          Tn7CfRoGaCorr(1),Tn7CfRoWtCorr(1),
     *          Tn7Waste_T(20,1),Tn7Rowttrue(1),Tn7RoWtSat(1),
     *          Tn7RoVpSat(1),Tn7Dmint(1),Tn7Gcorr(1),
     *          Tn7WlUpQqOut(1),Tn7WlDoQqOut(1),Tn7TcwAlCulc(1,1),
     *          Tn7TtWlUp(1),Tn7TtWlDo(1),Tn7LaWl(1),Tn7QqWlEnv(1),
     >Tn7RoomInd(1)
     *          ,Tn7EvDo(1),Tn7EvDo0(1),Tn7TtGa(1),Tn7AlSqPhWl(2,1),
     >Tn7AlWlUpDo(1)
     *          ,Tn7dMIntGa(1),Tn7dMIntWt(1),Tn7GCorrGa(1),
     >Tn7LevTunG(1),Tn7LevTunPpRegG(1)
      logical*1 Tn7_Flag_Mas_Deb(1),Tn7CfCorr_On(1)


      logical*1 Tn7CcBorLb(1)

      parameter(n07=0)
      real*8 Tn7Pk(2,1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 5989 

C     common/gr_parC     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6046 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
      integer n08
      parameter(n08=0)
      integer*4 Tn8RelP(1,0),Tn8RelE(2,0),Tn8CcSetInd(1)
      real*8 Tn8RoWt(1),Tn8RoGa(1),Tn8GSumWt1(1),Tn8GSumWt2(1),
     *       Tn8Lev(1),Tn8RoWtk(1),Tn8PpWt(1),Tn8MasBor(1),Tn8ccBor(1),
     *       Tn8VolWt(1),Tn8XxWt(1),Tn8XxBalWt(1),
     *       Tn8MasWt(1),Tn8MasWtk(1),
     *       Tn8RoGak(1),Tn8Gsliv(1),Tn8EeGa(1),
     *       Tn8EeWt(1),Tn8PpGa(1),Tn8VsWt(1),Tn8VsGa(1),Tn8VsVp(1),
     *       Tn8EeWtSat(1),Tn8EeVpSat(1),Tn8LevRel(1),Tn8Hi(1),
     *       Tn8TtGa(1),Tn8TtWt(1),Tn8GgXx(1),
     *       Tn8dRdP1(1),Tn8dRdH1(1),Tn8dRdP2(1),Tn8dRdH2(1),
     *       Tn8Waste_T(20,1),Tn8RowtSat(1),Tn8RovpSat(1),
     *       Tn8QqEnv(1),Tn8RoomInd(1),Tn8TtMeDo(1)
     *       ,Tn8EvDo(1),Tn8EvDo0(1),Tn8GInOut(1),Tn8LevTunG(1)

      logical*1 Tn8CcBorLb(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6070 

C     common/gr_parC     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6107 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'

      integer n09
      parameter(n09=0)
      integer*4 Tn9RelP(2,1),Tn9RelE(Tn9Ne,1),Tn9CcSetInd(1)
      real*8 Tn9RoWt(1),Tn9Cp(1,1),Tn9Lev(1),
     *       Tn9Ek(1,1),Tn9Rok(1,Tn9Ne),Tn9PpWt(1),Tn9Pk(1,1),Tn9Rot(1,
     >Tn9Ne),
     *       Tn9Xxk(1,Tn9Ne),Tn9MasBor(1),Tn9ccBor(1),Tn9VolWt(1),
     *       Tn9dp(1,Tn9Ne),Tn9MasWt(1),Tn9GIspSum(1),Tn9Pp(1),
     *       Tn9GCondSum(1),Tn9G32sum(1),Tn9G34sum(1),Tn9G23sum(1),
     *       Tn9G43sum(1),Tn9TettaQ(1,Tn9Ne),Tn9Ev_t(1,1),Tn9Dim_t(1),
     *       Tn9Dim_tmax/1/,Tn9EvDo(1),Tn9EvDo0(1),Tn9FlwDdMin(1),
     >Tn9MasVpUp(1),
     *       Tn9MasVpDo(1),Tn9MasGa(1),Tn9LevMas(1),Tn9LevMask(1),
     >Tn9PpGa(1),
     *       Tn9PpVpUp(1),Tn9EeGa(1),Tn9EeVpUp(1),Tn9EeWt(1),
     *       Tn9EeVpDo(1),Tn9LevRelax(1),Tn9VsGa(1),Tn9VsVpUp(1),
     *       Tn9VsWt(1),Tn9VsVpDo(1),Tn9EeWtSatWt(1),Tn9EeVpSatWt(1),
     *       Tn9RoGa(1),Tn9ROVpUp(1),Tn9RoVpDo(1),Tn9LevRel(1),Tn9Hi(1),
     *       Tn9Volk(1,Tn9Ne),  ! ������ ��� �� ��������
     *       Tn9VsWtUp(1),Tn9VsWtJa(1),Tn9Ee(1,1),Tn9TtGa(1),
     *       Tn9TtVpUp(1),Tn9TtWt(1),Tn9TtVpDo(1),
     *       Tn9Mask(1,1),Tn9CcGas(1,1),
     *       Tn9CfMasGasMin(1)/7.5d-7/,
     *       Tn9CcUp(1,1),Tn9FlwHUp(1,1),Tn9FlwHDo(1,1),Tn9GSepSum(1),
     *       Tn9GFlwWtSum(1),Tn9MasGaRealk(1),Tn9dEInt(1,1),
     *       Tn9GCorr(1,1),Tn9drdp(1,1),Tn9drdh(1,1),Tn9vk(1,1),
     *       Tn9Waste_T(20,1),Tn9Dmint(1),Tn9MasWtUp(1),Tn9MasWtJa(1),
     >Tn9RoDo(1),
     *       Tn9WlUpQqOut(1),Tn9WlDoQqOut(1),Tn9TcwAlCulc(1,1),
     *       Tn9TtWlUp(1),Tn9TtWlDo(1),Tn9LaWl(1),
     *       Tn9QqWlEnvUp(1),Tn9QqWlEnvDo(1),Tn9RoomUpInd(1),
     >Tn9RoomDoInd(1),
     *       Tn9CcVpDo(1),Tn9pmax(1),Tn9TtUp(1),Tn9AlSqPhWl(6,1),
     >Tn9AlWlUpDo(1),
     *       Tn9TtSatWt_(1),Tn9Levadd(1),Tn9vol(1),Tn9VolCorrUp(1),
     >Tn9VolCorrDo(1),
     *       Tn9PressCorr(1),Tn9VolGa(1),Tn9FlwIntCoef(1),
     >Tn9VolVpUpLim(1),
     *       Tn9LevTunG(1),Tn9LevTunPpRegG(6,1)

      integer*4 Tn9FlwNum(1)
      logical*1 Tn9CcBorLb(1),Tn9VpInFlag(1),Tn9BorFl(1),
     >Tn9EntOut_Key(1),
     *          Tn9FlagGCorr(1),Tn9FlagGCorrDn(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6150 

C     common/gr_parC     common/gr_parC     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6294 


****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
      integer*4 Ntur1,Tr1Lin(1),tr2RLinTyp(1)
      parameter(ntur1=0)
      real*8    tr2FlwKsi1(1)
      real*8 Tr1Oin(1),Tr1Qsd(1),Tr1NdP(1),Tr1genTyp(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6306 
      integer*4 Ntur3
      parameter(Ntur3=0)
         integer*4 Tr3beaTyp(1),Tr3beaInd(1)
      real*8    Tr3Omdl(1),Tr3Qsd(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6312 
      integer*4 Ntur4
      parameter(Ntur4=0)
         integer*4 Tr4beaTyp(1),Tr4beaInd(1)
      real*8    Tr4Omdl(1),Tr4Qsd(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6318 



****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'

      real*8 C1serv(nwall) !����. ��� ������� ������. ������� ���
      real*8 C2serv(nwall) !����������� ���� � ���. ��. ����� ������
      real*8 wl1_QqOut(nwall)  !��������� �������� ����� � ������� ��������
      real*8 wl1AlCulcWl(nwall)   !��������� �������� ����� � ������� ��������
      real*8 wl1_Tt_t(nwall)
      real*8 wl1_XQ(nwall),wl1_BQ(nwall)  ! �-�� � ������� ���������
      common/gr_parWALL1/ C1serv,C2serv,wl1_QqOut,wl1AlCulcWl,
     *  wl1_Tt_t,wl1_XQ,wl1_BQ
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6338 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6352 

      integer*4 NRm1
      parameter ( NRm1=0 )
      real*8 Rm1Qq(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6358 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6378 

      integer*4 Nwall2
      parameter ( Nwall2=0 )
      real*8 wl2_QqOut(1)  !��������� �������� ����� � ������� ��������
      real*8 Wl2Tt_t(1)
      real*8 WL2mass(1),WL2Cp(1),WL2Tt(1)
      real*8 Wl2LamdC(1)
      integer*4 Wl2IndRoom(1),Wl2Ndet(1),Wl2Node(1)
      real*8 Wl2QqEnv(1),Wl2AlCulcWl2(1)
      real*8 Wl2Leff(1)
      real*8 Wl2Rb(1)
      real*8 Wl2Raw(1), Wl2SqEnv(1), Wl2QqBout(1), Wl2DOut(1)
      real*8 Wl2_XQ(1),Wl2_BQ(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6393 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      integer*4 Nwlcon2
      parameter ( Nwlcon2=0 )
      integer*4 WLC2Iwl(1)
      real*8 WLC2_Qq(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6402 




****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      
      gr_par_it=.true. !�������, ��� ���� ���������� ��������

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6413 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6417 

      deltat=deltat1
      call gr_par_conInQ
c   ������ ��������
!      IC=0
!      go=.true.
!      DO while(go.and.(IC.lt.MaxIternew(1)))
c ���������� �� 1, ����������� ������ �� 0 ��������!!!!
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6434 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6439 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6445 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6479 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6513 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6522 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6528 
c
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6599 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6669 

      time_tmp = time_df1()
      call HydroCon_In(nv,NHC1,HC1IndGg,FlwType,Gk,
     *                 HC1Lb,HC1LbOld,HC1Type,FlwKeyVp,FlwWt,
     *                 ResCf1,FlwResCf,FlwRedGa,FlwLl,FlwDd,
     *                 FlwADd,FlwSq,FlwASq2,GzIni,ROVk,
     *                 FlwResTyp,FlwPipeType,Kv1,Kv,FlwRes0,
     *                 FlwRes,FlwCon,FlwRg,FlwVs,FlwSqEnv,
     *                 FlwCfKv,FlwCfCorKv,FlwLnSq,FlwResDel,
     *                 ConFlOutNames,
     *                 FlwDdExt,FlwThi,FlwSqE,HFrom,HTo,
     *                 FlwPipeType,FlwKForm,FlwResFric,FlwResAcc,
     *                 FlwResLoc,FlwFlowType,FlwResl,FlwBndLam,
     *                 FlwBndTur,FlwVlvFlIni,FlwResTur,FlwResLam,
     *                 FlwKsi1,FlwKsi2,FlwRo0,FlwCfResl,FlwSq2,
     *                 HC1Gg,HC1GgOld,HC1LbAlone,HC1FlagInOut,
     *                 FlwCfTime,IFROKZ,ITONKZ,
     *                 HC1HFrom,HC1HTo,
     *                 HC1FlwCfResDel,
     *                 FlagCon(1)*DpMaxFlag(1),HC1CfTime,0)
      tick(4,1) = time_df1()-time_tmp
      ErrorNum(1)=4
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6693 

      call FlwRes_new(nv,Gk,Rovk,FlwLl,
     &             FlwRg,FlwSq,FlwDd,FlwCon,
     &             FlwRes_Iter,FlwRes0,FlwResTyp,FlwResDel,
     &             FlwVs,FlwRoFrom,FlwRoTo,
     &             np,VNKZ,PumRes,
     &             np2,Pm2Nb,Pm2Res,     
     &             nFan,FanFlw,FanRes,
     &             nFan2,Fan2Flw,Fan2Res,
     &             np4,Pm4Nb,Pm4Res,     
     &             FlwSumRes,FlwResCf,FlwKeyVp,
     &             FlwXxBalFrom,FlwXxBalTo,
     &             nf,FlwRedGa,FlwADd,FlwASq2,
     &             Rov0,LbResRo
     &            ,FlwType,FlwRe,Flwltr,FlwKform
     &            ,FlwCfResL
     &            ,FlwResFric,FlwResAcc,FlwResLoc
     &            ,FlwdPResdG,FlwdltrdG
     &            ,FlwdPFricdG,FlwFlowType
     &            ,FlwdPLocdG
     &            ,FlwBndLam,FlwBndTur,FlwdPRes
     &            ,FlwResTur,FlwResLam,Flwksi2,Flwksi1
     &            ,GzIni,FlwRo0,FlwksiSAR
     &            ,LinIBNfro,LinIBNto      
     &            ,FlwSqMin
     &            ,ComGamFrom,ComGamTo
     &            ,IFROKZ,ITONKZ
     &            ,Ntur1,Tr1Lin,tr2RLinTyp
     &            ,FlwCfSum,Tr2Flwksi1,FlwRen)
      time_tmp = time_df1()
      call HydroCon_Dens(nv,NHC1,HC1Type,HC1Lb,Rovk,HC1Rov,
     *                    HC1IndGg,HC1LbAlone)
      tick(10,1) = time_df1()-time_tmp
      ErrorNum(1)=10
      call HydroCon_Res(nv,NHC1,HC1Type,HC1Lb,FlwRes_Iter,
     *                  FlwKsi1,HC1Con,HC1Ksi1,HC1IndGg,
     *                  HC1LbAlone)




      do i = 1,nv ! ������ �������������
       FlwRes_tmp = FlwRes(i)+FlwRes_Iter(i)
       if(FlwRes_tmp.le.1d-25) then
         FlwCon(i)=1d25
       else
         FlwCon(i)=1d0/FlwRes_tmp
       endif
      enddo
      do i=1,nfwv ! ��������� ������������� ��� ������� �������
        FlwCon(FWVFlowW(i))=FlwCon(FWVFlowW(i))*.25
        FlwCon(FWVFlowV(i))=FlwCon(FWVFlowV(i))*.25
      enddo

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6848 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6857 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 6863 

      ICint=0
      goint=.true.
      time_tmp = time_df1()
        call Node_Vol(nu,Npres,VOLUKZ,NodVolAl(1),NodVolPp0(1),
     *                Pk,NodRelP,NodVolk,NoddVdPk,NodVolPp1(1),
     *                NodFast(1))
      tick(46,1) = time_df1()-time_tmp
      ErrorNum(1)=46

      if(ic.lt.ItIntStart(1)) goint=.false.
      DO while(GoInt.and.(MaxIterInt(1).ne.0))
c --- ���������� �������� ---                         !Internal
      InternalIter(1)=.true.

      call Clr_Y(nv,FlwdPdGPm)
c      call Clr_Y(nv,FlwdPPm)
      FlwdPPm=FlwdPdop
      time_tmp = time_df1()
c        call correctkv(nv,LinIbnFro,LinIbnTo
c     *,ifrokz,itonkz,FlwCon,Tn3Volk,n03,FlwCfCorCon,FlwCon,Tn3Ne)
      tick(47,1) = time_df1()-time_tmp
      ErrorNum(1)=47
        if((SrvLb1(1).ne.0).and.(icint.ne.0)) then ! ������� �� �����������
      time_tmp = time_df1()
        call Node_GgSum(nu,nv,Rok,NodRoMasPos,Gk,IFROKZ,ITONKZ,
     *                  LinIbnFro,LinIbnTo,NodGgSum,NodGgSumIn,
     *                  dtInt,NodVolk,NodSum1,NodSum2,FlwC7work3,
     >FlwC7work4,
     *                  dRoMas,FlwC7work5,NodRoMas,FlwRoMas,ROVZ,
     *                  InternalIter,FlwType)
      tick(48,1) = time_df1()-time_tmp
      ErrorNum(1)=48
        endif
      time_tmp = time_df1()

**************************************************
      time_tmp = time_df1()
      call clr_FlwKge(FlwKge,nf,nv,LinIbnFro,LinIbnTo)
      tick(54,1) = time_df1()-time_tmp
      ErrorNum(1)=54
      time_tmp = time_df1()
      call Tn9_kG(Gk,Tn9Lev,FlwKgp,FlwKgg,FlwKge
     *,Tn9Mask,nv,ifrokz
     *,itonkz,n09,Tn9CcGas,LinIbnFro,LinIbnTo,nu
     *,NodBodyTyp,Tn9Pk,Tn9Rok
     *,Tn9LevMask,dtint,Tn9CcUp,FlwXxFrom,FlwXxTo,nf
     *,FlwHTnFromDo,FlwHTnFromUp,FlwHTnToDo,FlwHTnToUp
     *,Tn9LevRelax,Tn9EvDo
     *,Tn9Ne,n03,Tn3LevRelax
     *,Tn9Volk
     *,FlwType,FlwCfGgJa,FlwGgVel,ComGamFrom,ComGamTo,FlwRoFrom,FlwRoTo
     *,NodXx,Tn8XxWt,Tn7Xk,n08,n07,Tn9GSepSum
     *,FlwGaSlFrom,FlwGaSlTo,Tn9GFlwWtSum,Tn9MasGaRealk
     *,Tn9drdp,FlwdRodPFrom,FlwdRodPTo,Tn9Vk
     *,Tn9drdh,FlwCfAdFrom,FlwCfAdTo
     *,FlwRoFromSt,FlwRoToSt,FlwCcGgWtJa
     *,FlwdRodPSndFrom,FlwdRodPSndTo
     *,FlwVpInFlagFrom,FlwVpInFlagTo
     *,Tn9Vol,Tn9VolCorrUp,Tn9VolCorrDo
     *,FlwFromSoc,FlwToSoc,FlwSp)
      tick(62,1) = time_df1()-time_tmp
      ErrorNum(1)=62

      time_tmp = time_df1()
      call Tn3_kG(Gk,Tn3Lev,FlwKgp,FlwKgg,FlwKge
     *,Tn3Mask,nv,ifrokz
     *,itonkz,n03,Tn3CcGas,LinIbnFro,LinIbnTo,nu
     *,NodBodyTyp,Tn3Pk,Tn3Rok
     *,Tn3LevMask,dtint,Tn3CcUp,FlwXxFrom,FlwXxTo,nf
     *,FlwHTnFromDo,FlwHTnFromUp,FlwHTnToDo,FlwHTnToUp
     *,Tn3LevRelax,Tn3EvDo
     *,Tn3Ne,n09,Tn9LevRelax
     *,Tn3Volk,FlwType
     *,ComGamFrom,ComGamTo,FlwRoFrom,FlwRoTo
     *,NodXx,Tn8XxWt,Tn7Xk,n08,n07
     *,FlwGaSlFrom,FlwGaSlTo,Tn3MasGaRealk
     *,Tn3drdp,FlwdRodPFrom,FlwdRodPTo,Tn3Vk
     *,Tn3drdh,FlwCfAdFrom,FlwCfAdTo
     *,FlwRoFromSt,FlwRoToSt,FlwCcGgWtJa
     *,FlwdRodPSndFrom,FlwdRodPSndTo
     *,FlwVpInFlagFrom,FlwVpInFlagTo
     *,FlwFromSoc,FlwToSoc,FlwSp)
      tick(61,1) = time_df1()-time_tmp
      ErrorNum(1)=61
*****************************************

         call ABconsqrt(nv,nu,Npres,n08,n03,
     *                 FlwCon,C7,AconFrom,AconTo,Bcon,
     *                 FlwType,Flwsq,FlwSqMin,Gk,
     *                 LinIBNfro,LinIBNto,NodRelP,Tn8RelP,
     *                 Tn3RelP,
     *                 FlwdPdGPm,FlwGrdPp,FlwRoFrom,FlwRoTo,
     *                 IFROKZ,ITONKZ,Pk,Flwsq2,
     *                 rovk,FlwGgVel,
     *                 InternalIter,flwbcon,
     *                 FlwLnSq,atau,Gz,FlwdPStat,FlwDpPm,FlwKsi1,
     *                 FlwResTyp,
     *                 FlwC7work1,FlwC7work2,FlwC7work3,FlwC7work4,
     *                 n07,Tn7RelP,
     *                 rovz,n09,Tn9RelP,
     *                 FlwGgVeCor1,FlwCorAccel,
     *                 NodGgSum,NodGgSumIn,aconrovk,
     *                 FlwCfTime,FlwdRodPFrom,FlwdRodPTo,
     *                 Flwcfadfrom,FlwcfadTo,FlwC7work7,ic,
     *                 flwdcon,flwfrco,Tn7dcon
     *,FlwHTnFromDo,FlwHTnToDo,Tn7Lev,FlwTnSqFrom,FlwTnSqTo
     *,FlwCorSound,Gzz,flwsound
     *,FlwdRodPSndFrom,FlwdRodPSndTo,Gk_ext,Gk_INT,FlwCondRegul
     *,Tn3FlwIntCoef,Tn9FlwIntCoef,FlwCondRegul1
     *,FlwCondRegul0,MasdtIntegr_Ga,MasdtIntegr_Wv
     *,NodMasGaMax,NodMasWvMax,NodRo,Volukz,dtint
     *,NodGgCompGas,NodGgCompWvp
     *,HCOFlMod,HCOCfMod,HCOOFg,HCOONodVol,HCOONoddRodP,NHCO,HCOIndFlw,
     >Pzt)

      tick(49,1) = time_df1()-time_tmp
      ErrorNum(1)=49
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 7109 

c   ��������� ��������� ������� Dmp (����� ������� ���������)
      time_tmp = time_df1()
c        call SetMusor8(Dmp,Npres*Npres)
        call CLR_S_e(Dmp,Npres,ILNZp,INZSUBp,NZSUBp,PIntIter)
      tick(52,1) = time_df1()-time_tmp
      ErrorNum(1)=52

      time_tmp = time_df1()
c        call SetMusor8(Yp,Npres)
        call clr_Y(Npres,Yp) !��������� ������ ������ ��� ��������
      tick(53,1) = time_df1()-time_tmp
      ErrorNum(1)=53

* ���������������� �������  ��� Tn3 � Tn9
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

* ���������������� ������� ��� ����� �������
      time_tmp = time_df1()
         call Tn39_Srv1(n03,Tn3Volk,Tn3Volkdt,Atau,Tn3Ne)
      tick(215,1) = time_df1()-time_tmp
      ErrorNum(1)=215
* ������� ��������� � ����������� � �������           !Internal
      time_tmp = time_df1()

      call Tn3_isp_cond(Tn3Pk,Tn3Ek,Tn3Tt,n03,Tn3G,Tn3Mask
     *,Tn3hw,Tn3hs,Tn3dGdP,Tn3TtWlk,Tn3AlGw23,Tn3Gw23,Tn3dGw23dP
     *,Tn3AlGw34,Tn3Gw34,Tn3dGw34dP,Tn3AlGw43,Tn3Gw43,Tn3dGw43dP
     *,Tn3CfGgWpUpWt,Tn3CfGgWtWpUp,Tn3CfGgWpDoWt,Tn3CfGgWtWpDo
     *,Tn3CfGgWpDoWpUp,Tn3CfGgWlWpUpWt,Tn3CfGgWlWtWpDo,Tn3CfGgWlWpDoWt
     *,Tn3Ts,Tn3Pk2,Tn3hw2,Tn3hs2,Tn3ts2,Tn3Tt2,Tn3Gw32,Tn3DGw32dP
     *,Tc2AlG23,Tc2G23,Tc2dG23dp,Tc2G34,Tc2dG34dp,Tc2AlG43,Tc2G43
     *,Tc2dG43dp,Tc2CfGgWpUpWt,Tc2CfGgWt,Tc2CfGgWpDoWt,NTc2,Tc2TtWlk
     *,Tc2Ibn,Tc2Node,Tc2G32,Tc2dG32dp,Tc2CcGgWtWpDo,Tn3Volk,Tn3Vol
     *,Tn3AlSqPhWl,Tc2AlSqPhWl,Tc2SqWl,Tc2TettaQ,Tc2QqIsp,Tc2AlIsp,
     >Tc2DeltaTs
     *,Tn3AlGv23,Tn3CfGgGaWpUpWt,Tn3Gv23,Tn3dGv23dP,Tn3SqWl
     *,Tc0AlG23,Tc0G23,Tc0dG23dp,Tc0G34,Tc0dG34dp,Tc0AlG43,Tc0G43
     *,Tc0dG43dp,Tc0CfGgWpUpWt,Tc0CfGgWt,Tc0CfGgWpDoWt,NTc0,Tc0TtWlk
     *,Tc0IbnTo,Tc0NodTo,Tc0G32,Tc0dG32dp,Tc0CcGgWtWpDo
     *,Tc0AlSqPhWl,Tc0SqWl,Tc0TettaQ,Tc0QqIsp,Tc0AlIsp,Tc0DeltaTs
     *,TenAlG23,TenG23,TendG23dp,TenG34,TendG34dp,TenAlG43,TenG43
     *,TendG43dp,TenCfGgWpUpWt,TenCfGgWt,TenCfGgWpDoWt,NTen,TenTtWlk
     *,TenIbn,TenNode,TenG32,TendG32dp,TenCcGgWtWpDo
     *,TenAlSqPhWl,TenSqWl,TenTettaQ,TenQqIsp,TenAlIsp
     *,Tn3AlSqPhPh,Tn3QqPhPh,Tn3QqGen,Tn3SqPhPh,Tn3dg32dp2,Tn3dg32dp3
     *,Tn3Ne,dtInt,Tn3TettaQ,Tc2dQIspdP,Tc2TtTnIsp,Tc2AlSqIsp
     *,Tc0dQIspdP,Tc0TtTnIsp,Tc0AlSqIsp
     *,TendQIspdP,TenTtTnIsp,TenAlSqIsp,Tc2ResWl,Tc0ResWl,Tc0dQG23dp
     *,Tc0TettaQ2,Tn3CfVolVpUp,TenResWl,Tn3CfGgWtWpUp2
     *,nf,Tc0IbnFro,Tc0NodFro,Tc0CcGgWtWpDoFrom,Tc0AlG23From,
     >Tc0CfGgWpUpWtFrom
     *,Tc0AlSqPhWlFrom,Tc0G23From,Tc0G32From,Tc0G43From,Tc0G34From
     *,Tc0dG23dPFrom,Tc0AlG43From,Tc0CfGgWpDoWtFrom
     *,Tc0dG43dPFrom,Tc0TtTnIspFrom,Tc0QqIspFrom,Tc0AlIspFrom
     *,Tc0CfGgWtFrom,Tc0AlSqIspFrom,Tc0dG34dpFrom,Tc0dG32dpFrom
     *,Tc0dQIspdPFrom,Tc0TettaQFrom,Tn3Qq23CfRel
     *,Tn3Cf42
     *,Tn3WlTtIsp,Tn3WlQqIsp,Tn3WlAlIsp,Tn3WlAlSqIsp
     *,Tn3G42TypCalc,Tn3Al34TypCalc
     *,Tn3CcWpDo,Tn3Rok,Tn3La,Tn3HD,Tn3Cp,Tn3Sigma,Tn3VsWt,Tn3Vel42
     *,Tn3Dd4,Tn3ro2,Tn3cp2,Tn3hd2,Tn3Sq34,Tn3CfSq34,Tn3CfSqWl34
     *,Tn3CfGgWpDo_tmp,FirstStepLabel,Tc2CfRelAlIsp,Tc0CfRelAlIsp
     *,Tn3TtWtMi,Tn3dTWtMi
     *,Tn3Volt,Tn3Ts_,Tn3Ts2_,Tn3Hi,Tn3LevMas,Tn3VolVpUpLim,Tn3Pk_dev,
     >Tn3Qq34dec)
      tick(216,1) = time_df1()-time_tmp
      ErrorNum(1)=216
c       if (icint.gt.0) then                           !Internal
c* ������� ������������ ��� �������� �/�� �������
c      call Tn3_kG(Gk,Tn3Lev,hfrom,hto,FlwKgp,FlwKgg,FlwKge
c       endif
      time_tmp = time_df1()
      call Tn3_Spr(nv,LinIbnFro,LinIbnTo,ifrokz,itonkz,FlwType,n03
     *,Tn3RelE,nent,EeWtSatk,nu,NodEeWv,FlwSp,Tn3Mask,gk,dtint,Tn3Ek,
     >Tn3GSpCond
     *,Tn3QSpCond,FlwKgp,Tn3GSpCondSum
     *,Tn3Lev,FlwHTnFromDo,FlwHTnFromUp,FlwHTnToDo,FlwHTnToUp,
     >Tn3GgSpSum,Tn3Ne,nf
     *,n09,Tn9Ek,n07,Tn7Ek,n08,Tn8EeWt)
      tick(217,1) = time_df1()-time_tmp
      ErrorNum(1)=217

* ���� dh � ��������� ��� P
c         call Tn3_tp(n03,Tn3AlSqPhWl,Tn3AlPhPh
c         call Tn3Tc2_tp(n03,Tn3MasCp
c         CALL Tn3_nh4 (Nent,Tn3VOLDT,Tn3Rot
c      CALL Tn3Tc2_nh4 (Nent,Dmh,Ye,invperE
c      call Tn3_HPre(Nent,invperE,n03,Tn3RelE,Dmh,Ye,Tn3dh)


      time_tmp = time_df1()
      call Tn3_PPre_new(nv,n03,IFROKZ
     *,ITONKZ,Bcon,LinIBNto,LinIBNfro
     *,FlwKgp,Tn3VOLdt,Tn3rot,Tn3Rok,Tn3G
     *,Tn3gw23,Tn3gw34,Tn3gw43,Tn3Gw32
     *,Tn3volkdt,Tn3MasGenMult,Tn3masgen
     *,Tn3dH,Tn3dRdH,Tn3GCorr,Tn3GSpCond,Tn3Gv23
     *,Tc2G23,Tc2G34,Tc2G43,Tc2G32,ntc2,Tc2Ibn,Tc2Node
     *,Tc0G23,Tc0G34,Tc0G43,Tc0G32,ntc0,Tc0IbnTo,Tc0NodTo
     *,TenG23,TenG34,TenG43,TenG32,nten,TenIbn,TenNode
     *,Tn3Ne,nf
     *,Tc0G23From,Tc0G34From,Tc0G43From,Tc0G32From
     *,Tc0IbnFro,Tc0NodFro,Tn3LevTunG,Tn3LevTunPpRegG)
      tick(218,1) = time_df1()-time_tmp
      ErrorNum(1)=218
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 7260 


****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 7334 


****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'call_PreNp4Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c   ������������ ������� ��������� ��� ��������       !Internal 
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

* ���������� ������������ ������ ������� �������� ����� �������
      time_tmp = time_df1()
      call Tn3_np4(Dmp,Yp,Npres,Tn3drdp,invperP,n03
     *,Tn3dGdP,Tn3RelP,Tn3dGw23dP
     *,Tn3dGw34dP,Tn3dGw43dP,Tn3dGw32dP
     *,Tn3Volkdt,Tn3rv,Tn3masgenMult,Tn3masgen,Tn3Vk
     *,Tc2dG23dp,Tc2dG34dp,Tc2dG43dp,Tc2dG32dp,NTc2,Tc2Ibn,Tc2Node
     *,Tc0dG23dp,Tc0dG34dp,Tc0dG43dp,Tc0dG32dp,NTc0,Tc0IbnTo,Tc0NodTo
     *,TendG23dp,TendG34dp,TendG43dp,TendG32dp,NTen,TenIbn,TenNode
     *,Tn3dg32dp2,Tn3dg32dp3,Tn3Ne
     *,Tc0dG23dpFrom,Tc0dG34dpFrom,Tc0dG43dpFrom,Tc0dG32dpFrom
     *,Tc0IbnFro,Tc0NodFro)
c      call Tn3_np4_p1(Dmp,Yp,Npres,Tn3drdp,invperP,n03
c     *,Tn3dGdP,Tn3RelP,Tn3dGw23dP,Tn3dGw34dP,Tn3dGw43dP,Tn3dGw32dP
c     *,Tn3Volkdt,Tn3masgen,Tn3Vk
c     *,Tc2dG23dp,Tc2dG34dp,Tc2dG43dp,Tc2dG32dp,NTc2,Tc2Ibn,Tc2Node
c     *,Tc0dG23dp,Tc0dG34dp,Tc0dG43dp,Tc0dG32dp,NTc0,Tc0IbnTo,Tc0NodTo
c     *,TendG23dp,TendG34dp,TendG43dp,TendG32dp,NTen,TenIbn,TenNode
c     *,Tn3dg32dp2,Tn3dg32dp3,Tn3Ne,Tn3y1,Tn3y2,Tn3z1,Tn3z2,Tn3Rok)
      tick(219,1) = time_df1()-time_tmp
      ErrorNum(1)=219
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 7424 


****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 7452 


****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_NP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

      time_tmp = time_df1()
        call Tn3_Gp4_new(nv,n03,Npres,Tn3Np,
     *               IFROKZ,ITONKZ,Dmp,AconFrom,AconTo,
     *               invperP,LinIBNto,LinIBNfro,
     *               Tn3RelP,FlwKgp,
     *               Tn3masgenMult,FlwIndFromPp,FlwIndToPp,
     *               FlwIndFromPp1,FlwIndToPp1,Tn3Vk,Tn3Ne,nf,n09,
     >Tn9RelP)
      tick(220,1) = time_df1()-time_tmp
      ErrorNum(1)=220
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 7531 


****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 7557 


****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_GP4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c   ���������� �������� ��� �������� (dP - ������ ��������)
      time_tmp = time_df1()
        call Clr_Y(Npres,dP)
      tick(55,1) = time_df1()-time_tmp
      ErrorNum(1)=55
      time_tmp = time_df1()
c        call ERRA(Dmp,Npres,ILNZp,INZSUBp,NZSUBp,InvperP,PIntIter,1)
        call LINE_S_e(Npres,Dmp,Yp,dP,ILNZp,INZSUBp,NZSUBp,PIntIter)
      tick(56,1) = time_df1()-time_tmp
      ErrorNum(1)=56

c   ��������� ��������                                !Internal
      time_tmp = time_df1()
        CALL CorrectP1(Npres,nu,n08,n03,
     *                NodTyp,Pk,dP,Pk1,invperp,
     *                NodRelP,Tn8RelP,Tn3RelP,
     *                Tn3Pk,Tn3dp,Tn3CfPp,n07,Tn7RelP,Tn7Pk,
     *                n09,Tn9RelP,Tn9Pk,Tn9dp,Tn9CfPp,NodCfPp,
     *                Tn7CfPp,ic,NodCfPpUp,
     *                Tn3pmax,Tn9pmax,Tn9Pp,Tn3Pp,Tn9CfRelax(1),
     *                InternalIter,NodCfPpType)

      tick(57,1) = time_df1()-time_tmp
      ErrorNum(1)=57

C   ���������� �������� �� ������ ����������������� ��������
      time_tmp = time_df1()
        CALL Flowssqrt(nv,nu,n08,n03,
     *             Npres,Gk,Bcon,Pk1,
     *             IFROKZ,ITONKZ,FlwType,c7,
     *             LinIBNfro,LinIBNto,
     *             NodRelP,Tn8RelP,Tn3RelP,
     *             InternalIter,
     *             flwbcon,
     *             FlwC7work3,FlwC7work4,
     *             n07,Tn7RelP,n09,Tn9RelP,
     *             FlwdPLevFrom,FlwdPLevTo,FlwPpFrom,FlwPpTo,FlwGrdPp,
     *             Gk_int,Gk_ext,
     *             Tn3FlwIntCoef,Tn9FlwIntCoef)


         call Correctp2(Npres,nu,NodTyp,Pk,dP,Pk1,invperP,NodRelP,
     *      NodPp,Tn9CfRelax(1))

      tick(58,1) = time_df1()-time_tmp
      ErrorNum(1)=58

      time_tmp = time_df1()
      tick(60,1) = time_df1()-time_tmp
      ErrorNum(1)=60

*******************************
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 7633 

c   ------------------------------------------------------------------
c   ��������� ��������� ������� Dmh (����� ������� ���������)
c        call CLR_S(Dmh, Nent, ILNZe, INZSUBe, NZSUBe)
      time_tmp = time_df1()
c        call SetMusor8(Dmh,Nent*Nent)  !!!�����
        call CLR_S_e(Dmh,Nent,ILNZe,INZSUBe,NZSUBe,EIntIter)
      tick(63,1) = time_df1()-time_tmp
      ErrorNum(1)=63
      time_tmp = time_df1()
        call clr_Y(Nent,Ye)  !��������� ������ ������ ��� ���������
      tick(64,1) = time_df1()-time_tmp
      ErrorNum(1)=64

****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 7683 


****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'

      time_tmp = time_df1()
      call Tc2_AlNodWl(NTc2,Tc2CfAlEnv,Tc2AlEnv
     *,Tc2NodGgSumIn,Tc2NodDd,Tc2NodPp,Tc2NodSq,Tc2NodVsWt,Tc2NodVsVp
     *,Tc2TtEnv,Tc2NodTtSat,Tc2NodXx,Tc2QqEnv,Tc2TypAlNodWl
     *,Tc2AlEnvMod,Tc2SqWl,Tc2NodBodyTyp,Tc2NodTypeBut
     *,Tc2AlEnv_P,Tc2NodConcGl)
      tick(182,1) = time_df1()-time_tmp
      ErrorNum(1)=182
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 7700 


****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
c   ��������� ��������� �������� ������� �� ����. ���. Wl2
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 7748 

****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c   ������������ ������� ��������� ��� ���������       !Internal 
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'


* ������ �-��� ������������� ��� �������
      time_tmp = time_df1()
      call Tn3_tp(n03,Tn3AlSqPhWl,Tn3AlSqPhPh,Tn3AlWlUpDo
     *,Tn3AlSqWlEnv,Tn3MasCpWl,Tn3ActWlUpDo
     *,Tn3CfAlGaWl,Tn3CfAlWpUpWl,Tn3CfAlWtWl,Tn3CfAlWpDoWl
     *,Tn3CfAlGaWpUp,Tn3CfAlGaWt,Tn3CfAlGaWpDo,Tn3CfAlWpUpWpDo
     *,Tn3CfAlWlUpEnv,Tn3CfAlWlDoEnv,Tn3CfAlWlUpDo
     *,Tn3TettaQ,Tn3SqWl
     *,Tn3Rok,Tn3AlPhPh,Tn3SqPhPh,Tn3Ne,Tn3CfAlEnvGr,Tn3AlWlEnv
     *,Tn3volt,Tn3vol
     *,Tn3AlGaWl,Tn3AlWpUpWl,Tn3AlWtWl,Tn3AlWpDoWl)
      tick(221,1) = time_df1()-time_tmp
      ErrorNum(1)=221
      time_tmp = time_df1()
      call Tn3Tc2_tp(NTc2,Tc2Ibn,Tc2Node
     *,Tc2AlSqEnv,Tc2AlSqPhWl,Tc2CfAlGaWl,Tc2CfAlWpUpWl
     *,Tc2CfAlWtWl,Tc2CfAlWpDoWl
     *,Tc2TettaQ,Tc2SqWl,Tc2AlPhWl,Tc2AlEnvMod
     *,Tc2ResWl,nf,Tc2LlWl,Tc2LaWl,Tc2ResWl0)
      tick(222,1) = time_df1()-time_tmp
      ErrorNum(1)=222

* ���������� ������������ ������ ������� ��������� ��� �������
      time_tmp = time_df1()
      CALL Tn3_nh4 (Nent,Tn3VOLDT,Tn3Rot,Tn3Ee,Dmh,Ye,Tn3Pk,Tn3Pp,Tn3Ek
     *,invperE,Tn3hw,Tn3hs,Tn3tt,Tn3TtWl,Tn3G,n03,Tn3AlSqPhPh,
     >Tn3AlSqPhWl
     *,Tn3cp,Tn3RelE,Tn3MasCpWl,Tn3AlSqWlEnv,Tn3AlWlUpDo,Tn3TtEnv,dtInt
     *,Tn3zd,Tn3zu,Tn3det,Tn3Gw23,Tn3Gw34,Tn3Gw43,Tn3Ts,Tn3Ne
     *,Tc2G23,Tc2G34,Tc2G43,NTc2,Tc2Ibn,Tc2Node,Tc2G32,Tc2QqIsp
     *,Tn3QCorr,Tn3dtdh,Tn3Gv23,Tn3TettaWl
     *,Tc0G23,Tc0G34,Tc0G43,Tc0G32,NTc0,Tc0IbnTo,Tc0NodTo,Tc0QqIsp
     *,TenG23,TenG34,TenG43,TenG32,NTen,TenIbn,TenNode,TenQqIsp,
     >Tn3QCorrUpDo
     *,Tn3drdh,Tn3drdp,Tc2dQIspdP,Tc0dQIspdP,TendQIspdP,Tn3QCorrPh,
     >Tc0dQG23dp
     *,Tn3WlUpQqOut,Tn3WlDoQqOut,Tn3TtWlk,Tn3TcwAlCulc !,Tn3CcUp,Tn3RoomQq)
     *,Tn3QqPhPh,Tn3WlAlSqIsp,Tn3WlTtIsp,Tn3WlQqIsp,DmhG,YeG
     *,Tn3WlUpQqDop,Tn3WlDoQqDop)
      tick(225,1) = time_df1()-time_tmp
      ErrorNum(1)=225
      time_tmp = time_df1()
      CALL Tn3Tc2_nh4 (Nent,Dmh,Ye,invperE,Tn3Tt,n03,Tn3RelE,dtInt
     *,Tc2zt,Tc2QqEnv,NTc2,Tc2Ibn,Tc2Node,Tc2TtEnv,Tc2TtWlk,Tc2TtWl
     *,Tc2AlSqEnv,Tc2AlSqPhWl
     *,Tc2MasCpWl,Tn3dtdh,Tc2QqIsp,Tn3Ne,nf,Tc2TtTnIsp,Tc2AlSqIsp)
      tick(226,1) = time_df1()-time_tmp
      ErrorNum(1)=226
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 7897 


****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 7998 


****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_NH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 8026 

c   ������������ ������� ��������� ��� ���������       !Internal 
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

      time_tmp = time_df1()
      call Tn3_Gh4(nu,nv,n08,n03,Nent
     *,Tn3Ne,IFROKZ,ITONKZ
     *,LinIBNfro,LinIBNto,Gk,Ek,Dmh,Ye,invperE
     *,NodRelE,Tn8RelE,Tn3RelE
     *,Tn3Cp,Tn3Ek,EeWtSatk,EeVpSatk,FlwKge
     *,Tn3QSpCond,Tn3GSpCond,FlwXxFrom,FlwXxTo,Nf
     *,n09,Tn9Ne,Tn9RelE,Tn9Ek,Tn9Cp,NodEeGa,Tn7RelE,Tn7Ek,n07
     *,NodXx,Tn8XxWt,Tn7Xk,NodEeWv)
      tick(229,1) = time_df1()-time_tmp
      ErrorNum(1)=229
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 8087 


****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 8117 


****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_GH4_INT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c   ������ ������� � ��������� ������� ��� ��������� � � ������ �������
c   �� ����������, ��������� � ������������ ����� ������ ��� �������
c   �����
c        call Wl1_HhEqv(nu,Nwall,Nent,Wl1Node,Wl1Mass,
c     *                 Wl1AlNode,Wl1Sq,
c     *               NodCpk,NodXxBal,Ek,NodTtk,Wl1Cp,Wl1Tt,
c     *               Dmh,Ye,C1serv,C2serv,invperE,Wl1TtEnv,Wl1AlEnv,
c     *               Wl1SqEnv,
c     *               IC,MaxIternew(1),Wl1decr_alf,
c     *               falfa,falfa_e)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 8151 

      time_tmp = time_df1()
      call Tn3_CorrectYe(Nent,Tn3VOLDT,Tn3Rot,Tn3Ee,Ye,invperE
     *  ,n03,Tn3RelE)
      tick(66,1) = time_df1()-time_tmp
      ErrorNum(1)=66
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 8159 


c   ���������� �������� ��� ��������� (dE - ������ ��������)
      time_tmp = time_df1()
        call Clr_Y(Nent,dE)
      tick(67,1) = time_df1()-time_tmp
      ErrorNum(1)=67
      time_tmp = time_df1()
        call ERRA(Dmh,Nent,ILNZe,INZSUBe,NZSUBe,InvperE,EIntIter,2)
        call LINE_S_e(Nent,Dmh,Ye,dE,ILNZe,INZSUBe,NZSUBe,EIntIter)
      tick(68,1) = time_df1()-time_tmp
      ErrorNum(1)=68

c   ��������� ���������                           !Internal
***************************************************************
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

      time_tmp = time_df1()
      call Tn3_CorH(n03,Tn3Ne,Nent,Tn3RelE,invperE,dE,Tn3Tt,Tn3Ek,Ek,
     *                  Ek1,Tn3CfEe,Tn3TtGa,Tn3Ee,Tn9CfRelax(1))
      tick(230,1) = time_df1()-time_tmp
      ErrorNum(1)=230
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 8225 


****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 8248 


****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CorH_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

      time_tmp = time_df1()
      call Tn3_Restore(pk1,ek1,Tn3Pk,Tn3Ek,Tn3Tt,Tn3RelP,Tn3RelE
     *,n03,npres,nent,Tn3Ne,Tn3pmax)
c      call Tn3_Restore_p1(pk1,ek1,Tn3Pk,Tn3Ek,Tn3Tt,Tn3RelP,Tn3RelE
c     *,n03,npres,nent,Tn3Ne)
      tick(257,1) = time_df1()-time_tmp
      ErrorNum(1)=257
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 8322 


****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 8344 


****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c *******************************************************************
c     �������� ����, �������, ���������� �������
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

* ��������� � �������                                 !Internal
      time_tmp = time_df1()
      call Tn3_Prop(Tn3Pk,Tn3Ek,Tn3Tt,Tn3Rok,Tn3drdp,Tn3drdh,n03
     *,Tn3Mask,Tn3hw,Tn3hs,Tn3cp,Tn3Ts
     *,EeWtSatk,EeVpSatk,Tn3RelE,Nent,Tn3Pk2,Tn3hw2,Tn3hs2,Tn3ts2
     *,Tn3Tt2,Tn3Vk,Tn3dTdH,Tn3RoWtSatk,Tn3Ne,Tn3CcUp2,Tn3TtFr,Tn3La
     *,Tn3HD,Tn3ro2,Tn3cp2,Tn3hd2
     *,Tn3rot,Tn3LevMas,Tn3Levadd,Tn3ts_,Tn3Ts2_,Tn3Pk_dev)
      tick(231,1) = time_df1()-time_tmp
      ErrorNum(1)=231

* �������� ���� � �������                             !Internal
      time_tmp = time_df1()
      call Tn3_Mass(nv,IFROKZ,ITONKZ,n03,dtInt,Gk
     *,Tn3G,FlwKgp,Tn3Mas,Tn3Mask
     *,LinIBNFro,LinIBNTo,Tn3Gsum
     *,Tn3Gw23,Tn3Gw34,Tn3Gw43,Tn3Gw32
     *,Tc2G23,Tc2G34,Tc2G43,Tc2G32,ntc2,Tc2Ibn,Tc2Node
     *,Tn3GCorr,Tn3CcGas
     *,Tn3CfMasGasMin,Tn3Pk,Tn3GSpCondSum,Tn3Gv23
     *,Tc0G23,Tc0G34,Tc0G43,Tc0G32,ntc0,Tc0IbnTo,Tc0NodTo
     *,TenG23,TenG34,TenG43,TenG32,nten,TenIbn,TenNode,Tn3Ne,nf,Gz,
     >Tn3Volk,Tn3MasGaMin
     *,Tn3MasGaReal,Tn3MasGaRealk
     *,Tc0G23From,Tc0G34From,Tc0G43From,Tc0G32From
     *,Tc0IbnFro,Tc0NodFro,Tn3VolNorm0,Tn3VolNorm,Tn9CfRelax(1)
     *,Tn3rok,Tn3MassInterpKey,Tn3LevTunG,Tn3LevTunPpRegG)

c      call Tn3PPart(n03,Tn3Mask,Tn3Pk,Tn3Rok,Tn3RelE,Nent,Tn3Ek,Tn3Tt)

      tick(232,1) = time_df1()-time_tmp
      ErrorNum(1)=232

c                                                     !Internal
* ������, ������, ����� �������� ������ � ������� �� ��������
*                  (��� ������� � ������)
      time_tmp = time_df1()
      call Tn3_Vol(Tn3rok,n03,Tn3LevRelk,Tn3LevMask,Tn3volk,Tn3Vol
     *,Tn3Mask,Tn3RoDo,Tn3Ev_T,Tn3Vol_T,Tn3Dim_TMax,Tn3Dim_T,Tn3RelE
     *,Tn3EvDo,Tn3Lev,Tn3MasWl,Tn3MasCpWl,Tn3CpWl,Tn3TettaQ
     *,Tn3Vk,Tn3CcUp,Tn3AVolDo,Tn3VolCorrUp,Tn3VolCorrDo,Tn3CcWpDo
     *,Tn3TettaWl,Tn3EvSq_T,Tn3Sq_T,Tn3Sq,Tn3Ts,Tn3SqPhPh,Tn3Ne
     *,Tn3La,Tn3LaFr,Tn3LaCf,Tn3Cf42,Tn3CfGgWpDoWpUp,Tn3G42TypCalc
     *,Tn3Vel42,Tn3Dd4,Tn3Sigma,Tn3VolDoVH,Tn3ficr,Tn3CfGgWpDo_tmp,
     >Tn3CfMasWl,Tn3VolNorm0
     *,Tn3Levadd,Tn3Ts_,Tn3MassInterpKey,Tn3Vlim1,Tn3Vlim2,Tn3VolFull)
      tick(233,1) = time_df1()-time_tmp
      ErrorNum(1)=233
      time_tmp = time_df1()
       call Tn3Tc2_Lev(n03,Tn3Lev,NTc2,Tc2Ibn,Tc2Node
     *,Tc2EvUpWl,Tc2EvDoWl,Tc2TettaQ,Tn3Volk,Tn3CcUp,Tn3AVolDo,Tn3Ne,nf
     *,Tc2Form,Tc2Ev_T,Tc2Sq_T,Tc2Dim_Tmax,Tc2Dim_T,Tc2SqWl,Tn3LevRelk,
     >Tc2CfVolWt,Tc2CfCcWpUp)
      tick(234,1) = time_df1()-time_tmp
      ErrorNum(1)=234
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 8484 


****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 8563 


****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Mas_Vol_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

* ����������� ������ � �������                        !Internal
      time_tmp = time_df1()
      call Tn3_WlTt(Tn3MasCpWl,Tn3TtWl,Tn3TtWlk,dtInt,Tn3AlSqPhWl,Tn3Tt
     *,Tn3AlSqWlEnv,Tn3AlWlUpDo,Tn3TtEnv,Tn3zd,Tn3zu,Tn3det,n03
     *,Tn3Ne,Tn3QCorrUpDo,Tn3WlUpQqOut,Tn3WlDoQqOut,Tn3TcwAlCulc
     *,Tn3WlAlSqIsp,Tn3WlTtIsp)
      tick(237,1) = time_df1()-time_tmp
      ErrorNum(1)=237
      time_tmp = time_df1()
      call Tn3Tc2_WlTt(dtInt,Tn3Tt,n03,NTc2,Tc2Ibn,Tc2Node,Tc2zt
     *,Tc2TtEnv,Tc2TtWlk,Tc2TtWl,Tc2AlSqEnv,Tc2AlSqPhWl,Tc2MasCpWl
     *,Tc2QqIsp,Tn3Ne,nf,Tc2TtTnIsp,Tc2AlSqIsp)
      tick(238,1) = time_df1()-time_tmp
      ErrorNum(1)=238
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 8664 


****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 8712 


****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'WallTemp_Int' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'


c   ���� �������� ����� � ������� �� ������           !Internal
      time_tmp = time_df1()
      call Volume(nv,LinIBNFro,LinIBNTo,IFROKZ,ITONKZ,
     *            HFrom,HTo,
     *            Tn8Lev,Tn3LevRel,Tn9LevRel,
     *            Tn8RoWtk,Tn3Rok(1,3),Tn9Rok(1,3),
     *            dHLev,FlwdPStat,
     *            FlwRoFrc,
     *            InternalIter,Tn3EvDo,
     *            Tn7Levk,Tn7Rok(1,2),Tn9EvDo,
     *            Tn8RoGak,Tn3Rok(1,1),Tn9Rok(1,1),
     *            Tn7Rok(1,1),Gk,aconRovk,
     *            FlwdPLevFrom,FlwdPLevTo,
     *            Flwdd,FlwAngFrom,FlwAngTo,Tn3RoDo,Tn9RoDo
     *                 ,FlwHTnFromUp,FlwHTnFromDo,FlwHTnToUp,FlwHTnToDo
     *                 ,FlwRoFrom,FlwRoTo,n08,n03,n07,n09,FlwKkdh,
     >FlwFlagm2,
     *            NodCcGa,NodRoWtSat,NodRoGa,Tn3RoWpUp,Tn9RoVpUp)
      tick(69,1) = time_df1()-time_tmp
      ErrorNum(1)=69
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 8756 
c                                                     !Internal
c   ���� ������ �������
c --- ����� ��� ������ �������
        ICint=ICint+1
      time_tmp = time_df1()
        call stopiter(goint,icint,maxiterint,npres,pk,pk1,dpstop)
      tick(71,1) = time_df1()-time_tmp
      ErrorNum(1)=71
c        call stopiter(goint,icint,maxiterint,npres,pk,pk1,dpstop,dpmaxin)
c   ���������� � ��������� ��������
      time_tmp = time_df1()
        do i=1,Npres
        if(dabs(Pk1(i)).lt.1.d-20) Pk1(i)=1.d-20
            Pk(i)=Pk1(i)
          enddo
      enddo  !����� ���������� ��������
      tick(72,1) = time_df1()-time_tmp
      ErrorNum(1)=72

c --- ����� ���������� �������� ---                   !Internal

      icint=0
      InternalIter(1)=.false.
      time_tmp = time_df1()
      call Clr_Y(nv,FlwdPdGPm)
      tick(73,1) = time_df1()-time_tmp
      ErrorNum(1)=73
      time_tmp = time_df1()
c      call Clr_Y(nv,FlwdPPm)
      FlwdPPm=FlwdPdop
      tick(74,1) = time_df1()-time_tmp
      ErrorNum(1)=74

****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
c ��������� �������������� ��������� ������� ��������� 
c ��� ���������� ������������� �� �������� �����������
c

****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CompNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'


****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'


****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'TurPumpI' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

      time_tmp = time_df1()
c        call correctkv(nv,LinIbnFro,LinIbnTo
c     *,ifrokz,itonkz,FlwCon,Tn3Volk,n03,FlwCfCorCon,FlwCon,Tn3Ne)
      tick(80,1) = time_df1()-time_tmp
      ErrorNum(1)=80
        if((SrvLb1(1).ne.0).and.(ic.ne.0)) then ! ������� �� �����������
      time_tmp = time_df1()
        call Node_GgSum(nu,nv,Rok,NodRoMasPos,Gk,IFROKZ,ITONKZ,
     *                  LinIbnFro,LinIbnTo,NodGgSum,NodGgSumIn,
     *                  dtInt,NodVolk,NodSum1,NodSum2,FlwC7work3,
     >FlwC7work4,
     *                  dRoMas,FlwC7work5,NodRoMas,FlwRoMas,ROVZ,
     *                  InternalIter,FlwType)
      tick(81,1) = time_df1()-time_tmp
      ErrorNum(1)=81
        endif
      time_tmp = time_df1()

************************************************************
      time_tmp = time_df1()
      call clr_FlwKge(FlwKge,nf,nv,LinIbnFro,LinIbnTo)
      tick(92,1) = time_df1()-time_tmp
      ErrorNum(1)=92
      time_tmp = time_df1()
      call Com_Gam_Ro(nv,nu,n08,nf,IFROKZ,ITONKZ,LinIBNto,
     *                LinIBNfro,HFrom,HTo,FlwDd,NodBodyTyp,ComGamFrom,
     *                ComGamTo,Tn8Lev,FlwAngFrom,
     *                FlwAngTo,
     *                FlwRoInUpDo,
     *                FlwCcSqUpDo,n07,Tn7Levk,Tn7Rok,Tn7CcVolk,
     >FlwRoFrom,
     *                FlwRoTo,NodRo,Tn8RoGa,Tn8RoWt,
     *                Tn7homo,Tn7CcMask,
     *                Nodccga,
     *                FlwHTnFromDo,FlwHTnFromUp,FlwHTnToDo,FlwHTnToUp,
     *                Tn7Lev_Rlx,ic,FlwRoFromSt,FlwRoToSt,
     *                FlwdRodPFrom,FlwdRodPTo,
     *                NodDroDpk,Tn7dRdPk,Tn7dRdHk,Flwcfadfrom,
     *                FlwcfadTo,Nodcfad,Tn8dRdP1,Tn8dRdH1,
     *                Tn8dRdP2,Tn8dRdH2,
     *                NodCgGa,NodCgMa,NodCgMa0,NodCgEe,NodRoGg,  
     *                nsep1,Sep1FlwS,  
     *                Tn7CfRoGaCorr,Tn7CfRoWtCorr,  
     *                ComEeFrom,ComEeTo,FlwSlGaFrom,FlwSlGaFrom0,
     *                FlwSlGaTo,FlwSlGaTo0,  
     *                NodRoWv,NodRoGa,NodEeWv,NodEeGa,
     *                Nair,AirFlwInd,FlwXxFrom,FlwXxTo,
     *                ComRoFrom, ComRoTo, 
     *                NodRoTrue,NodRoWtSat,NodRoVpSat,
     *                Tn7RoWtTrue,Tn7RoWtSat,Tn7RoVpSat,
     *                Tn8RoWtSat,Tn8RoVpSat,FlwType,
     *                AirCf,NodCgVp,EeWtSatk,EeVpSatk,NodPropsKey1,
     *                FlwdRoDpSndFrom,FlwdRoDpSndTo,NoddRoDpSnd,
     *                NodRoTot,CoolerType,
     *                FlwdCgdCcFrom,FlwdCgdCcTo,NoddCgdCc,NodNumEe(1),
     *                FlwFromSoc,FlwToSoc,FlwSlip
     *                     ,nfwv,FWVFLOWW,FWVFLOWV,NodXxk1,NodXx,
     >NodCcGat
     *             ,FwvRoFrom1,FwvRoFrom2,FwvRoTo1,FwvRoTo2
     *             ,nfwv1,Fwv1Flow,NodEv1)

      tick(99,1) = time_df1()-time_tmp
      ErrorNum(1)=99

      time_tmp = time_df1()
      call Tn9_kG(Gk,Tn9Lev,FlwKgp,FlwKgg,FlwKge
     *,Tn9Mask,nv,ifrokz
     *,itonkz,n09,Tn9CcGas,LinIbnFro,LinIbnTo,nu
     *,NodBodyTyp,Tn9Pk,Tn9Rok
     *,Tn9LevMask,dtint,Tn9CcUp,FlwXxFrom,FlwXxTo,nf
     *,FlwHTnFromDo,FlwHTnFromUp,FlwHTnToDo,FlwHTnToUp
     *,Tn9LevRelax,Tn9EvDo
     *,Tn9Ne,n03,Tn3LevRelax
     *,Tn9Volk
     *,FlwType,FlwCfGgJa,FlwGgVel,ComGamFrom,ComGamTo,FlwRoFrom,FlwRoTo
     *,NodXx,Tn8XxWt,Tn7Xk,n08,n07,Tn9GSepSum
     *,FlwGaSlFrom,FlwGaSlTo,Tn9GFlwWtSum,Tn9MasGaRealk
     *,Tn9drdp,FlwdRodPFrom,FlwdRodPTo,Tn9Vk
     *,Tn9drdh,FlwCfAdFrom,FlwCfAdTo
     *,FlwRoFromSt,FlwRoToSt,FlwCcGgWtJa
     *,FlwdRodPSndFrom,FlwdRodPSndTo
     *,FlwVpInFlagFrom,FlwVpInFlagTo
     *,Tn9Vol,Tn9VolCorrUp,Tn9VolCorrDo
     *,FlwFromSoc,FlwToSoc,FlwSp)

      time_tmp = time_df1()
      call Tn3_kG(Gk,Tn3Lev,FlwKgp,FlwKgg,FlwKge
     *,Tn3Mask,nv,ifrokz
     *,itonkz,n03,Tn3CcGas,LinIbnFro,LinIbnTo,nu
     *,NodBodyTyp,Tn3Pk,Tn3Rok
     *,Tn3LevMask,dtint,Tn3CcUp,FlwXxFrom,FlwXxTo,nf
     *,FlwHTnFromDo,FlwHTnFromUp,FlwHTnToDo,FlwHTnToUp
     *,Tn3LevRelax,Tn3EvDo
     *,Tn3Ne,n09,Tn9LevRelax
     *,Tn3Volk,FlwType
     *,ComGamFrom,ComGamTo,FlwRoFrom,FlwRoTo
     *,NodXx,Tn8XxWt,Tn7Xk,n08,n07
     *,FlwGaSlFrom,FlwGaSlTo,Tn3MasGaRealk
     *,Tn3drdp,FlwdRodPFrom,FlwdRodPTo,Tn3Vk
     *,Tn3drdh,FlwCfAdFrom,FlwCfAdTo
     *,FlwRoFromSt,FlwRoToSt,FlwCcGgWtJa
     *,FlwdRodPSndFrom,FlwdRodPSndTo
     *,FlwVpInFlagFrom,FlwVpInFlagTo
     *,FlwFromSoc,FlwToSoc,FlwSp)
      tick(100,1) = time_df1()-time_tmp
      ErrorNum(1)=100
**************************************************



         call ABconsqrt(nv,nu,Npres,n08,n03,
     *                 FlwCon,C7,AconFrom,AconTo,Bcon,
     *                 FlwType,Flwsq,FlwSqMin,Gk,
     *                 LinIBNfro,LinIBNto,NodRelP,Tn8RelP,
     *                 Tn3RelP,
     *                 FlwdPdGPm,FlwGrdPp,FlwRoFrom,FlwRoTo,
     *                 IFROKZ,ITONKZ,Pk,Flwsq2,
     *                 rovk,FlwGgVel,
     *                 InternalIter,flwbcon,
     *                 FlwLnSq,atau,Gz,FlwdPStat,FlwDpPm,FlwKsi1,
     *                 FlwResTyp,
     *                 FlwC7work1,FlwC7work2,FlwC7work3,FlwC7work4,
     *                 n07,Tn7RelP,
     *                 rovz,n09,Tn9RelP,
     *                 FlwGgVeCor1,FlwCorAccel,
     *                 NodGgSum,NodGgSumIn,aconrovk,
     *                 FlwCfTime,FlwdRodPFrom,FlwdRodPTo,
     *                 Flwcfadfrom,FlwcfadTo,FlwC7work7,ic,
     *                 flwdcon,flwfrco,Tn7dcon
     *,FlwHTnFromDo,FlwHTnToDo,Tn7Lev,FlwTnSqFrom,FlwTnSqTo
     *,FlwCorSound,Gzz,flwsound
     *,FlwdRodPSndFrom,FlwdRodPSndTo,Gk_ext,Gk_INT,FlwCondRegul
     *,Tn3FlwIntCoef,Tn9FlwIntCoef,FlwCondRegul1
     *,FlwCondRegul0,MasdtIntegr_Ga,MasdtIntegr_Wv
     *,NodMasGaMax,NodMasWvMax,NodRo,Volukz,dtint
     *,NodGgCompGas,NodGgCompWvp
     *,HCOFlMod,HCOCfMod,HCOOFg,HCOONodVol,HCOONoddRodP,NHCO,HCOIndFlw,
     >Pzt)

      tick(82,1) = time_df1()-time_tmp
      ErrorNum(1)=82

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 9361 

c   ��������� ��������� ������� Dmp (����� ������� ���������)
      time_tmp = time_df1()
c        call SetMusor8(Dmp,Npres*Npres)
        call CLR_S_e(Dmp,Npres,ILNZp,INZSUBp,NZSUBp,PExtIter_)
      tick(85,1) = time_df1()-time_tmp
      ErrorNum(1)=85
      time_tmp = time_df1()
        call clr_Y(Npres,Yp) !��������� ������ ������ ��� ��������
      tick(86,1) = time_df1()-time_tmp
      ErrorNum(1)=86

****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

* ���������������� ������� ��� ����� �������
      time_tmp = time_df1()
         call Tn39_Srv1(n03,Tn3Volk,Tn3Volkdt,Atau,Tn3Ne)
      tick(241,1) = time_df1()-time_tmp
      ErrorNum(1)=241
* ������� ��������� � ����������� � �������
      time_tmp = time_df1()
      call Tn3_isp_cond(Tn3Pk,Tn3Ek,Tn3Tt,n03,Tn3G,Tn3Mask
     *,Tn3hw,Tn3hs,Tn3dGdP,Tn3TtWlk,Tn3AlGw23,Tn3Gw23,Tn3dGw23dP
     *,Tn3AlGw34,Tn3Gw34,Tn3dGw34dP,Tn3AlGw43,Tn3Gw43,Tn3dGw43dP
     *,Tn3CfGgWpUpWt,Tn3CfGgWtWpUp,Tn3CfGgWpDoWt,Tn3CfGgWtWpDo
     *,Tn3CfGgWpDoWpUp,Tn3CfGgWlWpUpWt,Tn3CfGgWlWtWpDo,Tn3CfGgWlWpDoWt
     *,Tn3Ts,Tn3Pk2,Tn3hw2,Tn3hs2,Tn3ts2,Tn3Tt2,Tn3Gw32,Tn3DGw32dP
     *,Tc2AlG23,Tc2G23,Tc2dG23dp,Tc2G34,Tc2dG34dp,Tc2AlG43,Tc2G43
     *,Tc2dG43dp,Tc2CfGgWpUpWt,Tc2CfGgWt,Tc2CfGgWpDoWt,NTc2,Tc2TtWlk
     *,Tc2Ibn,Tc2Node,Tc2G32,Tc2dG32dp,Tc2CcGgWtWpDo,Tn3Volk,Tn3Vol
     *,Tn3AlSqPhWl,Tc2AlSqPhWl,Tc2SqWl,Tc2TettaQ,Tc2QqIsp,Tc2AlIsp,
     >Tc2DeltaTs
     *,Tn3AlGv23,Tn3CfGgGaWpUpWt,Tn3Gv23,Tn3dGv23dP,Tn3SqWl
     *,Tc0AlG23,Tc0G23,Tc0dG23dp,Tc0G34,Tc0dG34dp,Tc0AlG43,Tc0G43
     *,Tc0dG43dp,Tc0CfGgWpUpWt,Tc0CfGgWt,Tc0CfGgWpDoWt,NTc0,Tc0TtWlk
     *,Tc0IbnTo,Tc0NodTo,Tc0G32,Tc0dG32dp,Tc0CcGgWtWpDo
     *,Tc0AlSqPhWl,Tc0SqWl,Tc0TettaQ,Tc0QqIsp,Tc0AlIsp,Tc0DeltaTs
     *,TenAlG23,TenG23,TendG23dp,TenG34,TendG34dp,TenAlG43,TenG43
     *,TendG43dp,TenCfGgWpUpWt,TenCfGgWt,TenCfGgWpDoWt,NTen,TenTtWlk
     *,TenIbn,TenNode,TenG32,TendG32dp,TenCcGgWtWpDo
     *,TenAlSqPhWl,TenSqWl,TenTettaQ,TenQqIsp,TenAlIsp
     *,Tn3AlSqPhPh,Tn3QqPhPh,Tn3QqGen,Tn3SqPhPh,Tn3dg32dp2,Tn3dg32dp3
     *,Tn3Ne,dtInt,Tn3TettaQ,Tc2dQIspdP,Tc2TtTnIsp,Tc2AlSqIsp
     *,Tc0dQIspdP,Tc0TtTnIsp,Tc0AlSqIsp
     *,TendQIspdP,TenTtTnIsp,TenAlSqIsp,Tc2ResWl,Tc0ResWl,Tc0dQG23dp
     *,Tc0TettaQ2,Tn3CfVolVpUp,TenResWl,Tn3CfGgWtWpUp2
     *,nf,Tc0IbnFro,Tc0NodFro,Tc0CcGgWtWpDoFrom,Tc0AlG23From,
     >Tc0CfGgWpUpWtFrom
     *,Tc0AlSqPhWlFrom,Tc0G23From,Tc0G32From,Tc0G43From,Tc0G34From
     *,Tc0dG23dPFrom,Tc0AlG43From,Tc0CfGgWpDoWtFrom
     *,Tc0dG43dPFrom,Tc0TtTnIspFrom,Tc0QqIspFrom,Tc0AlIspFrom
     *,Tc0CfGgWtFrom,Tc0AlSqIspFrom,Tc0dG34dpFrom,Tc0dG32dpFrom
     *,Tc0dQIspdPFrom,Tc0TettaQFrom,Tn3Qq23CfRel
     *,Tn3Cf42
     *,Tn3WlTtIsp,Tn3WlQqIsp,Tn3WlAlIsp,Tn3WlAlSqIsp
     *,Tn3G42TypCalc,Tn3Al34TypCalc
     *,Tn3CcWpDo,Tn3Rok,Tn3La,Tn3HD,Tn3Cp,Tn3Sigma,Tn3VsWt,Tn3Vel42
     *,Tn3Dd4,Tn3ro2,Tn3cp2,Tn3hd2,Tn3Sq34,Tn3CfSq34,Tn3CfSqWl34
     *,Tn3CfGgWpDo_tmp,FirstStepLabel,Tc2CfRelAlIsp,Tc0CfRelAlIsp
     *,Tn3TtWtMi,Tn3dTWtMi
     *,Tn3Volt,Tn3Ts_,Tn3Ts2_,Tn3Hi,Tn3LevMas,Tn3VolVpUpLim,Tn3Pk_dev,
     >Tn3Qq34dec)
      tick(242,1) = time_df1()-time_tmp
      ErrorNum(1)=242

c       if (ic.gt.0) then
c* ������� ������������ ��� �������� �/�� �������
c      call Tn3_kG(Gk,Tn3Lev,hfrom,hto
c       endif
      time_tmp = time_df1()
      call Tn3_Spr(nv,LinIbnFro,LinIbnTo,ifrokz,itonkz,FlwType,n03
     *,Tn3RelE,nent,EeWtSatk,nu,NodEeWv,FlwSp,Tn3Mask,gk,dtint,Tn3Ek,
     >Tn3GSpCond
     *,Tn3QSpCond,FlwKgp,Tn3GSpCondSum
     *,Tn3Lev,FlwHTnFromDo,FlwHTnFromUp,FlwHTnToDo,FlwHTnToUp,
     >Tn3GgSpSum,Tn3Ne,nf
     *,n09,Tn9Ek,n07,Tn7Ek,n08,Tn8EeWt)
      tick(243,1) = time_df1()-time_tmp
      ErrorNum(1)=243

      time_tmp = time_df1()
      call Tn3_PPre_new(nv,n03,IFROKZ
     *,ITONKZ,Bcon,LinIBNto,LinIBNfro
     *,FlwKgp,Tn3VOLdt,Tn3rot,Tn3Rok,Tn3G
     *,Tn3gw23,Tn3gw34,Tn3gw43,Tn3Gw32
     *,Tn3volkdt,Tn3MasGenMult,Tn3masgen
     *,Tn3dH,Tn3dRdH,Tn3GCorr,Tn3GSpCond,Tn3Gv23
     *,Tc2G23,Tc2G34,Tc2G43,Tc2G32,ntc2,Tc2Ibn,Tc2Node
     *,Tc0G23,Tc0G34,Tc0G43,Tc0G32,ntc0,Tc0IbnTo,Tc0NodTo
     *,TenG23,TenG34,TenG43,TenG32,nten,TenIbn,TenNode
     *,Tn3Ne,nf
     *,Tc0G23From,Tc0G34From,Tc0G43From,Tc0G32From
     *,Tc0IbnFro,Tc0NodFro,Tn3LevTunG,Tn3LevTunPpRegG)
      tick(244,1) = time_df1()-time_tmp
      ErrorNum(1)=244
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 9497 



****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 9529 


****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 9592 


****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'call_PreNp4Ext' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c   ��������������� ������� ��� ������� �����
      time_tmp = time_df1()
        call Node_Srv1(nv,nu,IFROKZ,ITONKZ,Gk,LinIBNto,
     *                 LinIBNfro,Vol1,NoddRodE,NodRo,NodCfSrv1,
     *                 NodCfdE)
      tick(93,1) = time_df1()-time_tmp
      ErrorNum(1)=93

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 9624 
c ---  ������������ ������� ��������� ��� �������� ---

c ---  ���������� ������������ ��������� ������� ��������
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'

c       ������. ����. ��������� ��� ������� �����
      time_tmp = time_df1()

       call Node_np4_1new(nu,Npres,invperp,NodTyp,Dmp,NodRo,Rok,
     >NoddRodPk,
     *                    Vol1,Yp,NodGgCompWvp,NodGgCompGas,RotVol,
     >RokVol,
     *                    NodCfRo1,NodCfRo2,NodCfRo,NodCcga,NodCcgat,
     *                    NodGgSum,NodGgSumin,NodFreez,
     *                    dtint,Volukz,relaxold,NodTypTnk,
     *                    NodCfCorr_On,NODFREEZPH,NodPp,masdt)

      tick(163,1) = time_df1()-time_tmp
      ErrorNum(1)=163
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 9661 

****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_NP4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'


****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'


****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

* ���������� ������������ ������ ������� �������� ����� �������
      time_tmp = time_df1()
      call Tn3_np4(Dmp,Yp,Npres,Tn3drdp,invperP,n03
     *,Tn3dGdP,Tn3RelP,Tn3dGw23dP
     *,Tn3dGw34dP,Tn3dGw43dP,Tn3dGw32dP
     *,Tn3Volkdt,Tn3rv,Tn3masgenMult,Tn3masgen,Tn3Vk
     *,Tc2dG23dp,Tc2dG34dp,Tc2dG43dp,Tc2dG32dp,NTc2,Tc2Ibn,Tc2Node
     *,Tc0dG23dp,Tc0dG34dp,Tc0dG43dp,Tc0dG32dp,NTc0,Tc0IbnTo,Tc0NodTo
     *,TendG23dp,TendG34dp,TendG43dp,TendG32dp,NTen,TenIbn,TenNode
     *,Tn3dg32dp2,Tn3dg32dp3,Tn3Ne
     *,Tc0dG23dpFrom,Tc0dG34dpFrom,Tc0dG43dpFrom,Tc0dG32dpFrom
     *,Tc0IbnFro,Tc0NodFro)
c      call Tn3_np4_p1(Dmp,Yp,Npres,Tn3drdp,invperP,n03
c     *,Tn3dGdP,Tn3RelP,Tn3dGw23dP,Tn3dGw34dP,Tn3dGw43dP,Tn3dGw32dP
c     *,Tn3Volkdt,Tn3masgen,Tn3Vk
c     *,Tc2dG23dp,Tc2dG34dp,Tc2dG43dp,Tc2dG32dp,NTc2,Tc2Ibn,Tc2Node
c     *,Tc0dG23dp,Tc0dG34dp,Tc0dG43dp,Tc0dG32dp,NTc0,Tc0IbnTo,Tc0NodTo
c     *,TendG23dp,TendG34dp,TendG43dp,TendG32dp,NTen,TenIbn,TenNode
c     *,Tn3dg32dp2,Tn3dg32dp3,Tn3Ne,Tn3y1,Tn3y2,Tn3z1,Tn3z2,Tn3Rok)
      tick(245,1) = time_df1()-time_tmp
      ErrorNum(1)=245
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 9789 

****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 9807 

****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 9820 

****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 9839 


****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_NP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c --- ���������� �������������� ��������� ������� ��������
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'

      time_tmp = time_df1()

        call Node_Gp4_1new(nf,nv,nu,n03,n08,Npres,
     *                  Tn3Np,IFROKZ,ITONKZ,Pk,Gk,
     *                  Dmp,AconFrom,AconTo,Bcon,Yp,invperP,LinIBNto,
     *                  LinIBNfro,
     *                  Tn3RelP,FlwIndFromPp,
     *                  FlwIndToPp,FlwIndFromPp1,FlwIndToPp1,
     *                  NodRelE,Tn3RelE,
     *                  Tn8RelE,Tn3Ne,Ek,Nent,
     *                  NodCfdE,Ye,FlwType,n09,Tn9RelP,
     *                  ComGamFrom,ComGamTo,NodCfRo1,NodCfRo2,NodFreez,
     >NODFREEZPH)


      tick(164,1) = time_df1()-time_tmp
      ErrorNum(1)=164

****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'


****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'


****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

      time_tmp = time_df1()
        call Tn3_Gp4_new(nv,n03,Npres,Tn3Np,
     *               IFROKZ,ITONKZ,Dmp,AconFrom,AconTo,
     *               invperP,LinIBNto,LinIBNfro,
     *               Tn3RelP,FlwKgp,
     *               Tn3masgenMult,FlwIndFromPp,FlwIndToPp,
     *               FlwIndFromPp1,FlwIndToPp1,Tn3Vk,Tn3Ne,nf,n09,
     >Tn9RelP)
      tick(246,1) = time_df1()-time_tmp
      ErrorNum(1)=246



****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'



****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'


****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'


****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_GP4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 10011 

c   ���������� �������� ��� �������� (dP - ������ ��������)
      time_tmp = time_df1()
        call Clr_Y(Npres,dP)
      tick(94,1) = time_df1()-time_tmp
      ErrorNum(1)=94
      time_tmp = time_df1()
        call LINE_S_e(Npres,Dmp,Yp,dP,ILNZp,INZSUBp,NZSUBp,PExtIter_)
      tick(95,1) = time_df1()-time_tmp
      ErrorNum(1)=95


c      call relaxP(Npres,nu,n07,NodRelP,Tn7RelP,invperP,
c     *          Pk,dP,NodCfPp,Tn7CfPp,NodCfPpUp,NodCfPpUp_,dp_Tn7,
c     *          dp_nod,NodTypTnk,DpmaxNorm)

c        call Clr_Y(Npres,dP)
c   ��������� ��������
      time_tmp = time_df1()
        CALL CorrectP1(Npres,nu,n08,n03,
     *                NodTyp,Pk,dP,Pk1,invperp,
     *                NodRelP,Tn8RelP,Tn3RelP,
     *                Tn3Pk,Tn3dp,Tn3CfPp,n07,Tn7RelP,Tn7Pk,
     *                n09,Tn9RelP,Tn9Pk,Tn9dp,Tn9CfPp,NodCfPp,
     *                Tn7CfPp,ic,NodCfPpUp,
     *                Tn3pmax,Tn9pmax,Tn9Pp,Tn3Pp,Tn9CfRelax(1),
     *                InternalIter,NodCfPpType)
      tick(96,1) = time_df1()-time_tmp
      ErrorNum(1)=96
C   ���������� �������� �� ������ ����������������� ��������
      time_tmp = time_df1()


         call Correctp2(Npres,nu,NodTyp,Pk,dP,Pk1,invperP,NodRelP,
     *      NodPp,Tn9CfRelax(1))


        CALL Flowssqrt(nv,nu,n08,n03,
     *             Npres,Gk,Bcon,Pk1,
     *             IFROKZ,ITONKZ,FlwType,c7,
     *             LinIBNfro,LinIBNto,
     *             NodRelP,Tn8RelP,Tn3RelP,
     *             InternalIter,
     *             flwbcon,
     *             FlwC7work3,FlwC7work4,
     *             n07,Tn7RelP,n09,Tn9RelP,
     *             FlwdPLevFrom,FlwdPLevTo,FlwPpFrom,FlwPpTo,FlwGrdPp,
     *             Gk_int,Gk_ext,
     *             Tn3FlwIntCoef,Tn9FlwIntCoef)


      tick(97,1) = time_df1()-time_tmp
      ErrorNum(1)=97

**************************************************
      call Com_Gam_Gg(nv,nu,nf,n03,n09,Tn3ne,tn9ne,IFROkz,ITONkz,
     >LinIBNto,
     *                LinIBNfro,NodBodyTyp,ComGamFrom, 
     *                ComGamTo,n07,
     *                aconfROM,aconto,GK,Tn7acon,Tn7Sq,dtint,
     *                FlwGgGa,FlwGgWv,
     *                NodGgSumZeroin,flwdcon,flwfrco,Tn7dcon,
     *                NodGgSum,NodGgSumIn,NodGgSumWvin,FlwXxFrom,
     *                FlwXxTo,FlwGgVp,ComRoFrom,ComRoTo,
     *                Tn3Rok,Tn9Rok,AirG,AirGw,AirGg,AirGv,
     *                Nair,AirFlwInd)


      tick(101,1) = time_df1()-time_tmp
      ErrorNum(1)=101



* ���� �������� �� �������
      call Flw_QqAd(nu,nv,ifrokz,itonkz,LinIbnFro,LinIbnTo,Gk
     *,NodQqAd,FlwCfQqAd,FlwQqAd,FlwPpFrom,FlwPpTo,rovk,NodTt)

      time_tmp = time_df1()
      call Tn258_GgSum_v1(nv,n08,Gk,IFROKZ,
     *                 ITONKZ,
     *                 Tn8GSumWt1,Tn8GSumWt2,
     *                 InternalIter,Nf,LinIbnFro,LinIbnTo,
     *                 ComGamFrom,ComGamTo
     *,FlwHTnFRomUp,FlwHTnToUp,Tn8Lev,Tn8GInOut)
      tick(102,1) = time_df1()-time_tmp
      ErrorNum(1)=102
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 10133 

C ���������� � ������� �������� ������������
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'


             call Node_CcMas_Pre(nu,ic,NodTyp,RotVol,
     *                           NodCcGa_Beq,NodCcGa_Beq0,
     *                           NodCcGa_Aeq,NodCcGa_Deq,NodccGat,
     *                           NodGgCompGas,NodGgCompWvp,NodGgSumIn,
     *                           NodFreez,Nodccga,
     *                           NodCcGa_Aeq0,NodCcGa_Deq0,NODFREEZPH)  

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 10165 
                               
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CcMas_Pre' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

* ������ �������� ������������ �� ���������� ���������
      do i=1,1
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'

      time_tmp = time_df1()

             call Node_CcMas(nf,nu,nv,NodTyp,NodBodyTyp,
     *                       IFROKZ,ITONKZ,
     *                       LinIBNfro,LinIBNto,RotVol,RokVol,
     *                       NodCcGa_Beq,NodCcGa_Beq0,
     *                       NodCcGa_Aeq,NodCcGa_Deq,NodCcGa_Deq0,
     *                       NodCcga,NodFreez,
     *                       nsep1,Sep1FlwS,Sep1Node,
     *                       FlwSlGaFrom, FlwSlGaTo ,
     *                       FlwSlGaFrom0,FlwSlGaTo0,
     *                       FlwGgGa,FlwGgWv,Gk,NODFREEZPH)

      tick(165,1) = time_df1()-time_tmp
      ErrorNum(1)=165
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 10261 

****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 10315 
      


****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CcMas_IterInt' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      enddo


****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 10384 


****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'

      time_tmp = time_df1()
      call Tc2_AlNodWl(NTc2,Tc2CfAlEnv,Tc2AlEnv
     *,Tc2NodGgSumIn,Tc2NodDd,Tc2NodPp,Tc2NodSq,Tc2NodVsWt,Tc2NodVsVp
     *,Tc2TtEnv,Tc2NodTtSat,Tc2NodXx,Tc2QqEnv,Tc2TypAlNodWl
     *,Tc2AlEnvMod,Tc2SqWl,Tc2NodBodyTyp,Tc2NodTypeBut
     *,Tc2AlEnv_P,Tc2NodConcGl)
      tick(182,1) = time_df1()-time_tmp
      ErrorNum(1)=182
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 10401 


****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
c   ��������� ��������� �������� ������� �� ����. ���. Wl2
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 10449 

****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Tc*_AlNodWl' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 10511 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 10517 


****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'

      call Tcw_0all(Nwall,NMtl,NMtlO
     *   ,Wl1_QqOut,Mtl_QqOut,Wl1AlCulcWl,MtlAlCulc
     *   ,Tn3WlDoQqOut,Tn3WlUpQqOut,Tn3TcwAlCulc,n03
     *   ,Tn9WlDoQqOut,Tn9WlUpQqOut,Tn9TcwAlCulc,n09
     *   ,Tn7WlDoQqOut,Tn7WlUpQqOut,Tn7TcwAlCulc,n07
     *   ,TenTcwQq,TenTcwAlCulc,nTen
     &   ,WLC2_Qq,WLC2Iwl,Nwlcon2
     &   ,Nwall2,Wl2Ndet,wl2_QqOut,wl2AlCulcWl2
     &   ,MtlO_QqOut,MtlOAlCulcWl1)


C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 10554 

****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 10578 

****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_TcW_Qq' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'

      call Wl1_Flw(Nwall,nv,nu,
     *  Wl1Tt,Wl1Lamd,Wl1_QqOut,Wl1AlCulcWl,
     *  FlwFlQqWl1,FlwDdExt,FlwThi,ifrokz,itonkz,
     *  FlwQqWl1,FlwKkQqWl1,FlwLl,NodIndWl1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 10689 

c   ������ ������� � ��������� ������� ��� ��������� � � ������ �������
c   �� ����������, ��������� � ������������ ����� ������ ��� �������
c   �����
      time_tmp = time_df1()
        call Wl1_HhEqv(nu,Nwall,Nent,Wl1Node,Wl1Mass,
     &               NodCp,NodTtk,Wl1Cp,wl1_Tt_t,
     &               Dmh,Ye,C1serv,C2serv,invperE,Wl1TtEnv,
     &               Pk,NoddRodPk,NoddRodEk,NodXxBalk1,
     &               Wl1AlCulcNode,Wl1AlCulcEnv,WL1Qq,dtInt,
     &               wl1_QqOut,Wl1CpC,wl1AlCulcWl,wl1Tt,
     &               Wl1CfCulcNode,Wl1CfCulcEnv,Wl1CfAllEnv,
     &               Wl1_XQ,Wl1_BQ,Wl1CfMasMe) 
  
      tick(379,1) = time_df1()-time_tmp
      ErrorNum(1)=3791
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 10707 


****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 10738 



****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'call_PreNh4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

      do i=1,ItZei(1)

c   ------------------------------------------------------------------
c   ��������� ��������� ������� Dmh (����� ������� ���������)
      time_tmp = time_df1()
c        call SetMusor8(Dmh,Nent*Nent)  !!!�����
        call CLR_S_e(Dmh,Nent,ILNZe,INZSUBe,NZSUBe,EExtIter_)
        call CLR_S_e(DmhG,Nent,ILNZe,INZSUBe,NZSUBe,EExtIter_)
      tick(103,1) = time_df1()-time_tmp
      ErrorNum(1)=103
      time_tmp = time_df1()
        call clr_Y(Nent,Ye)  !��������� ������ ������ ��� ���������
        call clr_Y(Nent,YeG)  !��������� ������ ������ ��� ���������
      tick(104,1) = time_df1()-time_tmp
      ErrorNum(1)=104

c --- ������������ ����. ��������� ������� ���������
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'

      call clr_Y(Nu,NodYe)
      call clr_Y(Nu,NodDmh)
      time_tmp = time_df1()
        call Node_nh4_new(nu,Npres,Nent,Dmh,Pk1,NodPp,NodEe,Ek,Vol1,
     *                NodQqAdd,NodDmhAdd,Ye,invperE,NodRelP,NodRelE,
     >NodQqPm,
     *                NodTyp,NodQqComp,RotVol,RokVol,NodFreez,
     *                NodCgEe,NodCgEe0,NodGgSum,NodGgSumIn,
     *                NodRoTot,NodRoTot0,NodCfdPdt,NODFREEZPH,NodQqAd,
     *                WasteNoGas(6,1),NodWaste_T,NodCfEeAsh,RadIndSA,
     *                NodTtk,NodCp,AshCfCp,
     *                NodGgCompWvp,NodGgCompGas,NodEeWv,NodEeGa,
     *                Masdt_Ga,Masdt_Wv,dtint,NodCcGa,NodCcGat,NodVvGa,
     >NodNumEe(1),
     *                NodEeGat,NodEeWvt,DmhG,YeG,EGk)

      tick(166,1) = time_df1()-time_tmp
      ErrorNum(1)=166
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 10802 

****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_NH4_NODE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'

c   ������ ������� � ��������� ������� ��� ��������� � � ������ �������
c   �� ����������, ��������� � ���������� � ���������������
            DTHe2 = dble(dtInt)
      time_tmp = time_df1()
c      call SetMusor
      call He2_HhEqv(nu,nv,Nhe2,Nent,He2PAIR,He2Mf2Lb
     &       ,He2Nod1Int,He2Nod2Int,He2Nod1Tub,He2Nod2Tub
     &       ,He2FlowInt,He2FlowTub,He2ind1co,He2ind2co
     &       ,DTHe2,Gk,GZ,He2TET,He2TET2,NodRo,VOLUKZ
     &       ,He2Mass,He2AlfInt,He2AlfTub,He2AreaInt,He2AreaTub
     &       ,NodCp,NodXxBal,NodTtk
     &       ,He2Cp,He2Tt,Dmh,Ye,C0He2,C1He2,C2He2,invperE
     &       ,IC,MaxIter(1),NodQqHe
     &       ,He2T11,He2T12,He2T21,He2T22,He2GgInt,He2GgTub
     &       ,He2pr1,He2pr2,He2dzetQ,He2tub1QQ,He2tub2QQ
     &       ,He2prb11,He2prb12,He2prb21,He2prb22
     &       ,He2T1t,He2T2t,He2decQ,He2decQt
     &       ,He2Ttb1,C0He2b1,C1He2b1,C2He2b1
     &       ,He2Ttb2,C0He2b2,C1He2b2,C2He2b2
     &        ,He2AlfCulcInt,He2AlfCulcTub
     &        ,Pk,NoddRodPk,NoddRodEk,NodXxBalk1
     &        ,He2_Gar2,He2_Cfcut
     &        ,NodVvGa,He2vvGa
     &        ,He2CfMasMe,He2QqRadAd
     *        ,NodYe,NodDmh,NodNumEe(1),He2tub1dmh,He2tub2dmh
     &        ,He2G0wvL1,He2G0gaL1,He2G0wvL2,He2G0gaL2
     &        ,He2ResMlf,FlwResDel,FlwSq,Rovz,He2Mf1Lb,He2Mf1Cf
     &        ,He2CfMf1Cf
     &        ,FlwGgGa,FlwGgWv,NodCpGa,NodCpWv,NodEeWv)
      tick(158,1) = time_df1()-time_tmp
      ErrorNum(1)=158

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 10903 


****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'



****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 10942 
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

* ������ �-��� ������������� ��� �������
      time_tmp = time_df1()
      call Tn3_tp(n03,Tn3AlSqPhWl,Tn3AlSqPhPh,Tn3AlWlUpDo
     *,Tn3AlSqWlEnv,Tn3MasCpWl,Tn3ActWlUpDo
     *,Tn3CfAlGaWl,Tn3CfAlWpUpWl,Tn3CfAlWtWl,Tn3CfAlWpDoWl
     *,Tn3CfAlGaWpUp,Tn3CfAlGaWt,Tn3CfAlGaWpDo,Tn3CfAlWpUpWpDo
     *,Tn3CfAlWlUpEnv,Tn3CfAlWlDoEnv,Tn3CfAlWlUpDo
     *,Tn3TettaQ,Tn3SqWl
     *,Tn3Rok,Tn3AlPhPh,Tn3SqPhPh,Tn3Ne,Tn3CfAlEnvGr,Tn3AlWlEnv
     *,Tn3volt,Tn3vol
     *,Tn3AlGaWl,Tn3AlWpUpWl,Tn3AlWtWl,Tn3AlWpDoWl)
      tick(247,1) = time_df1()-time_tmp
      ErrorNum(1)=247
      time_tmp = time_df1()
      call Tn3Tc2_tp(NTc2,Tc2Ibn,Tc2Node
     *,Tc2AlSqEnv,Tc2AlSqPhWl,Tc2CfAlGaWl,Tc2CfAlWpUpWl
     *,Tc2CfAlWtWl,Tc2CfAlWpDoWl
     *,Tc2TettaQ,Tc2SqWl,Tc2AlPhWl,Tc2AlEnvMod
     *,Tc2ResWl,nf,Tc2LlWl,Tc2LaWl,Tc2ResWl0)
      tick(248,1) = time_df1()-time_tmp
      ErrorNum(1)=248
* ���������� ������������ ������ ������� ��������� ��� �������
      time_tmp = time_df1()
      CALL Tn3_nh4 (Nent,Tn3VOLDT,Tn3Rot,Tn3Ee,Dmh,Ye,Tn3Pk,Tn3Pp,Tn3Ek
     *,invperE,Tn3hw,Tn3hs,Tn3tt,Tn3TtWl,Tn3G,n03,Tn3AlSqPhPh,
     >Tn3AlSqPhWl
     *,Tn3cp,Tn3RelE,Tn3MasCpWl,Tn3AlSqWlEnv,Tn3AlWlUpDo,Tn3TtEnv,dtInt
     *,Tn3zd,Tn3zu,Tn3det,Tn3Gw23,Tn3Gw34,Tn3Gw43,Tn3Ts,Tn3Ne
     *,Tc2G23,Tc2G34,Tc2G43,NTc2,Tc2Ibn,Tc2Node,Tc2G32,Tc2QqIsp
     *,Tn3QCorr,Tn3dtdh,Tn3Gv23,Tn3TettaWl
     *,Tc0G23,Tc0G34,Tc0G43,Tc0G32,NTc0,Tc0IbnTo,Tc0NodTo,Tc0QqIsp
     *,TenG23,TenG34,TenG43,TenG32,NTen,TenIbn,TenNode,TenQqIsp,
     >Tn3QCorrUpDo
     *,Tn3drdh,Tn3drdp,Tc2dQIspdP,Tc0dQIspdP,TendQIspdP,Tn3QCorrPh,
     >Tc0dQG23dp
     *,Tn3WlUpQqOut,Tn3WlDoQqOut,Tn3TtWlk,Tn3TcwAlCulc !,Tn3CcUp,Tn3RoomQq)
     *,Tn3QqPhPh,Tn3WlAlSqIsp,Tn3WlTtIsp,Tn3WlQqIsp,DmhG,YeG
     *,Tn3WlUpQqDop,Tn3WlDoQqDop)
      tick(251,1) = time_df1()-time_tmp
      ErrorNum(1)=251
      time_tmp = time_df1()
      CALL Tn3Tc2_nh4 (Nent,Dmh,Ye,invperE,Tn3Tt,n03,Tn3RelE,dtInt
     *,Tc2zt,Tc2QqEnv,NTc2
     *,Tc2Ibn,Tc2Node,Tc2TtEnv,Tc2TtWlk,Tc2TtWl,Tc2AlSqEnv,Tc2AlSqPhWl
     *,Tc2MasCpWl,Tn3dtdh,Tc2QqIsp,Tn3Ne,nf,Tc2TtTnIsp,Tc2AlSqIsp)
      tick(252,1) = time_df1()-time_tmp
      ErrorNum(1)=252
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11049 


****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11130 

****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11167 


****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11261 


****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'

c   ������ ������� � ��������� ������� ��� ��������� � � ������ �������
c   �� ����������, ��������� � ������������ ����� ������ ��� �������
c   �����
c      time_tmp = time_df1()
      if(NodNumEe(1).eq.1) then
        call Wl1_nh4(nu,nwall,Nent,Wl1Node,Dmh,Ye,invperE,Wl1_XQ,
     >Wl1_BQ) 
      endif
c      tick(379,1) = time_df1()-time_tmp
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11284 


****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11301 



****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_NH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11354 

****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11381 

****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Tn8Tc*' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11438 

c --- ���������� �������������� ��������� ������� ���������
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'

      time_tmp = time_df1()
      if(NodNumEe(1).eq.1) then
        call Node_Gh4_v1(nu,nv,n08,n03,Nent,
     *                Tn3Ne,IFROKZ,ITONKZ,
     *                LinIBNfro,LinIBNto,Gk,Ek,Dmh,Ye,invperE,
     *                NodRelE,Tn8RelE,Tn3RelE,
     *                Tn3Ek,Nf,
     *                n07,Tn7RelE,n09,Tn9RelE,Tn9Ek,Tn9Ne,
     *                Tn7Ek,Tn3Cp,ComGamFrom,ComGamTo,Tn9Cp,
     *                NodFreez,
     *                NodEeWv,NodEeGa,FlwGgWv,FlwGgGa,
     *                ComEeFrom,ComEeTo,NodTt,NodCp,FlwLaFrc,SchEnt,
     >NODFREEZPH,NodCfEeAsh,
     *                Tn3EntOut_Key,Tn9EntOut_Key)
      else
        call Node_Gh4_e2(nu,nv,n08,n03,Nent,
     *                Tn3Ne,IFROKZ,ITONKZ,
     *                LinIBNfro,LinIBNto,Gk,Ek,Dmh,Ye,invperE,
     *                NodRelE,Tn8RelE,Tn3RelE,
     *                Tn3Ek,Nf,
     *                n07,Tn7RelE,n09,Tn9RelE,Tn9Ek,Tn9Ne,
     *                Tn7Ek,Tn3Cp,ComGamFrom,ComGamTo,Tn9Cp,
     *                NodFreez,
     *                NodEeWv,NodEeGa,FlwGgWv,FlwGgGa,
     *                ComEeFrom,ComEeTo,NodTt,NodCp,FlwLaFrc,SchEnt,
     >NODFREEZPH,NodCfEeAsh,
     *                Tn3EntOut_Key,Tn9EntOut_Key,NodYe,NodDmh,YeG,DmhG,
     >EGk)
      endif
      tick(167,1) = time_df1()-time_tmp
      ErrorNum(1)=167
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11492 

****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

      time_tmp = time_df1()
      call Tn3_Gh4(nu,nv,n08,n03,Nent
     *,Tn3Ne,IFROKZ,ITONKZ
     *,LinIBNfro,LinIBNto,Gk,Ek,Dmh,Ye,invperE
     *,NodRelE,Tn8RelE,Tn3RelE
     *,Tn3Cp,Tn3Ek,EeWtSatk,EeVpSatk,FlwKge
     *,Tn3QSpCond,Tn3GSpCond,FlwXxFrom,FlwXxTo,Nf
     *,n09,Tn9Ne,Tn9RelE,Tn9Ek,Tn9Cp,NodEeGa,Tn7RelE,Tn7Ek,n07
     *,NodXx,Tn8XxWt,Tn7Xk,NodEeWv)
      tick(255,1) = time_df1()-time_tmp
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11532 


****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11551 

****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11571 

****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11592 

****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_GH4' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
c ��������� �������������� ��������� ������� ��������� 
c ��� ������������ ������������� �� �������� �������
c
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11701 

****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'TurNodeGH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
      if(NodNumEe(1).ne.1) then
        call Node_nh4_disp(nu,NodCcGa,NodCpGa,NodCpWv,Volukzt,NodRo
     *  ,NodAlSqGaWv,NodYe,NodQqAdd,NodDmhAdd,NodQqAd,NodQqPm,NodDmh,
     >NodXxBal
     *  ,NodXxBalk1,NodRelE,invperE,RotVol,Dmh,Ye,nent,NodCcGat
     *  ,NodEeWtSat,NodEeVpSat
     *  ,NodTtGa,NodTtWv,Pk,NoddRodPk,NoddRodEk,nwall,nwall2,Wl1Node
     *  ,Wl2Node,Wl1_XQ,Wl1_BQ,Wl2_XQ,Wl2_BQ,NodGgSumIn,dtint,NodVvGat
     *  ,DmhG,YeG,ntcr,TcRNode,TcR_XQ,TcR_BQ)
      endif
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11746 

****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_NH4_NODE_FINAL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11808 

      time_tmp = time_df1()
      if(IC.eq.MaxIternew(1)-1) then
        call Node_YeSave(Nu,Nent,Ye,Qdt,invperE,NodRelE,NodTyp)
      endif
      tick(107,1) = time_df1()-time_tmp
      ErrorNum(1)=107
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11817 


c   ���������� �������� ��� ��������� (dE - ������ ��������)
      time_tmp = time_df1()
        call Clr_Y(Nent,dE)
        call Clr_Y(Nent,dEG)
      tick(108,1) = time_df1()-time_tmp
      ErrorNum(1)=108
      time_tmp = time_df1()
      if(SchEnt(1).ne.1) then
        call LINE_S_e(Nent,Dmh,Ye,dE,ILNZe,INZSUBe,NZSUBe,EExtIter_)
        if (NodNumEe(1).ne.1) 
     *    call LINE_S_e(Nent,DmhG,YeG,dEG,ILNZe,INZSUBe,NZSUBe,
     >EExtIter_)
      else
        call line_zeidel(Nent,Dmh,Ye,dE,EExtIter_)
      endif
      tick(109,1) = time_df1()-time_tmp
      ErrorNum(1)=109

c   ��������� ���������
      time_tmp = time_df1()
        CALL PreCorH(Nent,nu,n08,n07,
     *                NodTyp,Ek,dE,Ek1,invperE,
     *                NodRelE,Tn8RelE,
     *                Tn7RelE,NodNumEe(1),EGk,EGk1,dEG)
      tick(110,1) = time_df1()-time_tmp
      ErrorNum(1)=110

****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'

      time_tmp = time_df1()
      if(NodNumEe(1).eq.1) then
        call Node_CorH(nu,Nent,NodTyp,NodBodyTyp,
     *                    Ek,Ek1,NodEeGa,NodTtk,
     *                    NodCp,NodCfEe,
     *                    Nodccga,
     *                    NodEeWv,
     *                    NodTtWv
     *                   ,NodAlfaH,NodFreez,NodEe,Tn9CfRelax(1))
      else
        call Node_CorH_e2(nu,Nent,NodTyp,NodBodyTyp,
     *                    Ek,Ek1,NodTtk,
     *                    NodCfEe,
     *                    NodEeWv,NodCpWv,
     *                    NodTtWv
     *                   ,NodAlfaH,NodFreez,Tn9CfRelax(1)
     *                   ,NodEeGat,NodeeWvt,EGk,EGk1)
      endif      

      tick(111,1) = time_df1()-time_tmp
      ErrorNum(1)=111
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11888 

****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

      time_tmp = time_df1()
      call Tn3_CorH(n03,Tn3Ne,Nent,Tn3RelE,invperE,dE,Tn3Tt,Tn3Ek,Ek,
     *                  Ek1,Tn3CfEe,Tn3TtGa,Tn3Ee,Tn9CfRelax(1))

      tick(256,1) = time_df1()-time_tmp
      ErrorNum(1)=256
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11922 


****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11948 

****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11961 

****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 11975 


****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CorH' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

        do j=1,Nent
          Ek(j)=Ek1(j)
          EGk(j)=EGk1(j)
        enddo
      enddo

c   ������ �������
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'

      time_tmp = time_df1()
      if(NodNumEe(1).eq.1) then
        call 
     *   Node_Prop(nu,Npres,Nent,NodTyp,NodBodyTyp
     *           ,Pk,Pk1,Ek,Ek1,NodEega
     *           ,nodDRDPWv,NoddRodPk1,nodDRDHWv,NoddRodEk1
     *           ,nodRoWv,Rok,nodRoGa,NodTtga,NodTtk,NodXxk1
     *           ,EeWtSatk,EeVpSatk,NodCp,NodRoWtSat
     *           ,NodTtSat,NodCfAd,Nodccga
     *           ,Nodvvga,NodGaType
     *           ,NodCfTur1,NodCfTur2,NodXxRel
     *           ,NodXxBalk1
     *           ,NodEeWv,NodCpGa,NodCfRo1,NodCfRo2,NodCfRo
     *           ,NodCpWv,NodTtWv,Masdt,NodRotrue
     *           ,NodCgGa,NodCgMa,NodCgEe
     *           ,NodRoGg,NodCgSqrt,NodalphMax
     *           ,NodGgSumZero,NodeSlipCor(1)
     *           ,Nodccgat,NodCgMa0
     *           ,NodKeyProps,NodRoVpSat
     *           ,NodTypIni,TypeBut
     *           ,NodAlphXbTun,NoddrdpWv_true
     *           ,NodXb_tmp,NodCcga_tmp,NodTt_tmp
     *           ,NodPp0,NodRoTot
     *           ,WasteNoGas,NodWaste_T,NodExhBet,NodSigmaSurf      
     *           ,NodPropsKey,NodPpVp,NodPp
     *           ,PpMax_iter(1),flag_first(1),MaxPpIterIn(1)
     *           ,PpPrecision(1),NodEe_tmp,NoddRodC
     *           ,NodePropsKey(1),NodFreez
     *           ,NodeSlipVp(1),NodCgVp
     *           ,NodTur2Key,PartpreIntFlag,NodExhBet_tmp,NoddRodPsnd
     *           ,NoddCgdCc,NodNumEe(1)
     *           ,NodPsw,NodRoWvTmp,Nod_ghm_flag,NodPropRelaxFlag
     *           ,NoddRdPGas,NoddRdHGas
     *           ,NodTtWvTmp,NodCpWvTmp,NodXxTmp,NoddRdPWvTmp,
     >NoddRdHWvTmp
     *           ,TypedROdH(1),OildROdH(1)
     *           ,WasteNoGl)
      else
        call Node_Prop_e2(nu,Npres,Nent,NodTyp,NodBodyTyp
     *           ,Pk,Pk1,Ek,Ek1,NodEega
     *           ,nodDRDPWv,NoddRodPk1,nodDRDHWv,NoddRodEk1
     *           ,nodRoWv,Rok,nodRoGa,NodTtga,NodTtk,NodXxk1
     *           ,EeWtSatk,EeVpSatk,NodCp,NodRoWtSat
     *           ,NodTtSat,NodCfAd,Nodccga
     *           ,Nodvvga,NodGaType
     *           ,NodCfTur1,NodCfTur2,NodXxRel
     *           ,NodXxBalk1
     *           ,NodEeWv,NodCpGa,NodCfRo1,NodCfRo2,NodCfRo
     *           ,NodCpWv,NodTtWv,NodRotrue
     *           ,NodCgGa,NodCgMa,NodCgEe
     *           ,NodRoGg,NodalphMax
     *           ,NodGgSumZero
     *           ,Nodccgat
     *           ,NodKeyProps,NodRoVpSat
     *           ,NodTypIni,TypeBut
     *           ,NodAlphXbTun,NoddrdpWv_true
     *           ,NodXb_tmp,NodCcga_tmp,NodTt_tmp
     *           ,NodPp0,NodRoTot
     *           ,WasteNoGas,NodWaste_T,NodExhBet,NodSigmaSurf      
     *           ,NodPropsKey,NodPpVp,NodPp
     *           ,PpMax_iter(1),flag_first(1),MaxPpIterIn(1)
     *           ,PpPrecision(1),NodEe_tmp,NodEeGa_tmp,NoddRodC
     *           ,NodePropsKey(1),NodFreez
     *           ,NodeSlipVp(1),NodCgVp
     *           ,NodTur2Key,PartpreIntFlag,NodExhBet_tmp,NoddRodPsnd
     *           ,NodEe,EGk,EGk1,NoddCgdCc,NodNumEe(1)
     *           ,NodPsw,NodRoWvTmp,Nod_ghm_flag,NodPropRelaxFlag
     *           ,NoddRdPGas,NoddRdHGas
     *           ,NodTtWvTmp,NodCpWvTmp,NodXxTmp,NoddRdPWvTmp,
     >NoddRdHWvTmp
     *           ,TypedROdH(1),OildROdH(1)
     *           ,WasteNoGl)
      endif
      tick(382,1) = time_df1()-time_tmp
      ErrorNum(1)=382

      time_tmp = time_df1()
      call Node_PropRelax(nu,NodPropRelaxFlag
     *,NoddRdPWvTmp,NoddRdPWv_true,Nodvvga,NodXxBalk1
     *,NodalphMax,NodPsw,masdt,NodRoWv,NodRoWtSat
     *,NodAlphXbTun,nodalph1,NodRelaxNo,NodCcGa
     *,NodMassFreez,Mass_Freez,NoddrdpWv,NodRoWvTmp,NodRoTrue
     *,NoddRdHWv,NoddRdHWvTmp,NodRoGa,NoddRdPGas,NoddRdHGas
     *,NodXxRel,NodTtWv,NodTtWvTmp,NodCpWv,NodCpWvTmp
     *,NodXxTmp,NodZalivDef,NodXxBal,NodCgasFreez)
      tick(383,1) = time_df1()-time_tmp
      ErrorNum(1)=383

      time_tmp = time_df1()
      call Nod_ghm(nu,Nod_ghm_flag,NodPsw,Nodccga,NodEega,Nodcpga
     *,NodRoGa,NodEewv,NodCpWv,NodRoWv,NodVvga,Rok,NoddRdPWv,NoddRdHwv
     *,NoddRodPk1,NoddRodEk1,NoddRdPGas,NoddRdHGas,NodCp
     *,NodCfRo1,NodCfRo2,NodCfRo,NodRoTrue
     *,NodCgGa,NodCgMa,NodCgEe,NodRoGg,NodCgSqrt
     *,NodGgSumZero,NodeSlipCor,NodBodyTyp
     *,NodCcgat,NodCgMa0,NodRoTot,NodDroDc
     *,NodXxBalk1,EeWtSatk,EeVpSatk,NodeSlipVp,NodCgVp,NoddRodPsnd
     *,NoddCgdCc,NodNumEe,NodRowvtmp,NoddRdPWvTmp,NoddRdHWvTmp
     *,NodCfTur1,NodCfTur2,NodCfad,NodXxk1)
      tick(384,1) = time_df1()-time_tmp
      ErrorNum(1)=384
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 12125 

****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 12171 

****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Prop' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'


c --- ���������� ����. �������������� ���������� ��� ����. � ���.
      time_tmp = time_df1()
      call Deviation(MaxDevP,MaxDevH,Npres,Nent,Pk1,Pk,Ek1,Ek)
      tick(115,1) = time_df1()-time_tmp
      ErrorNum(1)=115

      !��� ������� �����
      time_tmp = time_df1()
      call DerivCor(nu,MaxDevP,MaxDevH,NodXxBal,NodXxBalk1,NoddRodPk,
     *              NoddRodPk1,NoddRodEk,NoddRodEk1)
      tick(116,1) = time_df1()-time_tmp
      ErrorNum(1)=116
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 12215 

****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

      time_tmp = time_df1()
      call Tn3_Restore(pk1,ek1,Tn3Pk,Tn3Ek,Tn3Tt,Tn3RelP,Tn3RelE
     *,n03,npres,nent,Tn3Ne,Tn3pmax)
c      call Tn3_Restore_p1(pk1,ek1,Tn3Pk,Tn3Ek,Tn3Tt,Tn3RelP,Tn3RelE
c     *,n03,npres,nent,Tn3Ne)
      tick(257,1) = time_df1()-time_tmp
      ErrorNum(1)=257
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 12269 


****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 12291 


****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Restore' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

* �������� ���� � �������           
      time_tmp = time_df1()
      call Tn3_Mass(nv,IFROKZ,ITONKZ,n03,dtInt,Gk
     *,Tn3G,FlwKgp,Tn3Mas,Tn3Mask
     *,LinIBNFro,LinIBNTo,Tn3Gsum
     *,Tn3Gw23,Tn3Gw34,Tn3Gw43,Tn3Gw32
     *,Tc2G23,Tc2G34,Tc2G43,Tc2G32,ntc2,Tc2Ibn,Tc2Node
     *,Tn3GCorr,Tn3CcGas
     *,Tn3CfMasGasMin,Tn3Pk,Tn3GSpCondSum,Tn3Gv23
     *,Tc0G23,Tc0G34,Tc0G43,Tc0G32,ntc0,Tc0IbnTo,Tc0NodTo
     *,TenG23,TenG34,TenG43,TenG32,nten,TenIbn,TenNode,Tn3Ne,nf,Gz,
     >Tn3Volk,Tn3MasGaMin
     *,Tn3MasGaReal,Tn3MasGaRealk
     *,Tc0G23From,Tc0G34From,Tc0G43From,Tc0G32From
     *,Tc0IbnFro,Tc0NodFro,Tn3VolNorm0,Tn3VolNorm,Tn9CfRelax(1)
     *,Tn3rok,Tn3MassInterpKey,Tn3LevTunG,Tn3LevTunPpRegG)
      tick(385,1) = time_df1()-time_tmp
      ErrorNum(1)=385

* ��������� � �������                              
      time_tmp = time_df1()
      call Tn3_Prop(Tn3Pk,Tn3Ek,Tn3Tt,Tn3Rok,Tn3drdp,Tn3drdh,n03
     *,Tn3Mask,Tn3hw,Tn3hs,Tn3cp,Tn3Ts
     *,EeWtSatk,EeVpSatk,Tn3RelE,Nent,Tn3Pk2,Tn3hw2,Tn3hs2,Tn3ts2
     *,Tn3Tt2,Tn3Vk,Tn3dTdH,Tn3RoWtSatk,Tn3Ne,Tn3CcUp2,Tn3TtFr,Tn3La
     *,Tn3HD,Tn3ro2,Tn3cp2,Tn3hd2
     *,Tn3rot,Tn3LevMas,Tn3Levadd,Tn3ts_,Tn3Ts2_,Tn3Pk_dev)
      tick(386,1) = time_df1()-time_tmp
      ErrorNum(1)=386
                                                 
* ������, ������, ����� �������� ������ � ������� �� ��������
*                  (��� ������� � ������)
      time_tmp = time_df1()
      call Tn3_Vol(Tn3rok,n03,Tn3LevRelk,Tn3LevMask,Tn3volk,Tn3Vol
     *,Tn3Mask,Tn3RoDo,Tn3Ev_T,Tn3Vol_T,Tn3Dim_TMax,Tn3Dim_T,Tn3RelE
     *,Tn3EvDo,Tn3Lev,Tn3MasWl,Tn3MasCpWl,Tn3CpWl,Tn3TettaQ
     *,Tn3Vk,Tn3CcUp,Tn3AVolDo,Tn3VolCorrUp,Tn3VolCorrDo,Tn3CcWpDo
     *,Tn3TettaWl,Tn3EvSq_T,Tn3Sq_T,Tn3Sq,Tn3Ts,Tn3SqPhPh,Tn3Ne
     *,Tn3La,Tn3LaFr,Tn3LaCf,Tn3Cf42,Tn3CfGgWpDoWpUp,Tn3G42TypCalc
     *,Tn3Vel42,Tn3Dd4,Tn3Sigma,Tn3VolDoVH,Tn3ficr,Tn3CfGgWpDo_tmp,
     >Tn3CfMasWl,Tn3VolNorm0
     *,Tn3Levadd,Tn3Ts_,Tn3MassInterpKey,Tn3Vlim1,Tn3Vlim2,Tn3VolFull)
      tick(387,1) = time_df1()-time_tmp
      ErrorNum(1)=387
      time_tmp = time_df1()
       call Tn3Tc2_Lev(n03,Tn3Lev,NTc2,Tc2Ibn,Tc2Node
     *,Tc2EvUpWl,Tc2EvDoWl,Tc2TettaQ,Tn3Volk,Tn3CcUp,Tn3AVolDo,Tn3Ne,nf
     *,Tc2Form,Tc2Ev_T,Tc2Sq_T,Tc2Dim_Tmax,Tc2Dim_T,Tc2SqWl,Tn3LevRelk,
     >Tc2CfVolWt,Tc2CfCcWpUp)
      tick(388,1) = time_df1()-time_tmp
      ErrorNum(1)=388
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 12425 


****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 12470 


****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 12491 

****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 12561 


****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Mas_Vol' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

* ����������� ������ � �������
      time_tmp = time_df1()
      call Tn3_WlTt(Tn3MasCpWl,Tn3TtWl,Tn3TtWlk,dtInt,Tn3AlSqPhWl,Tn3Tt
     *,Tn3AlSqWlEnv,Tn3AlWlUpDo,Tn3TtEnv,Tn3zd,Tn3zu,Tn3det,n03
     *,Tn3Ne,Tn3QCorrUpDo,Tn3WlUpQqOut,Tn3WlDoQqOut,Tn3TcwAlCulc
     *,Tn3WlAlSqIsp,Tn3WlTtIsp)
      tick(258,1) = time_df1()-time_tmp
      ErrorNum(1)=258
      time_tmp = time_df1()
      call Tn3Tc2_WlTt(dtInt,Tn3Tt,n03,NTc2,Tc2Ibn,Tc2Node,Tc2zt
     *,Tc2TtEnv,Tc2TtWlk,Tc2TtWl,Tc2AlSqEnv,Tc2AlSqPhWl,Tc2MasCpWl
     *,Tc2QqIsp,Tn3Ne,nf,Tc2TtTnIsp,Tc2AlSqIsp)
      tick(259,1) = time_df1()-time_tmp
      ErrorNum(1)=259
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 12663 


****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 12700 

****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 12719 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 12742 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 12763 


****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 12803 


****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'WallTemp' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c   ���������� � ��������� ��������
      time_tmp = time_df1()
      do i=1,Npres
        if(dabs(Pk1(i)).lt.1.d-20) Pk1(i)=1.d-20
          Pk(i)=Pk1(i)
        enddo
      tick(134,1) = time_df1()-time_tmp
      ErrorNum(1)=134
      time_tmp = time_df1()
      do i=1,Nent
        Ek(i)=Ek1(i)
        EGk(i)=EGk1(i)
      enddo
      tick(135,1) = time_df1()-time_tmp
      ErrorNum(1)=135

c --- ���������� ������������ ��������
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'

      !���������� ��������� � ������� �����
      time_tmp = time_df1()
      call Node_Vs(nu,NodPp,NodTt,NodVsVp,NodVsWt,
     *             NodVsFr,NodXx,NodBodyTyp,TypeBut,
     *             NodLaFr,NodVvga,NodRoVpSat,NodRoWtSat,NodLaCf
     *            ,NodGaType,EXbetmid,NodExhBet,NodXb_prev,NodXb_next,
     >NodVsGa,NodRo
     *                  ,NodRoWv,NodWaste_T,WasteNoGl)
      tick(169,1) = time_df1()-time_tmp
      ErrorNum(1)=169
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 12873 
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

      !���������� ��������� � "����� �������"
      time_tmp = time_df1()
      call Tn3_Vs(n03,Tn3PpWpUp,Tn3PpWt,Tn3TtWpUp,Tn3TtWt,
     *            Tn3VsGa,Tn3VsVpUp,Tn3VsWt,Tn3VsVpDo)
      tick(265,1) = time_df1()-time_tmp
      ErrorNum(1)=265
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 12906 
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 12920 
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 12934 
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 12947 
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Viscosity' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c     ���������� ������� ������������ ��������� �� ������
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'

c --- ����������� ��������� �� ������ ������
      time_tmp = time_df1()
      call Flw_Viscos_v1(nv,IFROKZ,ITONKz,LinIBNfro,LinIBNto,
     *                   NodVsWt,Tn3VsGa,
     *                   Tn3VsVpUp,Tn3VsWt,Tn3VsVpDo,
     *                   Tn8VsGa,Tn8VsWt,
     *                   FlwVsFrom,
     *                   FlwVsTo,Nf,nu,n03,n08,
     *                   n07,Tn7VsGa,Tn7VsWt,Tn9VsGa,Tn9VsVpUp,Tn9VsWt,
     *                   Tn9VsVpDo,Tn9VsWtUp,Tn9VsWtJa,n09,ComGamFrom,
     *                   ComGamTo,NodVsVp,NodVsFr,Rovz,NodRoGa,NodRoWv,
     *                   Tn7RoGa,Tn7RoWt,NodBodyTyp,NodVsGa)

      tick(155,1) = time_df1()-time_tmp
      ErrorNum(1)=155
c --- ����������� ������� �������� �� �����
      time_tmp = time_df1()
c       call Viscosity(nv,FlwVsFrom,FlwVsTo,FlwVs)
      tick(156,1) = time_df1()-time_tmp
      ErrorNum(1)=156
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 12996 
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Flow_Visc' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c   ������ ������� ��������� � ����. ������ �������� �� ������
      time_tmp = time_df1()
        call Flw_Xx_v1(nv,n03,Nf,IFROKZ,ITONKZ,LinIBNfro,
     *                 LinIBNto,NodXxBalk1,Tn8XxBalWt,
     *                 Tn3Xxk,
     *                 FlwXxBalFrom,FlwXxBalTo,FlwXxFrom,
     *                 FlwXxTo,Tn7Xk(1,2),n09,Tn9Xxk,
     *                 ComGamFrom,ComGamTo,nu,n07,n08,
     *                 NodXb_prev,NodXb_next,Nodxb_filter,Gk)
      tick(138,1) = time_df1()-time_tmp
      ErrorNum(1)=138
      time_tmp = time_df1()
        call Density_new(nv,nu,
     *               n07,n03,n08,n09,
     *               LinIBNfro,LinIBNto,FlwRoFrom,
     *               IFROKZ,ITONKZ,
     *               FlwRoTo,
     *               ROVz,Rovk,
     *               FlwLl,FlwSq,Gk,FlwRo,
     *               dtInt,FlwRoFrc,Hfrom,Hto,
     *               AconRovk,Tn7lev,Tn8lev,Tn3Lev,Tn9Lev,NodTypTnk,
     >FlwResTyp,
     *               FlwHTnToDo,FlwHTnToUp,
     *               FlwHTnFromDo,FlwHTnFromUp,
     *               FlwVsFrom,FlwVsTo,FlwVsk,FlwVs,
     *               FlwRoFromSt,
     *               FlwRoToSt,NodLaFr,FlwLaFrc,FlwRoFlag,FlwCfdh,
     *               NodGgSumZero,NodGgSumZeroin,NodLaFr_cor(1),
     *                 FlwDensCfL,FlwRoC)

      tick(139,1) = time_df1()-time_tmp
      ErrorNum(1)=139
      time_tmp = time_df1()
      call Volume(nv,LinIBNFro,LinIBNTo,IFROKZ,ITONKZ,
     *            HFrom,HTo,
     *            Tn8Lev,Tn3LevRel,Tn9LevRel,
     *            Tn8RoWtk,Tn3Rok(1,3),Tn9Rok(1,3),
     *            dHLev,FlwdPStat,
     *            FlwRoFrc,
     *            InternalIter,Tn3EvDo,
     *            Tn7Levk,Tn7Rok(1,2),Tn9EvDo,
     *            Tn8RoGak,Tn3Rok(1,1),Tn9Rok(1,1),
     *            Tn7Rok(1,1),Gk,aconRovk,
     *            FlwdPLevFrom,FlwdPLevTo,
     *            Flwdd,FlwAngFrom,FlwAngTo,Tn3RoDo,Tn9RoDo
     *                 ,FlwHTnFromUp,FlwHTnFromDo,FlwHTnToUp,FlwHTnToDo
     *                 ,FlwRoFrom,FlwRoTo,n08,n03,n07,n09,FlwKkdh,
     >FlwFlagm2,
     *            NodCcGa,NodRoWtSat,NodRoGa,Tn3RoWpUp,Tn9RoVpUp)
      tick(140,1) = time_df1()-time_tmp
      ErrorNum(1)=140

c   ���� �������� ����� � ������� �� ������
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13119 

****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'

c   ���������� ����������� ������ ����� ��� ���������������
      time_tmp = time_df1()
c      call SetMusor
      call He2_TtTube(nu,Nhe2,He2tet,He2TET2
     &              ,He2QQH,He2QQC
     &              ,He2AlfInt,He2AlfTub,He2AreaInt,He2AreaTub
     &              ,NodTtk,He2Tt,C0He2,C1He2,C2He2
     &              ,He2T11,He2T12,He2T21,He2T22
     &              ,He2pr1,He2pr2,He2dzetQ
     &              ,He2prb11,He2prb12,He2prb21,He2prb22
     &              ,He2Nod1Int,He2Nod2Int,He2Nod1Tub,He2Nod2Tub
     &              ,He2Ttb1,C0He2b1,C1He2b1,C2He2b1
     &              ,He2Ttb2,C0He2b2,C1He2b2,C2He2b2 
     &              ,He2QWsufI,He2QWsufT,He2QqRadAd)
      tick(159,1) = time_df1()-time_tmp
      ErrorNum(1)=159

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13149 
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13244 
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_TEMP' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

      if(AlfaItFlag(1).eq.1) Then
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'

c   ���������� ����������� ������ ��� ������� ���������� �����
      time_tmp = time_df1()
        call WL1_Temp(nu,nwall,Wl1Node,C1serv,C2serv
     &               ,Wl1AlCulcNode,Wl1AlCulcEnv
     &               ,Wl1QqCl,Wl1QqEnv,NodQqWl
     &               ,NodTtk,Wl1Tt,Wl1TtEnv
     &               ,Wl1CfCulcEnv,Wl1CfCulcNode,Wl1CfAllEnv,
     &                Wl1IndRoom,NRm1,Rm1Qq,Wl1QqNodeSum)
      tick(380,1) = time_df1()-time_tmp
      ErrorNum(1)=380
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13328 
c   ���������� ������������� ������������� ���������� �� �������
      time_tmp = time_df1()
C        call WL1_alfaSq(Nv,Nu,Nwall,WL1Node
C     &                 ,Wl1CulcTyp,Wl1AlCulcNode,Wl1AlCulcEnv,Wl1QqCl
C     &            ,NodTt,NodPp,NodVsWt,NodVsVp,NodTtSat,NodXxBal,NodGgSumIn
C     &            ,WL1Thick,WL1DdEqv,WL1Sq,WL1SqEnv,Wl1SqCros
C     &            ,WL1AlfaNode,WL1AlfaEnv,WL1Tt,WL1Lamd,Wl1LamdC
C     &            ,NodBodyTyp,TypeBut )
       CALL WL1_alfaSq(Nv,Nu,Nwall,WL1Node
     &            ,Wl1CulcTyp,Wl1AlCulcNode,Wl1AlCulcEnv,Wl1QqCl
     &            ,NodTtk,NodPp,NodVsWt,NodVsVp,NodTtSat,NodXxBal,
     >NodGgSumIn
     &            ,WL1Thick,WL1DdEqv,WL1Sq,WL1SqEnv,Wl1SqCros
     &            ,WL1AlfaNode,WL1AlfaEnv,WL1Tt,WL1Lamd,Wl1LamdC
     &            ,NodBodyTyp,TypeBut
     - ,NODEEWV,NodEeWtSat,NodEeVPSat,NODRO,NodRoVpSat
     - ,NODROWTSAT,NodRoGa,NodcpGa,NodCgGa,VOLUKZ,NODSIGMASURF
     -,NODFLOWREGIME,ALFAFLAG(1),NodLaFr,ATYPPCRIT(1),Wl1alprev,NodVsGa
     &            ,Wl1CfCulcNode,Wl1CfCulcEnv,Wl1CfAllEnv,NodAlphaMin
     &            ,Wl1CfAllAlf
     & ,Wl1AlfaMinNode,Wl1DecCond,Wl1Decboil
     *          ,NodGgSumZeroIn,Wl1Robn,NodWaste_T,WasteNoGl)

      tick(381,1) = time_df1()-time_tmp
      ErrorNum(1)=381
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13354 
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      end if


****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13386 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13394 
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_TEMP_Met' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

c   ����� ��� ���� ������ �������
        IC=IC+1
        if(IC.lt.MaxIternew(1)) go=.true.
c\/\/\/\ ��������� �.�. (������) /\/\/\/\/\/\/\/\/\/\
      time_tmp = time_df1()
      if(dPMaxFlag(1)) then
       call dP_Max(Npres,nu,n03,n08,
     *      Pk,dP,ic,icint,11,NumMax,TypMax,dPMax,
     *      invperP,NodRelP,Tn3RelP,Tn8RelP
     *      ,n07,n09,Tn7RelP,Tn9RelP)
      call dM_Max(nu,Masdt,Volukz,ic,icint,2,
     *            NumMaxM,dMMax)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13474 
      endif
      tick(141,1) = time_df1()-time_tmp
      ErrorNum(1)=141
c\/\/\/\ ��������� �.�. (�����) /\/\/\/\/\/\/\/\/\/\

!      enddo
c   ����������� �������� ��� ��������� ������� �����������
      time_tmp = time_df1()
      call HydroCon_Out(nv,nu,Nf,
     *                  NHC1,HC1IndGg,Gk,HC1Gg,
     *                  HC1CfGg,NodPp,NodEe,HC1CfPp,HC1Pp,
     *                  HC1Ee,HC1Lb,IFROKZ,ITONKZ,
     *                  HC1Acon,HC1Bcon,Acon,Bcon,Hc1CcBor,
     *                  NodCcBor,
     *                  LinIbnFro,LinIbnTo,FlwAngFrom,FlwAngTo,
     *                  HFrom,HTo,
     *                  HC1PpGa,HC1PpVp,HC1TypPro,
     *                  HC1EeGa,HC1EeVpUp,HC1EeWt,HC1EeVpDo,
     *                  NodVsFr,
     *                  HC1VsGa,HC1VsVpUp,
     *                  HC1VsWt,HC1VsVpDo,NodEeWtSat,NodEeVpSat,
     *                  HC1EeWtSat,HC1EeVpSat,
     *                  NodRo,
     *                  HC1RoGa,HC1RoVpUp,HC1RoWt,
     *                  HC1RoVpDo,
     *                  HC1Lev,
     *                  HC1LevRel,HC1LevMas,HC1LevRelax,
     *                  HC1Hi,
     *                  HC1MasGa,HC1MasVpUp,HC1MasWt,HC1MasVpDo,
     *                  HC1AconFrom,HC1AconTo,AconFrom,AconTo,
     *                  HC1Type,HC1LbAlone,HC1GgOld,
     *                  HC1TtGa,HC1TtVpUp,HC1TtWt,HC1TtVpDo,
     *                  NodTt,
     *                  HC1XxBalWt,
     *                  NodXxBal,
     *                  HC1XxWt,NodXx,
     *                  Hc1CcGa,NodCcGa,Hc1CfAd,NodCfAd,Hc1dGdP,NoddGdP,
     *                  HC1CgGa,NodCgGa,NodEeGa,NodEeWv,
     *                  NodWaste_T,HC1Waste_T,NumWaste,NodDroDp,
     >Hc1DroDp,
     *                  NodRogg,HC1DtInt,dtint,HC1ConTun,
     *                        Hc1MasTot,coolermass00,
     *                        HC1MasTot0,
     *                        Hc1ConTun0,
     *                        MasdtIntegr_Wv,MasdtIntegr_ga,
     *                        HC1DebMas,
     *                        Hc1MasNod,
     *                        Volukz,
     *                      HC1DebLim,HC1DebLim0,
     *                      Hc1GgTun2,Hc1GgTun20,
     *                      HC1MasMax,NodMasWvMax,NodMasGaMax,
     *                      HC1GgZero,NodGgSumZero,
     *                      Hc1RelaxTun2,HC1FlagAut,
     *                      HC1GgTun2_tmp,HC1DebLim_tmp,
     *                      FlwCfTime,Hc1CfTime,
     *                      HC1HFrom,HC1HTo,
     *                      FlwSqMin,FlwCon,FlwLnSq,FlwCorSound,FlwKsi1,
     *                      FlwSq,HC1FlwSq,
     *                      HC1FlwSqMin,HC1FlwCon,HC1FlwLnSq,
     *                      HC1FlwCorS,HC1FlwKsi1,IC,
     *                      HC1FlwCfResL,FlwCfResL,HC1FlagAut0,
     *                      Npres,Pk1,
     *                      Hc1FlOne_tmp)

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13539 
      tick(146,1) = time_df1()-time_tmp
      ErrorNum(1)=146
C     call gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13553 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13628 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13700 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13704 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13710 

      end

C-----THIS IS THE END
****** End   file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET42.fs'
****** Begin file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET43.fs'

        subroutine gr_par_end
        implicit none
      include 'gr_par.fh'
      REAL*8 FlwTnSqFrom(44)
      COMMON/gr_par_nowrt/ FlwTnSqFrom
      REAL*8 FlwTnSqTo(44)
      COMMON/gr_par_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromUp(44)
      COMMON/gr_par_nowrt/ FlwHTnFromUp
      REAL*8 FlwHTnFromDo(44)
      COMMON/gr_par_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(44)
      COMMON/gr_par_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(44)
      COMMON/gr_par_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tc2Dim_TMax
      PARAMETER (Tc2Dim_TMax=2)
      INTEGER*4 Tn3Dim_TMax
      PARAMETER (Tn3Dim_TMax=2)
      INTEGER*4 INVPERe(51)
      COMMON/gr_par_nowrt/ INVPERe
      INTEGER*4 NZSUBe(540)
      COMMON/gr_par_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(52)
      COMMON/gr_par_nowrt/ INZSUBe
      INTEGER*4 ILNZe(52)
      COMMON/gr_par_nowrt/ ILNZe
      INTEGER*4 INVPERp(43)
      COMMON/gr_par_nowrt/ INVPERp
      INTEGER*4 NZSUBp(324)
      COMMON/gr_par_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(44)
      COMMON/gr_par_nowrt/ INZSUBp
      INTEGER*4 ILNZp(44)
      COMMON/gr_par_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=129)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=65)
      INTEGER*4 Nent
      PARAMETER (Nent=51)
      INTEGER*4 Npc
      PARAMETER (Npc=86)
      INTEGER*4 Npres
      PARAMETER (Npres=43)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(43)
      COMMON/gr_par_nowrt/ PIntIter
      LOGICAL*1 EIntIter(51)
      COMMON/gr_par_nowrt/ EIntIter
      LOGICAL*1 PExtIter(43)
      COMMON/gr_par_nowrt/ PExtIter
      LOGICAL*1 EextIter(51)
      COMMON/gr_par_nowrt/ EextIter
      CHARACTER*16 ConFlOutNames(1)
      COMMON/gr_par_nowrt/ ConFlOutNames
      !DEC$PSECT/gr_par_NOWRT/ NoWRT
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13723 
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
      integer*4 Ncomp,CompLin(1),CompOin(1),CompGenTyp(1)
      parameter(Ncomp=0)
      real*8 CompQq(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13731 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13736 


****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'

c      real*8 FlwCondRegul(nv)
c      common/gr_parFlwL/ FlwCondRegul
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13747 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13782 
      integer*4 nfwv
      parameter(nfwv=0)
      integer*4 FwvFloWw(1),FwvFloWv(1)
      real*8 FwvRoFrom1(1),FwvRoFrom2(1),FwvRoTo1(1),FwvRoTo2(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13788 
      integer*4 nfwv1
      parameter(nfwv1=0)
      integer*4 Fwv1Flow(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13793 
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13809 
* ���������� ��� ��������������� ��������� ����������
      real*8 C0He2(NHe2) !����. ��� ������� ����������� ���� ��
      real*8 C1He2(NHe2) !����. ��� ������� ����������� ���� ��
      real*8 C2He2(NHe2) !����. ��� ������� ����������� ���� ��
      real*8 DTHe2
      real*8 C0He2b1(NHe2),C1He2b1(NHe2),C2He2b1(NHe2)
      real*8 C0He2b2(NHe2),C1He2b2(NHe2),C2He2b2(NHe2)
      real*8 He2T1t(2,NHe2),He2T2t(2,NHe2),He2decQt(NHe2)
      real*8 He2vvGa(NHe2)
      common/gr_parHE2/ C0He2,C1He2,C2He2,DTHe2,
     * C0He2b1,C1He2b1,C2He2b1,C0He2b2,C1He2b2,C2He2b2,
     * He2T1t,He2T2t,He2decQt,He2vvGa
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13823 
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13834 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13841 

      integer*4 NMtl
      parameter ( NMtl=0 )
      real*8 MtlTt(1),MtlMass(1),MtlCp(1),MtlAlCulc(1)
      real*8 Mtl_QqOut(1)  !��������� �������� ����� � ������� ��������
      real*8 MtlLamdC(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13849 

      integer*4 NMtlO,MtlOIndRoom(1)
      parameter ( NMtlO=0 )
      real*8 MtlOTt(1),MtlOMass(1),MtlOCp(1),MtlOAlCulcWl1(1)
      real*8 MtlO_QqOut(1)  !��������� �������� ����� � ������� ��������
      real*8 MtlOLamdC(1),MtlOLamd(1),MtlOQqEnv(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13857 
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'

      integer*4 NUCH
      parameter (NUCH=0)
      integer*4 CoalHConNodInd(1)
      real*8 CoalHNodMoistG(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13866 

      integer*4 NFlwWasteSet
      parameter(NFlwWasteSet=0)
      integer*4 FlwWasteSetConnInd(1),FlwWasteSetNum(1)
      real*8 FlwWasteSetWaste(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13873 

      integer*4 FlwConnFlwInd(1),NFlwConn
      parameter(NFlwConn=0)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13878 

      real*8 HC1PpFrom(NHC1),HC1PpTo(NHC1)
      common/gr_parConFlOut/ HC1PpFrom,HC1PpTo
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13883 

      integer*4 NHCO
      parameter(NHCO=0)
      integer*4  HCOFlMod(1),HCOIndFlw(1)
      real*8 HCOCfMod(1),HCOOFg(1),HCOONodVol(1),HCOONoddRodP(1)
      common/gr_parConFlOut/ HCOFlMod,HCOCfMod,HCOOFg,HCOONodVol,
     >HCOONoddRodP,HCOIndFlw
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13891 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13898 

      integer*4 nCrv
      parameter(nCrv=0)
      real*8 CrvRes(1)
      integer*4 CrvFlw(nCrv)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13905 

      integer*4 nWst
      parameter(nWst=0)
      integer*4 WstNode(1)
      real*8    WSTConsN2(1),WSTConsO2(1),WSTConsH2O(1),
     *          WSTConsCO2(1),WSTConsSoot(1),WSTConsAsh(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13913 

      integer*4 NXS
      parameter(NXS=0)
      integer*4 XSFlagSetCc(1),XSFlSet(30,1),XSIndSetCc(1),
     >XSIndSetCc0(1)
      real*8    XSCcstr(30,1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13920 

      integer*4 ntFrB
      parameter(ntFrB=0)
      integer*4 tFrBNdInd(1),tFrBi(1)
      real*8    tFrBCcOld(100,1),tFrBTau(1)
      real*8    tFrBGIn(1),tFrBAa(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13928 

      integer*4 np4
      parameter(np4=0)
      integer*4 Pm4Nb(1)
      real*8 Pm4Res(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13935 

c ��������� �����. ��� ������� � ���������������, ���� �� (�������) ���
      integer*4 np
      parameter(np=0)
      integer*4 VNKZ(1)
      real*8 PumRes(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13943 
      integer*4 np2
      parameter(np2=0)
      integer*4 Pm2Nb(1)
      real*8 Pm2Res(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13949 
      integer*4 nFan
      parameter(nFan=0)
      integer*4 FanFlw(1)
      real*8 FanRes(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13955 
      integer*4 nFan2
      parameter(nFan2=0)
      integer*4 Fan2Flw(1)
      real*8 Fan2Res(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13961 
      integer*4 np3
      parameter(np3=0)
      real*8 Pm3QqNod(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13966 
      integer*4 nfr1
      parameter(nfr1=0)
      integer*4 Fr1Flw(1)
      real*8 Fr1Bor(1)
      real*8 Fr1KkFullWst(1)
      real*8 Fr1Waste_T(100,1)
      real*8 Fr1WstMass_T(100,1)
      integer*4 Fr1SumMas_0(1),Fr1FlSumMas(1),Fr1F0SumMas(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 13976 

      real*8 time_df1  !������� ��� ������� ������� (� �����)
      real*8 time_tmp

      real   deltat      !��� ����������
      real   deltat1      !��� ����������
      REAL*8 Atau        !1/dt
c      save Atau
      real*4 dtInt       !dt*Accel
      real*8 TimeSrv     !dt/Niter
      character*100 fn1  ! ��� ������������� .cbf ������
c      common/gr_parLocal1/ time_tmp,deltat,Atau,dtInt,TimeSrv,fn1
      common/gr_parLocal1/ Atau,time_tmp,deltat1,dtInt,TimeSrv,fn1
      real*8 VOL1(nu) !V/dt
      real*8 NodCfdE(nu) !����. � ��. ����. ��� ����� ���������
      real*8 NodTtk(nu)  !����������� ��������. � ������� ����� �� k-�� ��������
      real*8 RotVol(nu) !��������� ������
      real*8 RokVol(nu) !��������� ������
      real*8 NodVolk(nu)  !������������ ����� �� k-�� ��������
      real*8 NoddVdPk(nu) !����������� ������ � ���� �� �������� �� k-�� ����.

c ---  ������ �. (29.08.02) - ������
      real*8 NodSum1(nu)
      real*8 NodSum2(nu)
c ---  ������ �. (29.08.02) - �����

      real*8 NodQqPmAd(nu)

      common/gr_parNODE1/ VOL1,NodCfdE,NodTtk,RotVol,RokVol,
     * NodVolk,NoddVdPk,NodSum1,NodSum2,NodQqPmAd
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14008 

      real*8 C7(nv)   !��������� ������������
      real*8 Gk(nv)   !������� �� k-�� ��������
      real*8 dHLev(nv)!������ �� ����� �� ���� ������� � �������
      real*8 Rovk(nv)    !��. ��������� �� k-�� ��������
      real*8 AconRovk(nv)!��������� ��� ��������
c###### ��������� ####################################
      real*8 FlwGaVeSq(nv)  ! ��������� ������ ��� �������� ������������� �����
      real*8 flwbcon(nv)    ! �������� ��������� �������� �������� �� ����� 
      real*8 FlwC7work1(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work2(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work3(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work4(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work5(nv)   ! ��������� ������ ��� �������� ������������� �����
      real*8 FlwC7work7(nv)
      real*8 FlwVsk    (nv)   ! �������� �� ����� �� ���������
      real*8 ComEeFrom  (nv)   ! ��������� ��������� ��������� � ��������
      real*8 ComEeTo    (nv)   ! ��������� ��������� ��������� � ��������  
      real*8 FlwSlGaFrom(nv)   ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaFrom0(nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaTo   (nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwSlGaTo0  (nv)  ! �������� ��� ��������������� ���� �� �����
      real*8 FlwCcFrom(nv) ! ��������� ������ ��� ������������ ���� �� ������ �����
      real*8 FlwCcTo(nv)   ! ��������� ������ ��� ������������ ���� �� ������ �����
      real*8 flwdhz0(nv)
      real*8 ComRoFrom(3,nv)
      real*8 ComRoTo  (3,nv)
c###### ��������� ####################################
      real*8 FlwKgp(nf,2,nv)    ! 
      real*8 FlwKge(nf,nf,nv)
      real*8 FlwKgg(3,nv)
      real*8 FlwKgpt(2,nv)
      real*8 FlwdPLevFrom(nv),FlwdPLevTo(nv)
      real*8 FlwCcGgWtJa(nv)
      real*8 FlwRes_Iter(nv)
      real*8 FlwGG_TMP(nv)
      real*8 FlwRes_Tmp
      real*8 FlwdCgDCcFrom(nv),FlwdCgDCcTo(nv)
c      real*8 frco0(nv)
      common/gr_parFLOW/ C7,Gk,dHLev,Rovk,AconRovk,FlwGaVeSq,
     * flwbcon,FlwC7work1,
     * FlwC7work2,FlwC7work3,FlwC7work4,FlwC7work5,FlwC7work7,
     * FlwVsk,ComEeFrom,ComEeTo,FlwSlGaFrom,FlwSlGaFrom0,
     * FlwSlGaTo,FlwSlGaTo0,FlwCcFrom,FlwCcTo,flwdhz0,ComRoFrom,
     * ComRoTo,FlwKgp,FlwKge,FlwKgg,FlwKgpt,
     * FlwdPLevFrom,FlwdPLevTo,FlwCcGgWtJa,
     * FlwRes_Iter,FlwRes_Tmp,FlwGG_TMP,FlwdCgDCcFrom,FlwdCgDCcTo
c    * ,frco0
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14058 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14063 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14068 


c      !������� ����. ����� ����� � ���. ��������� ��� ��������
       real*8 Dmp(Npres,Npres)
C       real*8 Dmpc(Npc,Npc) ! �������� 
c      !������� ����. ����� ����� � ������� ��������� ��� ���������
       real*8 Dmh(Nent,Nent)
       real*8 DmhG(Nent,Nent)
c      !������ ����� ��� ���������� ������� �� ����.
      Real*8 Yp(Npres)
c      Real*8 Ypc(Npc)
c      !������ ����� ��� ���������� ������� �� ���.
      real*8 Ye(Nent)
      real*8 YeG(Nent)
c      !������� �� ���������
      real*8 dP(Npres)
c      real*8 dPC(Npc)
c      !������� �� ����������
      real*8 dE(Nent)
      real*8 dEG(Nent)
c      !������ ������ �������� �� k-�� ��������
      real*8 Pk(Npres)
c      !������ ������ �������� �� (k+1)-�� ��������
      real*8 Pk1(Npres)
c      !������ ������ ��������� �� k-�� ��������
      real*8 Ek(Nent)
      real*8 EGk(Nent)
c      !������ ������ ��������� �� (k+1)-�� ��������
      real*8 Ek1(Nent)
      real*8 EGk1(Nent)
      real*8 Rok(Nu) !�����. �\� ����� � ������� ����� �� k-�� ����.
      !������ ������ ��������� �����. ����
      real*8 EeWtSatk(Nent)
      !������ ������ ��������� �����. ����
      real*8 EeVpSatk(Nent)
      real*8 MaxDevP     !����. �����. ����. �������� �� ��������
      real*8 MaxDevH     !����. �����. ����. ��������� �� ��������
      INTEGER*4 I,j     !���������� �����
      integer*4 IC        !������� ��������
      integer*4 ICInt !������� ���������� ��������
      common/gr_parLocal2/ Dmp,Dmh,Yp,Ye,dP,dE,Pk,Pk1,Ek,Ek1,Rok,
     *  EeWtSatk,EeVpSatk,MaxDevP,MaxDevH,IC,ICInt,DmhG,YeG,dEG,
     *  EGk,EGk1       !,dPC,Ypc,Dmpc <=������ ��������!!!!

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14116 

      INTEGER*4 nvlgpos !����� ����� � �������������� ���������� � ����������
      common/gr_parVlvGeo/ nvlgpos
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14121 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14126 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14131 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14136 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14141 

      integer*4 nsep1,Sep1FlwS(1),Sep1Node(1)
        parameter(nsep1=0)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14146 

      integer*4 nInWst,InWstNum(1),InWstNode(1),InWstNdTp(1)
      real*8    InWstdt(1)
      integer*4 InWstTyp(1)
      real*8    InWstCc(1)
      real*8 InWstExtCon(1),InWstExtWstdt(1)
      parameter(nInWst=0)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14155 

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14160 


      LOGICAL*1 go,goint  !������� ����������� ��������
      DATA go/.true./     !����������� �������� �������. ��������
      common/gr_parLocal4/ go,goint
      real*8  RDTSC
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14199 
      real*8 NoddRodPk(nu) !dRo/dP ��� ������� ����� �� ����. ����.
      real*8 NoddRodEk(nu) !dRo/d� ��� ������� ����� �� ����. ����.
      real*8 NoddRodPk1(nu) !dRo/dP ��� ������� ����� �� ���. ����.
      real*8 NoddRodEk1(nu) !dRo/d� ��� ������� ����� �� ���. ����.
      real*8 nodrorelax(nu)
      real*8 NodRoGa0(nu)
      real*8 NodCcGa_Aeq0(nu) 
      real*8 NodCcGa_Deq0(nu) 
      real*8 NodCcGa_Beq (nu)
      real*8 NodCcGa_Beq0(nu)
      real*8 NodCcGa_Aeq (nu)
      real*8 NodCcGa_Deq (nu)
      real*8 NodXxk1(nu)    !���. �������. � ������� ����� �� ���. ����.
      real*8 NodXxBalk1(nu) !���. �������. � ������� ����� �� ���. ����.
      real*8 NodQqAd(nu)
      real*8 NodCfEeAsh(nu)
      real*8 NodEeWvt(nu)
      real*8 NodAlSqGaWv(nu),NodYe(nu),NodDmh(nu)
      real*8 NodPsw(nu),NodRoWvTmp(nu),NoddRdHGas(nu)
     *,NoddRdPWvTmp(nu),NoddRdHWvTmp(nu)
     *,NodTtWvTmp(nu),NodCpWvTmp(nu),NodXxTmp(nu)
      integer*4 Nod_ghm_flag(nu),NodPropRelaxFlag(nu)
      logical*1 EExtIter_(Nent)
      logical*1 PExtIter_(Npres)
      common /gr_parNODE/ NoddRodPk,NoddRodEk,NoddRodPk1,NoddRodEk1,
     *  nodrorelax,NodRoGa0,NodCcGa_Aeq0,NodCcGa_Deq0,NodCcGa_Beq,
     *  NodCcGa_Beq0,NodCcGa_Aeq,NodCcGa_Deq,NodXxk1,NodXxBalk1,
     *  NodQqAd,NodCfEeAsh,NodEeWvt,NodAlSqGaWv,NodYe,NodDmh,
     *  NodPsw,NodRoWvTmp,NoddRdHGas,NodTtWvTmp,NodCpWvTmp,NodXxTmp
      common /gr_parNODE_I/ Nod_ghm_flag,NodPropRelaxFlag
      common /gr_parNODE_L/ EExtIter_,PExtIter_
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14232 
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'

      integer NTc0
      parameter(NTc0=0)
      integer*4 Tc0IbnFro(1),Tc0NodFro(1),Tc0IbnTo(1),Tc0NodTo(1)
      real*8 Tc0zt(1),Tc0AlG23(1),Tc0AlG43(1)
     *,Tc0G43(1),Tc0G23(1),Tc0G34(1),Tc0CpWl(1)
     *,Tc0TtEnv(1),Tc0TtWlk(1),Tc0TtWl(1),Tc0AlSqEnv(1),Tc0AlEnv(1)
     *,Tc0AlSqPhWl(1,nf),Tc0MasCpWl(1),Tc0CfAlEnv(1)
     *,Tc0CfAlGaWl(1),Tc0CfAlWpUpWl(1)
     *,Tc0CfAlWtWl(1),Tc0CfAlWpDoWl(1)
     *,Tc0EvUpWl(1),Tc0EvDoWl(1)
     *,Tc0dG23dp(1),Tc0dG34dp(1),Tc0dG43dp(1)
     *,Tc0CfGgWpUpWt(1),Tc0CcGgWtWpDo(1),Tc0CfGgWpDoWt(1)
     *,Tc0G32(1),Tc0dG32dp(1),Tc0CfGgWt(1),Tc0SqWl(1),Tc0TettaQ(1,nf)
     *,Tc0QqIsp(1),Tc0AlIsp(1)
     *,Tc0dQIspdP(1),Tc0TtTnIsp(1),Tc0AlSqIsp(1),TC0ResWl(1)
     *,Tc0dQG23dp(1),Tc0TettaQ2(1,nf),Tc0DeltaTs(1)
     *,Tc0AlG23From(1),Tc0AlG43From(1)
     *,Tc0G23From(1),Tc0G34From(1),Tc0G32From(1),Tc0G43From(1)
     *,Tc0dG23dpFrom(1),Tc0dG34dpFrom(1),Tc0dG43dpFrom(1),
     >Tc0dG32dpFrom(1)
     *,Tc0dQIspdPFrom(1),Tc0TtTnIspFrom(1),Tc0AlSqIspFrom(1)
     *,Tc0CcGgWtWpDoFrom(1),Tc0CfGgWpUpWtFrom(1),Tc0CfGgWpDoWtFrom(1)
     *,Tc0QqIspFrom(1),Tc0AlIspFrom(1),Tc0CfGgWtFrom(1),Tc0TettaQFrom(1,
     >nf)
     *,Tc0AlSqPhWlFrom(1,nf),Tc0CfRelAlIsp(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14262 
C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14291 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14314 
      real*8 Tc2zt(NTc2),Tc2AlG23(NTc2),Tc2G23(NTc2),Tc2G34(NTc2)
     *,Tc2AlG43(NTc2),Tc2G43(NTc2),Tc2TtWlk(NTc2),Tc2AlPhWl(NTc2,nf)
     *,Tc2AlSqPhWl(NTc2,nf),Tc2dG23dp(NTc2),Tc2dG34dp(NTc2)
     *,Tc2dG43dp(NTc2),Tc2G32(Ntc2),Tc2dG32dp(Ntc2)
c     *,Tc2TettaQ(NTc2,nf)
     *,Tc2dQIspdP(NTc2),Tc2TtTnIsp(NTc2),Tc2AlSqIsp(NTc2)
      real*8 Tc2AlEnv_P(NTc2)
      common/gr_parTC2/ Tc2zt,Tc2AlG23,Tc2G23,Tc2G34
     *,Tc2AlG43,Tc2G43,Tc2TtWlk,Tc2AlPhWl
     *,Tc2AlSqPhWl,Tc2dG23dp,Tc2dG34dp,Tc2dG43dp,Tc2G32,Tc2dG32dp
c     *,Tc2TettaQ
     *,Tc2dQIspdP,Tc2TtTnIsp,Tc2AlSqIsp,Tc2AlEnv_P
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14328 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14338 

      integer*4 NTCR,TcRNode(1)
      parameter ( NTCR=0 )
      real*8 TcR_XQ(1),TcR_BQ(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14344 

      integer*4 Nwall2O
      parameter ( Nwall2O=0 )
      real*8  Wl2OTt(6,1),Wl2ORAw(1),Wl2OSqEnv(1),Wl2OQqBout(1),
     &        Wl2ODout(1)

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14352 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14358 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'

      integer NTen
      parameter(NTen=0)
      integer*4 TenIbn(1),TenNode(1)
      real*8 Tenzt(1),TenQqIn(1),TenAlG23(1),TenG23(1),TenG34(1)
     *,TenAlG43(1),TenG43(1),TenCpWl(1),TenTtWlk(1),TenTtWl(1)
     *,TenAlSqPhWl(1,nf),TenMasCpWl(1),TenCfAlGaWl(1),TenCfAlWpUpWl(1)
     *,TenCfAlWtWl(1),TenCfAlWpDoWl(1),TenEvUpWl(1),TenEvDoWl(1)
     *,TendG23dp(1),TendG34dp(1),TendG43dp(1)
     *,TenCfGgWpUpWt(1),TenCcGgWtWpDo(1),TenCfGgWpDoWt(1)
     *,TenG32(1),TendG32dp(1),TenCfGgWt(1),TenSqWl(1),TenTettaQ(1,nf)
     *,TenQqIsp(1),TenAlIsp(1),TenDeltaTs(1)
     *,TendQIspdP(1),TenTtTnIsp(1),TenAlSqIsp(1),TenResWl(1),
     >TenTcwAlCulc(1),TenTcwQq(1)
     *,TenLawL(1),TenQqInt(1),TenQqExt(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14381 
C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14395 
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14433 

c   ��������� ���������� ��� "����� �������"
*--- ����� ���������� ����������
      real*8 Tn3Pp(n03,Tn3Ne)    ! �������
      real*8 Tn3Ee(n03,Tn3Ne)    ! ���������
      real*8 Tn3Tt(n03,Tn3Ne)    ! �����������
      real*8 Tn3TtWl(n03,2)  ! ����������� ������
      real*8 Tn3TtEnv(n03,2) ! ����������� � ���������� ����������
      real*8 Tn3Mas(n03,Tn3Ne)   ! ����� ���
      real*8 Tn3rot(n03,Tn3Ne)   ! ��������� ���
      real*8 Tn3Volt(n03,Tn3Ne)  ! ������ ���
      real*8 Tn3Voldt(Tn3Ne,n03) ! ������ ���/dt
      real*8 Tn3MasGas(n03,3)! ����� ������������ �����
*---

      real*8 Tn3rok(n03,Tn3Ne)   ! ��������� �� ��������
      real*8 Tn3pk (n03,Tn3Ne)   ! �������� �� ��������
      real*8 Tn3ek (n03,Tn3Ne)   ! �������� �� ��������
      real*8 Tn3hw (n03,Tn3Ne)   ! ��������� ��������� ���� �� ��������
      real*8 Tn3hs (n03,Tn3Ne)   ! ��������� ��������� ���� �� ��������
      real*8 Tn3RoWtSatk(n03,Tn3Ne)   ! ��������� ��������� ���� �� ��������
      real*8 Tn3cp (n03,Tn3Ne)   ! ������������
      real*8 Tn3Volk(n03,Tn3Ne)  ! ������ ��� �� ��������
      real*8 Tn3TtWlk(n03,2) ! ����������� ������ �� ��������
      real*8 Tn3Mask(Tn3Ne,n03)  ! ����� ��� �� ���������
      real*8 Tn3MasGask(3,n03) ! ����� ������������ ����� �� ���������
      real*8 Tn3Volkdt(Tn3Ne,n03) ! ������ ���/dt �� ��������

      real*8 Tn3AlSqPhPh(Tn3Ne,Tn3Ne,n03) ! alfa*S ����� ������
      real*8 Tn3SqPhPh(Tn3Ne,Tn3Ne,n03) ! S ����� ������
      real*8 Tn3AlPhPh(Tn3Ne,Tn3Ne,n03) ! �-�� ������������� ����� ����� ������
      real*8 Tn3AlSqPhWl(n03,Tn3Ne)   ! �-�� ��������. ����� ������ � �������
      real*8 Tn3AlWlUpDo(n03)
      !����. ������� ��������. ����� ������ ����� ������ � ����� ������
      real*8 Tn3ActWlUpDo(n03)
      !�-�� ������������� ����� ������� � ����������
      real*8 Tn3AlSqWlEnv(n03,2)
      real*8 Tn3MasCpWl(n03,2)    ! �����*Cp ������

      real*8 Tn3drdp(n03,Tn3Ne)
      real*8 Tn3drdh(n03,Tn3Ne)
      real*8 Tn3dh(n03,Tn3Ne)

      real*8 Tn3G(Tn3Ne,Tn3Ne,n03) 
      real*8 Tn3dGdP(Tn3Ne,Tn3Ne,n03),Tn3dg32dp2(n03),Tn3dg32dp3(n03)
      real*8 Tn3Gw23(n03) 
      real*8 Tn3dGw23dP(n03) 
      real*8 Tn3Gw34(n03) 
      real*8 Tn3dGw34dP(n03) 
      real*8 Tn3Gw43(n03) 
      real*8 Tn3dGw43dP(n03) 
      real*8 Tn3Gw32(n03) 
      real*8 Tn3dGw32dP(n03) 
      real*8 Tn3G32Sum(n03)
      real*8 Tn3G34Sum(n03)
      real*8 Tn3G23Sum(n03)
      real*8 Tn3G43Sum(n03)

      real*8 Tn3G0(Tn3Ne,Tn3Ne,n03)
      real*8 Tn3CcGas(3,n03)
      real*8 Tn3AlGw23(n03)
      real*8 Tn3AlGw34(n03)
      real*8 Tn3AlGw43(n03)
      real*8 Tn3zd(n03)
      real*8 Tn3zu(n03)
      real*8 Tn3det(n03)
      real*8 Tn3zt(n03)
      real*8 Tn3Ts(n03,Tn3Ne)
      real*8 Tn3Ts_(n03,Tn3Ne)
      real*8 Tn3rv(Tn3Ne,n03)
      real*8 Tn3GSum(Tn3Ne,n03)
      real*8 Tn3MasgenMult(Tn3Ne,n03)
      real*8 Tn3Masgen(Tn3Ne,n03)
      real*8 Tn3AlGv23(n03)
      real*8 Tn3Gv23(n03)
      real*8 Tn3dGv23dP(n03)
 
      real*8 Tn3Pk2(n03,Tn3Ne)
      real*8 Tn3hw2(n03,Tn3Ne)
      real*8 Tn3hs2(n03,Tn3Ne)
      real*8 Tn3ts2(n03,Tn3Ne)
      real*8 Tn3ts2_(n03,Tn3Ne)
      real*8 Tn3Tt2(n03,Tn3Ne)
      real*8 Tn3ro2(n03,Tn3Ne)
      real*8 Tn3cp2(n03,Tn3Ne)
      real*8 Tn3hd2(n03,Tn3Ne)
      real*8 Tn3CcUp2(2,n03)
      real*8 Tn3Xxk(n03,Tn3Ne)
      logical*1 Tn3RelaxKG
      real*8 Tn3dp(n03,Tn3Ne)
      real*8 Tn3Mas0(n03,Tn3Ne)
c      real*8 Tn3CfMasGasMin ! /1.0d-7/
c      parameter (Tn3CfMasGasMin=1.0d-7)
      real*8 Tn3TettaQ(n03,Tn3Ne)
      real*8 Tn3Vk(n03,Tn3Ne)
      real*8 Tn3dTdH(n03,Tn3Ne)
      real*8 Tn3CcUp(2,n03)
      real*8 Tn3AVolDo(n03)
      real*8 Tn3TettaWl(n03)
      real*8 Tn3LevRelk(n03),Tn3LevMask(n03)
      real*8 Tn3QqPhPh(Tn3Ne,Tn3Ne,n03),Tn3QqGen(Tn3Ne,Tn3Ne,n03)
        real*8 Tn3y1(n03),Tn3y2(n03),Tn3z1(n03),Tn3z2(n03)
      real*8 Tn3MasGaRealk(n03),Tn3MasGaReal0(n03)
      real*8 Tn3TcwAlCulc(n03,2)
      real*8 Tn3La(n03,Tn3Ne)
      real*8 Tn3Cf42(n03)
      real*8 Tn3WlTtIsp(n03)
      real*8 Tn3HD(n03,Tn3Ne),Tn3Sigma(n03),Tn3Vel42(n03)
     *,Tn3Dd4(n03)
      real*8 Tn3FlwHUp(1,1)
      real*8 Tn3FlwHDo(1,1)
      real*8 Tn3VolNorm0(n03)
      real*8 Tn3dMLoc_tmp(Tn3Ne,n03)
      real*8 Tn3VolFull(n03)
         real*8 Tn3Pk_dev(n03)

      common/gr_parTn3/
     * Tn3Pp,Tn3Ee,Tn3Tt,Tn3TtWl,Tn3TtEnv,Tn3Mas,Tn3rot,Tn3Volt,Tn3Voldt
     * ,Tn3MasGas,Tn3rok,Tn3pk,Tn3ek,Tn3hw,Tn3hs,Tn3RoWtSatk,Tn3cp
     * ,Tn3Volk,Tn3TtWlk,Tn3Mask,Tn3MasGask,Tn3Volkdt,Tn3AlSqPhPh
     * ,Tn3SqPhPh,Tn3AlPhPh,Tn3AlSqPhWl,Tn3AlWlUpDo,Tn3ActWlUpDo
     * ,Tn3AlSqWlEnv,Tn3MasCpWl,Tn3drdp,Tn3drdh,Tn3dh,Tn3G,Tn3dGdP
     * ,Tn3dg32dp2,Tn3dg32dp3,Tn3Gw23,Tn3dGw23dP,Tn3Gw34,Tn3dGw34dP
     * ,Tn3Gw43,Tn3dGw43dP,Tn3Gw32,Tn3dGw32dP,Tn3G32Sum,Tn3G34Sum
     * ,Tn3G23Sum,Tn3G43Sum,Tn3G0,Tn3CcGas,Tn3AlGw23
     * ,Tn3AlGw34,Tn3AlGw43,Tn3zd,Tn3zu,Tn3det,Tn3zt,Tn3Ts,Tn3rv
     * ,Tn3GSum,Tn3MasgenMult,Tn3Masgen,Tn3AlGv23,Tn3Gv23,Tn3dGv23dP
     * ,Tn3Pk2,Tn3hw2,Tn3hs2,Tn3ts2,Tn3Tt2,Tn3ro2,Tn3cp2,Tn3hd2
     * ,Tn3CcUp2,Tn3Xxk,Tn3dp,Tn3Mas0,Tn3TettaQ,Tn3Vk,Tn3dTdH
     * ,Tn3CcUp,Tn3AVolDo,Tn3TettaWl,Tn3LevRelk,Tn3LevMask,Tn3QqPhPh
     * ,Tn3QqGen,Tn3y1,Tn3y2,Tn3z1,Tn3z2,Tn3MasGaRealk,Tn3MasGaReal0
     * ,Tn3TcwAlCulc,Tn3La,Tn3Cf42,Tn3WlTtIsp,Tn3HD,Tn3Sigma
     * ,Tn3Vel42,Tn3Dd4,Tn3FlwHUp,Tn3FlwHDo,Tn3VolNorm0,Tn3ts_,Tn3ts2_
     * ,Tn3dMLoc_tmp,Tn3VolFull,Tn3Pk_dev
      common/gr_parTn3L/ Tn3RelaxKG
      real*8 Tn3GSpCond(nv)
      real*8 Tn3QSpCond(nv)
      common/gr_parTn3Flow/ Tn3GSpCond,Tn3QSpCond
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14575 


****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'

      integer*4 n07,Tn7RelP(1,1),Tn7RelE(2,1),Tn7Homo(1),Tn7CcSetInd(1)
      real*8    Tn7RoGa(1),Tn7RoWt(1),Tn7Rok(1,2),Tn7XxWt(1),Tn7Xk(1,2),
     *          Tn7Lev(1),Tn7EeWt(1),Tn7VsGa(1),Tn7VsWt(1),Tn7PpGa(1),
     *          Tn7EeGa(1),Tn7EeWtSat(1),Tn7EeVpSat(1),Tn7LevRel(1),
     *          Tn7Hi(1),Tn7PpWt(1),Tn7TtWt(1),Tn7Ee(2,1),
     *          Tn7Ek(2,1),Tn7CcVolk(1),Tn7CcMask(1)
     *         ,Tn7XxBalWt(1),Tn7Levk(1),Tn7Lev_Rlx(1),Tn7Acon(1),
     *          Tn7Sq(1),Tn7CcBor(1),Tn7MasWt(1),Tn7MasGa(1),
     *          Tn7MasBor(1),Tn7GCorrWt(1),
     *          Tn7dRdPk(2,1),Tn7dRdP(2,1),Tn7dRdHk(2,1),Tn7dRdH(2,1),
     *          dP_Tn7(1),Tn7Dcon(1),Tn7LsMin(1),Tn7dMLoc(2,1),
     *          Tn7CfRoGaCorr(1),Tn7CfRoWtCorr(1),
     *          Tn7Waste_T(20,1),Tn7Rowttrue(1),Tn7RoWtSat(1),
     *          Tn7RoVpSat(1),Tn7Dmint(1),Tn7Gcorr(1),
     *          Tn7WlUpQqOut(1),Tn7WlDoQqOut(1),Tn7TcwAlCulc(1,1),
     *          Tn7TtWlUp(1),Tn7TtWlDo(1),Tn7LaWl(1),Tn7QqWlEnv(1),
     >Tn7RoomInd(1)
     *          ,Tn7EvDo(1),Tn7EvDo0(1),Tn7TtGa(1),Tn7AlSqPhWl(2,1),
     >Tn7AlWlUpDo(1)
     *          ,Tn7dMIntGa(1),Tn7dMIntWt(1),Tn7GCorrGa(1),
     >Tn7LevTunG(1),Tn7LevTunPpRegG(1)
      logical*1 Tn7_Flag_Mas_Deb(1),Tn7CfCorr_On(1)


      logical*1 Tn7CcBorLb(1)

      parameter(n07=0)
      real*8 Tn7Pk(2,1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14609 

C     common/gr_parC     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14666 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
      integer n08
      parameter(n08=0)
      integer*4 Tn8RelP(1,0),Tn8RelE(2,0),Tn8CcSetInd(1)
      real*8 Tn8RoWt(1),Tn8RoGa(1),Tn8GSumWt1(1),Tn8GSumWt2(1),
     *       Tn8Lev(1),Tn8RoWtk(1),Tn8PpWt(1),Tn8MasBor(1),Tn8ccBor(1),
     *       Tn8VolWt(1),Tn8XxWt(1),Tn8XxBalWt(1),
     *       Tn8MasWt(1),Tn8MasWtk(1),
     *       Tn8RoGak(1),Tn8Gsliv(1),Tn8EeGa(1),
     *       Tn8EeWt(1),Tn8PpGa(1),Tn8VsWt(1),Tn8VsGa(1),Tn8VsVp(1),
     *       Tn8EeWtSat(1),Tn8EeVpSat(1),Tn8LevRel(1),Tn8Hi(1),
     *       Tn8TtGa(1),Tn8TtWt(1),Tn8GgXx(1),
     *       Tn8dRdP1(1),Tn8dRdH1(1),Tn8dRdP2(1),Tn8dRdH2(1),
     *       Tn8Waste_T(20,1),Tn8RowtSat(1),Tn8RovpSat(1),
     *       Tn8QqEnv(1),Tn8RoomInd(1),Tn8TtMeDo(1)
     *       ,Tn8EvDo(1),Tn8EvDo0(1),Tn8GInOut(1),Tn8LevTunG(1)

      logical*1 Tn8CcBorLb(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14690 

C     common/gr_parC     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14727 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'

      integer n09
      parameter(n09=0)
      integer*4 Tn9RelP(2,1),Tn9RelE(Tn9Ne,1),Tn9CcSetInd(1)
      real*8 Tn9RoWt(1),Tn9Cp(1,1),Tn9Lev(1),
     *       Tn9Ek(1,1),Tn9Rok(1,Tn9Ne),Tn9PpWt(1),Tn9Pk(1,1),Tn9Rot(1,
     >Tn9Ne),
     *       Tn9Xxk(1,Tn9Ne),Tn9MasBor(1),Tn9ccBor(1),Tn9VolWt(1),
     *       Tn9dp(1,Tn9Ne),Tn9MasWt(1),Tn9GIspSum(1),Tn9Pp(1),
     *       Tn9GCondSum(1),Tn9G32sum(1),Tn9G34sum(1),Tn9G23sum(1),
     *       Tn9G43sum(1),Tn9TettaQ(1,Tn9Ne),Tn9Ev_t(1,1),Tn9Dim_t(1),
     *       Tn9Dim_tmax/1/,Tn9EvDo(1),Tn9EvDo0(1),Tn9FlwDdMin(1),
     >Tn9MasVpUp(1),
     *       Tn9MasVpDo(1),Tn9MasGa(1),Tn9LevMas(1),Tn9LevMask(1),
     >Tn9PpGa(1),
     *       Tn9PpVpUp(1),Tn9EeGa(1),Tn9EeVpUp(1),Tn9EeWt(1),
     *       Tn9EeVpDo(1),Tn9LevRelax(1),Tn9VsGa(1),Tn9VsVpUp(1),
     *       Tn9VsWt(1),Tn9VsVpDo(1),Tn9EeWtSatWt(1),Tn9EeVpSatWt(1),
     *       Tn9RoGa(1),Tn9ROVpUp(1),Tn9RoVpDo(1),Tn9LevRel(1),Tn9Hi(1),
     *       Tn9Volk(1,Tn9Ne),  ! ������ ��� �� ��������
     *       Tn9VsWtUp(1),Tn9VsWtJa(1),Tn9Ee(1,1),Tn9TtGa(1),
     *       Tn9TtVpUp(1),Tn9TtWt(1),Tn9TtVpDo(1),
     *       Tn9Mask(1,1),Tn9CcGas(1,1),
     *       Tn9CfMasGasMin(1)/7.5d-7/,
     *       Tn9CcUp(1,1),Tn9FlwHUp(1,1),Tn9FlwHDo(1,1),Tn9GSepSum(1),
     *       Tn9GFlwWtSum(1),Tn9MasGaRealk(1),Tn9dEInt(1,1),
     *       Tn9GCorr(1,1),Tn9drdp(1,1),Tn9drdh(1,1),Tn9vk(1,1),
     *       Tn9Waste_T(20,1),Tn9Dmint(1),Tn9MasWtUp(1),Tn9MasWtJa(1),
     >Tn9RoDo(1),
     *       Tn9WlUpQqOut(1),Tn9WlDoQqOut(1),Tn9TcwAlCulc(1,1),
     *       Tn9TtWlUp(1),Tn9TtWlDo(1),Tn9LaWl(1),
     *       Tn9QqWlEnvUp(1),Tn9QqWlEnvDo(1),Tn9RoomUpInd(1),
     >Tn9RoomDoInd(1),
     *       Tn9CcVpDo(1),Tn9pmax(1),Tn9TtUp(1),Tn9AlSqPhWl(6,1),
     >Tn9AlWlUpDo(1),
     *       Tn9TtSatWt_(1),Tn9Levadd(1),Tn9vol(1),Tn9VolCorrUp(1),
     >Tn9VolCorrDo(1),
     *       Tn9PressCorr(1),Tn9VolGa(1),Tn9FlwIntCoef(1),
     >Tn9VolVpUpLim(1),
     *       Tn9LevTunG(1),Tn9LevTunPpRegG(6,1)

      integer*4 Tn9FlwNum(1)
      logical*1 Tn9CcBorLb(1),Tn9VpInFlag(1),Tn9BorFl(1),
     >Tn9EntOut_Key(1),
     *          Tn9FlagGCorr(1),Tn9FlagGCorrDn(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14770 

C     common/gr_parC     common/gr_parC     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14914 


****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
      integer*4 Ntur1,Tr1Lin(1),tr2RLinTyp(1)
      parameter(ntur1=0)
      real*8    tr2FlwKsi1(1)
      real*8 Tr1Oin(1),Tr1Qsd(1),Tr1NdP(1),Tr1genTyp(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14926 
      integer*4 Ntur3
      parameter(Ntur3=0)
         integer*4 Tr3beaTyp(1),Tr3beaInd(1)
      real*8    Tr3Omdl(1),Tr3Qsd(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14932 
      integer*4 Ntur4
      parameter(Ntur4=0)
         integer*4 Tr4beaTyp(1),Tr4beaInd(1)
      real*8    Tr4Omdl(1),Tr4Qsd(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14938 



****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'

      real*8 C1serv(nwall) !����. ��� ������� ������. ������� ���
      real*8 C2serv(nwall) !����������� ���� � ���. ��. ����� ������
      real*8 wl1_QqOut(nwall)  !��������� �������� ����� � ������� ��������
      real*8 wl1AlCulcWl(nwall)   !��������� �������� ����� � ������� ��������
      real*8 wl1_Tt_t(nwall)
      real*8 wl1_XQ(nwall),wl1_BQ(nwall)  ! �-�� � ������� ���������
      common/gr_parWALL1/ C1serv,C2serv,wl1_QqOut,wl1AlCulcWl,
     *  wl1_Tt_t,wl1_XQ,wl1_BQ
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14958 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14972 

      integer*4 NRm1
      parameter ( NRm1=0 )
      real*8 Rm1Qq(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14978 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'

C     common/gr_parC file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 14998 

      integer*4 Nwall2
      parameter ( Nwall2=0 )
      real*8 wl2_QqOut(1)  !��������� �������� ����� � ������� ��������
      real*8 Wl2Tt_t(1)
      real*8 WL2mass(1),WL2Cp(1),WL2Tt(1)
      real*8 Wl2LamdC(1)
      integer*4 Wl2IndRoom(1),Wl2Ndet(1),Wl2Node(1)
      real*8 Wl2QqEnv(1),Wl2AlCulcWl2(1)
      real*8 Wl2Leff(1)
      real*8 Wl2Rb(1)
      real*8 Wl2Raw(1), Wl2SqEnv(1), Wl2QqBout(1), Wl2DOut(1)
      real*8 Wl2_XQ(1),Wl2_BQ(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15013 

****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      integer*4 Nwlcon2
      parameter ( Nwlcon2=0 )
      integer*4 WLC2Iwl(1)
      real*8 WLC2_Qq(1)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15022 




****** End   section 'DESCRIPTION' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      deltat=deltat1
ccc      call gr_parconnin

      InpMass (1) = 0.d0
      InpMass0(1) = 0.d0

      if(AlfaItFlag(1).eq.0) Then
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'

c   ���������� ����������� ������ ��� ������� ���������� �����
      time_tmp = time_df1()
        call WL1_Temp(nu,nwall,Wl1Node,C1serv,C2serv
     &               ,Wl1AlCulcNode,Wl1AlCulcEnv
     &               ,Wl1QqCl,Wl1QqEnv,NodQqWl
     &               ,NodTtk,Wl1Tt,Wl1TtEnv
     &               ,Wl1CfCulcEnv,Wl1CfCulcNode,Wl1CfAllEnv,
     &                Wl1IndRoom,NRm1,Rm1Qq,Wl1QqNodeSum)
      tick(380,1) = time_df1()-time_tmp
      ErrorNum(1)=380
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15112 
c   ���������� ������������� ������������� ���������� �� �������
      time_tmp = time_df1()
C        call WL1_alfaSq(Nv,Nu,Nwall,WL1Node
C     &                 ,Wl1CulcTyp,Wl1AlCulcNode,Wl1AlCulcEnv,Wl1QqCl
C     &            ,NodTt,NodPp,NodVsWt,NodVsVp,NodTtSat,NodXxBal,NodGgSumIn
C     &            ,WL1Thick,WL1DdEqv,WL1Sq,WL1SqEnv,Wl1SqCros
C     &            ,WL1AlfaNode,WL1AlfaEnv,WL1Tt,WL1Lamd,Wl1LamdC
C     &            ,NodBodyTyp,TypeBut )
       CALL WL1_alfaSq(Nv,Nu,Nwall,WL1Node
     &            ,Wl1CulcTyp,Wl1AlCulcNode,Wl1AlCulcEnv,Wl1QqCl
     &            ,NodTtk,NodPp,NodVsWt,NodVsVp,NodTtSat,NodXxBal,
     >NodGgSumIn
     &            ,WL1Thick,WL1DdEqv,WL1Sq,WL1SqEnv,Wl1SqCros
     &            ,WL1AlfaNode,WL1AlfaEnv,WL1Tt,WL1Lamd,Wl1LamdC
     &            ,NodBodyTyp,TypeBut
     - ,NODEEWV,NodEeWtSat,NodEeVPSat,NODRO,NodRoVpSat
     - ,NODROWTSAT,NodRoGa,NodcpGa,NodCgGa,VOLUKZ,NODSIGMASURF
     -,NODFLOWREGIME,ALFAFLAG(1),NodLaFr,ATYPPCRIT(1),Wl1alprev,NodVsGa
     &            ,Wl1CfCulcNode,Wl1CfCulcEnv,Wl1CfAllEnv,NodAlphaMin
     &            ,Wl1CfAllAlf
     & ,Wl1AlfaMinNode,Wl1DecCond,Wl1Decboil
     *          ,NodGgSumZeroIn,Wl1Robn,NodWaste_T,WasteNoGl)

      tick(381,1) = time_df1()-time_tmp
      ErrorNum(1)=381
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15138 
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'CALL_TEMP_WL' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
      end if


C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15164 

****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'

      time_tmp = time_df1()
      call Flw_BackEx(nv,GZ,Gk,ROVZ,Rovk,FlwVsk,FlwVs,FlwRo,FlwRoFrc,
     >Gzz)
      tick(154,1) = time_df1()-time_tmp
      ErrorNum(1)=154
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15177 


****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'Flw_BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'FlwdPPump' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'


      time_tmp = time_df1()
      call Node_BackEx(nu,Npres,Nent,NodRelP,NodRelE,NodPp,
     *                 Pk,NodEe,Ek,NodRo,Rok,NodEeWtSat,EeWtSatk,
     *                 NodEeVpSat,EeVpSatk,NodTyp,NodGgSum,Pzt,
     *                 Ezt,NodQqSum,NodTt,NodTtk,NodCp,NodCp,
     *                 NoddRodP,NoddRodPk,NoddRodE,NoddRodEk,
     *                 EnrdtIntegr,Qdt,nodEeGa,
     *                 nodccga,nodvvga,
     *                 NodEeGat,NodCcGat,NodVvGat,
     *                 VOLUKZ0,NodVolk,NodGgSum_ga,NodGgSum_Wv,
     *                 NodXx,NodXxk1,NodXxBal,NodXxBalk1,NodNumEe(1),
     *                 NodTtGa,NodTtGat,NodTtWv,NodTtWvt)

      tick(168,1) = time_df1()-time_tmp
      ErrorNum(1)=168


C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15364 

****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15382 
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'

      time_tmp = time_df1()
      call Tc2_BackEx(NTc2,Tc2TtWl,Tc2TtWlk,Tc2G32,Tc2GgWtWpUp
     *               ,Tc2AlGaWl,Tc2AlWpUpWl,Tc2AlWtWl,Tc2AlWpDoWl
     *               ,Tc2AlPhWl,Tc2TettaQ,Tc2TettaDo,Tc2QqEnv,Tc2QqEnv1)
      tick(189,1) = time_df1()-time_tmp
      ErrorNum(1)=189
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15395 
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15416 
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'

      time_tmp = time_df1()
      call Tn3_BackEx (n03,Tn3PpGa,Tn3PpWpUp,Tn3PpWt,Tn3PpWpDo,Tn3Pp
     *,Tn3EeGa,Tn3EeWpUp,Tn3EeWt,Tn3EeWpDo,Tn3Ee
     *,Tn3TtGa,Tn3TtWpUp,Tn3TtWt,Tn3TtWpDo,Tn3Tt
     *,Tn3TtWlUp,Tn3TtWlDo,Tn3TtWl,Tn3TtEnvUp,Tn3TtEnvDo,Tn3TtEnv
     *,Tn3MasGa,Tn3MasWpUp,Tn3MasWt,Tn3MasWpDo,Tn3Mas,Tn3Mas0
     *,Tn3RoGa,Tn3RoWpUp,Tn3RoWt,Tn3RoWpDo,Tn3Rot
     *,Tn3VolGa,Tn3VolWpUp,Tn3VolWt,Tn3VolWpDo,Tn3Volt
     *,Tn3Pk,Tn3TtWlk,Tn3Ek,Tn3Mask,Tn3Rok,Tn3Volk
     *,Tn3EeWtSatWt,Tn3EeWpSatWt,EeWtSatk,EeVpSatk,Tn3RelE,Nent
     *,Tn3Lev,Tn3LevRel,Tn3EvDo,Tn3G,Tn3GgWtWpUp,Tn3GgWpDoWpUp
     *,Tn3GgWpUpWt,Tn3gw23,Tn3GgWlWpUpWt
     *,Tn3QqWlEnvUp,Tn3AlSqWlEnv,Tn3QqWlEnvDo
     *,Tn3Gv23,Tn3GgGaWpUpWt,Tn3RoWtSatk,Tn3RoWtSatWt,Tn3TtUp
     *,Tn3CcUp,Tn3CcWpUp,Tn3GgWtWpDoBal
     *,Tn3Ts,Tn3TsWt,Tn3AlPhPh,Tn3AlGaWpUp,Tn3AlGaWt
     *,Tn3AlWpUpWt,Tn3AlWtWpDo,Tn3Ne,Tn3LevMask,Tn3LevMas
     *,Tn3TtSatWt,Tn3TtSatWpUp
     *,Tn3MasGaRealk,Tn3MasGaReal,Tn3MasGaReal0
     *,Tn3SqPhPh,Tn3Sq23,Tn3Sq34,Tn3QqPhPh,Tn3Al23,Tn3Al32,Tn3Al43
     *,Tn3Al34,dpmaxflag(1),Tn3TtRoom,Tn3PpRoom
     *,Tn3AlSqGaWl,Tn3AlSqWpUpWl,Tn3AlSqWtWl,Tn3AlSqWpDoWl,Tn3AlSqPhWl
     *,Tn3AlSq23,Tn3AlSq32,Tn3AlSq43,Tn3AlSq34,Tn3AlSqPhPh
     *,Tn3QqWtMi,Tn3QqVpUpMi,Tn3CfVH,Tn3CfVHR,Tn3LevRelOld
     *,Tn3ev_min_old,Tn3ev_min
     *,Tn3ev_max_old,Tn3ev_max
     *,Tn3vol_min_old,Tn3vol_min
     *,Tn3vol_max_old,Tn3vol_max
     *,Tn3TtSatWt_,Tn3Ts_,Tn3DebCorFl
     *,Tn3Cp,Tn3CpVpGa,Tn3CpWt)
      tick(262,1) = time_df1()-time_tmp
      ErrorNum(1)=262

      time_tmp = time_df1()
      call Tn3_GSum(n03,ntc2,Tc2Ibn,Tc2Node,Tn3GIspSum,Tn3GCondSum,Tn3G
     *,Tn3Gw34,Tn3Gw32,Tn3Gw43,Tn3Gw23,Tc2G32,Tc2G34,Tc2G43,Tc2G23
     *,Tn3G32sum,Tn3G34sum,Tn3G23sum,Tn3G43sum,Tn3GSpCondSum,Tn3Gv23
     *,ntc0,Tc0IbnTo,Tc0NodTo,Tc0G32,Tc0G34,Tc0G43,Tc0G23
     *,nTen,TenIbn,TenNode,TenG32,TenG34,TenG43,TenG23,Tn3Ne
     *,Tc0G23From,Tc0G34From,Tc0G43From,Tc0G32From
     *,Tc0IbnFro,Tc0NodFro)
      tick(263,1) = time_df1()-time_tmp
      ErrorNum(1)=263

      time_tmp = time_df1()
      call Tn3_Enrg(n03,Tn3EnrgSum,Tn3EnrgBal,Tn3dELoc,Tn3dEInt,Tn3mas
     *,Tn3Ee,Tn3Pp,Tn3Rot,Tn3AlSqPhWl,ntc2,Tc2AlSqPhWl,Tn3Tt,Tn3TtWl
     *,Tc2TtWl,nv,Tc2Ibn,Tc2Node,ifrokz,itonkz,linibnfro,linibnto,Gz
     *,FlwKgp,nu,Ek,nent,Tn3MasSum,Tn3MasBal,Tn3dMLoc,Tn3dMInt,dtint
     *,atau,Tn3QCorr,NodRelE,Tn3CfQCorr,FirstStepLabel
     *,Tn3Mas0,Tn3G32sum,Tn3G34sum,Tn3G23sum,Tn3G43sum,Tn3G
     *,Tn3GCorr,Tn3KeyGCorr,Tn3CfGCorr,Tn3TauGCorr,Tn3CfMasGasMin
     *,n08,Tn8RelE,FlwKge
     *,ntc0,Tc0IbnTo,Tc0NodTo,Tc0AlSqPhWl,Tc0TtWl,Tc2QqIsp,Tc0QqIsp
     *,nten,TenIbn,TenNode,TenAlSqPhWl,TenTtWl,TenQqIsp,Tn3Ne,nf
     *,Tn3QCorrUpDo,Tn9Ee,n09,Tn7Ee,n07
     *,Tn3QCorrPh,Tn3FlagQCorr
     *,Tn3dMLocSum,Tn3dMIntSum,Tn3GCorrSum,Tn3FlagGCorr,Tn3FlagGCorrGa
     *,Tn3MasGaReal,Tn3MasGaReal0,Tn3dMLocGa,Tn3dMIntGa
     *,FlwGgGa,comgamfrom,comgamto
     *,Tc0IbnFro,Tc0NodFro,Tc0AlSqPhWlFRom,Tc0QqIspFrom
     *,Tn3VolGa,Tn3FlagGCorrHup
     *,Tn3dMLocVpUp,Tn3dMIntVpUp,Tn3dMLocWt,Tn3dMIntWt
     *,Tn3MasgasPress,Tn3Hi,Tn3LevMas,Tn3cfdebGg,Tn3VolFull
     *,Tn3PressCorr,Tn3dMLoc_tmp,Tn3ddMLoc,Tn3K_loc,Tn3K_dif,Tn3K_int
     *,flag_first,Tn3cp
     *,FlwEeFrom,FlwEeTo,Tn3FlagGCorrDn,Tn3LevTunG,Tn3LevTunPpRegG)

       call Tn3_LevTun(n03,Tn3LevTunOn,Tn3LevTunOld,Tn3LevSet
     *,Tn3LevSetOld,Tn3LevRel,Tn3LevTunSign,Tn3LevTunG,Tn3VolWt,
     >Tn3Rok(1,3)
     *,Tn3Ev_T,Tn3Vol_T,Tn3Dim_TMax,Tn3Dim_T,Tn3Hi,Tn3PpWt,Tn3PpTun,
     >Tn3LevTunPpReg,Tn3Sq)
       call Tn39_LevTunPpReg(Tn3LevTunOn,Tn3LevTunPpReg,Tn3PpTun
     *,Tn3PpWt,Tn3LevTunPpRegG,Tn3MasGaReal,Tn3MasWpUp,n03,dtint)
      tick(264,1) = time_df1()-time_tmp
      ErrorNum(1)=264

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15499 

****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15557 

****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15584 

****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15673 


****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15697 

****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15719 

****** End   section 'BackEx' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

      time_tmp = time_df1()
      call Flw_Ee_v1(nv,n03,Nf,IFROKZ,ITONKZ,LinIBNfro,
     *               LinIBNto,NodEe,Tn8EeWt,Tn3Ek,
     *               FlwEeFrom,FlwEeTo,
     *               Tn7EeWt,n09,Tn9Ee,
     *               Tn7EeGa,ComGamFrom,ComGamTo,nu,n07,n08,
     *               NodEeGa,NodEeWv,NodEeVpSat,NodEeWtSat,
     *               FlwXxFrom,FlwXxTo)
      tick(142,1) = time_df1()-time_tmp
      ErrorNum(1)=142
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15734 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15739 

c --- �������� ��������� ���������� ��� �������
      time_tmp = time_df1()
        call gr_parMQNEW(dtInt)
      tick(144,1) = time_df1()-time_tmp
      ErrorNum(1)=144

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15757 
      do i = 1,nv
       FlwRes(i) = FlwRes(i)+FlwRes_Iter(i)
      enddo
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15762 

c --- ������������� �������� ����
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'

      if(Bor(1).ne.0) then
c   ��������� ��������� ������� Dmp (����� ������� ���������)
      time_tmp = time_df1()
      call CLR_S(Dmp, Npres, ILNZp, INZSUBp, NZSUBp)
      tick(149,1) = time_df1()-time_tmp
      ErrorNum(1)=149
      time_tmp = time_df1()
      call Bor_node_II(atau,AccelBor(1),Npres,nv,
     *     nu,n03,n07,n08,n09,
     *     IFROKZ,ITONKZ,LinIBNfro,LinIBNto,Gz,
     *     dmp,yp,NodCcBor,
     *     Tn3CcBor,Tn7CcBor,Tn8CcBor,
     *     Tn9CcBor,
     *     volukz,
     *     Tn3MasWt,Tn3MasWpDo,Tn3MasWpUp,Tn3MasGa,
     *     Tn3GIspSum,Tn3GCondSum,
     *     Tn7MasWt,Tn7MasGa,
     *     Tn8MasWt,
     *     Tn9MasWt,Tn9MasVpDo,Tn9MasVpUp,Tn9MasGa,
     *     Tn9GIspSum,Tn9GCondSum,
     *     NodRo,
     *     FlwXxFrom,FlwXxTo,ComGamFrom,ComGamTo,
     *     Nf,Bor,
     *     invperp,
     *     NodRelP,Tn3RelP,Tn7RelP,Tn8RelP,Tn9RelP,
     *     nfr1,Fr1Flw,Fr1Bor,
     *     Tn3BorFl,Tn9BorFl,
     *   ntFrB,tFrBNdInd,tFrBCcOld,tFrBGIn,tFrBAa,tFrBTau)
c      call Bor_comp(AccelBor(1),Npres,nu,n03,n07,n08,
c     *              n09,dmp,invperp,Bor,
c     *              NodRelP,NodXx,NodGgCompWvp,MasdtIntegr_Wv,
c     *              Tn3RelP,Tn3GCorr,
c     *              Tn7RelP,Tn7XxWt,Tn7GCorrWt,
c     *              Tn8RelP,Tn8XxWt,Tn8GgXx,Tn8GSliv,
c     *              Tn9RelP,Tn9GCorr)
      tick(150,1) = time_df1()-time_tmp
      ErrorNum(1)=150
      time_tmp = time_df1()

      call LINE_S(Npres,Dmp,Yp,dP,ILNZp,INZSUBp,NZSUBp)
      tick(151,1) = time_df1()-time_tmp
      ErrorNum(1)=151
      time_tmp = time_df1()

      call BorInNod_II(Npres,
     *nu,n03,n07,n08,n09,
     *NodTyp,dP,
     *NodCcBor,Tn3CcBor,
     *Tn7CcBor,Tn8CcBor,Tn9CcBor,
     *NodMasBor,Tn3MasBor,
     *Tn7MasBor,Tn8MasBor,Tn9MasBor,
     *NodRelP,Tn3RelP,
     *Tn7RelP,Tn8RelP,Tn9RelP,
     *invperp,ExtCcBor(1),ExtCcBorLb(1),NodCcBorLb,
     *Tn3CcBorLb,
     *Tn7CcBorLb,Tn8CcBorLb,Tn9CcBorLb)
      tick(152,1) = time_df1()-time_tmp
      ErrorNum(1)=152
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15830 
      endif
      call BorSetNod(Npres,
     *nu,n03,n07,n08,n09,
     *NodCcBor,Tn3CcBor,
     *Tn7CcBor,Tn8CcBor,Tn9CcBor,
     *NodMasBor,Tn3MasBor,
     *Tn7MasBor,Tn8MasBor,Tn9MasBor,
     *ExtCcBor(1),ExtCcBorLb(1),NodCcBorLb,
     *Tn3CcBorLb,
     *Tn7CcBorLb,Tn8CcBorLb,Tn9CcBorLb)

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 15845 
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'BOR' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'


      call TransWst(NTRWST,nu,TransWstNod,TransWstType,
     *      TransWst01,TransWst02,TransWst03,TransWst04,
     *      TransWst05,TransWst06,TransWst07,TransWst08,
     *      TransWst09,TransWst10,
     *      TransWst11,TransWst12,TransWst13,TransWst14,
     *      TransWst15,TransWst16,TransWst17,TransWst18,
     *      TransWst19,TransWst20,
     *      TransWst21,TransWst22,TransWst23,TransWst24,
     *      TransWst25,TransWst26,TransWst27,TransWst28,
     *      TransWst29,TransWst30,
     *      TransWst31,TransWst32,TransWst33,TransWst34,
     *      TransWst35,TransWst36,TransWst37,TransWst38,
     *      TransWst39,TransWst40,
     *      TransWst41,TransWst42,TransWst43,TransWst44,
     *      TransWst45,TransWst46,TransWst47,TransWst48,
     *      TransWst49,TransWst50,
     *      TransWst51,TransWst52,TransWst53,TransWst54,
     *      TransWst55,TransWst56,TransWst57,TransWst58,
     *      TransWst59,TransWst60,
     *      TransWst61,TransWst62,TransWst63,TransWst64,
     *      TransWst65,TransWst66,TransWst67,TransWst68,
     *      TransWst69,TransWst70,
     *      TransWst71,TransWst72,TransWst73,TransWst74,
     *      TransWst75,TransWst76,TransWst77,TransWst78,
     *      TransWst79,TransWst80,
     *      TransWst81,TransWst82,TransWst83,TransWst84,
     *      TransWst85,TransWst86,TransWst87,TransWst88,
     *      TransWst89,TransWst90,
     *      TransWst91,TransWst92,TransWst93,TransWst94,
     *      TransWst95,TransWst96,TransWst97,TransWst98,
     *      TransWst99,TransWst100,
     *      NodWaste_T,TransSumWst,NodRo,NodCcGa,TransSumWstV,
     *      WasteTyp_T,TransWstFlRo,TransWstRo,
     *            n08,Tn8Waste_T,Tn8RoWt,Tn8RoGa,Tn8LevRel,
     *            n07,Tn7Waste_T,Tn7RoWt,Tn7RoGa,Tn7LevRel,
     *            n03,Tn3Waste_T,Tn3RoDo,Tn3RoWpUp,Tn3RoGa,Tn3LevRel,
     *            n09,Tn9Waste_T,Tn9RoDo,Tn9RoVpUp,Tn9RoGa,Tn9LevRel,
     >TransWstL)
      ErrorNum(1)=1523
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16024 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16059 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16095 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16131 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16167 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16203 
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'WASTEINOUT' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'

       call wasteset(NumWaste,nu,nv,n03,n07,n08,n09,Nf,
     *             NodTyp,IFROKZ,ITONKZ,LinIBNfro,LinIBNto,GZ,
     *             RovZ,FlwLl,FlwSq,dtInt, 
     *             WasteTyp_T,WasteKkVp_T,WasteMax_T,
     *             ComGamFrom,ComGamTo,
     *             NodWaste_T,Tn3Waste_T,Tn7Waste_T,Tn8Waste_T,
     *             Tn9Waste_T,
     *             NodRo,Volukz,NodCcGa,
     *             Tn3MasWt,Tn3MasWpDo,Tn3MasWpUp,Tn3MasGa,
     *             Tn7MasWt,Tn7MasGa,Tn8MasWt,
     *             Tn9MasWt,Tn9MasVpDo,Tn9MasVpUp,Tn9MasGa,
     *             Waste_dt_1,Waste_accel,
     *             nInWst,InWstNum,InWstNode,InWstNdTp,InWstdt,
     *             WasteIO_T,
     *             Nwst,WSTNode,WasteNoGas,
     *             WSTConsN2,WSTConsO2,WSTConsH2O,WSTConsCO2,
     *             WSTConsSoot,WSTConsAsh,
     *       nfr1,Fr1flw,Fr1Waste_T,InWstTyp,InWstCc,Fr1WstMass_T,
     *             NFlwWasteSet,FlwWasteSetConnInd,FlwWasteSetNum,
     *             FlwWasteSetWaste,FlwConnFlwInd,Fr1KkFullWst,
     * NUCH,CoalHConNodInd,CoalHNodMoistG,InWstExtCon,InWstExtWstdt,
     *             WasteAa_T,WasteFlAa_T,Fr1SumMas_0,Fr1FlSumMas,
     *             Fr1F0SumMas,
     * NXS,XSFlagSetCc,XSFlSet,XSCcstr,NodCcSetInd,XSIndSetCc0,
     * Tn3CcSetInd,Tn7CcSetInd,Tn8CcSetInd,Tn9CcSetInd,XSIndSetCc,
     * Tn3GispSum,Tn3GcondSum,Tn9GispSum,Tn9GcondSum)
      ErrorNum(1)=1521
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16308 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16320 


C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16332 

****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'WASTE' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Bor.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Comp.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Flow_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\He2_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Mtl_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET4_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Node_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC0_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TC2_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcR_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TcW2.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Ten_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn3_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn7_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn8_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tn9_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\TransTH.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
c ��������� � �������� ����������� �������
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16412 

c ������ ���������� �������� ������� ����� ������� �� ���������
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16424 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16431 
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Tur.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\waste.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl1_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\Wl2_L.fsd'
****** Begin section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'
****** End   section 'TurPostStep' of file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\WlCon2.fsd'

      if(dPMaxFlag(1)) 
     *      call ColorNode(nu,
     *           ClrType,ClrScale,ClrKey,ClrTypeold,
     *           ClrNum0,ClrNum1,NodClr,NodClr1,
     *           NodPp,NodEv,NodTt,ClrPmax,ClrPmin,ClrHmax,ClrHmin,
     *           ClrTmax,ClrTmin,ClrSmax,ClrSmin,NodCcGa,NodWaste_T)

c   ����������� �������� ��� ��������� ������� �����������

      time_tmp = time_df1()
      call HydroCon_Out(nv,nu,Nf,
     *                  NHC1,HC1IndGg,Gz,HC1Gg,
     *                  HC1CfGg,NodPp,NodEe,HC1CfPp,HC1Pp,
     *                  HC1Ee,HC1Lb,IFROKZ,ITONKZ,
     *                  HC1Acon,HC1Bcon,Acon,Bcon,Hc1CcBor,
     *                  NodCcBor,
     *                  LinIbnFro,LinIbnTo,FlwAngFrom,FlwAngTo,
     *                  HFrom,HTo,
     *                  HC1PpGa,HC1PpVp,HC1TypPro,
     *                  HC1EeGa,HC1EeVpUp,HC1EeWt,HC1EeVpDo,
     *                  NodVsFr,
     *                  HC1VsGa,HC1VsVpUp,
     *                  HC1VsWt,HC1VsVpDo,NodEeWtSat,NodEeVpSat,
     *                  HC1EeWtSat,HC1EeVpSat,
     *                  NodRo,
     *                  HC1RoGa,HC1RoVpUp,HC1RoWt,
     *                  HC1RoVpDo,
     *                  HC1Lev,
     *                  HC1LevRel,HC1LevMas,HC1LevRelax,
     *                  HC1Hi,
     *                  HC1MasGa,HC1MasVpUp,HC1MasWt,HC1MasVpDo,
     *                  HC1AconFrom,HC1AconTo,AconFrom,AconTo,
     *                  HC1Type,HC1LbAlone,HC1GgOld,
     *                  HC1TtGa,HC1TtVpUp,HC1TtWt,HC1TtVpDo,
     *                  NodTt,
     *                  HC1XxBalWt,
     *                  NodXxBal,
     *                  HC1XxWt,NodXx,
     *                  Hc1CcGa,NodCcGa,Hc1CfAd,NodCfAd,Hc1dGdP,NoddGdP,
     *                  HC1CgGa,NodCgGa,NodEeGa,NodEeWv,
     *                  NodWaste_T,HC1Waste_T,NumWaste,NodDroDp,
     >Hc1DroDp,
     *                  NodRogg,HC1DtInt,dtint,HC1ConTun,
     *                        Hc1MasTot,coolermass00,
     *                        HC1MasTot0,
     *                        Hc1ConTun0,
     *                        MasdtIntegr_Wv,MasdtIntegr_ga,
     *                        HC1DebMas,
     *                        Hc1MasNod,
     *                        Volukz,
     *                      HC1DebLim,HC1DebLim0,
     *                      Hc1GgTun2,Hc1GgTun20,
     *                      HC1MasMax,NodMasWvMax,NodMasGaMax,
     *                      HC1GgZero,NodGgSumZero,
     *                      Hc1RelaxTun2,HC1FlagAut,
     *                      HC1GgTun2_tmp,HC1DebLim_tmp,
     *                      FlwCfTime,Hc1CfTime,
     *                      HC1HFrom,HC1HTo,
     *                      FlwSqMin,FlwCon,FlwLnSq,FlwCorSound,FlwKsi1,
     *                      FlwSq,HC1FlwSq,
     *                      HC1FlwSqMin,HC1FlwCon,HC1FlwLnSq,
     *                      HC1FlwCorS,HC1FlwKsi1,777,
     *                      HC1FlwCfResL,FlwCfResL,HC1FlagAut0,
     *                      Npres,Pk,
     *                      Hc1FlOne_tmp)

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16512 
c      tick(146,1) = time_df1()-time_tmp
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16524 
c --- �������� �������� ���������� ��� ����� � ��������
      time_tmp = time_df1()
      call gr_parconnOut
      tick(148,1) = time_df1()-time_tmp
      ErrorNum(1)=148

      call TransH_Back_Ex(ntrh,transhnodtype,transhnodind
     *,TransHLev,TransHOffset,n03,n07,n08,n09
     *,Tn3LevRel,Tn7LevRel,Tn8LevRel,Tn9LevRel
     *,TransHLevMas,Tn3CcWpDo,Tn9CcVpDo
     *,Tn3EvDo,Tn9EvDo,Tn7EvDo,Tn8EvDo
     *,Tn3EvDo0,Tn9EvDo0,Tn7EvDo0,Tn8EvDo0
     *,TransHLevFiz,TransHKey,TransHLim)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16539 

      call TransPTn_Back_Ex(ntrptn,transptnnodtype,transptnnodind
     *,TransPTnP,TransPTnLev
     *,n03,n07,n08,n09,Tn3LevRel,Tn7LevRel,Tn8LevRel,Tn9LevRel
     *,Tn3RoDo,Tn7RoWt,Tn8RoWt,Tn9RoDo
     *,Tn3PpWt,Tn7PpGa,Tn8PpGa,Tn9PpWt
     *,Tn3RoGa,Tn3RoWpUp,Tn7RoGa,Tn8RoGa,Tn9RoGa,Tn9RoVpUp)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16548 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16561 

      call TransTN_Back_Ex(ntrtn,nu,TransTNTemp,TransTNNodInd,
     &     NodTt,NodPp,NodEe,NodCcga,NodEeWv,NodRo,NodEeGa,NodCpWv,
     &     TransTNCorSl,TransTNFlag,TransTNTau,dtint,
     *     NodPropsKey,NodePropsKey)

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16569 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16575 

      call TransP_nod(nu,NTRPN,TransPNone,TransPNTyp,
     *           TransPNRoomFl,TransPN_Pp,TransPNTtEnv,TransPNPpEnv,
     *           NodPp,NodEv,TransPNEvRel,NodRoTot)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16581 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16586 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16592 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16603 

c-------------���㫥��� ���࣮�뤥�����-----vvvvvvv
      if(FlagNulQq(1)) call Nul_Qq(nu,NodQqAdd)   !
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16608 
      if(FlagNulQq(1)) call Nul_Qq(nwall,Wl1Qq)   !
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16611 
      if(FlagNulQq(1)) FlagNulQq(1)=.false.       !
c-------------���㫥��� ���࣮�뤥�����-----^^^^^^^

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16627 
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16643 

      call ConNodTnk_Out(nv,nu,n03,NCNTO,
     *       CNTOIndFlw,CNTONodOut,CNTONodIn,CNTOFlIO,CNTOFlOI,
     *       CNTONodPp,CNTONodEeWv,CNTONodEeGa,CNTONodCcGa,
     *       NodPp,NodEeWv,NodEeGa,NodCcGa,NodCcBor,NodTt,
     *       NodWaste_T,CNTONodCcBor,CNTOONodCcBor,Tn3TtWt,
     *CNTOWaste_01,CNTOWaste_02,CNTOWaste_03,CNTOWaste_04,CNTOWaste_05,
     *CNTOWaste_06,CNTOWaste_07,CNTOWaste_08,CNTOWaste_09,CNTOWaste_10,
     *CNTOWaste_11,CNTOWaste_12,CNTOWaste_13,CNTOWaste_14,CNTOWaste_15,
     *CNTOWaste_16,CNTOWaste_17,CNTOWaste_18,CNTOWaste_19,CNTOWaste_20,
     *CNTOWaste_21,CNTOWaste_22,CNTOWaste_23,CNTOWaste_24,CNTOWaste_25,
     *CNTOWaste_26,CNTOWaste_27,CNTOWaste_28,CNTOWaste_29,CNTOWaste_30,
     *CNTOWaste_31,CNTOWaste_32,CNTOWaste_33,CNTOWaste_34,CNTOWaste_35,
     *CNTOWaste_36,CNTOWaste_37,CNTOWaste_38,CNTOWaste_39,CNTOWaste_40,
     *CNTOWaste_41,CNTOWaste_42,CNTOWaste_43,CNTOWaste_44,CNTOWaste_45,
     *CNTOWaste_46,CNTOWaste_47,CNTOWaste_48,CNTOWaste_49,CNTOWaste_50,
     *CNTOWaste_51,CNTOWaste_52,CNTOWaste_53,CNTOWaste_54,CNTOWaste_55,
     *CNTOWaste_56,CNTOWaste_57,CNTOWaste_58,CNTOWaste_59,CNTOWaste_60,
     *CNTOWaste_61,CNTOWaste_62,CNTOWaste_63,CNTOWaste_64,CNTOWaste_65,
     *CNTOWaste_66,CNTOWaste_67,CNTOWaste_68,CNTOWaste_69,CNTOWaste_70,
     *CNTOWaste_71,CNTOWaste_72,CNTOWaste_73,CNTOWaste_74,CNTOWaste_75,
     *CNTOWaste_76,CNTOWaste_77,CNTOWaste_78,CNTOWaste_79,CNTOWaste_80,
     *CNTOWaste_81,CNTOWaste_82,CNTOWaste_83,CNTOWaste_84,CNTOWaste_85,
     *CNTOWaste_86,CNTOWaste_87,CNTOWaste_88,CNTOWaste_89,CNTOWaste_90,
     *CNTOWaste_91,CNTOWaste_92,CNTOWaste_93,CNTOWaste_94,CNTOWaste_95,
     *CNTOWaste_96,CNTOWaste_97,CNTOWaste_98,CNTOWaste_99,
     *CNTOWaste_100,
     *CNTOGz,Gz,FlwPpFrom,ComGamFrom,LinIBNfro,CNTONodTt,
     *CNTOONodPp,CNTOONodEeWv,CNTOONodEeGa,CNTOONodCcGa,CNTOONodTt,
     *CNTOOWaste_01,CNTOOWaste_02,CNTOOWaste_03,CNTOOWaste_04,
     *CNTOOWaste_05,CNTOOWaste_06,CNTOOWaste_07,CNTOOWaste_08,
     *CNTOOWaste_09,CNTOOWaste_10,
     *CNTOOWaste_11,CNTOOWaste_12,CNTOOWaste_13,CNTOOWaste_14,
     *CNTOOWaste_15,CNTOOWaste_16,CNTOOWaste_17,CNTOOWaste_18,
     *CNTOOWaste_19,CNTOOWaste_20,
     *CNTOOWaste_21,CNTOOWaste_22,CNTOOWaste_23,CNTOOWaste_24,
     *CNTOOWaste_25,CNTOOWaste_26,CNTOOWaste_27,CNTOOWaste_28,
     *CNTOOWaste_29,CNTOOWaste_30,
     *CNTOOWaste_31,CNTOOWaste_32,CNTOOWaste_33,CNTOOWaste_34,
     *CNTOOWaste_35,CNTOOWaste_36,CNTOOWaste_37,CNTOOWaste_38,
     *CNTOOWaste_39,CNTOOWaste_40,
     *CNTOOWaste_41,CNTOOWaste_42,CNTOOWaste_43,CNTOOWaste_44,
     *CNTOOWaste_45,CNTOOWaste_46,CNTOOWaste_47,CNTOOWaste_48,
     *CNTOOWaste_49,CNTOOWaste_50,
     *CNTOOWaste_51,CNTOOWaste_52,CNTOOWaste_53,CNTOOWaste_54,
     *CNTOOWaste_55,CNTOOWaste_56,CNTOOWaste_57,CNTOOWaste_58,
     *CNTOOWaste_59,CNTOOWaste_60,
     *CNTOOWaste_61,CNTOOWaste_62,CNTOOWaste_63,CNTOOWaste_64,
     *CNTOOWaste_65,CNTOOWaste_66,CNTOOWaste_67,CNTOOWaste_68,
     *CNTOOWaste_69,CNTOOWaste_70,
     *CNTOOWaste_71,CNTOOWaste_72,CNTOOWaste_73,CNTOOWaste_74,
     *CNTOOWaste_75,CNTOOWaste_76,CNTOOWaste_77,CNTOOWaste_78,
     *CNTOOWaste_79,CNTOOWaste_80,
     *CNTOOWaste_81,CNTOOWaste_82,CNTOOWaste_83,CNTOOWaste_84,
     *CNTOOWaste_85,CNTOOWaste_86,CNTOOWaste_87,CNTOOWaste_88,
     *CNTOOWaste_89,CNTOOWaste_90,
     *CNTOOWaste_91,CNTOOWaste_92,CNTOOWaste_93,CNTOOWaste_94,
     *CNTOOWaste_95,CNTOOWaste_96,CNTOOWaste_97,CNTOOWaste_98,
     *CNTOOWaste_99,CNTOOWaste_100, 
     *Tn3Waste_T,Tn3EeWpUp,Tn3EeWt,Tn3EeWpDo,Tn3EeGa,Tn3CcBor,
     *IFROKZ,ITONKZ)
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16706 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16768 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16822 

C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16847 
c
      call Sock(nSock,nv,FlwFromSoc,FlwFromISoc,
     *                 FlwToSoc,FlwToISoc,SockWvGa,SockColFl(1))
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16852 
c
C file D:\PTKATOM\MODEL\MODEL\SHK_PART\PROCESS\SHK\KPG\work\gr_par.tms str 16857 
      ErrorNum(1)=0

      return
      END



****** End   file 'D:\PTKATOM\MODEL\ENICAD\TG_CAD\HYDRO\NET43.fs'

      subroutine gr_parConnIn
      implicit none
      include 'gr_par.fh'

      Call gr_par_ConIn
      return
      end

      subroutine gr_parConnOut
      Implicit none
      include 'gr_par.fh'

      return
      end
