      Subroutine vu_l(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'vu_l.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R_(41)=R8_eke
C vu_l.fgi(  62,  79):pre: �������������� �����  
      R_(37)=R8_ofe
C vu_l.fgi(  62,  63):pre: �������������� �����  
      R_(33)=R8_ibe
C vu_l.fgi(  62,  40):pre: �������������� �����  
      R_(29)=R8_ux
C vu_l.fgi(  62,  24):pre: �������������� �����  
      R_(25)=R8_ot
C vu_l.fgi( 153,  78):pre: �������������� �����  
      R_(21)=R8_at
C vu_l.fgi( 153,  62):pre: �������������� �����  
      R_(17)=R8_up
C vu_l.fgi( 153,  42):pre: �������������� �����  
      R_(13)=R8_ep
C vu_l.fgi( 153,  26):pre: �������������� �����  
      R_(9)=R8_al
C vu_l.fgi( 245,  39):pre: �������������� �����  
      R_(5)=R8_ik
C vu_l.fgi( 245,  23):pre: �������������� �����  
      !��������� R_(1) = vu_lC?? /40e3/
      R_(1)=R0_e
C vu_l.fgi( 134, 338):���������
      !��������� R_(2) = vu_lC?? /0.0/
      R_(2)=R0_i
C vu_l.fgi( 134, 340):���������
      if(L_o) then
         R8_ire=R_(1)
      else
         R8_ire=R_(2)
      endif
C vu_l.fgi( 137, 338):���� RE IN LO CH7
      if(R8_ire.le.R0_ere) then
         R8_upe=R0_are
      elseif(R8_ire.gt.R0_ope) then
         R8_upe=R0_ipe
      else
         R8_upe=R0_are+(R8_ire-(R0_ere))*(R0_ipe-(R0_are)
     &)/(R0_ope-(R0_ere))
      endif
C vu_l.fgi( 200, 340):��������������� ���������
      !��������� R_(3) = vu_lC?? /40e3/
      R_(3)=R0_u
C vu_l.fgi( 134, 351):���������
      !��������� R_(4) = vu_lC?? /0.0/
      R_(4)=R0_ad
C vu_l.fgi( 134, 353):���������
      if(L_ed) then
         R8_ose=R_(3)
      else
         R8_ose=R_(4)
      endif
C vu_l.fgi( 137, 351):���� RE IN LO CH7
      if(R8_ose.le.R0_ise) then
         R8_ase=R0_ese
      elseif(R8_ose.gt.R0_ure) then
         R8_ase=R0_ore
      else
         R8_ase=R0_ese+(R8_ose-(R0_ise))*(R0_ore-(R0_ese)
     &)/(R0_ure-(R0_ise))
      endif
C vu_l.fgi( 200, 352):��������������� ���������
      R8_ud=R0_od*R8_id
C vu_l.fgi( 178, 258):����������� ��������
      !��������� R_(8) = vu_lC?? /20.0/
      R_(8)=R0_ef
C vu_l.fgi( 227,  24):���������
      !��������� R_(7) = vu_lC?? /60.0/
      R_(7)=R0_if
C vu_l.fgi( 227,  22):���������
      if(L_of) then
         R_(6)=R_(7)
      else
         R_(6)=R_(8)
      endif
C vu_l.fgi( 230,  22):���� RE IN LO CH7
      R8_ik=(R0_af*R_(5)+deltat*R_(6))/(R0_af+deltat)
C vu_l.fgi( 245,  23):�������������� �����  
      R8_ek=R8_ik
C vu_l.fgi( 268,  23):������,20KPH15CT002_T
      R8_uf=R8_ik
C vu_l.fgi( 268,  20):������,20KPH15CT004_T
      !��������� R_(12) = vu_lC?? /20.0/
      R_(12)=R0_el
C vu_l.fgi( 227,  40):���������
      !��������� R_(11) = vu_lC?? /60.0/
      R_(11)=R0_il
C vu_l.fgi( 227,  38):���������
      if(L_ol) then
         R_(10)=R_(11)
      else
         R_(10)=R_(12)
      endif
C vu_l.fgi( 230,  38):���� RE IN LO CH7
      R8_al=(R0_uk*R_(9)+deltat*R_(10))/(R0_uk+deltat)
C vu_l.fgi( 245,  39):�������������� �����  
      R8_ok=R8_al
C vu_l.fgi( 268,  39):������,20KPH15CT001_T
      R8_ak=R8_al
C vu_l.fgi( 268,  36):������,20KPH15CT003_T
      !��������� R_(16) = vu_lC?? /20.0/
      R_(16)=R0_am
C vu_l.fgi( 135,  27):���������
      !��������� R_(15) = vu_lC?? /60.0/
      R_(15)=R0_em
C vu_l.fgi( 135,  25):���������
      if(L_im) then
         R_(14)=R_(15)
      else
         R_(14)=R_(16)
      endif
C vu_l.fgi( 138,  25):���� RE IN LO CH7
      R8_ep=(R0_ul*R_(13)+deltat*R_(14))/(R0_ul+deltat)
C vu_l.fgi( 153,  26):�������������� �����  
      R8_ap=R8_ep
C vu_l.fgi( 176,  26):������,20KPH41CT001_T
      R8_om=R8_ep
C vu_l.fgi( 176,  23):������,20KPH41CT003_T
      !��������� R_(20) = vu_lC?? /20.0/
      R_(20)=R0_ar
C vu_l.fgi( 135,  43):���������
      !��������� R_(19) = vu_lC?? /60.0/
      R_(19)=R0_er
C vu_l.fgi( 135,  41):���������
      if(L_ir) then
         R_(18)=R_(19)
      else
         R_(18)=R_(20)
      endif
C vu_l.fgi( 138,  41):���� RE IN LO CH7
      R8_up=(R0_op*R_(17)+deltat*R_(18))/(R0_op+deltat)
C vu_l.fgi( 153,  42):�������������� �����  
      R8_ip=R8_up
C vu_l.fgi( 176,  42):������,20KPH41CT002_T
      R8_um=R8_up
C vu_l.fgi( 176,  39):������,20KPH41CT004_T
      !��������� R_(24) = vu_lC?? /20.0/
      R_(24)=R0_ur
C vu_l.fgi( 135,  63):���������
      !��������� R_(23) = vu_lC?? /60.0/
      R_(23)=R0_as
C vu_l.fgi( 135,  61):���������
      if(L_es) then
         R_(22)=R_(23)
      else
         R_(22)=R_(24)
      endif
C vu_l.fgi( 138,  61):���� RE IN LO CH7
      R8_at=(R0_or*R_(21)+deltat*R_(22))/(R0_or+deltat)
C vu_l.fgi( 153,  62):�������������� �����  
      R8_us=R8_at
C vu_l.fgi( 176,  62):������,20KPH31CT002_T
      R8_is=R8_at
C vu_l.fgi( 176,  59):������,20KPH31CT004_T
      !��������� R_(28) = vu_lC?? /20.0/
      R_(28)=R0_ut
C vu_l.fgi( 135,  79):���������
      !��������� R_(27) = vu_lC?? /60.0/
      R_(27)=R0_av
C vu_l.fgi( 135,  77):���������
      if(L_ev) then
         R_(26)=R_(27)
      else
         R_(26)=R_(28)
      endif
C vu_l.fgi( 138,  77):���� RE IN LO CH7
      R8_ot=(R0_it*R_(25)+deltat*R_(26))/(R0_it+deltat)
C vu_l.fgi( 153,  78):�������������� �����  
      R8_et=R8_ot
C vu_l.fgi( 176,  78):������,20KPH31CT001_T
      R8_os=R8_ot
C vu_l.fgi( 176,  75):������,20KPH31CT003_T
      !��������� R_(32) = vu_lC?? /20.0/
      R_(32)=R0_ov
C vu_l.fgi(  44,  25):���������
      !��������� R_(31) = vu_lC?? /60.0/
      R_(31)=R0_uv
C vu_l.fgi(  44,  23):���������
      if(L_ax) then
         R_(30)=R_(31)
      else
         R_(30)=R_(32)
      endif
C vu_l.fgi(  47,  23):���� RE IN LO CH7
      R8_ux=(R0_iv*R_(29)+deltat*R_(30))/(R0_iv+deltat)
C vu_l.fgi(  62,  24):�������������� �����  
      R8_ox=R8_ux
C vu_l.fgi(  86,  24):������,20KPG32CT002_T
      R8_ex=R8_ux
C vu_l.fgi(  86,  21):������,20KPG32CT004_T
      !��������� R_(36) = vu_lC?? /20.0/
      R_(36)=R0_obe
C vu_l.fgi(  44,  41):���������
      !��������� R_(35) = vu_lC?? /60.0/
      R_(35)=R0_ube
C vu_l.fgi(  44,  39):���������
      if(L_ade) then
         R_(34)=R_(35)
      else
         R_(34)=R_(36)
      endif
C vu_l.fgi(  47,  39):���� RE IN LO CH7
      R8_ibe=(R0_ebe*R_(33)+deltat*R_(34))/(R0_ebe+deltat
     &)
C vu_l.fgi(  62,  40):�������������� �����  
      R8_abe=R8_ibe
C vu_l.fgi(  86,  40):������,20KPG32CT001_T
      R8_ix=R8_ibe
C vu_l.fgi(  86,  37):������,20KPG32CT003_T
      !��������� R_(40) = vu_lC?? /20.0/
      R_(40)=R0_ide
C vu_l.fgi(  44,  64):���������
      !��������� R_(39) = vu_lC?? /60.0/
      R_(39)=R0_ode
C vu_l.fgi(  44,  62):���������
      if(L_ude) then
         R_(38)=R_(39)
      else
         R_(38)=R_(40)
      endif
C vu_l.fgi(  47,  62):���� RE IN LO CH7
      R8_ofe=(R0_ede*R_(37)+deltat*R_(38))/(R0_ede+deltat
     &)
C vu_l.fgi(  62,  63):�������������� �����  
      R8_ife=R8_ofe
C vu_l.fgi(  86,  63):������,20KPG12CT002_T
      R8_afe=R8_ofe
C vu_l.fgi(  86,  60):������,20KPG12CT004_T
      !��������� R_(44) = vu_lC?? /20.0/
      R_(44)=R0_ike
C vu_l.fgi(  44,  80):���������
      !��������� R_(43) = vu_lC?? /60.0/
      R_(43)=R0_oke
C vu_l.fgi(  44,  78):���������
      if(L_uke) then
         R_(42)=R_(43)
      else
         R_(42)=R_(44)
      endif
C vu_l.fgi(  47,  78):���� RE IN LO CH7
      R8_eke=(R0_ake*R_(41)+deltat*R_(42))/(R0_ake+deltat
     &)
C vu_l.fgi(  62,  79):�������������� �����  
      R8_ufe=R8_eke
C vu_l.fgi(  86,  79):������,20KPG12CT001_T
      R8_efe=R8_eke
C vu_l.fgi(  86,  76):������,20KPG12CT003_T
      R8_ale=R0_ele*R8_ile
C vu_l.fgi( 199, 214):����������� ��������
      R8_ame=R0_ule*R8_ole
C vu_l.fgi( 178, 269):����������� ��������
      R8_ome=R0_ime*R8_eme
C vu_l.fgi( 178, 280):����������� ��������
      R8_epe=R0_ape*R8_ume
C vu_l.fgi( 178, 291):����������� ��������
      R8_use=R8_ate
C vu_l.fgi( 222, 374):������,20KPH40AA103_1
      if(R8_ave.le.R0_ute) then
         R8_eve=R0_ote
      elseif(R8_ave.gt.R0_ite) then
         R8_eve=R0_ete
      else
         R8_eve=R0_ote+(R8_ave-(R0_ute))*(R0_ete-(R0_ote)
     &)/(R0_ite-(R0_ute))
      endif
C vu_l.fgi( 210, 389):��������������� ���������
      if(R8_exe.le.R0_axe) then
         R8_ixe=R0_uve
      elseif(R8_exe.gt.R0_ove) then
         R8_ixe=R0_ive
      else
         R8_ixe=R0_uve+(R8_exe-(R0_axe))*(R0_ive-(R0_uve)
     &)/(R0_ove-(R0_axe))
      endif
C vu_l.fgi( 210, 402):��������������� ���������
      Call trans_pH(deltat,REAL(R8_uxe,8),R8_ebi,REAL(R8_abi
     &,8))
C vu_l.fgi( 113, 374):trans_pH,20KPG32CQ001
      R8_oxe=R8_ebi
C vu_l.fgi( 136, 375):������,20KPG32CQ001_pH
      Call trans_pH(deltat,REAL(R8_ubi,8),R8_edi,REAL(R8_adi
     &,8))
C vu_l.fgi( 113, 386):trans_pH,20KPG12CQ002
      R8_ibi=R8_edi
C vu_l.fgi( 136, 387):������,20KPG12CQ002_pH
      Call trans_pH(deltat,REAL(R8_idi,8),R8_udi,REAL(R8_odi
     &,8))
C vu_l.fgi( 113, 398):trans_pH,20KPG12CQ001
      R8_obi=R8_udi
C vu_l.fgi( 136, 399):������,20KPG12CQ001_pH
      End
