      subroutine gr_parData!
      implicit none
      include 'gr_par.fh'

      REAL*8 FlwTnSqFrom(44)
      COMMON/gr_par_nowrt/ FlwTnSqFrom
      REAL*8 FlwTnSqTo(44)
      COMMON/gr_par_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromUp(44)
      COMMON/gr_par_nowrt/ FlwHTnFromUp
      REAL*8 FlwHTnFromDo(44)
      COMMON/gr_par_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(44)
      COMMON/gr_par_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(44)
      COMMON/gr_par_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tc2Dim_TMax
      PARAMETER (Tc2Dim_TMax=2)
      INTEGER*4 Tn3Dim_TMax
      PARAMETER (Tn3Dim_TMax=2)
      INTEGER*4 INVPERe(51)
      COMMON/gr_par_nowrt/ INVPERe
      INTEGER*4 NZSUBe(540)
      COMMON/gr_par_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(52)
      COMMON/gr_par_nowrt/ INZSUBe
      INTEGER*4 ILNZe(52)
      COMMON/gr_par_nowrt/ ILNZe
      INTEGER*4 INVPERp(43)
      COMMON/gr_par_nowrt/ INVPERp
      INTEGER*4 NZSUBp(324)
      COMMON/gr_par_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(44)
      COMMON/gr_par_nowrt/ INZSUBp
      INTEGER*4 ILNZp(44)
      COMMON/gr_par_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=129)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=65)
      INTEGER*4 Nent
      PARAMETER (Nent=51)
      INTEGER*4 Npc
      PARAMETER (Npc=86)
      INTEGER*4 Npres
      PARAMETER (Npres=43)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(43)
      COMMON/gr_par_nowrt/ PIntIter
      LOGICAL*1 EIntIter(51)
      COMMON/gr_par_nowrt/ EIntIter
      LOGICAL*1 PExtIter(43)
      COMMON/gr_par_nowrt/ PExtIter
      LOGICAL*1 EextIter(51)
      COMMON/gr_par_nowrt/ EextIter
      CHARACTER*16 ConFlOutNames(1)
      COMMON/gr_par_nowrt/ ConFlOutNames
      !DEC$PSECT/gr_par_NOWRT/ NoWRT
      DATA FlwTnSqTo(1:44)/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
     >,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwTnSqFrom(1:44)/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
     >,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnToDo(1:44)/0,0,0,0,0.29,0.29,0.29,0,0,0,0,0,0,0,0,0,
     >0.85,0,0,0,0,0.3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnToUp(1:44)/0,0,0,0,0.3,0.3,0.3,0,0,0,0,0,0,0,0,0,0.86,0
     >,0,0,0,0.31,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnFromDo(1:44)/0,0,0,0,0,0,0,0.29,0,0,0,0.29,0.29,0,0,0,0
     >,0,0,0,0,0.85,0.85,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnFromUp(1:44)/0,0,0,0,0,0,0,0.3,0.01,0.01,0.01,0.3,0.3,
     >0.01,0.01,0.01,0,0,0,0,0,0.86,0.86,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
     >,0,0,0,0,0/
      DATA EextIter(1:51)/.false.,.false.,.false.,.false.,.false.,
     >.false.,.true.,.true.,.true.,.true.,.true.,.true.,.false.,.false.,
     >.true.,.false.,.false.,.true.,.true.,.true.,.true.,.true.,.false.,
     >.true.,.false.,.false.,.true.,.true.,.true.,.true.,.true.,.true.,
     >.false.,.false.,.true.,.true.,.true.,.true.,.true.,.true.,.true.,
     >.true.,.true.,.true.,.true.,.true.,.true.,.true.,.true.,.true.,
     >.true./
      DATA PExtIter(1:43)/.false.,.false.,.false.,.false.,.true.,.false.
     >,.false.,.true.,.true.,.true.,.false.,.false.,.true.,.true.,.true.
     >,.true.,.false.,.false.,.true.,.true.,.true.,.true.,.false.,.true.
     >,.true.,.true.,.false.,.false.,.true.,.true.,.true.,.true.,.true.,
     >.true.,.true.,.true.,.true.,.true.,.true.,.true.,.true.,.false.,
     >.false./
      DATA EIntIter(1:51)/.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false.,.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false.,.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false.,.false.,.false.,.true.,.true.,.true.,
     >.true.,.false.,.false.,.false.,.false.,.true.,.true.,.true.,.true.
     >,.false.,.false.,.false.,.false.,.true.,.false.,.true.,.true.,
     >.true.,.true.,.true.,.true.,.true./
      DATA PIntIter(1:43)/.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false.,.false.,.false.,.false.,.false.,.false.,
     >.true.,.true.,.false.,.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false.,.false.,.false.,.false.,.false.,.false.,
     >.true.,.true.,.false.,.false.,.false.,.false.,.false.,.true.,
     >.false.,.true.,.true.,.true.,.false.,.false./
      DATA INVPERe(1:51)/23,24,31,41,40,34,2,33,25,1,26,32,42,39,6,7,11,
     >3,8,44,10,9,13,12,16,19,18,17,14,21,15,5,20,22,4,27,30,29,28,51,50
     >,49,46,35,38,37,36,48,45,47,43/
      DATA NZSUBe(1:540)/46,49,50,51,44,22,21,7,8,32,9,32,21,22,32,11,12
     >,39,21,22,39,19,18,20,17,18,19,20,44,21,22,44,22,32,39,44,24,31,40
     >,41,27,28,29,30,31,32,40,41,39,40,41,44,35,36,37,38,39,40,44,46,49
     >,50,51,43,45,46,47,48,49,50,51,44,45,46,47,48,49,50,51,45,46,47,48
     >,49,50,51,44,45,46,47,48,49,50,51,5,6,7,8,9,10,11,12,13,14,15,16,
     >17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,
     >39,40,41,42,43,1214343000,1664477234,1,2,2,2,3,4,5,6,7,8,9,10,11,
     >12,13,14,13,12,14,38,20,15,18,16,17,19,20,21,23,22,22,24,25,26,24,
     >27,27,30,31,30,33,34,36,38,40,42,3,4,5,6,7,8,9,10,11,12,13,14,13,
     >39,38,39,20,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,
     >22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,
     >1214343000,1664477219,10,7,18,1,4,32,35,21,17,24,8,6,5,41,40,14,29
     >,23,26,27,31,33,15,16,19,22,11,9,3,37,36,12,34,30,2,13,43,20,39,42
     >,38,25,28,1664477192,1,39,41,38,35,39,41,34,33,9,10,16,33,34,14,15
     >,35,15,16,35,33,34,35,20,19,20,38,21,38,22,38,33,34,38,24,25,32,26
     >,32,33,34,30,31,35,31,32,35,33,34,35,38,39,41,37,39,40,41,38,39,40
     >,41,43,36,37,40,41,43,39,41,42,43,40,41,42,43,0,36,1214273448,
     >1214272020,1132328,1214273460,1214272068,1,1160342,1214272068,
     >1214272044,1133123,1214272068,1,1214273448,1214343424,1214273416,
     >1135555,1214272068,1,44,1214343448,-1072040654,0,0,0,0,0,0,0,0,0,0
     >,0,0,0,0,858993459,-1075104973,0,0,0,0,1132304,1214273448,1,0,0,0,
     >0,0,858993459,-1075104973,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
     >0,0,0,0,0,0,0,0,0,0,35,1661131975,0,0,0,1074790400,0,1073741824,0,
     >1073741824,0,1073741824,0,0,0,0,0,0,0,0,0,0,0,0,0,1074266112,0,
     >1073741824,0,1074266112,0,0,0,1074790400,0,1074266112,0,0,0,
     >1073741824,0,1074266112,0,1073741824,0,1074266112,0,0,0,1074266112
     >,0,0,0,1074266112,0,1074266112,0,0,0,0,0,1074790400,0,1073741824,0
     >,0,0,1074266112,0,1074790400,0,0,35,1661131960,-34359738,
     >1070168276,1202590842,1068792545,-34359738,1070168276,-1717986918,
     >1068079513,1202590842,1068792545,1,103527216,0,0,0,1072693248,0,
     >1077149696,2068562635,1079860720,0/
      DATA INZSUBe(1:52)/1,1,5,6,7,8,9,11,13,16,17,19,22,23,23,25,26,27,
     >28,30,33,34,37,38,41,41,42,43,44,45,46,49,53,53,54,55,56,57,50,51,
     >59,64,72,73,74,75,76,77,78,79,79,79/
      DATA ILNZe(1:52)/1,5,9,10,11,12,13,15,17,20,22,24,27,28,29,31,34,
     >36,38,40,43,47,50,51,54,58,62,67,71,74,76,79,83,87,91,96,100,103,
     >105,108,110,115,123,131,138,144,149,153,156,158,159,159/
      DATA INVPERp(1:43)/4,35,29,5,13,12,2,11,28,1,27,32,36,16,23,24,9,3
     >,25,38,8,26,18,10,42,19,20,43,17,34,21,6,22,33,7,31,30,41,39,15,14
     >,40,37/
      DATA NZSUBp(1:324)/39,41,38,35,39,41,34,33,9,10,16,33,34,14,15,35,
     >15,16,35,33,34,35,20,19,20,38,21,38,22,38,33,34,38,24,25,32,26,32,
     >33,34,30,31,35,31,32,35,33,34,35,38,39,41,37,39,40,41,38,39,40,41,
     >43,36,37,40,41,43,39,41,42,43,40,41,42,43,0,36,1214273448,
     >1214272020,1132328,1214273460,1214272068,1,1160342,1214272068,
     >1214272044,1133123,1214272068,1,1214273448,1214343424,1214273416,
     >1135555,1214272068,1,44,1214343448,-1072040654,0,0,0,0,0,0,0,0,0,0
     >,0,0,0,0,858993459,-1075104973,0,0,0,0,1132304,1214273448,1,0,0,0,
     >0,0,858993459,-1075104973,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
     >0,0,0,0,0,0,0,0,0,0,35,1661131975,0,0,0,1074790400,0,1073741824,0,
     >1073741824,0,1073741824,0,0,0,0,0,0,0,0,0,0,0,0,0,1074266112,0,
     >1073741824,0,1074266112,0,0,0,1074790400,0,1074266112,0,0,0,
     >1073741824,0,1074266112,0,1073741824,0,1074266112,0,0,0,1074266112
     >,0,0,0,1074266112,0,1074266112,0,0,0,0,0,1074790400,0,1073741824,0
     >,0,0,1074266112,0,1074790400,0,0,35,1661131960,-34359738,
     >1070168276,1202590842,1068792545,-34359738,1070168276,-1717986918,
     >1068079513,1202590842,1068792545,1,103527216,0,0,0,1072693248,0,
     >1077149696,2068562635,1079860720,0,0,0,1090021888,-1756609280,
     >1062278156,-993293661,1055485335,0,1077149696,-1972925208,
     >1079568406,0,0,0,1072693248,0,0,0,1077149696,-1717986918,
     >1067030937,0,0,0,0,-63136811,1079475801,-1724500739,1057143214,0,0
     >,0,0,810195973,1089766662,-565666436,1085299794,-604521378,
     >1062234516,2105675787,-1117656173,-1977663035,1036680769,-
     >253574788,1072372994,536871297,1083158876,-1300759023,1083126308,
     >810195973,1089766662,-1001968658,1092188870,-767146649,1095002504,
     >-470383823,1071834077,-273665552,1083045013,834647552,-1077746077,
     >151013792,1081560172,-556009876,1062279057,-910915398,1073420968,-
     >1001968658,1092188870,-767146649,1095002504,-448998979/
      DATA INZSUBp(1:44)/1,1,3,4,4,7,8,9,10,11,14,14,14,17,18,20,23,24,
     >25,27,29,31,34,35,37,38,41,41,41,44,45,47,48,49,50,53,57,58,59,60,
     >60,61,61,61/
      DATA ILNZp(1:44)/1,3,5,6,7,10,11,12,14,16,19,21,23,26,29,31,34,35,
     >36,38,40,42,45,46,48,50,53,55,57,60,63,65,68,71,73,76,80,84,87,89,
     >90,90,91,91/
      DATA ConFlOutNames(1:1)/'20KPH10AC001_Int'/
      return
      end

