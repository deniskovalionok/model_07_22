      subroutine FDB40_TGData!
      implicit none
      include 'FDB40_TG.fh'

      REAL*8 FlwTnSqFrom(16)
      COMMON/FDB40_TG_nowrt/ FlwTnSqFrom
      REAL*8 FlwHTnFromUp(16)
      COMMON/FDB40_TG_nowrt/ FlwHTnFromUp
      REAL*8 FlwTnSqTo(16)
      COMMON/FDB40_TG_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromDo(16)
      COMMON/FDB40_TG_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(16)
      COMMON/FDB40_TG_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(16)
      COMMON/FDB40_TG_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 INVPERe(14)
      COMMON/FDB40_TG_nowrt/ INVPERe
      INTEGER*4 NZSUBe(90)
      COMMON/FDB40_TG_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(15)
      COMMON/FDB40_TG_nowrt/ INZSUBe
      INTEGER*4 ILNZe(15)
      COMMON/FDB40_TG_nowrt/ ILNZe
      INTEGER*4 INVPERp(14)
      COMMON/FDB40_TG_nowrt/ INVPERp
      INTEGER*4 NZSUBp(90)
      COMMON/FDB40_TG_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(15)
      COMMON/FDB40_TG_nowrt/ INZSUBp
      INTEGER*4 ILNZp(15)
      COMMON/FDB40_TG_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=16)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=16)
      INTEGER*4 Nent
      PARAMETER (Nent=14)
      INTEGER*4 Npc
      PARAMETER (Npc=28)
      INTEGER*4 Npres
      PARAMETER (Npres=14)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(14)
      COMMON/FDB40_TG_nowrt/ PIntIter
      LOGICAL*1 EIntIter(14)
      COMMON/FDB40_TG_nowrt/ EIntIter
      LOGICAL*1 EextIter(14)
      COMMON/FDB40_TG_nowrt/ EextIter
      LOGICAL*1 PExtIter(14)
      COMMON/FDB40_TG_nowrt/ PExtIter
      !DEC$PSECT/FDB40_TG_NOWRT/ NoWRT
      DATA FlwTnSqTo(1:16)/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwTnSqFrom(1:16)/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnToDo(1:16)/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnToUp(1:16)/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnFromDo(1:16)/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnFromUp(1:16)/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA EextIter(1:14)/.true.,.false.,.false.,.true.,.true.,.true.,
     >.true.,.false.,.true.,.true.,.true.,.true.,.false.,.true./
      DATA PExtIter(1:14)/.true.,.false.,.false.,.true.,.true.,.true.,
     >.true.,.false.,.true.,.true.,.true.,.true.,.false.,.true./
      DATA EIntIter(1:14)/.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false.,.false.,.false.,.false.,.false.,.false.,
     >.false./
      DATA PIntIter(1:14)/.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false.,.false.,.false.,.false.,.false.,.false.,
     >.false./
      DATA INVPERe(1:14)/8,13,2,14,5,6,9,7,4,10,3,11,12,1/
      DATA NZSUBe(1:90)/12,10,4,5,8,7,8,10,14,11,14,12,14,13,14,-1,0,0,0
     >,0,1816225892,1685016172,0,0,0,1214318498,1160334,0,0,-1,-3,
     >14958092,14958172,1214318828,1055177,1665174804,2,1665173708,
     >1665174792,8,1214318776,1065353216,0,1214318608,106026448,
     >106032368,1214318368,1214318256,1,0,0,0,1086556160,0,1086556160,0,
     >0,1214318480,16,13,1,14,1,1,14,1214343432,1661190084,0,0,0,
     >1214343448,1661190084,1666603948,1666603944,0,65,223852720,
     >1666604284,105310176,1214318924,1108610,1194692,1214318936,1108790
     >,1194692,1214318952,1111215,56,1666384032,1214343008/
      DATA INZSUBe(1:15)/1,2,3,4,5,6,7,8,8,10,12,14,15,15,15/
      DATA ILNZe(1:15)/1,2,3,4,5,6,8,10,12,14,16,18,20,21,21/
      DATA INVPERp(1:14)/8,13,2,14,5,6,9,7,4,10,3,11,12,1/
      DATA NZSUBp(1:90)/12,10,4,5,8,7,8,10,14,11,14,12,14,13,14,-1,0,0,0
     >,0,1816225892,1685016172,0,0,0,1214318498,1160334,0,0,-1,-3,
     >14958092,14958172,1214318828,1055177,1665174804,2,1665173708,
     >1665174792,8,1214318776,1065353216,0,1214318608,106026448,
     >106032368,1214318368,1214318256,1,0,0,0,1086556160,0,1086556160,0,
     >0,1214318480,16,13,1,14,1,1,14,1214343432,1661190084,0,0,0,
     >1214343448,1661190084,1666603948,1666603944,1666604084,1666604304,
     >1666604036,1666604284,105310176,105310272,105309728,105309824,
     >106011600,106009360,106009040,106008720,0,0,0,1214343000/
      DATA INZSUBp(1:15)/1,2,3,4,5,6,7,8,8,10,12,14,15,15,15/
      DATA ILNZp(1:15)/1,2,3,4,5,6,8,10,12,14,16,18,20,21,21/
      return
      end

