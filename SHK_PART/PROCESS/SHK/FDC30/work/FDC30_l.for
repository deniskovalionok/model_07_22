      Subroutine FDC30_l(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDC30_l.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      if(R8_ed.le.R0_u) then
         R_(1)=R0_o
      elseif(R8_ed.gt.R0_i) then
         R_(1)=R0_e
      else
         R_(1)=R0_o+(R8_ed-(R0_u))*(R0_e-(R0_o))/(R0_i-(R0_u
     &))
      endif
C FDC34_l.fgi(  88, 235):��������������� ���������
      R_(2) = R_(1) * R8_ad
C FDC34_l.fgi( 101, 233):����������
      R_(3) = 0.0
C FDC34_l.fgi( 115, 234):��������� (RE4) (�������)
      if(L_od) then
         R8_id=R_(2)
      else
         R8_id=R_(3)
      endif
C FDC34_l.fgi( 121, 233):���� RE IN LO CH7
      if(R8_uf.le.R0_if) then
         R_(4)=R0_ef
      elseif(R8_uf.gt.R0_af) then
         R_(4)=R0_ud
      else
         R_(4)=R0_ef+(R8_uf-(R0_if))*(R0_ud-(R0_ef))/(R0_af
     &-(R0_if))
      endif
C FDC34_l.fgi(  86, 271):��������������� ���������
      R_(5) = R_(4) * R8_of
C FDC34_l.fgi(  99, 269):����������
      R_(6) = 0.0
C FDC34_l.fgi( 113, 270):��������� (RE4) (�������)
      if(L_ek) then
         R8_ak=R_(5)
      else
         R8_ak=R_(6)
      endif
C FDC34_l.fgi( 119, 269):���� RE IN LO CH7
      End
