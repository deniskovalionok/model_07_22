      subroutine FDC30_hData!
      implicit none
      include 'FDC30_h.fh'

      REAL*8 FlwHTnToDo(23)
      COMMON/FDC30_h_nowrt/ FlwHTnToDo
      REAL*8 FlwTnSqFrom(23)
      COMMON/FDC30_h_nowrt/ FlwTnSqFrom
      REAL*8 FlwHTnFromUp(23)
      COMMON/FDC30_h_nowrt/ FlwHTnFromUp
      REAL*8 FlwTnSqTo(23)
      COMMON/FDC30_h_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromDo(23)
      COMMON/FDC30_h_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(23)
      COMMON/FDC30_h_nowrt/ FlwHTnToUp
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 INVPERe(20)
      COMMON/FDC30_h_nowrt/ INVPERe
      INTEGER*4 NZSUBe(189)
      COMMON/FDC30_h_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(21)
      COMMON/FDC30_h_nowrt/ INZSUBe
      INTEGER*4 ILNZe(21)
      COMMON/FDC30_h_nowrt/ ILNZe
      INTEGER*4 INVPERp(16)
      COMMON/FDC30_h_nowrt/ INVPERp
      INTEGER*4 NZSUBp(117)
      COMMON/FDC30_h_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(17)
      COMMON/FDC30_h_nowrt/ INZSUBp
      INTEGER*4 ILNZp(17)
      COMMON/FDC30_h_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=43)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=23)
      INTEGER*4 Nent
      PARAMETER (Nent=20)
      INTEGER*4 Npc
      PARAMETER (Npc=32)
      INTEGER*4 Npres
      PARAMETER (Npres=16)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn8Dim_TMax
      PARAMETER (Tn8Dim_TMax=2)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(16)
      COMMON/FDC30_h_nowrt/ PIntIter
      LOGICAL*1 EIntIter(20)
      COMMON/FDC30_h_nowrt/ EIntIter
      LOGICAL*1 PExtIter(16)
      COMMON/FDC30_h_nowrt/ PExtIter
      LOGICAL*1 EextIter(20)
      COMMON/FDC30_h_nowrt/ EextIter
      !DEC$PSECT/FDC30_h_NOWRT/ NoWRT
      DATA FlwTnSqTo(1:23)/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
     >/
      DATA FlwTnSqFrom(1:23)/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
     >,0/
      DATA FlwHTnToDo(1:23)/0,0,0,0,0,0,0,0,0,0,0.28,0.28,0,0,0,0,0.301,
     >0.301,0.28,0.28,0,0,0/
      DATA FlwHTnToUp(1:23)/0,0,0,0,0,0,0,0,0,0,0.296,0.296,0,0,0,0,
     >0.317,0.317,0.296,0.296,0,0,0/
      DATA FlwHTnFromDo(1:23)/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.001,0.001,
     >0.001,0.001,0.301,0.301,0,0,0/
      DATA FlwHTnFromUp(1:23)/0,0,0,0,0,0,0,0,0,0,0,0,0,0,0.017,0.017,
     >0.017,0.017,0.317,0.317,0,0,0/
      DATA EextIter(1:20)/.false.,.false.,.true.,.true.,.true.,.true.,
     >.false.,.true.,.true.,.true.,.true.,.false.,.true.,.false.,.false.
     >,.false.,.true.,.true.,.true.,.true./
      DATA PExtIter(1:16)/.true.,.false.,.true.,.true.,.true.,.false.,
     >.true.,.false.,.false.,.false.,.true.,.true.,.false.,.false.,
     >.true.,.true./
      DATA EIntIter(1:20)/.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false.,.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false.,.false.,.false.,.false.,.false./
      DATA PIntIter(1:16)/.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false.,.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false./
      DATA INVPERe(1:20)/7,12,14,8,9,10,15,1,11,13,16,2,17,20,3,6,19,18,
     >5,4/
      DATA NZSUBe(1:189)/3,6,4,5,6,8,12,14,13,14,17,20,18,19,20,14,15,16
     >,18,19,20,6,7,8,9,10,11,12,13,14,15,16,947388416,0,947388416,0,
     >947388416,1214343000,1664477234,1,3,3,3,2,2,2,7,8,3,3,11,12,13,14,
     >2,2,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,1074266112,0,1074266112
     >,0,0,1214343000,1664477219,10,1,4,5,6,2,9,3,7,11,13,15,8,12,14,16,
     >1,6,8,3,6,8,7,8,11,12,15,16,-755914244,1066426957,-755914244,
     >1066426957,-755914244,1065378381,-755914244,1065378381,-755914244,
     >1066426957,-755914244,1066426957,-755914244,1066426957,-755914244,
     >1066426957,-755914244,1066426957,-755914244,1066426957,-1924145348
     >,1066561175,1649267442,1070159888,-1924145348,1066561175,12,
     >1661131931,3,6,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1816225892,1685016172
     >,0,0,0,1214308082,0,0,0,-1,-3,14958088,14958168,1214308412,1055177
     >,1665174804,2,1665173708,1665174792,8,1214308360,1065353216,0,
     >1214308192,106023344,106028784,1214307904,1214307808,1,0,0,0,
     >1086556160,0,1086556160,0,0/
      DATA INZSUBe(1:21)/1,1,3,4,5,5,6,7,7,7,7,9,10,10,11,11,13,14,15,15
     >,15/
      DATA ILNZe(1:21)/1,3,5,8,10,11,11,12,14,16,18,20,22,23,23,25,27,30
     >,32,33,33/
      DATA INVPERp(1:16)/2,6,8,3,4,5,9,13,7,1,10,14,11,15,12,16/
      DATA NZSUBp(1:117)/6,8,3,6,8,7,8,11,12,15,16,-755914244,1066426957
     >,-755914244,1066426957,-755914244,1065378381,-755914244,1065378381
     >,-755914244,1066426957,-755914244,1066426957,-755914244,1066426957
     >,-755914244,1066426957,-755914244,1066426957,-755914244,1066426957
     >,-1924145348,1066561175,1649267442,1070159888,-1924145348,
     >1066561175,12,1661131931,3,6,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,
     >1816225892,1685016172,0,0,0,1214308082,0,0,0,-1,-3,14958088,
     >14958168,1214308412,1055177,1665174804,2,1665173708,1665174792,8,
     >1214308360,1065353216,0,1214308192,106023344,106028784,1214307904,
     >1214307808,1,0,0,0,1086556160,0,1086556160,0,0,1214308000,23,2,0,
     >10,1,2,12,1214343432,1661190084,0,0,0,1214343448,1661190084,
     >1666603948,1666603944,1666604084,1666604304,1666604036,1666604284,
     >107146576,107146688,107146000/
      DATA INZSUBp(1:17)/1,3,4,4,4,6,7,7,8,8,9,9,10,10,11,11,11/
      DATA ILNZp(1:17)/1,3,4,6,8,10,12,13,13,14,15,16,16,17,18,19,19/
      return
      end

