      subroutine FDB60_vent_hData!
      implicit none
      include 'FDB60_vent_h.fh'

      REAL*8 FlwTnSqFrom(13)
      COMMON/FDB60_vent_h_nowrt/ FlwTnSqFrom
      REAL*8 FlwHTnFromUp(13)
      COMMON/FDB60_vent_h_nowrt/ FlwHTnFromUp
      REAL*8 FlwTnSqTo(13)
      COMMON/FDB60_vent_h_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromDo(13)
      COMMON/FDB60_vent_h_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(13)
      COMMON/FDB60_vent_h_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(13)
      COMMON/FDB60_vent_h_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 INVPERe(12)
      COMMON/FDB60_vent_h_nowrt/ INVPERe
      INTEGER*4 NZSUBe(75)
      COMMON/FDB60_vent_h_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(13)
      COMMON/FDB60_vent_h_nowrt/ INZSUBe
      INTEGER*4 ILNZe(13)
      COMMON/FDB60_vent_h_nowrt/ ILNZe
      INTEGER*4 INVPERp(12)
      COMMON/FDB60_vent_h_nowrt/ INVPERp
      INTEGER*4 NZSUBp(75)
      COMMON/FDB60_vent_h_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(13)
      COMMON/FDB60_vent_h_nowrt/ INZSUBp
      INTEGER*4 ILNZp(13)
      COMMON/FDB60_vent_h_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=13)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=13)
      INTEGER*4 Nent
      PARAMETER (Nent=12)
      INTEGER*4 Npc
      PARAMETER (Npc=24)
      INTEGER*4 Npres
      PARAMETER (Npres=12)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(12)
      COMMON/FDB60_vent_h_nowrt/ PIntIter
      LOGICAL*1 EIntIter(12)
      COMMON/FDB60_vent_h_nowrt/ EIntIter
      LOGICAL*1 EextIter(12)
      COMMON/FDB60_vent_h_nowrt/ EextIter
      LOGICAL*1 PExtIter(12)
      COMMON/FDB60_vent_h_nowrt/ PExtIter
      !DEC$PSECT/FDB60_vent_h_NOWRT/ NoWRT
      DATA FlwTnSqTo(1:13)/0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwTnSqFrom(1:13)/0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnToDo(1:13)/0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnToUp(1:13)/0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnFromDo(1:13)/0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnFromUp(1:13)/0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA EextIter(1:12)/.false.,.true.,.true.,.false.,.true.,.false.,
     >.true.,.true.,.true.,.true.,.false.,.true./
      DATA PExtIter(1:12)/.false.,.true.,.true.,.false.,.true.,.false.,
     >.true.,.true.,.true.,.true.,.false.,.true./
      DATA EIntIter(1:12)/.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false.,.false.,.false.,.false.,.false./
      DATA PIntIter(1:12)/.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false.,.false.,.false.,.false.,.false./
      DATA INVPERe(1:12)/4,11,6,12,3,2,7,1,8,9,10,5/
      DATA NZSUBe(1:75)/2,3,4,12,10,8,12,9,12,10,12,11,12,12,1055177,
     >1665174804,2,1665173708,1665174792,8,1214322920,1065353216,0,
     >1214322752,106011792,106022608,1214322544,1214322448,1,0,0,0,
     >1086556160,0,1086556160,0,0,1214322640,13,11,1,12,1,1,12,
     >1214343432,1661190084,0,0,0,1214343448,1661190084,1666603948,
     >1666603944,0,57,16752312,1666604284,80471552,1214323068,1108610,
     >1194692,1214323080,1108790,1194692,1214323096,1110864,48,
     >1666384032,1214323108,1110734,57,0,1214343008,1664477176/
      DATA INZSUBe(1:13)/1,2,3,4,5,6,6,8,10,12,13,13,13/
      DATA ILNZe(1:13)/1,2,3,4,5,6,7,9,11,13,15,16,16/
      DATA INVPERp(1:12)/4,11,6,12,3,2,7,1,8,9,10,5/
      DATA NZSUBp(1:75)/2,3,4,12,10,8,12,9,12,10,12,11,12,12,1055177,
     >1665174804,2,1665173708,1665174792,8,1214322920,1065353216,0,
     >1214322752,106011792,106022608,1214322544,1214322448,1,0,0,0,
     >1086556160,0,1086556160,0,0,1214322640,13,11,1,12,1,1,12,
     >1214343432,1661190084,0,0,0,1214343448,1661190084,1666603948,
     >1666603944,1666604084,1666604304,1666604036,1666604284,80471552,
     >80472032,80471712,80471952,106000320,106002352,106001264,106000176
     >,0,0,0,0,0,0,80473520,1214343000,1664477176/
      DATA INZSUBp(1:13)/1,2,3,4,5,6,6,8,10,12,13,13,13/
      DATA ILNZp(1:13)/1,2,3,4,5,6,7,9,11,13,15,16,16/
      return
      end

