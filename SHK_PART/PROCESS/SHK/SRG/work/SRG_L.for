      Subroutine SRG_L(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'SRG_L.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      if(R8_ad.le.R0_u) then
         R_(3)=R0_o
      elseif(R8_ad.gt.R0_i) then
         R_(3)=R0_e
      else
         R_(3)=R0_o+(R8_ad-(R0_u))*(R0_e-(R0_o))/(R0_i-(R0_u
     &))
      endif
C SRG_L.fgi( 108,  73):��������������� ���������
      !��������� R_(1) = SRG_LC?? /6/
      R_(1)=R0_ed
C SRG_L.fgi( 134,  77):���������
      !��������� R_(2) = SRG_LC?? /10/
      R_(2)=R0_od
C SRG_L.fgi( 134,  85):���������
      !��������� R_(4) = SRG_LC?? /8/
      R_(4)=R0_af
C SRG_L.fgi( 134,  93):���������
      if(R8_il.le.R0_ik) then
         R_(8)=R0_ek
      elseif(R8_il.gt.R0_ak) then
         R_(8)=R0_uf
      else
         R_(8)=R0_ek+(R8_il-(R0_ik))*(R0_uf-(R0_ek))/(R0_ak
     &-(R0_ik))
      endif
C SRG_L.fgi(  82,  82):��������������� ���������
      R_(6) = R8_if * R_(8)
C SRG_L.fgi( 107,  82):����������
      if(R8_ol.le.R0_el) then
         R_(9)=R0_al
      elseif(R8_ol.gt.R0_uk) then
         R_(9)=R0_ok
      else
         R_(9)=R0_al+(R8_ol-(R0_el))*(R0_ok-(R0_al))/(R0_uk
     &-(R0_el))
      endif
C SRG_L.fgi(  82,  96):��������������� ���������
      R_(7) = R8_of * R_(9)
C SRG_L.fgi( 107,  96):����������
      R_(5) = R_(7) + R_(6)
C SRG_L.fgi( 124,  95):��������
      R8_ef = R_(5) * R_(4) * R_(3)
C SRG_L.fgi( 137,  93):����������
      R8_ud = R_(5) * R_(2) * R_(3)
C SRG_L.fgi( 137,  85):����������
      R8_id = R_(5) * R_(1) * R_(3)
C SRG_L.fgi( 137,  77):����������
      if(R8_om.le.R0_im) then
         R_(12)=R0_em
      elseif(R8_om.gt.R0_am) then
         R_(12)=R0_ul
      else
         R_(12)=R0_em+(R8_om-(R0_im))*(R0_ul-(R0_em))/(R0_am
     &-(R0_im))
      endif
C SRG_L.fgi( 110, 122):��������������� ���������
      !��������� R_(10) = SRG_LC?? /6/
      R_(10)=R0_um
C SRG_L.fgi( 136, 126):���������
      !��������� R_(11) = SRG_LC?? /10/
      R_(11)=R0_ep
C SRG_L.fgi( 136, 134):���������
      !��������� R_(13) = SRG_LC?? /8/
      R_(13)=R0_op
C SRG_L.fgi( 136, 142):���������
      if(R8_at.le.R0_as) then
         R_(17)=R0_ur
      elseif(R8_at.gt.R0_or) then
         R_(17)=R0_ir
      else
         R_(17)=R0_ur+(R8_at-(R0_as))*(R0_ir-(R0_ur))/(R0_or
     &-(R0_as))
      endif
C SRG_L.fgi(  84, 131):��������������� ���������
      R_(15) = R8_ar * R_(17)
C SRG_L.fgi( 109, 131):����������
      if(R8_et.le.R0_us) then
         R_(18)=R0_os
      elseif(R8_et.gt.R0_is) then
         R_(18)=R0_es
      else
         R_(18)=R0_os+(R8_et-(R0_us))*(R0_es-(R0_os))/(R0_is
     &-(R0_us))
      endif
C SRG_L.fgi(  84, 145):��������������� ���������
      R_(16) = R8_er * R_(18)
C SRG_L.fgi( 109, 145):����������
      R_(14) = R_(16) + R_(15)
C SRG_L.fgi( 126, 144):��������
      R8_up = R_(14) * R_(13) * R_(12)
C SRG_L.fgi( 139, 142):����������
      R8_ip = R_(14) * R_(11) * R_(12)
C SRG_L.fgi( 139, 134):����������
      R8_ap = R_(14) * R_(10) * R_(12)
C SRG_L.fgi( 139, 126):����������
      if(R8_ev.le.R0_av) then
         R_(21)=R0_ut
      elseif(R8_ev.gt.R0_ot) then
         R_(21)=R0_it
      else
         R_(21)=R0_ut+(R8_ev-(R0_av))*(R0_it-(R0_ut))/(R0_ot
     &-(R0_av))
      endif
C SRG_L.fgi( 109, 171):��������������� ���������
      !��������� R_(19) = SRG_LC?? /6/
      R_(19)=R0_iv
C SRG_L.fgi( 135, 175):���������
      !��������� R_(20) = SRG_LC?? /10/
      R_(20)=R0_uv
C SRG_L.fgi( 135, 183):���������
      !��������� R_(22) = SRG_LC?? /8/
      R_(22)=R0_ex
C SRG_L.fgi( 135, 191):���������
      if(R8_ode.le.R0_obe) then
         R_(26)=R0_ibe
      elseif(R8_ode.gt.R0_ebe) then
         R_(26)=R0_abe
      else
         R_(26)=R0_ibe+(R8_ode-(R0_obe))*(R0_abe-(R0_ibe)
     &)/(R0_ebe-(R0_obe))
      endif
C SRG_L.fgi(  83, 180):��������������� ���������
      R_(24) = R8_ox * R_(26)
C SRG_L.fgi( 108, 180):����������
      if(R8_ude.le.R0_ide) then
         R_(27)=R0_ede
      elseif(R8_ude.gt.R0_ade) then
         R_(27)=R0_ube
      else
         R_(27)=R0_ede+(R8_ude-(R0_ide))*(R0_ube-(R0_ede)
     &)/(R0_ade-(R0_ide))
      endif
C SRG_L.fgi(  83, 194):��������������� ���������
      R_(25) = R8_ux * R_(27)
C SRG_L.fgi( 108, 194):����������
      R_(23) = R_(25) + R_(24)
C SRG_L.fgi( 125, 193):��������
      R8_ix = R_(23) * R_(22) * R_(21)
C SRG_L.fgi( 138, 191):����������
      R8_ax = R_(23) * R_(20) * R_(21)
C SRG_L.fgi( 138, 183):����������
      R8_ov = R_(23) * R_(19) * R_(21)
C SRG_L.fgi( 138, 175):����������
      End
