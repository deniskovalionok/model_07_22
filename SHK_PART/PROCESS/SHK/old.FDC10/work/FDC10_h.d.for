      subroutine FDC10_hData!
      implicit none
      include 'FDC10_h.fh'

      REAL*8 FlwTnSqFrom(13)
      COMMON/FDC10_h_nowrt/ FlwTnSqFrom
      REAL*8 FlwHTnFromUp(13)
      COMMON/FDC10_h_nowrt/ FlwHTnFromUp
      REAL*8 FlwTnSqTo(13)
      COMMON/FDC10_h_nowrt/ FlwTnSqTo
      REAL*8 FlwHTnFromDo(13)
      COMMON/FDC10_h_nowrt/ FlwHTnFromDo
      REAL*8 FlwHTnToUp(13)
      COMMON/FDC10_h_nowrt/ FlwHTnToUp
      REAL*8 FlwHTnToDo(13)
      COMMON/FDC10_h_nowrt/ FlwHTnToDo
      INTEGER*4 Fan2NTN2N
      PARAMETER (Fan2NTN2N=0)
      INTEGER*4 Fan2NTN0N
      PARAMETER (Fan2NTN0N=0)
      INTEGER*4 Fan2NTauNN
      PARAMETER (Fan2NTauNN=0)
      INTEGER*4 Fan2NdPG5N
      PARAMETER (Fan2NdPG5N=0)
      INTEGER*4 Fan2NdPG4N
      PARAMETER (Fan2NdPG4N=0)
      INTEGER*4 Fan2NdPG3N
      PARAMETER (Fan2NdPG3N=0)
      INTEGER*4 Fan2NdPG2N
      PARAMETER (Fan2NdPG2N=0)
      INTEGER*4 Fan2NdPG0N
      PARAMETER (Fan2NdPG0N=0)
      INTEGER*4 Fan2TauNN
      PARAMETER (Fan2TauNN=0)
      INTEGER*4 FanDim_TauNmax
      PARAMETER (FanDim_TauNmax=0)
      INTEGER*4 FanDim_Taumax
      PARAMETER (FanDim_Taumax=0)
      INTEGER*4 FanDim_KMax
      PARAMETER (FanDim_KMax=0)
      INTEGER*4 FanDim_PMax
      PARAMETER (FanDim_PMax=0)
      INTEGER*4 Tn8Ne
      PARAMETER (Tn8Ne=2)
      INTEGER*4 Tn3Np
      PARAMETER (Tn3Np=2)
      INTEGER*4 Tn3Ne
      PARAMETER (Tn3Ne=4)
      INTEGER*4 INVPERe(16)
      COMMON/FDC10_h_nowrt/ INVPERe
      INTEGER*4 NZSUBe(87)
      COMMON/FDC10_h_nowrt/ NZSUBe
      INTEGER*4 INZSUBe(17)
      COMMON/FDC10_h_nowrt/ INZSUBe
      INTEGER*4 ILNZe(17)
      COMMON/FDC10_h_nowrt/ ILNZe
      INTEGER*4 INVPERp(16)
      COMMON/FDC10_h_nowrt/ INVPERp
      INTEGER*4 NZSUBp(87)
      COMMON/FDC10_h_nowrt/ NZSUBp
      INTEGER*4 INZSUBp(17)
      COMMON/FDC10_h_nowrt/ INZSUBp
      INTEGER*4 ILNZp(17)
      COMMON/FDC10_h_nowrt/ ILNZp
      INTEGER*4 Nthermo
      PARAMETER (Nthermo=13)
      INTEGER*4 Nhydro
      PARAMETER (Nhydro=13)
      INTEGER*4 Nent
      PARAMETER (Nent=16)
      INTEGER*4 Npc
      PARAMETER (Npc=32)
      INTEGER*4 Npres
      PARAMETER (Npres=16)
      INTEGER*4 Nf
      PARAMETER (Nf=6)
      INTEGER*4 Tn7Ne
      PARAMETER (Tn7Ne=1)
      INTEGER*4 Tn7Np
      PARAMETER (Tn7Np=1)
      INTEGER*4 Tn9Ne
      PARAMETER (Tn9Ne=6)
      INTEGER*4 Tn9Np
      PARAMETER (Tn9Np=2)
      INTEGER*4 Fan2NTN3N
      PARAMETER (Fan2NTN3N=0)
      INTEGER*4 Fan2NTN4N
      PARAMETER (Fan2NTN4N=0)
      INTEGER*4 Fan2NTN5N
      PARAMETER (Fan2NTN5N=0)
      INTEGER*4 Tr1Dim_Nu
      PARAMETER (Tr1Dim_Nu=-111)
      INTEGER*4 Tr2Dim_Ksi
      PARAMETER (Tr2Dim_Ksi=-111)
      INTEGER*4 Tn8Np
      PARAMETER (Tn8Np=1)
      LOGICAL*1 PIntIter(16)
      COMMON/FDC10_h_nowrt/ PIntIter
      LOGICAL*1 EIntIter(16)
      COMMON/FDC10_h_nowrt/ EIntIter
      LOGICAL*1 EextIter(16)
      COMMON/FDC10_h_nowrt/ EextIter
      LOGICAL*1 PExtIter(16)
      COMMON/FDC10_h_nowrt/ PExtIter
      !DEC$PSECT/FDC10_h_NOWRT/ NoWRT
      DATA FlwTnSqTo(1:13)/0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwTnSqFrom(1:13)/0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnToDo(1:13)/0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnToUp(1:13)/0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnFromDo(1:13)/0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA FlwHTnFromUp(1:13)/0,0,0,0,0,0,0,0,0,0,0,0,0/
      DATA EextIter(1:16)/.false.,.false.,.true.,.true.,.false.,.false.,
     >.true.,.false.,.false.,.true.,.false.,.false.,.false.,.true.,
     >.true.,.false./
      DATA PExtIter(1:16)/.false.,.false.,.true.,.true.,.false.,.false.,
     >.true.,.false.,.false.,.true.,.false.,.false.,.false.,.true.,
     >.true.,.false./
      DATA EIntIter(1:16)/.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false.,.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false./
      DATA PIntIter(1:16)/.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false.,.false.,.false.,.false.,.false.,.false.,
     >.false.,.false.,.false./
      DATA INVPERe(1:16)/6,12,13,9,1,2,7,14,10,3,15,4,8,11,16,5/
      DATA NZSUBe(1:87)/3,4,5,7,8,10,11,14,15,16,15,16,0,0,0,0,
     >1816225892,1685016172,0,0,0,1268651762,71349390,0,0,-1,-3,
     >412239416,412239496,1268652092,71244233,420515092,2,420513996,
     >420515080,8,1268652040,1065353216,0,1268651872,183288672,183301792
     >,1268651632,1268651504,1,0,0,0,1086556160,0,1086556160,0,0,
     >1268651760,13,16,0,12,1,1,16,1268672776,416530372,0,0,0,1268672792
     >,416530372,421944236,421944232,0,73,416180760,421944572,100461136,
     >1268652188,71297666,71383748,1268652200,71297846,71383748,
     >1268652216,71299920,64,421724320,1268672352,419817464/
      DATA INZSUBe(1:17)/1,1,2,3,3,4,5,5,6,7,7,8,8,9,10,10,10/
      DATA ILNZe(1:17)/1,2,3,4,5,5,6,7,7,8,9,9,10,11,12,13,13/
      DATA INVPERp(1:16)/6,12,13,9,1,2,7,14,10,3,15,4,8,11,16,5/
      DATA NZSUBp(1:87)/3,4,5,7,8,10,11,14,15,16,15,16,0,0,0,0,
     >1816225892,1685016172,0,0,0,1268651762,71349390,0,0,-1,-3,
     >412239416,412239496,1268652092,71244233,420515092,2,420513996,
     >420515080,8,1268652040,1065353216,0,1268651872,183288672,183301792
     >,1268651632,1268651504,1,0,0,0,1086556160,0,1086556160,0,0,
     >1268651760,13,16,0,12,1,1,16,1268672776,416530372,0,0,0,1268672792
     >,416530372,421944236,421944232,421944372,421944592,421944324,
     >421944572,100461136,100461216,100460768,100460848,183280304,
     >183279904,183281664,183281120,0,0,0,1268672344,419817464/
      DATA INZSUBp(1:17)/1,1,2,3,3,4,5,5,6,7,7,8,8,9,10,10,10/
      DATA ILNZp(1:17)/1,2,3,4,5,5,6,7,7,8,9,9,10,11,12,13,13/
      return
      end

