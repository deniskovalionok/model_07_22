      Subroutine X20FDC20(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include '20FDC20.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      ! ��������� ������ ����� 20FDC20 ����� pnevmo_log
      L_(31)=.true.
C pnevmo_log.fgi( 324,  15):pre: ����� 0
      ! ��������� ������ ����� 20FDC20 ����� jgt_log
      L_(38)=.true.
C jgt_log.fgi( 114,  15):pre: ����� 0
      ! ��������� ������ ����� 20FDC20 ����� dsu_log
      L_(60)=.true.
C dsu_log.fgi( 324,  15):pre: ����� 0
      ! ��������� ������ ����� 20FDC20 ����� dio_log
      L_(501)=.true.
C dio_log.fgi( 742,  15):pre: ����� 0
      ! ��������� ������ ����� 20FDC20 ����� 1762-70-0014_log
      L_(532)=.true.
C 1762-70-0014_log.fgi( 324,  15):pre: ����� 0
      ! ��������� ������ ����� 20FDC20 ����� 1762-12-0001_log
      L_(575)=.true.
C 1762-12-0001_log.fgi( 324,  15):pre: ����� 0
      ! ��������� ������ ����� 20FDC20 ����� 1762-11-0006_log
      L_(651)=.true.
C 1762-11-0006_log.fgi( 324,  15):pre: ����� 0
      ! ��������� ������ ����� 20FDC20 ����� 1762-11-0001_log
      L_(692)=.true.
C 1762-11-0001_log.fgi( 324,  15):pre: ����� 0
      ! ��������� ������ ����� 20FDC20 ����� 1762-10-0001_log
      L_(736)=.true.
C 1762-10-0001_log.fgi( 324,  15):pre: ����� 0
      ! ��������� ������ ����� 20FDC20 ����� 1762-07-0001_log
      L_(903)=.true.
C 1762-07-0001_log.fgi( 324,  15):pre: ����� 0
      ! ��������� ������ ����� 20FDC20 ����� 1762-06-0001_log
      L_(1126)=.true.
C 1762-06-0001_log.fgi( 324,  15):pre: ����� 0
      R_(36)=R0_abip
C 1762-12-0001_log.fgi( 219,  58):pre: �������� ��������� ������
      R_(37)=R0_ibip
C 1762-12-0001_log.fgi( 208,  58):pre: ������������  �� T
      R_(38)=R0_efes
C 1762-10-0001_log.fgi( 219,  58):pre: �������� ��������� ������
      R_(39)=R0_ofes
C 1762-10-0001_log.fgi( 208,  58):pre: ������������  �� T
      Call X20FDC20_1(ext_deltat)
      Call X20FDC20_2(ext_deltat)
      End
      Subroutine X20FDC20_1(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 loop_var
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include '20FDC20.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      L_(2)=.false.
C pnevmo_log.fgi(  29, 146):��������� ���������� (�������)
      L_(1)=.false.
C pnevmo_log.fgi(  29, 150):��������� ���������� (�������)
      L0_ad=R0_o.ne.R0_i
      R0_i=R0_o
C pnevmo_log.fgi(  14, 148):���������� ������������� ������
      if(L_(1).or.L_(2)) then
         L0_ed=(L_(1).or.L0_ed).and..not.(L_(2))
      else
         if(L0_ad.and..not.L0_u) L0_ed=.not.L0_ed
      endif
      L0_u=L0_ad
      L_(3)=.not.L0_ed
C pnevmo_log.fgi(  35, 148):T �������
      if(L0_ed) then
         I_e=1
      else
         I_e=0
      endif
C pnevmo_log.fgi(  48, 150):��������� LO->1
      L_(5)=.false.
C pnevmo_log.fgi(  29, 159):��������� ���������� (�������)
      L_(4)=.false.
C pnevmo_log.fgi(  29, 163):��������� ���������� (�������)
      L0_ef=R0_ud.ne.R0_od
      R0_od=R0_ud
C pnevmo_log.fgi(  14, 161):���������� ������������� ������
      if(L_(4).or.L_(5)) then
         L0_if=(L_(4).or.L0_if).and..not.(L_(5))
      else
         if(L0_ef.and..not.L0_af) L0_if=.not.L0_if
      endif
      L0_af=L0_ef
      L_(6)=.not.L0_if
C pnevmo_log.fgi(  35, 161):T �������
      if(L0_if) then
         I_id=1
      else
         I_id=0
      endif
C pnevmo_log.fgi(  48, 163):��������� LO->1
      L_(8)=.false.
C pnevmo_log.fgi(  29, 172):��������� ���������� (�������)
      L_(7)=.false.
C pnevmo_log.fgi(  29, 176):��������� ���������� (�������)
      L0_ik=R0_ak.ne.R0_uf
      R0_uf=R0_ak
C pnevmo_log.fgi(  14, 174):���������� ������������� ������
      if(L_(7).or.L_(8)) then
         L0_ok=(L_(7).or.L0_ok).and..not.(L_(8))
      else
         if(L0_ik.and..not.L0_ek) L0_ok=.not.L0_ok
      endif
      L0_ek=L0_ik
      L_(9)=.not.L0_ok
C pnevmo_log.fgi(  35, 174):T �������
      if(L0_ok) then
         I_of=1
      else
         I_of=0
      endif
C pnevmo_log.fgi(  48, 176):��������� LO->1
      L_(11)=.false.
C pnevmo_log.fgi(  29, 185):��������� ���������� (�������)
      L_(10)=.false.
C pnevmo_log.fgi(  29, 189):��������� ���������� (�������)
      L0_ol=R0_el.ne.R0_al
      R0_al=R0_el
C pnevmo_log.fgi(  14, 187):���������� ������������� ������
      if(L_(10).or.L_(11)) then
         L0_ul=(L_(10).or.L0_ul).and..not.(L_(11))
      else
         if(L0_ol.and..not.L0_il) L0_ul=.not.L0_ul
      endif
      L0_il=L0_ol
      L_(12)=.not.L0_ul
C pnevmo_log.fgi(  35, 187):T �������
      if(L0_ul) then
         I_uk=1
      else
         I_uk=0
      endif
C pnevmo_log.fgi(  48, 189):��������� LO->1
      L_(14)=.false.
C pnevmo_log.fgi(  29, 198):��������� ���������� (�������)
      L_(13)=.false.
C pnevmo_log.fgi(  29, 202):��������� ���������� (�������)
      L0_um=R0_im.ne.R0_em
      R0_em=R0_im
C pnevmo_log.fgi(  14, 200):���������� ������������� ������
      if(L_(13).or.L_(14)) then
         L0_ap=(L_(13).or.L0_ap).and..not.(L_(14))
      else
         if(L0_um.and..not.L0_om) L0_ap=.not.L0_ap
      endif
      L0_om=L0_um
      L_(15)=.not.L0_ap
C pnevmo_log.fgi(  35, 200):T �������
      if(L0_ap) then
         I_am=1
      else
         I_am=0
      endif
C pnevmo_log.fgi(  48, 202):��������� LO->1
      L_(17)=.false.
C pnevmo_log.fgi(  29, 211):��������� ���������� (�������)
      L_(16)=.false.
C pnevmo_log.fgi(  29, 215):��������� ���������� (�������)
      L0_ar=R0_op.ne.R0_ip
      R0_ip=R0_op
C pnevmo_log.fgi(  14, 213):���������� ������������� ������
      if(L_(16).or.L_(17)) then
         L0_er=(L_(16).or.L0_er).and..not.(L_(17))
      else
         if(L0_ar.and..not.L0_up) L0_er=.not.L0_er
      endif
      L0_up=L0_ar
      L_(18)=.not.L0_er
C pnevmo_log.fgi(  35, 213):T �������
      if(L0_er) then
         I_ep=1
      else
         I_ep=0
      endif
C pnevmo_log.fgi(  48, 215):��������� LO->1
      L_(20)=.false.
C pnevmo_log.fgi(  29, 224):��������� ���������� (�������)
      L_(19)=.false.
C pnevmo_log.fgi(  29, 228):��������� ���������� (�������)
      L0_es=R0_ur.ne.R0_or
      R0_or=R0_ur
C pnevmo_log.fgi(  14, 226):���������� ������������� ������
      if(L_(19).or.L_(20)) then
         L0_is=(L_(19).or.L0_is).and..not.(L_(20))
      else
         if(L0_es.and..not.L0_as) L0_is=.not.L0_is
      endif
      L0_as=L0_es
      L_(21)=.not.L0_is
C pnevmo_log.fgi(  35, 226):T �������
      if(L0_is) then
         I_ir=1
      else
         I_ir=0
      endif
C pnevmo_log.fgi(  48, 228):��������� LO->1
      L_(23)=.false.
C pnevmo_log.fgi(  29, 237):��������� ���������� (�������)
      L_(22)=.false.
C pnevmo_log.fgi(  29, 241):��������� ���������� (�������)
      L0_it=R0_at.ne.R0_us
      R0_us=R0_at
C pnevmo_log.fgi(  14, 239):���������� ������������� ������
      if(L_(22).or.L_(23)) then
         L0_ot=(L_(22).or.L0_ot).and..not.(L_(23))
      else
         if(L0_it.and..not.L0_et) L0_ot=.not.L0_ot
      endif
      L0_et=L0_it
      L_(24)=.not.L0_ot
C pnevmo_log.fgi(  35, 239):T �������
      if(L0_ot) then
         I_os=1
      else
         I_os=0
      endif
C pnevmo_log.fgi(  48, 241):��������� LO->1
      L_(26)=.false.
C pnevmo_log.fgi(  29, 250):��������� ���������� (�������)
      L_(25)=.false.
C pnevmo_log.fgi(  29, 254):��������� ���������� (�������)
      L0_ov=R0_ev.ne.R0_av
      R0_av=R0_ev
C pnevmo_log.fgi(  14, 252):���������� ������������� ������
      if(L_(25).or.L_(26)) then
         L0_uv=(L_(25).or.L0_uv).and..not.(L_(26))
      else
         if(L0_ov.and..not.L0_iv) L0_uv=.not.L0_uv
      endif
      L0_iv=L0_ov
      L_(27)=.not.L0_uv
C pnevmo_log.fgi(  35, 252):T �������
      if(L0_uv) then
         I_ut=1
      else
         I_ut=0
      endif
C pnevmo_log.fgi(  48, 254):��������� LO->1
      L_(29)=.false.
C pnevmo_log.fgi(  29, 263):��������� ���������� (�������)
      L_(28)=.false.
C pnevmo_log.fgi(  29, 267):��������� ���������� (�������)
      L0_ux=R0_ix.ne.R0_ex
      R0_ex=R0_ix
C pnevmo_log.fgi(  14, 265):���������� ������������� ������
      if(L_(28).or.L_(29)) then
         L0_abe=(L_(28).or.L0_abe).and..not.(L_(29))
      else
         if(L0_ux.and..not.L0_ox) L0_abe=.not.L0_abe
      endif
      L0_ox=L0_ux
      L_(30)=.not.L0_abe
C pnevmo_log.fgi(  35, 265):T �������
      if(L0_abe) then
         I_ax=1
      else
         I_ax=0
      endif
C pnevmo_log.fgi(  48, 267):��������� LO->1
      L_ibe=.false.
C pnevmo_log.fgi( 148, 280):��������� ���������� (�������)
      L_ebe=L_ibe
C pnevmo_log.fgi( 164, 280):������,pnevmo_lamp01
      L_(33)=.false.
C pnevmo_log.fgi(  29, 277):��������� ���������� (�������)
      L_(32)=.false.
C pnevmo_log.fgi(  29, 281):��������� ���������� (�������)
      L0_ide=R0_ade.ne.R0_ube
      R0_ube=R0_ade
C pnevmo_log.fgi(  14, 279):���������� ������������� ������
      if(L_(32).or.L_(33)) then
         L0_ode=(L_(32).or.L0_ode).and..not.(L_(33))
      else
         if(L0_ide.and..not.L0_ede) L0_ode=.not.L0_ode
      endif
      L0_ede=L0_ide
      L_(34)=.not.L0_ode
C pnevmo_log.fgi(  35, 279):T �������
      if(L0_ode) then
         I_obe=1
      else
         I_obe=0
      endif
C pnevmo_log.fgi(  48, 281):��������� LO->1
      L_(35)=.false.
C jgt_log.fgi(  80, 126):��������� ���������� (�������)
      I_ile=I_osofe
C jgt_log.fgi( 146, 140):������,se_sv
      I_ome=I_usofe
C jgt_log.fgi( 146, 142):������,mi_sv
      I_epe=I_atofe
C jgt_log.fgi( 146, 144):������,ho_sv
      I_uke=I_otofe
C jgt_log.fgi( 146, 146):������,ye_sv
      I_eme=I_itofe
C jgt_log.fgi( 146, 148):������,mo_sv
      I_ope=I_etofe
C jgt_log.fgi( 146, 150):������,day_sv
      I_ole=I_osofe
C jgt_log.fgi( 122, 140):������,se_opr
      I_ume=I_usofe
C jgt_log.fgi( 122, 142):������,mi_opr
      I_ipe=I_atofe
C jgt_log.fgi( 122, 144):������,ho_opr
      I_ale=I_otofe
C jgt_log.fgi( 122, 146):������,ye_opr
      I_ime=I_itofe
C jgt_log.fgi( 122, 148):������,mo_opr
      I_upe=I_etofe
C jgt_log.fgi( 122, 150):������,day_opr
      call date_and_time(C10_eke, C10_ake, C10_ufe, I_utofe
     &)
C jgt_log.fgi( 127, 159):��������� ������� ���� � �������,GET_T
      L_(37)=L_oke.and..not.L0_ike
      L0_ike=L_oke
C jgt_log.fgi(  72, 202):������������  �� 1 ���
      L_ire=R0_ure.ne.R0_ore
      R0_ore=R0_ure
C jgt_log.fgi(  54, 216):���������� ������������� ������
      if(L_ire)then
        I_ife=1
        do while(I_ife.le.40)
          call putotable(I_ife,1234567890,0.0,00,00,00,00
     &,00,00,00,00,00,00,00,00,C255_ere,.false.,.true.,C255_ofe
     &)
          I_ife=I_ife+1
        end do
      endif
C jgt_log.fgi( 127, 219):������� �������
      R0_uki=R0_uki+deltat
      if(R0_uki.ge.R0_ali) R0_uki=0.0
      L_ipo=R0_uki.le.R0_oki.and.R0_uki.gt.0.0
C dsu_log.fgi( 146, 281):�������������� ���������
      L_uti=L_ipo
C dsu_log.fgi( 164, 168):������,dsu_lamp01
      L_eli=L_ipo
C dsu_log.fgi( 164, 280):������,dsu_lamp29
      L_ili=L_ipo
C dsu_log.fgi( 164, 276):������,dsu_lamp28
      L_oli=L_ipo
C dsu_log.fgi( 164, 272):������,dsu_lamp27
      L_uli=L_ipo
C dsu_log.fgi( 164, 268):������,dsu_lamp26
      L_ami=L_ipo
C dsu_log.fgi( 164, 264):������,dsu_lamp25
      L_emi=L_ipo
C dsu_log.fgi( 164, 260):������,dsu_lamp24
      L_imi=L_ipo
C dsu_log.fgi( 164, 256):������,dsu_lamp23
      L_omi=L_ipo
C dsu_log.fgi( 164, 252):������,dsu_lamp22
      L_umi=L_ipo
C dsu_log.fgi( 164, 248):������,dsu_lamp21
      L_api=L_ipo
C dsu_log.fgi( 164, 244):������,dsu_lamp20
      L_epi=L_ipo
C dsu_log.fgi( 164, 240):������,dsu_lamp19
      L_ipi=L_ipo
C dsu_log.fgi( 164, 236):������,dsu_lamp18
      L_opi=L_ipo
C dsu_log.fgi( 164, 232):������,dsu_lamp17
      L_upi=L_ipo
C dsu_log.fgi( 164, 228):������,dsu_lamp16
      L_ari=L_ipo
C dsu_log.fgi( 164, 224):������,dsu_lamp15
      L_eri=L_ipo
C dsu_log.fgi( 164, 220):������,dsu_lamp14
      L_iri=L_ipo
C dsu_log.fgi( 164, 216):������,dsu_lamp13
      L_ori=L_ipo
C dsu_log.fgi( 164, 212):������,dsu_lamp12
      L_uri=L_ipo
C dsu_log.fgi( 164, 208):������,dsu_lamp11
      L_asi=L_ipo
C dsu_log.fgi( 164, 204):������,dsu_lamp10
      L_esi=L_ipo
C dsu_log.fgi( 164, 200):������,dsu_lamp09
      L_isi=L_ipo
C dsu_log.fgi( 164, 196):������,dsu_lamp08
      L_osi=L_ipo
C dsu_log.fgi( 164, 192):������,dsu_lamp07
      L_usi=L_ipo
C dsu_log.fgi( 164, 188):������,dsu_lamp06
      L_ati=L_ipo
C dsu_log.fgi( 164, 184):������,dsu_lamp05
      L_eti=L_ipo
C dsu_log.fgi( 164, 180):������,dsu_lamp04
      L_iti=L_ipo
C dsu_log.fgi( 164, 176):������,dsu_lamp03
      L_oti=L_ipo
C dsu_log.fgi( 164, 172):������,dsu_lamp02
      L_iki=L_ipo
C dsu_log.fgi( 164,   9):������,dsu_tablo01
      L_eki=L_ipo
C dsu_log.fgi( 164,  13):������,dsu_tablo02
      L_aki=L_ipo
C dsu_log.fgi( 164,  17):������,dsu_tablo03
      L_ufi=L_ipo
C dsu_log.fgi( 164,  21):������,dsu_tablo04
      L_ofi=L_ipo
C dsu_log.fgi( 164,  25):������,dsu_tablo05
      L_ifi=L_ipo
C dsu_log.fgi( 164,  29):������,dsu_tablo06
      L_efi=L_ipo
C dsu_log.fgi( 164,  33):������,dsu_tablo07
      L_afi=L_ipo
C dsu_log.fgi( 164,  37):������,dsu_tablo08
      L_udi=L_ipo
C dsu_log.fgi( 164,  41):������,dsu_tablo09
      L_odi=L_ipo
C dsu_log.fgi( 164,  45):������,dsu_tablo10
      L_idi=L_ipo
C dsu_log.fgi( 164,  49):������,dsu_tablo11
      L_edi=L_ipo
C dsu_log.fgi( 164,  53):������,dsu_tablo12
      L_adi=L_ipo
C dsu_log.fgi( 164,  57):������,dsu_tablo13
      L_ubi=L_ipo
C dsu_log.fgi( 164,  61):������,dsu_tablo14
      L_obi=L_ipo
C dsu_log.fgi( 164,  65):������,dsu_tablo15
      L_ibi=L_ipo
C dsu_log.fgi( 164,  69):������,dsu_tablo16
      L_ebi=L_ipo
C dsu_log.fgi( 164,  73):������,dsu_tablo17
      L_abi=L_ipo
C dsu_log.fgi( 164,  77):������,dsu_tablo18
      L_uxe=L_ipo
C dsu_log.fgi( 164,  81):������,dsu_tablo19
      L_oxe=L_ipo
C dsu_log.fgi( 164,  85):������,dsu_tablo20
      L_ixe=L_ipo
C dsu_log.fgi( 164,  89):������,dsu_tablo21
      L_exe=L_ipo
C dsu_log.fgi( 164,  93):������,dsu_tablo22
      L_axe=L_ipo
C dsu_log.fgi( 164,  97):������,dsu_tablo23
      L_uve=L_ipo
C dsu_log.fgi( 164, 101):������,dsu_tablo24
      L_ove=L_ipo
C dsu_log.fgi( 164, 105):������,dsu_tablo25
      L_ive=L_ipo
C dsu_log.fgi( 164, 109):������,dsu_tablo26
      L_eve=L_ipo
C dsu_log.fgi( 164, 113):������,dsu_tablo27
      L_ave=L_ipo
C dsu_log.fgi( 164, 117):������,dsu_tablo28
      L_ute=L_ipo
C dsu_log.fgi( 164, 121):������,dsu_tablo29
      L_ote=L_ipo
C dsu_log.fgi( 164, 125):������,dsu_tablo30
      L_ite=L_ipo
C dsu_log.fgi( 164, 129):������,dsu_tablo31
      L_ete=L_ipo
C dsu_log.fgi( 164, 133):������,dsu_tablo32
      L_ate=L_ipo
C dsu_log.fgi( 164, 137):������,dsu_tablo33
      L_use=L_ipo
C dsu_log.fgi( 164, 141):������,dsu_tablo34
      L_ose=L_ipo
C dsu_log.fgi( 164, 145):������,dsu_tablo35
      L_ise=L_ipo
C dsu_log.fgi( 164, 149):������,dsu_tablo36
      L_ese=L_ipo
C dsu_log.fgi( 164, 153):������,dsu_tablo37
      L_ase=L_ipo
C dsu_log.fgi( 164, 157):������,dsu_tablo38
      L_(40)=.false.
C dsu_log.fgi(  29, 185):��������� ���������� (�������)
      L_(39)=.false.
C dsu_log.fgi(  29, 189):��������� ���������� (�������)
      L0_uvi=R0_ivi.ne.R0_evi
      R0_evi=R0_ivi
C dsu_log.fgi(  14, 187):���������� ������������� ������
      if(L_(39).or.L_(40)) then
         L0_axi=(L_(39).or.L0_axi).and..not.(L_(40))
      else
         if(L0_uvi.and..not.L0_ovi) L0_axi=.not.L0_axi
      endif
      L0_ovi=L0_uvi
      L_(41)=.not.L0_axi
C dsu_log.fgi(  35, 187):T �������
      if(L0_axi) then
         I_avi=1
      else
         I_avi=0
      endif
C dsu_log.fgi(  48, 189):��������� LO->1
      L_(43)=.false.
C dsu_log.fgi(  29, 198):��������� ���������� (�������)
      L_(42)=.false.
C dsu_log.fgi(  29, 202):��������� ���������� (�������)
      L0_abo=R0_oxi.ne.R0_ixi
      R0_ixi=R0_oxi
C dsu_log.fgi(  14, 200):���������� ������������� ������
      if(L_(42).or.L_(43)) then
         L0_ebo=(L_(42).or.L0_ebo).and..not.(L_(43))
      else
         if(L0_abo.and..not.L0_uxi) L0_ebo=.not.L0_ebo
      endif
      L0_uxi=L0_abo
      L_(44)=.not.L0_ebo
C dsu_log.fgi(  35, 200):T �������
      if(L0_ebo) then
         I_exi=1
      else
         I_exi=0
      endif
C dsu_log.fgi(  48, 202):��������� LO->1
      L_(46)=.false.
C dsu_log.fgi(  29, 211):��������� ���������� (�������)
      L_(45)=.false.
C dsu_log.fgi(  29, 215):��������� ���������� (�������)
      L0_edo=R0_ubo.ne.R0_obo
      R0_obo=R0_ubo
C dsu_log.fgi(  14, 213):���������� ������������� ������
      if(L_(45).or.L_(46)) then
         L0_ido=(L_(45).or.L0_ido).and..not.(L_(46))
      else
         if(L0_edo.and..not.L0_ado) L0_ido=.not.L0_ido
      endif
      L0_ado=L0_edo
      L_(47)=.not.L0_ido
C dsu_log.fgi(  35, 213):T �������
      if(L0_ido) then
         I_ibo=1
      else
         I_ibo=0
      endif
C dsu_log.fgi(  48, 215):��������� LO->1
      L_(49)=.false.
C dsu_log.fgi(  29, 224):��������� ���������� (�������)
      L_(48)=.false.
C dsu_log.fgi(  29, 228):��������� ���������� (�������)
      L0_ifo=R0_afo.ne.R0_udo
      R0_udo=R0_afo
C dsu_log.fgi(  14, 226):���������� ������������� ������
      if(L_(48).or.L_(49)) then
         L0_ofo=(L_(48).or.L0_ofo).and..not.(L_(49))
      else
         if(L0_ifo.and..not.L0_efo) L0_ofo=.not.L0_ofo
      endif
      L0_efo=L0_ifo
      L_(50)=.not.L0_ofo
C dsu_log.fgi(  35, 226):T �������
      if(L0_ofo) then
         I_odo=1
      else
         I_odo=0
      endif
C dsu_log.fgi(  48, 228):��������� LO->1
      L_(52)=.false.
C dsu_log.fgi(  29, 237):��������� ���������� (�������)
      L_(51)=.false.
C dsu_log.fgi(  29, 241):��������� ���������� (�������)
      L0_oko=R0_eko.ne.R0_ako
      R0_ako=R0_eko
C dsu_log.fgi(  14, 239):���������� ������������� ������
      if(L_(51).or.L_(52)) then
         L0_uko=(L_(51).or.L0_uko).and..not.(L_(52))
      else
         if(L0_oko.and..not.L0_iko) L0_uko=.not.L0_uko
      endif
      L0_iko=L0_oko
      L_(53)=.not.L0_uko
C dsu_log.fgi(  35, 239):T �������
      if(L0_uko) then
         I_ufo=1
      else
         I_ufo=0
      endif
C dsu_log.fgi(  48, 241):��������� LO->1
      L_(55)=.false.
C dsu_log.fgi(  29, 250):��������� ���������� (�������)
      L_(54)=.false.
C dsu_log.fgi(  29, 254):��������� ���������� (�������)
      L0_ulo=R0_ilo.ne.R0_elo
      R0_elo=R0_ilo
C dsu_log.fgi(  14, 252):���������� ������������� ������
      if(L_(54).or.L_(55)) then
         L0_amo=(L_(54).or.L0_amo).and..not.(L_(55))
      else
         if(L0_ulo.and..not.L0_olo) L0_amo=.not.L0_amo
      endif
      L0_olo=L0_ulo
      L_(56)=.not.L0_amo
C dsu_log.fgi(  35, 252):T �������
      if(L0_amo) then
         I_alo=1
      else
         I_alo=0
      endif
C dsu_log.fgi(  48, 254):��������� LO->1
      L_(58)=.false.
C dsu_log.fgi(  29, 263):��������� ���������� (�������)
      L_(57)=.false.
C dsu_log.fgi(  29, 267):��������� ���������� (�������)
      L0_apo=R0_omo.ne.R0_imo
      R0_imo=R0_omo
C dsu_log.fgi(  14, 265):���������� ������������� ������
      if(L_(57).or.L_(58)) then
         L0_epo=(L_(57).or.L0_epo).and..not.(L_(58))
      else
         if(L0_apo.and..not.L0_umo) L0_epo=.not.L0_epo
      endif
      L0_umo=L0_apo
      L_(59)=.not.L0_epo
C dsu_log.fgi(  35, 265):T �������
      if(L0_epo) then
         I_emo=1
      else
         I_emo=0
      endif
C dsu_log.fgi(  48, 267):��������� LO->1
      L_(62)=.false.
C dsu_log.fgi(  29, 277):��������� ���������� (�������)
      L_(61)=.false.
C dsu_log.fgi(  29, 281):��������� ���������� (�������)
      L0_iro=R0_aro.ne.R0_upo
      R0_upo=R0_aro
C dsu_log.fgi(  14, 279):���������� ������������� ������
      if(L_(61).or.L_(62)) then
         L0_oro=(L_(61).or.L0_oro).and..not.(L_(62))
      else
         if(L0_iro.and..not.L0_ero) L0_oro=.not.L0_oro
      endif
      L0_ero=L0_iro
      L_(63)=.not.L0_oro
C dsu_log.fgi(  35, 279):T �������
      if(L0_oro) then
         I_opo=1
      else
         I_opo=0
      endif
C dsu_log.fgi(  48, 281):��������� LO->1
      R0_aso=R0_aso+deltat
      if(R0_aso.ge.R0_eso) R0_aso=0.0
      L_araf=R0_aso.le.R0_uro.and.R0_aso.gt.0.0
C dio_log.fgi( 405,  16):�������������� ���������
      L_upaf=L_araf
C dio_log.fgi( 432,  25):������,DiPanel01_lamp28
      L_opaf=L_araf
C dio_log.fgi( 432,  29):������,DiPanel01_lamp27
      L_ipaf=L_araf
C dio_log.fgi( 432,  33):������,DiPanel01_lamp26
      L_epaf=L_araf
C dio_log.fgi( 432,  37):������,DiPanel01_lamp25
      L_apaf=L_araf
C dio_log.fgi( 432,  41):������,DiPanel01_lamp24
      L_umaf=L_araf
C dio_log.fgi( 432,  45):������,DiPanel01_lamp23
      L_omaf=L_araf
C dio_log.fgi( 432,  49):������,DiPanel01_lamp22
      L_imaf=L_araf
C dio_log.fgi( 432,  53):������,DiPanel01_lamp21
      L_emaf=L_araf
C dio_log.fgi( 432,  57):������,DiPanel01_lamp20
      L_amaf=L_araf
C dio_log.fgi( 432,  61):������,DiPanel01_lamp19
      L_ulaf=L_araf
C dio_log.fgi( 432,  65):������,DiPanel01_lamp18
      L_olaf=L_araf
C dio_log.fgi( 432,  69):������,DiPanel01_lamp17
      L_ilaf=L_araf
C dio_log.fgi( 432,  73):������,DiPanel01_lamp16
      L_elaf=L_araf
C dio_log.fgi( 432,  77):������,DiPanel01_lamp15
      L_alaf=L_araf
C dio_log.fgi( 432,  81):������,DiPanel01_lamp14
      L_ukaf=L_araf
C dio_log.fgi( 432,  85):������,DiPanel01_lamp13
      L_okaf=L_araf
C dio_log.fgi( 432,  89):������,DiPanel01_lamp12
      L_ikaf=L_araf
C dio_log.fgi( 432,  93):������,DiPanel01_lamp11
      L_ekaf=L_araf
C dio_log.fgi( 432,  97):������,DiPanel01_lamp10
      L_akaf=L_araf
C dio_log.fgi( 432, 101):������,DiPanel01_lamp09
      L_ufaf=L_araf
C dio_log.fgi( 432, 105):������,DiPanel01_lamp08
      L_ofaf=L_araf
C dio_log.fgi( 432, 109):������,DiPanel01_lamp07
      L_ifaf=L_araf
C dio_log.fgi( 432, 113):������,DiPanel01_lamp06
      L_efaf=L_araf
C dio_log.fgi( 432, 117):������,DiPanel01_lamp05
      L_afaf=L_araf
C dio_log.fgi( 432, 121):������,DiPanel01_lamp04
      L_udaf=L_araf
C dio_log.fgi( 432, 125):������,DiPanel01_lamp03
      L_odaf=L_araf
C dio_log.fgi( 432, 129):������,DiPanel01_lamp02
      L_idaf=L_araf
C dio_log.fgi( 432, 133):������,DiPanel01_lamp01
      L_edaf=L_araf
C dio_log.fgi( 432, 137):������,DiPanel01_lamp56
      L_adaf=L_araf
C dio_log.fgi( 432, 141):������,DiPanel01_lamp55
      L_ubaf=L_araf
C dio_log.fgi( 432, 145):������,DiPanel01_lamp54
      L_obaf=L_araf
C dio_log.fgi( 432, 149):������,DiPanel01_lamp53
      L_ibaf=L_araf
C dio_log.fgi( 432, 153):������,DiPanel01_lamp52
      L_ebaf=L_araf
C dio_log.fgi( 432, 157):������,DiPanel01_lamp51
      L_abaf=L_araf
C dio_log.fgi( 432, 161):������,DiPanel01_lamp50
      L_uxud=L_araf
C dio_log.fgi( 432, 165):������,DiPanel01_lamp49
      L_oxud=L_araf
C dio_log.fgi( 432, 169):������,DiPanel01_lamp48
      L_ixud=L_araf
C dio_log.fgi( 432, 173):������,DiPanel01_lamp47
      L_exud=L_araf
C dio_log.fgi( 432, 177):������,DiPanel01_lamp46
      L_axud=L_araf
C dio_log.fgi( 432, 181):������,DiPanel01_lamp45
      L_uvud=L_araf
C dio_log.fgi( 432, 185):������,DiPanel01_lamp44
      L_ovud=L_araf
C dio_log.fgi( 432, 189):������,DiPanel01_lamp43
      L_ivud=L_araf
C dio_log.fgi( 432, 193):������,DiPanel01_lamp42
      L_evud=L_araf
C dio_log.fgi( 432, 197):������,DiPanel01_lamp41
      L_avud=L_araf
C dio_log.fgi( 432, 201):������,DiPanel01_lamp40
      L_utud=L_araf
C dio_log.fgi( 432, 205):������,DiPanel01_lamp39
      L_otud=L_araf
C dio_log.fgi( 432, 209):������,DiPanel01_lamp38
      L_itud=L_araf
C dio_log.fgi( 432, 213):������,DiPanel01_lamp37
      L_etud=L_araf
C dio_log.fgi( 432, 217):������,DiPanel01_lamp36
      L_atud=L_araf
C dio_log.fgi( 432, 221):������,DiPanel01_lamp35
      L_usud=L_araf
C dio_log.fgi( 432, 225):������,DiPanel01_lamp34
      L_osud=L_araf
C dio_log.fgi( 432, 229):������,DiPanel01_lamp33
      L_isud=L_araf
C dio_log.fgi( 432, 233):������,DiPanel01_lamp32
      L_esud=L_araf
C dio_log.fgi( 432, 237):������,DiPanel01_lamp31
      L_asud=L_araf
C dio_log.fgi( 432, 241):������,DiPanel01_lamp30
      L_urud=L_araf
C dio_log.fgi( 432, 245):������,DiPanel01_lamp29
      L_uvod=L_araf
C dio_log.fgi( 462,  25):������,DiPanel02_lamp28
      L_ovod=L_araf
C dio_log.fgi( 462,  29):������,DiPanel02_lamp27
      L_ivod=L_araf
C dio_log.fgi( 462,  33):������,DiPanel02_lamp26
      L_evod=L_araf
C dio_log.fgi( 462,  37):������,DiPanel02_lamp25
      L_avod=L_araf
C dio_log.fgi( 462,  41):������,DiPanel02_lamp24
      L_utod=L_araf
C dio_log.fgi( 462,  45):������,DiPanel02_lamp23
      L_otod=L_araf
C dio_log.fgi( 462,  49):������,DiPanel02_lamp22
      L_itod=L_araf
C dio_log.fgi( 462,  53):������,DiPanel02_lamp21
      L_etod=L_araf
C dio_log.fgi( 462,  57):������,DiPanel02_lamp20
      L_atod=L_araf
C dio_log.fgi( 462,  61):������,DiPanel02_lamp19
      L_usod=L_araf
C dio_log.fgi( 462,  65):������,DiPanel02_lamp18
      L_osod=L_araf
C dio_log.fgi( 462,  69):������,DiPanel02_lamp17
      L_isod=L_araf
C dio_log.fgi( 462,  73):������,DiPanel02_lamp16
      L_esod=L_araf
C dio_log.fgi( 462,  77):������,DiPanel02_lamp15
      L_asod=L_araf
C dio_log.fgi( 462,  81):������,DiPanel02_lamp14
      L_urod=L_araf
C dio_log.fgi( 462,  85):������,DiPanel02_lamp13
      L_orod=L_araf
C dio_log.fgi( 462,  89):������,DiPanel02_lamp12
      L_irod=L_araf
C dio_log.fgi( 462,  93):������,DiPanel02_lamp11
      L_erod=L_araf
C dio_log.fgi( 462,  97):������,DiPanel02_lamp10
      L_arod=L_araf
C dio_log.fgi( 462, 101):������,DiPanel02_lamp09
      L_upod=L_araf
C dio_log.fgi( 462, 105):������,DiPanel02_lamp08
      L_opod=L_araf
C dio_log.fgi( 462, 109):������,DiPanel02_lamp07
      L_ipod=L_araf
C dio_log.fgi( 462, 113):������,DiPanel02_lamp06
      L_epod=L_araf
C dio_log.fgi( 462, 117):������,DiPanel02_lamp05
      L_apod=L_araf
C dio_log.fgi( 462, 121):������,DiPanel02_lamp04
      L_umod=L_araf
C dio_log.fgi( 462, 125):������,DiPanel02_lamp03
      L_omod=L_araf
C dio_log.fgi( 462, 129):������,DiPanel02_lamp02
      L_imod=L_araf
C dio_log.fgi( 462, 133):������,DiPanel02_lamp01
      L_emod=L_araf
C dio_log.fgi( 462, 137):������,DiPanel02_lamp56
      L_amod=L_araf
C dio_log.fgi( 462, 141):������,DiPanel02_lamp55
      L_ulod=L_araf
C dio_log.fgi( 462, 145):������,DiPanel02_lamp54
      L_olod=L_araf
C dio_log.fgi( 462, 149):������,DiPanel02_lamp53
      L_ilod=L_araf
C dio_log.fgi( 462, 153):������,DiPanel02_lamp52
      L_elod=L_araf
C dio_log.fgi( 462, 157):������,DiPanel02_lamp51
      L_alod=L_araf
C dio_log.fgi( 462, 161):������,DiPanel02_lamp50
      L_ukod=L_araf
C dio_log.fgi( 462, 165):������,DiPanel02_lamp49
      L_okod=L_araf
C dio_log.fgi( 462, 169):������,DiPanel02_lamp48
      L_ikod=L_araf
C dio_log.fgi( 462, 173):������,DiPanel02_lamp47
      L_ekod=L_araf
C dio_log.fgi( 462, 177):������,DiPanel02_lamp46
      L_akod=L_araf
C dio_log.fgi( 462, 181):������,DiPanel02_lamp45
      L_ufod=L_araf
C dio_log.fgi( 462, 185):������,DiPanel02_lamp44
      L_ofod=L_araf
C dio_log.fgi( 462, 189):������,DiPanel02_lamp43
      L_ifod=L_araf
C dio_log.fgi( 462, 193):������,DiPanel02_lamp42
      L_efod=L_araf
C dio_log.fgi( 462, 197):������,DiPanel02_lamp41
      L_afod=L_araf
C dio_log.fgi( 462, 201):������,DiPanel02_lamp40
      L_udod=L_araf
C dio_log.fgi( 462, 205):������,DiPanel02_lamp39
      L_odod=L_araf
C dio_log.fgi( 462, 209):������,DiPanel02_lamp38
      L_idod=L_araf
C dio_log.fgi( 462, 213):������,DiPanel02_lamp37
      L_edod=L_araf
C dio_log.fgi( 462, 217):������,DiPanel02_lamp36
      L_adod=L_araf
C dio_log.fgi( 462, 221):������,DiPanel02_lamp35
      L_ubod=L_araf
C dio_log.fgi( 462, 225):������,DiPanel02_lamp34
      L_obod=L_araf
C dio_log.fgi( 462, 229):������,DiPanel02_lamp33
      L_ibod=L_araf
C dio_log.fgi( 462, 233):������,DiPanel02_lamp32
      L_ebod=L_araf
C dio_log.fgi( 462, 237):������,DiPanel02_lamp31
      L_abod=L_araf
C dio_log.fgi( 462, 241):������,DiPanel02_lamp30
      L_uxid=L_araf
C dio_log.fgi( 462, 245):������,DiPanel02_lamp29
      L_oxid=L_araf
C dio_log.fgi( 492,  25):������,DiPanel03_lamp28
      L_ixid=L_araf
C dio_log.fgi( 492,  29):������,DiPanel03_lamp27
      L_exid=L_araf
C dio_log.fgi( 492,  33):������,DiPanel03_lamp26
      L_axid=L_araf
C dio_log.fgi( 492,  37):������,DiPanel03_lamp25
      L_uvid=L_araf
C dio_log.fgi( 492,  41):������,DiPanel03_lamp24
      L_ovid=L_araf
C dio_log.fgi( 492,  45):������,DiPanel03_lamp23
      L_ivid=L_araf
C dio_log.fgi( 492,  49):������,DiPanel03_lamp22
      L_evid=L_araf
C dio_log.fgi( 492,  53):������,DiPanel03_lamp21
      L_avid=L_araf
C dio_log.fgi( 492,  57):������,DiPanel03_lamp20
      L_utid=L_araf
C dio_log.fgi( 492,  61):������,DiPanel03_lamp19
      L_otid=L_araf
C dio_log.fgi( 492,  65):������,DiPanel03_lamp18
      L_itid=L_araf
C dio_log.fgi( 492,  69):������,DiPanel03_lamp17
      L_etid=L_araf
C dio_log.fgi( 492,  73):������,DiPanel03_lamp16
      L_atid=L_araf
C dio_log.fgi( 492,  77):������,DiPanel03_lamp15
      L_usid=L_araf
C dio_log.fgi( 492,  81):������,DiPanel03_lamp14
      L_osid=L_araf
C dio_log.fgi( 492,  85):������,DiPanel03_lamp13
      L_isid=L_araf
C dio_log.fgi( 492,  89):������,DiPanel03_lamp12
      L_esid=L_araf
C dio_log.fgi( 492,  93):������,DiPanel03_lamp11
      L_asid=L_araf
C dio_log.fgi( 492,  97):������,DiPanel03_lamp10
      L_urid=L_araf
C dio_log.fgi( 492, 101):������,DiPanel03_lamp09
      L_orid=L_araf
C dio_log.fgi( 492, 105):������,DiPanel03_lamp08
      L_irid=L_araf
C dio_log.fgi( 492, 109):������,DiPanel03_lamp07
      L_erid=L_araf
C dio_log.fgi( 492, 113):������,DiPanel03_lamp06
      L_arid=L_araf
C dio_log.fgi( 492, 117):������,DiPanel03_lamp05
      L_upid=L_araf
C dio_log.fgi( 492, 121):������,DiPanel03_lamp04
      L_opid=L_araf
C dio_log.fgi( 492, 125):������,DiPanel03_lamp03
      L_ipid=L_araf
C dio_log.fgi( 492, 129):������,DiPanel03_lamp02
      L_epid=L_araf
C dio_log.fgi( 492, 133):������,DiPanel03_lamp01
      L_apid=L_araf
C dio_log.fgi( 492, 137):������,DiPanel03_lamp56
      L_umid=L_araf
C dio_log.fgi( 492, 141):������,DiPanel03_lamp55
      L_omid=L_araf
C dio_log.fgi( 492, 145):������,DiPanel03_lamp54
      L_imid=L_araf
C dio_log.fgi( 492, 149):������,DiPanel03_lamp53
      L_emid=L_araf
C dio_log.fgi( 492, 153):������,DiPanel03_lamp52
      L_amid=L_araf
C dio_log.fgi( 492, 157):������,DiPanel03_lamp51
      L_ulid=L_araf
C dio_log.fgi( 492, 161):������,DiPanel03_lamp50
      L_olid=L_araf
C dio_log.fgi( 492, 165):������,DiPanel03_lamp49
      L_ilid=L_araf
C dio_log.fgi( 492, 169):������,DiPanel03_lamp48
      L_elid=L_araf
C dio_log.fgi( 492, 173):������,DiPanel03_lamp47
      L_alid=L_araf
C dio_log.fgi( 492, 177):������,DiPanel03_lamp46
      L_ukid=L_araf
C dio_log.fgi( 492, 181):������,DiPanel03_lamp45
      L_okid=L_araf
C dio_log.fgi( 492, 185):������,DiPanel03_lamp44
      L_ikid=L_araf
C dio_log.fgi( 492, 189):������,DiPanel03_lamp43
      L_ekid=L_araf
C dio_log.fgi( 492, 193):������,DiPanel03_lamp42
      L_akid=L_araf
C dio_log.fgi( 492, 197):������,DiPanel03_lamp41
      L_ufid=L_araf
C dio_log.fgi( 492, 201):������,DiPanel03_lamp40
      L_ofid=L_araf
C dio_log.fgi( 492, 205):������,DiPanel03_lamp39
      L_ifid=L_araf
C dio_log.fgi( 492, 209):������,DiPanel03_lamp38
      L_efid=L_araf
C dio_log.fgi( 492, 213):������,DiPanel03_lamp37
      L_afid=L_araf
C dio_log.fgi( 492, 217):������,DiPanel03_lamp36
      L_udid=L_araf
C dio_log.fgi( 492, 221):������,DiPanel03_lamp35
      L_odid=L_araf
C dio_log.fgi( 492, 225):������,DiPanel03_lamp34
      L_idid=L_araf
C dio_log.fgi( 492, 229):������,DiPanel03_lamp33
      L_edid=L_araf
C dio_log.fgi( 492, 233):������,DiPanel03_lamp32
      L_adid=L_araf
C dio_log.fgi( 492, 237):������,DiPanel03_lamp31
      L_ubid=L_araf
C dio_log.fgi( 492, 241):������,DiPanel03_lamp30
      L_obid=L_araf
C dio_log.fgi( 492, 245):������,DiPanel03_lamp29
      L_ibid=L_araf
C dio_log.fgi( 522,  25):������,DiPanel04_lamp28
      L_ebid=L_araf
C dio_log.fgi( 522,  29):������,DiPanel04_lamp27
      L_abid=L_araf
C dio_log.fgi( 522,  33):������,DiPanel04_lamp26
      L_uxed=L_araf
C dio_log.fgi( 522,  37):������,DiPanel04_lamp25
      L_oxed=L_araf
C dio_log.fgi( 522,  41):������,DiPanel04_lamp24
      L_ixed=L_araf
C dio_log.fgi( 522,  45):������,DiPanel04_lamp23
      L_exed=L_araf
C dio_log.fgi( 522,  49):������,DiPanel04_lamp22
      L_axed=L_araf
C dio_log.fgi( 522,  53):������,DiPanel04_lamp21
      L_uved=L_araf
C dio_log.fgi( 522,  57):������,DiPanel04_lamp20
      L_oved=L_araf
C dio_log.fgi( 522,  61):������,DiPanel04_lamp19
      L_ived=L_araf
C dio_log.fgi( 522,  65):������,DiPanel04_lamp18
      L_eved=L_araf
C dio_log.fgi( 522,  69):������,DiPanel04_lamp17
      L_aved=L_araf
C dio_log.fgi( 522,  73):������,DiPanel04_lamp16
      L_uted=L_araf
C dio_log.fgi( 522,  77):������,DiPanel04_lamp15
      L_oted=L_araf
C dio_log.fgi( 522,  81):������,DiPanel04_lamp14
      L_ited=L_araf
C dio_log.fgi( 522,  85):������,DiPanel04_lamp13
      L_eted=L_araf
C dio_log.fgi( 522,  89):������,DiPanel04_lamp12
      L_ated=L_araf
C dio_log.fgi( 522,  93):������,DiPanel04_lamp11
      L_used=L_araf
C dio_log.fgi( 522,  97):������,DiPanel04_lamp10
      L_osed=L_araf
C dio_log.fgi( 522, 101):������,DiPanel04_lamp09
      L_ised=L_araf
C dio_log.fgi( 522, 105):������,DiPanel04_lamp08
      L_esed=L_araf
C dio_log.fgi( 522, 109):������,DiPanel04_lamp07
      L_ased=L_araf
C dio_log.fgi( 522, 113):������,DiPanel04_lamp06
      L_ured=L_araf
C dio_log.fgi( 522, 117):������,DiPanel04_lamp05
      L_ored=L_araf
C dio_log.fgi( 522, 121):������,DiPanel04_lamp04
      L_ired=L_araf
C dio_log.fgi( 522, 125):������,DiPanel04_lamp03
      L_ered=L_araf
C dio_log.fgi( 522, 129):������,DiPanel04_lamp02
      L_ared=L_araf
C dio_log.fgi( 522, 133):������,DiPanel04_lamp01
      L_uped=L_araf
C dio_log.fgi( 522, 137):������,DiPanel04_lamp56
      L_oped=L_araf
C dio_log.fgi( 522, 141):������,DiPanel04_lamp55
      L_iped=L_araf
C dio_log.fgi( 522, 145):������,DiPanel04_lamp54
      L_eped=L_araf
C dio_log.fgi( 522, 149):������,DiPanel04_lamp53
      L_aped=L_araf
C dio_log.fgi( 522, 153):������,DiPanel04_lamp52
      L_umed=L_araf
C dio_log.fgi( 522, 157):������,DiPanel04_lamp51
      L_omed=L_araf
C dio_log.fgi( 522, 161):������,DiPanel04_lamp50
      L_imed=L_araf
C dio_log.fgi( 522, 165):������,DiPanel04_lamp49
      L_emed=L_araf
C dio_log.fgi( 522, 169):������,DiPanel04_lamp48
      L_amed=L_araf
C dio_log.fgi( 522, 173):������,DiPanel04_lamp47
      L_uled=L_araf
C dio_log.fgi( 522, 177):������,DiPanel04_lamp46
      L_oled=L_araf
C dio_log.fgi( 522, 181):������,DiPanel04_lamp45
      L_iled=L_araf
C dio_log.fgi( 522, 185):������,DiPanel04_lamp44
      L_eled=L_araf
C dio_log.fgi( 522, 189):������,DiPanel04_lamp43
      L_aled=L_araf
C dio_log.fgi( 522, 193):������,DiPanel04_lamp42
      L_uked=L_araf
C dio_log.fgi( 522, 197):������,DiPanel04_lamp41
      L_oked=L_araf
C dio_log.fgi( 522, 201):������,DiPanel04_lamp40
      L_iked=L_araf
C dio_log.fgi( 522, 205):������,DiPanel04_lamp39
      L_eked=L_araf
C dio_log.fgi( 522, 209):������,DiPanel04_lamp38
      L_aked=L_araf
C dio_log.fgi( 522, 213):������,DiPanel04_lamp37
      L_ufed=L_araf
C dio_log.fgi( 522, 217):������,DiPanel04_lamp36
      L_ofed=L_araf
C dio_log.fgi( 522, 221):������,DiPanel04_lamp35
      L_ifed=L_araf
C dio_log.fgi( 522, 225):������,DiPanel04_lamp34
      L_efed=L_araf
C dio_log.fgi( 522, 229):������,DiPanel04_lamp33
      L_afed=L_araf
C dio_log.fgi( 522, 233):������,DiPanel04_lamp32
      L_uded=L_araf
C dio_log.fgi( 522, 237):������,DiPanel04_lamp31
      L_oded=L_araf
C dio_log.fgi( 522, 241):������,DiPanel04_lamp30
      L_ided=L_araf
C dio_log.fgi( 522, 245):������,DiPanel04_lamp29
      L_eded=L_araf
C dio_log.fgi( 554,  25):������,DiPanel05_lamp28
      L_aded=L_araf
C dio_log.fgi( 554,  29):������,DiPanel05_lamp27
      L_ubed=L_araf
C dio_log.fgi( 554,  33):������,DiPanel05_lamp26
      L_obed=L_araf
C dio_log.fgi( 554,  37):������,DiPanel05_lamp25
      L_ibed=L_araf
C dio_log.fgi( 554,  41):������,DiPanel05_lamp24
      L_ebed=L_araf
C dio_log.fgi( 554,  45):������,DiPanel05_lamp23
      L_abed=L_araf
C dio_log.fgi( 554,  49):������,DiPanel05_lamp22
      L_uxad=L_araf
C dio_log.fgi( 554,  53):������,DiPanel05_lamp21
      L_oxad=L_araf
C dio_log.fgi( 554,  57):������,DiPanel05_lamp20
      L_ixad=L_araf
C dio_log.fgi( 554,  61):������,DiPanel05_lamp19
      L_exad=L_araf
C dio_log.fgi( 554,  65):������,DiPanel05_lamp18
      L_axad=L_araf
C dio_log.fgi( 554,  69):������,DiPanel05_lamp17
      L_uvad=L_araf
C dio_log.fgi( 554,  73):������,DiPanel05_lamp16
      L_ovad=L_araf
C dio_log.fgi( 554,  77):������,DiPanel05_lamp15
      L_ivad=L_araf
C dio_log.fgi( 554,  81):������,DiPanel05_lamp14
      L_evad=L_araf
C dio_log.fgi( 554,  85):������,DiPanel05_lamp13
      L_avad=L_araf
C dio_log.fgi( 554,  89):������,DiPanel05_lamp12
      L_utad=L_araf
C dio_log.fgi( 554,  93):������,DiPanel05_lamp11
      L_otad=L_araf
C dio_log.fgi( 554,  97):������,DiPanel05_lamp10
      L_itad=L_araf
C dio_log.fgi( 554, 101):������,DiPanel05_lamp09
      L_etad=L_araf
C dio_log.fgi( 554, 105):������,DiPanel05_lamp08
      L_atad=L_araf
C dio_log.fgi( 554, 109):������,DiPanel05_lamp07
      L_usad=L_araf
C dio_log.fgi( 554, 113):������,DiPanel05_lamp06
      L_osad=L_araf
C dio_log.fgi( 554, 117):������,DiPanel05_lamp05
      L_isad=L_araf
C dio_log.fgi( 554, 121):������,DiPanel05_lamp04
      L_esad=L_araf
C dio_log.fgi( 554, 125):������,DiPanel05_lamp03
      L_asad=L_araf
C dio_log.fgi( 554, 129):������,DiPanel05_lamp02
      L_urad=L_araf
C dio_log.fgi( 554, 133):������,DiPanel05_lamp01
      L_orad=L_araf
C dio_log.fgi( 554, 137):������,DiPanel05_lamp32
      L_irad=L_araf
C dio_log.fgi( 554, 141):������,DiPanel05_lamp31
      L_erad=L_araf
C dio_log.fgi( 554, 145):������,DiPanel05_lamp30
      L_arad=L_araf
C dio_log.fgi( 554, 149):������,DiPanel05_lamp29
      L_upad=L_araf
C dio_log.fgi( 586,  25):������,DiPanel06_lamp28
      L_opad=L_araf
C dio_log.fgi( 586,  29):������,DiPanel06_lamp27
      L_ipad=L_araf
C dio_log.fgi( 586,  33):������,DiPanel06_lamp26
      L_epad=L_araf
C dio_log.fgi( 586,  37):������,DiPanel06_lamp25
      L_apad=L_araf
C dio_log.fgi( 586,  41):������,DiPanel06_lamp24
      L_umad=L_araf
C dio_log.fgi( 586,  45):������,DiPanel06_lamp23
      L_omad=L_araf
C dio_log.fgi( 586,  49):������,DiPanel06_lamp22
      L_imad=L_araf
C dio_log.fgi( 586,  53):������,DiPanel06_lamp21
      L_emad=L_araf
C dio_log.fgi( 586,  57):������,DiPanel06_lamp20
      L_amad=L_araf
C dio_log.fgi( 586,  61):������,DiPanel06_lamp19
      L_ulad=L_araf
C dio_log.fgi( 586,  65):������,DiPanel06_lamp18
      L_olad=L_araf
C dio_log.fgi( 586,  69):������,DiPanel06_lamp17
      L_ilad=L_araf
C dio_log.fgi( 586,  73):������,DiPanel06_lamp16
      L_elad=L_araf
C dio_log.fgi( 586,  77):������,DiPanel06_lamp15
      L_alad=L_araf
C dio_log.fgi( 586,  81):������,DiPanel06_lamp14
      L_ukad=L_araf
C dio_log.fgi( 586,  85):������,DiPanel06_lamp13
      L_okad=L_araf
C dio_log.fgi( 586,  89):������,DiPanel06_lamp12
      L_ikad=L_araf
C dio_log.fgi( 586,  93):������,DiPanel06_lamp11
      L_ekad=L_araf
C dio_log.fgi( 586,  97):������,DiPanel06_lamp10
      L_akad=L_araf
C dio_log.fgi( 586, 101):������,DiPanel06_lamp09
      L_ufad=L_araf
C dio_log.fgi( 586, 105):������,DiPanel06_lamp08
      L_ofad=L_araf
C dio_log.fgi( 586, 109):������,DiPanel06_lamp07
      L_ifad=L_araf
C dio_log.fgi( 586, 113):������,DiPanel06_lamp06
      L_efad=L_araf
C dio_log.fgi( 586, 117):������,DiPanel06_lamp05
      L_afad=L_araf
C dio_log.fgi( 586, 121):������,DiPanel06_lamp04
      L_udad=L_araf
C dio_log.fgi( 586, 125):������,DiPanel06_lamp03
      L_odad=L_araf
C dio_log.fgi( 586, 129):������,DiPanel06_lamp02
      L_idad=L_araf
C dio_log.fgi( 586, 133):������,DiPanel06_lamp01
      L_edad=L_araf
C dio_log.fgi( 586, 137):������,DiPanel06_lamp56
      L_adad=L_araf
C dio_log.fgi( 586, 141):������,DiPanel06_lamp55
      L_ubad=L_araf
C dio_log.fgi( 586, 145):������,DiPanel06_lamp54
      L_obad=L_araf
C dio_log.fgi( 586, 149):������,DiPanel06_lamp53
      L_ibad=L_araf
C dio_log.fgi( 586, 153):������,DiPanel06_lamp52
      L_ebad=L_araf
C dio_log.fgi( 586, 157):������,DiPanel06_lamp51
      L_abad=L_araf
C dio_log.fgi( 586, 161):������,DiPanel06_lamp50
      L_uxu=L_araf
C dio_log.fgi( 586, 165):������,DiPanel06_lamp49
      L_oxu=L_araf
C dio_log.fgi( 586, 169):������,DiPanel06_lamp48
      L_ixu=L_araf
C dio_log.fgi( 586, 173):������,DiPanel06_lamp47
      L_exu=L_araf
C dio_log.fgi( 586, 177):������,DiPanel06_lamp46
      L_axu=L_araf
C dio_log.fgi( 586, 181):������,DiPanel06_lamp45
      L_uvu=L_araf
C dio_log.fgi( 586, 185):������,DiPanel06_lamp44
      L_ovu=L_araf
C dio_log.fgi( 586, 189):������,DiPanel06_lamp43
      L_ivu=L_araf
C dio_log.fgi( 586, 193):������,DiPanel06_lamp42
      L_evu=L_araf
C dio_log.fgi( 586, 197):������,DiPanel06_lamp41
      L_avu=L_araf
C dio_log.fgi( 586, 201):������,DiPanel06_lamp40
      L_utu=L_araf
C dio_log.fgi( 586, 205):������,DiPanel06_lamp39
      L_otu=L_araf
C dio_log.fgi( 586, 209):������,DiPanel06_lamp38
      L_itu=L_araf
C dio_log.fgi( 586, 213):������,DiPanel06_lamp37
      L_etu=L_araf
C dio_log.fgi( 586, 217):������,DiPanel06_lamp36
      L_atu=L_araf
C dio_log.fgi( 586, 221):������,DiPanel06_lamp35
      L_usu=L_araf
C dio_log.fgi( 586, 225):������,DiPanel06_lamp34
      L_osu=L_araf
C dio_log.fgi( 586, 229):������,DiPanel06_lamp33
      L_isu=L_araf
C dio_log.fgi( 586, 233):������,DiPanel06_lamp32
      L_esu=L_araf
C dio_log.fgi( 586, 237):������,DiPanel06_lamp31
      L_asu=L_araf
C dio_log.fgi( 586, 241):������,DiPanel06_lamp30
      L_uru=L_araf
C dio_log.fgi( 586, 245):������,DiPanel06_lamp29
      L_oru=L_araf
C dio_log.fgi( 586, 249):������,DiPanel06_lamp58
      L_iru=L_araf
C dio_log.fgi( 586, 253):������,DiPanel06_lamp57
      L_eru=L_araf
C dio_log.fgi( 618,  25):������,DiPanel07_lamp28
      L_aru=L_araf
C dio_log.fgi( 618,  29):������,DiPanel07_lamp27
      L_upu=L_araf
C dio_log.fgi( 618,  33):������,DiPanel07_lamp26
      L_opu=L_araf
C dio_log.fgi( 618,  37):������,DiPanel07_lamp25
      L_ipu=L_araf
C dio_log.fgi( 618,  41):������,DiPanel07_lamp24
      L_epu=L_araf
C dio_log.fgi( 618,  45):������,DiPanel07_lamp23
      L_apu=L_araf
C dio_log.fgi( 618,  49):������,DiPanel07_lamp22
      L_umu=L_araf
C dio_log.fgi( 618,  53):������,DiPanel07_lamp21
      L_omu=L_araf
C dio_log.fgi( 618,  57):������,DiPanel07_lamp20
      L_imu=L_araf
C dio_log.fgi( 618,  61):������,DiPanel07_lamp19
      L_emu=L_araf
C dio_log.fgi( 618,  65):������,DiPanel07_lamp18
      L_amu=L_araf
C dio_log.fgi( 618,  69):������,DiPanel07_lamp17
      L_ulu=L_araf
C dio_log.fgi( 618,  73):������,DiPanel07_lamp16
      L_olu=L_araf
C dio_log.fgi( 618,  77):������,DiPanel07_lamp15
      L_ilu=L_araf
C dio_log.fgi( 618,  81):������,DiPanel07_lamp14
      L_elu=L_araf
C dio_log.fgi( 618,  85):������,DiPanel07_lamp13
      L_alu=L_araf
C dio_log.fgi( 618,  89):������,DiPanel07_lamp12
      L_uku=L_araf
C dio_log.fgi( 618,  93):������,DiPanel07_lamp11
      L_oku=L_araf
C dio_log.fgi( 618,  97):������,DiPanel07_lamp10
      L_iku=L_araf
C dio_log.fgi( 618, 101):������,DiPanel07_lamp09
      L_eku=L_araf
C dio_log.fgi( 618, 105):������,DiPanel07_lamp08
      L_aku=L_araf
C dio_log.fgi( 618, 109):������,DiPanel07_lamp07
      L_ufu=L_araf
C dio_log.fgi( 618, 113):������,DiPanel07_lamp06
      L_ofu=L_araf
C dio_log.fgi( 618, 117):������,DiPanel07_lamp05
      L_ifu=L_araf
C dio_log.fgi( 618, 121):������,DiPanel07_lamp04
      L_efu=L_araf
C dio_log.fgi( 618, 125):������,DiPanel07_lamp03
      L_afu=L_araf
C dio_log.fgi( 618, 129):������,DiPanel07_lamp02
      L_udu=L_araf
C dio_log.fgi( 618, 133):������,DiPanel07_lamp01
      L_odu=L_araf
C dio_log.fgi( 618, 137):������,DiPanel07_lamp56
      L_idu=L_araf
C dio_log.fgi( 618, 141):������,DiPanel07_lamp55
      L_edu=L_araf
C dio_log.fgi( 618, 145):������,DiPanel07_lamp54
      L_adu=L_araf
C dio_log.fgi( 618, 149):������,DiPanel07_lamp53
      L_ubu=L_araf
C dio_log.fgi( 618, 153):������,DiPanel07_lamp52
      L_obu=L_araf
C dio_log.fgi( 618, 157):������,DiPanel07_lamp51
      L_ibu=L_araf
C dio_log.fgi( 618, 161):������,DiPanel07_lamp50
      L_ebu=L_araf
C dio_log.fgi( 618, 165):������,DiPanel07_lamp49
      L_abu=L_araf
C dio_log.fgi( 618, 169):������,DiPanel07_lamp48
      L_uxo=L_araf
C dio_log.fgi( 618, 173):������,DiPanel07_lamp47
      L_oxo=L_araf
C dio_log.fgi( 618, 177):������,DiPanel07_lamp46
      L_ixo=L_araf
C dio_log.fgi( 618, 181):������,DiPanel07_lamp45
      L_exo=L_araf
C dio_log.fgi( 618, 185):������,DiPanel07_lamp44
      L_axo=L_araf
C dio_log.fgi( 618, 189):������,DiPanel07_lamp43
      L_uvo=L_araf
C dio_log.fgi( 618, 193):������,DiPanel07_lamp42
      L_ovo=L_araf
C dio_log.fgi( 618, 197):������,DiPanel07_lamp41
      L_ivo=L_araf
C dio_log.fgi( 618, 201):������,DiPanel07_lamp40
      L_evo=L_araf
C dio_log.fgi( 618, 205):������,DiPanel07_lamp39
      L_avo=L_araf
C dio_log.fgi( 618, 209):������,DiPanel07_lamp38
      L_uto=L_araf
C dio_log.fgi( 618, 213):������,DiPanel07_lamp37
      L_oto=L_araf
C dio_log.fgi( 618, 217):������,DiPanel07_lamp36
      L_ito=L_araf
C dio_log.fgi( 618, 221):������,DiPanel07_lamp35
      L_eto=L_araf
C dio_log.fgi( 618, 225):������,DiPanel07_lamp34
      L_ato=L_araf
C dio_log.fgi( 618, 229):������,DiPanel07_lamp33
      L_uso=L_araf
C dio_log.fgi( 618, 233):������,DiPanel07_lamp32
      L_oso=L_araf
C dio_log.fgi( 618, 237):������,DiPanel07_lamp31
      L_iso=L_araf
C dio_log.fgi( 618, 241):������,DiPanel07_lamp30
      L_orud=L_araf
C dio_log.fgi( 650,  33):������,DiPanel08_lamp28
      L_irud=L_araf
C dio_log.fgi( 650,  37):������,DiPanel08_lamp27
      L_erud=L_araf
C dio_log.fgi( 650,  41):������,DiPanel08_lamp26
      L_arud=L_araf
C dio_log.fgi( 650,  45):������,DiPanel08_lamp25
      L_upud=L_araf
C dio_log.fgi( 650,  49):������,DiPanel08_lamp24
      L_opud=L_araf
C dio_log.fgi( 650,  53):������,DiPanel08_lamp23
      L_ipud=L_araf
C dio_log.fgi( 650,  57):������,DiPanel08_lamp22
      L_epud=L_araf
C dio_log.fgi( 650,  61):������,DiPanel08_lamp21
      L_apud=L_araf
C dio_log.fgi( 650,  65):������,DiPanel08_lamp20
      L_umud=L_araf
C dio_log.fgi( 650,  69):������,DiPanel08_lamp19
      L_omud=L_araf
C dio_log.fgi( 650,  73):������,DiPanel08_lamp18
      L_imud=L_araf
C dio_log.fgi( 650,  77):������,DiPanel08_lamp17
      L_emud=L_araf
C dio_log.fgi( 650,  81):������,DiPanel08_lamp16
      L_amud=L_araf
C dio_log.fgi( 650,  85):������,DiPanel08_lamp15
      L_ulud=L_araf
C dio_log.fgi( 650,  89):������,DiPanel08_lamp14
      L_olud=L_araf
C dio_log.fgi( 650,  93):������,DiPanel08_lamp13
      L_ilud=L_araf
C dio_log.fgi( 650,  97):������,DiPanel08_lamp12
      L_elud=L_araf
C dio_log.fgi( 650, 101):������,DiPanel08_lamp11
      L_alud=L_araf
C dio_log.fgi( 650, 105):������,DiPanel08_lamp10
      L_ukud=L_araf
C dio_log.fgi( 650, 109):������,DiPanel08_lamp09
      L_okud=L_araf
C dio_log.fgi( 650, 113):������,DiPanel08_lamp08
      L_ikud=L_araf
C dio_log.fgi( 650, 117):������,DiPanel08_lamp07
      L_ekud=L_araf
C dio_log.fgi( 650, 121):������,DiPanel08_lamp06
      L_akud=L_araf
C dio_log.fgi( 650, 125):������,DiPanel08_lamp05
      L_ufud=L_araf
C dio_log.fgi( 650, 129):������,DiPanel08_lamp04
      L_ofud=L_araf
C dio_log.fgi( 650, 133):������,DiPanel08_lamp03
      L_ifud=L_araf
C dio_log.fgi( 650, 137):������,DiPanel08_lamp02
      L_efud=L_araf
C dio_log.fgi( 650, 141):������,DiPanel08_lamp01
      L_afud=L_araf
C dio_log.fgi( 650, 145):������,DiPanel08_lamp44
      L_udud=L_araf
C dio_log.fgi( 650, 149):������,DiPanel08_lamp43
      L_odud=L_araf
C dio_log.fgi( 650, 153):������,DiPanel08_lamp42
      L_idud=L_araf
C dio_log.fgi( 650, 157):������,DiPanel08_lamp41
      L_edud=L_araf
C dio_log.fgi( 650, 161):������,DiPanel08_lamp40
      L_adud=L_araf
C dio_log.fgi( 650, 165):������,DiPanel08_lamp39
      L_ubud=L_araf
C dio_log.fgi( 650, 169):������,DiPanel08_lamp38
      L_obud=L_araf
C dio_log.fgi( 650, 173):������,DiPanel08_lamp37
      L_ibud=L_araf
C dio_log.fgi( 650, 177):������,DiPanel08_lamp36
      L_ebud=L_araf
C dio_log.fgi( 650, 181):������,DiPanel08_lamp35
      L_abud=L_araf
C dio_log.fgi( 650, 185):������,DiPanel08_lamp34
      L_uxod=L_araf
C dio_log.fgi( 650, 189):������,DiPanel08_lamp33
      L_oxod=L_araf
C dio_log.fgi( 650, 193):������,DiPanel08_lamp32
      L_ixod=L_araf
C dio_log.fgi( 650, 197):������,DiPanel08_lamp31
      L_exod=L_araf
C dio_log.fgi( 650, 201):������,DiPanel08_lamp30
      L_axod=L_araf
C dio_log.fgi( 650, 205):������,DiPanel08_lamp29
      L_(65)=.false.
C dio_log.fgi( 258, 336):��������� ���������� (�������)
      L_(64)=.false.
C dio_log.fgi( 258, 340):��������� ���������� (�������)
      L0_asaf=R0_oraf.ne.R0_iraf
      R0_iraf=R0_oraf
C dio_log.fgi( 247, 338):���������� ������������� ������
      if(L_(64).or.L_(65)) then
         L0_esaf=(L_(64).or.L0_esaf).and..not.(L_(65))
      else
         if(L0_asaf.and..not.L0_uraf) L0_esaf=.not.L0_esaf
      endif
      L0_uraf=L0_asaf
      L_(66)=.not.L0_esaf
C dio_log.fgi( 264, 338):T �������
      if(L0_esaf) then
         I_eraf=1
      else
         I_eraf=0
      endif
C dio_log.fgi( 275, 340):��������� LO->1
      L_(68)=.false.
C dio_log.fgi( 258, 324):��������� ���������� (�������)
      L_(67)=.false.
C dio_log.fgi( 258, 328):��������� ���������� (�������)
      L0_etaf=R0_usaf.ne.R0_osaf
      R0_osaf=R0_usaf
C dio_log.fgi( 247, 326):���������� ������������� ������
      if(L_(67).or.L_(68)) then
         L0_itaf=(L_(67).or.L0_itaf).and..not.(L_(68))
      else
         if(L0_etaf.and..not.L0_ataf) L0_itaf=.not.L0_itaf
      endif
      L0_ataf=L0_etaf
      L_(69)=.not.L0_itaf
C dio_log.fgi( 264, 326):T �������
      if(L0_itaf) then
         I_isaf=1
      else
         I_isaf=0
      endif
C dio_log.fgi( 275, 328):��������� LO->1
      L_(71)=.false.
C dio_log.fgi( 258, 312):��������� ���������� (�������)
      L_(70)=.false.
C dio_log.fgi( 258, 316):��������� ���������� (�������)
      L0_ivaf=R0_avaf.ne.R0_utaf
      R0_utaf=R0_avaf
C dio_log.fgi( 247, 314):���������� ������������� ������
      if(L_(70).or.L_(71)) then
         L0_ovaf=(L_(70).or.L0_ovaf).and..not.(L_(71))
      else
         if(L0_ivaf.and..not.L0_evaf) L0_ovaf=.not.L0_ovaf
      endif
      L0_evaf=L0_ivaf
      L_(72)=.not.L0_ovaf
C dio_log.fgi( 264, 314):T �������
      if(L0_ovaf) then
         I_otaf=1
      else
         I_otaf=0
      endif
C dio_log.fgi( 275, 316):��������� LO->1
      L_(74)=.false.
C dio_log.fgi( 258, 300):��������� ���������� (�������)
      L_(73)=.false.
C dio_log.fgi( 258, 304):��������� ���������� (�������)
      L0_oxaf=R0_exaf.ne.R0_axaf
      R0_axaf=R0_exaf
C dio_log.fgi( 247, 302):���������� ������������� ������
      if(L_(73).or.L_(74)) then
         L0_uxaf=(L_(73).or.L0_uxaf).and..not.(L_(74))
      else
         if(L0_oxaf.and..not.L0_ixaf) L0_uxaf=.not.L0_uxaf
      endif
      L0_ixaf=L0_oxaf
      L_(75)=.not.L0_uxaf
C dio_log.fgi( 264, 302):T �������
      if(L0_uxaf) then
         I_uvaf=1
      else
         I_uvaf=0
      endif
C dio_log.fgi( 275, 304):��������� LO->1
      L_(77)=.false.
C dio_log.fgi( 258, 288):��������� ���������� (�������)
      L_(76)=.false.
C dio_log.fgi( 258, 292):��������� ���������� (�������)
      L0_ubef=R0_ibef.ne.R0_ebef
      R0_ebef=R0_ibef
C dio_log.fgi( 247, 290):���������� ������������� ������
      if(L_(76).or.L_(77)) then
         L0_adef=(L_(76).or.L0_adef).and..not.(L_(77))
      else
         if(L0_ubef.and..not.L0_obef) L0_adef=.not.L0_adef
      endif
      L0_obef=L0_ubef
      L_(78)=.not.L0_adef
C dio_log.fgi( 264, 290):T �������
      if(L0_adef) then
         I_abef=1
      else
         I_abef=0
      endif
C dio_log.fgi( 275, 292):��������� LO->1
      L_(80)=.false.
C dio_log.fgi( 258, 276):��������� ���������� (�������)
      L_(79)=.false.
C dio_log.fgi( 258, 280):��������� ���������� (�������)
      L0_afef=R0_odef.ne.R0_idef
      R0_idef=R0_odef
C dio_log.fgi( 247, 278):���������� ������������� ������
      if(L_(79).or.L_(80)) then
         L0_efef=(L_(79).or.L0_efef).and..not.(L_(80))
      else
         if(L0_afef.and..not.L0_udef) L0_efef=.not.L0_efef
      endif
      L0_udef=L0_afef
      L_(81)=.not.L0_efef
C dio_log.fgi( 264, 278):T �������
      if(L0_efef) then
         I_edef=1
      else
         I_edef=0
      endif
C dio_log.fgi( 275, 280):��������� LO->1
      L_(83)=.false.
C dio_log.fgi( 258, 264):��������� ���������� (�������)
      L_(82)=.false.
C dio_log.fgi( 258, 268):��������� ���������� (�������)
      L0_ekef=R0_ufef.ne.R0_ofef
      R0_ofef=R0_ufef
C dio_log.fgi( 247, 266):���������� ������������� ������
      if(L_(82).or.L_(83)) then
         L0_ikef=(L_(82).or.L0_ikef).and..not.(L_(83))
      else
         if(L0_ekef.and..not.L0_akef) L0_ikef=.not.L0_ikef
      endif
      L0_akef=L0_ekef
      L_(84)=.not.L0_ikef
C dio_log.fgi( 264, 266):T �������
      if(L0_ikef) then
         I_ifef=1
      else
         I_ifef=0
      endif
C dio_log.fgi( 275, 268):��������� LO->1
      L_(86)=.false.
C dio_log.fgi( 258, 252):��������� ���������� (�������)
      L_(85)=.false.
C dio_log.fgi( 258, 256):��������� ���������� (�������)
      L0_ilef=R0_alef.ne.R0_ukef
      R0_ukef=R0_alef
C dio_log.fgi( 247, 254):���������� ������������� ������
      if(L_(85).or.L_(86)) then
         L0_olef=(L_(85).or.L0_olef).and..not.(L_(86))
      else
         if(L0_ilef.and..not.L0_elef) L0_olef=.not.L0_olef
      endif
      L0_elef=L0_ilef
      L_(87)=.not.L0_olef
C dio_log.fgi( 264, 254):T �������
      if(L0_olef) then
         I_okef=1
      else
         I_okef=0
      endif
C dio_log.fgi( 275, 256):��������� LO->1
      L_(89)=.false.
C dio_log.fgi( 258, 240):��������� ���������� (�������)
      L_(88)=.false.
C dio_log.fgi( 258, 244):��������� ���������� (�������)
      L0_omef=R0_emef.ne.R0_amef
      R0_amef=R0_emef
C dio_log.fgi( 247, 242):���������� ������������� ������
      if(L_(88).or.L_(89)) then
         L0_umef=(L_(88).or.L0_umef).and..not.(L_(89))
      else
         if(L0_omef.and..not.L0_imef) L0_umef=.not.L0_umef
      endif
      L0_imef=L0_omef
      L_(90)=.not.L0_umef
C dio_log.fgi( 264, 242):T �������
      if(L0_umef) then
         I_ulef=1
      else
         I_ulef=0
      endif
C dio_log.fgi( 275, 244):��������� LO->1
      L_(92)=.false.
C dio_log.fgi( 258, 228):��������� ���������� (�������)
      L_(91)=.false.
C dio_log.fgi( 258, 232):��������� ���������� (�������)
      L0_upef=R0_ipef.ne.R0_epef
      R0_epef=R0_ipef
C dio_log.fgi( 247, 230):���������� ������������� ������
      if(L_(91).or.L_(92)) then
         L0_aref=(L_(91).or.L0_aref).and..not.(L_(92))
      else
         if(L0_upef.and..not.L0_opef) L0_aref=.not.L0_aref
      endif
      L0_opef=L0_upef
      L_(93)=.not.L0_aref
C dio_log.fgi( 264, 230):T �������
      if(L0_aref) then
         I_apef=1
      else
         I_apef=0
      endif
C dio_log.fgi( 275, 232):��������� LO->1
      L_(95)=.false.
C dio_log.fgi( 258, 216):��������� ���������� (�������)
      L_(94)=.false.
C dio_log.fgi( 258, 220):��������� ���������� (�������)
      L0_asef=R0_oref.ne.R0_iref
      R0_iref=R0_oref
C dio_log.fgi( 247, 218):���������� ������������� ������
      if(L_(94).or.L_(95)) then
         L0_esef=(L_(94).or.L0_esef).and..not.(L_(95))
      else
         if(L0_asef.and..not.L0_uref) L0_esef=.not.L0_esef
      endif
      L0_uref=L0_asef
      L_(96)=.not.L0_esef
C dio_log.fgi( 264, 218):T �������
      if(L0_esef) then
         I_eref=1
      else
         I_eref=0
      endif
C dio_log.fgi( 275, 220):��������� LO->1
      L_(98)=.false.
C dio_log.fgi( 258, 204):��������� ���������� (�������)
      L_(97)=.false.
C dio_log.fgi( 258, 208):��������� ���������� (�������)
      L0_etef=R0_usef.ne.R0_osef
      R0_osef=R0_usef
C dio_log.fgi( 247, 206):���������� ������������� ������
      if(L_(97).or.L_(98)) then
         L0_itef=(L_(97).or.L0_itef).and..not.(L_(98))
      else
         if(L0_etef.and..not.L0_atef) L0_itef=.not.L0_itef
      endif
      L0_atef=L0_etef
      L_(99)=.not.L0_itef
C dio_log.fgi( 264, 206):T �������
      if(L0_itef) then
         I_isef=1
      else
         I_isef=0
      endif
C dio_log.fgi( 275, 208):��������� LO->1
      L_(101)=.false.
C dio_log.fgi( 258, 192):��������� ���������� (�������)
      L_(100)=.false.
C dio_log.fgi( 258, 196):��������� ���������� (�������)
      L0_ivef=R0_avef.ne.R0_utef
      R0_utef=R0_avef
C dio_log.fgi( 247, 194):���������� ������������� ������
      if(L_(100).or.L_(101)) then
         L0_ovef=(L_(100).or.L0_ovef).and..not.(L_(101))
      else
         if(L0_ivef.and..not.L0_evef) L0_ovef=.not.L0_ovef
      endif
      L0_evef=L0_ivef
      L_(102)=.not.L0_ovef
C dio_log.fgi( 264, 194):T �������
      if(L0_ovef) then
         I_otef=1
      else
         I_otef=0
      endif
C dio_log.fgi( 275, 196):��������� LO->1
      L_(104)=.false.
C dio_log.fgi( 258, 180):��������� ���������� (�������)
      L_(103)=.false.
C dio_log.fgi( 258, 184):��������� ���������� (�������)
      L0_oxef=R0_exef.ne.R0_axef
      R0_axef=R0_exef
C dio_log.fgi( 247, 182):���������� ������������� ������
      if(L_(103).or.L_(104)) then
         L0_uxef=(L_(103).or.L0_uxef).and..not.(L_(104))
      else
         if(L0_oxef.and..not.L0_ixef) L0_uxef=.not.L0_uxef
      endif
      L0_ixef=L0_oxef
      L_(105)=.not.L0_uxef
C dio_log.fgi( 264, 182):T �������
      if(L0_uxef) then
         I_uvef=1
      else
         I_uvef=0
      endif
C dio_log.fgi( 275, 184):��������� LO->1
      L_(107)=.false.
C dio_log.fgi( 258, 168):��������� ���������� (�������)
      L_(106)=.false.
C dio_log.fgi( 258, 172):��������� ���������� (�������)
      L0_ubif=R0_ibif.ne.R0_ebif
      R0_ebif=R0_ibif
C dio_log.fgi( 247, 170):���������� ������������� ������
      if(L_(106).or.L_(107)) then
         L0_adif=(L_(106).or.L0_adif).and..not.(L_(107))
      else
         if(L0_ubif.and..not.L0_obif) L0_adif=.not.L0_adif
      endif
      L0_obif=L0_ubif
      L_(108)=.not.L0_adif
C dio_log.fgi( 264, 170):T �������
      if(L0_adif) then
         I_abif=1
      else
         I_abif=0
      endif
C dio_log.fgi( 275, 172):��������� LO->1
      L_(110)=.false.
C dio_log.fgi( 258, 156):��������� ���������� (�������)
      L_(109)=.false.
C dio_log.fgi( 258, 160):��������� ���������� (�������)
      L0_afif=R0_odif.ne.R0_idif
      R0_idif=R0_odif
C dio_log.fgi( 247, 158):���������� ������������� ������
      if(L_(109).or.L_(110)) then
         L0_efif=(L_(109).or.L0_efif).and..not.(L_(110))
      else
         if(L0_afif.and..not.L0_udif) L0_efif=.not.L0_efif
      endif
      L0_udif=L0_afif
      L_(111)=.not.L0_efif
C dio_log.fgi( 264, 158):T �������
      if(L0_efif) then
         I_edif=1
      else
         I_edif=0
      endif
C dio_log.fgi( 275, 160):��������� LO->1
      L_(113)=.false.
C dio_log.fgi( 258, 144):��������� ���������� (�������)
      L_(112)=.false.
C dio_log.fgi( 258, 148):��������� ���������� (�������)
      L0_ekif=R0_ufif.ne.R0_ofif
      R0_ofif=R0_ufif
C dio_log.fgi( 247, 146):���������� ������������� ������
      if(L_(112).or.L_(113)) then
         L0_ikif=(L_(112).or.L0_ikif).and..not.(L_(113))
      else
         if(L0_ekif.and..not.L0_akif) L0_ikif=.not.L0_ikif
      endif
      L0_akif=L0_ekif
      L_(114)=.not.L0_ikif
C dio_log.fgi( 264, 146):T �������
      if(L0_ikif) then
         I_ifif=1
      else
         I_ifif=0
      endif
C dio_log.fgi( 275, 148):��������� LO->1
      L_(116)=.false.
C dio_log.fgi( 258, 132):��������� ���������� (�������)
      L_(115)=.false.
C dio_log.fgi( 258, 136):��������� ���������� (�������)
      L0_ilif=R0_alif.ne.R0_ukif
      R0_ukif=R0_alif
C dio_log.fgi( 247, 134):���������� ������������� ������
      if(L_(115).or.L_(116)) then
         L0_olif=(L_(115).or.L0_olif).and..not.(L_(116))
      else
         if(L0_ilif.and..not.L0_elif) L0_olif=.not.L0_olif
      endif
      L0_elif=L0_ilif
      L_(117)=.not.L0_olif
C dio_log.fgi( 264, 134):T �������
      if(L0_olif) then
         I_okif=1
      else
         I_okif=0
      endif
C dio_log.fgi( 275, 136):��������� LO->1
      L_(119)=.false.
C dio_log.fgi( 258, 120):��������� ���������� (�������)
      L_(118)=.false.
C dio_log.fgi( 258, 124):��������� ���������� (�������)
      L0_omif=R0_emif.ne.R0_amif
      R0_amif=R0_emif
C dio_log.fgi( 247, 122):���������� ������������� ������
      if(L_(118).or.L_(119)) then
         L0_umif=(L_(118).or.L0_umif).and..not.(L_(119))
      else
         if(L0_omif.and..not.L0_imif) L0_umif=.not.L0_umif
      endif
      L0_imif=L0_omif
      L_(120)=.not.L0_umif
C dio_log.fgi( 264, 122):T �������
      if(L0_umif) then
         I_ulif=1
      else
         I_ulif=0
      endif
C dio_log.fgi( 275, 124):��������� LO->1
      L_(122)=.false.
C dio_log.fgi( 258, 108):��������� ���������� (�������)
      L_(121)=.false.
C dio_log.fgi( 258, 112):��������� ���������� (�������)
      L0_upif=R0_ipif.ne.R0_epif
      R0_epif=R0_ipif
C dio_log.fgi( 247, 110):���������� ������������� ������
      if(L_(121).or.L_(122)) then
         L0_arif=(L_(121).or.L0_arif).and..not.(L_(122))
      else
         if(L0_upif.and..not.L0_opif) L0_arif=.not.L0_arif
      endif
      L0_opif=L0_upif
      L_(123)=.not.L0_arif
C dio_log.fgi( 264, 110):T �������
      if(L0_arif) then
         I_apif=1
      else
         I_apif=0
      endif
C dio_log.fgi( 275, 112):��������� LO->1
      L_(125)=.false.
C dio_log.fgi( 258,  96):��������� ���������� (�������)
      L_(124)=.false.
C dio_log.fgi( 258, 100):��������� ���������� (�������)
      L0_asif=R0_orif.ne.R0_irif
      R0_irif=R0_orif
C dio_log.fgi( 247,  98):���������� ������������� ������
      if(L_(124).or.L_(125)) then
         L0_esif=(L_(124).or.L0_esif).and..not.(L_(125))
      else
         if(L0_asif.and..not.L0_urif) L0_esif=.not.L0_esif
      endif
      L0_urif=L0_asif
      L_(126)=.not.L0_esif
C dio_log.fgi( 264,  98):T �������
      if(L0_esif) then
         I_erif=1
      else
         I_erif=0
      endif
C dio_log.fgi( 275, 100):��������� LO->1
      L_(128)=.false.
C dio_log.fgi( 258,  84):��������� ���������� (�������)
      L_(127)=.false.
C dio_log.fgi( 258,  88):��������� ���������� (�������)
      L0_etif=R0_usif.ne.R0_osif
      R0_osif=R0_usif
C dio_log.fgi( 247,  86):���������� ������������� ������
      if(L_(127).or.L_(128)) then
         L0_itif=(L_(127).or.L0_itif).and..not.(L_(128))
      else
         if(L0_etif.and..not.L0_atif) L0_itif=.not.L0_itif
      endif
      L0_atif=L0_etif
      L_(129)=.not.L0_itif
C dio_log.fgi( 264,  86):T �������
      if(L0_itif) then
         I_isif=1
      else
         I_isif=0
      endif
C dio_log.fgi( 275,  88):��������� LO->1
      L_(131)=.false.
C dio_log.fgi( 258,  72):��������� ���������� (�������)
      L_(130)=.false.
C dio_log.fgi( 258,  76):��������� ���������� (�������)
      L0_ivif=R0_avif.ne.R0_utif
      R0_utif=R0_avif
C dio_log.fgi( 247,  74):���������� ������������� ������
      if(L_(130).or.L_(131)) then
         L0_ovif=(L_(130).or.L0_ovif).and..not.(L_(131))
      else
         if(L0_ivif.and..not.L0_evif) L0_ovif=.not.L0_ovif
      endif
      L0_evif=L0_ivif
      L_(132)=.not.L0_ovif
C dio_log.fgi( 264,  74):T �������
      if(L0_ovif) then
         I_otif=1
      else
         I_otif=0
      endif
C dio_log.fgi( 275,  76):��������� LO->1
      L_(134)=.false.
C dio_log.fgi( 258,  60):��������� ���������� (�������)
      L_(133)=.false.
C dio_log.fgi( 258,  64):��������� ���������� (�������)
      L0_oxif=R0_exif.ne.R0_axif
      R0_axif=R0_exif
C dio_log.fgi( 247,  62):���������� ������������� ������
      if(L_(133).or.L_(134)) then
         L0_uxif=(L_(133).or.L0_uxif).and..not.(L_(134))
      else
         if(L0_oxif.and..not.L0_ixif) L0_uxif=.not.L0_uxif
      endif
      L0_ixif=L0_oxif
      L_(135)=.not.L0_uxif
C dio_log.fgi( 264,  62):T �������
      if(L0_uxif) then
         I_uvif=1
      else
         I_uvif=0
      endif
C dio_log.fgi( 275,  64):��������� LO->1
      L_(137)=.false.
C dio_log.fgi( 258,  48):��������� ���������� (�������)
      L_(136)=.false.
C dio_log.fgi( 258,  52):��������� ���������� (�������)
      L0_ubof=R0_ibof.ne.R0_ebof
      R0_ebof=R0_ibof
C dio_log.fgi( 247,  50):���������� ������������� ������
      if(L_(136).or.L_(137)) then
         L0_adof=(L_(136).or.L0_adof).and..not.(L_(137))
      else
         if(L0_ubof.and..not.L0_obof) L0_adof=.not.L0_adof
      endif
      L0_obof=L0_ubof
      L_(138)=.not.L0_adof
C dio_log.fgi( 264,  50):T �������
      if(L0_adof) then
         I_abof=1
      else
         I_abof=0
      endif
C dio_log.fgi( 275,  52):��������� LO->1
      L_(140)=.false.
C dio_log.fgi( 258,  36):��������� ���������� (�������)
      L_(139)=.false.
C dio_log.fgi( 258,  40):��������� ���������� (�������)
      L0_afof=R0_odof.ne.R0_idof
      R0_idof=R0_odof
C dio_log.fgi( 247,  38):���������� ������������� ������
      if(L_(139).or.L_(140)) then
         L0_efof=(L_(139).or.L0_efof).and..not.(L_(140))
      else
         if(L0_afof.and..not.L0_udof) L0_efof=.not.L0_efof
      endif
      L0_udof=L0_afof
      L_(141)=.not.L0_efof
C dio_log.fgi( 264,  38):T �������
      if(L0_efof) then
         I_edof=1
      else
         I_edof=0
      endif
C dio_log.fgi( 275,  40):��������� LO->1
      L_(143)=.false.
C dio_log.fgi( 258,  24):��������� ���������� (�������)
      L_(142)=.false.
C dio_log.fgi( 258,  28):��������� ���������� (�������)
      L0_ekof=R0_ufof.ne.R0_ofof
      R0_ofof=R0_ufof
C dio_log.fgi( 247,  26):���������� ������������� ������
      if(L_(142).or.L_(143)) then
         L0_ikof=(L_(142).or.L0_ikof).and..not.(L_(143))
      else
         if(L0_ekof.and..not.L0_akof) L0_ikof=.not.L0_ikof
      endif
      L0_akof=L0_ekof
      L_(144)=.not.L0_ikof
C dio_log.fgi( 264,  26):T �������
      if(L0_ikof) then
         I_ifof=1
      else
         I_ifof=0
      endif
C dio_log.fgi( 275,  28):��������� LO->1
      L_(146)=.false.
C dio_log.fgi( 258,  12):��������� ���������� (�������)
      L_(145)=.false.
C dio_log.fgi( 258,  16):��������� ���������� (�������)
      L0_ilof=R0_alof.ne.R0_ukof
      R0_ukof=R0_alof
C dio_log.fgi( 247,  14):���������� ������������� ������
      if(L_(145).or.L_(146)) then
         L0_olof=(L_(145).or.L0_olof).and..not.(L_(146))
      else
         if(L0_ilof.and..not.L0_elof) L0_olof=.not.L0_olof
      endif
      L0_elof=L0_ilof
      L_(147)=.not.L0_olof
C dio_log.fgi( 264,  14):T �������
      if(L0_olof) then
         I_okof=1
      else
         I_okof=0
      endif
C dio_log.fgi( 275,  16):��������� LO->1
      L_(149)=.false.
C dio_log.fgi( 333, 218):��������� ���������� (�������)
      L_(148)=.false.
C dio_log.fgi( 333, 222):��������� ���������� (�������)
      L0_omof=R0_emof.ne.R0_amof
      R0_amof=R0_emof
C dio_log.fgi( 322, 220):���������� ������������� ������
      if(L_(148).or.L_(149)) then
         L0_umof=(L_(148).or.L0_umof).and..not.(L_(149))
      else
         if(L0_omof.and..not.L0_imof) L0_umof=.not.L0_umof
      endif
      L0_imof=L0_omof
      L_(150)=.not.L0_umof
C dio_log.fgi( 339, 220):T �������
      if(L0_umof) then
         I_ulof=1
      else
         I_ulof=0
      endif
C dio_log.fgi( 350, 222):��������� LO->1
      L_(152)=.false.
C dio_log.fgi( 333, 206):��������� ���������� (�������)
      L_(151)=.false.
C dio_log.fgi( 333, 210):��������� ���������� (�������)
      L0_upof=R0_ipof.ne.R0_epof
      R0_epof=R0_ipof
C dio_log.fgi( 322, 208):���������� ������������� ������
      if(L_(151).or.L_(152)) then
         L0_arof=(L_(151).or.L0_arof).and..not.(L_(152))
      else
         if(L0_upof.and..not.L0_opof) L0_arof=.not.L0_arof
      endif
      L0_opof=L0_upof
      L_(153)=.not.L0_arof
C dio_log.fgi( 339, 208):T �������
      if(L0_arof) then
         I_apof=1
      else
         I_apof=0
      endif
C dio_log.fgi( 350, 210):��������� LO->1
      L_(155)=.false.
C dio_log.fgi( 333, 194):��������� ���������� (�������)
      L_(154)=.false.
C dio_log.fgi( 333, 198):��������� ���������� (�������)
      L0_asof=R0_orof.ne.R0_irof
      R0_irof=R0_orof
C dio_log.fgi( 322, 196):���������� ������������� ������
      if(L_(154).or.L_(155)) then
         L0_esof=(L_(154).or.L0_esof).and..not.(L_(155))
      else
         if(L0_asof.and..not.L0_urof) L0_esof=.not.L0_esof
      endif
      L0_urof=L0_asof
      L_(156)=.not.L0_esof
C dio_log.fgi( 339, 196):T �������
      if(L0_esof) then
         I_erof=1
      else
         I_erof=0
      endif
C dio_log.fgi( 350, 198):��������� LO->1
      L_(158)=.false.
C dio_log.fgi( 333, 182):��������� ���������� (�������)
      L_(157)=.false.
C dio_log.fgi( 333, 186):��������� ���������� (�������)
      L0_etof=R0_usof.ne.R0_osof
      R0_osof=R0_usof
C dio_log.fgi( 322, 184):���������� ������������� ������
      if(L_(157).or.L_(158)) then
         L0_itof=(L_(157).or.L0_itof).and..not.(L_(158))
      else
         if(L0_etof.and..not.L0_atof) L0_itof=.not.L0_itof
      endif
      L0_atof=L0_etof
      L_(159)=.not.L0_itof
C dio_log.fgi( 339, 184):T �������
      if(L0_itof) then
         I_isof=1
      else
         I_isof=0
      endif
C dio_log.fgi( 350, 186):��������� LO->1
      L_(161)=.false.
C dio_log.fgi( 333, 170):��������� ���������� (�������)
      L_(160)=.false.
C dio_log.fgi( 333, 174):��������� ���������� (�������)
      L0_ivof=R0_avof.ne.R0_utof
      R0_utof=R0_avof
C dio_log.fgi( 322, 172):���������� ������������� ������
      if(L_(160).or.L_(161)) then
         L0_ovof=(L_(160).or.L0_ovof).and..not.(L_(161))
      else
         if(L0_ivof.and..not.L0_evof) L0_ovof=.not.L0_ovof
      endif
      L0_evof=L0_ivof
      L_(162)=.not.L0_ovof
C dio_log.fgi( 339, 172):T �������
      if(L0_ovof) then
         I_otof=1
      else
         I_otof=0
      endif
C dio_log.fgi( 350, 174):��������� LO->1
      L_(164)=.false.
C dio_log.fgi( 333, 158):��������� ���������� (�������)
      L_(163)=.false.
C dio_log.fgi( 333, 162):��������� ���������� (�������)
      L0_oxof=R0_exof.ne.R0_axof
      R0_axof=R0_exof
C dio_log.fgi( 322, 160):���������� ������������� ������
      if(L_(163).or.L_(164)) then
         L0_uxof=(L_(163).or.L0_uxof).and..not.(L_(164))
      else
         if(L0_oxof.and..not.L0_ixof) L0_uxof=.not.L0_uxof
      endif
      L0_ixof=L0_oxof
      L_(165)=.not.L0_uxof
C dio_log.fgi( 339, 160):T �������
      if(L0_uxof) then
         I_uvof=1
      else
         I_uvof=0
      endif
C dio_log.fgi( 350, 162):��������� LO->1
      L_(167)=.false.
C dio_log.fgi( 333, 146):��������� ���������� (�������)
      L_(166)=.false.
C dio_log.fgi( 333, 150):��������� ���������� (�������)
      L0_ubuf=R0_ibuf.ne.R0_ebuf
      R0_ebuf=R0_ibuf
C dio_log.fgi( 322, 148):���������� ������������� ������
      if(L_(166).or.L_(167)) then
         L0_aduf=(L_(166).or.L0_aduf).and..not.(L_(167))
      else
         if(L0_ubuf.and..not.L0_obuf) L0_aduf=.not.L0_aduf
      endif
      L0_obuf=L0_ubuf
      L_(168)=.not.L0_aduf
C dio_log.fgi( 339, 148):T �������
      if(L0_aduf) then
         I_abuf=1
      else
         I_abuf=0
      endif
C dio_log.fgi( 350, 150):��������� LO->1
      L_(170)=.false.
C dio_log.fgi( 333, 134):��������� ���������� (�������)
      L_(169)=.false.
C dio_log.fgi( 333, 138):��������� ���������� (�������)
      L0_afuf=R0_oduf.ne.R0_iduf
      R0_iduf=R0_oduf
C dio_log.fgi( 322, 136):���������� ������������� ������
      if(L_(169).or.L_(170)) then
         L0_efuf=(L_(169).or.L0_efuf).and..not.(L_(170))
      else
         if(L0_afuf.and..not.L0_uduf) L0_efuf=.not.L0_efuf
      endif
      L0_uduf=L0_afuf
      L_(171)=.not.L0_efuf
C dio_log.fgi( 339, 136):T �������
      if(L0_efuf) then
         I_eduf=1
      else
         I_eduf=0
      endif
C dio_log.fgi( 350, 138):��������� LO->1
      L_(173)=.false.
C dio_log.fgi( 333, 122):��������� ���������� (�������)
      L_(172)=.false.
C dio_log.fgi( 333, 126):��������� ���������� (�������)
      L0_ekuf=R0_ufuf.ne.R0_ofuf
      R0_ofuf=R0_ufuf
C dio_log.fgi( 322, 124):���������� ������������� ������
      if(L_(172).or.L_(173)) then
         L0_ikuf=(L_(172).or.L0_ikuf).and..not.(L_(173))
      else
         if(L0_ekuf.and..not.L0_akuf) L0_ikuf=.not.L0_ikuf
      endif
      L0_akuf=L0_ekuf
      L_(174)=.not.L0_ikuf
C dio_log.fgi( 339, 124):T �������
      if(L0_ikuf) then
         I_ifuf=1
      else
         I_ifuf=0
      endif
C dio_log.fgi( 350, 126):��������� LO->1
      L_(176)=.false.
C dio_log.fgi( 333, 110):��������� ���������� (�������)
      L_(175)=.false.
C dio_log.fgi( 333, 114):��������� ���������� (�������)
      L0_iluf=R0_aluf.ne.R0_ukuf
      R0_ukuf=R0_aluf
C dio_log.fgi( 322, 112):���������� ������������� ������
      if(L_(175).or.L_(176)) then
         L0_oluf=(L_(175).or.L0_oluf).and..not.(L_(176))
      else
         if(L0_iluf.and..not.L0_eluf) L0_oluf=.not.L0_oluf
      endif
      L0_eluf=L0_iluf
      L_(177)=.not.L0_oluf
C dio_log.fgi( 339, 112):T �������
      if(L0_oluf) then
         I_okuf=1
      else
         I_okuf=0
      endif
C dio_log.fgi( 350, 114):��������� LO->1
      L_(179)=.false.
C dio_log.fgi( 333,  98):��������� ���������� (�������)
      L_(178)=.false.
C dio_log.fgi( 333, 102):��������� ���������� (�������)
      L0_omuf=R0_emuf.ne.R0_amuf
      R0_amuf=R0_emuf
C dio_log.fgi( 322, 100):���������� ������������� ������
      if(L_(178).or.L_(179)) then
         L0_umuf=(L_(178).or.L0_umuf).and..not.(L_(179))
      else
         if(L0_omuf.and..not.L0_imuf) L0_umuf=.not.L0_umuf
      endif
      L0_imuf=L0_omuf
      L_(180)=.not.L0_umuf
C dio_log.fgi( 339, 100):T �������
      if(L0_umuf) then
         I_uluf=1
      else
         I_uluf=0
      endif
C dio_log.fgi( 350, 102):��������� LO->1
      L_(182)=.false.
C dio_log.fgi( 333,  86):��������� ���������� (�������)
      L_(181)=.false.
C dio_log.fgi( 333,  90):��������� ���������� (�������)
      L0_upuf=R0_ipuf.ne.R0_epuf
      R0_epuf=R0_ipuf
C dio_log.fgi( 322,  88):���������� ������������� ������
      if(L_(181).or.L_(182)) then
         L0_aruf=(L_(181).or.L0_aruf).and..not.(L_(182))
      else
         if(L0_upuf.and..not.L0_opuf) L0_aruf=.not.L0_aruf
      endif
      L0_opuf=L0_upuf
      L_(183)=.not.L0_aruf
C dio_log.fgi( 339,  88):T �������
      if(L0_aruf) then
         I_apuf=1
      else
         I_apuf=0
      endif
C dio_log.fgi( 350,  90):��������� LO->1
      L_(185)=.false.
C dio_log.fgi( 333,  74):��������� ���������� (�������)
      L_(184)=.false.
C dio_log.fgi( 333,  78):��������� ���������� (�������)
      L0_asuf=R0_oruf.ne.R0_iruf
      R0_iruf=R0_oruf
C dio_log.fgi( 322,  76):���������� ������������� ������
      if(L_(184).or.L_(185)) then
         L0_esuf=(L_(184).or.L0_esuf).and..not.(L_(185))
      else
         if(L0_asuf.and..not.L0_uruf) L0_esuf=.not.L0_esuf
      endif
      L0_uruf=L0_asuf
      L_(186)=.not.L0_esuf
C dio_log.fgi( 339,  76):T �������
      if(L0_esuf) then
         I_eruf=1
      else
         I_eruf=0
      endif
C dio_log.fgi( 350,  78):��������� LO->1
      L_(188)=.false.
C dio_log.fgi( 333,  62):��������� ���������� (�������)
      L_(187)=.false.
C dio_log.fgi( 333,  66):��������� ���������� (�������)
      L0_etuf=R0_usuf.ne.R0_osuf
      R0_osuf=R0_usuf
C dio_log.fgi( 322,  64):���������� ������������� ������
      if(L_(187).or.L_(188)) then
         L0_ituf=(L_(187).or.L0_ituf).and..not.(L_(188))
      else
         if(L0_etuf.and..not.L0_atuf) L0_ituf=.not.L0_ituf
      endif
      L0_atuf=L0_etuf
      L_(189)=.not.L0_ituf
C dio_log.fgi( 339,  64):T �������
      if(L0_ituf) then
         I_isuf=1
      else
         I_isuf=0
      endif
C dio_log.fgi( 350,  66):��������� LO->1
      L_(191)=.false.
C dio_log.fgi( 333,  50):��������� ���������� (�������)
      L_(190)=.false.
C dio_log.fgi( 333,  54):��������� ���������� (�������)
      L0_ivuf=R0_avuf.ne.R0_utuf
      R0_utuf=R0_avuf
C dio_log.fgi( 322,  52):���������� ������������� ������
      if(L_(190).or.L_(191)) then
         L0_ovuf=(L_(190).or.L0_ovuf).and..not.(L_(191))
      else
         if(L0_ivuf.and..not.L0_evuf) L0_ovuf=.not.L0_ovuf
      endif
      L0_evuf=L0_ivuf
      L_(192)=.not.L0_ovuf
C dio_log.fgi( 339,  52):T �������
      if(L0_ovuf) then
         I_otuf=1
      else
         I_otuf=0
      endif
C dio_log.fgi( 350,  54):��������� LO->1
      L_(194)=.false.
C dio_log.fgi( 333,  38):��������� ���������� (�������)
      L_(193)=.false.
C dio_log.fgi( 333,  42):��������� ���������� (�������)
      L0_oxuf=R0_exuf.ne.R0_axuf
      R0_axuf=R0_exuf
C dio_log.fgi( 322,  40):���������� ������������� ������
      if(L_(193).or.L_(194)) then
         L0_uxuf=(L_(193).or.L0_uxuf).and..not.(L_(194))
      else
         if(L0_oxuf.and..not.L0_ixuf) L0_uxuf=.not.L0_uxuf
      endif
      L0_ixuf=L0_oxuf
      L_(195)=.not.L0_uxuf
C dio_log.fgi( 339,  40):T �������
      if(L0_uxuf) then
         I_uvuf=1
      else
         I_uvuf=0
      endif
C dio_log.fgi( 350,  42):��������� LO->1
      L_(197)=.false.
C dio_log.fgi( 333,  26):��������� ���������� (�������)
      L_(196)=.false.
C dio_log.fgi( 333,  30):��������� ���������� (�������)
      L0_ubak=R0_ibak.ne.R0_ebak
      R0_ebak=R0_ibak
C dio_log.fgi( 322,  28):���������� ������������� ������
      if(L_(196).or.L_(197)) then
         L0_adak=(L_(196).or.L0_adak).and..not.(L_(197))
      else
         if(L0_ubak.and..not.L0_obak) L0_adak=.not.L0_adak
      endif
      L0_obak=L0_ubak
      L_(198)=.not.L0_adak
C dio_log.fgi( 339,  28):T �������
      if(L0_adak) then
         I_abak=1
      else
         I_abak=0
      endif
C dio_log.fgi( 350,  30):��������� LO->1
      L_(200)=.false.
C dio_log.fgi( 333,  14):��������� ���������� (�������)
      L_(199)=.false.
C dio_log.fgi( 333,  18):��������� ���������� (�������)
      L0_afak=R0_odak.ne.R0_idak
      R0_idak=R0_odak
C dio_log.fgi( 322,  16):���������� ������������� ������
      if(L_(199).or.L_(200)) then
         L0_efak=(L_(199).or.L0_efak).and..not.(L_(200))
      else
         if(L0_afak.and..not.L0_udak) L0_efak=.not.L0_efak
      endif
      L0_udak=L0_afak
      L_(201)=.not.L0_efak
C dio_log.fgi( 339,  16):T �������
      if(L0_efak) then
         I_edak=1
      else
         I_edak=0
      endif
C dio_log.fgi( 350,  18):��������� LO->1
      L_(203)=.false.
C dio_log.fgi( 180, 167):��������� ���������� (�������)
      L_(202)=.false.
C dio_log.fgi( 180, 171):��������� ���������� (�������)
      L0_ekak=R0_ufak.ne.R0_ofak
      R0_ofak=R0_ufak
C dio_log.fgi( 169, 169):���������� ������������� ������
      if(L_(202).or.L_(203)) then
         L0_ikak=(L_(202).or.L0_ikak).and..not.(L_(203))
      else
         if(L0_ekak.and..not.L0_akak) L0_ikak=.not.L0_ikak
      endif
      L0_akak=L0_ekak
      L_(204)=.not.L0_ikak
C dio_log.fgi( 186, 169):T �������
      if(L0_ikak) then
         I_ifak=1
      else
         I_ifak=0
      endif
C dio_log.fgi( 197, 171):��������� LO->1
      L_(206)=.false.
C dio_log.fgi( 180, 155):��������� ���������� (�������)
      L_(205)=.false.
C dio_log.fgi( 180, 159):��������� ���������� (�������)
      L0_ilak=R0_alak.ne.R0_ukak
      R0_ukak=R0_alak
C dio_log.fgi( 169, 157):���������� ������������� ������
      if(L_(205).or.L_(206)) then
         L0_olak=(L_(205).or.L0_olak).and..not.(L_(206))
      else
         if(L0_ilak.and..not.L0_elak) L0_olak=.not.L0_olak
      endif
      L0_elak=L0_ilak
      L_(207)=.not.L0_olak
C dio_log.fgi( 186, 157):T �������
      if(L0_olak) then
         I_okak=1
      else
         I_okak=0
      endif
C dio_log.fgi( 197, 159):��������� LO->1
      L_(209)=.false.
C dio_log.fgi( 180, 143):��������� ���������� (�������)
      L_(208)=.false.
C dio_log.fgi( 180, 147):��������� ���������� (�������)
      L0_omak=R0_emak.ne.R0_amak
      R0_amak=R0_emak
C dio_log.fgi( 169, 145):���������� ������������� ������
      if(L_(208).or.L_(209)) then
         L0_umak=(L_(208).or.L0_umak).and..not.(L_(209))
      else
         if(L0_omak.and..not.L0_imak) L0_umak=.not.L0_umak
      endif
      L0_imak=L0_omak
      L_(210)=.not.L0_umak
C dio_log.fgi( 186, 145):T �������
      if(L0_umak) then
         I_ulak=1
      else
         I_ulak=0
      endif
C dio_log.fgi( 197, 147):��������� LO->1
      L_(212)=.false.
C dio_log.fgi( 180, 131):��������� ���������� (�������)
      L_(211)=.false.
C dio_log.fgi( 180, 135):��������� ���������� (�������)
      L0_upak=R0_ipak.ne.R0_epak
      R0_epak=R0_ipak
C dio_log.fgi( 169, 133):���������� ������������� ������
      if(L_(211).or.L_(212)) then
         L0_arak=(L_(211).or.L0_arak).and..not.(L_(212))
      else
         if(L0_upak.and..not.L0_opak) L0_arak=.not.L0_arak
      endif
      L0_opak=L0_upak
      L_(213)=.not.L0_arak
C dio_log.fgi( 186, 133):T �������
      if(L0_arak) then
         I_apak=1
      else
         I_apak=0
      endif
C dio_log.fgi( 197, 135):��������� LO->1
      L_(215)=.false.
C dio_log.fgi( 180, 119):��������� ���������� (�������)
      L_(214)=.false.
C dio_log.fgi( 180, 123):��������� ���������� (�������)
      L0_asak=R0_orak.ne.R0_irak
      R0_irak=R0_orak
C dio_log.fgi( 169, 121):���������� ������������� ������
      if(L_(214).or.L_(215)) then
         L0_esak=(L_(214).or.L0_esak).and..not.(L_(215))
      else
         if(L0_asak.and..not.L0_urak) L0_esak=.not.L0_esak
      endif
      L0_urak=L0_asak
      L_(216)=.not.L0_esak
C dio_log.fgi( 186, 121):T �������
      if(L0_esak) then
         I_erak=1
      else
         I_erak=0
      endif
C dio_log.fgi( 197, 123):��������� LO->1
      L_(218)=.false.
C dio_log.fgi( 180, 107):��������� ���������� (�������)
      L_(217)=.false.
C dio_log.fgi( 180, 111):��������� ���������� (�������)
      L0_etak=R0_usak.ne.R0_osak
      R0_osak=R0_usak
C dio_log.fgi( 169, 109):���������� ������������� ������
      if(L_(217).or.L_(218)) then
         L0_itak=(L_(217).or.L0_itak).and..not.(L_(218))
      else
         if(L0_etak.and..not.L0_atak) L0_itak=.not.L0_itak
      endif
      L0_atak=L0_etak
      L_(219)=.not.L0_itak
C dio_log.fgi( 186, 109):T �������
      if(L0_itak) then
         I_isak=1
      else
         I_isak=0
      endif
C dio_log.fgi( 197, 111):��������� LO->1
      L_(221)=.false.
C dio_log.fgi( 180,  95):��������� ���������� (�������)
      L_(220)=.false.
C dio_log.fgi( 180,  99):��������� ���������� (�������)
      L0_ivak=R0_avak.ne.R0_utak
      R0_utak=R0_avak
C dio_log.fgi( 169,  97):���������� ������������� ������
      if(L_(220).or.L_(221)) then
         L0_ovak=(L_(220).or.L0_ovak).and..not.(L_(221))
      else
         if(L0_ivak.and..not.L0_evak) L0_ovak=.not.L0_ovak
      endif
      L0_evak=L0_ivak
      L_(222)=.not.L0_ovak
C dio_log.fgi( 186,  97):T �������
      if(L0_ovak) then
         I_otak=1
      else
         I_otak=0
      endif
C dio_log.fgi( 197,  99):��������� LO->1
      L_(224)=.false.
C dio_log.fgi( 180,  83):��������� ���������� (�������)
      L_(223)=.false.
C dio_log.fgi( 180,  87):��������� ���������� (�������)
      L0_oxak=R0_exak.ne.R0_axak
      R0_axak=R0_exak
C dio_log.fgi( 169,  85):���������� ������������� ������
      if(L_(223).or.L_(224)) then
         L0_uxak=(L_(223).or.L0_uxak).and..not.(L_(224))
      else
         if(L0_oxak.and..not.L0_ixak) L0_uxak=.not.L0_uxak
      endif
      L0_ixak=L0_oxak
      L_(225)=.not.L0_uxak
C dio_log.fgi( 186,  85):T �������
      if(L0_uxak) then
         I_uvak=1
      else
         I_uvak=0
      endif
C dio_log.fgi( 197,  87):��������� LO->1
      L_(227)=.false.
C dio_log.fgi( 180,  71):��������� ���������� (�������)
      L_(226)=.false.
C dio_log.fgi( 180,  75):��������� ���������� (�������)
      L0_ubek=R0_ibek.ne.R0_ebek
      R0_ebek=R0_ibek
C dio_log.fgi( 169,  73):���������� ������������� ������
      if(L_(226).or.L_(227)) then
         L0_adek=(L_(226).or.L0_adek).and..not.(L_(227))
      else
         if(L0_ubek.and..not.L0_obek) L0_adek=.not.L0_adek
      endif
      L0_obek=L0_ubek
      L_(228)=.not.L0_adek
C dio_log.fgi( 186,  73):T �������
      if(L0_adek) then
         I_abek=1
      else
         I_abek=0
      endif
C dio_log.fgi( 197,  75):��������� LO->1
      L_(230)=.false.
C dio_log.fgi( 180,  59):��������� ���������� (�������)
      L_(229)=.false.
C dio_log.fgi( 180,  63):��������� ���������� (�������)
      L0_afek=R0_odek.ne.R0_idek
      R0_idek=R0_odek
C dio_log.fgi( 169,  61):���������� ������������� ������
      if(L_(229).or.L_(230)) then
         L0_efek=(L_(229).or.L0_efek).and..not.(L_(230))
      else
         if(L0_afek.and..not.L0_udek) L0_efek=.not.L0_efek
      endif
      L0_udek=L0_afek
      L_(231)=.not.L0_efek
C dio_log.fgi( 186,  61):T �������
      if(L0_efek) then
         I_edek=1
      else
         I_edek=0
      endif
C dio_log.fgi( 197,  63):��������� LO->1
      L_(233)=.false.
C dio_log.fgi( 180,  47):��������� ���������� (�������)
      L_(232)=.false.
C dio_log.fgi( 180,  51):��������� ���������� (�������)
      L0_ekek=R0_ufek.ne.R0_ofek
      R0_ofek=R0_ufek
C dio_log.fgi( 169,  49):���������� ������������� ������
      if(L_(232).or.L_(233)) then
         L0_ikek=(L_(232).or.L0_ikek).and..not.(L_(233))
      else
         if(L0_ekek.and..not.L0_akek) L0_ikek=.not.L0_ikek
      endif
      L0_akek=L0_ekek
      L_(234)=.not.L0_ikek
C dio_log.fgi( 186,  49):T �������
      if(L0_ikek) then
         I_ifek=1
      else
         I_ifek=0
      endif
C dio_log.fgi( 197,  51):��������� LO->1
      L_(236)=.false.
C dio_log.fgi( 180,  35):��������� ���������� (�������)
      L_(235)=.false.
C dio_log.fgi( 180,  39):��������� ���������� (�������)
      L0_ilek=R0_alek.ne.R0_ukek
      R0_ukek=R0_alek
C dio_log.fgi( 169,  37):���������� ������������� ������
      if(L_(235).or.L_(236)) then
         L0_olek=(L_(235).or.L0_olek).and..not.(L_(236))
      else
         if(L0_ilek.and..not.L0_elek) L0_olek=.not.L0_olek
      endif
      L0_elek=L0_ilek
      L_(237)=.not.L0_olek
C dio_log.fgi( 186,  37):T �������
      if(L0_olek) then
         I_okek=1
      else
         I_okek=0
      endif
C dio_log.fgi( 197,  39):��������� LO->1
      L_(239)=.false.
C dio_log.fgi( 180,  23):��������� ���������� (�������)
      L_(238)=.false.
C dio_log.fgi( 180,  27):��������� ���������� (�������)
      L0_omek=R0_emek.ne.R0_amek
      R0_amek=R0_emek
C dio_log.fgi( 169,  25):���������� ������������� ������
      if(L_(238).or.L_(239)) then
         L0_umek=(L_(238).or.L0_umek).and..not.(L_(239))
      else
         if(L0_omek.and..not.L0_imek) L0_umek=.not.L0_umek
      endif
      L0_imek=L0_omek
      L_(240)=.not.L0_umek
C dio_log.fgi( 186,  25):T �������
      if(L0_umek) then
         I_ulek=1
      else
         I_ulek=0
      endif
C dio_log.fgi( 197,  27):��������� LO->1
      L_(242)=.false.
C dio_log.fgi( 180,  11):��������� ���������� (�������)
      L_(241)=.false.
C dio_log.fgi( 180,  15):��������� ���������� (�������)
      L0_upek=R0_ipek.ne.R0_epek
      R0_epek=R0_ipek
C dio_log.fgi( 169,  13):���������� ������������� ������
      if(L_(241).or.L_(242)) then
         L0_arek=(L_(241).or.L0_arek).and..not.(L_(242))
      else
         if(L0_upek.and..not.L0_opek) L0_arek=.not.L0_arek
      endif
      L0_opek=L0_upek
      L_(243)=.not.L0_arek
C dio_log.fgi( 186,  13):T �������
      if(L0_arek) then
         I_apek=1
      else
         I_apek=0
      endif
C dio_log.fgi( 197,  15):��������� LO->1
      L_(245)=.false.
C dio_log.fgi( 102, 503):��������� ���������� (�������)
      L_(244)=.false.
C dio_log.fgi( 102, 507):��������� ���������� (�������)
      L0_asek=R0_orek.ne.R0_irek
      R0_irek=R0_orek
C dio_log.fgi(  91, 505):���������� ������������� ������
      if(L_(244).or.L_(245)) then
         L0_esek=(L_(244).or.L0_esek).and..not.(L_(245))
      else
         if(L0_asek.and..not.L0_urek) L0_esek=.not.L0_esek
      endif
      L0_urek=L0_asek
      L_(246)=.not.L0_esek
C dio_log.fgi( 108, 505):T �������
      if(L0_esek) then
         I_erek=1
      else
         I_erek=0
      endif
C dio_log.fgi( 119, 507):��������� LO->1
      L_(248)=.false.
C dio_log.fgi( 102, 491):��������� ���������� (�������)
      L_(247)=.false.
C dio_log.fgi( 102, 495):��������� ���������� (�������)
      L0_etek=R0_usek.ne.R0_osek
      R0_osek=R0_usek
C dio_log.fgi(  91, 493):���������� ������������� ������
      if(L_(247).or.L_(248)) then
         L0_itek=(L_(247).or.L0_itek).and..not.(L_(248))
      else
         if(L0_etek.and..not.L0_atek) L0_itek=.not.L0_itek
      endif
      L0_atek=L0_etek
      L_(249)=.not.L0_itek
C dio_log.fgi( 108, 493):T �������
      if(L0_itek) then
         I_isek=1
      else
         I_isek=0
      endif
C dio_log.fgi( 119, 495):��������� LO->1
      L_(251)=.false.
C dio_log.fgi( 102, 479):��������� ���������� (�������)
      L_(250)=.false.
C dio_log.fgi( 102, 483):��������� ���������� (�������)
      L0_ivek=R0_avek.ne.R0_utek
      R0_utek=R0_avek
C dio_log.fgi(  91, 481):���������� ������������� ������
      if(L_(250).or.L_(251)) then
         L0_ovek=(L_(250).or.L0_ovek).and..not.(L_(251))
      else
         if(L0_ivek.and..not.L0_evek) L0_ovek=.not.L0_ovek
      endif
      L0_evek=L0_ivek
      L_(252)=.not.L0_ovek
C dio_log.fgi( 108, 481):T �������
      if(L0_ovek) then
         I_otek=1
      else
         I_otek=0
      endif
C dio_log.fgi( 119, 483):��������� LO->1
      L_(254)=.false.
C dio_log.fgi( 102, 467):��������� ���������� (�������)
      L_(253)=.false.
C dio_log.fgi( 102, 471):��������� ���������� (�������)
      L0_oxek=R0_exek.ne.R0_axek
      R0_axek=R0_exek
C dio_log.fgi(  91, 469):���������� ������������� ������
      if(L_(253).or.L_(254)) then
         L0_uxek=(L_(253).or.L0_uxek).and..not.(L_(254))
      else
         if(L0_oxek.and..not.L0_ixek) L0_uxek=.not.L0_uxek
      endif
      L0_ixek=L0_oxek
      L_(255)=.not.L0_uxek
C dio_log.fgi( 108, 469):T �������
      if(L0_uxek) then
         I_uvek=1
      else
         I_uvek=0
      endif
C dio_log.fgi( 119, 471):��������� LO->1
      L_(257)=.false.
C dio_log.fgi( 102, 455):��������� ���������� (�������)
      L_(256)=.false.
C dio_log.fgi( 102, 459):��������� ���������� (�������)
      L0_ubik=R0_ibik.ne.R0_ebik
      R0_ebik=R0_ibik
C dio_log.fgi(  91, 457):���������� ������������� ������
      if(L_(256).or.L_(257)) then
         L0_adik=(L_(256).or.L0_adik).and..not.(L_(257))
      else
         if(L0_ubik.and..not.L0_obik) L0_adik=.not.L0_adik
      endif
      L0_obik=L0_ubik
      L_(258)=.not.L0_adik
C dio_log.fgi( 108, 457):T �������
      if(L0_adik) then
         I_abik=1
      else
         I_abik=0
      endif
C dio_log.fgi( 119, 459):��������� LO->1
      L_(260)=.false.
C dio_log.fgi( 102, 443):��������� ���������� (�������)
      L_(259)=.false.
C dio_log.fgi( 102, 447):��������� ���������� (�������)
      L0_afik=R0_odik.ne.R0_idik
      R0_idik=R0_odik
C dio_log.fgi(  91, 445):���������� ������������� ������
      if(L_(259).or.L_(260)) then
         L0_efik=(L_(259).or.L0_efik).and..not.(L_(260))
      else
         if(L0_afik.and..not.L0_udik) L0_efik=.not.L0_efik
      endif
      L0_udik=L0_afik
      L_(261)=.not.L0_efik
C dio_log.fgi( 108, 445):T �������
      if(L0_efik) then
         I_edik=1
      else
         I_edik=0
      endif
C dio_log.fgi( 119, 447):��������� LO->1
      L_(263)=.false.
C dio_log.fgi( 102, 431):��������� ���������� (�������)
      L_(262)=.false.
C dio_log.fgi( 102, 435):��������� ���������� (�������)
      L0_ekik=R0_ufik.ne.R0_ofik
      R0_ofik=R0_ufik
C dio_log.fgi(  91, 433):���������� ������������� ������
      if(L_(262).or.L_(263)) then
         L0_ikik=(L_(262).or.L0_ikik).and..not.(L_(263))
      else
         if(L0_ekik.and..not.L0_akik) L0_ikik=.not.L0_ikik
      endif
      L0_akik=L0_ekik
      L_(264)=.not.L0_ikik
C dio_log.fgi( 108, 433):T �������
      if(L0_ikik) then
         I_ifik=1
      else
         I_ifik=0
      endif
C dio_log.fgi( 119, 435):��������� LO->1
      L_(266)=.false.
C dio_log.fgi( 102, 419):��������� ���������� (�������)
      L_(265)=.false.
C dio_log.fgi( 102, 423):��������� ���������� (�������)
      L0_ilik=R0_alik.ne.R0_ukik
      R0_ukik=R0_alik
C dio_log.fgi(  91, 421):���������� ������������� ������
      if(L_(265).or.L_(266)) then
         L0_olik=(L_(265).or.L0_olik).and..not.(L_(266))
      else
         if(L0_ilik.and..not.L0_elik) L0_olik=.not.L0_olik
      endif
      L0_elik=L0_ilik
      L_(267)=.not.L0_olik
C dio_log.fgi( 108, 421):T �������
      if(L0_olik) then
         I_okik=1
      else
         I_okik=0
      endif
C dio_log.fgi( 119, 423):��������� LO->1
      L_(269)=.false.
C dio_log.fgi( 102, 407):��������� ���������� (�������)
      L_(268)=.false.
C dio_log.fgi( 102, 411):��������� ���������� (�������)
      L0_omik=R0_emik.ne.R0_amik
      R0_amik=R0_emik
C dio_log.fgi(  91, 409):���������� ������������� ������
      if(L_(268).or.L_(269)) then
         L0_umik=(L_(268).or.L0_umik).and..not.(L_(269))
      else
         if(L0_omik.and..not.L0_imik) L0_umik=.not.L0_umik
      endif
      L0_imik=L0_omik
      L_(270)=.not.L0_umik
C dio_log.fgi( 108, 409):T �������
      if(L0_umik) then
         I_ulik=1
      else
         I_ulik=0
      endif
C dio_log.fgi( 119, 411):��������� LO->1
      L_(272)=.false.
C dio_log.fgi( 102, 395):��������� ���������� (�������)
      L_(271)=.false.
C dio_log.fgi( 102, 399):��������� ���������� (�������)
      L0_upik=R0_ipik.ne.R0_epik
      R0_epik=R0_ipik
C dio_log.fgi(  91, 397):���������� ������������� ������
      if(L_(271).or.L_(272)) then
         L0_arik=(L_(271).or.L0_arik).and..not.(L_(272))
      else
         if(L0_upik.and..not.L0_opik) L0_arik=.not.L0_arik
      endif
      L0_opik=L0_upik
      L_(273)=.not.L0_arik
C dio_log.fgi( 108, 397):T �������
      if(L0_arik) then
         I_apik=1
      else
         I_apik=0
      endif
C dio_log.fgi( 119, 399):��������� LO->1
      L_(275)=.false.
C dio_log.fgi( 102, 383):��������� ���������� (�������)
      L_(274)=.false.
C dio_log.fgi( 102, 387):��������� ���������� (�������)
      L0_asik=R0_orik.ne.R0_irik
      R0_irik=R0_orik
C dio_log.fgi(  91, 385):���������� ������������� ������
      if(L_(274).or.L_(275)) then
         L0_esik=(L_(274).or.L0_esik).and..not.(L_(275))
      else
         if(L0_asik.and..not.L0_urik) L0_esik=.not.L0_esik
      endif
      L0_urik=L0_asik
      L_(276)=.not.L0_esik
C dio_log.fgi( 108, 385):T �������
      if(L0_esik) then
         I_erik=1
      else
         I_erik=0
      endif
C dio_log.fgi( 119, 387):��������� LO->1
      L_(278)=.false.
C dio_log.fgi( 102, 371):��������� ���������� (�������)
      L_(277)=.false.
C dio_log.fgi( 102, 375):��������� ���������� (�������)
      L0_etik=R0_usik.ne.R0_osik
      R0_osik=R0_usik
C dio_log.fgi(  91, 373):���������� ������������� ������
      if(L_(277).or.L_(278)) then
         L0_itik=(L_(277).or.L0_itik).and..not.(L_(278))
      else
         if(L0_etik.and..not.L0_atik) L0_itik=.not.L0_itik
      endif
      L0_atik=L0_etik
      L_(279)=.not.L0_itik
C dio_log.fgi( 108, 373):T �������
      if(L0_itik) then
         I_isik=1
      else
         I_isik=0
      endif
C dio_log.fgi( 119, 375):��������� LO->1
      L_(281)=.false.
C dio_log.fgi( 102, 359):��������� ���������� (�������)
      L_(280)=.false.
C dio_log.fgi( 102, 363):��������� ���������� (�������)
      L0_ivik=R0_avik.ne.R0_utik
      R0_utik=R0_avik
C dio_log.fgi(  91, 361):���������� ������������� ������
      if(L_(280).or.L_(281)) then
         L0_ovik=(L_(280).or.L0_ovik).and..not.(L_(281))
      else
         if(L0_ivik.and..not.L0_evik) L0_ovik=.not.L0_ovik
      endif
      L0_evik=L0_ivik
      L_(282)=.not.L0_ovik
C dio_log.fgi( 108, 361):T �������
      if(L0_ovik) then
         I_otik=1
      else
         I_otik=0
      endif
C dio_log.fgi( 119, 363):��������� LO->1
      L_(284)=.false.
C dio_log.fgi( 102, 347):��������� ���������� (�������)
      L_(283)=.false.
C dio_log.fgi( 102, 351):��������� ���������� (�������)
      L0_oxik=R0_exik.ne.R0_axik
      R0_axik=R0_exik
C dio_log.fgi(  91, 349):���������� ������������� ������
      if(L_(283).or.L_(284)) then
         L0_uxik=(L_(283).or.L0_uxik).and..not.(L_(284))
      else
         if(L0_oxik.and..not.L0_ixik) L0_uxik=.not.L0_uxik
      endif
      L0_ixik=L0_oxik
      L_(285)=.not.L0_uxik
C dio_log.fgi( 108, 349):T �������
      if(L0_uxik) then
         I_uvik=1
      else
         I_uvik=0
      endif
C dio_log.fgi( 119, 351):��������� LO->1
      L_(287)=.false.
C dio_log.fgi( 102, 335):��������� ���������� (�������)
      L_(286)=.false.
C dio_log.fgi( 102, 339):��������� ���������� (�������)
      L0_ubok=R0_ibok.ne.R0_ebok
      R0_ebok=R0_ibok
C dio_log.fgi(  91, 337):���������� ������������� ������
      if(L_(286).or.L_(287)) then
         L0_adok=(L_(286).or.L0_adok).and..not.(L_(287))
      else
         if(L0_ubok.and..not.L0_obok) L0_adok=.not.L0_adok
      endif
      L0_obok=L0_ubok
      L_(288)=.not.L0_adok
C dio_log.fgi( 108, 337):T �������
      if(L0_adok) then
         I_abok=1
      else
         I_abok=0
      endif
C dio_log.fgi( 119, 339):��������� LO->1
      L_(290)=.false.
C dio_log.fgi( 102, 323):��������� ���������� (�������)
      L_(289)=.false.
C dio_log.fgi( 102, 327):��������� ���������� (�������)
      L0_afok=R0_odok.ne.R0_idok
      R0_idok=R0_odok
C dio_log.fgi(  91, 325):���������� ������������� ������
      if(L_(289).or.L_(290)) then
         L0_efok=(L_(289).or.L0_efok).and..not.(L_(290))
      else
         if(L0_afok.and..not.L0_udok) L0_efok=.not.L0_efok
      endif
      L0_udok=L0_afok
      L_(291)=.not.L0_efok
C dio_log.fgi( 108, 325):T �������
      if(L0_efok) then
         I_edok=1
      else
         I_edok=0
      endif
C dio_log.fgi( 119, 327):��������� LO->1
      L_(293)=.false.
C dio_log.fgi( 102, 311):��������� ���������� (�������)
      L_(292)=.false.
C dio_log.fgi( 102, 315):��������� ���������� (�������)
      L0_ekok=R0_ufok.ne.R0_ofok
      R0_ofok=R0_ufok
C dio_log.fgi(  91, 313):���������� ������������� ������
      if(L_(292).or.L_(293)) then
         L0_ikok=(L_(292).or.L0_ikok).and..not.(L_(293))
      else
         if(L0_ekok.and..not.L0_akok) L0_ikok=.not.L0_ikok
      endif
      L0_akok=L0_ekok
      L_(294)=.not.L0_ikok
C dio_log.fgi( 108, 313):T �������
      if(L0_ikok) then
         I_ifok=1
      else
         I_ifok=0
      endif
C dio_log.fgi( 119, 315):��������� LO->1
      L_(296)=.false.
C dio_log.fgi( 102, 299):��������� ���������� (�������)
      L_(295)=.false.
C dio_log.fgi( 102, 303):��������� ���������� (�������)
      L0_ilok=R0_alok.ne.R0_ukok
      R0_ukok=R0_alok
C dio_log.fgi(  91, 301):���������� ������������� ������
      if(L_(295).or.L_(296)) then
         L0_olok=(L_(295).or.L0_olok).and..not.(L_(296))
      else
         if(L0_ilok.and..not.L0_elok) L0_olok=.not.L0_olok
      endif
      L0_elok=L0_ilok
      L_(297)=.not.L0_olok
C dio_log.fgi( 108, 301):T �������
      if(L0_olok) then
         I_okok=1
      else
         I_okok=0
      endif
C dio_log.fgi( 119, 303):��������� LO->1
      L_(299)=.false.
C dio_log.fgi( 102, 287):��������� ���������� (�������)
      L_(298)=.false.
C dio_log.fgi( 102, 291):��������� ���������� (�������)
      L0_omok=R0_emok.ne.R0_amok
      R0_amok=R0_emok
C dio_log.fgi(  91, 289):���������� ������������� ������
      if(L_(298).or.L_(299)) then
         L0_umok=(L_(298).or.L0_umok).and..not.(L_(299))
      else
         if(L0_omok.and..not.L0_imok) L0_umok=.not.L0_umok
      endif
      L0_imok=L0_omok
      L_(300)=.not.L0_umok
C dio_log.fgi( 108, 289):T �������
      if(L0_umok) then
         I_ulok=1
      else
         I_ulok=0
      endif
C dio_log.fgi( 119, 291):��������� LO->1
      L_(302)=.false.
C dio_log.fgi( 102, 275):��������� ���������� (�������)
      L_(301)=.false.
C dio_log.fgi( 102, 279):��������� ���������� (�������)
      L0_upok=R0_ipok.ne.R0_epok
      R0_epok=R0_ipok
C dio_log.fgi(  91, 277):���������� ������������� ������
      if(L_(301).or.L_(302)) then
         L0_arok=(L_(301).or.L0_arok).and..not.(L_(302))
      else
         if(L0_upok.and..not.L0_opok) L0_arok=.not.L0_arok
      endif
      L0_opok=L0_upok
      L_(303)=.not.L0_arok
C dio_log.fgi( 108, 277):T �������
      if(L0_arok) then
         I_apok=1
      else
         I_apok=0
      endif
C dio_log.fgi( 119, 279):��������� LO->1
      L_(305)=.false.
C dio_log.fgi( 102, 263):��������� ���������� (�������)
      L_(304)=.false.
C dio_log.fgi( 102, 267):��������� ���������� (�������)
      L0_asok=R0_orok.ne.R0_irok
      R0_irok=R0_orok
C dio_log.fgi(  91, 265):���������� ������������� ������
      if(L_(304).or.L_(305)) then
         L0_esok=(L_(304).or.L0_esok).and..not.(L_(305))
      else
         if(L0_asok.and..not.L0_urok) L0_esok=.not.L0_esok
      endif
      L0_urok=L0_asok
      L_(306)=.not.L0_esok
C dio_log.fgi( 108, 265):T �������
      if(L0_esok) then
         I_erok=1
      else
         I_erok=0
      endif
C dio_log.fgi( 119, 267):��������� LO->1
      L_(308)=.false.
C dio_log.fgi( 102, 251):��������� ���������� (�������)
      L_(307)=.false.
C dio_log.fgi( 102, 255):��������� ���������� (�������)
      L0_etok=R0_usok.ne.R0_osok
      R0_osok=R0_usok
C dio_log.fgi(  91, 253):���������� ������������� ������
      if(L_(307).or.L_(308)) then
         L0_itok=(L_(307).or.L0_itok).and..not.(L_(308))
      else
         if(L0_etok.and..not.L0_atok) L0_itok=.not.L0_itok
      endif
      L0_atok=L0_etok
      L_(309)=.not.L0_itok
C dio_log.fgi( 108, 253):T �������
      if(L0_itok) then
         I_isok=1
      else
         I_isok=0
      endif
C dio_log.fgi( 119, 255):��������� LO->1
      L_(311)=.false.
C dio_log.fgi( 102, 239):��������� ���������� (�������)
      L_(310)=.false.
C dio_log.fgi( 102, 243):��������� ���������� (�������)
      L0_ivok=R0_avok.ne.R0_utok
      R0_utok=R0_avok
C dio_log.fgi(  91, 241):���������� ������������� ������
      if(L_(310).or.L_(311)) then
         L0_ovok=(L_(310).or.L0_ovok).and..not.(L_(311))
      else
         if(L0_ivok.and..not.L0_evok) L0_ovok=.not.L0_ovok
      endif
      L0_evok=L0_ivok
      L_(312)=.not.L0_ovok
C dio_log.fgi( 108, 241):T �������
      if(L0_ovok) then
         I_otok=1
      else
         I_otok=0
      endif
C dio_log.fgi( 119, 243):��������� LO->1
      L_(314)=.false.
C dio_log.fgi( 102, 227):��������� ���������� (�������)
      L_(313)=.false.
C dio_log.fgi( 102, 231):��������� ���������� (�������)
      L0_oxok=R0_exok.ne.R0_axok
      R0_axok=R0_exok
C dio_log.fgi(  91, 229):���������� ������������� ������
      if(L_(313).or.L_(314)) then
         L0_uxok=(L_(313).or.L0_uxok).and..not.(L_(314))
      else
         if(L0_oxok.and..not.L0_ixok) L0_uxok=.not.L0_uxok
      endif
      L0_ixok=L0_oxok
      L_(315)=.not.L0_uxok
C dio_log.fgi( 108, 229):T �������
      if(L0_uxok) then
         I_uvok=1
      else
         I_uvok=0
      endif
C dio_log.fgi( 119, 231):��������� LO->1
      L_(317)=.false.
C dio_log.fgi( 102, 215):��������� ���������� (�������)
      L_(316)=.false.
C dio_log.fgi( 102, 219):��������� ���������� (�������)
      L0_ubuk=R0_ibuk.ne.R0_ebuk
      R0_ebuk=R0_ibuk
C dio_log.fgi(  91, 217):���������� ������������� ������
      if(L_(316).or.L_(317)) then
         L0_aduk=(L_(316).or.L0_aduk).and..not.(L_(317))
      else
         if(L0_ubuk.and..not.L0_obuk) L0_aduk=.not.L0_aduk
      endif
      L0_obuk=L0_ubuk
      L_(318)=.not.L0_aduk
C dio_log.fgi( 108, 217):T �������
      if(L0_aduk) then
         I_abuk=1
      else
         I_abuk=0
      endif
C dio_log.fgi( 119, 219):��������� LO->1
      L_(320)=.false.
C dio_log.fgi( 102, 203):��������� ���������� (�������)
      L_(319)=.false.
C dio_log.fgi( 102, 207):��������� ���������� (�������)
      L0_afuk=R0_oduk.ne.R0_iduk
      R0_iduk=R0_oduk
C dio_log.fgi(  91, 205):���������� ������������� ������
      if(L_(319).or.L_(320)) then
         L0_efuk=(L_(319).or.L0_efuk).and..not.(L_(320))
      else
         if(L0_afuk.and..not.L0_uduk) L0_efuk=.not.L0_efuk
      endif
      L0_uduk=L0_afuk
      L_(321)=.not.L0_efuk
C dio_log.fgi( 108, 205):T �������
      if(L0_efuk) then
         I_eduk=1
      else
         I_eduk=0
      endif
C dio_log.fgi( 119, 207):��������� LO->1
      L_(323)=.false.
C dio_log.fgi( 102, 191):��������� ���������� (�������)
      L_(322)=.false.
C dio_log.fgi( 102, 195):��������� ���������� (�������)
      L0_ekuk=R0_ufuk.ne.R0_ofuk
      R0_ofuk=R0_ufuk
C dio_log.fgi(  91, 193):���������� ������������� ������
      if(L_(322).or.L_(323)) then
         L0_ikuk=(L_(322).or.L0_ikuk).and..not.(L_(323))
      else
         if(L0_ekuk.and..not.L0_akuk) L0_ikuk=.not.L0_ikuk
      endif
      L0_akuk=L0_ekuk
      L_(324)=.not.L0_ikuk
C dio_log.fgi( 108, 193):T �������
      if(L0_ikuk) then
         I_ifuk=1
      else
         I_ifuk=0
      endif
C dio_log.fgi( 119, 195):��������� LO->1
      L_(326)=.false.
C dio_log.fgi( 102, 179):��������� ���������� (�������)
      L_(325)=.false.
C dio_log.fgi( 102, 183):��������� ���������� (�������)
      L0_iluk=R0_aluk.ne.R0_ukuk
      R0_ukuk=R0_aluk
C dio_log.fgi(  91, 181):���������� ������������� ������
      if(L_(325).or.L_(326)) then
         L0_oluk=(L_(325).or.L0_oluk).and..not.(L_(326))
      else
         if(L0_iluk.and..not.L0_eluk) L0_oluk=.not.L0_oluk
      endif
      L0_eluk=L0_iluk
      L_(327)=.not.L0_oluk
C dio_log.fgi( 108, 181):T �������
      if(L0_oluk) then
         I_okuk=1
      else
         I_okuk=0
      endif
C dio_log.fgi( 119, 183):��������� LO->1
      L_(329)=.false.
C dio_log.fgi( 102, 167):��������� ���������� (�������)
      L_(328)=.false.
C dio_log.fgi( 102, 171):��������� ���������� (�������)
      L0_omuk=R0_emuk.ne.R0_amuk
      R0_amuk=R0_emuk
C dio_log.fgi(  91, 169):���������� ������������� ������
      if(L_(328).or.L_(329)) then
         L0_umuk=(L_(328).or.L0_umuk).and..not.(L_(329))
      else
         if(L0_omuk.and..not.L0_imuk) L0_umuk=.not.L0_umuk
      endif
      L0_imuk=L0_omuk
      L_(330)=.not.L0_umuk
C dio_log.fgi( 108, 169):T �������
      if(L0_umuk) then
         I_uluk=1
      else
         I_uluk=0
      endif
C dio_log.fgi( 119, 171):��������� LO->1
      L_(332)=.false.
C dio_log.fgi( 102, 155):��������� ���������� (�������)
      L_(331)=.false.
C dio_log.fgi( 102, 159):��������� ���������� (�������)
      L0_upuk=R0_ipuk.ne.R0_epuk
      R0_epuk=R0_ipuk
C dio_log.fgi(  91, 157):���������� ������������� ������
      if(L_(331).or.L_(332)) then
         L0_aruk=(L_(331).or.L0_aruk).and..not.(L_(332))
      else
         if(L0_upuk.and..not.L0_opuk) L0_aruk=.not.L0_aruk
      endif
      L0_opuk=L0_upuk
      L_(333)=.not.L0_aruk
C dio_log.fgi( 108, 157):T �������
      if(L0_aruk) then
         I_apuk=1
      else
         I_apuk=0
      endif
C dio_log.fgi( 119, 159):��������� LO->1
      L_(335)=.false.
C dio_log.fgi( 102, 143):��������� ���������� (�������)
      L_(334)=.false.
C dio_log.fgi( 102, 147):��������� ���������� (�������)
      L0_asuk=R0_oruk.ne.R0_iruk
      R0_iruk=R0_oruk
C dio_log.fgi(  91, 145):���������� ������������� ������
      if(L_(334).or.L_(335)) then
         L0_esuk=(L_(334).or.L0_esuk).and..not.(L_(335))
      else
         if(L0_asuk.and..not.L0_uruk) L0_esuk=.not.L0_esuk
      endif
      L0_uruk=L0_asuk
      L_(336)=.not.L0_esuk
C dio_log.fgi( 108, 145):T �������
      if(L0_esuk) then
         I_eruk=1
      else
         I_eruk=0
      endif
C dio_log.fgi( 119, 147):��������� LO->1
      L_(338)=.false.
C dio_log.fgi( 102, 131):��������� ���������� (�������)
      L_(337)=.false.
C dio_log.fgi( 102, 135):��������� ���������� (�������)
      L0_etuk=R0_usuk.ne.R0_osuk
      R0_osuk=R0_usuk
C dio_log.fgi(  91, 133):���������� ������������� ������
      if(L_(337).or.L_(338)) then
         L0_ituk=(L_(337).or.L0_ituk).and..not.(L_(338))
      else
         if(L0_etuk.and..not.L0_atuk) L0_ituk=.not.L0_ituk
      endif
      L0_atuk=L0_etuk
      L_(339)=.not.L0_ituk
C dio_log.fgi( 108, 133):T �������
      if(L0_ituk) then
         I_isuk=1
      else
         I_isuk=0
      endif
C dio_log.fgi( 119, 135):��������� LO->1
      L_(341)=.false.
C dio_log.fgi( 102, 119):��������� ���������� (�������)
      L_(340)=.false.
C dio_log.fgi( 102, 123):��������� ���������� (�������)
      L0_ivuk=R0_avuk.ne.R0_utuk
      R0_utuk=R0_avuk
C dio_log.fgi(  91, 121):���������� ������������� ������
      if(L_(340).or.L_(341)) then
         L0_ovuk=(L_(340).or.L0_ovuk).and..not.(L_(341))
      else
         if(L0_ivuk.and..not.L0_evuk) L0_ovuk=.not.L0_ovuk
      endif
      L0_evuk=L0_ivuk
      L_(342)=.not.L0_ovuk
C dio_log.fgi( 108, 121):T �������
      if(L0_ovuk) then
         I_otuk=1
      else
         I_otuk=0
      endif
C dio_log.fgi( 119, 123):��������� LO->1
      L_(344)=.false.
C dio_log.fgi( 102, 107):��������� ���������� (�������)
      L_(343)=.false.
C dio_log.fgi( 102, 111):��������� ���������� (�������)
      L0_oxuk=R0_exuk.ne.R0_axuk
      R0_axuk=R0_exuk
C dio_log.fgi(  91, 109):���������� ������������� ������
      if(L_(343).or.L_(344)) then
         L0_uxuk=(L_(343).or.L0_uxuk).and..not.(L_(344))
      else
         if(L0_oxuk.and..not.L0_ixuk) L0_uxuk=.not.L0_uxuk
      endif
      L0_ixuk=L0_oxuk
      L_(345)=.not.L0_uxuk
C dio_log.fgi( 108, 109):T �������
      if(L0_uxuk) then
         I_uvuk=1
      else
         I_uvuk=0
      endif
C dio_log.fgi( 119, 111):��������� LO->1
      L_(347)=.false.
C dio_log.fgi( 102,  95):��������� ���������� (�������)
      L_(346)=.false.
C dio_log.fgi( 102,  99):��������� ���������� (�������)
      L0_ubal=R0_ibal.ne.R0_ebal
      R0_ebal=R0_ibal
C dio_log.fgi(  91,  97):���������� ������������� ������
      if(L_(346).or.L_(347)) then
         L0_adal=(L_(346).or.L0_adal).and..not.(L_(347))
      else
         if(L0_ubal.and..not.L0_obal) L0_adal=.not.L0_adal
      endif
      L0_obal=L0_ubal
      L_(348)=.not.L0_adal
C dio_log.fgi( 108,  97):T �������
      if(L0_adal) then
         I_abal=1
      else
         I_abal=0
      endif
C dio_log.fgi( 119,  99):��������� LO->1
      L_(350)=.false.
C dio_log.fgi( 102,  83):��������� ���������� (�������)
      L_(349)=.false.
C dio_log.fgi( 102,  87):��������� ���������� (�������)
      L0_afal=R0_odal.ne.R0_idal
      R0_idal=R0_odal
C dio_log.fgi(  91,  85):���������� ������������� ������
      if(L_(349).or.L_(350)) then
         L0_efal=(L_(349).or.L0_efal).and..not.(L_(350))
      else
         if(L0_afal.and..not.L0_udal) L0_efal=.not.L0_efal
      endif
      L0_udal=L0_afal
      L_(351)=.not.L0_efal
C dio_log.fgi( 108,  85):T �������
      if(L0_efal) then
         I_edal=1
      else
         I_edal=0
      endif
C dio_log.fgi( 119,  87):��������� LO->1
      L_(353)=.false.
C dio_log.fgi( 102,  71):��������� ���������� (�������)
      L_(352)=.false.
C dio_log.fgi( 102,  75):��������� ���������� (�������)
      L0_ekal=R0_ufal.ne.R0_ofal
      R0_ofal=R0_ufal
C dio_log.fgi(  91,  73):���������� ������������� ������
      if(L_(352).or.L_(353)) then
         L0_ikal=(L_(352).or.L0_ikal).and..not.(L_(353))
      else
         if(L0_ekal.and..not.L0_akal) L0_ikal=.not.L0_ikal
      endif
      L0_akal=L0_ekal
      L_(354)=.not.L0_ikal
C dio_log.fgi( 108,  73):T �������
      if(L0_ikal) then
         I_ifal=1
      else
         I_ifal=0
      endif
C dio_log.fgi( 119,  75):��������� LO->1
      L_(356)=.false.
C dio_log.fgi( 102,  59):��������� ���������� (�������)
      L_(355)=.false.
C dio_log.fgi( 102,  63):��������� ���������� (�������)
      L0_ilal=R0_alal.ne.R0_ukal
      R0_ukal=R0_alal
C dio_log.fgi(  91,  61):���������� ������������� ������
      if(L_(355).or.L_(356)) then
         L0_olal=(L_(355).or.L0_olal).and..not.(L_(356))
      else
         if(L0_ilal.and..not.L0_elal) L0_olal=.not.L0_olal
      endif
      L0_elal=L0_ilal
      L_(357)=.not.L0_olal
C dio_log.fgi( 108,  61):T �������
      if(L0_olal) then
         I_okal=1
      else
         I_okal=0
      endif
C dio_log.fgi( 119,  63):��������� LO->1
      L_(359)=.false.
C dio_log.fgi( 102,  47):��������� ���������� (�������)
      L_(358)=.false.
C dio_log.fgi( 102,  51):��������� ���������� (�������)
      L0_omal=R0_emal.ne.R0_amal
      R0_amal=R0_emal
C dio_log.fgi(  91,  49):���������� ������������� ������
      if(L_(358).or.L_(359)) then
         L0_umal=(L_(358).or.L0_umal).and..not.(L_(359))
      else
         if(L0_omal.and..not.L0_imal) L0_umal=.not.L0_umal
      endif
      L0_imal=L0_omal
      L_(360)=.not.L0_umal
C dio_log.fgi( 108,  49):T �������
      if(L0_umal) then
         I_ulal=1
      else
         I_ulal=0
      endif
C dio_log.fgi( 119,  51):��������� LO->1
      L_(362)=.false.
C dio_log.fgi( 102,  35):��������� ���������� (�������)
      L_(361)=.false.
C dio_log.fgi( 102,  39):��������� ���������� (�������)
      L0_upal=R0_ipal.ne.R0_epal
      R0_epal=R0_ipal
C dio_log.fgi(  91,  37):���������� ������������� ������
      if(L_(361).or.L_(362)) then
         L0_aral=(L_(361).or.L0_aral).and..not.(L_(362))
      else
         if(L0_upal.and..not.L0_opal) L0_aral=.not.L0_aral
      endif
      L0_opal=L0_upal
      L_(363)=.not.L0_aral
C dio_log.fgi( 108,  37):T �������
      if(L0_aral) then
         I_apal=1
      else
         I_apal=0
      endif
C dio_log.fgi( 119,  39):��������� LO->1
      L_(365)=.false.
C dio_log.fgi( 102,  23):��������� ���������� (�������)
      L_(364)=.false.
C dio_log.fgi( 102,  27):��������� ���������� (�������)
      L0_asal=R0_oral.ne.R0_iral
      R0_iral=R0_oral
C dio_log.fgi(  91,  25):���������� ������������� ������
      if(L_(364).or.L_(365)) then
         L0_esal=(L_(364).or.L0_esal).and..not.(L_(365))
      else
         if(L0_asal.and..not.L0_ural) L0_esal=.not.L0_esal
      endif
      L0_ural=L0_asal
      L_(366)=.not.L0_esal
C dio_log.fgi( 108,  25):T �������
      if(L0_esal) then
         I_eral=1
      else
         I_eral=0
      endif
C dio_log.fgi( 119,  27):��������� LO->1
      L_(368)=.false.
C dio_log.fgi( 102,  11):��������� ���������� (�������)
      L_(367)=.false.
C dio_log.fgi( 102,  15):��������� ���������� (�������)
      L0_etal=R0_usal.ne.R0_osal
      R0_osal=R0_usal
C dio_log.fgi(  91,  13):���������� ������������� ������
      if(L_(367).or.L_(368)) then
         L0_ital=(L_(367).or.L0_ital).and..not.(L_(368))
      else
         if(L0_etal.and..not.L0_atal) L0_ital=.not.L0_ital
      endif
      L0_atal=L0_etal
      L_(369)=.not.L0_ital
C dio_log.fgi( 108,  13):T �������
      if(L0_ital) then
         I_isal=1
      else
         I_isal=0
      endif
C dio_log.fgi( 119,  15):��������� LO->1
      L_(371)=.false.
C dio_log.fgi(  29, 510):��������� ���������� (�������)
      L_(370)=.false.
C dio_log.fgi(  29, 514):��������� ���������� (�������)
      L0_ival=R0_aval.ne.R0_utal
      R0_utal=R0_aval
C dio_log.fgi(  18, 512):���������� ������������� ������
      if(L_(370).or.L_(371)) then
         L0_oval=(L_(370).or.L0_oval).and..not.(L_(371))
      else
         if(L0_ival.and..not.L0_eval) L0_oval=.not.L0_oval
      endif
      L0_eval=L0_ival
      L_(372)=.not.L0_oval
C dio_log.fgi(  35, 512):T �������
      if(L0_oval) then
         I_otal=1
      else
         I_otal=0
      endif
C dio_log.fgi(  46, 514):��������� LO->1
      L_(374)=.false.
C dio_log.fgi(  29, 498):��������� ���������� (�������)
      L_(373)=.false.
C dio_log.fgi(  29, 502):��������� ���������� (�������)
      L0_oxal=R0_exal.ne.R0_axal
      R0_axal=R0_exal
C dio_log.fgi(  18, 500):���������� ������������� ������
      if(L_(373).or.L_(374)) then
         L0_uxal=(L_(373).or.L0_uxal).and..not.(L_(374))
      else
         if(L0_oxal.and..not.L0_ixal) L0_uxal=.not.L0_uxal
      endif
      L0_ixal=L0_oxal
      L_(375)=.not.L0_uxal
C dio_log.fgi(  35, 500):T �������
      if(L0_uxal) then
         I_uval=1
      else
         I_uval=0
      endif
C dio_log.fgi(  46, 502):��������� LO->1
      L_(377)=.false.
C dio_log.fgi(  29, 486):��������� ���������� (�������)
      L_(376)=.false.
C dio_log.fgi(  29, 490):��������� ���������� (�������)
      L0_ubel=R0_ibel.ne.R0_ebel
      R0_ebel=R0_ibel
C dio_log.fgi(  18, 488):���������� ������������� ������
      if(L_(376).or.L_(377)) then
         L0_adel=(L_(376).or.L0_adel).and..not.(L_(377))
      else
         if(L0_ubel.and..not.L0_obel) L0_adel=.not.L0_adel
      endif
      L0_obel=L0_ubel
      L_(378)=.not.L0_adel
C dio_log.fgi(  35, 488):T �������
      if(L0_adel) then
         I_abel=1
      else
         I_abel=0
      endif
C dio_log.fgi(  46, 490):��������� LO->1
      L_(380)=.false.
C dio_log.fgi(  29, 474):��������� ���������� (�������)
      L_(379)=.false.
C dio_log.fgi(  29, 478):��������� ���������� (�������)
      L0_afel=R0_odel.ne.R0_idel
      R0_idel=R0_odel
C dio_log.fgi(  18, 476):���������� ������������� ������
      if(L_(379).or.L_(380)) then
         L0_efel=(L_(379).or.L0_efel).and..not.(L_(380))
      else
         if(L0_afel.and..not.L0_udel) L0_efel=.not.L0_efel
      endif
      L0_udel=L0_afel
      L_(381)=.not.L0_efel
C dio_log.fgi(  35, 476):T �������
      if(L0_efel) then
         I_edel=1
      else
         I_edel=0
      endif
C dio_log.fgi(  46, 478):��������� LO->1
      L_(383)=.false.
C dio_log.fgi(  29, 462):��������� ���������� (�������)
      L_(382)=.false.
C dio_log.fgi(  29, 466):��������� ���������� (�������)
      L0_ekel=R0_ufel.ne.R0_ofel
      R0_ofel=R0_ufel
C dio_log.fgi(  18, 464):���������� ������������� ������
      if(L_(382).or.L_(383)) then
         L0_ikel=(L_(382).or.L0_ikel).and..not.(L_(383))
      else
         if(L0_ekel.and..not.L0_akel) L0_ikel=.not.L0_ikel
      endif
      L0_akel=L0_ekel
      L_(384)=.not.L0_ikel
C dio_log.fgi(  35, 464):T �������
      if(L0_ikel) then
         I_ifel=1
      else
         I_ifel=0
      endif
C dio_log.fgi(  46, 466):��������� LO->1
      L_(386)=.false.
C dio_log.fgi(  29, 450):��������� ���������� (�������)
      L_(385)=.false.
C dio_log.fgi(  29, 454):��������� ���������� (�������)
      L0_ilel=R0_alel.ne.R0_ukel
      R0_ukel=R0_alel
C dio_log.fgi(  18, 452):���������� ������������� ������
      if(L_(385).or.L_(386)) then
         L0_olel=(L_(385).or.L0_olel).and..not.(L_(386))
      else
         if(L0_ilel.and..not.L0_elel) L0_olel=.not.L0_olel
      endif
      L0_elel=L0_ilel
      L_(387)=.not.L0_olel
C dio_log.fgi(  35, 452):T �������
      if(L0_olel) then
         I_okel=1
      else
         I_okel=0
      endif
C dio_log.fgi(  46, 454):��������� LO->1
      L_(389)=.false.
C dio_log.fgi(  29, 438):��������� ���������� (�������)
      L_(388)=.false.
C dio_log.fgi(  29, 442):��������� ���������� (�������)
      L0_omel=R0_emel.ne.R0_amel
      R0_amel=R0_emel
C dio_log.fgi(  18, 440):���������� ������������� ������
      if(L_(388).or.L_(389)) then
         L0_umel=(L_(388).or.L0_umel).and..not.(L_(389))
      else
         if(L0_omel.and..not.L0_imel) L0_umel=.not.L0_umel
      endif
      L0_imel=L0_omel
      L_(390)=.not.L0_umel
C dio_log.fgi(  35, 440):T �������
      if(L0_umel) then
         I_ulel=1
      else
         I_ulel=0
      endif
C dio_log.fgi(  46, 442):��������� LO->1
      L_(392)=.false.
C dio_log.fgi(  29, 426):��������� ���������� (�������)
      L_(391)=.false.
C dio_log.fgi(  29, 430):��������� ���������� (�������)
      L0_upel=R0_ipel.ne.R0_epel
      R0_epel=R0_ipel
C dio_log.fgi(  18, 428):���������� ������������� ������
      if(L_(391).or.L_(392)) then
         L0_arel=(L_(391).or.L0_arel).and..not.(L_(392))
      else
         if(L0_upel.and..not.L0_opel) L0_arel=.not.L0_arel
      endif
      L0_opel=L0_upel
      L_(393)=.not.L0_arel
C dio_log.fgi(  35, 428):T �������
      if(L0_arel) then
         I_apel=1
      else
         I_apel=0
      endif
C dio_log.fgi(  46, 430):��������� LO->1
      L_(395)=.false.
C dio_log.fgi(  29, 414):��������� ���������� (�������)
      L_(394)=.false.
C dio_log.fgi(  29, 418):��������� ���������� (�������)
      L0_asel=R0_orel.ne.R0_irel
      R0_irel=R0_orel
C dio_log.fgi(  18, 416):���������� ������������� ������
      if(L_(394).or.L_(395)) then
         L0_esel=(L_(394).or.L0_esel).and..not.(L_(395))
      else
         if(L0_asel.and..not.L0_urel) L0_esel=.not.L0_esel
      endif
      L0_urel=L0_asel
      L_(396)=.not.L0_esel
C dio_log.fgi(  35, 416):T �������
      if(L0_esel) then
         I_erel=1
      else
         I_erel=0
      endif
C dio_log.fgi(  46, 418):��������� LO->1
      L_(398)=.false.
C dio_log.fgi(  29, 402):��������� ���������� (�������)
      L_(397)=.false.
C dio_log.fgi(  29, 406):��������� ���������� (�������)
      L0_etel=R0_usel.ne.R0_osel
      R0_osel=R0_usel
C dio_log.fgi(  18, 404):���������� ������������� ������
      if(L_(397).or.L_(398)) then
         L0_itel=(L_(397).or.L0_itel).and..not.(L_(398))
      else
         if(L0_etel.and..not.L0_atel) L0_itel=.not.L0_itel
      endif
      L0_atel=L0_etel
      L_(399)=.not.L0_itel
C dio_log.fgi(  35, 404):T �������
      if(L0_itel) then
         I_isel=1
      else
         I_isel=0
      endif
C dio_log.fgi(  46, 406):��������� LO->1
      L_(401)=.false.
C dio_log.fgi(  29, 390):��������� ���������� (�������)
      L_(400)=.false.
C dio_log.fgi(  29, 394):��������� ���������� (�������)
      L0_ivel=R0_avel.ne.R0_utel
      R0_utel=R0_avel
C dio_log.fgi(  18, 392):���������� ������������� ������
      if(L_(400).or.L_(401)) then
         L0_ovel=(L_(400).or.L0_ovel).and..not.(L_(401))
      else
         if(L0_ivel.and..not.L0_evel) L0_ovel=.not.L0_ovel
      endif
      L0_evel=L0_ivel
      L_(402)=.not.L0_ovel
C dio_log.fgi(  35, 392):T �������
      if(L0_ovel) then
         I_otel=1
      else
         I_otel=0
      endif
C dio_log.fgi(  46, 394):��������� LO->1
      L_(404)=.false.
C dio_log.fgi(  29, 378):��������� ���������� (�������)
      L_(403)=.false.
C dio_log.fgi(  29, 382):��������� ���������� (�������)
      L0_oxel=R0_exel.ne.R0_axel
      R0_axel=R0_exel
C dio_log.fgi(  18, 380):���������� ������������� ������
      if(L_(403).or.L_(404)) then
         L0_uxel=(L_(403).or.L0_uxel).and..not.(L_(404))
      else
         if(L0_oxel.and..not.L0_ixel) L0_uxel=.not.L0_uxel
      endif
      L0_ixel=L0_oxel
      L_(405)=.not.L0_uxel
C dio_log.fgi(  35, 380):T �������
      if(L0_uxel) then
         I_uvel=1
      else
         I_uvel=0
      endif
C dio_log.fgi(  46, 382):��������� LO->1
      L_(407)=.false.
C dio_log.fgi(  29, 366):��������� ���������� (�������)
      L_(406)=.false.
C dio_log.fgi(  29, 370):��������� ���������� (�������)
      L0_ubil=R0_ibil.ne.R0_ebil
      R0_ebil=R0_ibil
C dio_log.fgi(  18, 368):���������� ������������� ������
      if(L_(406).or.L_(407)) then
         L0_adil=(L_(406).or.L0_adil).and..not.(L_(407))
      else
         if(L0_ubil.and..not.L0_obil) L0_adil=.not.L0_adil
      endif
      L0_obil=L0_ubil
      L_(408)=.not.L0_adil
C dio_log.fgi(  35, 368):T �������
      if(L0_adil) then
         I_abil=1
      else
         I_abil=0
      endif
C dio_log.fgi(  46, 370):��������� LO->1
      L_(410)=.false.
C dio_log.fgi(  29, 354):��������� ���������� (�������)
      L_(409)=.false.
C dio_log.fgi(  29, 358):��������� ���������� (�������)
      L0_afil=R0_odil.ne.R0_idil
      R0_idil=R0_odil
C dio_log.fgi(  18, 356):���������� ������������� ������
      if(L_(409).or.L_(410)) then
         L0_efil=(L_(409).or.L0_efil).and..not.(L_(410))
      else
         if(L0_afil.and..not.L0_udil) L0_efil=.not.L0_efil
      endif
      L0_udil=L0_afil
      L_(411)=.not.L0_efil
C dio_log.fgi(  35, 356):T �������
      if(L0_efil) then
         I_edil=1
      else
         I_edil=0
      endif
C dio_log.fgi(  46, 358):��������� LO->1
      L_(413)=.false.
C dio_log.fgi(  29, 342):��������� ���������� (�������)
      L_(412)=.false.
C dio_log.fgi(  29, 346):��������� ���������� (�������)
      L0_ekil=R0_ufil.ne.R0_ofil
      R0_ofil=R0_ufil
C dio_log.fgi(  18, 344):���������� ������������� ������
      if(L_(412).or.L_(413)) then
         L0_ikil=(L_(412).or.L0_ikil).and..not.(L_(413))
      else
         if(L0_ekil.and..not.L0_akil) L0_ikil=.not.L0_ikil
      endif
      L0_akil=L0_ekil
      L_(414)=.not.L0_ikil
C dio_log.fgi(  35, 344):T �������
      if(L0_ikil) then
         I_ifil=1
      else
         I_ifil=0
      endif
C dio_log.fgi(  46, 346):��������� LO->1
      L_(416)=.false.
C dio_log.fgi(  29, 330):��������� ���������� (�������)
      L_(415)=.false.
C dio_log.fgi(  29, 334):��������� ���������� (�������)
      L0_ilil=R0_alil.ne.R0_ukil
      R0_ukil=R0_alil
C dio_log.fgi(  18, 332):���������� ������������� ������
      if(L_(415).or.L_(416)) then
         L0_olil=(L_(415).or.L0_olil).and..not.(L_(416))
      else
         if(L0_ilil.and..not.L0_elil) L0_olil=.not.L0_olil
      endif
      L0_elil=L0_ilil
      L_(417)=.not.L0_olil
C dio_log.fgi(  35, 332):T �������
      if(L0_olil) then
         I_okil=1
      else
         I_okil=0
      endif
C dio_log.fgi(  46, 334):��������� LO->1
      L_(419)=.false.
C dio_log.fgi(  29, 318):��������� ���������� (�������)
      L_(418)=.false.
C dio_log.fgi(  29, 322):��������� ���������� (�������)
      L0_omil=R0_emil.ne.R0_amil
      R0_amil=R0_emil
C dio_log.fgi(  18, 320):���������� ������������� ������
      if(L_(418).or.L_(419)) then
         L0_umil=(L_(418).or.L0_umil).and..not.(L_(419))
      else
         if(L0_omil.and..not.L0_imil) L0_umil=.not.L0_umil
      endif
      L0_imil=L0_omil
      L_(420)=.not.L0_umil
C dio_log.fgi(  35, 320):T �������
      if(L0_umil) then
         I_ulil=1
      else
         I_ulil=0
      endif
C dio_log.fgi(  46, 322):��������� LO->1
      L_(422)=.false.
C dio_log.fgi(  29, 306):��������� ���������� (�������)
      L_(421)=.false.
C dio_log.fgi(  29, 310):��������� ���������� (�������)
      L0_upil=R0_ipil.ne.R0_epil
      R0_epil=R0_ipil
C dio_log.fgi(  18, 308):���������� ������������� ������
      if(L_(421).or.L_(422)) then
         L0_aril=(L_(421).or.L0_aril).and..not.(L_(422))
      else
         if(L0_upil.and..not.L0_opil) L0_aril=.not.L0_aril
      endif
      L0_opil=L0_upil
      L_(423)=.not.L0_aril
C dio_log.fgi(  35, 308):T �������
      if(L0_aril) then
         I_apil=1
      else
         I_apil=0
      endif
C dio_log.fgi(  46, 310):��������� LO->1
      L_(425)=.false.
C dio_log.fgi(  29, 294):��������� ���������� (�������)
      L_(424)=.false.
C dio_log.fgi(  29, 298):��������� ���������� (�������)
      L0_asil=R0_oril.ne.R0_iril
      R0_iril=R0_oril
C dio_log.fgi(  18, 296):���������� ������������� ������
      if(L_(424).or.L_(425)) then
         L0_esil=(L_(424).or.L0_esil).and..not.(L_(425))
      else
         if(L0_asil.and..not.L0_uril) L0_esil=.not.L0_esil
      endif
      L0_uril=L0_asil
      L_(426)=.not.L0_esil
C dio_log.fgi(  35, 296):T �������
      if(L0_esil) then
         I_eril=1
      else
         I_eril=0
      endif
C dio_log.fgi(  46, 298):��������� LO->1
      L_(428)=.false.
C dio_log.fgi(  29, 282):��������� ���������� (�������)
      L_(427)=.false.
C dio_log.fgi(  29, 286):��������� ���������� (�������)
      L0_etil=R0_usil.ne.R0_osil
      R0_osil=R0_usil
C dio_log.fgi(  18, 284):���������� ������������� ������
      if(L_(427).or.L_(428)) then
         L0_itil=(L_(427).or.L0_itil).and..not.(L_(428))
      else
         if(L0_etil.and..not.L0_atil) L0_itil=.not.L0_itil
      endif
      L0_atil=L0_etil
      L_(429)=.not.L0_itil
C dio_log.fgi(  35, 284):T �������
      if(L0_itil) then
         I_isil=1
      else
         I_isil=0
      endif
C dio_log.fgi(  46, 286):��������� LO->1
      L_(431)=.false.
C dio_log.fgi(  29, 270):��������� ���������� (�������)
      L_(430)=.false.
C dio_log.fgi(  29, 274):��������� ���������� (�������)
      L0_ivil=R0_avil.ne.R0_util
      R0_util=R0_avil
C dio_log.fgi(  18, 272):���������� ������������� ������
      if(L_(430).or.L_(431)) then
         L0_ovil=(L_(430).or.L0_ovil).and..not.(L_(431))
      else
         if(L0_ivil.and..not.L0_evil) L0_ovil=.not.L0_ovil
      endif
      L0_evil=L0_ivil
      L_(432)=.not.L0_ovil
C dio_log.fgi(  35, 272):T �������
      if(L0_ovil) then
         I_otil=1
      else
         I_otil=0
      endif
C dio_log.fgi(  46, 274):��������� LO->1
      L_(434)=.false.
C dio_log.fgi(  29, 258):��������� ���������� (�������)
      L_(433)=.false.
C dio_log.fgi(  29, 262):��������� ���������� (�������)
      L0_oxil=R0_exil.ne.R0_axil
      R0_axil=R0_exil
C dio_log.fgi(  18, 260):���������� ������������� ������
      if(L_(433).or.L_(434)) then
         L0_uxil=(L_(433).or.L0_uxil).and..not.(L_(434))
      else
         if(L0_oxil.and..not.L0_ixil) L0_uxil=.not.L0_uxil
      endif
      L0_ixil=L0_oxil
      L_(435)=.not.L0_uxil
C dio_log.fgi(  35, 260):T �������
      if(L0_uxil) then
         I_uvil=1
      else
         I_uvil=0
      endif
C dio_log.fgi(  46, 262):��������� LO->1
      L_(437)=.false.
C dio_log.fgi(  29, 246):��������� ���������� (�������)
      L_(436)=.false.
C dio_log.fgi(  29, 250):��������� ���������� (�������)
      L0_ubol=R0_ibol.ne.R0_ebol
      R0_ebol=R0_ibol
C dio_log.fgi(  18, 248):���������� ������������� ������
      if(L_(436).or.L_(437)) then
         L0_adol=(L_(436).or.L0_adol).and..not.(L_(437))
      else
         if(L0_ubol.and..not.L0_obol) L0_adol=.not.L0_adol
      endif
      L0_obol=L0_ubol
      L_(438)=.not.L0_adol
C dio_log.fgi(  35, 248):T �������
      if(L0_adol) then
         I_abol=1
      else
         I_abol=0
      endif
C dio_log.fgi(  46, 250):��������� LO->1
      L_(440)=.false.
C dio_log.fgi(  29, 234):��������� ���������� (�������)
      L_(439)=.false.
C dio_log.fgi(  29, 238):��������� ���������� (�������)
      L0_afol=R0_odol.ne.R0_idol
      R0_idol=R0_odol
C dio_log.fgi(  18, 236):���������� ������������� ������
      if(L_(439).or.L_(440)) then
         L0_efol=(L_(439).or.L0_efol).and..not.(L_(440))
      else
         if(L0_afol.and..not.L0_udol) L0_efol=.not.L0_efol
      endif
      L0_udol=L0_afol
      L_(441)=.not.L0_efol
C dio_log.fgi(  35, 236):T �������
      if(L0_efol) then
         I_edol=1
      else
         I_edol=0
      endif
C dio_log.fgi(  46, 238):��������� LO->1
      L_(443)=.false.
C dio_log.fgi(  29, 222):��������� ���������� (�������)
      L_(442)=.false.
C dio_log.fgi(  29, 226):��������� ���������� (�������)
      L0_ekol=R0_ufol.ne.R0_ofol
      R0_ofol=R0_ufol
C dio_log.fgi(  18, 224):���������� ������������� ������
      if(L_(442).or.L_(443)) then
         L0_ikol=(L_(442).or.L0_ikol).and..not.(L_(443))
      else
         if(L0_ekol.and..not.L0_akol) L0_ikol=.not.L0_ikol
      endif
      L0_akol=L0_ekol
      L_(444)=.not.L0_ikol
C dio_log.fgi(  35, 224):T �������
      if(L0_ikol) then
         I_ifol=1
      else
         I_ifol=0
      endif
C dio_log.fgi(  46, 226):��������� LO->1
      L_(446)=.false.
C dio_log.fgi(  29, 210):��������� ���������� (�������)
      L_(445)=.false.
C dio_log.fgi(  29, 214):��������� ���������� (�������)
      L0_ilol=R0_alol.ne.R0_ukol
      R0_ukol=R0_alol
C dio_log.fgi(  18, 212):���������� ������������� ������
      if(L_(445).or.L_(446)) then
         L0_olol=(L_(445).or.L0_olol).and..not.(L_(446))
      else
         if(L0_ilol.and..not.L0_elol) L0_olol=.not.L0_olol
      endif
      L0_elol=L0_ilol
      L_(447)=.not.L0_olol
C dio_log.fgi(  35, 212):T �������
      if(L0_olol) then
         I_okol=1
      else
         I_okol=0
      endif
C dio_log.fgi(  46, 214):��������� LO->1
      L_(449)=.false.
C dio_log.fgi(  29, 198):��������� ���������� (�������)
      L_(448)=.false.
C dio_log.fgi(  29, 202):��������� ���������� (�������)
      L0_omol=R0_emol.ne.R0_amol
      R0_amol=R0_emol
C dio_log.fgi(  18, 200):���������� ������������� ������
      if(L_(448).or.L_(449)) then
         L0_umol=(L_(448).or.L0_umol).and..not.(L_(449))
      else
         if(L0_omol.and..not.L0_imol) L0_umol=.not.L0_umol
      endif
      L0_imol=L0_omol
      L_(450)=.not.L0_umol
C dio_log.fgi(  35, 200):T �������
      if(L0_umol) then
         I_ulol=1
      else
         I_ulol=0
      endif
C dio_log.fgi(  46, 202):��������� LO->1
      L_(452)=.false.
C dio_log.fgi(  29, 186):��������� ���������� (�������)
      L_(451)=.false.
C dio_log.fgi(  29, 190):��������� ���������� (�������)
      L0_upol=R0_ipol.ne.R0_epol
      R0_epol=R0_ipol
C dio_log.fgi(  18, 188):���������� ������������� ������
      if(L_(451).or.L_(452)) then
         L0_arol=(L_(451).or.L0_arol).and..not.(L_(452))
      else
         if(L0_upol.and..not.L0_opol) L0_arol=.not.L0_arol
      endif
      L0_opol=L0_upol
      L_(453)=.not.L0_arol
C dio_log.fgi(  35, 188):T �������
      if(L0_arol) then
         I_apol=1
      else
         I_apol=0
      endif
C dio_log.fgi(  46, 190):��������� LO->1
      L_(455)=.false.
C dio_log.fgi(  29, 174):��������� ���������� (�������)
      L_(454)=.false.
C dio_log.fgi(  29, 178):��������� ���������� (�������)
      L0_asol=R0_orol.ne.R0_irol
      R0_irol=R0_orol
C dio_log.fgi(  18, 176):���������� ������������� ������
      if(L_(454).or.L_(455)) then
         L0_esol=(L_(454).or.L0_esol).and..not.(L_(455))
      else
         if(L0_asol.and..not.L0_urol) L0_esol=.not.L0_esol
      endif
      L0_urol=L0_asol
      L_(456)=.not.L0_esol
C dio_log.fgi(  35, 176):T �������
      if(L0_esol) then
         I_erol=1
      else
         I_erol=0
      endif
C dio_log.fgi(  46, 178):��������� LO->1
      L_(458)=.false.
C dio_log.fgi(  29, 162):��������� ���������� (�������)
      L_(457)=.false.
C dio_log.fgi(  29, 166):��������� ���������� (�������)
      L0_etol=R0_usol.ne.R0_osol
      R0_osol=R0_usol
C dio_log.fgi(  18, 164):���������� ������������� ������
      if(L_(457).or.L_(458)) then
         L0_itol=(L_(457).or.L0_itol).and..not.(L_(458))
      else
         if(L0_etol.and..not.L0_atol) L0_itol=.not.L0_itol
      endif
      L0_atol=L0_etol
      L_(459)=.not.L0_itol
C dio_log.fgi(  35, 164):T �������
      if(L0_itol) then
         I_isol=1
      else
         I_isol=0
      endif
C dio_log.fgi(  46, 166):��������� LO->1
      L_(461)=.false.
C dio_log.fgi(  29, 150):��������� ���������� (�������)
      L_(460)=.false.
C dio_log.fgi(  29, 154):��������� ���������� (�������)
      L0_ivol=R0_avol.ne.R0_utol
      R0_utol=R0_avol
C dio_log.fgi(  18, 152):���������� ������������� ������
      if(L_(460).or.L_(461)) then
         L0_ovol=(L_(460).or.L0_ovol).and..not.(L_(461))
      else
         if(L0_ivol.and..not.L0_evol) L0_ovol=.not.L0_ovol
      endif
      L0_evol=L0_ivol
      L_(462)=.not.L0_ovol
C dio_log.fgi(  35, 152):T �������
      if(L0_ovol) then
         I_otol=1
      else
         I_otol=0
      endif
C dio_log.fgi(  46, 154):��������� LO->1
      L_(464)=.false.
C dio_log.fgi(  29, 138):��������� ���������� (�������)
      L_(463)=.false.
C dio_log.fgi(  29, 142):��������� ���������� (�������)
      L0_oxol=R0_exol.ne.R0_axol
      R0_axol=R0_exol
C dio_log.fgi(  18, 140):���������� ������������� ������
      if(L_(463).or.L_(464)) then
         L0_uxol=(L_(463).or.L0_uxol).and..not.(L_(464))
      else
         if(L0_oxol.and..not.L0_ixol) L0_uxol=.not.L0_uxol
      endif
      L0_ixol=L0_oxol
      L_(465)=.not.L0_uxol
C dio_log.fgi(  35, 140):T �������
      if(L0_uxol) then
         I_uvol=1
      else
         I_uvol=0
      endif
C dio_log.fgi(  46, 142):��������� LO->1
      L_(467)=.false.
C dio_log.fgi(  29, 126):��������� ���������� (�������)
      L_(466)=.false.
C dio_log.fgi(  29, 130):��������� ���������� (�������)
      L0_ubul=R0_ibul.ne.R0_ebul
      R0_ebul=R0_ibul
C dio_log.fgi(  18, 128):���������� ������������� ������
      if(L_(466).or.L_(467)) then
         L0_adul=(L_(466).or.L0_adul).and..not.(L_(467))
      else
         if(L0_ubul.and..not.L0_obul) L0_adul=.not.L0_adul
      endif
      L0_obul=L0_ubul
      L_(468)=.not.L0_adul
C dio_log.fgi(  35, 128):T �������
      if(L0_adul) then
         I_abul=1
      else
         I_abul=0
      endif
C dio_log.fgi(  46, 130):��������� LO->1
      L_(470)=.false.
C dio_log.fgi(  29, 114):��������� ���������� (�������)
      L_(469)=.false.
C dio_log.fgi(  29, 118):��������� ���������� (�������)
      L0_aful=R0_odul.ne.R0_idul
      R0_idul=R0_odul
C dio_log.fgi(  18, 116):���������� ������������� ������
      if(L_(469).or.L_(470)) then
         L0_eful=(L_(469).or.L0_eful).and..not.(L_(470))
      else
         if(L0_aful.and..not.L0_udul) L0_eful=.not.L0_eful
      endif
      L0_udul=L0_aful
      L_(471)=.not.L0_eful
C dio_log.fgi(  35, 116):T �������
      if(L0_eful) then
         I_edul=1
      else
         I_edul=0
      endif
C dio_log.fgi(  46, 118):��������� LO->1
      L_(473)=.false.
C dio_log.fgi(  29, 102):��������� ���������� (�������)
      L_(472)=.false.
C dio_log.fgi(  29, 106):��������� ���������� (�������)
      L0_ekul=R0_uful.ne.R0_oful
      R0_oful=R0_uful
C dio_log.fgi(  18, 104):���������� ������������� ������
      if(L_(472).or.L_(473)) then
         L0_ikul=(L_(472).or.L0_ikul).and..not.(L_(473))
      else
         if(L0_ekul.and..not.L0_akul) L0_ikul=.not.L0_ikul
      endif
      L0_akul=L0_ekul
      L_(474)=.not.L0_ikul
C dio_log.fgi(  35, 104):T �������
      if(L0_ikul) then
         I_iful=1
      else
         I_iful=0
      endif
C dio_log.fgi(  46, 106):��������� LO->1
      L_(476)=.false.
C dio_log.fgi(  29,  90):��������� ���������� (�������)
      L_(475)=.false.
C dio_log.fgi(  29,  94):��������� ���������� (�������)
      L0_ilul=R0_alul.ne.R0_ukul
      R0_ukul=R0_alul
C dio_log.fgi(  18,  92):���������� ������������� ������
      if(L_(475).or.L_(476)) then
         L0_olul=(L_(475).or.L0_olul).and..not.(L_(476))
      else
         if(L0_ilul.and..not.L0_elul) L0_olul=.not.L0_olul
      endif
      L0_elul=L0_ilul
      L_(477)=.not.L0_olul
C dio_log.fgi(  35,  92):T �������
      if(L0_olul) then
         I_okul=1
      else
         I_okul=0
      endif
C dio_log.fgi(  46,  94):��������� LO->1
      L_(479)=.false.
C dio_log.fgi(  29,  78):��������� ���������� (�������)
      L_(478)=.false.
C dio_log.fgi(  29,  82):��������� ���������� (�������)
      L0_omul=R0_emul.ne.R0_amul
      R0_amul=R0_emul
C dio_log.fgi(  18,  80):���������� ������������� ������
      if(L_(478).or.L_(479)) then
         L0_umul=(L_(478).or.L0_umul).and..not.(L_(479))
      else
         if(L0_omul.and..not.L0_imul) L0_umul=.not.L0_umul
      endif
      L0_imul=L0_omul
      L_(480)=.not.L0_umul
C dio_log.fgi(  35,  80):T �������
      if(L0_umul) then
         I_ulul=1
      else
         I_ulul=0
      endif
C dio_log.fgi(  46,  82):��������� LO->1
      L_(482)=.false.
C dio_log.fgi(  29,  66):��������� ���������� (�������)
      L_(481)=.false.
C dio_log.fgi(  29,  70):��������� ���������� (�������)
      L0_upul=R0_ipul.ne.R0_epul
      R0_epul=R0_ipul
C dio_log.fgi(  18,  68):���������� ������������� ������
      if(L_(481).or.L_(482)) then
         L0_arul=(L_(481).or.L0_arul).and..not.(L_(482))
      else
         if(L0_upul.and..not.L0_opul) L0_arul=.not.L0_arul
      endif
      L0_opul=L0_upul
      L_(483)=.not.L0_arul
C dio_log.fgi(  35,  68):T �������
      if(L0_arul) then
         I_apul=1
      else
         I_apul=0
      endif
C dio_log.fgi(  46,  70):��������� LO->1
      L_(485)=.false.
C dio_log.fgi(  29,  54):��������� ���������� (�������)
      L_(484)=.false.
C dio_log.fgi(  29,  58):��������� ���������� (�������)
      L0_asul=R0_orul.ne.R0_irul
      R0_irul=R0_orul
C dio_log.fgi(  18,  56):���������� ������������� ������
      if(L_(484).or.L_(485)) then
         L0_esul=(L_(484).or.L0_esul).and..not.(L_(485))
      else
         if(L0_asul.and..not.L0_urul) L0_esul=.not.L0_esul
      endif
      L0_urul=L0_asul
      L_(486)=.not.L0_esul
C dio_log.fgi(  35,  56):T �������
      if(L0_esul) then
         I_erul=1
      else
         I_erul=0
      endif
C dio_log.fgi(  46,  58):��������� LO->1
      L_(488)=.false.
C dio_log.fgi(  29,  42):��������� ���������� (�������)
      L_(487)=.false.
C dio_log.fgi(  29,  46):��������� ���������� (�������)
      L0_etul=R0_usul.ne.R0_osul
      R0_osul=R0_usul
C dio_log.fgi(  18,  44):���������� ������������� ������
      if(L_(487).or.L_(488)) then
         L0_itul=(L_(487).or.L0_itul).and..not.(L_(488))
      else
         if(L0_etul.and..not.L0_atul) L0_itul=.not.L0_itul
      endif
      L0_atul=L0_etul
      L_(489)=.not.L0_itul
C dio_log.fgi(  35,  44):T �������
      if(L0_itul) then
         I_isul=1
      else
         I_isul=0
      endif
C dio_log.fgi(  46,  46):��������� LO->1
      L_(491)=.false.
C dio_log.fgi(  29,  30):��������� ���������� (�������)
      L_(490)=.false.
C dio_log.fgi(  29,  34):��������� ���������� (�������)
      L0_ivul=R0_avul.ne.R0_utul
      R0_utul=R0_avul
C dio_log.fgi(  18,  32):���������� ������������� ������
      if(L_(490).or.L_(491)) then
         L0_ovul=(L_(490).or.L0_ovul).and..not.(L_(491))
      else
         if(L0_ivul.and..not.L0_evul) L0_ovul=.not.L0_ovul
      endif
      L0_evul=L0_ivul
      L_(492)=.not.L0_ovul
C dio_log.fgi(  35,  32):T �������
      if(L0_ovul) then
         I_otul=1
      else
         I_otul=0
      endif
C dio_log.fgi(  46,  34):��������� LO->1
      L_(494)=.false.
C dio_log.fgi(  29,  18):��������� ���������� (�������)
      L_(493)=.false.
C dio_log.fgi(  29,  22):��������� ���������� (�������)
      L0_oxul=R0_exul.ne.R0_axul
      R0_axul=R0_exul
C dio_log.fgi(  18,  20):���������� ������������� ������
      if(L_(493).or.L_(494)) then
         L0_uxul=(L_(493).or.L0_uxul).and..not.(L_(494))
      else
         if(L0_oxul.and..not.L0_ixul) L0_uxul=.not.L0_uxul
      endif
      L0_ixul=L0_oxul
      L_(495)=.not.L0_uxul
C dio_log.fgi(  35,  20):T �������
      if(L0_uxul) then
         I_uvul=1
      else
         I_uvul=0
      endif
C dio_log.fgi(  46,  22):��������� LO->1
      L_(497)=.false.
C dio_log.fgi(  29,   6):��������� ���������� (�������)
      L_(496)=.false.
C dio_log.fgi(  29,  10):��������� ���������� (�������)
      L0_ubam=R0_ibam.ne.R0_ebam
      R0_ebam=R0_ibam
C dio_log.fgi(  18,   8):���������� ������������� ������
      if(L_(496).or.L_(497)) then
         L0_adam=(L_(496).or.L0_adam).and..not.(L_(497))
      else
         if(L0_ubam.and..not.L0_obam) L0_adam=.not.L0_adam
      endif
      L0_obam=L0_ubam
      L_(498)=.not.L0_adam
C dio_log.fgi(  35,   8):T �������
      if(L0_adam) then
         I_abam=1
      else
         I_abam=0
      endif
C dio_log.fgi(  46,  10):��������� LO->1
      L_(500)=.false.
C dio_log.fgi(  10, 574):��������� ���������� (�������)
      if(L_(500)) then
         I0_edam=0
      endif
      I_idam=I0_edam
      L_(499)=I0_edam.ne.0
C dio_log.fgi(  18, 576):���������� ��������� 
      R_(3) = -1.0
C 1762-70-0014_log.fgi(  36, 266):��������� (RE4) (�������)
      R_(2) = 1.0
C 1762-70-0014_log.fgi(  36, 264):��������� (RE4) (�������)
      L_(503)=.false.
C 1762-70-0014_log.fgi(  26, 256):��������� ���������� (�������)
      L_(502)=.false.
C 1762-70-0014_log.fgi(  26, 260):��������� ���������� (�������)
      L0_ukam=R0_ikam.ne.R0_ekam
      R0_ekam=R0_ikam
C 1762-70-0014_log.fgi(  17, 258):���������� ������������� ������
      if(L_(502).or.L_(503)) then
         L0_alam=(L_(502).or.L0_alam).and..not.(L_(503))
      else
         if(L0_ukam.and..not.L0_okam) L0_alam=.not.L0_alam
      endif
      L0_okam=L0_ukam
      L_(504)=.not.L0_alam
C 1762-70-0014_log.fgi(  32, 258):T �������
      if(L0_alam) then
         R_(1)=R_(2)
      else
         R_(1)=R_(3)
      endif
C 1762-70-0014_log.fgi(  39, 265):���� RE IN LO CH7
      R8_ofam=R8_ofam+deltat/R0_ifam*R_(1)
      if(R8_ofam.gt.R0_efam) then
         R8_ofam=R0_efam
      elseif(R8_ofam.lt.R0_ufam) then
         R8_ofam=R0_ufam
      endif
C 1762-70-0014_log.fgi(  49, 264):����������
      R8_afam=R8_ofam
C 1762-70-0014_log.fgi(  90, 266):������,20FDC_K84_pos
      R8_opam=R0_elam*R8_imam
C 1762-70-0014_log.fgi( 297, 239):����������� ��������
      R8_upam=R0_ilam*R8_omam
C 1762-70-0014_log.fgi( 297, 245):����������� ��������
      R8_eram=R0_olam*R8_apam
C 1762-70-0014_log.fgi( 297, 251):����������� ��������
      R8_iram=R0_ulam*R8_epam
C 1762-70-0014_log.fgi( 297, 257):����������� ��������
      R8_aram=R0_amam*R8_umam
C 1762-70-0014_log.fgi( 297, 263):����������� ��������
      R8_oram=R0_emam*R8_ipam
C 1762-70-0014_log.fgi( 297, 269):����������� ��������
      R_(6) = -1.0
C 1762-70-0014_log.fgi(  36,  57):��������� (RE4) (�������)
      R_(5) = 1.0
C 1762-70-0014_log.fgi(  36,  55):��������� (RE4) (�������)
      L_(506)=.false.
C 1762-70-0014_log.fgi(  26,  47):��������� ���������� (�������)
      L_(505)=.false.
C 1762-70-0014_log.fgi(  26,  51):��������� ���������� (�������)
      L0_avam=R0_otam.ne.R0_itam
      R0_itam=R0_otam
C 1762-70-0014_log.fgi(  17,  49):���������� ������������� ������
      if(L_(505).or.L_(506)) then
         L0_evam=(L_(505).or.L0_evam).and..not.(L_(506))
      else
         if(L0_avam.and..not.L0_utam) L0_evam=.not.L0_evam
      endif
      L0_utam=L0_avam
      L_(507)=.not.L0_evam
C 1762-70-0014_log.fgi(  32,  49):T �������
      if(L0_evam) then
         R_(4)=R_(5)
      else
         R_(4)=R_(6)
      endif
C 1762-70-0014_log.fgi(  39,  56):���� RE IN LO CH7
      R8_usam=R8_usam+deltat/R0_osam*R_(4)
      if(R8_usam.gt.R0_isam) then
         R8_usam=R0_isam
      elseif(R8_usam.lt.R0_atam) then
         R8_usam=R0_atam
      endif
C 1762-70-0014_log.fgi(  49,  55):����������
      R8_esam=R8_usam
C 1762-70-0014_log.fgi(  90,  57):������,20FDC_K427_pos
      R_(9) = -1.0
C 1762-70-0014_log.fgi(  36,  80):��������� (RE4) (�������)
      R_(8) = 1.0
C 1762-70-0014_log.fgi(  36,  78):��������� (RE4) (�������)
      L_(509)=.false.
C 1762-70-0014_log.fgi(  26,  70):��������� ���������� (�������)
      L_(508)=.false.
C 1762-70-0014_log.fgi(  26,  74):��������� ���������� (�������)
      L0_obem=R0_ebem.ne.R0_abem
      R0_abem=R0_ebem
C 1762-70-0014_log.fgi(  17,  72):���������� ������������� ������
      if(L_(508).or.L_(509)) then
         L0_ubem=(L_(508).or.L0_ubem).and..not.(L_(509))
      else
         if(L0_obem.and..not.L0_ibem) L0_ubem=.not.L0_ubem
      endif
      L0_ibem=L0_obem
      L_(510)=.not.L0_ubem
C 1762-70-0014_log.fgi(  32,  72):T �������
      if(L0_ubem) then
         R_(7)=R_(8)
      else
         R_(7)=R_(9)
      endif
C 1762-70-0014_log.fgi(  39,  79):���� RE IN LO CH7
      R8_ixam=R8_ixam+deltat/R0_exam*R_(7)
      if(R8_ixam.gt.R0_axam) then
         R8_ixam=R0_axam
      elseif(R8_ixam.lt.R0_oxam) then
         R8_ixam=R0_oxam
      endif
C 1762-70-0014_log.fgi(  49,  78):����������
      R8_uvam=R8_ixam
C 1762-70-0014_log.fgi(  90,  80):������,20FDC_K426_pos
      R_(12) = -1.0
C 1762-70-0014_log.fgi(  36, 103):��������� (RE4) (�������)
      R_(11) = 1.0
C 1762-70-0014_log.fgi(  36, 101):��������� (RE4) (�������)
      L_(512)=.false.
C 1762-70-0014_log.fgi(  26,  93):��������� ���������� (�������)
      L_(511)=.false.
C 1762-70-0014_log.fgi(  26,  97):��������� ���������� (�������)
      L0_ekem=R0_ufem.ne.R0_ofem
      R0_ofem=R0_ufem
C 1762-70-0014_log.fgi(  17,  95):���������� ������������� ������
      if(L_(511).or.L_(512)) then
         L0_ikem=(L_(511).or.L0_ikem).and..not.(L_(512))
      else
         if(L0_ekem.and..not.L0_akem) L0_ikem=.not.L0_ikem
      endif
      L0_akem=L0_ekem
      L_(513)=.not.L0_ikem
C 1762-70-0014_log.fgi(  32,  95):T �������
      if(L0_ikem) then
         R_(10)=R_(11)
      else
         R_(10)=R_(12)
      endif
C 1762-70-0014_log.fgi(  39, 102):���� RE IN LO CH7
      R8_afem=R8_afem+deltat/R0_udem*R_(10)
      if(R8_afem.gt.R0_odem) then
         R8_afem=R0_odem
      elseif(R8_afem.lt.R0_efem) then
         R8_afem=R0_efem
      endif
C 1762-70-0014_log.fgi(  49, 101):����������
      R8_idem=R8_afem
C 1762-70-0014_log.fgi(  90, 103):������,20FDC_K435_pos
      R_(15) = -1.0
C 1762-70-0014_log.fgi(  36, 126):��������� (RE4) (�������)
      R_(14) = 1.0
C 1762-70-0014_log.fgi(  36, 124):��������� (RE4) (�������)
      L_(515)=.false.
C 1762-70-0014_log.fgi(  26, 116):��������� ���������� (�������)
      L_(514)=.false.
C 1762-70-0014_log.fgi(  26, 120):��������� ���������� (�������)
      L0_umem=R0_imem.ne.R0_emem
      R0_emem=R0_imem
C 1762-70-0014_log.fgi(  17, 118):���������� ������������� ������
      if(L_(514).or.L_(515)) then
         L0_apem=(L_(514).or.L0_apem).and..not.(L_(515))
      else
         if(L0_umem.and..not.L0_omem) L0_apem=.not.L0_apem
      endif
      L0_omem=L0_umem
      L_(516)=.not.L0_apem
C 1762-70-0014_log.fgi(  32, 118):T �������
      if(L0_apem) then
         R_(13)=R_(14)
      else
         R_(13)=R_(15)
      endif
C 1762-70-0014_log.fgi(  39, 125):���� RE IN LO CH7
      R8_olem=R8_olem+deltat/R0_ilem*R_(13)
      if(R8_olem.gt.R0_elem) then
         R8_olem=R0_elem
      elseif(R8_olem.lt.R0_ulem) then
         R8_olem=R0_ulem
      endif
C 1762-70-0014_log.fgi(  49, 124):����������
      R8_alem=R8_olem
C 1762-70-0014_log.fgi(  90, 126):������,20FDC_K437_pos
      R_(18) = -1.0
C 1762-70-0014_log.fgi(  36, 148):��������� (RE4) (�������)
      R_(17) = 1.0
C 1762-70-0014_log.fgi(  36, 146):��������� (RE4) (�������)
      L_(518)=.false.
C 1762-70-0014_log.fgi(  26, 138):��������� ���������� (�������)
      L_(517)=.false.
C 1762-70-0014_log.fgi(  26, 142):��������� ���������� (�������)
      L0_isem=R0_asem.ne.R0_urem
      R0_urem=R0_asem
C 1762-70-0014_log.fgi(  17, 140):���������� ������������� ������
      if(L_(517).or.L_(518)) then
         L0_osem=(L_(517).or.L0_osem).and..not.(L_(518))
      else
         if(L0_isem.and..not.L0_esem) L0_osem=.not.L0_osem
      endif
      L0_esem=L0_isem
      L_(519)=.not.L0_osem
C 1762-70-0014_log.fgi(  32, 140):T �������
      if(L0_osem) then
         R_(16)=R_(17)
      else
         R_(16)=R_(18)
      endif
C 1762-70-0014_log.fgi(  39, 147):���� RE IN LO CH7
      R8_erem=R8_erem+deltat/R0_arem*R_(16)
      if(R8_erem.gt.R0_upem) then
         R8_erem=R0_upem
      elseif(R8_erem.lt.R0_irem) then
         R8_erem=R0_irem
      endif
C 1762-70-0014_log.fgi(  49, 146):����������
      R8_opem=R8_erem
C 1762-70-0014_log.fgi(  90, 148):������,20FDC_K436_pos
      R_(21) = -1.0
C 1762-70-0014_log.fgi(  36, 171):��������� (RE4) (�������)
      R_(20) = 1.0
C 1762-70-0014_log.fgi(  36, 169):��������� (RE4) (�������)
      L_(521)=.false.
C 1762-70-0014_log.fgi(  26, 161):��������� ���������� (�������)
      L_(520)=.false.
C 1762-70-0014_log.fgi(  26, 165):��������� ���������� (�������)
      L0_axem=R0_ovem.ne.R0_ivem
      R0_ivem=R0_ovem
C 1762-70-0014_log.fgi(  17, 163):���������� ������������� ������
      if(L_(520).or.L_(521)) then
         L0_exem=(L_(520).or.L0_exem).and..not.(L_(521))
      else
         if(L0_axem.and..not.L0_uvem) L0_exem=.not.L0_exem
      endif
      L0_uvem=L0_axem
      L_(522)=.not.L0_exem
C 1762-70-0014_log.fgi(  32, 163):T �������
      if(L0_exem) then
         R_(19)=R_(20)
      else
         R_(19)=R_(21)
      endif
C 1762-70-0014_log.fgi(  39, 170):���� RE IN LO CH7
      R8_utem=R8_utem+deltat/R0_otem*R_(19)
      if(R8_utem.gt.R0_item) then
         R8_utem=R0_item
      elseif(R8_utem.lt.R0_avem) then
         R8_utem=R0_avem
      endif
C 1762-70-0014_log.fgi(  49, 169):����������
      R8_etem=R8_utem
C 1762-70-0014_log.fgi(  90, 171):������,20FDC_K438_pos
      R_(24) = -1.0
C 1762-70-0014_log.fgi(  36, 194):��������� (RE4) (�������)
      R_(23) = 1.0
C 1762-70-0014_log.fgi(  36, 192):��������� (RE4) (�������)
      L_(524)=.false.
C 1762-70-0014_log.fgi(  26, 184):��������� ���������� (�������)
      L_(523)=.false.
C 1762-70-0014_log.fgi(  26, 188):��������� ���������� (�������)
      L0_odim=R0_edim.ne.R0_adim
      R0_adim=R0_edim
C 1762-70-0014_log.fgi(  17, 186):���������� ������������� ������
      if(L_(523).or.L_(524)) then
         L0_udim=(L_(523).or.L0_udim).and..not.(L_(524))
      else
         if(L0_odim.and..not.L0_idim) L0_udim=.not.L0_udim
      endif
      L0_idim=L0_odim
      L_(525)=.not.L0_udim
C 1762-70-0014_log.fgi(  32, 186):T �������
      if(L0_udim) then
         R_(22)=R_(23)
      else
         R_(22)=R_(24)
      endif
C 1762-70-0014_log.fgi(  39, 193):���� RE IN LO CH7
      R8_ibim=R8_ibim+deltat/R0_ebim*R_(22)
      if(R8_ibim.gt.R0_abim) then
         R8_ibim=R0_abim
      elseif(R8_ibim.lt.R0_obim) then
         R8_ibim=R0_obim
      endif
C 1762-70-0014_log.fgi(  49, 192):����������
      R8_uxem=R8_ibim
C 1762-70-0014_log.fgi(  90, 194):������,20FDC_K525_pos
      R_(27) = -1.0
C 1762-70-0014_log.fgi(  36, 217):��������� (RE4) (�������)
      R_(26) = 1.0
C 1762-70-0014_log.fgi(  36, 215):��������� (RE4) (�������)
      L_(527)=.false.
C 1762-70-0014_log.fgi(  26, 207):��������� ���������� (�������)
      L_(526)=.false.
C 1762-70-0014_log.fgi(  26, 211):��������� ���������� (�������)
      L0_elim=R0_ukim.ne.R0_okim
      R0_okim=R0_ukim
C 1762-70-0014_log.fgi(  17, 209):���������� ������������� ������
      if(L_(526).or.L_(527)) then
         L0_ilim=(L_(526).or.L0_ilim).and..not.(L_(527))
      else
         if(L0_elim.and..not.L0_alim) L0_ilim=.not.L0_ilim
      endif
      L0_alim=L0_elim
      L_(528)=.not.L0_ilim
C 1762-70-0014_log.fgi(  32, 209):T �������
      if(L0_ilim) then
         R_(25)=R_(26)
      else
         R_(25)=R_(27)
      endif
C 1762-70-0014_log.fgi(  39, 216):���� RE IN LO CH7
      R8_akim=R8_akim+deltat/R0_ufim*R_(25)
      if(R8_akim.gt.R0_ofim) then
         R8_akim=R0_ofim
      elseif(R8_akim.lt.R0_ekim) then
         R8_akim=R0_ekim
      endif
C 1762-70-0014_log.fgi(  49, 215):����������
      R8_ifim=R8_akim
C 1762-70-0014_log.fgi(  90, 217):������,20FDC_K524_pos
      R_(30) = -1.0
C 1762-70-0014_log.fgi(  36, 241):��������� (RE4) (�������)
      R_(29) = 1.0
C 1762-70-0014_log.fgi(  36, 239):��������� (RE4) (�������)
      L_(530)=.false.
C 1762-70-0014_log.fgi(  26, 231):��������� ���������� (�������)
      L_(529)=.false.
C 1762-70-0014_log.fgi(  26, 235):��������� ���������� (�������)
      L0_upim=R0_ipim.ne.R0_epim
      R0_epim=R0_ipim
C 1762-70-0014_log.fgi(  17, 233):���������� ������������� ������
      if(L_(529).or.L_(530)) then
         L0_arim=(L_(529).or.L0_arim).and..not.(L_(530))
      else
         if(L0_upim.and..not.L0_opim) L0_arim=.not.L0_arim
      endif
      L0_opim=L0_upim
      L_(531)=.not.L0_arim
C 1762-70-0014_log.fgi(  32, 233):T �������
      if(L0_arim) then
         R_(28)=R_(29)
      else
         R_(28)=R_(30)
      endif
C 1762-70-0014_log.fgi(  39, 240):���� RE IN LO CH7
      R8_omim=R8_omim+deltat/R0_imim*R_(28)
      if(R8_omim.gt.R0_emim) then
         R8_omim=R0_emim
      elseif(R8_omim.lt.R0_umim) then
         R8_omim=R0_umim
      endif
C 1762-70-0014_log.fgi(  49, 239):����������
      R8_amim=R8_omim
C 1762-70-0014_log.fgi(  90, 241):������,20FDC_K523_pos
      !�����. �������� I0_urim = 1762-70-0014_logC?? /z'01000087'
C /
C 1762-70-0014_log.fgi( 116, 287):��������� �����
      !�����. �������� I0_erim = 1762-70-0014_logC?? /z'0100000A'
C /
C 1762-70-0014_log.fgi( 142, 284):��������� �����
      !�����. �������� I0_orim = 1762-70-0014_logC?? /z'01000007'
C /
C 1762-70-0014_log.fgi( 116, 285):��������� �����
      !��������� R_(31) = 1762-70-0014_logC?? /0.03/
      R_(31)=R0_asim
C 1762-70-0014_log.fgi(  48, 275):���������
      L_ulim=R8_omim.lt.R_(31)
C 1762-70-0014_log.fgi(  74, 227):���������� <
      L_efim=R8_akim.lt.R_(31)
C 1762-70-0014_log.fgi(  74, 203):���������� <
      L_oxem=R8_ibim.lt.R_(31)
C 1762-70-0014_log.fgi(  74, 180):���������� <
      L_atem=R8_utem.lt.R_(31)
C 1762-70-0014_log.fgi(  74, 157):���������� <
      L_ipem=R8_erem.lt.R_(31)
C 1762-70-0014_log.fgi(  74, 134):���������� <
      L_ukem=R8_olem.lt.R_(31)
C 1762-70-0014_log.fgi(  74, 112):���������� <
      L_edem=R8_afem.lt.R_(31)
C 1762-70-0014_log.fgi(  74,  89):���������� <
      L_ovam=R8_ixam.lt.R_(31)
C 1762-70-0014_log.fgi(  74,  66):���������� <
      L_asam=R8_usam.lt.R_(31)
C 1762-70-0014_log.fgi(  74,  43):���������� <
      L_udam=R8_ofam.lt.R_(31)
C 1762-70-0014_log.fgi(  74, 252):���������� <
      !��������� R_(32) = 1762-70-0014_logC?? /0.97/
      R_(32)=R0_esim
C 1762-70-0014_log.fgi(  48, 279):���������
      L_apim=R8_omim.gt.R_(32)
C 1762-70-0014_log.fgi(  74, 231):���������� >
      if(L_apim) then
         I_(12)=I0_orim
      else
         I_(12)=I0_urim
      endif
C 1762-70-0014_log.fgi( 129, 236):���� RE IN LO CH7
      if(L_ulim) then
         I_olim=I0_erim
      else
         I_olim=I_(12)
      endif
C 1762-70-0014_log.fgi( 152, 235):���� RE IN LO CH7
      L_ikim=R8_akim.gt.R_(32)
C 1762-70-0014_log.fgi(  74, 207):���������� >
      if(L_ikim) then
         I_(11)=I0_orim
      else
         I_(11)=I0_urim
      endif
C 1762-70-0014_log.fgi( 129, 212):���� RE IN LO CH7
      if(L_efim) then
         I_afim=I0_erim
      else
         I_afim=I_(11)
      endif
C 1762-70-0014_log.fgi( 152, 211):���� RE IN LO CH7
      L_ubim=R8_ibim.gt.R_(32)
C 1762-70-0014_log.fgi(  74, 184):���������� >
      if(L_ubim) then
         I_(10)=I0_orim
      else
         I_(10)=I0_urim
      endif
C 1762-70-0014_log.fgi( 129, 189):���� RE IN LO CH7
      if(L_oxem) then
         I_ixem=I0_erim
      else
         I_ixem=I_(10)
      endif
C 1762-70-0014_log.fgi( 152, 188):���� RE IN LO CH7
      L_evem=R8_utem.gt.R_(32)
C 1762-70-0014_log.fgi(  74, 161):���������� >
      if(L_evem) then
         I_(9)=I0_orim
      else
         I_(9)=I0_urim
      endif
C 1762-70-0014_log.fgi( 129, 166):���� RE IN LO CH7
      if(L_atem) then
         I_usem=I0_erim
      else
         I_usem=I_(9)
      endif
C 1762-70-0014_log.fgi( 152, 165):���� RE IN LO CH7
      L_orem=R8_erem.gt.R_(32)
C 1762-70-0014_log.fgi(  74, 138):���������� >
      if(L_orem) then
         I_(8)=I0_orim
      else
         I_(8)=I0_urim
      endif
C 1762-70-0014_log.fgi( 129, 143):���� RE IN LO CH7
      if(L_ipem) then
         I_epem=I0_erim
      else
         I_epem=I_(8)
      endif
C 1762-70-0014_log.fgi( 152, 142):���� RE IN LO CH7
      L_amem=R8_olem.gt.R_(32)
C 1762-70-0014_log.fgi(  74, 116):���������� >
      if(L_amem) then
         I_(7)=I0_orim
      else
         I_(7)=I0_urim
      endif
C 1762-70-0014_log.fgi( 129, 121):���� RE IN LO CH7
      if(L_ukem) then
         I_okem=I0_erim
      else
         I_okem=I_(7)
      endif
C 1762-70-0014_log.fgi( 152, 120):���� RE IN LO CH7
      L_ifem=R8_afem.gt.R_(32)
C 1762-70-0014_log.fgi(  74,  93):���������� >
      if(L_ifem) then
         I_(6)=I0_orim
      else
         I_(6)=I0_urim
      endif
C 1762-70-0014_log.fgi( 129,  98):���� RE IN LO CH7
      if(L_edem) then
         I_adem=I0_erim
      else
         I_adem=I_(6)
      endif
C 1762-70-0014_log.fgi( 152,  97):���� RE IN LO CH7
      L_uxam=R8_ixam.gt.R_(32)
C 1762-70-0014_log.fgi(  74,  70):���������� >
      if(L_uxam) then
         I_(5)=I0_orim
      else
         I_(5)=I0_urim
      endif
C 1762-70-0014_log.fgi( 129,  75):���� RE IN LO CH7
      if(L_ovam) then
         I_ivam=I0_erim
      else
         I_ivam=I_(5)
      endif
C 1762-70-0014_log.fgi( 152,  74):���� RE IN LO CH7
      L_etam=R8_usam.gt.R_(32)
C 1762-70-0014_log.fgi(  74,  47):���������� >
      if(L_etam) then
         I_(4)=I0_orim
      else
         I_(4)=I0_urim
      endif
C 1762-70-0014_log.fgi( 129,  52):���� RE IN LO CH7
      if(L_asam) then
         I_uram=I0_erim
      else
         I_uram=I_(4)
      endif
C 1762-70-0014_log.fgi( 152,  51):���� RE IN LO CH7
      L_akam=R8_ofam.gt.R_(32)
C 1762-70-0014_log.fgi(  74, 256):���������� >
      if(L_akam) then
         I_(3)=I0_orim
      else
         I_(3)=I0_urim
      endif
C 1762-70-0014_log.fgi( 129, 261):���� RE IN LO CH7
      if(L_udam) then
         I_odam=I0_erim
      else
         I_odam=I_(3)
      endif
C 1762-70-0014_log.fgi( 152, 260):���� RE IN LO CH7
      R_(35) = -1.0
C 1762-70-0014_log.fgi(  36, 290):��������� (RE4) (�������)
      R_(34) = 1.0
C 1762-70-0014_log.fgi(  36, 288):��������� (RE4) (�������)
      L_(534)=.false.
C 1762-70-0014_log.fgi(  26, 280):��������� ���������� (�������)
      L_(533)=.false.
C 1762-70-0014_log.fgi(  26, 284):��������� ���������� (�������)
      L0_ivim=R0_avim.ne.R0_utim
      R0_utim=R0_avim
C 1762-70-0014_log.fgi(  17, 282):���������� ������������� ������
      if(L_(533).or.L_(534)) then
         L0_ovim=(L_(533).or.L0_ovim).and..not.(L_(534))
      else
         if(L0_ivim.and..not.L0_evim) L0_ovim=.not.L0_ovim
      endif
      L0_evim=L0_ivim
      L_(535)=.not.L0_ovim
C 1762-70-0014_log.fgi(  32, 282):T �������
      if(L0_ovim) then
         R_(33)=R_(34)
      else
         R_(33)=R_(35)
      endif
C 1762-70-0014_log.fgi(  39, 289):���� RE IN LO CH7
      R8_etim=R8_etim+deltat/R0_atim*R_(33)
      if(R8_etim.gt.R0_usim) then
         R8_etim=R0_usim
      elseif(R8_etim.lt.R0_itim) then
         R8_etim=R0_itim
      endif
C 1762-70-0014_log.fgi(  49, 288):����������
      R8_osim=R8_etim
C 1762-70-0014_log.fgi(  90, 290):������,20FDC_K83_pos
      L_isim=R8_etim.lt.R_(31)
C 1762-70-0014_log.fgi(  74, 276):���������� <
      L_otim=R8_etim.gt.R_(32)
C 1762-70-0014_log.fgi(  74, 280):���������� >
      if(L_otim) then
         I_(13)=I0_orim
      else
         I_(13)=I0_urim
      endif
C 1762-70-0014_log.fgi( 129, 285):���� RE IN LO CH7
      if(L_isim) then
         I_irim=I0_erim
      else
         I_irim=I_(13)
      endif
C 1762-70-0014_log.fgi( 152, 284):���� RE IN LO CH7
      I_(14) = 6842472
C 1762-12-0001_log.fgi( 356, 146):��������� ������������� IN (�������)
      !�����. �������� I0_otum = 1762-12-0001_logC?? /z'01000016'
C /
C 1762-12-0001_log.fgi( 355, 138):��������� �����
      !�����. �������� I0_evum = 1762-12-0001_logC?? /z'01000012'
C /
C 1762-12-0001_log.fgi( 353, 157):��������� �����
      !�����. �������� I0_ivum = 1762-12-0001_logC?? /z'01000003'
C /
C 1762-12-0001_log.fgi( 353, 159):��������� �����
      !�����. �������� I0_ovum = 1762-12-0001_logC?? /z'01000007'
C /
C 1762-12-0001_log.fgi( 353, 169):��������� �����
      !�����. �������� I0_uvum = 1762-12-0001_logC?? /z'01000003'
C /
C 1762-12-0001_log.fgi( 353, 171):��������� �����
      R0_exum=R0_exum+deltat
      if(R0_exum.ge.R0_ixum) R0_exum=0.0
      L_(576)=R0_exum.le.R0_axum.and.R0_exum.gt.0.0
C 1762-12-0001_log.fgi( 355, 270):�������������� ���������
      if(L_(576)) then
         I_oxep=I0_urep
      else
         I_oxep=I0_asep
      endif
C 1762-12-0001_log.fgi( 365, 279):���� RE IN LO CH7
      I_ixep=I_oxep
C 1762-12-0001_log.fgi( 395, 280):������,1762-12-0001_ellipce1_color
      I_exep=I_oxep
C 1762-12-0001_log.fgi( 395, 276):������,1762-12-0001_ellipce2_color
      I_axep=I_oxep
C 1762-12-0001_log.fgi( 395, 272):������,1762-12-0001_ellipce3_color
      I_uvep=I_oxep
C 1762-12-0001_log.fgi( 395, 268):������,1762-12-0001_ellipce4_color
      I_ovep=I_oxep
C 1762-12-0001_log.fgi( 395, 264):������,1762-12-0001_ellipce5_color
      I_ivep=I_oxep
C 1762-12-0001_log.fgi( 395, 260):������,1762-12-0001_ellipce6_color
      I_evep=I_oxep
C 1762-12-0001_log.fgi( 395, 256):������,1762-12-0001_ellipce7_color
      I_avep=I_oxep
C 1762-12-0001_log.fgi( 395, 252):������,1762-12-0001_ellipce8_color
      I_utep=I_oxep
C 1762-12-0001_log.fgi( 395, 248):������,1762-12-0001_ellipce9_color
      I_otep=I_oxep
C 1762-12-0001_log.fgi( 395, 244):������,1762-12-0001_ellipce10_color
      I_itep=I_oxep
C 1762-12-0001_log.fgi( 395, 240):������,1762-12-0001_ellipce11_color
      I_etep=I_oxep
C 1762-12-0001_log.fgi( 395, 236):������,1762-12-0001_ellipce12_color
      I_atep=I_oxep
C 1762-12-0001_log.fgi( 395, 226):������,1762-12-0001_poly1_color
      I_isep=I_oxep
C 1762-12-0001_log.fgi( 395, 222):������,1762-12-0001_poly3_color
      I_esep=I_oxep
C 1762-12-0001_log.fgi( 395, 218):������,1762-12-0001_poly4_color
      I_orep=I_oxep
C 1762-12-0001_log.fgi( 395, 214):������,1762-12-0001_poly5_color
      I_irep=I_oxep
C 1762-12-0001_log.fgi( 395, 210):������,1762-12-0001_poly6_color
      I_erep=I_oxep
C 1762-12-0001_log.fgi( 395, 206):������,1762-12-0001_poly7_color
      I_arep=I_oxep
C 1762-12-0001_log.fgi( 395, 202):������,1762-12-0001_poly8_color
      I_upep=I_oxep
C 1762-12-0001_log.fgi( 395, 198):������,1762-12-0001_poly9_color
      I_ipep=I_oxep
C 1762-12-0001_log.fgi( 395, 194):������,1762-12-0001_poly11_color
      I_abap=I_oxep
C 1762-12-0001_log.fgi( 395, 190):������,1762-12-0001_poly12_color
      I_uxum=I_oxep
C 1762-12-0001_log.fgi( 395, 186):������,1762-12-0001_poly13_color
      I_oxum=I_oxep
C 1762-12-0001_log.fgi( 395, 182):������,1762-12-0001_poly15_color
      if(L_(576)) then
         I_usep=I0_ovum
      else
         I_usep=I0_uvum
      endif
C 1762-12-0001_log.fgi( 365, 169):���� RE IN LO CH7
      I_osep=I_usep
C 1762-12-0001_log.fgi( 395, 170):������,1762-12-0001_poly2_color
      if(L_(576)) then
         I_opep=I0_evum
      else
         I_opep=I0_ivum
      endif
C 1762-12-0001_log.fgi( 365, 157):���� RE IN LO CH7
      if(L_(576)) then
         I_utum=I_(14)
      else
         I_utum=I0_otum
      endif
C 1762-12-0001_log.fgi( 365, 146):���� RE IN LO CH7
      if(L_(576)) then
         I_avum=I0_otum
      else
         I_avum=I_(14)
      endif
C 1762-12-0001_log.fgi( 365, 138):���� RE IN LO CH7
      L0_edap=R0_ibap.ne.R0_ebap
      R0_ebap=R0_ibap
C 1762-12-0001_log.fgi( 170, 108):���������� ������������� ������
      L_(537)=.false.
C 1762-12-0001_log.fgi( 183, 106):��������� ���������� (�������)
      L_(536)=.false.
C 1762-12-0001_log.fgi( 183, 110):��������� ���������� (�������)
      if(L_(536).or.L_(537)) then
         L_idap=(L_(536).or.L_idap).and..not.(L_(537))
      else
         if(L0_edap.and..not.L0_adap) L_idap=.not.L_idap
      endif
      L0_adap=L0_edap
      L_(538)=.not.L_idap
C 1762-12-0001_log.fgi( 189, 108):T �������
      L_obap=L_idap
C 1762-12-0001_log.fgi( 220, 106):������,1762-12-0001_lamp27
      if(L_idap) then
         I_ubap=1
      else
         I_ubap=0
      endif
C 1762-12-0001_log.fgi( 205, 110):��������� LO->1
      L0_ofap=R0_udap.ne.R0_odap
      R0_odap=R0_udap
C 1762-12-0001_log.fgi( 170, 121):���������� ������������� ������
      L_(540)=.false.
C 1762-12-0001_log.fgi( 183, 119):��������� ���������� (�������)
      L_(539)=.false.
C 1762-12-0001_log.fgi( 183, 123):��������� ���������� (�������)
      if(L_(539).or.L_(540)) then
         L_ufap=(L_(539).or.L_ufap).and..not.(L_(540))
      else
         if(L0_ofap.and..not.L0_ifap) L_ufap=.not.L_ufap
      endif
      L0_ifap=L0_ofap
      L_(541)=.not.L_ufap
C 1762-12-0001_log.fgi( 189, 121):T �������
      L_afap=L_ufap
C 1762-12-0001_log.fgi( 220, 119):������,1762-12-0001_lamp26
      if(L_ufap) then
         I_efap=1
      else
         I_efap=0
      endif
C 1762-12-0001_log.fgi( 205, 123):��������� LO->1
      L0_alap=R0_ekap.ne.R0_akap
      R0_akap=R0_ekap
C 1762-12-0001_log.fgi( 170, 134):���������� ������������� ������
      L_(543)=.false.
C 1762-12-0001_log.fgi( 183, 132):��������� ���������� (�������)
      L_(542)=.false.
C 1762-12-0001_log.fgi( 183, 136):��������� ���������� (�������)
      if(L_(542).or.L_(543)) then
         L_elap=(L_(542).or.L_elap).and..not.(L_(543))
      else
         if(L0_alap.and..not.L0_ukap) L_elap=.not.L_elap
      endif
      L0_ukap=L0_alap
      L_(544)=.not.L_elap
C 1762-12-0001_log.fgi( 189, 134):T �������
      L_ikap=L_elap
C 1762-12-0001_log.fgi( 220, 132):������,1762-12-0001_lamp25
      if(L_elap) then
         I_okap=1
      else
         I_okap=0
      endif
C 1762-12-0001_log.fgi( 205, 136):��������� LO->1
      L0_imap=R0_olap.ne.R0_ilap
      R0_ilap=R0_olap
C 1762-12-0001_log.fgi( 170, 147):���������� ������������� ������
      L_(546)=.false.
C 1762-12-0001_log.fgi( 183, 145):��������� ���������� (�������)
      L_(545)=.false.
C 1762-12-0001_log.fgi( 183, 149):��������� ���������� (�������)
      if(L_(545).or.L_(546)) then
         L_omap=(L_(545).or.L_omap).and..not.(L_(546))
      else
         if(L0_imap.and..not.L0_emap) L_omap=.not.L_omap
      endif
      L0_emap=L0_imap
      L_(547)=.not.L_omap
C 1762-12-0001_log.fgi( 189, 147):T �������
      L_ulap=L_omap
C 1762-12-0001_log.fgi( 220, 145):������,1762-12-0001_lamp24
      if(L_omap) then
         I_amap=1
      else
         I_amap=0
      endif
C 1762-12-0001_log.fgi( 205, 149):��������� LO->1
      L0_upap=R0_apap.ne.R0_umap
      R0_umap=R0_apap
C 1762-12-0001_log.fgi( 170, 160):���������� ������������� ������
      L_(549)=.false.
C 1762-12-0001_log.fgi( 183, 158):��������� ���������� (�������)
      L_(548)=.false.
C 1762-12-0001_log.fgi( 183, 162):��������� ���������� (�������)
      if(L_(548).or.L_(549)) then
         L_arap=(L_(548).or.L_arap).and..not.(L_(549))
      else
         if(L0_upap.and..not.L0_opap) L_arap=.not.L_arap
      endif
      L0_opap=L0_upap
      L_(550)=.not.L_arap
C 1762-12-0001_log.fgi( 189, 160):T �������
      L_epap=L_arap
C 1762-12-0001_log.fgi( 220, 158):������,1762-12-0001_lamp23
      if(L_arap) then
         I_ipap=1
      else
         I_ipap=0
      endif
C 1762-12-0001_log.fgi( 205, 162):��������� LO->1
      L0_esap=R0_irap.ne.R0_erap
      R0_erap=R0_irap
C 1762-12-0001_log.fgi( 170, 173):���������� ������������� ������
      L_(552)=.false.
C 1762-12-0001_log.fgi( 183, 171):��������� ���������� (�������)
      L_(551)=.false.
C 1762-12-0001_log.fgi( 183, 175):��������� ���������� (�������)
      if(L_(551).or.L_(552)) then
         L_isap=(L_(551).or.L_isap).and..not.(L_(552))
      else
         if(L0_esap.and..not.L0_asap) L_isap=.not.L_isap
      endif
      L0_asap=L0_esap
      L_(553)=.not.L_isap
C 1762-12-0001_log.fgi( 189, 173):T �������
      L_orap=L_isap
C 1762-12-0001_log.fgi( 220, 171):������,1762-12-0001_lamp22
      if(L_isap) then
         I_urap=1
      else
         I_urap=0
      endif
C 1762-12-0001_log.fgi( 205, 175):��������� LO->1
      L0_otap=R0_usap.ne.R0_osap
      R0_osap=R0_usap
C 1762-12-0001_log.fgi( 170, 186):���������� ������������� ������
      L_(555)=.false.
C 1762-12-0001_log.fgi( 183, 184):��������� ���������� (�������)
      L_(554)=.false.
C 1762-12-0001_log.fgi( 183, 188):��������� ���������� (�������)
      if(L_(554).or.L_(555)) then
         L_utap=(L_(554).or.L_utap).and..not.(L_(555))
      else
         if(L0_otap.and..not.L0_itap) L_utap=.not.L_utap
      endif
      L0_itap=L0_otap
      L_(556)=.not.L_utap
C 1762-12-0001_log.fgi( 189, 186):T �������
      L_atap=L_utap
C 1762-12-0001_log.fgi( 220, 184):������,1762-12-0001_lamp21
      if(L_utap) then
         I_etap=1
      else
         I_etap=0
      endif
C 1762-12-0001_log.fgi( 205, 188):��������� LO->1
      L0_axap=R0_evap.ne.R0_avap
      R0_avap=R0_evap
C 1762-12-0001_log.fgi( 170, 199):���������� ������������� ������
      L_(558)=.false.
C 1762-12-0001_log.fgi( 183, 197):��������� ���������� (�������)
      L_(557)=.false.
C 1762-12-0001_log.fgi( 183, 201):��������� ���������� (�������)
      if(L_(557).or.L_(558)) then
         L_exap=(L_(557).or.L_exap).and..not.(L_(558))
      else
         if(L0_axap.and..not.L0_uvap) L_exap=.not.L_exap
      endif
      L0_uvap=L0_axap
      L_(559)=.not.L_exap
C 1762-12-0001_log.fgi( 189, 199):T �������
      L_ivap=L_exap
C 1762-12-0001_log.fgi( 220, 197):������,1762-12-0001_lamp20
      if(L_exap) then
         I_ovap=1
      else
         I_ovap=0
      endif
C 1762-12-0001_log.fgi( 205, 201):��������� LO->1
      L0_ibep=R0_oxap.ne.R0_ixap
      R0_ixap=R0_oxap
C 1762-12-0001_log.fgi( 170, 212):���������� ������������� ������
      L_(561)=.false.
C 1762-12-0001_log.fgi( 183, 210):��������� ���������� (�������)
      L_(560)=.false.
C 1762-12-0001_log.fgi( 183, 214):��������� ���������� (�������)
      if(L_(560).or.L_(561)) then
         L_obep=(L_(560).or.L_obep).and..not.(L_(561))
      else
         if(L0_ibep.and..not.L0_ebep) L_obep=.not.L_obep
      endif
      L0_ebep=L0_ibep
      L_(562)=.not.L_obep
C 1762-12-0001_log.fgi( 189, 212):T �������
      L_uxap=L_obep
C 1762-12-0001_log.fgi( 220, 210):������,1762-12-0001_lamp19
      if(L_obep) then
         I_abep=1
      else
         I_abep=0
      endif
C 1762-12-0001_log.fgi( 205, 214):��������� LO->1
      L0_udep=R0_adep.ne.R0_ubep
      R0_ubep=R0_adep
C 1762-12-0001_log.fgi( 170, 225):���������� ������������� ������
      L_(564)=.false.
C 1762-12-0001_log.fgi( 183, 223):��������� ���������� (�������)
      L_(563)=.false.
C 1762-12-0001_log.fgi( 183, 227):��������� ���������� (�������)
      if(L_(563).or.L_(564)) then
         L_afep=(L_(563).or.L_afep).and..not.(L_(564))
      else
         if(L0_udep.and..not.L0_odep) L_afep=.not.L_afep
      endif
      L0_odep=L0_udep
      L_(565)=.not.L_afep
C 1762-12-0001_log.fgi( 189, 225):T �������
      L_edep=L_afep
C 1762-12-0001_log.fgi( 220, 223):������,1762-12-0001_lamp18
      if(L_afep) then
         I_idep=1
      else
         I_idep=0
      endif
C 1762-12-0001_log.fgi( 205, 227):��������� LO->1
      L0_ekep=R0_ifep.ne.R0_efep
      R0_efep=R0_ifep
C 1762-12-0001_log.fgi( 170, 238):���������� ������������� ������
      L_(567)=.false.
C 1762-12-0001_log.fgi( 183, 236):��������� ���������� (�������)
      L_(566)=.false.
C 1762-12-0001_log.fgi( 183, 240):��������� ���������� (�������)
      if(L_(566).or.L_(567)) then
         L_ikep=(L_(566).or.L_ikep).and..not.(L_(567))
      else
         if(L0_ekep.and..not.L0_akep) L_ikep=.not.L_ikep
      endif
      L0_akep=L0_ekep
      L_(568)=.not.L_ikep
C 1762-12-0001_log.fgi( 189, 238):T �������
      L_ofep=L_ikep
C 1762-12-0001_log.fgi( 220, 236):������,1762-12-0001_lamp17
      if(L_ikep) then
         I_ufep=1
      else
         I_ufep=0
      endif
C 1762-12-0001_log.fgi( 205, 240):��������� LO->1
      L0_olep=R0_ukep.ne.R0_okep
      R0_okep=R0_ukep
C 1762-12-0001_log.fgi( 170, 251):���������� ������������� ������
      L_(570)=.false.
C 1762-12-0001_log.fgi( 183, 249):��������� ���������� (�������)
      L_(569)=.false.
C 1762-12-0001_log.fgi( 183, 253):��������� ���������� (�������)
      if(L_(569).or.L_(570)) then
         L_ulep=(L_(569).or.L_ulep).and..not.(L_(570))
      else
         if(L0_olep.and..not.L0_ilep) L_ulep=.not.L_ulep
      endif
      L0_ilep=L0_olep
      L_(571)=.not.L_ulep
C 1762-12-0001_log.fgi( 189, 251):T �������
      L_alep=L_ulep
C 1762-12-0001_log.fgi( 220, 249):������,1762-12-0001_lamp16
      if(L_ulep) then
         I_elep=1
      else
         I_elep=0
      endif
C 1762-12-0001_log.fgi( 205, 253):��������� LO->1
      L0_apep=R0_emep.ne.R0_amep
      R0_amep=R0_emep
C 1762-12-0001_log.fgi( 170, 264):���������� ������������� ������
      L_(573)=.false.
C 1762-12-0001_log.fgi( 183, 262):��������� ���������� (�������)
      L_(572)=.false.
C 1762-12-0001_log.fgi( 183, 266):��������� ���������� (�������)
      if(L_(572).or.L_(573)) then
         L_epep=(L_(572).or.L_epep).and..not.(L_(573))
      else
         if(L0_apep.and..not.L0_umep) L_epep=.not.L_epep
      endif
      L0_umep=L0_apep
      L_(574)=.not.L_epep
C 1762-12-0001_log.fgi( 189, 264):T �������
      L_imep=L_epep
C 1762-12-0001_log.fgi( 220, 262):������,1762-12-0001_lamp15
      if(L_epep) then
         I_omep=1
      else
         I_omep=0
      endif
C 1762-12-0001_log.fgi( 205, 266):��������� LO->1
      !�����. �������� I0_urep = 1762-12-0001_logC?? /z'0100000A'
C /
C 1762-12-0001_log.fgi( 353, 279):��������� �����
      !�����. �������� I0_asep = 1762-12-0001_logC?? /z'01000003'
C /
C 1762-12-0001_log.fgi( 353, 281):��������� �����
      L_(585)=.false.
C 1762-12-0001_log.fgi( 341,  88):��������� ���������� (�������)
      if(L_(585)) then
         I0_ifip=0
      endif
      I_(16)=I0_ifip
      L_(584)=I0_ifip.ne.0
C 1762-12-0001_log.fgi( 349,  90):���������� ��������� 
      L_(579) = (.NOT.L_(584))
C 1762-12-0001_log.fgi( 361,  95):���
      L_(583)=I_(16).eq.1
      L_(582)=I_(16).eq.2
      L_(581)=I_(16).eq.3
      L_(580)=I_(16).eq.4
C 1762-12-0001_log.fgi( 361,  90):���������� �� 4
      L0_avom=.false.
      I0_utom=0

      if(L0_upav)then
        I0_ufip=1
         L0_otom=.false.
         L0_imom=.false.
         L0_edom=.false.
         L0_obom=.false.
         L0_ibom=.false.
         L0_abom=.false.
         L0_oxim=.false.
         L0_exim=.false.
         L0_uvim=.false.
         L0_etom=.false.
         L0_usom=.false.
         L0_isom=.false.
         L0_asom=.false.
         L0_orom=.false.
         L0_erom=.false.
         L0_upom=.false.
         L0_ipom=.false.
         L0_apom=.false.
         L0_omom=.false.
         L0_amom=.false.
         L0_olom=.false.
         L0_elom=.false.
         L0_ukom=.false.
         L0_ikom=.false.
         L0_akom=.false.
         L0_ofom=.false.
         L0_efom=.false.
         L0_udom=.false.
         L0_idom=.false.
         L0_ubom=.false.
      endif

      if(L0_otom)I0_utom=I0_utom+1
      if(L0_imom)I0_utom=I0_utom+1
      if(L0_edom)I0_utom=I0_utom+1
      if(L0_obom)I0_utom=I0_utom+1
      if(L0_ibom)I0_utom=I0_utom+1
      if(L0_abom)I0_utom=I0_utom+1
      if(L0_oxim)I0_utom=I0_utom+1
      if(L0_exim)I0_utom=I0_utom+1
      if(L0_uvim)I0_utom=I0_utom+1
      if(L0_etom)I0_utom=I0_utom+1
      if(L0_usom)I0_utom=I0_utom+1
      if(L0_isom)I0_utom=I0_utom+1
      if(L0_asom)I0_utom=I0_utom+1
      if(L0_orom)I0_utom=I0_utom+1
      if(L0_erom)I0_utom=I0_utom+1
      if(L0_upom)I0_utom=I0_utom+1
      if(L0_ipom)I0_utom=I0_utom+1
      if(L0_apom)I0_utom=I0_utom+1
      if(L0_omom)I0_utom=I0_utom+1
      if(L0_amom)I0_utom=I0_utom+1
      if(L0_olom)I0_utom=I0_utom+1
      if(L0_elom)I0_utom=I0_utom+1
      if(L0_ukom)I0_utom=I0_utom+1
      if(L0_ikom)I0_utom=I0_utom+1
      if(L0_akom)I0_utom=I0_utom+1
      if(L0_ofom)I0_utom=I0_utom+1
      if(L0_efom)I0_utom=I0_utom+1
      if(L0_udom)I0_utom=I0_utom+1
      if(L0_idom)I0_utom=I0_utom+1
      if(L0_ubom)I0_utom=I0_utom+1


      if(.not.L0_avom.and.L_(579).and..not.L0_otom)then
        I0_ufip=1
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L_(583).and..not.L0_imom)then
        I0_ufip=2
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L_(582).and..not.L0_edom)then
        I0_ufip=3
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L_(581).and..not.L0_obom)then
        I0_ufip=4
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L_(580).and..not.L0_ibom)then
        I0_ufip=5
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_ebom.and..not.L0_abom)then
        I0_ufip=6
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_uxim.and..not.L0_oxim)then
        I0_ufip=7
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_ixim.and..not.L0_exim)then
        I0_ufip=8
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_axim.and..not.L0_uvim)then
        I0_ufip=9
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_itom.and..not.L0_etom)then
        I0_ufip=10
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_atom.and..not.L0_usom)then
        I0_ufip=11
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_osom.and..not.L0_isom)then
        I0_ufip=12
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_esom.and..not.L0_asom)then
        I0_ufip=13
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_urom.and..not.L0_orom)then
        I0_ufip=14
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_irom.and..not.L0_erom)then
        I0_ufip=15
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_arom.and..not.L0_upom)then
        I0_ufip=16
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_opom.and..not.L0_ipom)then
        I0_ufip=17
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_epom.and..not.L0_apom)then
        I0_ufip=18
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_umom.and..not.L0_omom)then
        I0_ufip=19
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_emom.and..not.L0_amom)then
        I0_ufip=20
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_ulom.and..not.L0_olom)then
        I0_ufip=21
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_ilom.and..not.L0_elom)then
        I0_ufip=22
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_alom.and..not.L0_ukom)then
        I0_ufip=23
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_okom.and..not.L0_ikom)then
        I0_ufip=24
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_ekom.and..not.L0_akom)then
        I0_ufip=25
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_ufom.and..not.L0_ofom)then
        I0_ufip=26
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_ifom.and..not.L0_efom)then
        I0_ufip=27
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_afom.and..not.L0_udom)then
        I0_ufip=28
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_odom.and..not.L0_idom)then
        I0_ufip=29
        L0_avom=.true.
      endif

      if(.not.L0_avom.and.L0_adom.and..not.L0_ubom)then
        I0_ufip=30
        L0_avom=.true.
      endif

      if(I0_utom.eq.1)then
         L0_otom=.false.
         L0_imom=.false.
         L0_edom=.false.
         L0_obom=.false.
         L0_ibom=.false.
         L0_abom=.false.
         L0_oxim=.false.
         L0_exim=.false.
         L0_uvim=.false.
         L0_etom=.false.
         L0_usom=.false.
         L0_isom=.false.
         L0_asom=.false.
         L0_orom=.false.
         L0_erom=.false.
         L0_upom=.false.
         L0_ipom=.false.
         L0_apom=.false.
         L0_omom=.false.
         L0_amom=.false.
         L0_olom=.false.
         L0_elom=.false.
         L0_ukom=.false.
         L0_ikom=.false.
         L0_akom=.false.
         L0_ofom=.false.
         L0_efom=.false.
         L0_udom=.false.
         L0_idom=.false.
         L0_ubom=.false.
      else
         L0_otom=L_(579)
         L0_imom=L_(583)
         L0_edom=L_(582)
         L0_obom=L_(581)
         L0_ibom=L_(580)
         L0_abom=L0_ebom
         L0_oxim=L0_uxim
         L0_exim=L0_ixim
         L0_uvim=L0_axim
         L0_etom=L0_itom
         L0_usom=L0_atom
         L0_isom=L0_osom
         L0_asom=L0_esom
         L0_orom=L0_urom
         L0_erom=L0_irom
         L0_upom=L0_arom
         L0_ipom=L0_opom
         L0_apom=L0_epom
         L0_omom=L0_umom
         L0_amom=L0_emom
         L0_olom=L0_ulom
         L0_elom=L0_ilom
         L0_ukom=L0_alom
         L0_ikom=L0_okom
         L0_akom=L0_ekom
         L0_ofom=L0_ufom
         L0_efom=L0_ifom
         L0_udom=L0_afom
         L0_idom=L0_odom
         L0_ubom=L0_adom
      endif
C 1762-12-0001_log.fgi( 371,  66):��������� �������
      select case (I0_ufip)
      case (1)
        C255_ofip="�����.���������"
      case (2)
        C255_ofip="�����, �������� 1"
      case (3)
        C255_ofip="�����, �������� 2"
      case (4)
        C255_ofip="����"
      case (5)
        C255_ofip="������, �������� 1"
      case (6)
        C255_ofip="???"
      case (7)
        C255_ofip="???"
      case (8)
        C255_ofip="???"
      case (9)
        C255_ofip="???"
      case (10)
        C255_ofip="???"
      case (11)
        C255_ofip="???"
      case (12)
        C255_ofip="???"
      case (13)
        C255_ofip="???"
      case (14)
        C255_ofip="???"
      case (15)
        C255_ofip="???"
      case (16)
        C255_ofip="???"
      case (17)
        C255_ofip="???"
      case (18)
        C255_ofip="???"
      case (19)
        C255_ofip="???"
      case (20)
        C255_ofip="???"
      case (21)
        C255_ofip="???"
      case (22)
        C255_ofip="???"
      case (23)
        C255_ofip="???"
      case (24)
        C255_ofip="???"
      case (25)
        C255_ofip="???"
      case (26)
        C255_ofip="???"
      case (27)
        C255_ofip="???"
      case (28)
        C255_ofip="???"
      case (29)
        C255_ofip="???"
      case (30)
        C255_ofip="???"
      end select
C 1762-12-0001_log.fgi( 387,  95):MarkVI text out 30 variants,1762-12-0001_RolV_state
      L_(592)=.false.
C 1762-12-0001_log.fgi( 279,  88):��������� ���������� (�������)
      if(L_(592)) then
         I0_akip=0
      endif
      I_(17)=I0_akip
      L_(591)=I0_akip.ne.0
C 1762-12-0001_log.fgi( 287,  90):���������� ��������� 
      L_(586) = (.NOT.L_(591))
C 1762-12-0001_log.fgi( 299,  95):���
      L_(590)=I_(17).eq.1
      L_(589)=I_(17).eq.2
      L_(588)=I_(17).eq.3
      L_(587)=I_(17).eq.4
C 1762-12-0001_log.fgi( 299,  90):���������� �� 4
      L0_itum=.false.
      I0_etum=0

      if(L0_upav)then
        I0_ikip=1
         L0_atum=.false.
         L0_ulum=.false.
         L0_obum=.false.
         L0_abum=.false.
         L0_uxom=.false.
         L0_ixom=.false.
         L0_axom=.false.
         L0_ovom=.false.
         L0_evom=.false.
         L0_osum=.false.
         L0_esum=.false.
         L0_urum=.false.
         L0_irum=.false.
         L0_arum=.false.
         L0_opum=.false.
         L0_epum=.false.
         L0_umum=.false.
         L0_imum=.false.
         L0_amum=.false.
         L0_ilum=.false.
         L0_alum=.false.
         L0_okum=.false.
         L0_ekum=.false.
         L0_ufum=.false.
         L0_ifum=.false.
         L0_afum=.false.
         L0_odum=.false.
         L0_edum=.false.
         L0_ubum=.false.
         L0_ebum=.false.
      endif

      if(L0_atum)I0_etum=I0_etum+1
      if(L0_ulum)I0_etum=I0_etum+1
      if(L0_obum)I0_etum=I0_etum+1
      if(L0_abum)I0_etum=I0_etum+1
      if(L0_uxom)I0_etum=I0_etum+1
      if(L0_ixom)I0_etum=I0_etum+1
      if(L0_axom)I0_etum=I0_etum+1
      if(L0_ovom)I0_etum=I0_etum+1
      if(L0_evom)I0_etum=I0_etum+1
      if(L0_osum)I0_etum=I0_etum+1
      if(L0_esum)I0_etum=I0_etum+1
      if(L0_urum)I0_etum=I0_etum+1
      if(L0_irum)I0_etum=I0_etum+1
      if(L0_arum)I0_etum=I0_etum+1
      if(L0_opum)I0_etum=I0_etum+1
      if(L0_epum)I0_etum=I0_etum+1
      if(L0_umum)I0_etum=I0_etum+1
      if(L0_imum)I0_etum=I0_etum+1
      if(L0_amum)I0_etum=I0_etum+1
      if(L0_ilum)I0_etum=I0_etum+1
      if(L0_alum)I0_etum=I0_etum+1
      if(L0_okum)I0_etum=I0_etum+1
      if(L0_ekum)I0_etum=I0_etum+1
      if(L0_ufum)I0_etum=I0_etum+1
      if(L0_ifum)I0_etum=I0_etum+1
      if(L0_afum)I0_etum=I0_etum+1
      if(L0_odum)I0_etum=I0_etum+1
      if(L0_edum)I0_etum=I0_etum+1
      if(L0_ubum)I0_etum=I0_etum+1
      if(L0_ebum)I0_etum=I0_etum+1


      if(.not.L0_itum.and.L_(586).and..not.L0_atum)then
        I0_ikip=1
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L_(590).and..not.L0_ulum)then
        I0_ikip=2
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L_(589).and..not.L0_obum)then
        I0_ikip=3
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L_(588).and..not.L0_abum)then
        I0_ikip=4
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L_(587).and..not.L0_uxom)then
        I0_ikip=5
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_oxom.and..not.L0_ixom)then
        I0_ikip=6
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_exom.and..not.L0_axom)then
        I0_ikip=7
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_uvom.and..not.L0_ovom)then
        I0_ikip=8
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_ivom.and..not.L0_evom)then
        I0_ikip=9
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_usum.and..not.L0_osum)then
        I0_ikip=10
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_isum.and..not.L0_esum)then
        I0_ikip=11
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_asum.and..not.L0_urum)then
        I0_ikip=12
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_orum.and..not.L0_irum)then
        I0_ikip=13
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_erum.and..not.L0_arum)then
        I0_ikip=14
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_upum.and..not.L0_opum)then
        I0_ikip=15
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_ipum.and..not.L0_epum)then
        I0_ikip=16
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_apum.and..not.L0_umum)then
        I0_ikip=17
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_omum.and..not.L0_imum)then
        I0_ikip=18
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_emum.and..not.L0_amum)then
        I0_ikip=19
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_olum.and..not.L0_ilum)then
        I0_ikip=20
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_elum.and..not.L0_alum)then
        I0_ikip=21
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_ukum.and..not.L0_okum)then
        I0_ikip=22
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_ikum.and..not.L0_ekum)then
        I0_ikip=23
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_akum.and..not.L0_ufum)then
        I0_ikip=24
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_ofum.and..not.L0_ifum)then
        I0_ikip=25
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_efum.and..not.L0_afum)then
        I0_ikip=26
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_udum.and..not.L0_odum)then
        I0_ikip=27
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_idum.and..not.L0_edum)then
        I0_ikip=28
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_adum.and..not.L0_ubum)then
        I0_ikip=29
        L0_itum=.true.
      endif

      if(.not.L0_itum.and.L0_ibum.and..not.L0_ebum)then
        I0_ikip=30
        L0_itum=.true.
      endif

      if(I0_etum.eq.1)then
         L0_atum=.false.
         L0_ulum=.false.
         L0_obum=.false.
         L0_abum=.false.
         L0_uxom=.false.
         L0_ixom=.false.
         L0_axom=.false.
         L0_ovom=.false.
         L0_evom=.false.
         L0_osum=.false.
         L0_esum=.false.
         L0_urum=.false.
         L0_irum=.false.
         L0_arum=.false.
         L0_opum=.false.
         L0_epum=.false.
         L0_umum=.false.
         L0_imum=.false.
         L0_amum=.false.
         L0_ilum=.false.
         L0_alum=.false.
         L0_okum=.false.
         L0_ekum=.false.
         L0_ufum=.false.
         L0_ifum=.false.
         L0_afum=.false.
         L0_odum=.false.
         L0_edum=.false.
         L0_ubum=.false.
         L0_ebum=.false.
      else
         L0_atum=L_(586)
         L0_ulum=L_(590)
         L0_obum=L_(589)
         L0_abum=L_(588)
         L0_uxom=L_(587)
         L0_ixom=L0_oxom
         L0_axom=L0_exom
         L0_ovom=L0_uvom
         L0_evom=L0_ivom
         L0_osum=L0_usum
         L0_esum=L0_isum
         L0_urum=L0_asum
         L0_irum=L0_orum
         L0_arum=L0_erum
         L0_opum=L0_upum
         L0_epum=L0_ipum
         L0_umum=L0_apum
         L0_imum=L0_omum
         L0_amum=L0_emum
         L0_ilum=L0_olum
         L0_alum=L0_elum
         L0_okum=L0_ukum
         L0_ekum=L0_ikum
         L0_ufum=L0_akum
         L0_ifum=L0_ofum
         L0_afum=L0_efum
         L0_odum=L0_udum
         L0_edum=L0_idum
         L0_ubum=L0_adum
         L0_ebum=L0_ibum
      endif
C 1762-12-0001_log.fgi( 309,  66):��������� �������
      select case (I0_ikip)
      case (1)
        C255_ekip="�����.���������"
      case (2)
        C255_ekip="�����, �������� 1"
      case (3)
        C255_ekip="�����, �������� 2"
      case (4)
        C255_ekip="����"
      case (5)
        C255_ekip="������, �������� 1"
      case (6)
        C255_ekip="???"
      case (7)
        C255_ekip="???"
      case (8)
        C255_ekip="???"
      case (9)
        C255_ekip="???"
      case (10)
        C255_ekip="???"
      case (11)
        C255_ekip="???"
      case (12)
        C255_ekip="???"
      case (13)
        C255_ekip="???"
      case (14)
        C255_ekip="???"
      case (15)
        C255_ekip="???"
      case (16)
        C255_ekip="???"
      case (17)
        C255_ekip="???"
      case (18)
        C255_ekip="???"
      case (19)
        C255_ekip="???"
      case (20)
        C255_ekip="???"
      case (21)
        C255_ekip="???"
      case (22)
        C255_ekip="???"
      case (23)
        C255_ekip="???"
      case (24)
        C255_ekip="???"
      case (25)
        C255_ekip="???"
      case (26)
        C255_ekip="???"
      case (27)
        C255_ekip="???"
      case (28)
        C255_ekip="???"
      case (29)
        C255_ekip="???"
      case (30)
        C255_ekip="???"
      end select
C 1762-12-0001_log.fgi( 325,  95):MarkVI text out 30 variants,1762-12-0001_RolZ_state
      L0_okip=R0_alip.ne.R0_ukip
      R0_ukip=R0_alip
C 1762-12-0001_log.fgi(  14,  16):���������� ������������� ������
      L0_elip=R0_olip.ne.R0_ilip
      R0_ilip=R0_olip
C 1762-12-0001_log.fgi(  14,  28):���������� ������������� ������
      L0_amip=R0_imip.ne.R0_emip
      R0_emip=R0_imip
C 1762-12-0001_log.fgi(  14,  49):���������� ������������� ������
      L0_omip=R0_apip.ne.R0_umip
      R0_umip=R0_apip
C 1762-12-0001_log.fgi(  14,  61):���������� ������������� ������
      L0_ulip=L0_omip.or.(L0_ulip.and..not.(L0_amip))
      L_(593)=.not.L0_ulip
C 1762-12-0001_log.fgi(  33,  59):RS �������
      L_(596)=.false.
C 1762-12-0001_log.fgi(  45, 106):��������� ���������� (�������)
      L_(595)=.false.
C 1762-12-0001_log.fgi(  45, 110):��������� ���������� (�������)
      L0_exip=R0_uvip.ne.R0_ovip
      R0_ovip=R0_uvip
C 1762-12-0001_log.fgi(  14, 108):���������� ������������� ������
      if(L_(595).or.L_(596)) then
         L_ixip=(L_(595).or.L_ixip).and..not.(L_(596))
      else
         if(L0_exip.and..not.L0_axip) L_ixip=.not.L_ixip
      endif
      L0_axip=L0_exip
      L_(597)=.not.L_ixip
C 1762-12-0001_log.fgi(  51, 108):T �������
      L_epip=L_ixip
C 1762-12-0001_log.fgi( 104, 114):������,1762-12-0001_lamp14
      if(L_ixip) then
         I_ivip=1
      else
         I_ivip=0
      endif
C 1762-12-0001_log.fgi(  88, 110):��������� LO->1
      L_(599)=.false.
C 1762-12-0001_log.fgi(  45, 118):��������� ���������� (�������)
      L_(598)=.false.
C 1762-12-0001_log.fgi(  45, 122):��������� ���������� (�������)
      L0_ibop=R0_abop.ne.R0_uxip
      R0_uxip=R0_abop
C 1762-12-0001_log.fgi(  14, 120):���������� ������������� ������
      if(L_(598).or.L_(599)) then
         L_obop=(L_(598).or.L_obop).and..not.(L_(599))
      else
         if(L0_ibop.and..not.L0_ebop) L_obop=.not.L_obop
      endif
      L0_ebop=L0_ibop
      L_(600)=.not.L_obop
C 1762-12-0001_log.fgi(  51, 120):T �������
      L_ipip=L_obop
C 1762-12-0001_log.fgi( 104, 126):������,1762-12-0001_lamp13
      if(L_obop) then
         I_oxip=1
      else
         I_oxip=0
      endif
C 1762-12-0001_log.fgi(  88, 122):��������� LO->1
      L_(602)=.false.
C 1762-12-0001_log.fgi(  45, 130):��������� ���������� (�������)
      L_(601)=.false.
C 1762-12-0001_log.fgi(  45, 134):��������� ���������� (�������)
      L0_odop=R0_edop.ne.R0_adop
      R0_adop=R0_edop
C 1762-12-0001_log.fgi(  14, 132):���������� ������������� ������
      if(L_(601).or.L_(602)) then
         L_udop=(L_(601).or.L_udop).and..not.(L_(602))
      else
         if(L0_odop.and..not.L0_idop) L_udop=.not.L_udop
      endif
      L0_idop=L0_odop
      L_(603)=.not.L_udop
C 1762-12-0001_log.fgi(  51, 132):T �������
      L_opip=L_udop
C 1762-12-0001_log.fgi( 104, 138):������,1762-12-0001_lamp12
      if(L_udop) then
         I_ubop=1
      else
         I_ubop=0
      endif
C 1762-12-0001_log.fgi(  88, 134):��������� LO->1
      L_(605)=.false.
C 1762-12-0001_log.fgi(  45, 142):��������� ���������� (�������)
      L_(604)=.false.
C 1762-12-0001_log.fgi(  45, 146):��������� ���������� (�������)
      L0_ufop=R0_ifop.ne.R0_efop
      R0_efop=R0_ifop
C 1762-12-0001_log.fgi(  14, 144):���������� ������������� ������
      if(L_(604).or.L_(605)) then
         L_akop=(L_(604).or.L_akop).and..not.(L_(605))
      else
         if(L0_ufop.and..not.L0_ofop) L_akop=.not.L_akop
      endif
      L0_ofop=L0_ufop
      L_(606)=.not.L_akop
C 1762-12-0001_log.fgi(  51, 144):T �������
      L_upip=L_akop
C 1762-12-0001_log.fgi( 104, 150):������,1762-12-0001_lamp11
      if(L_akop) then
         I_afop=1
      else
         I_afop=0
      endif
C 1762-12-0001_log.fgi(  88, 146):��������� LO->1
      L_(608)=.false.
C 1762-12-0001_log.fgi(  45, 154):��������� ���������� (�������)
      L_(607)=.false.
C 1762-12-0001_log.fgi(  45, 158):��������� ���������� (�������)
      L0_alop=R0_okop.ne.R0_ikop
      R0_ikop=R0_okop
C 1762-12-0001_log.fgi(  14, 156):���������� ������������� ������
      if(L_(607).or.L_(608)) then
         L_elop=(L_(607).or.L_elop).and..not.(L_(608))
      else
         if(L0_alop.and..not.L0_ukop) L_elop=.not.L_elop
      endif
      L0_ukop=L0_alop
      L_(609)=.not.L_elop
C 1762-12-0001_log.fgi(  51, 156):T �������
      L_arip=L_elop
C 1762-12-0001_log.fgi( 104, 162):������,1762-12-0001_lamp10
      if(L_elop) then
         I_ekop=1
      else
         I_ekop=0
      endif
C 1762-12-0001_log.fgi(  88, 158):��������� LO->1
      L_(611)=.false.
C 1762-12-0001_log.fgi(  45, 166):��������� ���������� (�������)
      L_(610)=.false.
C 1762-12-0001_log.fgi(  45, 170):��������� ���������� (�������)
      L0_emop=R0_ulop.ne.R0_olop
      R0_olop=R0_ulop
C 1762-12-0001_log.fgi(  14, 168):���������� ������������� ������
      if(L_(610).or.L_(611)) then
         L_imop=(L_(610).or.L_imop).and..not.(L_(611))
      else
         if(L0_emop.and..not.L0_amop) L_imop=.not.L_imop
      endif
      L0_amop=L0_emop
      L_(612)=.not.L_imop
C 1762-12-0001_log.fgi(  51, 168):T �������
      L_erip=L_imop
C 1762-12-0001_log.fgi( 104, 174):������,1762-12-0001_lamp09
      if(L_imop) then
         I_ilop=1
      else
         I_ilop=0
      endif
C 1762-12-0001_log.fgi(  88, 170):��������� LO->1
      L_(614)=.false.
C 1762-12-0001_log.fgi(  45, 178):��������� ���������� (�������)
      L_(613)=.false.
C 1762-12-0001_log.fgi(  45, 182):��������� ���������� (�������)
      L0_ipop=R0_apop.ne.R0_umop
      R0_umop=R0_apop
C 1762-12-0001_log.fgi(  14, 180):���������� ������������� ������
      if(L_(613).or.L_(614)) then
         L_opop=(L_(613).or.L_opop).and..not.(L_(614))
      else
         if(L0_ipop.and..not.L0_epop) L_opop=.not.L_opop
      endif
      L0_epop=L0_ipop
      L_(615)=.not.L_opop
C 1762-12-0001_log.fgi(  51, 180):T �������
      L_irip=L_opop
C 1762-12-0001_log.fgi( 104, 186):������,1762-12-0001_lamp08
      if(L_opop) then
         I_omop=1
      else
         I_omop=0
      endif
C 1762-12-0001_log.fgi(  88, 182):��������� LO->1
      L_(617)=.false.
C 1762-12-0001_log.fgi(  45, 190):��������� ���������� (�������)
      L_(616)=.false.
C 1762-12-0001_log.fgi(  45, 194):��������� ���������� (�������)
      L0_orop=R0_erop.ne.R0_arop
      R0_arop=R0_erop
C 1762-12-0001_log.fgi(  14, 192):���������� ������������� ������
      if(L_(616).or.L_(617)) then
         L_urop=(L_(616).or.L_urop).and..not.(L_(617))
      else
         if(L0_orop.and..not.L0_irop) L_urop=.not.L_urop
      endif
      L0_irop=L0_orop
      L_(618)=.not.L_urop
C 1762-12-0001_log.fgi(  51, 192):T �������
      L_orip=L_urop
C 1762-12-0001_log.fgi( 104, 198):������,1762-12-0001_lamp07
      if(L_urop) then
         I_upop=1
      else
         I_upop=0
      endif
C 1762-12-0001_log.fgi(  88, 194):��������� LO->1
      L_(620)=.false.
C 1762-12-0001_log.fgi(  45, 202):��������� ���������� (�������)
      L_(619)=.false.
C 1762-12-0001_log.fgi(  45, 206):��������� ���������� (�������)
      L0_usop=R0_isop.ne.R0_esop
      R0_esop=R0_isop
C 1762-12-0001_log.fgi(  14, 204):���������� ������������� ������
      if(L_(619).or.L_(620)) then
         L_atop=(L_(619).or.L_atop).and..not.(L_(620))
      else
         if(L0_usop.and..not.L0_osop) L_atop=.not.L_atop
      endif
      L0_osop=L0_usop
      L_(621)=.not.L_atop
C 1762-12-0001_log.fgi(  51, 204):T �������
      L_urip=L_atop
C 1762-12-0001_log.fgi( 104, 210):������,1762-12-0001_lamp06
      if(L_atop) then
         I_asop=1
      else
         I_asop=0
      endif
C 1762-12-0001_log.fgi(  88, 206):��������� LO->1
      L_(623)=.false.
C 1762-12-0001_log.fgi(  45, 214):��������� ���������� (�������)
      L_(622)=.false.
C 1762-12-0001_log.fgi(  45, 218):��������� ���������� (�������)
      L0_avop=R0_otop.ne.R0_itop
      R0_itop=R0_otop
C 1762-12-0001_log.fgi(  14, 216):���������� ������������� ������
      if(L_(622).or.L_(623)) then
         L_evop=(L_(622).or.L_evop).and..not.(L_(623))
      else
         if(L0_avop.and..not.L0_utop) L_evop=.not.L_evop
      endif
      L0_utop=L0_avop
      L_(624)=.not.L_evop
C 1762-12-0001_log.fgi(  51, 216):T �������
      L_asip=L_evop
C 1762-12-0001_log.fgi( 104, 222):������,1762-12-0001_lamp05
      if(L_evop) then
         I_etop=1
      else
         I_etop=0
      endif
C 1762-12-0001_log.fgi(  88, 218):��������� LO->1
      L_(626)=.false.
C 1762-12-0001_log.fgi(  45, 226):��������� ���������� (�������)
      L_(625)=.false.
C 1762-12-0001_log.fgi(  45, 230):��������� ���������� (�������)
      L0_exop=R0_uvop.ne.R0_ovop
      R0_ovop=R0_uvop
C 1762-12-0001_log.fgi(  14, 228):���������� ������������� ������
      if(L_(625).or.L_(626)) then
         L_ixop=(L_(625).or.L_ixop).and..not.(L_(626))
      else
         if(L0_exop.and..not.L0_axop) L_ixop=.not.L_ixop
      endif
      L0_axop=L0_exop
      L_(627)=.not.L_ixop
C 1762-12-0001_log.fgi(  51, 228):T �������
      L_esip=L_ixop
C 1762-12-0001_log.fgi( 104, 234):������,1762-12-0001_lamp04
      if(L_ixop) then
         I_ivop=1
      else
         I_ivop=0
      endif
C 1762-12-0001_log.fgi(  88, 230):��������� LO->1
      L_(629)=.false.
C 1762-12-0001_log.fgi(  45, 238):��������� ���������� (�������)
      L_(628)=.false.
C 1762-12-0001_log.fgi(  45, 242):��������� ���������� (�������)
      L0_ibup=R0_abup.ne.R0_uxop
      R0_uxop=R0_abup
C 1762-12-0001_log.fgi(  14, 240):���������� ������������� ������
      if(L_(628).or.L_(629)) then
         L_obup=(L_(628).or.L_obup).and..not.(L_(629))
      else
         if(L0_ibup.and..not.L0_ebup) L_obup=.not.L_obup
      endif
      L0_ebup=L0_ibup
      L_(630)=.not.L_obup
C 1762-12-0001_log.fgi(  51, 240):T �������
      L_isip=L_obup
C 1762-12-0001_log.fgi( 104, 246):������,1762-12-0001_lamp03
      if(L_obup) then
         I_oxop=1
      else
         I_oxop=0
      endif
C 1762-12-0001_log.fgi(  88, 242):��������� LO->1
      L_(632)=.false.
C 1762-12-0001_log.fgi(  45, 250):��������� ���������� (�������)
      L_(631)=.false.
C 1762-12-0001_log.fgi(  45, 254):��������� ���������� (�������)
      L0_odup=R0_edup.ne.R0_adup
      R0_adup=R0_edup
C 1762-12-0001_log.fgi(  14, 252):���������� ������������� ������
      if(L_(631).or.L_(632)) then
         L_udup=(L_(631).or.L_udup).and..not.(L_(632))
      else
         if(L0_odup.and..not.L0_idup) L_udup=.not.L_udup
      endif
      L0_idup=L0_odup
      L_(633)=.not.L_udup
C 1762-12-0001_log.fgi(  51, 252):T �������
      L_osip=L_udup
C 1762-12-0001_log.fgi( 104, 258):������,1762-12-0001_lamp02
      if(L_udup) then
         I_ubup=1
      else
         I_ubup=0
      endif
C 1762-12-0001_log.fgi(  88, 254):��������� LO->1
      L_(635)=.false.
C 1762-12-0001_log.fgi(  45, 262):��������� ���������� (�������)
      L_(634)=.false.
C 1762-12-0001_log.fgi(  45, 266):��������� ���������� (�������)
      L0_ufup=R0_ifup.ne.R0_efup
      R0_efup=R0_ifup
C 1762-12-0001_log.fgi(  14, 264):���������� ������������� ������
      if(L_(634).or.L_(635)) then
         L_akup=(L_(634).or.L_akup).and..not.(L_(635))
      else
         if(L0_ufup.and..not.L0_ofup) L_akup=.not.L_akup
      endif
      L0_ofup=L0_ufup
      L_(636)=.not.L_akup
C 1762-12-0001_log.fgi(  51, 264):T �������
      L_usip=L_akup
C 1762-12-0001_log.fgi( 104, 270):������,1762-12-0001_lamp01
      L_(594) = L_ixip.OR.L_obop.OR.L_udop.OR.L_akop.OR.L_elop.OR.L_imop
     &.OR.L_opop.OR.L_urop.OR.L_atop.OR.L_evop.OR.L_ixop.OR.L_obup.OR.L_
     &udup.OR.L_akup
C 1762-12-0001_log.fgi(  79,  85):���
      select case (L_(594))
      case (.FALSE.)
        C255_otip="����������(�) ���������(�)"
      case (.TRUE.)
        C255_otip="����������(�) ��������(�)"
      end select

      select case (L_(594))
      case (.FALSE.)
        I_itip=I_etip
      case (.TRUE.)
        I_itip=I_atip
      end select

      select case (L_(594))
      case (.FALSE.)
        I_evip=I_avip
      case (.TRUE.)
        I_evip=I_utip
      end select
C 1762-12-0001_log.fgi( 104,  86):MarkVI text out,1762-12-0001_blkonoff
      if(L_akup) then
         I_afup=1
      else
         I_afup=0
      endif
C 1762-12-0001_log.fgi(  88, 266):��������� LO->1
      L0_ikar=.false.
      I0_ekar=0

      if(L0_upav)then
        I0_ukar=1
         L0_ufar=.false.
         L0_ivup=.false.
         L0_apup=.false.
         L0_emup=.false.
         L0_ulup=.false.
         L0_ilup=.false.
         L0_alup=.false.
         L0_okup=.false.
         L0_ekup=.false.
         L0_ifar=.false.
         L0_afar=.false.
         L0_odar=.false.
         L0_edar=.false.
         L0_ubar=.false.
         L0_ibar=.false.
         L0_abar=.false.
         L0_oxup=.false.
         L0_exup=.false.
         L0_uvup=.false.
         L0_avup=.false.
         L0_otup=.false.
         L0_etup=.false.
         L0_usup=.false.
         L0_isup=.false.
         L0_asup=.false.
         L0_orup=.false.
         L0_erup=.false.
         L0_upup=.false.
         L0_ipup=.false.
         L0_omup=.false.
      endif

      if(L0_ufar)I0_ekar=I0_ekar+1
      if(L0_ivup)I0_ekar=I0_ekar+1
      if(L0_apup)I0_ekar=I0_ekar+1
      if(L0_emup)I0_ekar=I0_ekar+1
      if(L0_ulup)I0_ekar=I0_ekar+1
      if(L0_ilup)I0_ekar=I0_ekar+1
      if(L0_alup)I0_ekar=I0_ekar+1
      if(L0_okup)I0_ekar=I0_ekar+1
      if(L0_ekup)I0_ekar=I0_ekar+1
      if(L0_ifar)I0_ekar=I0_ekar+1
      if(L0_afar)I0_ekar=I0_ekar+1
      if(L0_odar)I0_ekar=I0_ekar+1
      if(L0_edar)I0_ekar=I0_ekar+1
      if(L0_ubar)I0_ekar=I0_ekar+1
      if(L0_ibar)I0_ekar=I0_ekar+1
      if(L0_abar)I0_ekar=I0_ekar+1
      if(L0_oxup)I0_ekar=I0_ekar+1
      if(L0_exup)I0_ekar=I0_ekar+1
      if(L0_uvup)I0_ekar=I0_ekar+1
      if(L0_avup)I0_ekar=I0_ekar+1
      if(L0_otup)I0_ekar=I0_ekar+1
      if(L0_etup)I0_ekar=I0_ekar+1
      if(L0_usup)I0_ekar=I0_ekar+1
      if(L0_isup)I0_ekar=I0_ekar+1
      if(L0_asup)I0_ekar=I0_ekar+1
      if(L0_orup)I0_ekar=I0_ekar+1
      if(L0_erup)I0_ekar=I0_ekar+1
      if(L0_upup)I0_ekar=I0_ekar+1
      if(L0_ipup)I0_ekar=I0_ekar+1
      if(L0_omup)I0_ekar=I0_ekar+1


      if(.not.L0_ikar.and.L0_akar.and..not.L0_ufar)then
        I0_ukar=1
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_ovup.and..not.L0_ivup)then
        I0_ukar=2
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_epup.and..not.L0_apup)then
        I0_ukar=3
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_imup.and..not.L0_emup)then
        I0_ukar=4
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_amup.and..not.L0_ulup)then
        I0_ukar=5
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_olup.and..not.L0_ilup)then
        I0_ukar=6
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_elup.and..not.L0_alup)then
        I0_ukar=7
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_ukup.and..not.L0_okup)then
        I0_ukar=8
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_ikup.and..not.L0_ekup)then
        I0_ukar=9
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_ofar.and..not.L0_ifar)then
        I0_ukar=10
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_efar.and..not.L0_afar)then
        I0_ukar=11
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_udar.and..not.L0_odar)then
        I0_ukar=12
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_idar.and..not.L0_edar)then
        I0_ukar=13
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_adar.and..not.L0_ubar)then
        I0_ukar=14
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_obar.and..not.L0_ibar)then
        I0_ukar=15
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_ebar.and..not.L0_abar)then
        I0_ukar=16
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_uxup.and..not.L0_oxup)then
        I0_ukar=17
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_ixup.and..not.L0_exup)then
        I0_ukar=18
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_axup.and..not.L0_uvup)then
        I0_ukar=19
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_evup.and..not.L0_avup)then
        I0_ukar=20
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_utup.and..not.L0_otup)then
        I0_ukar=21
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_itup.and..not.L0_etup)then
        I0_ukar=22
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_atup.and..not.L0_usup)then
        I0_ukar=23
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_osup.and..not.L0_isup)then
        I0_ukar=24
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_esup.and..not.L0_asup)then
        I0_ukar=25
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_urup.and..not.L0_orup)then
        I0_ukar=26
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_irup.and..not.L0_erup)then
        I0_ukar=27
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_arup.and..not.L0_upup)then
        I0_ukar=28
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_opup.and..not.L0_ipup)then
        I0_ukar=29
        L0_ikar=.true.
      endif

      if(.not.L0_ikar.and.L0_umup.and..not.L0_omup)then
        I0_ukar=30
        L0_ikar=.true.
      endif

      if(I0_ekar.eq.1)then
         L0_ufar=.false.
         L0_ivup=.false.
         L0_apup=.false.
         L0_emup=.false.
         L0_ulup=.false.
         L0_ilup=.false.
         L0_alup=.false.
         L0_okup=.false.
         L0_ekup=.false.
         L0_ifar=.false.
         L0_afar=.false.
         L0_odar=.false.
         L0_edar=.false.
         L0_ubar=.false.
         L0_ibar=.false.
         L0_abar=.false.
         L0_oxup=.false.
         L0_exup=.false.
         L0_uvup=.false.
         L0_avup=.false.
         L0_otup=.false.
         L0_etup=.false.
         L0_usup=.false.
         L0_isup=.false.
         L0_asup=.false.
         L0_orup=.false.
         L0_erup=.false.
         L0_upup=.false.
         L0_ipup=.false.
         L0_omup=.false.
      else
         L0_ufar=L0_akar
         L0_ivup=L0_ovup
         L0_apup=L0_epup
         L0_emup=L0_imup
         L0_ulup=L0_amup
         L0_ilup=L0_olup
         L0_alup=L0_elup
         L0_okup=L0_ukup
         L0_ekup=L0_ikup
         L0_ifar=L0_ofar
         L0_afar=L0_efar
         L0_odar=L0_udar
         L0_edar=L0_idar
         L0_ubar=L0_adar
         L0_ibar=L0_obar
         L0_abar=L0_ebar
         L0_oxup=L0_uxup
         L0_exup=L0_ixup
         L0_uvup=L0_axup
         L0_avup=L0_evup
         L0_otup=L0_utup
         L0_etup=L0_itup
         L0_usup=L0_atup
         L0_isup=L0_osup
         L0_asup=L0_esup
         L0_orup=L0_urup
         L0_erup=L0_irup
         L0_upup=L0_arup
         L0_ipup=L0_opup
         L0_omup=L0_umup
      endif
C 1762-11-0006_log.fgi( 270, 241):��������� �������
      select case (I0_ukar)
      case (1)
        C255_okar="�����.���������"
      case (2)
        C255_okar="???"
      case (3)
        C255_okar="???"
      case (4)
        C255_okar="???"
      case (5)
        C255_okar="???"
      case (6)
        C255_okar="???"
      case (7)
        C255_okar="???"
      case (8)
        C255_okar="???"
      case (9)
        C255_okar="???"
      case (10)
        C255_okar="???"
      case (11)
        C255_okar="???"
      case (12)
        C255_okar="???"
      case (13)
        C255_okar="???"
      case (14)
        C255_okar="???"
      case (15)
        C255_okar="???"
      case (16)
        C255_okar="???"
      case (17)
        C255_okar="???"
      case (18)
        C255_okar="???"
      case (19)
        C255_okar="???"
      case (20)
        C255_okar="???"
      case (21)
        C255_okar="???"
      case (22)
        C255_okar="???"
      case (23)
        C255_okar="???"
      case (24)
        C255_okar="???"
      case (25)
        C255_okar="???"
      case (26)
        C255_okar="???"
      case (27)
        C255_okar="???"
      case (28)
        C255_okar="???"
      case (29)
        C255_okar="???"
      case (30)
        C255_okar="???"
      end select
C 1762-11-0006_log.fgi( 286, 270):MarkVI text out 30 variants,1762-11-0006_LogText
      L_(643)=.false.
C 1762-11-0006_log.fgi( 198, 263):��������� ���������� (�������)
      if(L_(643)) then
         I0_ufir=0
      endif
      I_(18)=I0_ufir
      L_(642)=I0_ufir.ne.0
C 1762-11-0006_log.fgi( 206, 265):���������� ��������� 
      L_(637) = (.NOT.L_(642))
C 1762-11-0006_log.fgi( 218, 270):���
      L_(641)=I_(18).eq.1
      L_(640)=I_(18).eq.2
      L_(639)=I_(18).eq.3
      L_(638)=I_(18).eq.4
C 1762-11-0006_log.fgi( 218, 265):���������� �� 4
      L0_eker=.false.
      I0_aker=0

      if(L0_upav)then
        I0_ekir=1
         L0_ufer=.false.
         L0_ovar=.false.
         L0_ipar=.false.
         L0_umar=.false.
         L0_omar=.false.
         L0_emar=.false.
         L0_ular=.false.
         L0_ilar=.false.
         L0_alar=.false.
         L0_ifer=.false.
         L0_afer=.false.
         L0_oder=.false.
         L0_eder=.false.
         L0_uber=.false.
         L0_iber=.false.
         L0_aber=.false.
         L0_oxar=.false.
         L0_exar=.false.
         L0_uvar=.false.
         L0_evar=.false.
         L0_utar=.false.
         L0_itar=.false.
         L0_atar=.false.
         L0_osar=.false.
         L0_esar=.false.
         L0_urar=.false.
         L0_irar=.false.
         L0_arar=.false.
         L0_opar=.false.
         L0_apar=.false.
      endif

      if(L0_ufer)I0_aker=I0_aker+1
      if(L0_ovar)I0_aker=I0_aker+1
      if(L0_ipar)I0_aker=I0_aker+1
      if(L0_umar)I0_aker=I0_aker+1
      if(L0_omar)I0_aker=I0_aker+1
      if(L0_emar)I0_aker=I0_aker+1
      if(L0_ular)I0_aker=I0_aker+1
      if(L0_ilar)I0_aker=I0_aker+1
      if(L0_alar)I0_aker=I0_aker+1
      if(L0_ifer)I0_aker=I0_aker+1
      if(L0_afer)I0_aker=I0_aker+1
      if(L0_oder)I0_aker=I0_aker+1
      if(L0_eder)I0_aker=I0_aker+1
      if(L0_uber)I0_aker=I0_aker+1
      if(L0_iber)I0_aker=I0_aker+1
      if(L0_aber)I0_aker=I0_aker+1
      if(L0_oxar)I0_aker=I0_aker+1
      if(L0_exar)I0_aker=I0_aker+1
      if(L0_uvar)I0_aker=I0_aker+1
      if(L0_evar)I0_aker=I0_aker+1
      if(L0_utar)I0_aker=I0_aker+1
      if(L0_itar)I0_aker=I0_aker+1
      if(L0_atar)I0_aker=I0_aker+1
      if(L0_osar)I0_aker=I0_aker+1
      if(L0_esar)I0_aker=I0_aker+1
      if(L0_urar)I0_aker=I0_aker+1
      if(L0_irar)I0_aker=I0_aker+1
      if(L0_arar)I0_aker=I0_aker+1
      if(L0_opar)I0_aker=I0_aker+1
      if(L0_apar)I0_aker=I0_aker+1


      if(.not.L0_eker.and.L_(637).and..not.L0_ufer)then
        I0_ekir=1
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L_(641).and..not.L0_ovar)then
        I0_ekir=2
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L_(640).and..not.L0_ipar)then
        I0_ekir=3
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L_(639).and..not.L0_umar)then
        I0_ekir=4
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L_(638).and..not.L0_omar)then
        I0_ekir=5
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_imar.and..not.L0_emar)then
        I0_ekir=6
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_amar.and..not.L0_ular)then
        I0_ekir=7
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_olar.and..not.L0_ilar)then
        I0_ekir=8
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_elar.and..not.L0_alar)then
        I0_ekir=9
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_ofer.and..not.L0_ifer)then
        I0_ekir=10
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_efer.and..not.L0_afer)then
        I0_ekir=11
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_uder.and..not.L0_oder)then
        I0_ekir=12
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_ider.and..not.L0_eder)then
        I0_ekir=13
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_ader.and..not.L0_uber)then
        I0_ekir=14
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_ober.and..not.L0_iber)then
        I0_ekir=15
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_eber.and..not.L0_aber)then
        I0_ekir=16
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_uxar.and..not.L0_oxar)then
        I0_ekir=17
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_ixar.and..not.L0_exar)then
        I0_ekir=18
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_axar.and..not.L0_uvar)then
        I0_ekir=19
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_ivar.and..not.L0_evar)then
        I0_ekir=20
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_avar.and..not.L0_utar)then
        I0_ekir=21
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_otar.and..not.L0_itar)then
        I0_ekir=22
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_etar.and..not.L0_atar)then
        I0_ekir=23
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_usar.and..not.L0_osar)then
        I0_ekir=24
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_isar.and..not.L0_esar)then
        I0_ekir=25
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_asar.and..not.L0_urar)then
        I0_ekir=26
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_orar.and..not.L0_irar)then
        I0_ekir=27
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_erar.and..not.L0_arar)then
        I0_ekir=28
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_upar.and..not.L0_opar)then
        I0_ekir=29
        L0_eker=.true.
      endif

      if(.not.L0_eker.and.L0_epar.and..not.L0_apar)then
        I0_ekir=30
        L0_eker=.true.
      endif

      if(I0_aker.eq.1)then
         L0_ufer=.false.
         L0_ovar=.false.
         L0_ipar=.false.
         L0_umar=.false.
         L0_omar=.false.
         L0_emar=.false.
         L0_ular=.false.
         L0_ilar=.false.
         L0_alar=.false.
         L0_ifer=.false.
         L0_afer=.false.
         L0_oder=.false.
         L0_eder=.false.
         L0_uber=.false.
         L0_iber=.false.
         L0_aber=.false.
         L0_oxar=.false.
         L0_exar=.false.
         L0_uvar=.false.
         L0_evar=.false.
         L0_utar=.false.
         L0_itar=.false.
         L0_atar=.false.
         L0_osar=.false.
         L0_esar=.false.
         L0_urar=.false.
         L0_irar=.false.
         L0_arar=.false.
         L0_opar=.false.
         L0_apar=.false.
      else
         L0_ufer=L_(637)
         L0_ovar=L_(641)
         L0_ipar=L_(640)
         L0_umar=L_(639)
         L0_omar=L_(638)
         L0_emar=L0_imar
         L0_ular=L0_amar
         L0_ilar=L0_olar
         L0_alar=L0_elar
         L0_ifer=L0_ofer
         L0_afer=L0_efer
         L0_oder=L0_uder
         L0_eder=L0_ider
         L0_uber=L0_ader
         L0_iber=L0_ober
         L0_aber=L0_eber
         L0_oxar=L0_uxar
         L0_exar=L0_ixar
         L0_uvar=L0_axar
         L0_evar=L0_ivar
         L0_utar=L0_avar
         L0_itar=L0_otar
         L0_atar=L0_etar
         L0_osar=L0_usar
         L0_esar=L0_isar
         L0_urar=L0_asar
         L0_irar=L0_orar
         L0_arar=L0_erar
         L0_opar=L0_upar
         L0_apar=L0_epar
      endif
C 1762-11-0006_log.fgi( 228, 241):��������� �������
      select case (I0_ekir)
      case (1)
        C255_akir="�����.���������"
      case (2)
        C255_akir="�����, �������� 1"
      case (3)
        C255_akir="�����, �������� 2"
      case (4)
        C255_akir="����"
      case (5)
        C255_akir="������, �������� 1"
      case (6)
        C255_akir="???"
      case (7)
        C255_akir="???"
      case (8)
        C255_akir="???"
      case (9)
        C255_akir="???"
      case (10)
        C255_akir="???"
      case (11)
        C255_akir="???"
      case (12)
        C255_akir="???"
      case (13)
        C255_akir="???"
      case (14)
        C255_akir="???"
      case (15)
        C255_akir="???"
      case (16)
        C255_akir="???"
      case (17)
        C255_akir="???"
      case (18)
        C255_akir="???"
      case (19)
        C255_akir="???"
      case (20)
        C255_akir="???"
      case (21)
        C255_akir="???"
      case (22)
        C255_akir="???"
      case (23)
        C255_akir="???"
      case (24)
        C255_akir="???"
      case (25)
        C255_akir="???"
      case (26)
        C255_akir="???"
      case (27)
        C255_akir="???"
      case (28)
        C255_akir="???"
      case (29)
        C255_akir="???"
      case (30)
        C255_akir="???"
      end select
C 1762-11-0006_log.fgi( 244, 270):MarkVI text out 30 variants,1762-11-0006_RolV_state
      L_(650)=.false.
C 1762-11-0006_log.fgi( 136, 263):��������� ���������� (�������)
      if(L_(650)) then
         I0_ikir=0
      endif
      I_(19)=I0_ikir
      L_(649)=I0_ikir.ne.0
C 1762-11-0006_log.fgi( 144, 265):���������� ��������� 
      L_(644) = (.NOT.L_(649))
C 1762-11-0006_log.fgi( 156, 270):���
      L_(648)=I_(19).eq.1
      L_(647)=I_(19).eq.2
      L_(646)=I_(19).eq.3
      L_(645)=I_(19).eq.4
C 1762-11-0006_log.fgi( 156, 265):���������� �� 4
      L0_ofir=.false.
      I0_ifir=0

      if(L0_upav)then
        I0_ukir=1
         L0_efir=.false.
         L0_aver=.false.
         L0_umer=.false.
         L0_emer=.false.
         L0_amer=.false.
         L0_oler=.false.
         L0_eler=.false.
         L0_uker=.false.
         L0_iker=.false.
         L0_udir=.false.
         L0_idir=.false.
         L0_adir=.false.
         L0_obir=.false.
         L0_ebir=.false.
         L0_uxer=.false.
         L0_ixer=.false.
         L0_axer=.false.
         L0_over=.false.
         L0_ever=.false.
         L0_oter=.false.
         L0_eter=.false.
         L0_user=.false.
         L0_iser=.false.
         L0_aser=.false.
         L0_orer=.false.
         L0_erer=.false.
         L0_uper=.false.
         L0_iper=.false.
         L0_aper=.false.
         L0_imer=.false.
      endif

      if(L0_efir)I0_ifir=I0_ifir+1
      if(L0_aver)I0_ifir=I0_ifir+1
      if(L0_umer)I0_ifir=I0_ifir+1
      if(L0_emer)I0_ifir=I0_ifir+1
      if(L0_amer)I0_ifir=I0_ifir+1
      if(L0_oler)I0_ifir=I0_ifir+1
      if(L0_eler)I0_ifir=I0_ifir+1
      if(L0_uker)I0_ifir=I0_ifir+1
      if(L0_iker)I0_ifir=I0_ifir+1
      if(L0_udir)I0_ifir=I0_ifir+1
      if(L0_idir)I0_ifir=I0_ifir+1
      if(L0_adir)I0_ifir=I0_ifir+1
      if(L0_obir)I0_ifir=I0_ifir+1
      if(L0_ebir)I0_ifir=I0_ifir+1
      if(L0_uxer)I0_ifir=I0_ifir+1
      if(L0_ixer)I0_ifir=I0_ifir+1
      if(L0_axer)I0_ifir=I0_ifir+1
      if(L0_over)I0_ifir=I0_ifir+1
      if(L0_ever)I0_ifir=I0_ifir+1
      if(L0_oter)I0_ifir=I0_ifir+1
      if(L0_eter)I0_ifir=I0_ifir+1
      if(L0_user)I0_ifir=I0_ifir+1
      if(L0_iser)I0_ifir=I0_ifir+1
      if(L0_aser)I0_ifir=I0_ifir+1
      if(L0_orer)I0_ifir=I0_ifir+1
      if(L0_erer)I0_ifir=I0_ifir+1
      if(L0_uper)I0_ifir=I0_ifir+1
      if(L0_iper)I0_ifir=I0_ifir+1
      if(L0_aper)I0_ifir=I0_ifir+1
      if(L0_imer)I0_ifir=I0_ifir+1


      if(.not.L0_ofir.and.L_(644).and..not.L0_efir)then
        I0_ukir=1
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L_(648).and..not.L0_aver)then
        I0_ukir=2
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L_(647).and..not.L0_umer)then
        I0_ukir=3
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L_(646).and..not.L0_emer)then
        I0_ukir=4
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L_(645).and..not.L0_amer)then
        I0_ukir=5
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_uler.and..not.L0_oler)then
        I0_ukir=6
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_iler.and..not.L0_eler)then
        I0_ukir=7
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_aler.and..not.L0_uker)then
        I0_ukir=8
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_oker.and..not.L0_iker)then
        I0_ukir=9
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_afir.and..not.L0_udir)then
        I0_ukir=10
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_odir.and..not.L0_idir)then
        I0_ukir=11
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_edir.and..not.L0_adir)then
        I0_ukir=12
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_ubir.and..not.L0_obir)then
        I0_ukir=13
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_ibir.and..not.L0_ebir)then
        I0_ukir=14
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_abir.and..not.L0_uxer)then
        I0_ukir=15
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_oxer.and..not.L0_ixer)then
        I0_ukir=16
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_exer.and..not.L0_axer)then
        I0_ukir=17
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_uver.and..not.L0_over)then
        I0_ukir=18
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_iver.and..not.L0_ever)then
        I0_ukir=19
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_uter.and..not.L0_oter)then
        I0_ukir=20
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_iter.and..not.L0_eter)then
        I0_ukir=21
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_ater.and..not.L0_user)then
        I0_ukir=22
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_oser.and..not.L0_iser)then
        I0_ukir=23
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_eser.and..not.L0_aser)then
        I0_ukir=24
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_urer.and..not.L0_orer)then
        I0_ukir=25
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_irer.and..not.L0_erer)then
        I0_ukir=26
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_arer.and..not.L0_uper)then
        I0_ukir=27
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_oper.and..not.L0_iper)then
        I0_ukir=28
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_eper.and..not.L0_aper)then
        I0_ukir=29
        L0_ofir=.true.
      endif

      if(.not.L0_ofir.and.L0_omer.and..not.L0_imer)then
        I0_ukir=30
        L0_ofir=.true.
      endif

      if(I0_ifir.eq.1)then
         L0_efir=.false.
         L0_aver=.false.
         L0_umer=.false.
         L0_emer=.false.
         L0_amer=.false.
         L0_oler=.false.
         L0_eler=.false.
         L0_uker=.false.
         L0_iker=.false.
         L0_udir=.false.
         L0_idir=.false.
         L0_adir=.false.
         L0_obir=.false.
         L0_ebir=.false.
         L0_uxer=.false.
         L0_ixer=.false.
         L0_axer=.false.
         L0_over=.false.
         L0_ever=.false.
         L0_oter=.false.
         L0_eter=.false.
         L0_user=.false.
         L0_iser=.false.
         L0_aser=.false.
         L0_orer=.false.
         L0_erer=.false.
         L0_uper=.false.
         L0_iper=.false.
         L0_aper=.false.
         L0_imer=.false.
      else
         L0_efir=L_(644)
         L0_aver=L_(648)
         L0_umer=L_(647)
         L0_emer=L_(646)
         L0_amer=L_(645)
         L0_oler=L0_uler
         L0_eler=L0_iler
         L0_uker=L0_aler
         L0_iker=L0_oker
         L0_udir=L0_afir
         L0_idir=L0_odir
         L0_adir=L0_edir
         L0_obir=L0_ubir
         L0_ebir=L0_ibir
         L0_uxer=L0_abir
         L0_ixer=L0_oxer
         L0_axer=L0_exer
         L0_over=L0_uver
         L0_ever=L0_iver
         L0_oter=L0_uter
         L0_eter=L0_iter
         L0_user=L0_ater
         L0_iser=L0_oser
         L0_aser=L0_eser
         L0_orer=L0_urer
         L0_erer=L0_irer
         L0_uper=L0_arer
         L0_iper=L0_oper
         L0_aper=L0_eper
         L0_imer=L0_omer
      endif
C 1762-11-0006_log.fgi( 166, 241):��������� �������
      select case (I0_ukir)
      case (1)
        C255_okir="�����.���������"
      case (2)
        C255_okir="�����, �������� 1"
      case (3)
        C255_okir="�����, �������� 2"
      case (4)
        C255_okir="����"
      case (5)
        C255_okir="������, �������� 1"
      case (6)
        C255_okir="???"
      case (7)
        C255_okir="???"
      case (8)
        C255_okir="???"
      case (9)
        C255_okir="???"
      case (10)
        C255_okir="???"
      case (11)
        C255_okir="???"
      case (12)
        C255_okir="???"
      case (13)
        C255_okir="???"
      case (14)
        C255_okir="???"
      case (15)
        C255_okir="???"
      case (16)
        C255_okir="???"
      case (17)
        C255_okir="???"
      case (18)
        C255_okir="???"
      case (19)
        C255_okir="???"
      case (20)
        C255_okir="???"
      case (21)
        C255_okir="???"
      case (22)
        C255_okir="???"
      case (23)
        C255_okir="???"
      case (24)
        C255_okir="???"
      case (25)
        C255_okir="???"
      case (26)
        C255_okir="???"
      case (27)
        C255_okir="???"
      case (28)
        C255_okir="???"
      case (29)
        C255_okir="???"
      case (30)
        C255_okir="???"
      end select
C 1762-11-0006_log.fgi( 182, 270):MarkVI text out 30 variants,1762-11-0006_RolZ_state
      L_(654)=.false.
C 1762-11-0006_log.fgi(  45, 132):��������� ���������� (�������)
      L_(653)=.false.
C 1762-11-0006_log.fgi(  45, 136):��������� ���������� (�������)
      L0_esir=R0_urir.ne.R0_orir
      R0_orir=R0_urir
C 1762-11-0006_log.fgi(  14, 134):���������� ������������� ������
      if(L_(653).or.L_(654)) then
         L_isir=(L_(653).or.L_isir).and..not.(L_(654))
      else
         if(L0_esir.and..not.L0_asir) L_isir=.not.L_isir
      endif
      L0_asir=L0_esir
      L_(655)=.not.L_isir
C 1762-11-0006_log.fgi(  51, 134):T �������
      L_alir=L_isir
C 1762-11-0006_log.fgi( 104, 140):������,1762-11-0006_lamp10
      if(L_isir) then
         I_irir=1
      else
         I_irir=0
      endif
C 1762-11-0006_log.fgi(  88, 136):��������� LO->1
      L_(657)=.false.
C 1762-11-0006_log.fgi(  45, 144):��������� ���������� (�������)
      L_(656)=.false.
C 1762-11-0006_log.fgi(  45, 148):��������� ���������� (�������)
      L0_itir=R0_atir.ne.R0_usir
      R0_usir=R0_atir
C 1762-11-0006_log.fgi(  14, 146):���������� ������������� ������
      if(L_(656).or.L_(657)) then
         L_otir=(L_(656).or.L_otir).and..not.(L_(657))
      else
         if(L0_itir.and..not.L0_etir) L_otir=.not.L_otir
      endif
      L0_etir=L0_itir
      L_(658)=.not.L_otir
C 1762-11-0006_log.fgi(  51, 146):T �������
      L_elir=L_otir
C 1762-11-0006_log.fgi( 104, 152):������,1762-11-0006_lamp09
      if(L_otir) then
         I_osir=1
      else
         I_osir=0
      endif
C 1762-11-0006_log.fgi(  88, 148):��������� LO->1
      L_(660)=.false.
C 1762-11-0006_log.fgi(  45, 178):��������� ���������� (�������)
      L_(659)=.false.
C 1762-11-0006_log.fgi(  45, 182):��������� ���������� (�������)
      L0_ovir=R0_evir.ne.R0_avir
      R0_avir=R0_evir
C 1762-11-0006_log.fgi(  14, 180):���������� ������������� ������
      if(L_(659).or.L_(660)) then
         L_uvir=(L_(659).or.L_uvir).and..not.(L_(660))
      else
         if(L0_ovir.and..not.L0_ivir) L_uvir=.not.L_uvir
      endif
      L0_ivir=L0_ovir
      L_(661)=.not.L_uvir
C 1762-11-0006_log.fgi(  51, 180):T �������
      L_ilir=L_uvir
C 1762-11-0006_log.fgi( 104, 186):������,1762-11-0006_lamp08
      if(L_uvir) then
         I_utir=1
      else
         I_utir=0
      endif
C 1762-11-0006_log.fgi(  88, 182):��������� LO->1
      L_(663)=.false.
C 1762-11-0006_log.fgi(  45, 190):��������� ���������� (�������)
      L_(662)=.false.
C 1762-11-0006_log.fgi(  45, 194):��������� ���������� (�������)
      L0_uxir=R0_ixir.ne.R0_exir
      R0_exir=R0_ixir
C 1762-11-0006_log.fgi(  14, 192):���������� ������������� ������
      if(L_(662).or.L_(663)) then
         L_abor=(L_(662).or.L_abor).and..not.(L_(663))
      else
         if(L0_uxir.and..not.L0_oxir) L_abor=.not.L_abor
      endif
      L0_oxir=L0_uxir
      L_(664)=.not.L_abor
C 1762-11-0006_log.fgi(  51, 192):T �������
      L_olir=L_abor
C 1762-11-0006_log.fgi( 104, 198):������,1762-11-0006_lamp07
      if(L_abor) then
         I_axir=1
      else
         I_axir=0
      endif
C 1762-11-0006_log.fgi(  88, 194):��������� LO->1
      L_(666)=.false.
C 1762-11-0006_log.fgi(  45, 202):��������� ���������� (�������)
      L_(665)=.false.
C 1762-11-0006_log.fgi(  45, 206):��������� ���������� (�������)
      L0_ador=R0_obor.ne.R0_ibor
      R0_ibor=R0_obor
C 1762-11-0006_log.fgi(  14, 204):���������� ������������� ������
      if(L_(665).or.L_(666)) then
         L_edor=(L_(665).or.L_edor).and..not.(L_(666))
      else
         if(L0_ador.and..not.L0_ubor) L_edor=.not.L_edor
      endif
      L0_ubor=L0_ador
      L_(667)=.not.L_edor
C 1762-11-0006_log.fgi(  51, 204):T �������
      L_ulir=L_edor
C 1762-11-0006_log.fgi( 104, 210):������,1762-11-0006_lamp06
      if(L_edor) then
         I_ebor=1
      else
         I_ebor=0
      endif
C 1762-11-0006_log.fgi(  88, 206):��������� LO->1
      L_(669)=.false.
C 1762-11-0006_log.fgi(  45, 214):��������� ���������� (�������)
      L_(668)=.false.
C 1762-11-0006_log.fgi(  45, 218):��������� ���������� (�������)
      L0_efor=R0_udor.ne.R0_odor
      R0_odor=R0_udor
C 1762-11-0006_log.fgi(  14, 216):���������� ������������� ������
      if(L_(668).or.L_(669)) then
         L_ifor=(L_(668).or.L_ifor).and..not.(L_(669))
      else
         if(L0_efor.and..not.L0_afor) L_ifor=.not.L_ifor
      endif
      L0_afor=L0_efor
      L_(670)=.not.L_ifor
C 1762-11-0006_log.fgi(  51, 216):T �������
      L_amir=L_ifor
C 1762-11-0006_log.fgi( 104, 222):������,1762-11-0006_lamp05
      if(L_ifor) then
         I_idor=1
      else
         I_idor=0
      endif
C 1762-11-0006_log.fgi(  88, 218):��������� LO->1
      L_(672)=.false.
C 1762-11-0006_log.fgi(  45, 226):��������� ���������� (�������)
      L_(671)=.false.
C 1762-11-0006_log.fgi(  45, 230):��������� ���������� (�������)
      L0_ikor=R0_akor.ne.R0_ufor
      R0_ufor=R0_akor
C 1762-11-0006_log.fgi(  14, 228):���������� ������������� ������
      if(L_(671).or.L_(672)) then
         L_okor=(L_(671).or.L_okor).and..not.(L_(672))
      else
         if(L0_ikor.and..not.L0_ekor) L_okor=.not.L_okor
      endif
      L0_ekor=L0_ikor
      L_(673)=.not.L_okor
C 1762-11-0006_log.fgi(  51, 228):T �������
      L_emir=L_okor
C 1762-11-0006_log.fgi( 104, 234):������,1762-11-0006_lamp04
      if(L_okor) then
         I_ofor=1
      else
         I_ofor=0
      endif
C 1762-11-0006_log.fgi(  88, 230):��������� LO->1
      L_(675)=.false.
C 1762-11-0006_log.fgi(  45, 238):��������� ���������� (�������)
      L_(674)=.false.
C 1762-11-0006_log.fgi(  45, 242):��������� ���������� (�������)
      L0_olor=R0_elor.ne.R0_alor
      R0_alor=R0_elor
C 1762-11-0006_log.fgi(  14, 240):���������� ������������� ������
      if(L_(674).or.L_(675)) then
         L_ulor=(L_(674).or.L_ulor).and..not.(L_(675))
      else
         if(L0_olor.and..not.L0_ilor) L_ulor=.not.L_ulor
      endif
      L0_ilor=L0_olor
      L_(676)=.not.L_ulor
C 1762-11-0006_log.fgi(  51, 240):T �������
      L_imir=L_ulor
C 1762-11-0006_log.fgi( 104, 246):������,1762-11-0006_lamp03
      if(L_ulor) then
         I_ukor=1
      else
         I_ukor=0
      endif
C 1762-11-0006_log.fgi(  88, 242):��������� LO->1
      L_(678)=.false.
C 1762-11-0006_log.fgi(  45, 250):��������� ���������� (�������)
      L_(677)=.false.
C 1762-11-0006_log.fgi(  45, 254):��������� ���������� (�������)
      L0_umor=R0_imor.ne.R0_emor
      R0_emor=R0_imor
C 1762-11-0006_log.fgi(  14, 252):���������� ������������� ������
      if(L_(677).or.L_(678)) then
         L_apor=(L_(677).or.L_apor).and..not.(L_(678))
      else
         if(L0_umor.and..not.L0_omor) L_apor=.not.L_apor
      endif
      L0_omor=L0_umor
      L_(679)=.not.L_apor
C 1762-11-0006_log.fgi(  51, 252):T �������
      L_omir=L_apor
C 1762-11-0006_log.fgi( 104, 258):������,1762-11-0006_lamp02
      if(L_apor) then
         I_amor=1
      else
         I_amor=0
      endif
C 1762-11-0006_log.fgi(  88, 254):��������� LO->1
      L_(681)=.false.
C 1762-11-0006_log.fgi(  45, 262):��������� ���������� (�������)
      L_(680)=.false.
C 1762-11-0006_log.fgi(  45, 266):��������� ���������� (�������)
      L0_aror=R0_opor.ne.R0_ipor
      R0_ipor=R0_opor
C 1762-11-0006_log.fgi(  14, 264):���������� ������������� ������
      if(L_(680).or.L_(681)) then
         L_eror=(L_(680).or.L_eror).and..not.(L_(681))
      else
         if(L0_aror.and..not.L0_upor) L_eror=.not.L_eror
      endif
      L0_upor=L0_aror
      L_(682)=.not.L_eror
C 1762-11-0006_log.fgi(  51, 264):T �������
      L_umir=L_eror
C 1762-11-0006_log.fgi( 104, 270):������,1762-11-0006_lamp01
      L_(652) = L_okor.OR.L_ulor.OR.L_apor.OR.L_eror
C 1762-11-0006_log.fgi(  79, 165):���
      select case (L_(652))
      case (.FALSE.)
        C255_opir="����������(�) ���������(�)"
      case (.TRUE.)
        C255_opir="����������(�) ��������(�)"
      end select

      select case (L_(652))
      case (.FALSE.)
        I_ipir=I_epir
      case (.TRUE.)
        I_ipir=I_apir
      end select

      select case (L_(652))
      case (.FALSE.)
        I_erir=I_arir
      case (.TRUE.)
        I_erir=I_upir
      end select
C 1762-11-0006_log.fgi( 104, 166):MarkVI text out,1762-11-0006_blkonoff
      if(L_eror) then
         I_epor=1
      else
         I_epor=0
      endif
C 1762-11-0006_log.fgi(  88, 266):��������� LO->1
      L_(684)=.false.
C 1762-11-0001_log.fgi(  45,  47):��������� ���������� (�������)
      L_(683)=.false.
C 1762-11-0001_log.fgi(  45,  51):��������� ���������� (�������)
      L0_isor=R0_asor.ne.R0_uror
      R0_uror=R0_asor
C 1762-11-0001_log.fgi(  14,  49):���������� ������������� ������
      if(L_(683).or.L_(684)) then
         L_osor=(L_(683).or.L_osor).and..not.(L_(684))
      else
         if(L0_isor.and..not.L0_esor) L_osor=.not.L_osor
      endif
      L0_esor=L0_isor
      L_(685)=.not.L_osor
C 1762-11-0001_log.fgi(  51,  49):T �������
      L_iror=L_osor
C 1762-11-0001_log.fgi( 104,  55):������,1762-11-0001_lamp17
      if(L_osor) then
         I_oror=1
      else
         I_oror=0
      endif
C 1762-11-0001_log.fgi(  88,  51):��������� LO->1
      L_(687)=.false.
C 1762-11-0001_log.fgi(  45,  60):��������� ���������� (�������)
      L_(686)=.false.
C 1762-11-0001_log.fgi(  45,  64):��������� ���������� (�������)
      L0_utor=R0_itor.ne.R0_etor
      R0_etor=R0_itor
C 1762-11-0001_log.fgi(  14,  62):���������� ������������� ������
      if(L_(686).or.L_(687)) then
         L_avor=(L_(686).or.L_avor).and..not.(L_(687))
      else
         if(L0_utor.and..not.L0_otor) L_avor=.not.L_avor
      endif
      L0_otor=L0_utor
      L_(688)=.not.L_avor
C 1762-11-0001_log.fgi(  51,  62):T �������
      L_usor=L_avor
C 1762-11-0001_log.fgi( 104,  68):������,1762-11-0001_lamp16
      if(L_avor) then
         I_ator=1
      else
         I_ator=0
      endif
C 1762-11-0001_log.fgi(  88,  64):��������� LO->1
      L_(690)=.false.
C 1762-11-0001_log.fgi(  45,  72):��������� ���������� (�������)
      L_(689)=.false.
C 1762-11-0001_log.fgi(  45,  76):��������� ���������� (�������)
      L0_exor=R0_uvor.ne.R0_ovor
      R0_ovor=R0_uvor
C 1762-11-0001_log.fgi(  14,  74):���������� ������������� ������
      if(L_(689).or.L_(690)) then
         L_ixor=(L_(689).or.L_ixor).and..not.(L_(690))
      else
         if(L0_exor.and..not.L0_axor) L_ixor=.not.L_ixor
      endif
      L0_axor=L0_exor
      L_(691)=.not.L_ixor
C 1762-11-0001_log.fgi(  51,  74):T �������
      L_evor=L_ixor
C 1762-11-0001_log.fgi( 104,  80):������,1762-11-0001_lamp15
      if(L_ixor) then
         I_ivor=1
      else
         I_ivor=0
      endif
C 1762-11-0001_log.fgi(  88,  76):��������� LO->1
      L_(695)=.false.
C 1762-11-0001_log.fgi(  45,  84):��������� ���������� (�������)
      L_(694)=.false.
C 1762-11-0001_log.fgi(  45,  88):��������� ���������� (�������)
      L0_olur=R0_elur.ne.R0_alur
      R0_alur=R0_elur
C 1762-11-0001_log.fgi(  14,  86):���������� ������������� ������
      if(L_(694).or.L_(695)) then
         L_ulur=(L_(694).or.L_ulur).and..not.(L_(695))
      else
         if(L0_olur.and..not.L0_ilur) L_ulur=.not.L_ulur
      endif
      L0_ilur=L0_olur
      L_(696)=.not.L_ulur
C 1762-11-0001_log.fgi(  51,  86):T �������
      L_oxor=L_ulur
C 1762-11-0001_log.fgi( 104,  92):������,1762-11-0001_lamp14
      if(L_ulur) then
         I_ukur=1
      else
         I_ukur=0
      endif
C 1762-11-0001_log.fgi(  88,  88):��������� LO->1
      L_(698)=.false.
C 1762-11-0001_log.fgi(  45,  96):��������� ���������� (�������)
      L_(697)=.false.
C 1762-11-0001_log.fgi(  45, 100):��������� ���������� (�������)
      L0_umur=R0_imur.ne.R0_emur
      R0_emur=R0_imur
C 1762-11-0001_log.fgi(  14,  98):���������� ������������� ������
      if(L_(697).or.L_(698)) then
         L_apur=(L_(697).or.L_apur).and..not.(L_(698))
      else
         if(L0_umur.and..not.L0_omur) L_apur=.not.L_apur
      endif
      L0_omur=L0_umur
      L_(699)=.not.L_apur
C 1762-11-0001_log.fgi(  51,  98):T �������
      L_uxor=L_apur
C 1762-11-0001_log.fgi( 104, 104):������,1762-11-0001_lamp13
      if(L_apur) then
         I_amur=1
      else
         I_amur=0
      endif
C 1762-11-0001_log.fgi(  88, 100):��������� LO->1
      L_(701)=.false.
C 1762-11-0001_log.fgi(  45, 108):��������� ���������� (�������)
      L_(700)=.false.
C 1762-11-0001_log.fgi(  45, 112):��������� ���������� (�������)
      L0_arur=R0_opur.ne.R0_ipur
      R0_ipur=R0_opur
C 1762-11-0001_log.fgi(  14, 110):���������� ������������� ������
      if(L_(700).or.L_(701)) then
         L_erur=(L_(700).or.L_erur).and..not.(L_(701))
      else
         if(L0_arur.and..not.L0_upur) L_erur=.not.L_erur
      endif
      L0_upur=L0_arur
      L_(702)=.not.L_erur
C 1762-11-0001_log.fgi(  51, 110):T �������
      L_abur=L_erur
C 1762-11-0001_log.fgi( 104, 116):������,1762-11-0001_lamp12
      if(L_erur) then
         I_epur=1
      else
         I_epur=0
      endif
C 1762-11-0001_log.fgi(  88, 112):��������� LO->1
      L_(704)=.false.
C 1762-11-0001_log.fgi(  45, 120):��������� ���������� (�������)
      L_(703)=.false.
C 1762-11-0001_log.fgi(  45, 124):��������� ���������� (�������)
      L0_esur=R0_urur.ne.R0_orur
      R0_orur=R0_urur
C 1762-11-0001_log.fgi(  14, 122):���������� ������������� ������
      if(L_(703).or.L_(704)) then
         L_isur=(L_(703).or.L_isur).and..not.(L_(704))
      else
         if(L0_esur.and..not.L0_asur) L_isur=.not.L_isur
      endif
      L0_asur=L0_esur
      L_(705)=.not.L_isur
C 1762-11-0001_log.fgi(  51, 122):T �������
      L_ebur=L_isur
C 1762-11-0001_log.fgi( 104, 128):������,1762-11-0001_lamp11
      if(L_isur) then
         I_irur=1
      else
         I_irur=0
      endif
C 1762-11-0001_log.fgi(  88, 124):��������� LO->1
      L_(707)=.false.
C 1762-11-0001_log.fgi(  45, 132):��������� ���������� (�������)
      L_(706)=.false.
C 1762-11-0001_log.fgi(  45, 136):��������� ���������� (�������)
      L0_itur=R0_atur.ne.R0_usur
      R0_usur=R0_atur
C 1762-11-0001_log.fgi(  14, 134):���������� ������������� ������
      if(L_(706).or.L_(707)) then
         L_otur=(L_(706).or.L_otur).and..not.(L_(707))
      else
         if(L0_itur.and..not.L0_etur) L_otur=.not.L_otur
      endif
      L0_etur=L0_itur
      L_(708)=.not.L_otur
C 1762-11-0001_log.fgi(  51, 134):T �������
      L_ibur=L_otur
C 1762-11-0001_log.fgi( 104, 140):������,1762-11-0001_lamp10
      if(L_otur) then
         I_osur=1
      else
         I_osur=0
      endif
C 1762-11-0001_log.fgi(  88, 136):��������� LO->1
      L_(710)=.false.
C 1762-11-0001_log.fgi(  45, 144):��������� ���������� (�������)
      L_(709)=.false.
C 1762-11-0001_log.fgi(  45, 148):��������� ���������� (�������)
      L0_ovur=R0_evur.ne.R0_avur
      R0_avur=R0_evur
C 1762-11-0001_log.fgi(  14, 146):���������� ������������� ������
      if(L_(709).or.L_(710)) then
         L_uvur=(L_(709).or.L_uvur).and..not.(L_(710))
      else
         if(L0_ovur.and..not.L0_ivur) L_uvur=.not.L_uvur
      endif
      L0_ivur=L0_ovur
      L_(711)=.not.L_uvur
C 1762-11-0001_log.fgi(  51, 146):T �������
      L_obur=L_uvur
C 1762-11-0001_log.fgi( 104, 152):������,1762-11-0001_lamp9
      if(L_uvur) then
         I_utur=1
      else
         I_utur=0
      endif
C 1762-11-0001_log.fgi(  88, 148):��������� LO->1
      L_(713)=.false.
C 1762-11-0001_log.fgi(  45, 178):��������� ���������� (�������)
      L_(712)=.false.
C 1762-11-0001_log.fgi(  45, 182):��������� ���������� (�������)
      L0_uxur=R0_ixur.ne.R0_exur
      R0_exur=R0_ixur
C 1762-11-0001_log.fgi(  14, 180):���������� ������������� ������
      if(L_(712).or.L_(713)) then
         L_abas=(L_(712).or.L_abas).and..not.(L_(713))
      else
         if(L0_uxur.and..not.L0_oxur) L_abas=.not.L_abas
      endif
      L0_oxur=L0_uxur
      L_(714)=.not.L_abas
C 1762-11-0001_log.fgi(  51, 180):T �������
      L_ubur=L_abas
C 1762-11-0001_log.fgi( 104, 186):������,1762-11-0001_lamp8
      if(L_abas) then
         I_axur=1
      else
         I_axur=0
      endif
C 1762-11-0001_log.fgi(  88, 182):��������� LO->1
      L_(716)=.false.
C 1762-11-0001_log.fgi(  45, 190):��������� ���������� (�������)
      L_(715)=.false.
C 1762-11-0001_log.fgi(  45, 194):��������� ���������� (�������)
      L0_adas=R0_obas.ne.R0_ibas
      R0_ibas=R0_obas
C 1762-11-0001_log.fgi(  14, 192):���������� ������������� ������
      if(L_(715).or.L_(716)) then
         L_edas=(L_(715).or.L_edas).and..not.(L_(716))
      else
         if(L0_adas.and..not.L0_ubas) L_edas=.not.L_edas
      endif
      L0_ubas=L0_adas
      L_(717)=.not.L_edas
C 1762-11-0001_log.fgi(  51, 192):T �������
      L_adur=L_edas
C 1762-11-0001_log.fgi( 104, 198):������,1762-11-0001_lamp7
      if(L_edas) then
         I_ebas=1
      else
         I_ebas=0
      endif
C 1762-11-0001_log.fgi(  88, 194):��������� LO->1
      L_(719)=.false.
C 1762-11-0001_log.fgi(  45, 202):��������� ���������� (�������)
      L_(718)=.false.
C 1762-11-0001_log.fgi(  45, 206):��������� ���������� (�������)
      L0_efas=R0_udas.ne.R0_odas
      R0_odas=R0_udas
C 1762-11-0001_log.fgi(  14, 204):���������� ������������� ������
      if(L_(718).or.L_(719)) then
         L_ifas=(L_(718).or.L_ifas).and..not.(L_(719))
      else
         if(L0_efas.and..not.L0_afas) L_ifas=.not.L_ifas
      endif
      L0_afas=L0_efas
      L_(720)=.not.L_ifas
C 1762-11-0001_log.fgi(  51, 204):T �������
      L_edur=L_ifas
C 1762-11-0001_log.fgi( 104, 210):������,1762-11-0001_lamp6
      if(L_ifas) then
         I_idas=1
      else
         I_idas=0
      endif
C 1762-11-0001_log.fgi(  88, 206):��������� LO->1
      L_(722)=.false.
C 1762-11-0001_log.fgi(  45, 214):��������� ���������� (�������)
      L_(721)=.false.
C 1762-11-0001_log.fgi(  45, 218):��������� ���������� (�������)
      L0_ikas=R0_akas.ne.R0_ufas
      R0_ufas=R0_akas
C 1762-11-0001_log.fgi(  14, 216):���������� ������������� ������
      if(L_(721).or.L_(722)) then
         L_okas=(L_(721).or.L_okas).and..not.(L_(722))
      else
         if(L0_ikas.and..not.L0_ekas) L_okas=.not.L_okas
      endif
      L0_ekas=L0_ikas
      L_(723)=.not.L_okas
C 1762-11-0001_log.fgi(  51, 216):T �������
      L_idur=L_okas
C 1762-11-0001_log.fgi( 104, 222):������,1762-11-0001_lamp5
      if(L_okas) then
         I_ofas=1
      else
         I_ofas=0
      endif
C 1762-11-0001_log.fgi(  88, 218):��������� LO->1
      L_(725)=.false.
C 1762-11-0001_log.fgi(  45, 226):��������� ���������� (�������)
      L_(724)=.false.
C 1762-11-0001_log.fgi(  45, 230):��������� ���������� (�������)
      L0_olas=R0_elas.ne.R0_alas
      R0_alas=R0_elas
C 1762-11-0001_log.fgi(  14, 228):���������� ������������� ������
      if(L_(724).or.L_(725)) then
         L_ulas=(L_(724).or.L_ulas).and..not.(L_(725))
      else
         if(L0_olas.and..not.L0_ilas) L_ulas=.not.L_ulas
      endif
      L0_ilas=L0_olas
      L_(726)=.not.L_ulas
C 1762-11-0001_log.fgi(  51, 228):T �������
      L_odur=L_ulas
C 1762-11-0001_log.fgi( 104, 234):������,1762-11-0001_lamp4
      if(L_ulas) then
         I_ukas=1
      else
         I_ukas=0
      endif
C 1762-11-0001_log.fgi(  88, 230):��������� LO->1
      L_(728)=.false.
C 1762-11-0001_log.fgi(  45, 238):��������� ���������� (�������)
      L_(727)=.false.
C 1762-11-0001_log.fgi(  45, 242):��������� ���������� (�������)
      L0_umas=R0_imas.ne.R0_emas
      R0_emas=R0_imas
C 1762-11-0001_log.fgi(  14, 240):���������� ������������� ������
      if(L_(727).or.L_(728)) then
         L_apas=(L_(727).or.L_apas).and..not.(L_(728))
      else
         if(L0_umas.and..not.L0_omas) L_apas=.not.L_apas
      endif
      L0_omas=L0_umas
      L_(729)=.not.L_apas
C 1762-11-0001_log.fgi(  51, 240):T �������
      L_udur=L_apas
C 1762-11-0001_log.fgi( 104, 246):������,1762-11-0001_lamp3
      if(L_apas) then
         I_amas=1
      else
         I_amas=0
      endif
C 1762-11-0001_log.fgi(  88, 242):��������� LO->1
      L_(731)=.false.
C 1762-11-0001_log.fgi(  45, 250):��������� ���������� (�������)
      L_(730)=.false.
C 1762-11-0001_log.fgi(  45, 254):��������� ���������� (�������)
      L0_aras=R0_opas.ne.R0_ipas
      R0_ipas=R0_opas
C 1762-11-0001_log.fgi(  14, 252):���������� ������������� ������
      if(L_(730).or.L_(731)) then
         L_eras=(L_(730).or.L_eras).and..not.(L_(731))
      else
         if(L0_aras.and..not.L0_upas) L_eras=.not.L_eras
      endif
      L0_upas=L0_aras
      L_(732)=.not.L_eras
C 1762-11-0001_log.fgi(  51, 252):T �������
      L_afur=L_eras
C 1762-11-0001_log.fgi( 104, 258):������,1762-11-0001_lamp2
      if(L_eras) then
         I_epas=1
      else
         I_epas=0
      endif
C 1762-11-0001_log.fgi(  88, 254):��������� LO->1
      L_(734)=.false.
C 1762-11-0001_log.fgi(  45, 262):��������� ���������� (�������)
      L_(733)=.false.
C 1762-11-0001_log.fgi(  45, 266):��������� ���������� (�������)
      L0_esas=R0_uras.ne.R0_oras
      R0_oras=R0_uras
C 1762-11-0001_log.fgi(  14, 264):���������� ������������� ������
      if(L_(733).or.L_(734)) then
         L_isas=(L_(733).or.L_isas).and..not.(L_(734))
      else
         if(L0_esas.and..not.L0_asas) L_isas=.not.L_isas
      endif
      L0_asas=L0_esas
      L_(735)=.not.L_isas
C 1762-11-0001_log.fgi(  51, 264):T �������
      L_efur=L_isas
C 1762-11-0001_log.fgi( 104, 270):������,1762-11-0001_lamp1
      L_(693) = L_abas.OR.L_edas.OR.L_ifas.OR.L_okas.OR.L_ulas.OR.L_apas
     &.OR.L_eras.OR.L_isas
C 1762-11-0001_log.fgi(  79, 169):���
      select case (L_(693))
      case (.FALSE.)
        C255_akur="����������(�) ���������(�)"
      case (.TRUE.)
        C255_akur="����������(�) ��������(�)"
      end select

      select case (L_(693))
      case (.FALSE.)
        I_ufur=I_ofur
      case (.TRUE.)
        I_ufur=I_ifur
      end select

      select case (L_(693))
      case (.FALSE.)
        I_okur=I_ikur
      case (.TRUE.)
        I_okur=I_ekur
      end select
C 1762-11-0001_log.fgi( 104, 170):MarkVI text out,1762-11-0001_blkonoff
      if(L_isas) then
         I_iras=1
      else
         I_iras=0
      endif
C 1762-11-0001_log.fgi(  88, 266):��������� LO->1
      !�����. �������� I0_avas = 1762-10-0001_logC?? /z'0100000A'
C /
C 1762-10-0001_log.fgi( 353, 279):��������� �����
      !�����. �������� I0_evas = 1762-10-0001_logC?? /z'01000003'
C /
C 1762-10-0001_log.fgi( 353, 281):��������� �����
      !Constant L_(737) = 1762-10-0001_logClJ423 /.false.
C /
      L_(737)=L0_utas
C 1762-10-0001_log.fgi( 358, 272):��������� ���. ������������� 
      if(L_(737)) then
         I_udes=I0_avas
      else
         I_udes=I0_evas
      endif
C 1762-10-0001_log.fgi( 365, 279):���� RE IN LO CH7
      I_odes=I_udes
C 1762-10-0001_log.fgi( 395, 280):������,1762-10-0001_ellipce1_color
      I_ides=I_udes
C 1762-10-0001_log.fgi( 395, 276):������,1762-10-0001_ellipce2_color
      I_edes=I_udes
C 1762-10-0001_log.fgi( 395, 272):������,1762-10-0001_ellipce3_color
      I_ades=I_udes
C 1762-10-0001_log.fgi( 395, 268):������,1762-10-0001_ellipce4_color
      I_ubes=I_udes
C 1762-10-0001_log.fgi( 395, 264):������,1762-10-0001_ellipce5_color
      I_obes=I_udes
C 1762-10-0001_log.fgi( 395, 260):������,1762-10-0001_ellipce6_color
      I_ibes=I_udes
C 1762-10-0001_log.fgi( 395, 256):������,1762-10-0001_ellipce7_color
      I_ebes=I_udes
C 1762-10-0001_log.fgi( 395, 252):������,1762-10-0001_ellipce8_color
      I_abes=I_udes
C 1762-10-0001_log.fgi( 395, 248):������,1762-10-0001_ellipce9_color
      I_uxas=I_udes
C 1762-10-0001_log.fgi( 395, 244):������,1762-10-0001_ellipce10_color
      I_oxas=I_udes
C 1762-10-0001_log.fgi( 395, 240):������,1762-10-0001_ellipce11_color
      I_ixas=I_udes
C 1762-10-0001_log.fgi( 395, 236):������,1762-10-0001_ellipce12_color
      I_exas=I_udes
C 1762-10-0001_log.fgi( 395, 232):������,1762-10-0001_ellipce13_color
      I_axas=I_udes
C 1762-10-0001_log.fgi( 395, 226):������,1762-10-0001_poly1_color
      I_uvas=I_udes
C 1762-10-0001_log.fgi( 395, 222):������,1762-10-0001_poly2_color
      I_ovas=I_udes
C 1762-10-0001_log.fgi( 395, 218):������,1762-10-0001_poly3_color
      I_ivas=I_udes
C 1762-10-0001_log.fgi( 395, 214):������,1762-10-0001_poly4_color
      I_otas=I_udes
C 1762-10-0001_log.fgi( 395, 210):������,1762-10-0001_poly5_color
      I_itas=I_udes
C 1762-10-0001_log.fgi( 395, 206):������,1762-10-0001_poly6_color
      I_etas=I_udes
C 1762-10-0001_log.fgi( 395, 202):������,1762-10-0001_poly7_color
      I_atas=I_udes
C 1762-10-0001_log.fgi( 395, 198):������,1762-10-0001_poly8_color
      I_usas=I_udes
C 1762-10-0001_log.fgi( 395, 194):������,1762-10-0001_poly9_color
      I_osas=I_udes
C 1762-10-0001_log.fgi( 395, 190):������,1762-10-0001_poly10_color
      L0_epes=R0_opes.ne.R0_ipes
      R0_ipes=R0_opes
C 1762-10-0001_log.fgi( 109,  11):���������� ������������� ������
      L0_upes=R0_eres.ne.R0_ares
      R0_ares=R0_eres
C 1762-10-0001_log.fgi( 109,  23):���������� ������������� ������
      L_apes=L0_upes.or.(L_apes.and..not.(L0_epes))
      L_(740)=.not.L_apes
C 1762-10-0001_log.fgi( 128,  21):RS �������
      select case (L_apes)
      case (.FALSE.)
        C255_emes="������� ��������� ��������"
      case (.TRUE.)
        C255_emes="������� ��������� ��������"
      end select

      select case (L_apes)
      case (.FALSE.)
        I_ames=I_ules
      case (.TRUE.)
        I_ames=I_oles
      end select

      select case (L_apes)
      case (.FALSE.)
        I_umes=I_omes
      case (.TRUE.)
        I_umes=I_imes
      end select
C 1762-10-0001_log.fgi( 155,  24):MarkVI text out,CoordTType
      do loop_var=1,40
        if(L_apes) then
           I_ales(loop_var)=I_eles(loop_var)
        else
           I_ales(loop_var)=I_iles(loop_var)
        endif
C 1762-10-0001_log.fgi( 137,  36):���� RE IN LO CH7
      end do
      L_(742)=.false.
C 1762-10-0001_log.fgi( 341,  88):��������� ���������� (�������)
      if(L_(742)) then
         I0_ures=0
      endif
      I_(21)=I0_ures
      L_(741)=I0_ures.ne.0
C 1762-10-0001_log.fgi( 349,  90):���������� ��������� 
      L_(747) = (.NOT.L_(741))
C 1762-10-0001_log.fgi( 361,  95):���
      L_(746)=I_(21).eq.1
      L_(745)=I_(21).eq.2
      L_(744)=I_(21).eq.3
      L_(743)=I_(21).eq.4
C 1762-10-0001_log.fgi( 361,  90):���������� �� 4
      L0_eris=.false.
      I0_aris=0

      if(L0_upav)then
        I0_oris=1
         L0_upis=.false.
         L0_ofis=.false.
         L0_ives=.false.
         L0_utes=.false.
         L0_otes=.false.
         L0_etes=.false.
         L0_uses=.false.
         L0_ises=.false.
         L0_ases=.false.
         L0_ipis=.false.
         L0_apis=.false.
         L0_omis=.false.
         L0_emis=.false.
         L0_ulis=.false.
         L0_ilis=.false.
         L0_alis=.false.
         L0_okis=.false.
         L0_ekis=.false.
         L0_ufis=.false.
         L0_efis=.false.
         L0_udis=.false.
         L0_idis=.false.
         L0_adis=.false.
         L0_obis=.false.
         L0_ebis=.false.
         L0_uxes=.false.
         L0_ixes=.false.
         L0_axes=.false.
         L0_oves=.false.
         L0_aves=.false.
      endif

      if(L0_upis)I0_aris=I0_aris+1
      if(L0_ofis)I0_aris=I0_aris+1
      if(L0_ives)I0_aris=I0_aris+1
      if(L0_utes)I0_aris=I0_aris+1
      if(L0_otes)I0_aris=I0_aris+1
      if(L0_etes)I0_aris=I0_aris+1
      if(L0_uses)I0_aris=I0_aris+1
      if(L0_ises)I0_aris=I0_aris+1
      if(L0_ases)I0_aris=I0_aris+1
      if(L0_ipis)I0_aris=I0_aris+1
      if(L0_apis)I0_aris=I0_aris+1
      if(L0_omis)I0_aris=I0_aris+1
      if(L0_emis)I0_aris=I0_aris+1
      if(L0_ulis)I0_aris=I0_aris+1
      if(L0_ilis)I0_aris=I0_aris+1
      if(L0_alis)I0_aris=I0_aris+1
      if(L0_okis)I0_aris=I0_aris+1
      if(L0_ekis)I0_aris=I0_aris+1
      if(L0_ufis)I0_aris=I0_aris+1
      if(L0_efis)I0_aris=I0_aris+1
      if(L0_udis)I0_aris=I0_aris+1
      if(L0_idis)I0_aris=I0_aris+1
      if(L0_adis)I0_aris=I0_aris+1
      if(L0_obis)I0_aris=I0_aris+1
      if(L0_ebis)I0_aris=I0_aris+1
      if(L0_uxes)I0_aris=I0_aris+1
      if(L0_ixes)I0_aris=I0_aris+1
      if(L0_axes)I0_aris=I0_aris+1
      if(L0_oves)I0_aris=I0_aris+1
      if(L0_aves)I0_aris=I0_aris+1


      if(.not.L0_eris.and.L_(747).and..not.L0_upis)then
        I0_oris=1
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L_(746).and..not.L0_ofis)then
        I0_oris=2
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L_(745).and..not.L0_ives)then
        I0_oris=3
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L_(744).and..not.L0_utes)then
        I0_oris=4
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L_(743).and..not.L0_otes)then
        I0_oris=5
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_ites.and..not.L0_etes)then
        I0_oris=6
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_ates.and..not.L0_uses)then
        I0_oris=7
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_oses.and..not.L0_ises)then
        I0_oris=8
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_eses.and..not.L0_ases)then
        I0_oris=9
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_opis.and..not.L0_ipis)then
        I0_oris=10
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_epis.and..not.L0_apis)then
        I0_oris=11
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_umis.and..not.L0_omis)then
        I0_oris=12
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_imis.and..not.L0_emis)then
        I0_oris=13
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_amis.and..not.L0_ulis)then
        I0_oris=14
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_olis.and..not.L0_ilis)then
        I0_oris=15
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_elis.and..not.L0_alis)then
        I0_oris=16
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_ukis.and..not.L0_okis)then
        I0_oris=17
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_ikis.and..not.L0_ekis)then
        I0_oris=18
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_akis.and..not.L0_ufis)then
        I0_oris=19
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_ifis.and..not.L0_efis)then
        I0_oris=20
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_afis.and..not.L0_udis)then
        I0_oris=21
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_odis.and..not.L0_idis)then
        I0_oris=22
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_edis.and..not.L0_adis)then
        I0_oris=23
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_ubis.and..not.L0_obis)then
        I0_oris=24
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_ibis.and..not.L0_ebis)then
        I0_oris=25
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_abis.and..not.L0_uxes)then
        I0_oris=26
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_oxes.and..not.L0_ixes)then
        I0_oris=27
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_exes.and..not.L0_axes)then
        I0_oris=28
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_uves.and..not.L0_oves)then
        I0_oris=29
        L0_eris=.true.
      endif

      if(.not.L0_eris.and.L0_eves.and..not.L0_aves)then
        I0_oris=30
        L0_eris=.true.
      endif

      if(I0_aris.eq.1)then
         L0_upis=.false.
         L0_ofis=.false.
         L0_ives=.false.
         L0_utes=.false.
         L0_otes=.false.
         L0_etes=.false.
         L0_uses=.false.
         L0_ises=.false.
         L0_ases=.false.
         L0_ipis=.false.
         L0_apis=.false.
         L0_omis=.false.
         L0_emis=.false.
         L0_ulis=.false.
         L0_ilis=.false.
         L0_alis=.false.
         L0_okis=.false.
         L0_ekis=.false.
         L0_ufis=.false.
         L0_efis=.false.
         L0_udis=.false.
         L0_idis=.false.
         L0_adis=.false.
         L0_obis=.false.
         L0_ebis=.false.
         L0_uxes=.false.
         L0_ixes=.false.
         L0_axes=.false.
         L0_oves=.false.
         L0_aves=.false.
      else
         L0_upis=L_(747)
         L0_ofis=L_(746)
         L0_ives=L_(745)
         L0_utes=L_(744)
         L0_otes=L_(743)
         L0_etes=L0_ites
         L0_uses=L0_ates
         L0_ises=L0_oses
         L0_ases=L0_eses
         L0_ipis=L0_opis
         L0_apis=L0_epis
         L0_omis=L0_umis
         L0_emis=L0_imis
         L0_ulis=L0_amis
         L0_ilis=L0_olis
         L0_alis=L0_elis
         L0_okis=L0_ukis
         L0_ekis=L0_ikis
         L0_ufis=L0_akis
         L0_efis=L0_ifis
         L0_udis=L0_afis
         L0_idis=L0_odis
         L0_adis=L0_edis
         L0_obis=L0_ubis
         L0_ebis=L0_ibis
         L0_uxes=L0_abis
         L0_ixes=L0_oxes
         L0_axes=L0_exes
         L0_oves=L0_uves
         L0_aves=L0_eves
      endif
C 1762-10-0001_log.fgi( 371,  66):��������� �������
      select case (I0_oris)
      case (1)
        C255_iris="�����.���������"
      case (2)
        C255_iris="�����, �������� 1"
      case (3)
        C255_iris="�����, �������� 2"
      case (4)
        C255_iris="����"
      case (5)
        C255_iris="������, �������� 1"
      case (6)
        C255_iris="???"
      case (7)
        C255_iris="???"
      case (8)
        C255_iris="???"
      case (9)
        C255_iris="???"
      case (10)
        C255_iris="???"
      case (11)
        C255_iris="???"
      case (12)
        C255_iris="???"
      case (13)
        C255_iris="???"
      case (14)
        C255_iris="???"
      case (15)
        C255_iris="???"
      case (16)
        C255_iris="???"
      case (17)
        C255_iris="???"
      case (18)
        C255_iris="???"
      case (19)
        C255_iris="???"
      case (20)
        C255_iris="???"
      case (21)
        C255_iris="???"
      case (22)
        C255_iris="???"
      case (23)
        C255_iris="???"
      case (24)
        C255_iris="???"
      case (25)
        C255_iris="???"
      case (26)
        C255_iris="???"
      case (27)
        C255_iris="???"
      case (28)
        C255_iris="???"
      case (29)
        C255_iris="???"
      case (30)
        C255_iris="???"
      end select
C 1762-10-0001_log.fgi( 387,  95):MarkVI text out 30 variants,RolV_state
      L_(749)=.false.
C 1762-10-0001_log.fgi( 279,  88):��������� ���������� (�������)
      if(L_(749)) then
         I0_uris=0
      endif
      I_(22)=I0_uris
      L_(748)=I0_uris.ne.0
C 1762-10-0001_log.fgi( 287,  90):���������� ��������� 
      L_(754) = (.NOT.L_(748))
C 1762-10-0001_log.fgi( 299,  95):���
      L_(753)=I_(22).eq.1
      L_(752)=I_(22).eq.2
      L_(751)=I_(22).eq.3
      L_(750)=I_(22).eq.4
C 1762-10-0001_log.fgi( 299,  90):���������� �� 4
      L0_eros=.false.
      I0_aros=0

      if(L0_upav)then
        I0_oros=1
         L0_upos=.false.
         L0_ofos=.false.
         L0_ivis=.false.
         L0_utis=.false.
         L0_otis=.false.
         L0_etis=.false.
         L0_usis=.false.
         L0_isis=.false.
         L0_asis=.false.
         L0_ipos=.false.
         L0_apos=.false.
         L0_omos=.false.
         L0_emos=.false.
         L0_ulos=.false.
         L0_ilos=.false.
         L0_alos=.false.
         L0_okos=.false.
         L0_ekos=.false.
         L0_ufos=.false.
         L0_efos=.false.
         L0_udos=.false.
         L0_idos=.false.
         L0_ados=.false.
         L0_obos=.false.
         L0_ebos=.false.
         L0_uxis=.false.
         L0_ixis=.false.
         L0_axis=.false.
         L0_ovis=.false.
         L0_avis=.false.
      endif

      if(L0_upos)I0_aros=I0_aros+1
      if(L0_ofos)I0_aros=I0_aros+1
      if(L0_ivis)I0_aros=I0_aros+1
      if(L0_utis)I0_aros=I0_aros+1
      if(L0_otis)I0_aros=I0_aros+1
      if(L0_etis)I0_aros=I0_aros+1
      if(L0_usis)I0_aros=I0_aros+1
      if(L0_isis)I0_aros=I0_aros+1
      if(L0_asis)I0_aros=I0_aros+1
      if(L0_ipos)I0_aros=I0_aros+1
      if(L0_apos)I0_aros=I0_aros+1
      if(L0_omos)I0_aros=I0_aros+1
      if(L0_emos)I0_aros=I0_aros+1
      if(L0_ulos)I0_aros=I0_aros+1
      if(L0_ilos)I0_aros=I0_aros+1
      if(L0_alos)I0_aros=I0_aros+1
      if(L0_okos)I0_aros=I0_aros+1
      if(L0_ekos)I0_aros=I0_aros+1
      if(L0_ufos)I0_aros=I0_aros+1
      if(L0_efos)I0_aros=I0_aros+1
      if(L0_udos)I0_aros=I0_aros+1
      if(L0_idos)I0_aros=I0_aros+1
      if(L0_ados)I0_aros=I0_aros+1
      if(L0_obos)I0_aros=I0_aros+1
      if(L0_ebos)I0_aros=I0_aros+1
      if(L0_uxis)I0_aros=I0_aros+1
      if(L0_ixis)I0_aros=I0_aros+1
      if(L0_axis)I0_aros=I0_aros+1
      if(L0_ovis)I0_aros=I0_aros+1
      if(L0_avis)I0_aros=I0_aros+1


      if(.not.L0_eros.and.L_(754).and..not.L0_upos)then
        I0_oros=1
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L_(753).and..not.L0_ofos)then
        I0_oros=2
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L_(752).and..not.L0_ivis)then
        I0_oros=3
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L_(751).and..not.L0_utis)then
        I0_oros=4
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L_(750).and..not.L0_otis)then
        I0_oros=5
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_itis.and..not.L0_etis)then
        I0_oros=6
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_atis.and..not.L0_usis)then
        I0_oros=7
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_osis.and..not.L0_isis)then
        I0_oros=8
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_esis.and..not.L0_asis)then
        I0_oros=9
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_opos.and..not.L0_ipos)then
        I0_oros=10
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_epos.and..not.L0_apos)then
        I0_oros=11
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_umos.and..not.L0_omos)then
        I0_oros=12
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_imos.and..not.L0_emos)then
        I0_oros=13
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_amos.and..not.L0_ulos)then
        I0_oros=14
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_olos.and..not.L0_ilos)then
        I0_oros=15
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_elos.and..not.L0_alos)then
        I0_oros=16
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_ukos.and..not.L0_okos)then
        I0_oros=17
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_ikos.and..not.L0_ekos)then
        I0_oros=18
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_akos.and..not.L0_ufos)then
        I0_oros=19
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_ifos.and..not.L0_efos)then
        I0_oros=20
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_afos.and..not.L0_udos)then
        I0_oros=21
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_odos.and..not.L0_idos)then
        I0_oros=22
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_edos.and..not.L0_ados)then
        I0_oros=23
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_ubos.and..not.L0_obos)then
        I0_oros=24
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_ibos.and..not.L0_ebos)then
        I0_oros=25
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_abos.and..not.L0_uxis)then
        I0_oros=26
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_oxis.and..not.L0_ixis)then
        I0_oros=27
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_exis.and..not.L0_axis)then
        I0_oros=28
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_uvis.and..not.L0_ovis)then
        I0_oros=29
        L0_eros=.true.
      endif

      if(.not.L0_eros.and.L0_evis.and..not.L0_avis)then
        I0_oros=30
        L0_eros=.true.
      endif

      if(I0_aros.eq.1)then
         L0_upos=.false.
         L0_ofos=.false.
         L0_ivis=.false.
         L0_utis=.false.
         L0_otis=.false.
         L0_etis=.false.
         L0_usis=.false.
         L0_isis=.false.
         L0_asis=.false.
         L0_ipos=.false.
         L0_apos=.false.
         L0_omos=.false.
         L0_emos=.false.
         L0_ulos=.false.
         L0_ilos=.false.
         L0_alos=.false.
         L0_okos=.false.
         L0_ekos=.false.
         L0_ufos=.false.
         L0_efos=.false.
         L0_udos=.false.
         L0_idos=.false.
         L0_ados=.false.
         L0_obos=.false.
         L0_ebos=.false.
         L0_uxis=.false.
         L0_ixis=.false.
         L0_axis=.false.
         L0_ovis=.false.
         L0_avis=.false.
      else
         L0_upos=L_(754)
         L0_ofos=L_(753)
         L0_ivis=L_(752)
         L0_utis=L_(751)
         L0_otis=L_(750)
         L0_etis=L0_itis
         L0_usis=L0_atis
         L0_isis=L0_osis
         L0_asis=L0_esis
         L0_ipos=L0_opos
         L0_apos=L0_epos
         L0_omos=L0_umos
         L0_emos=L0_imos
         L0_ulos=L0_amos
         L0_ilos=L0_olos
         L0_alos=L0_elos
         L0_okos=L0_ukos
         L0_ekos=L0_ikos
         L0_ufos=L0_akos
         L0_efos=L0_ifos
         L0_udos=L0_afos
         L0_idos=L0_odos
         L0_ados=L0_edos
         L0_obos=L0_ubos
         L0_ebos=L0_ibos
         L0_uxis=L0_abos
         L0_ixis=L0_oxis
         L0_axis=L0_exis
         L0_ovis=L0_uvis
         L0_avis=L0_evis
      endif
C 1762-10-0001_log.fgi( 309,  66):��������� �������
      select case (I0_oros)
      case (1)
        C255_iros="�����.���������"
      case (2)
        C255_iros="�����, �������� 1"
      case (3)
        C255_iros="�����, �������� 2"
      case (4)
        C255_iros="����"
      case (5)
        C255_iros="������, �������� 1"
      case (6)
        C255_iros="???"
      case (7)
        C255_iros="???"
      case (8)
        C255_iros="???"
      case (9)
        C255_iros="???"
      case (10)
        C255_iros="???"
      case (11)
        C255_iros="???"
      case (12)
        C255_iros="???"
      case (13)
        C255_iros="???"
      case (14)
        C255_iros="???"
      case (15)
        C255_iros="???"
      case (16)
        C255_iros="???"
      case (17)
        C255_iros="???"
      case (18)
        C255_iros="???"
      case (19)
        C255_iros="???"
      case (20)
        C255_iros="???"
      case (21)
        C255_iros="???"
      case (22)
        C255_iros="???"
      case (23)
        C255_iros="???"
      case (24)
        C255_iros="???"
      case (25)
        C255_iros="???"
      case (26)
        C255_iros="???"
      case (27)
        C255_iros="???"
      case (28)
        C255_iros="???"
      case (29)
        C255_iros="???"
      case (30)
        C255_iros="???"
      end select
C 1762-10-0001_log.fgi( 325,  95):MarkVI text out 30 variants,RolZ_state
      L0_uros=R0_esos.ne.R0_asos
      R0_asos=R0_esos
C 1762-10-0001_log.fgi(  14,  16):���������� ������������� ������
      L0_isos=R0_usos.ne.R0_osos
      R0_osos=R0_usos
C 1762-10-0001_log.fgi(  14,  28):���������� ������������� ������
      L0_etos=R0_otos.ne.R0_itos
      R0_itos=R0_otos
C 1762-10-0001_log.fgi(  14,  49):���������� ������������� ������
      L0_utos=R0_evos.ne.R0_avos
      R0_avos=R0_evos
C 1762-10-0001_log.fgi(  14,  61):���������� ������������� ������
      L0_atos=L0_utos.or.(L0_atos.and..not.(L0_etos))
      L_(755)=.not.L0_atos
C 1762-10-0001_log.fgi(  33,  59):RS �������
      L_(758)=.false.
C 1762-10-0001_log.fgi(  45, 106):��������� ���������� (�������)
      L_(757)=.false.
C 1762-10-0001_log.fgi(  45, 110):��������� ���������� (�������)
      L0_ikus=R0_akus.ne.R0_ufus
      R0_ufus=R0_akus
C 1762-10-0001_log.fgi(  14, 108):���������� ������������� ������
      if(L_(757).or.L_(758)) then
         L_okus=(L_(757).or.L_okus).and..not.(L_(758))
      else
         if(L0_ikus.and..not.L0_ekus) L_okus=.not.L_okus
      endif
      L0_ekus=L0_ikus
      L_(759)=.not.L_okus
C 1762-10-0001_log.fgi(  51, 108):T �������
      L_ivos=L_okus
C 1762-10-0001_log.fgi( 104, 114):������,1762-10-0001_lamp14
      if(L_okus) then
         I_ofus=1
      else
         I_ofus=0
      endif
C 1762-10-0001_log.fgi(  88, 110):��������� LO->1
      L_(761)=.false.
C 1762-10-0001_log.fgi(  45, 118):��������� ���������� (�������)
      L_(760)=.false.
C 1762-10-0001_log.fgi(  45, 122):��������� ���������� (�������)
      L0_olus=R0_elus.ne.R0_alus
      R0_alus=R0_elus
C 1762-10-0001_log.fgi(  14, 120):���������� ������������� ������
      if(L_(760).or.L_(761)) then
         L_ulus=(L_(760).or.L_ulus).and..not.(L_(761))
      else
         if(L0_olus.and..not.L0_ilus) L_ulus=.not.L_ulus
      endif
      L0_ilus=L0_olus
      L_(762)=.not.L_ulus
C 1762-10-0001_log.fgi(  51, 120):T �������
      L_ovos=L_ulus
C 1762-10-0001_log.fgi( 104, 126):������,1762-10-0001_lamp13
      if(L_ulus) then
         I_ukus=1
      else
         I_ukus=0
      endif
C 1762-10-0001_log.fgi(  88, 122):��������� LO->1
      L_(764)=.false.
C 1762-10-0001_log.fgi(  45, 130):��������� ���������� (�������)
      L_(763)=.false.
C 1762-10-0001_log.fgi(  45, 134):��������� ���������� (�������)
      L0_umus=R0_imus.ne.R0_emus
      R0_emus=R0_imus
C 1762-10-0001_log.fgi(  14, 132):���������� ������������� ������
      if(L_(763).or.L_(764)) then
         L_apus=(L_(763).or.L_apus).and..not.(L_(764))
      else
         if(L0_umus.and..not.L0_omus) L_apus=.not.L_apus
      endif
      L0_omus=L0_umus
      L_(765)=.not.L_apus
C 1762-10-0001_log.fgi(  51, 132):T �������
      L_uvos=L_apus
C 1762-10-0001_log.fgi( 104, 138):������,1762-10-0001_lamp12
      if(L_apus) then
         I_amus=1
      else
         I_amus=0
      endif
C 1762-10-0001_log.fgi(  88, 134):��������� LO->1
      L_(767)=.false.
C 1762-10-0001_log.fgi(  45, 142):��������� ���������� (�������)
      L_(766)=.false.
C 1762-10-0001_log.fgi(  45, 146):��������� ���������� (�������)
      L0_arus=R0_opus.ne.R0_ipus
      R0_ipus=R0_opus
C 1762-10-0001_log.fgi(  14, 144):���������� ������������� ������
      if(L_(766).or.L_(767)) then
         L_erus=(L_(766).or.L_erus).and..not.(L_(767))
      else
         if(L0_arus.and..not.L0_upus) L_erus=.not.L_erus
      endif
      L0_upus=L0_arus
      L_(768)=.not.L_erus
C 1762-10-0001_log.fgi(  51, 144):T �������
      L_axos=L_erus
C 1762-10-0001_log.fgi( 104, 150):������,1762-10-0001_lamp11
      if(L_erus) then
         I_epus=1
      else
         I_epus=0
      endif
C 1762-10-0001_log.fgi(  88, 146):��������� LO->1
      L_(770)=.false.
C 1762-10-0001_log.fgi(  45, 154):��������� ���������� (�������)
      L_(769)=.false.
C 1762-10-0001_log.fgi(  45, 158):��������� ���������� (�������)
      L0_esus=R0_urus.ne.R0_orus
      R0_orus=R0_urus
C 1762-10-0001_log.fgi(  14, 156):���������� ������������� ������
      if(L_(769).or.L_(770)) then
         L_isus=(L_(769).or.L_isus).and..not.(L_(770))
      else
         if(L0_esus.and..not.L0_asus) L_isus=.not.L_isus
      endif
      L0_asus=L0_esus
      L_(771)=.not.L_isus
C 1762-10-0001_log.fgi(  51, 156):T �������
      L_exos=L_isus
C 1762-10-0001_log.fgi( 104, 162):������,1762-10-0001_lamp10
      if(L_isus) then
         I_irus=1
      else
         I_irus=0
      endif
C 1762-10-0001_log.fgi(  88, 158):��������� LO->1
      L_(773)=.false.
C 1762-10-0001_log.fgi(  45, 166):��������� ���������� (�������)
      L_(772)=.false.
C 1762-10-0001_log.fgi(  45, 170):��������� ���������� (�������)
      L0_itus=R0_atus.ne.R0_usus
      R0_usus=R0_atus
C 1762-10-0001_log.fgi(  14, 168):���������� ������������� ������
      if(L_(772).or.L_(773)) then
         L_otus=(L_(772).or.L_otus).and..not.(L_(773))
      else
         if(L0_itus.and..not.L0_etus) L_otus=.not.L_otus
      endif
      L0_etus=L0_itus
      L_(774)=.not.L_otus
C 1762-10-0001_log.fgi(  51, 168):T �������
      L_ixos=L_otus
C 1762-10-0001_log.fgi( 104, 174):������,1762-10-0001_lamp9
      if(L_otus) then
         I_osus=1
      else
         I_osus=0
      endif
C 1762-10-0001_log.fgi(  88, 170):��������� LO->1
      L_(776)=.false.
C 1762-10-0001_log.fgi(  45, 178):��������� ���������� (�������)
      L_(775)=.false.
C 1762-10-0001_log.fgi(  45, 182):��������� ���������� (�������)
      L0_ovus=R0_evus.ne.R0_avus
      R0_avus=R0_evus
C 1762-10-0001_log.fgi(  14, 180):���������� ������������� ������
      if(L_(775).or.L_(776)) then
         L_uvus=(L_(775).or.L_uvus).and..not.(L_(776))
      else
         if(L0_ovus.and..not.L0_ivus) L_uvus=.not.L_uvus
      endif
      L0_ivus=L0_ovus
      L_(777)=.not.L_uvus
C 1762-10-0001_log.fgi(  51, 180):T �������
      L_oxos=L_uvus
C 1762-10-0001_log.fgi( 104, 186):������,1762-10-0001_lamp8
      if(L_uvus) then
         I_utus=1
      else
         I_utus=0
      endif
C 1762-10-0001_log.fgi(  88, 182):��������� LO->1
      L_(779)=.false.
C 1762-10-0001_log.fgi(  45, 190):��������� ���������� (�������)
      L_(778)=.false.
C 1762-10-0001_log.fgi(  45, 194):��������� ���������� (�������)
      L0_uxus=R0_ixus.ne.R0_exus
      R0_exus=R0_ixus
C 1762-10-0001_log.fgi(  14, 192):���������� ������������� ������
      if(L_(778).or.L_(779)) then
         L_abat=(L_(778).or.L_abat).and..not.(L_(779))
      else
         if(L0_uxus.and..not.L0_oxus) L_abat=.not.L_abat
      endif
      L0_oxus=L0_uxus
      L_(780)=.not.L_abat
C 1762-10-0001_log.fgi(  51, 192):T �������
      L_uxos=L_abat
C 1762-10-0001_log.fgi( 104, 198):������,1762-10-0001_lamp7
      if(L_abat) then
         I_axus=1
      else
         I_axus=0
      endif
C 1762-10-0001_log.fgi(  88, 194):��������� LO->1
      L_(782)=.false.
C 1762-10-0001_log.fgi(  45, 202):��������� ���������� (�������)
      L_(781)=.false.
C 1762-10-0001_log.fgi(  45, 206):��������� ���������� (�������)
      L0_adat=R0_obat.ne.R0_ibat
      R0_ibat=R0_obat
C 1762-10-0001_log.fgi(  14, 204):���������� ������������� ������
      if(L_(781).or.L_(782)) then
         L_edat=(L_(781).or.L_edat).and..not.(L_(782))
      else
         if(L0_adat.and..not.L0_ubat) L_edat=.not.L_edat
      endif
      L0_ubat=L0_adat
      L_(783)=.not.L_edat
C 1762-10-0001_log.fgi(  51, 204):T �������
      L_abus=L_edat
C 1762-10-0001_log.fgi( 104, 210):������,1762-10-0001_lamp6
      if(L_edat) then
         I_ebat=1
      else
         I_ebat=0
      endif
C 1762-10-0001_log.fgi(  88, 206):��������� LO->1
      L_(785)=.false.
C 1762-10-0001_log.fgi(  45, 214):��������� ���������� (�������)
      L_(784)=.false.
C 1762-10-0001_log.fgi(  45, 218):��������� ���������� (�������)
      L0_efat=R0_udat.ne.R0_odat
      R0_odat=R0_udat
C 1762-10-0001_log.fgi(  14, 216):���������� ������������� ������
      if(L_(784).or.L_(785)) then
         L_ifat=(L_(784).or.L_ifat).and..not.(L_(785))
      else
         if(L0_efat.and..not.L0_afat) L_ifat=.not.L_ifat
      endif
      L0_afat=L0_efat
      L_(786)=.not.L_ifat
C 1762-10-0001_log.fgi(  51, 216):T �������
      L_ebus=L_ifat
C 1762-10-0001_log.fgi( 104, 222):������,1762-10-0001_lamp5
      if(L_ifat) then
         I_idat=1
      else
         I_idat=0
      endif
C 1762-10-0001_log.fgi(  88, 218):��������� LO->1
      L_(788)=.false.
C 1762-10-0001_log.fgi(  45, 226):��������� ���������� (�������)
      L_(787)=.false.
C 1762-10-0001_log.fgi(  45, 230):��������� ���������� (�������)
      L0_ikat=R0_akat.ne.R0_ufat
      R0_ufat=R0_akat
C 1762-10-0001_log.fgi(  14, 228):���������� ������������� ������
      if(L_(787).or.L_(788)) then
         L_okat=(L_(787).or.L_okat).and..not.(L_(788))
      else
         if(L0_ikat.and..not.L0_ekat) L_okat=.not.L_okat
      endif
      L0_ekat=L0_ikat
      L_(789)=.not.L_okat
C 1762-10-0001_log.fgi(  51, 228):T �������
      L_ibus=L_okat
C 1762-10-0001_log.fgi( 104, 234):������,1762-10-0001_lamp4
      if(L_okat) then
         I_ofat=1
      else
         I_ofat=0
      endif
C 1762-10-0001_log.fgi(  88, 230):��������� LO->1
      L_(791)=.false.
C 1762-10-0001_log.fgi(  45, 238):��������� ���������� (�������)
      L_(790)=.false.
C 1762-10-0001_log.fgi(  45, 242):��������� ���������� (�������)
      L0_olat=R0_elat.ne.R0_alat
      R0_alat=R0_elat
C 1762-10-0001_log.fgi(  14, 240):���������� ������������� ������
      if(L_(790).or.L_(791)) then
         L_ulat=(L_(790).or.L_ulat).and..not.(L_(791))
      else
         if(L0_olat.and..not.L0_ilat) L_ulat=.not.L_ulat
      endif
      L0_ilat=L0_olat
      L_(792)=.not.L_ulat
C 1762-10-0001_log.fgi(  51, 240):T �������
      L_obus=L_ulat
C 1762-10-0001_log.fgi( 104, 246):������,1762-10-0001_lamp3
      if(L_ulat) then
         I_ukat=1
      else
         I_ukat=0
      endif
C 1762-10-0001_log.fgi(  88, 242):��������� LO->1
      L_(794)=.false.
C 1762-10-0001_log.fgi(  45, 250):��������� ���������� (�������)
      L_(793)=.false.
C 1762-10-0001_log.fgi(  45, 254):��������� ���������� (�������)
      L0_umat=R0_imat.ne.R0_emat
      R0_emat=R0_imat
C 1762-10-0001_log.fgi(  14, 252):���������� ������������� ������
      if(L_(793).or.L_(794)) then
         L_apat=(L_(793).or.L_apat).and..not.(L_(794))
      else
         if(L0_umat.and..not.L0_omat) L_apat=.not.L_apat
      endif
      L0_omat=L0_umat
      L_(795)=.not.L_apat
C 1762-10-0001_log.fgi(  51, 252):T �������
      L_ubus=L_apat
C 1762-10-0001_log.fgi( 104, 258):������,1762-10-0001_lamp2
      if(L_apat) then
         I_amat=1
      else
         I_amat=0
      endif
C 1762-10-0001_log.fgi(  88, 254):��������� LO->1
      L_(797)=.false.
C 1762-10-0001_log.fgi(  45, 262):��������� ���������� (�������)
      L_(796)=.false.
C 1762-10-0001_log.fgi(  45, 266):��������� ���������� (�������)
      L0_arat=R0_opat.ne.R0_ipat
      R0_ipat=R0_opat
C 1762-10-0001_log.fgi(  14, 264):���������� ������������� ������
      if(L_(796).or.L_(797)) then
         L_erat=(L_(796).or.L_erat).and..not.(L_(797))
      else
         if(L0_arat.and..not.L0_upat) L_erat=.not.L_erat
      endif
      L0_upat=L0_arat
      L_(798)=.not.L_erat
C 1762-10-0001_log.fgi(  51, 264):T �������
      L_adus=L_erat
C 1762-10-0001_log.fgi( 104, 270):������,1762-10-0001_lamp1
      L_(756) = L_okus.OR.L_ulus.OR.L_apus.OR.L_erus.OR.L_isus.OR.L_otus
     &.OR.L_uvus.OR.L_abat.OR.L_edat.OR.L_ifat.OR.L_okat.OR.L_ulat.OR.L_
     &apat.OR.L_erat
C 1762-10-0001_log.fgi(  79,  85):���
      select case (L_(756))
      case (.FALSE.)
        C255_udus="����������(�) ���������(�)"
      case (.TRUE.)
        C255_udus="����������(�) ��������(�)"
      end select

      select case (L_(756))
      case (.FALSE.)
        I_odus=I_idus
      case (.TRUE.)
        I_odus=I_edus
      end select

      select case (L_(756))
      case (.FALSE.)
        I_ifus=I_efus
      case (.TRUE.)
        I_ifus=I_afus
      end select
C 1762-10-0001_log.fgi( 104,  86):MarkVI text out,1762-10-0001_blkonoff
      if(L_erat) then
         I_epat=1
      else
         I_epat=0
      endif
C 1762-10-0001_log.fgi(  88, 266):��������� LO->1
      L_(800)=.false.
C 1762-07-0001_log.fgi( 110, 204):��������� ���������� (�������)
      if(L_(800)) then
         I0_irat=0
      endif
      I_(23)=I0_irat
      L_(799)=I0_irat.ne.0
C 1762-07-0001_log.fgi( 118, 206):���������� ��������� 
      L_(805) = (.NOT.L_(799))
C 1762-07-0001_log.fgi( 130, 211):���
      L_(804)=I_(23).eq.1
      L_(803)=I_(23).eq.2
      L_(802)=I_(23).eq.3
      L_(801)=I_(23).eq.4
C 1762-07-0001_log.fgi( 130, 206):���������� �� 4
      L0_upet=.false.
      I0_opet=0

      if(L0_upav)then
        I0_eret=1
         L0_ipet=.false.
         L0_efet=.false.
         L0_avat=.false.
         L0_itat=.false.
         L0_etat=.false.
         L0_usat=.false.
         L0_isat=.false.
         L0_asat=.false.
         L0_orat=.false.
         L0_apet=.false.
         L0_omet=.false.
         L0_emet=.false.
         L0_ulet=.false.
         L0_ilet=.false.
         L0_alet=.false.
         L0_oket=.false.
         L0_eket=.false.
         L0_ufet=.false.
         L0_ifet=.false.
         L0_udet=.false.
         L0_idet=.false.
         L0_adet=.false.
         L0_obet=.false.
         L0_ebet=.false.
         L0_uxat=.false.
         L0_ixat=.false.
         L0_axat=.false.
         L0_ovat=.false.
         L0_evat=.false.
         L0_otat=.false.
      endif

      if(L0_ipet)I0_opet=I0_opet+1
      if(L0_efet)I0_opet=I0_opet+1
      if(L0_avat)I0_opet=I0_opet+1
      if(L0_itat)I0_opet=I0_opet+1
      if(L0_etat)I0_opet=I0_opet+1
      if(L0_usat)I0_opet=I0_opet+1
      if(L0_isat)I0_opet=I0_opet+1
      if(L0_asat)I0_opet=I0_opet+1
      if(L0_orat)I0_opet=I0_opet+1
      if(L0_apet)I0_opet=I0_opet+1
      if(L0_omet)I0_opet=I0_opet+1
      if(L0_emet)I0_opet=I0_opet+1
      if(L0_ulet)I0_opet=I0_opet+1
      if(L0_ilet)I0_opet=I0_opet+1
      if(L0_alet)I0_opet=I0_opet+1
      if(L0_oket)I0_opet=I0_opet+1
      if(L0_eket)I0_opet=I0_opet+1
      if(L0_ufet)I0_opet=I0_opet+1
      if(L0_ifet)I0_opet=I0_opet+1
      if(L0_udet)I0_opet=I0_opet+1
      if(L0_idet)I0_opet=I0_opet+1
      if(L0_adet)I0_opet=I0_opet+1
      if(L0_obet)I0_opet=I0_opet+1
      if(L0_ebet)I0_opet=I0_opet+1
      if(L0_uxat)I0_opet=I0_opet+1
      if(L0_ixat)I0_opet=I0_opet+1
      if(L0_axat)I0_opet=I0_opet+1
      if(L0_ovat)I0_opet=I0_opet+1
      if(L0_evat)I0_opet=I0_opet+1
      if(L0_otat)I0_opet=I0_opet+1


      if(.not.L0_upet.and.L_(805).and..not.L0_ipet)then
        I0_eret=1
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L_(804).and..not.L0_efet)then
        I0_eret=2
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L_(803).and..not.L0_avat)then
        I0_eret=3
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L_(802).and..not.L0_itat)then
        I0_eret=4
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L_(801).and..not.L0_etat)then
        I0_eret=5
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_atat.and..not.L0_usat)then
        I0_eret=6
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_osat.and..not.L0_isat)then
        I0_eret=7
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_esat.and..not.L0_asat)then
        I0_eret=8
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_urat.and..not.L0_orat)then
        I0_eret=9
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_epet.and..not.L0_apet)then
        I0_eret=10
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_umet.and..not.L0_omet)then
        I0_eret=11
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_imet.and..not.L0_emet)then
        I0_eret=12
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_amet.and..not.L0_ulet)then
        I0_eret=13
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_olet.and..not.L0_ilet)then
        I0_eret=14
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_elet.and..not.L0_alet)then
        I0_eret=15
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_uket.and..not.L0_oket)then
        I0_eret=16
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_iket.and..not.L0_eket)then
        I0_eret=17
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_aket.and..not.L0_ufet)then
        I0_eret=18
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_ofet.and..not.L0_ifet)then
        I0_eret=19
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_afet.and..not.L0_udet)then
        I0_eret=20
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_odet.and..not.L0_idet)then
        I0_eret=21
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_edet.and..not.L0_adet)then
        I0_eret=22
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_ubet.and..not.L0_obet)then
        I0_eret=23
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_ibet.and..not.L0_ebet)then
        I0_eret=24
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_abet.and..not.L0_uxat)then
        I0_eret=25
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_oxat.and..not.L0_ixat)then
        I0_eret=26
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_exat.and..not.L0_axat)then
        I0_eret=27
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_uvat.and..not.L0_ovat)then
        I0_eret=28
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_ivat.and..not.L0_evat)then
        I0_eret=29
        L0_upet=.true.
      endif

      if(.not.L0_upet.and.L0_utat.and..not.L0_otat)then
        I0_eret=30
        L0_upet=.true.
      endif

      if(I0_opet.eq.1)then
         L0_ipet=.false.
         L0_efet=.false.
         L0_avat=.false.
         L0_itat=.false.
         L0_etat=.false.
         L0_usat=.false.
         L0_isat=.false.
         L0_asat=.false.
         L0_orat=.false.
         L0_apet=.false.
         L0_omet=.false.
         L0_emet=.false.
         L0_ulet=.false.
         L0_ilet=.false.
         L0_alet=.false.
         L0_oket=.false.
         L0_eket=.false.
         L0_ufet=.false.
         L0_ifet=.false.
         L0_udet=.false.
         L0_idet=.false.
         L0_adet=.false.
         L0_obet=.false.
         L0_ebet=.false.
         L0_uxat=.false.
         L0_ixat=.false.
         L0_axat=.false.
         L0_ovat=.false.
         L0_evat=.false.
         L0_otat=.false.
      else
         L0_ipet=L_(805)
         L0_efet=L_(804)
         L0_avat=L_(803)
         L0_itat=L_(802)
         L0_etat=L_(801)
         L0_usat=L0_atat
         L0_isat=L0_osat
         L0_asat=L0_esat
         L0_orat=L0_urat
         L0_apet=L0_epet
         L0_omet=L0_umet
         L0_emet=L0_imet
         L0_ulet=L0_amet
         L0_ilet=L0_olet
         L0_alet=L0_elet
         L0_oket=L0_uket
         L0_eket=L0_iket
         L0_ufet=L0_aket
         L0_ifet=L0_ofet
         L0_udet=L0_afet
         L0_idet=L0_odet
         L0_adet=L0_edet
         L0_obet=L0_ubet
         L0_ebet=L0_ibet
         L0_uxat=L0_abet
         L0_ixat=L0_oxat
         L0_axat=L0_exat
         L0_ovat=L0_uvat
         L0_evat=L0_ivat
         L0_otat=L0_utat
      endif
C 1762-07-0001_log.fgi( 140, 182):��������� �������
      select case (I0_eret)
      case (1)
        C255_aret="�����.���������"
      case (2)
        C255_aret="���������"
      case (3)
        C255_aret="����"
      case (4)
        C255_aret="���������� ���������� �����"
      case (5)
        C255_aret="������� ������� �����"
      case (6)
        C255_aret="???"
      case (7)
        C255_aret="???"
      case (8)
        C255_aret="???"
      case (9)
        C255_aret="???"
      case (10)
        C255_aret="???"
      case (11)
        C255_aret="???"
      case (12)
        C255_aret="???"
      case (13)
        C255_aret="???"
      case (14)
        C255_aret="???"
      case (15)
        C255_aret="???"
      case (16)
        C255_aret="???"
      case (17)
        C255_aret="???"
      case (18)
        C255_aret="???"
      case (19)
        C255_aret="???"
      case (20)
        C255_aret="???"
      case (21)
        C255_aret="???"
      case (22)
        C255_aret="???"
      case (23)
        C255_aret="???"
      case (24)
        C255_aret="???"
      case (25)
        C255_aret="???"
      case (26)
        C255_aret="???"
      case (27)
        C255_aret="???"
      case (28)
        C255_aret="???"
      case (29)
        C255_aret="???"
      case (30)
        C255_aret="???"
      end select
C 1762-07-0001_log.fgi( 156, 211):MarkVI text out 30 variants,1726-07-0001_vesstate
      L_(807)=.false.
C 1762-07-0001_log.fgi( 304, 275):��������� ���������� (�������)
      if(L_(807)) then
         I0_iret=0
      endif
      I_(24)=I0_iret
      L_(806)=I0_iret.ne.0
C 1762-07-0001_log.fgi( 312, 277):���������� ��������� 
      L_(812) = (.NOT.L_(806))
C 1762-07-0001_log.fgi( 324, 282):���
      L_(811)=I_(24).eq.1
      L_(810)=I_(24).eq.2
      L_(809)=I_(24).eq.3
      L_(808)=I_(24).eq.4
C 1762-07-0001_log.fgi( 324, 277):���������� �� 4
      L0_upit=.false.
      I0_opit=0

      if(L0_upav)then
        I0_erit=1
         L0_ipit=.false.
         L0_efit=.false.
         L0_avet=.false.
         L0_itet=.false.
         L0_etet=.false.
         L0_uset=.false.
         L0_iset=.false.
         L0_aset=.false.
         L0_oret=.false.
         L0_apit=.false.
         L0_omit=.false.
         L0_emit=.false.
         L0_ulit=.false.
         L0_ilit=.false.
         L0_alit=.false.
         L0_okit=.false.
         L0_ekit=.false.
         L0_ufit=.false.
         L0_ifit=.false.
         L0_udit=.false.
         L0_idit=.false.
         L0_adit=.false.
         L0_obit=.false.
         L0_ebit=.false.
         L0_uxet=.false.
         L0_ixet=.false.
         L0_axet=.false.
         L0_ovet=.false.
         L0_evet=.false.
         L0_otet=.false.
      endif

      if(L0_ipit)I0_opit=I0_opit+1
      if(L0_efit)I0_opit=I0_opit+1
      if(L0_avet)I0_opit=I0_opit+1
      if(L0_itet)I0_opit=I0_opit+1
      if(L0_etet)I0_opit=I0_opit+1
      if(L0_uset)I0_opit=I0_opit+1
      if(L0_iset)I0_opit=I0_opit+1
      if(L0_aset)I0_opit=I0_opit+1
      if(L0_oret)I0_opit=I0_opit+1
      if(L0_apit)I0_opit=I0_opit+1
      if(L0_omit)I0_opit=I0_opit+1
      if(L0_emit)I0_opit=I0_opit+1
      if(L0_ulit)I0_opit=I0_opit+1
      if(L0_ilit)I0_opit=I0_opit+1
      if(L0_alit)I0_opit=I0_opit+1
      if(L0_okit)I0_opit=I0_opit+1
      if(L0_ekit)I0_opit=I0_opit+1
      if(L0_ufit)I0_opit=I0_opit+1
      if(L0_ifit)I0_opit=I0_opit+1
      if(L0_udit)I0_opit=I0_opit+1
      if(L0_idit)I0_opit=I0_opit+1
      if(L0_adit)I0_opit=I0_opit+1
      if(L0_obit)I0_opit=I0_opit+1
      if(L0_ebit)I0_opit=I0_opit+1
      if(L0_uxet)I0_opit=I0_opit+1
      if(L0_ixet)I0_opit=I0_opit+1
      if(L0_axet)I0_opit=I0_opit+1
      if(L0_ovet)I0_opit=I0_opit+1
      if(L0_evet)I0_opit=I0_opit+1
      if(L0_otet)I0_opit=I0_opit+1


      if(.not.L0_upit.and.L_(812).and..not.L0_ipit)then
        I0_erit=1
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L_(811).and..not.L0_efit)then
        I0_erit=2
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L_(810).and..not.L0_avet)then
        I0_erit=3
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L_(809).and..not.L0_itet)then
        I0_erit=4
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L_(808).and..not.L0_etet)then
        I0_erit=5
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_atet.and..not.L0_uset)then
        I0_erit=6
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_oset.and..not.L0_iset)then
        I0_erit=7
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_eset.and..not.L0_aset)then
        I0_erit=8
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_uret.and..not.L0_oret)then
        I0_erit=9
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_epit.and..not.L0_apit)then
        I0_erit=10
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_umit.and..not.L0_omit)then
        I0_erit=11
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_imit.and..not.L0_emit)then
        I0_erit=12
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_amit.and..not.L0_ulit)then
        I0_erit=13
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_olit.and..not.L0_ilit)then
        I0_erit=14
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_elit.and..not.L0_alit)then
        I0_erit=15
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_ukit.and..not.L0_okit)then
        I0_erit=16
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_ikit.and..not.L0_ekit)then
        I0_erit=17
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_akit.and..not.L0_ufit)then
        I0_erit=18
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_ofit.and..not.L0_ifit)then
        I0_erit=19
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_afit.and..not.L0_udit)then
        I0_erit=20
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_odit.and..not.L0_idit)then
        I0_erit=21
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_edit.and..not.L0_adit)then
        I0_erit=22
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_ubit.and..not.L0_obit)then
        I0_erit=23
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_ibit.and..not.L0_ebit)then
        I0_erit=24
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_abit.and..not.L0_uxet)then
        I0_erit=25
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_oxet.and..not.L0_ixet)then
        I0_erit=26
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_exet.and..not.L0_axet)then
        I0_erit=27
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_uvet.and..not.L0_ovet)then
        I0_erit=28
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_ivet.and..not.L0_evet)then
        I0_erit=29
        L0_upit=.true.
      endif

      if(.not.L0_upit.and.L0_utet.and..not.L0_otet)then
        I0_erit=30
        L0_upit=.true.
      endif

      if(I0_opit.eq.1)then
         L0_ipit=.false.
         L0_efit=.false.
         L0_avet=.false.
         L0_itet=.false.
         L0_etet=.false.
         L0_uset=.false.
         L0_iset=.false.
         L0_aset=.false.
         L0_oret=.false.
         L0_apit=.false.
         L0_omit=.false.
         L0_emit=.false.
         L0_ulit=.false.
         L0_ilit=.false.
         L0_alit=.false.
         L0_okit=.false.
         L0_ekit=.false.
         L0_ufit=.false.
         L0_ifit=.false.
         L0_udit=.false.
         L0_idit=.false.
         L0_adit=.false.
         L0_obit=.false.
         L0_ebit=.false.
         L0_uxet=.false.
         L0_ixet=.false.
         L0_axet=.false.
         L0_ovet=.false.
         L0_evet=.false.
         L0_otet=.false.
      else
         L0_ipit=L_(812)
         L0_efit=L_(811)
         L0_avet=L_(810)
         L0_itet=L_(809)
         L0_etet=L_(808)
         L0_uset=L0_atet
         L0_iset=L0_oset
         L0_aset=L0_eset
         L0_oret=L0_uret
         L0_apit=L0_epit
         L0_omit=L0_umit
         L0_emit=L0_imit
         L0_ulit=L0_amit
         L0_ilit=L0_olit
         L0_alit=L0_elit
         L0_okit=L0_ukit
         L0_ekit=L0_ikit
         L0_ufit=L0_akit
         L0_ifit=L0_ofit
         L0_udit=L0_afit
         L0_idit=L0_odit
         L0_adit=L0_edit
         L0_obit=L0_ubit
         L0_ebit=L0_ibit
         L0_uxet=L0_abit
         L0_ixet=L0_oxet
         L0_axet=L0_exet
         L0_ovet=L0_uvet
         L0_evet=L0_ivet
         L0_otet=L0_utet
      endif
C 1762-07-0001_log.fgi( 334, 253):��������� �������
      select case (I0_erit)
      case (1)
        C255_arit="���� �� ������� �3"
      case (2)
        C255_arit="������ �� ������� �1"
      case (3)
        C255_arit="����"
      case (4)
        C255_arit="���� �� ������� �2"
      case (5)
        C255_arit="???"
      case (6)
        C255_arit="???"
      case (7)
        C255_arit="???"
      case (8)
        C255_arit="???"
      case (9)
        C255_arit="???"
      case (10)
        C255_arit="???"
      case (11)
        C255_arit="???"
      case (12)
        C255_arit="???"
      case (13)
        C255_arit="???"
      case (14)
        C255_arit="???"
      case (15)
        C255_arit="???"
      case (16)
        C255_arit="???"
      case (17)
        C255_arit="???"
      case (18)
        C255_arit="???"
      case (19)
        C255_arit="???"
      case (20)
        C255_arit="???"
      case (21)
        C255_arit="???"
      case (22)
        C255_arit="???"
      case (23)
        C255_arit="???"
      case (24)
        C255_arit="???"
      case (25)
        C255_arit="???"
      case (26)
        C255_arit="???"
      case (27)
        C255_arit="???"
      case (28)
        C255_arit="???"
      case (29)
        C255_arit="???"
      case (30)
        C255_arit="???"
      end select
C 1762-07-0001_log.fgi( 350, 282):MarkVI text out 30 variants,1726-07-0001_dosstate
      L_(814)=.false.
C 1762-07-0001_log.fgi( 240, 275):��������� ���������� (�������)
      if(L_(814)) then
         I0_irit=0
      endif
      I_(25)=I0_irit
      L_(813)=I0_irit.ne.0
C 1762-07-0001_log.fgi( 248, 277):���������� ��������� 
      L_(819) = (.NOT.L_(813))
C 1762-07-0001_log.fgi( 260, 282):���
      L_(818)=I_(25).eq.1
      L_(817)=I_(25).eq.2
      L_(816)=I_(25).eq.3
      L_(815)=I_(25).eq.4
C 1762-07-0001_log.fgi( 260, 277):���������� �� 4
      L0_upot=.false.
      I0_opot=0

      if(L0_upav)then
        I0_erot=1
         L0_ipot=.false.
         L0_efot=.false.
         L0_avit=.false.
         L0_itit=.false.
         L0_etit=.false.
         L0_usit=.false.
         L0_isit=.false.
         L0_asit=.false.
         L0_orit=.false.
         L0_apot=.false.
         L0_omot=.false.
         L0_emot=.false.
         L0_ulot=.false.
         L0_ilot=.false.
         L0_alot=.false.
         L0_okot=.false.
         L0_ekot=.false.
         L0_ufot=.false.
         L0_ifot=.false.
         L0_udot=.false.
         L0_idot=.false.
         L0_adot=.false.
         L0_obot=.false.
         L0_ebot=.false.
         L0_uxit=.false.
         L0_ixit=.false.
         L0_axit=.false.
         L0_ovit=.false.
         L0_evit=.false.
         L0_otit=.false.
      endif

      if(L0_ipot)I0_opot=I0_opot+1
      if(L0_efot)I0_opot=I0_opot+1
      if(L0_avit)I0_opot=I0_opot+1
      if(L0_itit)I0_opot=I0_opot+1
      if(L0_etit)I0_opot=I0_opot+1
      if(L0_usit)I0_opot=I0_opot+1
      if(L0_isit)I0_opot=I0_opot+1
      if(L0_asit)I0_opot=I0_opot+1
      if(L0_orit)I0_opot=I0_opot+1
      if(L0_apot)I0_opot=I0_opot+1
      if(L0_omot)I0_opot=I0_opot+1
      if(L0_emot)I0_opot=I0_opot+1
      if(L0_ulot)I0_opot=I0_opot+1
      if(L0_ilot)I0_opot=I0_opot+1
      if(L0_alot)I0_opot=I0_opot+1
      if(L0_okot)I0_opot=I0_opot+1
      if(L0_ekot)I0_opot=I0_opot+1
      if(L0_ufot)I0_opot=I0_opot+1
      if(L0_ifot)I0_opot=I0_opot+1
      if(L0_udot)I0_opot=I0_opot+1
      if(L0_idot)I0_opot=I0_opot+1
      if(L0_adot)I0_opot=I0_opot+1
      if(L0_obot)I0_opot=I0_opot+1
      if(L0_ebot)I0_opot=I0_opot+1
      if(L0_uxit)I0_opot=I0_opot+1
      if(L0_ixit)I0_opot=I0_opot+1
      if(L0_axit)I0_opot=I0_opot+1
      if(L0_ovit)I0_opot=I0_opot+1
      if(L0_evit)I0_opot=I0_opot+1
      if(L0_otit)I0_opot=I0_opot+1


      if(.not.L0_upot.and.L_(819).and..not.L0_ipot)then
        I0_erot=1
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L_(818).and..not.L0_efot)then
        I0_erot=2
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L_(817).and..not.L0_avit)then
        I0_erot=3
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L_(816).and..not.L0_itit)then
        I0_erot=4
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L_(815).and..not.L0_etit)then
        I0_erot=5
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_atit.and..not.L0_usit)then
        I0_erot=6
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_osit.and..not.L0_isit)then
        I0_erot=7
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_esit.and..not.L0_asit)then
        I0_erot=8
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_urit.and..not.L0_orit)then
        I0_erot=9
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_epot.and..not.L0_apot)then
        I0_erot=10
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_umot.and..not.L0_omot)then
        I0_erot=11
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_imot.and..not.L0_emot)then
        I0_erot=12
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_amot.and..not.L0_ulot)then
        I0_erot=13
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_olot.and..not.L0_ilot)then
        I0_erot=14
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_elot.and..not.L0_alot)then
        I0_erot=15
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_ukot.and..not.L0_okot)then
        I0_erot=16
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_ikot.and..not.L0_ekot)then
        I0_erot=17
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_akot.and..not.L0_ufot)then
        I0_erot=18
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_ofot.and..not.L0_ifot)then
        I0_erot=19
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_afot.and..not.L0_udot)then
        I0_erot=20
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_odot.and..not.L0_idot)then
        I0_erot=21
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_edot.and..not.L0_adot)then
        I0_erot=22
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_ubot.and..not.L0_obot)then
        I0_erot=23
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_ibot.and..not.L0_ebot)then
        I0_erot=24
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_abot.and..not.L0_uxit)then
        I0_erot=25
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_oxit.and..not.L0_ixit)then
        I0_erot=26
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_exit.and..not.L0_axit)then
        I0_erot=27
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_uvit.and..not.L0_ovit)then
        I0_erot=28
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_ivit.and..not.L0_evit)then
        I0_erot=29
        L0_upot=.true.
      endif

      if(.not.L0_upot.and.L0_utit.and..not.L0_otit)then
        I0_erot=30
        L0_upot=.true.
      endif

      if(I0_opot.eq.1)then
         L0_ipot=.false.
         L0_efot=.false.
         L0_avit=.false.
         L0_itit=.false.
         L0_etit=.false.
         L0_usit=.false.
         L0_isit=.false.
         L0_asit=.false.
         L0_orit=.false.
         L0_apot=.false.
         L0_omot=.false.
         L0_emot=.false.
         L0_ulot=.false.
         L0_ilot=.false.
         L0_alot=.false.
         L0_okot=.false.
         L0_ekot=.false.
         L0_ufot=.false.
         L0_ifot=.false.
         L0_udot=.false.
         L0_idot=.false.
         L0_adot=.false.
         L0_obot=.false.
         L0_ebot=.false.
         L0_uxit=.false.
         L0_ixit=.false.
         L0_axit=.false.
         L0_ovit=.false.
         L0_evit=.false.
         L0_otit=.false.
      else
         L0_ipot=L_(819)
         L0_efot=L_(818)
         L0_avit=L_(817)
         L0_itit=L_(816)
         L0_etit=L_(815)
         L0_usit=L0_atit
         L0_isit=L0_osit
         L0_asit=L0_esit
         L0_orit=L0_urit
         L0_apot=L0_epot
         L0_omot=L0_umot
         L0_emot=L0_imot
         L0_ulot=L0_amot
         L0_ilot=L0_olot
         L0_alot=L0_elot
         L0_okot=L0_ukot
         L0_ekot=L0_ikot
         L0_ufot=L0_akot
         L0_ifot=L0_ofot
         L0_udot=L0_afot
         L0_idot=L0_odot
         L0_adot=L0_edot
         L0_obot=L0_ubot
         L0_ebot=L0_ibot
         L0_uxit=L0_abot
         L0_ixit=L0_oxit
         L0_axit=L0_exit
         L0_ovit=L0_uvit
         L0_evit=L0_ivit
         L0_otit=L0_utit
      endif
C 1762-07-0001_log.fgi( 270, 253):��������� �������
      select case (I0_erot)
      case (1)
        C255_arot="�����.���������"
      case (2)
        C255_arot="������� � ����.�����."
      case (3)
        C255_arot="�������� � �����.�����."
      case (4)
        C255_arot="???"
      case (5)
        C255_arot="???"
      case (6)
        C255_arot="???"
      case (7)
        C255_arot="???"
      case (8)
        C255_arot="???"
      case (9)
        C255_arot="???"
      case (10)
        C255_arot="???"
      case (11)
        C255_arot="???"
      case (12)
        C255_arot="???"
      case (13)
        C255_arot="???"
      case (14)
        C255_arot="???"
      case (15)
        C255_arot="???"
      case (16)
        C255_arot="???"
      case (17)
        C255_arot="???"
      case (18)
        C255_arot="???"
      case (19)
        C255_arot="???"
      case (20)
        C255_arot="???"
      case (21)
        C255_arot="???"
      case (22)
        C255_arot="???"
      case (23)
        C255_arot="???"
      case (24)
        C255_arot="???"
      case (25)
        C255_arot="???"
      case (26)
        C255_arot="???"
      case (27)
        C255_arot="???"
      case (28)
        C255_arot="???"
      case (29)
        C255_arot="???"
      case (30)
        C255_arot="???"
      end select
C 1762-07-0001_log.fgi( 286, 282):MarkVI text out 30 variants,1726-07-0001_dkt_state
      L_(821)=.false.
C 1762-07-0001_log.fgi( 177, 275):��������� ���������� (�������)
      if(L_(821)) then
         I0_irot=0
      endif
      I_(26)=I0_irot
      L_(820)=I0_irot.ne.0
C 1762-07-0001_log.fgi( 185, 277):���������� ��������� 
      L_(826) = (.NOT.L_(820))
C 1762-07-0001_log.fgi( 197, 282):���
      L_(825)=I_(26).eq.1
      L_(824)=I_(26).eq.2
      L_(823)=I_(26).eq.3
      L_(822)=I_(26).eq.4
C 1762-07-0001_log.fgi( 197, 277):���������� �� 4
      L0_uput=.false.
      I0_oput=0

      if(L0_upav)then
        I0_erut=1
         L0_iput=.false.
         L0_efut=.false.
         L0_avot=.false.
         L0_itot=.false.
         L0_etot=.false.
         L0_usot=.false.
         L0_isot=.false.
         L0_asot=.false.
         L0_orot=.false.
         L0_aput=.false.
         L0_omut=.false.
         L0_emut=.false.
         L0_ulut=.false.
         L0_ilut=.false.
         L0_alut=.false.
         L0_okut=.false.
         L0_ekut=.false.
         L0_ufut=.false.
         L0_ifut=.false.
         L0_udut=.false.
         L0_idut=.false.
         L0_adut=.false.
         L0_obut=.false.
         L0_ebut=.false.
         L0_uxot=.false.
         L0_ixot=.false.
         L0_axot=.false.
         L0_ovot=.false.
         L0_evot=.false.
         L0_otot=.false.
      endif

      if(L0_iput)I0_oput=I0_oput+1
      if(L0_efut)I0_oput=I0_oput+1
      if(L0_avot)I0_oput=I0_oput+1
      if(L0_itot)I0_oput=I0_oput+1
      if(L0_etot)I0_oput=I0_oput+1
      if(L0_usot)I0_oput=I0_oput+1
      if(L0_isot)I0_oput=I0_oput+1
      if(L0_asot)I0_oput=I0_oput+1
      if(L0_orot)I0_oput=I0_oput+1
      if(L0_aput)I0_oput=I0_oput+1
      if(L0_omut)I0_oput=I0_oput+1
      if(L0_emut)I0_oput=I0_oput+1
      if(L0_ulut)I0_oput=I0_oput+1
      if(L0_ilut)I0_oput=I0_oput+1
      if(L0_alut)I0_oput=I0_oput+1
      if(L0_okut)I0_oput=I0_oput+1
      if(L0_ekut)I0_oput=I0_oput+1
      if(L0_ufut)I0_oput=I0_oput+1
      if(L0_ifut)I0_oput=I0_oput+1
      if(L0_udut)I0_oput=I0_oput+1
      if(L0_idut)I0_oput=I0_oput+1
      if(L0_adut)I0_oput=I0_oput+1
      if(L0_obut)I0_oput=I0_oput+1
      if(L0_ebut)I0_oput=I0_oput+1
      if(L0_uxot)I0_oput=I0_oput+1
      if(L0_ixot)I0_oput=I0_oput+1
      if(L0_axot)I0_oput=I0_oput+1
      if(L0_ovot)I0_oput=I0_oput+1
      if(L0_evot)I0_oput=I0_oput+1
      if(L0_otot)I0_oput=I0_oput+1


      if(.not.L0_uput.and.L_(826).and..not.L0_iput)then
        I0_erut=1
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L_(825).and..not.L0_efut)then
        I0_erut=2
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L_(824).and..not.L0_avot)then
        I0_erut=3
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L_(823).and..not.L0_itot)then
        I0_erut=4
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L_(822).and..not.L0_etot)then
        I0_erut=5
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_atot.and..not.L0_usot)then
        I0_erut=6
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_osot.and..not.L0_isot)then
        I0_erut=7
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_esot.and..not.L0_asot)then
        I0_erut=8
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_urot.and..not.L0_orot)then
        I0_erut=9
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_eput.and..not.L0_aput)then
        I0_erut=10
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_umut.and..not.L0_omut)then
        I0_erut=11
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_imut.and..not.L0_emut)then
        I0_erut=12
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_amut.and..not.L0_ulut)then
        I0_erut=13
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_olut.and..not.L0_ilut)then
        I0_erut=14
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_elut.and..not.L0_alut)then
        I0_erut=15
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_ukut.and..not.L0_okut)then
        I0_erut=16
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_ikut.and..not.L0_ekut)then
        I0_erut=17
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_akut.and..not.L0_ufut)then
        I0_erut=18
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_ofut.and..not.L0_ifut)then
        I0_erut=19
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_afut.and..not.L0_udut)then
        I0_erut=20
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_odut.and..not.L0_idut)then
        I0_erut=21
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_edut.and..not.L0_adut)then
        I0_erut=22
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_ubut.and..not.L0_obut)then
        I0_erut=23
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_ibut.and..not.L0_ebut)then
        I0_erut=24
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_abut.and..not.L0_uxot)then
        I0_erut=25
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_oxot.and..not.L0_ixot)then
        I0_erut=26
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_exot.and..not.L0_axot)then
        I0_erut=27
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_uvot.and..not.L0_ovot)then
        I0_erut=28
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_ivot.and..not.L0_evot)then
        I0_erut=29
        L0_uput=.true.
      endif

      if(.not.L0_uput.and.L0_utot.and..not.L0_otot)then
        I0_erut=30
        L0_uput=.true.
      endif

      if(I0_oput.eq.1)then
         L0_iput=.false.
         L0_efut=.false.
         L0_avot=.false.
         L0_itot=.false.
         L0_etot=.false.
         L0_usot=.false.
         L0_isot=.false.
         L0_asot=.false.
         L0_orot=.false.
         L0_aput=.false.
         L0_omut=.false.
         L0_emut=.false.
         L0_ulut=.false.
         L0_ilut=.false.
         L0_alut=.false.
         L0_okut=.false.
         L0_ekut=.false.
         L0_ufut=.false.
         L0_ifut=.false.
         L0_udut=.false.
         L0_idut=.false.
         L0_adut=.false.
         L0_obut=.false.
         L0_ebut=.false.
         L0_uxot=.false.
         L0_ixot=.false.
         L0_axot=.false.
         L0_ovot=.false.
         L0_evot=.false.
         L0_otot=.false.
      else
         L0_iput=L_(826)
         L0_efut=L_(825)
         L0_avot=L_(824)
         L0_itot=L_(823)
         L0_etot=L_(822)
         L0_usot=L0_atot
         L0_isot=L0_osot
         L0_asot=L0_esot
         L0_orot=L0_urot
         L0_aput=L0_eput
         L0_omut=L0_umut
         L0_emut=L0_imut
         L0_ulut=L0_amut
         L0_ilut=L0_olut
         L0_alut=L0_elut
         L0_okut=L0_ukut
         L0_ekut=L0_ikut
         L0_ufut=L0_akut
         L0_ifut=L0_ofut
         L0_udut=L0_afut
         L0_idut=L0_odut
         L0_adut=L0_edut
         L0_obut=L0_ubut
         L0_ebut=L0_ibut
         L0_uxot=L0_abut
         L0_ixot=L0_oxot
         L0_axot=L0_exot
         L0_ovot=L0_uvot
         L0_evot=L0_ivot
         L0_otot=L0_utot
      endif
C 1762-07-0001_log.fgi( 207, 253):��������� �������
      select case (I0_erut)
      case (1)
        C255_arut="�����.���������"
      case (2)
        C255_arut="�����, �������� 1"
      case (3)
        C255_arut="�����, �������� 2"
      case (4)
        C255_arut="����"
      case (5)
        C255_arut="������, �������� 1"
      case (6)
        C255_arut="???"
      case (7)
        C255_arut="???"
      case (8)
        C255_arut="???"
      case (9)
        C255_arut="???"
      case (10)
        C255_arut="???"
      case (11)
        C255_arut="???"
      case (12)
        C255_arut="???"
      case (13)
        C255_arut="???"
      case (14)
        C255_arut="???"
      case (15)
        C255_arut="???"
      case (16)
        C255_arut="???"
      case (17)
        C255_arut="???"
      case (18)
        C255_arut="???"
      case (19)
        C255_arut="???"
      case (20)
        C255_arut="???"
      case (21)
        C255_arut="???"
      case (22)
        C255_arut="???"
      case (23)
        C255_arut="???"
      case (24)
        C255_arut="???"
      case (25)
        C255_arut="???"
      case (26)
        C255_arut="???"
      case (27)
        C255_arut="???"
      case (28)
        C255_arut="???"
      case (29)
        C255_arut="???"
      case (30)
        C255_arut="???"
      end select
C 1762-07-0001_log.fgi( 223, 282):MarkVI text out 30 variants,1726-07-0001_RolV_state
      L_(828)=.false.
C 1762-07-0001_log.fgi( 110, 275):��������� ���������� (�������)
      if(L_(828)) then
         I0_irut=0
      endif
      I_(27)=I0_irut
      L_(827)=I0_irut.ne.0
C 1762-07-0001_log.fgi( 118, 277):���������� ��������� 
      L_(833) = (.NOT.L_(827))
C 1762-07-0001_log.fgi( 130, 282):���
      L_(832)=I_(27).eq.1
      L_(831)=I_(27).eq.2
      L_(830)=I_(27).eq.3
      L_(829)=I_(27).eq.4
C 1762-07-0001_log.fgi( 130, 277):���������� �� 4
      L0_arav=.false.
      I0_opav=0

      if(L0_upav)then
        I0_irav=1
         L0_ipav=.false.
         L0_efav=.false.
         L0_avut=.false.
         L0_itut=.false.
         L0_etut=.false.
         L0_usut=.false.
         L0_isut=.false.
         L0_asut=.false.
         L0_orut=.false.
         L0_apav=.false.
         L0_omav=.false.
         L0_emav=.false.
         L0_ulav=.false.
         L0_ilav=.false.
         L0_alav=.false.
         L0_okav=.false.
         L0_ekav=.false.
         L0_ufav=.false.
         L0_ifav=.false.
         L0_udav=.false.
         L0_idav=.false.
         L0_adav=.false.
         L0_obav=.false.
         L0_ebav=.false.
         L0_uxut=.false.
         L0_ixut=.false.
         L0_axut=.false.
         L0_ovut=.false.
         L0_evut=.false.
         L0_otut=.false.
      endif

      if(L0_ipav)I0_opav=I0_opav+1
      if(L0_efav)I0_opav=I0_opav+1
      if(L0_avut)I0_opav=I0_opav+1
      if(L0_itut)I0_opav=I0_opav+1
      if(L0_etut)I0_opav=I0_opav+1
      if(L0_usut)I0_opav=I0_opav+1
      if(L0_isut)I0_opav=I0_opav+1
      if(L0_asut)I0_opav=I0_opav+1
      if(L0_orut)I0_opav=I0_opav+1
      if(L0_apav)I0_opav=I0_opav+1
      if(L0_omav)I0_opav=I0_opav+1
      if(L0_emav)I0_opav=I0_opav+1
      if(L0_ulav)I0_opav=I0_opav+1
      if(L0_ilav)I0_opav=I0_opav+1
      if(L0_alav)I0_opav=I0_opav+1
      if(L0_okav)I0_opav=I0_opav+1
      if(L0_ekav)I0_opav=I0_opav+1
      if(L0_ufav)I0_opav=I0_opav+1
      if(L0_ifav)I0_opav=I0_opav+1
      if(L0_udav)I0_opav=I0_opav+1
      if(L0_idav)I0_opav=I0_opav+1
      if(L0_adav)I0_opav=I0_opav+1
      if(L0_obav)I0_opav=I0_opav+1
      if(L0_ebav)I0_opav=I0_opav+1
      if(L0_uxut)I0_opav=I0_opav+1
      if(L0_ixut)I0_opav=I0_opav+1
      if(L0_axut)I0_opav=I0_opav+1
      if(L0_ovut)I0_opav=I0_opav+1
      if(L0_evut)I0_opav=I0_opav+1
      if(L0_otut)I0_opav=I0_opav+1


      if(.not.L0_arav.and.L_(833).and..not.L0_ipav)then
        I0_irav=1
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L_(832).and..not.L0_efav)then
        I0_irav=2
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L_(831).and..not.L0_avut)then
        I0_irav=3
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L_(830).and..not.L0_itut)then
        I0_irav=4
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L_(829).and..not.L0_etut)then
        I0_irav=5
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_atut.and..not.L0_usut)then
        I0_irav=6
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_osut.and..not.L0_isut)then
        I0_irav=7
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_esut.and..not.L0_asut)then
        I0_irav=8
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_urut.and..not.L0_orut)then
        I0_irav=9
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_epav.and..not.L0_apav)then
        I0_irav=10
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_umav.and..not.L0_omav)then
        I0_irav=11
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_imav.and..not.L0_emav)then
        I0_irav=12
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_amav.and..not.L0_ulav)then
        I0_irav=13
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_olav.and..not.L0_ilav)then
        I0_irav=14
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_elav.and..not.L0_alav)then
        I0_irav=15
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_ukav.and..not.L0_okav)then
        I0_irav=16
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_ikav.and..not.L0_ekav)then
        I0_irav=17
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_akav.and..not.L0_ufav)then
        I0_irav=18
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_ofav.and..not.L0_ifav)then
        I0_irav=19
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_afav.and..not.L0_udav)then
        I0_irav=20
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_odav.and..not.L0_idav)then
        I0_irav=21
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_edav.and..not.L0_adav)then
        I0_irav=22
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_ubav.and..not.L0_obav)then
        I0_irav=23
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_ibav.and..not.L0_ebav)then
        I0_irav=24
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_abav.and..not.L0_uxut)then
        I0_irav=25
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_oxut.and..not.L0_ixut)then
        I0_irav=26
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_exut.and..not.L0_axut)then
        I0_irav=27
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_uvut.and..not.L0_ovut)then
        I0_irav=28
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_ivut.and..not.L0_evut)then
        I0_irav=29
        L0_arav=.true.
      endif

      if(.not.L0_arav.and.L0_utut.and..not.L0_otut)then
        I0_irav=30
        L0_arav=.true.
      endif

      if(I0_opav.eq.1)then
         L0_ipav=.false.
         L0_efav=.false.
         L0_avut=.false.
         L0_itut=.false.
         L0_etut=.false.
         L0_usut=.false.
         L0_isut=.false.
         L0_asut=.false.
         L0_orut=.false.
         L0_apav=.false.
         L0_omav=.false.
         L0_emav=.false.
         L0_ulav=.false.
         L0_ilav=.false.
         L0_alav=.false.
         L0_okav=.false.
         L0_ekav=.false.
         L0_ufav=.false.
         L0_ifav=.false.
         L0_udav=.false.
         L0_idav=.false.
         L0_adav=.false.
         L0_obav=.false.
         L0_ebav=.false.
         L0_uxut=.false.
         L0_ixut=.false.
         L0_axut=.false.
         L0_ovut=.false.
         L0_evut=.false.
         L0_otut=.false.
      else
         L0_ipav=L_(833)
         L0_efav=L_(832)
         L0_avut=L_(831)
         L0_itut=L_(830)
         L0_etut=L_(829)
         L0_usut=L0_atut
         L0_isut=L0_osut
         L0_asut=L0_esut
         L0_orut=L0_urut
         L0_apav=L0_epav
         L0_omav=L0_umav
         L0_emav=L0_imav
         L0_ulav=L0_amav
         L0_ilav=L0_olav
         L0_alav=L0_elav
         L0_okav=L0_ukav
         L0_ekav=L0_ikav
         L0_ufav=L0_akav
         L0_ifav=L0_ofav
         L0_udav=L0_afav
         L0_idav=L0_odav
         L0_adav=L0_edav
         L0_obav=L0_ubav
         L0_ebav=L0_ibav
         L0_uxut=L0_abav
         L0_ixut=L0_oxut
         L0_axut=L0_exut
         L0_ovut=L0_uvut
         L0_evut=L0_ivut
         L0_otut=L0_utut
      endif
C 1762-07-0001_log.fgi( 140, 253):��������� �������
      select case (I0_irav)
      case (1)
        C255_erav="�����.���������"
      case (2)
        C255_erav="�����, �������� 1"
      case (3)
        C255_erav="�����, �������� 2"
      case (4)
        C255_erav="����"
      case (5)
        C255_erav="������, �������� 1"
      case (6)
        C255_erav="???"
      case (7)
        C255_erav="???"
      case (8)
        C255_erav="???"
      case (9)
        C255_erav="???"
      case (10)
        C255_erav="???"
      case (11)
        C255_erav="???"
      case (12)
        C255_erav="???"
      case (13)
        C255_erav="???"
      case (14)
        C255_erav="???"
      case (15)
        C255_erav="???"
      case (16)
        C255_erav="???"
      case (17)
        C255_erav="???"
      case (18)
        C255_erav="???"
      case (19)
        C255_erav="???"
      case (20)
        C255_erav="???"
      case (21)
        C255_erav="???"
      case (22)
        C255_erav="???"
      case (23)
        C255_erav="???"
      case (24)
        C255_erav="???"
      case (25)
        C255_erav="???"
      case (26)
        C255_erav="???"
      case (27)
        C255_erav="???"
      case (28)
        C255_erav="???"
      case (29)
        C255_erav="???"
      case (30)
        C255_erav="???"
      end select
C 1762-07-0001_log.fgi( 156, 282):MarkVI text out 30 variants,1726-07-0001_RolZ_state
      L_(835)=.false.
C 1762-07-0001_log.fgi( 118,   8):��������� ���������� (�������)
      L_(834)=.false.
C 1762-07-0001_log.fgi( 118,  12):��������� ���������� (�������)
      L0_osav=R0_esav.ne.R0_asav
      R0_asav=R0_esav
C 1762-07-0001_log.fgi( 102,  10):���������� ������������� ������
      if(L_(834).or.L_(835)) then
         L_usav=(L_(834).or.L_usav).and..not.(L_(835))
      else
         if(L0_osav.and..not.L0_isav) L_usav=.not.L_usav
      endif
      L0_isav=L0_osav
      L_(836)=.not.L_usav
C 1762-07-0001_log.fgi( 124,  10):T �������
      L_orav=L_usav
C 1762-07-0001_log.fgi( 154,   8):������,1762-07-0001_lamp23
      if(L_usav) then
         I_urav=1
      else
         I_urav=0
      endif
C 1762-07-0001_log.fgi( 138,  12):��������� LO->1
      L_(838)=.false.
C 1762-07-0001_log.fgi(  33, 284):��������� ���������� (�������)
      L_(837)=.false.
C 1762-07-0001_log.fgi(  33, 288):��������� ���������� (�������)
      L0_avav=R0_otav.ne.R0_itav
      R0_itav=R0_otav
C 1762-07-0001_log.fgi(  17, 286):���������� ������������� ������
      if(L_(837).or.L_(838)) then
         L_evav=(L_(837).or.L_evav).and..not.(L_(838))
      else
         if(L0_avav.and..not.L0_utav) L_evav=.not.L_evav
      endif
      L0_utav=L0_avav
      L_(839)=.not.L_evav
C 1762-07-0001_log.fgi(  39, 286):T �������
      L_atav=L_evav
C 1762-07-0001_log.fgi(  68, 284):������,1762-07-0001_lamp22
      if(L_evav) then
         I_etav=1
      else
         I_etav=0
      endif
C 1762-07-0001_log.fgi(  53, 288):��������� LO->1
      L_(841)=.false.
C 1762-07-0001_log.fgi(  33, 270):��������� ���������� (�������)
      L_(840)=.false.
C 1762-07-0001_log.fgi(  33, 274):��������� ���������� (�������)
      L0_ixav=R0_axav.ne.R0_uvav
      R0_uvav=R0_axav
C 1762-07-0001_log.fgi(  17, 272):���������� ������������� ������
      if(L_(840).or.L_(841)) then
         L_oxav=(L_(840).or.L_oxav).and..not.(L_(841))
      else
         if(L0_ixav.and..not.L0_exav) L_oxav=.not.L_oxav
      endif
      L0_exav=L0_ixav
      L_(842)=.not.L_oxav
C 1762-07-0001_log.fgi(  39, 272):T �������
      L_ivav=L_oxav
C 1762-07-0001_log.fgi(  68, 270):������,1762-07-0001_lamp21
      if(L_oxav) then
         I_ovav=1
      else
         I_ovav=0
      endif
C 1762-07-0001_log.fgi(  53, 274):��������� LO->1
      L_(844)=.false.
C 1762-07-0001_log.fgi(  33, 257):��������� ���������� (�������)
      L_(843)=.false.
C 1762-07-0001_log.fgi(  33, 261):��������� ���������� (�������)
      L0_ubev=R0_ibev.ne.R0_ebev
      R0_ebev=R0_ibev
C 1762-07-0001_log.fgi(  17, 259):���������� ������������� ������
      if(L_(843).or.L_(844)) then
         L_adev=(L_(843).or.L_adev).and..not.(L_(844))
      else
         if(L0_ubev.and..not.L0_obev) L_adev=.not.L_adev
      endif
      L0_obev=L0_ubev
      L_(845)=.not.L_adev
C 1762-07-0001_log.fgi(  39, 259):T �������
      L_uxav=L_adev
C 1762-07-0001_log.fgi(  68, 257):������,1762-07-0001_lamp20
      if(L_adev) then
         I_abev=1
      else
         I_abev=0
      endif
C 1762-07-0001_log.fgi(  53, 261):��������� LO->1
      L_(847)=.false.
C 1762-07-0001_log.fgi(  33, 244):��������� ���������� (�������)
      L_(846)=.false.
C 1762-07-0001_log.fgi(  33, 248):��������� ���������� (�������)
      L0_efev=R0_udev.ne.R0_odev
      R0_odev=R0_udev
C 1762-07-0001_log.fgi(  17, 246):���������� ������������� ������
      if(L_(846).or.L_(847)) then
         L_ifev=(L_(846).or.L_ifev).and..not.(L_(847))
      else
         if(L0_efev.and..not.L0_afev) L_ifev=.not.L_ifev
      endif
      L0_afev=L0_efev
      L_(848)=.not.L_ifev
C 1762-07-0001_log.fgi(  39, 246):T �������
      L_edev=L_ifev
C 1762-07-0001_log.fgi(  68, 244):������,1762-07-0001_lamp19
      if(L_ifev) then
         I_idev=1
      else
         I_idev=0
      endif
C 1762-07-0001_log.fgi(  53, 248):��������� LO->1
      L_(850)=.false.
C 1762-07-0001_log.fgi(  33, 231):��������� ���������� (�������)
      L_(849)=.false.
C 1762-07-0001_log.fgi(  33, 235):��������� ���������� (�������)
      L0_okev=R0_ekev.ne.R0_akev
      R0_akev=R0_ekev
C 1762-07-0001_log.fgi(  17, 233):���������� ������������� ������
      if(L_(849).or.L_(850)) then
         L_ukev=(L_(849).or.L_ukev).and..not.(L_(850))
      else
         if(L0_okev.and..not.L0_ikev) L_ukev=.not.L_ukev
      endif
      L0_ikev=L0_okev
      L_(851)=.not.L_ukev
C 1762-07-0001_log.fgi(  39, 233):T �������
      L_ofev=L_ukev
C 1762-07-0001_log.fgi(  68, 231):������,1762-07-0001_lamp18
      if(L_ukev) then
         I_ufev=1
      else
         I_ufev=0
      endif
C 1762-07-0001_log.fgi(  53, 235):��������� LO->1
      L_(853)=.false.
C 1762-07-0001_log.fgi(  33, 218):��������� ���������� (�������)
      L_(852)=.false.
C 1762-07-0001_log.fgi(  33, 222):��������� ���������� (�������)
      L0_amev=R0_olev.ne.R0_ilev
      R0_ilev=R0_olev
C 1762-07-0001_log.fgi(  17, 220):���������� ������������� ������
      if(L_(852).or.L_(853)) then
         L_emev=(L_(852).or.L_emev).and..not.(L_(853))
      else
         if(L0_amev.and..not.L0_ulev) L_emev=.not.L_emev
      endif
      L0_ulev=L0_amev
      L_(854)=.not.L_emev
C 1762-07-0001_log.fgi(  39, 220):T �������
      L_alev=L_emev
C 1762-07-0001_log.fgi(  68, 218):������,1762-07-0001_lamp17
      if(L_emev) then
         I_elev=1
      else
         I_elev=0
      endif
C 1762-07-0001_log.fgi(  53, 222):��������� LO->1
      L_(856)=.false.
C 1762-07-0001_log.fgi(  33, 205):��������� ���������� (�������)
      L_(855)=.false.
C 1762-07-0001_log.fgi(  33, 209):��������� ���������� (�������)
      L0_ipev=R0_apev.ne.R0_umev
      R0_umev=R0_apev
C 1762-07-0001_log.fgi(  17, 207):���������� ������������� ������
      if(L_(855).or.L_(856)) then
         L_opev=(L_(855).or.L_opev).and..not.(L_(856))
      else
         if(L0_ipev.and..not.L0_epev) L_opev=.not.L_opev
      endif
      L0_epev=L0_ipev
      L_(857)=.not.L_opev
C 1762-07-0001_log.fgi(  39, 207):T �������
      L_imev=L_opev
C 1762-07-0001_log.fgi(  68, 205):������,1762-07-0001_lamp16
      if(L_opev) then
         I_omev=1
      else
         I_omev=0
      endif
C 1762-07-0001_log.fgi(  53, 209):��������� LO->1
      L_(859)=.false.
C 1762-07-0001_log.fgi(  33, 192):��������� ���������� (�������)
      L_(858)=.false.
C 1762-07-0001_log.fgi(  33, 196):��������� ���������� (�������)
      L0_urev=R0_irev.ne.R0_erev
      R0_erev=R0_irev
C 1762-07-0001_log.fgi(  17, 194):���������� ������������� ������
      if(L_(858).or.L_(859)) then
         L_asev=(L_(858).or.L_asev).and..not.(L_(859))
      else
         if(L0_urev.and..not.L0_orev) L_asev=.not.L_asev
      endif
      L0_orev=L0_urev
      L_(860)=.not.L_asev
C 1762-07-0001_log.fgi(  39, 194):T �������
      L_upev=L_asev
C 1762-07-0001_log.fgi(  68, 192):������,1762-07-0001_lamp15
      if(L_asev) then
         I_arev=1
      else
         I_arev=0
      endif
C 1762-07-0001_log.fgi(  53, 196):��������� LO->1
      L_(862)=.false.
C 1762-07-0001_log.fgi(  33, 179):��������� ���������� (�������)
      L_(861)=.false.
C 1762-07-0001_log.fgi(  33, 183):��������� ���������� (�������)
      L0_etev=R0_usev.ne.R0_osev
      R0_osev=R0_usev
C 1762-07-0001_log.fgi(  17, 181):���������� ������������� ������
      if(L_(861).or.L_(862)) then
         L_itev=(L_(861).or.L_itev).and..not.(L_(862))
      else
         if(L0_etev.and..not.L0_atev) L_itev=.not.L_itev
      endif
      L0_atev=L0_etev
      L_(863)=.not.L_itev
C 1762-07-0001_log.fgi(  39, 181):T �������
      L_esev=L_itev
C 1762-07-0001_log.fgi(  68, 179):������,1762-07-0001_lamp14
      if(L_itev) then
         I_isev=1
      else
         I_isev=0
      endif
C 1762-07-0001_log.fgi(  53, 183):��������� LO->1
      L_(865)=.false.
C 1762-07-0001_log.fgi(  33, 165):��������� ���������� (�������)
      L_(864)=.false.
C 1762-07-0001_log.fgi(  33, 169):��������� ���������� (�������)
      L0_ovev=R0_evev.ne.R0_avev
      R0_avev=R0_evev
C 1762-07-0001_log.fgi(  17, 167):���������� ������������� ������
      if(L_(864).or.L_(865)) then
         L_uvev=(L_(864).or.L_uvev).and..not.(L_(865))
      else
         if(L0_ovev.and..not.L0_ivev) L_uvev=.not.L_uvev
      endif
      L0_ivev=L0_ovev
      L_(866)=.not.L_uvev
C 1762-07-0001_log.fgi(  39, 167):T �������
      L_otev=L_uvev
C 1762-07-0001_log.fgi(  68, 165):������,1762-07-0001_lamp13
      if(L_uvev) then
         I_utev=1
      else
         I_utev=0
      endif
C 1762-07-0001_log.fgi(  53, 169):��������� LO->1
      L_(868)=.false.
C 1762-07-0001_log.fgi(  33, 152):��������� ���������� (�������)
      L_(867)=.false.
C 1762-07-0001_log.fgi(  33, 156):��������� ���������� (�������)
      L0_abiv=R0_oxev.ne.R0_ixev
      R0_ixev=R0_oxev
C 1762-07-0001_log.fgi(  17, 154):���������� ������������� ������
      if(L_(867).or.L_(868)) then
         L_ebiv=(L_(867).or.L_ebiv).and..not.(L_(868))
      else
         if(L0_abiv.and..not.L0_uxev) L_ebiv=.not.L_ebiv
      endif
      L0_uxev=L0_abiv
      L_(869)=.not.L_ebiv
C 1762-07-0001_log.fgi(  39, 154):T �������
      L_axev=L_ebiv
C 1762-07-0001_log.fgi(  68, 152):������,1762-07-0001_lamp12
      if(L_ebiv) then
         I_exev=1
      else
         I_exev=0
      endif
C 1762-07-0001_log.fgi(  53, 156):��������� LO->1
      L_(871)=.false.
C 1762-07-0001_log.fgi(  33, 139):��������� ���������� (�������)
      L_(870)=.false.
C 1762-07-0001_log.fgi(  33, 143):��������� ���������� (�������)
      L0_idiv=R0_adiv.ne.R0_ubiv
      R0_ubiv=R0_adiv
C 1762-07-0001_log.fgi(  17, 141):���������� ������������� ������
      if(L_(870).or.L_(871)) then
         L_odiv=(L_(870).or.L_odiv).and..not.(L_(871))
      else
         if(L0_idiv.and..not.L0_ediv) L_odiv=.not.L_odiv
      endif
      L0_ediv=L0_idiv
      L_(872)=.not.L_odiv
C 1762-07-0001_log.fgi(  39, 141):T �������
      L_ibiv=L_odiv
C 1762-07-0001_log.fgi(  68, 139):������,1762-07-0001_lamp11
      if(L_odiv) then
         I_obiv=1
      else
         I_obiv=0
      endif
C 1762-07-0001_log.fgi(  53, 143):��������� LO->1
      L_(874)=.false.
C 1762-07-0001_log.fgi(  33, 126):��������� ���������� (�������)
      L_(873)=.false.
C 1762-07-0001_log.fgi(  33, 130):��������� ���������� (�������)
      L0_ufiv=R0_ifiv.ne.R0_efiv
      R0_efiv=R0_ifiv
C 1762-07-0001_log.fgi(  17, 128):���������� ������������� ������
      if(L_(873).or.L_(874)) then
         L_akiv=(L_(873).or.L_akiv).and..not.(L_(874))
      else
         if(L0_ufiv.and..not.L0_ofiv) L_akiv=.not.L_akiv
      endif
      L0_ofiv=L0_ufiv
      L_(875)=.not.L_akiv
C 1762-07-0001_log.fgi(  39, 128):T �������
      L_udiv=L_akiv
C 1762-07-0001_log.fgi(  68, 126):������,1762-07-0001_lamp10
      if(L_akiv) then
         I_afiv=1
      else
         I_afiv=0
      endif
C 1762-07-0001_log.fgi(  53, 130):��������� LO->1
      L_(877)=.false.
C 1762-07-0001_log.fgi(  33, 113):��������� ���������� (�������)
      L_(876)=.false.
C 1762-07-0001_log.fgi(  33, 117):��������� ���������� (�������)
      L0_eliv=R0_ukiv.ne.R0_okiv
      R0_okiv=R0_ukiv
C 1762-07-0001_log.fgi(  17, 115):���������� ������������� ������
      if(L_(876).or.L_(877)) then
         L_iliv=(L_(876).or.L_iliv).and..not.(L_(877))
      else
         if(L0_eliv.and..not.L0_aliv) L_iliv=.not.L_iliv
      endif
      L0_aliv=L0_eliv
      L_(878)=.not.L_iliv
C 1762-07-0001_log.fgi(  39, 115):T �������
      L_ekiv=L_iliv
C 1762-07-0001_log.fgi(  68, 113):������,1762-07-0001_lamp09
      if(L_iliv) then
         I_ikiv=1
      else
         I_ikiv=0
      endif
C 1762-07-0001_log.fgi(  53, 117):��������� LO->1
      L_(880)=.false.
C 1762-07-0001_log.fgi(  33, 100):��������� ���������� (�������)
      L_(879)=.false.
C 1762-07-0001_log.fgi(  33, 104):��������� ���������� (�������)
      L0_omiv=R0_emiv.ne.R0_amiv
      R0_amiv=R0_emiv
C 1762-07-0001_log.fgi(  17, 102):���������� ������������� ������
      if(L_(879).or.L_(880)) then
         L_umiv=(L_(879).or.L_umiv).and..not.(L_(880))
      else
         if(L0_omiv.and..not.L0_imiv) L_umiv=.not.L_umiv
      endif
      L0_imiv=L0_omiv
      L_(881)=.not.L_umiv
C 1762-07-0001_log.fgi(  39, 102):T �������
      L_oliv=L_umiv
C 1762-07-0001_log.fgi(  68, 100):������,1762-07-0001_lamp08
      if(L_umiv) then
         I_uliv=1
      else
         I_uliv=0
      endif
C 1762-07-0001_log.fgi(  53, 104):��������� LO->1
      L_(883)=.false.
C 1762-07-0001_log.fgi(  33,  87):��������� ���������� (�������)
      L_(882)=.false.
C 1762-07-0001_log.fgi(  33,  91):��������� ���������� (�������)
      L0_ariv=R0_opiv.ne.R0_ipiv
      R0_ipiv=R0_opiv
C 1762-07-0001_log.fgi(  17,  89):���������� ������������� ������
      if(L_(882).or.L_(883)) then
         L_eriv=(L_(882).or.L_eriv).and..not.(L_(883))
      else
         if(L0_ariv.and..not.L0_upiv) L_eriv=.not.L_eriv
      endif
      L0_upiv=L0_ariv
      L_(884)=.not.L_eriv
C 1762-07-0001_log.fgi(  39,  89):T �������
      L_apiv=L_eriv
C 1762-07-0001_log.fgi(  68,  87):������,1762-07-0001_lamp07 
      if(L_eriv) then
         I_epiv=1
      else
         I_epiv=0
      endif
C 1762-07-0001_log.fgi(  53,  91):��������� LO->1
      L_(886)=.false.
C 1762-07-0001_log.fgi(  33,  74):��������� ���������� (�������)
      L_(885)=.false.
C 1762-07-0001_log.fgi(  33,  78):��������� ���������� (�������)
      L0_isiv=R0_asiv.ne.R0_uriv
      R0_uriv=R0_asiv
C 1762-07-0001_log.fgi(  17,  76):���������� ������������� ������
      if(L_(885).or.L_(886)) then
         L_osiv=(L_(885).or.L_osiv).and..not.(L_(886))
      else
         if(L0_isiv.and..not.L0_esiv) L_osiv=.not.L_osiv
      endif
      L0_esiv=L0_isiv
      L_(887)=.not.L_osiv
C 1762-07-0001_log.fgi(  39,  76):T �������
      L_iriv=L_osiv
C 1762-07-0001_log.fgi(  68,  74):������,1762-07-0001_lamp06 
      if(L_osiv) then
         I_oriv=1
      else
         I_oriv=0
      endif
C 1762-07-0001_log.fgi(  53,  78):��������� LO->1
      L_(889)=.false.
C 1762-07-0001_log.fgi(  33,  60):��������� ���������� (�������)
      L_(888)=.false.
C 1762-07-0001_log.fgi(  33,  64):��������� ���������� (�������)
      L0_utiv=R0_itiv.ne.R0_etiv
      R0_etiv=R0_itiv
C 1762-07-0001_log.fgi(  17,  62):���������� ������������� ������
      if(L_(888).or.L_(889)) then
         L_aviv=(L_(888).or.L_aviv).and..not.(L_(889))
      else
         if(L0_utiv.and..not.L0_otiv) L_aviv=.not.L_aviv
      endif
      L0_otiv=L0_utiv
      L_(890)=.not.L_aviv
C 1762-07-0001_log.fgi(  39,  62):T �������
      L_usiv=L_aviv
C 1762-07-0001_log.fgi(  68,  60):������,1762-07-0001_lamp05 
      if(L_aviv) then
         I_ativ=1
      else
         I_ativ=0
      endif
C 1762-07-0001_log.fgi(  53,  64):��������� LO->1
      L_(892)=.false.
C 1762-07-0001_log.fgi(  33,  47):��������� ���������� (�������)
      L_(891)=.false.
C 1762-07-0001_log.fgi(  33,  51):��������� ���������� (�������)
      L0_exiv=R0_uviv.ne.R0_oviv
      R0_oviv=R0_uviv
C 1762-07-0001_log.fgi(  17,  49):���������� ������������� ������
      if(L_(891).or.L_(892)) then
         L_ixiv=(L_(891).or.L_ixiv).and..not.(L_(892))
      else
         if(L0_exiv.and..not.L0_axiv) L_ixiv=.not.L_ixiv
      endif
      L0_axiv=L0_exiv
      L_(893)=.not.L_ixiv
C 1762-07-0001_log.fgi(  39,  49):T �������
      L_eviv=L_ixiv
C 1762-07-0001_log.fgi(  68,  47):������,1762-07-0001_lamp04 
      if(L_ixiv) then
         I_iviv=1
      else
         I_iviv=0
      endif
C 1762-07-0001_log.fgi(  53,  51):��������� LO->1
      L_(895)=.false.
C 1762-07-0001_log.fgi(  33,  34):��������� ���������� (�������)
      L_(894)=.false.
C 1762-07-0001_log.fgi(  33,  38):��������� ���������� (�������)
      L0_obov=R0_ebov.ne.R0_abov
      R0_abov=R0_ebov
C 1762-07-0001_log.fgi(  17,  36):���������� ������������� ������
      if(L_(894).or.L_(895)) then
         L_ubov=(L_(894).or.L_ubov).and..not.(L_(895))
      else
         if(L0_obov.and..not.L0_ibov) L_ubov=.not.L_ubov
      endif
      L0_ibov=L0_obov
      L_(896)=.not.L_ubov
C 1762-07-0001_log.fgi(  39,  36):T �������
      L_oxiv=L_ubov
C 1762-07-0001_log.fgi(  68,  34):������,1762-07-0001_lamp03 
      if(L_ubov) then
         I_uxiv=1
      else
         I_uxiv=0
      endif
C 1762-07-0001_log.fgi(  53,  38):��������� LO->1
      L_(898)=.false.
C 1762-07-0001_log.fgi(  33,  21):��������� ���������� (�������)
      L_(897)=.false.
C 1762-07-0001_log.fgi(  33,  25):��������� ���������� (�������)
      L0_afov=R0_odov.ne.R0_idov
      R0_idov=R0_odov
C 1762-07-0001_log.fgi(  17,  23):���������� ������������� ������
      if(L_(897).or.L_(898)) then
         L_efov=(L_(897).or.L_efov).and..not.(L_(898))
      else
         if(L0_afov.and..not.L0_udov) L_efov=.not.L_efov
      endif
      L0_udov=L0_afov
      L_(899)=.not.L_efov
C 1762-07-0001_log.fgi(  39,  23):T �������
      L_adov=L_efov
C 1762-07-0001_log.fgi(  68,  21):������,1762-07-0001_lamp02 
      if(L_efov) then
         I_edov=1
      else
         I_edov=0
      endif
C 1762-07-0001_log.fgi(  53,  25):��������� LO->1
      L_(901)=.false.
C 1762-07-0001_log.fgi(  33,   8):��������� ���������� (�������)
      L_(900)=.false.
C 1762-07-0001_log.fgi(  33,  12):��������� ���������� (�������)
      L0_ikov=R0_akov.ne.R0_ufov
      R0_ufov=R0_akov
C 1762-07-0001_log.fgi(  17,  10):���������� ������������� ������
      if(L_(900).or.L_(901)) then
         L_okov=(L_(900).or.L_okov).and..not.(L_(901))
      else
         if(L0_ikov.and..not.L0_ekov) L_okov=.not.L_okov
      endif
      L0_ekov=L0_ikov
      L_(902)=.not.L_okov
C 1762-07-0001_log.fgi(  39,  10):T �������
      L_ifov=L_okov
C 1762-07-0001_log.fgi(  68,   8):������,1762-07-0001_lamp01 
      if(L_okov) then
         I_ofov=1
      else
         I_ofov=0
      endif
C 1762-07-0001_log.fgi(  53,  12):��������� LO->1
      !��������� I_alov = 1762-07-0001_logC?? /0/
      I_alov=I0_ukov
C 1762-07-0001_log.fgi( 387,  33):��������� ������������� IN
      I_(28) = 8421440
C 1762-06-0001_log.fgi( 361, 263):��������� ������������� IN (�������)
      !�����. �������� I0_osov = 1762-06-0001_logC?? /z'01000016'
C /
C 1762-06-0001_log.fgi( 360, 255):��������� �����
      !�����. �������� I0_usov = 1762-06-0001_logC?? /z'01000007'
C /
C 1762-06-0001_log.fgi( 360, 247):��������� �����
      I_(29) = 16711680
C 1762-06-0001_log.fgi( 361, 221):��������� ������������� IN (�������)
      !�����. �������� I0_otov = 1762-06-0001_logC?? /z'0100000A'
C /
C 1762-06-0001_log.fgi( 360, 113):��������� �����
      R0_etov=R0_etov+deltat
      if(R0_etov.ge.R0_itov) R0_etov=0.0
      L_(904)=R0_etov.le.R0_atov.and.R0_etov.gt.0.0
C 1762-06-0001_log.fgi( 338, 108):�������������� ���������
      if(L_(904)) then
         I_aluv=I0_otov
      else
         I_aluv=I0_utov
      endif
C 1762-06-0001_log.fgi( 370, 113):���� RE IN LO CH7
      I_ukuv=I_aluv
C 1762-06-0001_log.fgi( 395,  34):������,1762-06-0001_poly01 _color
      I_okuv=I_aluv
C 1762-06-0001_log.fgi( 395,  38):������,1762-06-0001_poly02 _color
      I_ikuv=I_aluv
C 1762-06-0001_log.fgi( 395,  42):������,1762-06-0001_poly03 _color
      I_ekuv=I_aluv
C 1762-06-0001_log.fgi( 395,  46):������,1762-06-0001_poly04 _color
      I_akuv=I_aluv
C 1762-06-0001_log.fgi( 395,  50):������,1762-06-0001_poly05 _color
      I_ufuv=I_aluv
C 1762-06-0001_log.fgi( 395,  54):������,1762-06-0001_poly06 _color
      I_oduv=I_aluv
C 1762-06-0001_log.fgi( 395,  58):������,1762-06-0001_poly10 _color
      I_iduv=I_aluv
C 1762-06-0001_log.fgi( 395,  62):������,1762-06-0001_poly11 _color
      I_eduv=I_aluv
C 1762-06-0001_log.fgi( 395,  66):������,1762-06-0001_poly12 _color
      I_aduv=I_aluv
C 1762-06-0001_log.fgi( 395,  70):������,1762-06-0001_poly13 _color
      I_ubuv=I_aluv
C 1762-06-0001_log.fgi( 395,  74):������,1762-06-0001_poly14 _color
      I_ebuv=I_aluv
C 1762-06-0001_log.fgi( 395,  78):������,1762-06-0001_poly17 _color
      I_abuv=I_aluv
C 1762-06-0001_log.fgi( 395,  82):������,1762-06-0001_poly18 _color
      I_uxov=I_aluv
C 1762-06-0001_log.fgi( 395,  86):������,1762-06-0001_poly19 _color
      I_oxov=I_aluv
C 1762-06-0001_log.fgi( 395,  90):������,1762-06-0001_poly20 _color
      I_ixov=I_aluv
C 1762-06-0001_log.fgi( 395,  94):������,1762-06-0001_poly21 _color
      I_exov=I_aluv
C 1762-06-0001_log.fgi( 395,  98):������,1762-06-0001_poly22 _color
      I_axov=I_aluv
C 1762-06-0001_log.fgi( 395, 102):������,1762-06-0001_poly23 _color
      I_isov=I_aluv
C 1762-06-0001_log.fgi( 395, 110):������,1762-06-0001_poly29 _color
      I_esov=I_aluv
C 1762-06-0001_log.fgi( 395, 114):������,1762-06-0001_poly30 _color
      I_asov=I_aluv
C 1762-06-0001_log.fgi( 395, 118):������,1762-06-0001_poly31 _color
      I_urov=I_aluv
C 1762-06-0001_log.fgi( 395, 122):������,1762-06-0001_poly32 _color
      I_orov=I_aluv
C 1762-06-0001_log.fgi( 395, 130):������,1762-06-0001_ellipce01 _color
      I_irov=I_aluv
C 1762-06-0001_log.fgi( 395, 134):������,1762-06-0001_ellipce02 _color
      I_erov=I_aluv
C 1762-06-0001_log.fgi( 395, 138):������,1762-06-0001_ellipce03 _color
      I_arov=I_aluv
C 1762-06-0001_log.fgi( 395, 142):������,1762-06-0001_ellipce04 _color
      I_upov=I_aluv
C 1762-06-0001_log.fgi( 395, 146):������,1762-06-0001_ellipce05 _color
      I_opov=I_aluv
C 1762-06-0001_log.fgi( 395, 150):������,1762-06-0001_ellipce06 _color
      I_ipov=I_aluv
C 1762-06-0001_log.fgi( 395, 154):������,1762-06-0001_ellipce07 _color
      I_epov=I_aluv
C 1762-06-0001_log.fgi( 395, 158):������,1762-06-0001_ellipce08 _color
      I_apov=I_aluv
C 1762-06-0001_log.fgi( 395, 162):������,1762-06-0001_ellipce09 _color
      I_umov=I_aluv
C 1762-06-0001_log.fgi( 395, 166):������,1762-06-0001_ellipce10 _color
      I_omov=I_aluv
C 1762-06-0001_log.fgi( 395, 170):������,1762-06-0001_ellipce11 _color
      I_imov=I_aluv
C 1762-06-0001_log.fgi( 395, 174):������,1762-06-0001_ellipce12 _color
      I_emov=I_aluv
C 1762-06-0001_log.fgi( 395, 178):������,1762-06-0001_ellipce13 _color
      I_amov=I_aluv
C 1762-06-0001_log.fgi( 395, 182):������,1762-06-0001_ellipce14 _color
      I_ulov=I_aluv
C 1762-06-0001_log.fgi( 395, 186):������,1762-06-0001_ellipce15 _color
      I_olov=I_aluv
C 1762-06-0001_log.fgi( 395, 190):������,1762-06-0001_ellipce16 _color
      I_ilov=I_aluv
C 1762-06-0001_log.fgi( 395, 194):������,1762-06-0001_ellipce17 _color
      I_elov=I_aluv
C 1762-06-0001_log.fgi( 395, 198):������,1762-06-0001_ellipce18 _color
      I_ovov=I_aluv
C 1762-06-0001_log.fgi( 395, 106):������,1762-06-0001_poly25 _color
      if(L_(904)) then
         I_efuv=I_(29)
      else
         I_efuv=I0_utov
      endif
C 1762-06-0001_log.fgi( 370, 221):���� RE IN LO CH7
      I_afuv=I_efuv
C 1762-06-0001_log.fgi( 395, 218):������,1762-06-0001_poly08 _color
      I_ivov=I_efuv
C 1762-06-0001_log.fgi( 395, 210):������,1762-06-0001_poly26 _color
      I_obuv=I_efuv
C 1762-06-0001_log.fgi( 395, 222):������,1762-06-0001_poly15 _color
      if(L_(904)) then
         I_ofuv=I0_usov
      else
         I_ofuv=I0_utov
      endif
C 1762-06-0001_log.fgi( 370, 247):���� RE IN LO CH7
      I_ifuv=I_ofuv
C 1762-06-0001_log.fgi( 395, 236):������,1762-06-0001_poly07 _color
      I_ibuv=I_ofuv
C 1762-06-0001_log.fgi( 395, 232):������,1762-06-0001_poly16 _color
      I_uduv=I_ofuv
C 1762-06-0001_log.fgi( 395, 240):������,1762-06-0001_poly09 _color
      I_uvov=I_ofuv
C 1762-06-0001_log.fgi( 395, 244):������,1762-06-0001_poly24 _color
      if(L_(904)) then
         I_avov=I_(28)
      else
         I_avov=I0_osov
      endif
C 1762-06-0001_log.fgi( 370, 263):���� RE IN LO CH7
      if(L_(904)) then
         I_evov=I0_osov
      else
         I_evov=I_(28)
      endif
C 1762-06-0001_log.fgi( 370, 255):���� RE IN LO CH7
      !�����. �������� I0_utov = 1762-06-0001_logC?? /z'01000003'
C /
C 1762-06-0001_log.fgi( 360, 115):��������� �����
      L_(906)=.false.
C 1762-06-0001_log.fgi(  36, 263):��������� ���������� (�������)
      L_(905)=.false.
C 1762-06-0001_log.fgi(  36, 267):��������� ���������� (�������)
      L0_emuv=R0_uluv.ne.R0_oluv
      R0_oluv=R0_uluv
C 1762-06-0001_log.fgi(  20, 265):���������� ������������� ������
      if(L_(905).or.L_(906)) then
         L_imuv=(L_(905).or.L_imuv).and..not.(L_(906))
      else
         if(L0_emuv.and..not.L0_amuv) L_imuv=.not.L_imuv
      endif
      L0_amuv=L0_emuv
      L_(907)=.not.L_imuv
C 1762-06-0001_log.fgi(  42, 265):T �������
      L_eluv=L_imuv
C 1762-06-0001_log.fgi(  72, 271):������,1762-06-0001_lamp
      if(L_imuv) then
         I_iluv=1
      else
         I_iluv=0
      endif
C 1762-06-0001_log.fgi(  56, 267):��������� LO->1
      L_(909)=.false.
C 1762-06-0001_log.fgi( 268, 250):��������� ���������� (�������)
      L_(908)=.false.
C 1762-06-0001_log.fgi( 268, 254):��������� ���������� (�������)
      L0_opuv=R0_epuv.ne.R0_apuv
      R0_apuv=R0_epuv
C 1762-06-0001_log.fgi( 252, 252):���������� ������������� ������
      if(L_(908).or.L_(909)) then
         L_upuv=(L_(908).or.L_upuv).and..not.(L_(909))
      else
         if(L0_opuv.and..not.L0_ipuv) L_upuv=.not.L_upuv
      endif
      L0_ipuv=L0_opuv
      L_(910)=.not.L_upuv
C 1762-06-0001_log.fgi( 274, 252):T �������
      L_omuv=L_upuv
C 1762-06-0001_log.fgi( 304, 258):������,1762-06-0001_lamp72 
      if(L_upuv) then
         I_umuv=1
      else
         I_umuv=0
      endif
C 1762-06-0001_log.fgi( 288, 254):��������� LO->1
      L_(912)=.false.
C 1762-06-0001_log.fgi( 192, 250):��������� ���������� (�������)
      L_(911)=.false.
C 1762-06-0001_log.fgi( 192, 254):��������� ���������� (�������)
      L0_asuv=R0_oruv.ne.R0_iruv
      R0_iruv=R0_oruv
C 1762-06-0001_log.fgi( 176, 252):���������� ������������� ������
      if(L_(911).or.L_(912)) then
         L_esuv=(L_(911).or.L_esuv).and..not.(L_(912))
      else
         if(L0_asuv.and..not.L0_uruv) L_esuv=.not.L_esuv
      endif
      L0_uruv=L0_asuv
      L_(913)=.not.L_esuv
C 1762-06-0001_log.fgi( 198, 252):T �������
      L_aruv=L_esuv
C 1762-06-0001_log.fgi( 228, 258):������,1762-06-0001_lamp71 
      if(L_esuv) then
         I_eruv=1
      else
         I_eruv=0
      endif
C 1762-06-0001_log.fgi( 212, 254):��������� LO->1
      L_(915)=.false.
C 1762-06-0001_log.fgi( 114, 250):��������� ���������� (�������)
      L_(914)=.false.
C 1762-06-0001_log.fgi( 114, 254):��������� ���������� (�������)
      L0_ituv=R0_atuv.ne.R0_usuv
      R0_usuv=R0_atuv
C 1762-06-0001_log.fgi(  98, 252):���������� ������������� ������
      if(L_(914).or.L_(915)) then
         L_otuv=(L_(914).or.L_otuv).and..not.(L_(915))
      else
         if(L0_ituv.and..not.L0_etuv) L_otuv=.not.L_otuv
      endif
      L0_etuv=L0_ituv
      L_(916)=.not.L_otuv
C 1762-06-0001_log.fgi( 120, 252):T �������
      L_isuv=L_otuv
C 1762-06-0001_log.fgi( 150, 258):������,1762-06-0001_lamp70 
      if(L_otuv) then
         I_osuv=1
      else
         I_osuv=0
      endif
C 1762-06-0001_log.fgi( 134, 254):��������� LO->1
      L_(918)=.false.
C 1762-06-0001_log.fgi(  36, 250):��������� ���������� (�������)
      L_(917)=.false.
C 1762-06-0001_log.fgi(  36, 254):��������� ���������� (�������)
      L0_uvuv=R0_ivuv.ne.R0_evuv
      R0_evuv=R0_ivuv
C 1762-06-0001_log.fgi(  20, 252):���������� ������������� ������
      if(L_(917).or.L_(918)) then
         L_axuv=(L_(917).or.L_axuv).and..not.(L_(918))
      else
         if(L0_uvuv.and..not.L0_ovuv) L_axuv=.not.L_axuv
      endif
      L0_ovuv=L0_uvuv
      L_(919)=.not.L_axuv
C 1762-06-0001_log.fgi(  42, 252):T �������
      L_utuv=L_axuv
C 1762-06-0001_log.fgi(  72, 258):������,1762-06-0001_lamp69 
      if(L_axuv) then
         I_avuv=1
      else
         I_avuv=0
      endif
C 1762-06-0001_log.fgi(  56, 254):��������� LO->1
      L_(921)=.false.
C 1762-06-0001_log.fgi( 268, 237):��������� ���������� (�������)
      L_(920)=.false.
C 1762-06-0001_log.fgi( 268, 241):��������� ���������� (�������)
      L0_ebax=R0_uxuv.ne.R0_oxuv
      R0_oxuv=R0_uxuv
C 1762-06-0001_log.fgi( 252, 239):���������� ������������� ������
      if(L_(920).or.L_(921)) then
         L_ibax=(L_(920).or.L_ibax).and..not.(L_(921))
      else
         if(L0_ebax.and..not.L0_abax) L_ibax=.not.L_ibax
      endif
      L0_abax=L0_ebax
      L_(922)=.not.L_ibax
C 1762-06-0001_log.fgi( 274, 239):T �������
      L_exuv=L_ibax
C 1762-06-0001_log.fgi( 304, 245):������,1762-06-0001_lamp68 
      if(L_ibax) then
         I_ixuv=1
      else
         I_ixuv=0
      endif
C 1762-06-0001_log.fgi( 288, 241):��������� LO->1
      L_(924)=.false.
C 1762-06-0001_log.fgi( 192, 237):��������� ���������� (�������)
      L_(923)=.false.
C 1762-06-0001_log.fgi( 192, 241):��������� ���������� (�������)
      L0_odax=R0_edax.ne.R0_adax
      R0_adax=R0_edax
C 1762-06-0001_log.fgi( 176, 239):���������� ������������� ������
      if(L_(923).or.L_(924)) then
         L_udax=(L_(923).or.L_udax).and..not.(L_(924))
      else
         if(L0_odax.and..not.L0_idax) L_udax=.not.L_udax
      endif
      L0_idax=L0_odax
      L_(925)=.not.L_udax
C 1762-06-0001_log.fgi( 198, 239):T �������
      L_obax=L_udax
C 1762-06-0001_log.fgi( 228, 245):������,1762-06-0001_lamp67 
      if(L_udax) then
         I_ubax=1
      else
         I_ubax=0
      endif
C 1762-06-0001_log.fgi( 212, 241):��������� LO->1
      L_(927)=.false.
C 1762-06-0001_log.fgi( 114, 237):��������� ���������� (�������)
      L_(926)=.false.
C 1762-06-0001_log.fgi( 114, 241):��������� ���������� (�������)
      L0_akax=R0_ofax.ne.R0_ifax
      R0_ifax=R0_ofax
C 1762-06-0001_log.fgi(  98, 239):���������� ������������� ������
      if(L_(926).or.L_(927)) then
         L_ekax=(L_(926).or.L_ekax).and..not.(L_(927))
      else
         if(L0_akax.and..not.L0_ufax) L_ekax=.not.L_ekax
      endif
      L0_ufax=L0_akax
      L_(928)=.not.L_ekax
C 1762-06-0001_log.fgi( 120, 239):T �������
      L_afax=L_ekax
C 1762-06-0001_log.fgi( 150, 245):������,1762-06-0001_lamp66 
      if(L_ekax) then
         I_efax=1
      else
         I_efax=0
      endif
C 1762-06-0001_log.fgi( 134, 241):��������� LO->1
      L_(930)=.false.
C 1762-06-0001_log.fgi(  36, 237):��������� ���������� (�������)
      L_(929)=.false.
C 1762-06-0001_log.fgi(  36, 241):��������� ���������� (�������)
      L0_ilax=R0_alax.ne.R0_ukax
      R0_ukax=R0_alax
C 1762-06-0001_log.fgi(  20, 239):���������� ������������� ������
      if(L_(929).or.L_(930)) then
         L_olax=(L_(929).or.L_olax).and..not.(L_(930))
      else
         if(L0_ilax.and..not.L0_elax) L_olax=.not.L_olax
      endif
      L0_elax=L0_ilax
      L_(931)=.not.L_olax
C 1762-06-0001_log.fgi(  42, 239):T �������
      L_ikax=L_olax
C 1762-06-0001_log.fgi(  72, 245):������,1762-06-0001_lamp65 
      if(L_olax) then
         I_okax=1
      else
         I_okax=0
      endif
C 1762-06-0001_log.fgi(  56, 241):��������� LO->1
      L_(933)=.false.
C 1762-06-0001_log.fgi( 268, 225):��������� ���������� (�������)
      L_(932)=.false.
C 1762-06-0001_log.fgi( 268, 229):��������� ���������� (�������)
      L0_umax=R0_imax.ne.R0_emax
      R0_emax=R0_imax
C 1762-06-0001_log.fgi( 252, 227):���������� ������������� ������
      if(L_(932).or.L_(933)) then
         L_apax=(L_(932).or.L_apax).and..not.(L_(933))
      else
         if(L0_umax.and..not.L0_omax) L_apax=.not.L_apax
      endif
      L0_omax=L0_umax
      L_(934)=.not.L_apax
C 1762-06-0001_log.fgi( 274, 227):T �������
      L_ulax=L_apax
C 1762-06-0001_log.fgi( 304, 233):������,1762-06-0001_lamp64 
      if(L_apax) then
         I_amax=1
      else
         I_amax=0
      endif
C 1762-06-0001_log.fgi( 288, 229):��������� LO->1
      L_(936)=.false.
C 1762-06-0001_log.fgi( 192, 225):��������� ���������� (�������)
      L_(935)=.false.
C 1762-06-0001_log.fgi( 192, 229):��������� ���������� (�������)
      L0_erax=R0_upax.ne.R0_opax
      R0_opax=R0_upax
C 1762-06-0001_log.fgi( 176, 227):���������� ������������� ������
      if(L_(935).or.L_(936)) then
         L_irax=(L_(935).or.L_irax).and..not.(L_(936))
      else
         if(L0_erax.and..not.L0_arax) L_irax=.not.L_irax
      endif
      L0_arax=L0_erax
      L_(937)=.not.L_irax
C 1762-06-0001_log.fgi( 198, 227):T �������
      L_epax=L_irax
C 1762-06-0001_log.fgi( 228, 233):������,1762-06-0001_lamp63 
      if(L_irax) then
         I_ipax=1
      else
         I_ipax=0
      endif
C 1762-06-0001_log.fgi( 212, 229):��������� LO->1
      L_(939)=.false.
C 1762-06-0001_log.fgi( 114, 225):��������� ���������� (�������)
      L_(938)=.false.
C 1762-06-0001_log.fgi( 114, 229):��������� ���������� (�������)
      L0_osax=R0_esax.ne.R0_asax
      R0_asax=R0_esax
C 1762-06-0001_log.fgi(  98, 227):���������� ������������� ������
      if(L_(938).or.L_(939)) then
         L_usax=(L_(938).or.L_usax).and..not.(L_(939))
      else
         if(L0_osax.and..not.L0_isax) L_usax=.not.L_usax
      endif
      L0_isax=L0_osax
      L_(940)=.not.L_usax
C 1762-06-0001_log.fgi( 120, 227):T �������
      L_orax=L_usax
C 1762-06-0001_log.fgi( 150, 233):������,1762-06-0001_lamp62 
      if(L_usax) then
         I_urax=1
      else
         I_urax=0
      endif
C 1762-06-0001_log.fgi( 134, 229):��������� LO->1
      L_(942)=.false.
C 1762-06-0001_log.fgi(  36, 225):��������� ���������� (�������)
      L_(941)=.false.
C 1762-06-0001_log.fgi(  36, 229):��������� ���������� (�������)
      L0_avax=R0_otax.ne.R0_itax
      R0_itax=R0_otax
C 1762-06-0001_log.fgi(  20, 227):���������� ������������� ������
      if(L_(941).or.L_(942)) then
         L_evax=(L_(941).or.L_evax).and..not.(L_(942))
      else
         if(L0_avax.and..not.L0_utax) L_evax=.not.L_evax
      endif
      L0_utax=L0_avax
      L_(943)=.not.L_evax
C 1762-06-0001_log.fgi(  42, 227):T �������
      L_atax=L_evax
C 1762-06-0001_log.fgi(  72, 233):������,1762-06-0001_lamp61 
      if(L_evax) then
         I_etax=1
      else
         I_etax=0
      endif
C 1762-06-0001_log.fgi(  56, 229):��������� LO->1
      L_(945)=.false.
C 1762-06-0001_log.fgi( 268, 212):��������� ���������� (�������)
      L_(944)=.false.
C 1762-06-0001_log.fgi( 268, 216):��������� ���������� (�������)
      L0_ixax=R0_axax.ne.R0_uvax
      R0_uvax=R0_axax
C 1762-06-0001_log.fgi( 252, 214):���������� ������������� ������
      if(L_(944).or.L_(945)) then
         L_oxax=(L_(944).or.L_oxax).and..not.(L_(945))
      else
         if(L0_ixax.and..not.L0_exax) L_oxax=.not.L_oxax
      endif
      L0_exax=L0_ixax
      L_(946)=.not.L_oxax
C 1762-06-0001_log.fgi( 274, 214):T �������
      L_ivax=L_oxax
C 1762-06-0001_log.fgi( 304, 220):������,1762-06-0001_lamp60 
      if(L_oxax) then
         I_ovax=1
      else
         I_ovax=0
      endif
C 1762-06-0001_log.fgi( 288, 216):��������� LO->1
      L_(948)=.false.
C 1762-06-0001_log.fgi( 192, 212):��������� ���������� (�������)
      L_(947)=.false.
C 1762-06-0001_log.fgi( 192, 216):��������� ���������� (�������)
      L0_ubex=R0_ibex.ne.R0_ebex
      R0_ebex=R0_ibex
C 1762-06-0001_log.fgi( 176, 214):���������� ������������� ������
      if(L_(947).or.L_(948)) then
         L_adex=(L_(947).or.L_adex).and..not.(L_(948))
      else
         if(L0_ubex.and..not.L0_obex) L_adex=.not.L_adex
      endif
      L0_obex=L0_ubex
      L_(949)=.not.L_adex
C 1762-06-0001_log.fgi( 198, 214):T �������
      L_uxax=L_adex
C 1762-06-0001_log.fgi( 228, 220):������,1762-06-0001_lamp59 
      if(L_adex) then
         I_abex=1
      else
         I_abex=0
      endif
C 1762-06-0001_log.fgi( 212, 216):��������� LO->1
      L_(951)=.false.
C 1762-06-0001_log.fgi( 114, 212):��������� ���������� (�������)
      L_(950)=.false.
C 1762-06-0001_log.fgi( 114, 216):��������� ���������� (�������)
      L0_efex=R0_udex.ne.R0_odex
      R0_odex=R0_udex
C 1762-06-0001_log.fgi(  98, 214):���������� ������������� ������
      if(L_(950).or.L_(951)) then
         L_ifex=(L_(950).or.L_ifex).and..not.(L_(951))
      else
         if(L0_efex.and..not.L0_afex) L_ifex=.not.L_ifex
      endif
      L0_afex=L0_efex
      L_(952)=.not.L_ifex
C 1762-06-0001_log.fgi( 120, 214):T �������
      L_edex=L_ifex
C 1762-06-0001_log.fgi( 150, 220):������,1762-06-0001_lamp58 
      if(L_ifex) then
         I_idex=1
      else
         I_idex=0
      endif
C 1762-06-0001_log.fgi( 134, 216):��������� LO->1
      L_(954)=.false.
C 1762-06-0001_log.fgi(  36, 212):��������� ���������� (�������)
      L_(953)=.false.
C 1762-06-0001_log.fgi(  36, 216):��������� ���������� (�������)
      L0_okex=R0_ekex.ne.R0_akex
      R0_akex=R0_ekex
C 1762-06-0001_log.fgi(  20, 214):���������� ������������� ������
      if(L_(953).or.L_(954)) then
         L_ukex=(L_(953).or.L_ukex).and..not.(L_(954))
      else
         if(L0_okex.and..not.L0_ikex) L_ukex=.not.L_ukex
      endif
      L0_ikex=L0_okex
      L_(955)=.not.L_ukex
C 1762-06-0001_log.fgi(  42, 214):T �������
      L_ofex=L_ukex
C 1762-06-0001_log.fgi(  72, 220):������,1762-06-0001_lamp57 
      if(L_ukex) then
         I_ufex=1
      else
         I_ufex=0
      endif
C 1762-06-0001_log.fgi(  56, 216):��������� LO->1
      L_(957)=.false.
C 1762-06-0001_log.fgi( 268, 199):��������� ���������� (�������)
      L_(956)=.false.
C 1762-06-0001_log.fgi( 268, 203):��������� ���������� (�������)
      L0_amex=R0_olex.ne.R0_ilex
      R0_ilex=R0_olex
C 1762-06-0001_log.fgi( 252, 201):���������� ������������� ������
      if(L_(956).or.L_(957)) then
         L_emex=(L_(956).or.L_emex).and..not.(L_(957))
      else
         if(L0_amex.and..not.L0_ulex) L_emex=.not.L_emex
      endif
      L0_ulex=L0_amex
      L_(958)=.not.L_emex
C 1762-06-0001_log.fgi( 274, 201):T �������
      L_alex=L_emex
C 1762-06-0001_log.fgi( 304, 207):������,1762-06-0001_lamp56 
      if(L_emex) then
         I_elex=1
      else
         I_elex=0
      endif
C 1762-06-0001_log.fgi( 288, 203):��������� LO->1
      L_(960)=.false.
C 1762-06-0001_log.fgi( 192, 199):��������� ���������� (�������)
      L_(959)=.false.
C 1762-06-0001_log.fgi( 192, 203):��������� ���������� (�������)
      L0_ipex=R0_apex.ne.R0_umex
      R0_umex=R0_apex
C 1762-06-0001_log.fgi( 176, 201):���������� ������������� ������
      if(L_(959).or.L_(960)) then
         L_opex=(L_(959).or.L_opex).and..not.(L_(960))
      else
         if(L0_ipex.and..not.L0_epex) L_opex=.not.L_opex
      endif
      L0_epex=L0_ipex
      L_(961)=.not.L_opex
C 1762-06-0001_log.fgi( 198, 201):T �������
      L_imex=L_opex
C 1762-06-0001_log.fgi( 228, 207):������,1762-06-0001_lamp55 
      if(L_opex) then
         I_omex=1
      else
         I_omex=0
      endif
C 1762-06-0001_log.fgi( 212, 203):��������� LO->1
      L_(963)=.false.
C 1762-06-0001_log.fgi( 114, 199):��������� ���������� (�������)
      End
      Subroutine X20FDC20_2(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include '20FDC20.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      L_(962)=.false.
C 1762-06-0001_log.fgi( 114, 203):��������� ���������� (�������)
      L0_urex=R0_irex.ne.R0_erex
      R0_erex=R0_irex
C 1762-06-0001_log.fgi(  98, 201):���������� ������������� ������
      if(L_(962).or.L_(963)) then
         L_asex=(L_(962).or.L_asex).and..not.(L_(963))
      else
         if(L0_urex.and..not.L0_orex) L_asex=.not.L_asex
      endif
      L0_orex=L0_urex
      L_(964)=.not.L_asex
C 1762-06-0001_log.fgi( 120, 201):T �������
      L_upex=L_asex
C 1762-06-0001_log.fgi( 150, 207):������,1762-06-0001_lamp54 
      if(L_asex) then
         I_arex=1
      else
         I_arex=0
      endif
C 1762-06-0001_log.fgi( 134, 203):��������� LO->1
      L_(966)=.false.
C 1762-06-0001_log.fgi(  36, 199):��������� ���������� (�������)
      L_(965)=.false.
C 1762-06-0001_log.fgi(  36, 203):��������� ���������� (�������)
      L0_etex=R0_usex.ne.R0_osex
      R0_osex=R0_usex
C 1762-06-0001_log.fgi(  20, 201):���������� ������������� ������
      if(L_(965).or.L_(966)) then
         L_itex=(L_(965).or.L_itex).and..not.(L_(966))
      else
         if(L0_etex.and..not.L0_atex) L_itex=.not.L_itex
      endif
      L0_atex=L0_etex
      L_(967)=.not.L_itex
C 1762-06-0001_log.fgi(  42, 201):T �������
      L_esex=L_itex
C 1762-06-0001_log.fgi(  72, 207):������,1762-06-0001_lamp53 
      if(L_itex) then
         I_isex=1
      else
         I_isex=0
      endif
C 1762-06-0001_log.fgi(  56, 203):��������� LO->1
      L_(969)=.false.
C 1762-06-0001_log.fgi( 268, 187):��������� ���������� (�������)
      L_(968)=.false.
C 1762-06-0001_log.fgi( 268, 191):��������� ���������� (�������)
      L0_ovex=R0_evex.ne.R0_avex
      R0_avex=R0_evex
C 1762-06-0001_log.fgi( 252, 189):���������� ������������� ������
      if(L_(968).or.L_(969)) then
         L_uvex=(L_(968).or.L_uvex).and..not.(L_(969))
      else
         if(L0_ovex.and..not.L0_ivex) L_uvex=.not.L_uvex
      endif
      L0_ivex=L0_ovex
      L_(970)=.not.L_uvex
C 1762-06-0001_log.fgi( 274, 189):T �������
      L_otex=L_uvex
C 1762-06-0001_log.fgi( 304, 195):������,1762-06-0001_lamp52 
      if(L_uvex) then
         I_utex=1
      else
         I_utex=0
      endif
C 1762-06-0001_log.fgi( 288, 191):��������� LO->1
      L_(972)=.false.
C 1762-06-0001_log.fgi( 192, 187):��������� ���������� (�������)
      L_(971)=.false.
C 1762-06-0001_log.fgi( 192, 191):��������� ���������� (�������)
      L0_abix=R0_oxex.ne.R0_ixex
      R0_ixex=R0_oxex
C 1762-06-0001_log.fgi( 176, 189):���������� ������������� ������
      if(L_(971).or.L_(972)) then
         L_ebix=(L_(971).or.L_ebix).and..not.(L_(972))
      else
         if(L0_abix.and..not.L0_uxex) L_ebix=.not.L_ebix
      endif
      L0_uxex=L0_abix
      L_(973)=.not.L_ebix
C 1762-06-0001_log.fgi( 198, 189):T �������
      L_axex=L_ebix
C 1762-06-0001_log.fgi( 228, 195):������,1762-06-0001_lamp51 
      if(L_ebix) then
         I_exex=1
      else
         I_exex=0
      endif
C 1762-06-0001_log.fgi( 212, 191):��������� LO->1
      L_(975)=.false.
C 1762-06-0001_log.fgi( 114, 187):��������� ���������� (�������)
      L_(974)=.false.
C 1762-06-0001_log.fgi( 114, 191):��������� ���������� (�������)
      L0_idix=R0_adix.ne.R0_ubix
      R0_ubix=R0_adix
C 1762-06-0001_log.fgi(  98, 189):���������� ������������� ������
      if(L_(974).or.L_(975)) then
         L_odix=(L_(974).or.L_odix).and..not.(L_(975))
      else
         if(L0_idix.and..not.L0_edix) L_odix=.not.L_odix
      endif
      L0_edix=L0_idix
      L_(976)=.not.L_odix
C 1762-06-0001_log.fgi( 120, 189):T �������
      L_ibix=L_odix
C 1762-06-0001_log.fgi( 150, 195):������,1762-06-0001_lamp50 
      if(L_odix) then
         I_obix=1
      else
         I_obix=0
      endif
C 1762-06-0001_log.fgi( 134, 191):��������� LO->1
      L_(978)=.false.
C 1762-06-0001_log.fgi(  36, 187):��������� ���������� (�������)
      L_(977)=.false.
C 1762-06-0001_log.fgi(  36, 191):��������� ���������� (�������)
      L0_ufix=R0_ifix.ne.R0_efix
      R0_efix=R0_ifix
C 1762-06-0001_log.fgi(  20, 189):���������� ������������� ������
      if(L_(977).or.L_(978)) then
         L_akix=(L_(977).or.L_akix).and..not.(L_(978))
      else
         if(L0_ufix.and..not.L0_ofix) L_akix=.not.L_akix
      endif
      L0_ofix=L0_ufix
      L_(979)=.not.L_akix
C 1762-06-0001_log.fgi(  42, 189):T �������
      L_udix=L_akix
C 1762-06-0001_log.fgi(  72, 195):������,1762-06-0001_lamp49 
      if(L_akix) then
         I_afix=1
      else
         I_afix=0
      endif
C 1762-06-0001_log.fgi(  56, 191):��������� LO->1
      L_(981)=.false.
C 1762-06-0001_log.fgi( 268, 174):��������� ���������� (�������)
      L_(980)=.false.
C 1762-06-0001_log.fgi( 268, 178):��������� ���������� (�������)
      L0_elix=R0_ukix.ne.R0_okix
      R0_okix=R0_ukix
C 1762-06-0001_log.fgi( 252, 176):���������� ������������� ������
      if(L_(980).or.L_(981)) then
         L_ilix=(L_(980).or.L_ilix).and..not.(L_(981))
      else
         if(L0_elix.and..not.L0_alix) L_ilix=.not.L_ilix
      endif
      L0_alix=L0_elix
      L_(982)=.not.L_ilix
C 1762-06-0001_log.fgi( 274, 176):T �������
      L_ekix=L_ilix
C 1762-06-0001_log.fgi( 304, 182):������,1762-06-0001_lamp48 
      if(L_ilix) then
         I_ikix=1
      else
         I_ikix=0
      endif
C 1762-06-0001_log.fgi( 288, 178):��������� LO->1
      L_(984)=.false.
C 1762-06-0001_log.fgi( 192, 174):��������� ���������� (�������)
      L_(983)=.false.
C 1762-06-0001_log.fgi( 192, 178):��������� ���������� (�������)
      L0_omix=R0_emix.ne.R0_amix
      R0_amix=R0_emix
C 1762-06-0001_log.fgi( 176, 176):���������� ������������� ������
      if(L_(983).or.L_(984)) then
         L_umix=(L_(983).or.L_umix).and..not.(L_(984))
      else
         if(L0_omix.and..not.L0_imix) L_umix=.not.L_umix
      endif
      L0_imix=L0_omix
      L_(985)=.not.L_umix
C 1762-06-0001_log.fgi( 198, 176):T �������
      L_olix=L_umix
C 1762-06-0001_log.fgi( 228, 182):������,1762-06-0001_lamp47 
      if(L_umix) then
         I_ulix=1
      else
         I_ulix=0
      endif
C 1762-06-0001_log.fgi( 212, 178):��������� LO->1
      L_(987)=.false.
C 1762-06-0001_log.fgi( 114, 174):��������� ���������� (�������)
      L_(986)=.false.
C 1762-06-0001_log.fgi( 114, 178):��������� ���������� (�������)
      L0_arix=R0_opix.ne.R0_ipix
      R0_ipix=R0_opix
C 1762-06-0001_log.fgi(  98, 176):���������� ������������� ������
      if(L_(986).or.L_(987)) then
         L_erix=(L_(986).or.L_erix).and..not.(L_(987))
      else
         if(L0_arix.and..not.L0_upix) L_erix=.not.L_erix
      endif
      L0_upix=L0_arix
      L_(988)=.not.L_erix
C 1762-06-0001_log.fgi( 120, 176):T �������
      L_apix=L_erix
C 1762-06-0001_log.fgi( 150, 182):������,1762-06-0001_lamp46 
      if(L_erix) then
         I_epix=1
      else
         I_epix=0
      endif
C 1762-06-0001_log.fgi( 134, 178):��������� LO->1
      L_(990)=.false.
C 1762-06-0001_log.fgi(  36, 174):��������� ���������� (�������)
      L_(989)=.false.
C 1762-06-0001_log.fgi(  36, 178):��������� ���������� (�������)
      L0_isix=R0_asix.ne.R0_urix
      R0_urix=R0_asix
C 1762-06-0001_log.fgi(  20, 176):���������� ������������� ������
      if(L_(989).or.L_(990)) then
         L_osix=(L_(989).or.L_osix).and..not.(L_(990))
      else
         if(L0_isix.and..not.L0_esix) L_osix=.not.L_osix
      endif
      L0_esix=L0_isix
      L_(991)=.not.L_osix
C 1762-06-0001_log.fgi(  42, 176):T �������
      L_irix=L_osix
C 1762-06-0001_log.fgi(  72, 182):������,1762-06-0001_lamp45 
      if(L_osix) then
         I_orix=1
      else
         I_orix=0
      endif
C 1762-06-0001_log.fgi(  56, 178):��������� LO->1
      L_(993)=.false.
C 1762-06-0001_log.fgi( 268, 161):��������� ���������� (�������)
      L_(992)=.false.
C 1762-06-0001_log.fgi( 268, 165):��������� ���������� (�������)
      L0_utix=R0_itix.ne.R0_etix
      R0_etix=R0_itix
C 1762-06-0001_log.fgi( 252, 163):���������� ������������� ������
      if(L_(992).or.L_(993)) then
         L_avix=(L_(992).or.L_avix).and..not.(L_(993))
      else
         if(L0_utix.and..not.L0_otix) L_avix=.not.L_avix
      endif
      L0_otix=L0_utix
      L_(994)=.not.L_avix
C 1762-06-0001_log.fgi( 274, 163):T �������
      L_usix=L_avix
C 1762-06-0001_log.fgi( 304, 169):������,1762-06-0001_lamp44 
      if(L_avix) then
         I_atix=1
      else
         I_atix=0
      endif
C 1762-06-0001_log.fgi( 288, 165):��������� LO->1
      L_(996)=.false.
C 1762-06-0001_log.fgi( 192, 161):��������� ���������� (�������)
      L_(995)=.false.
C 1762-06-0001_log.fgi( 192, 165):��������� ���������� (�������)
      L0_exix=R0_uvix.ne.R0_ovix
      R0_ovix=R0_uvix
C 1762-06-0001_log.fgi( 176, 163):���������� ������������� ������
      if(L_(995).or.L_(996)) then
         L_ixix=(L_(995).or.L_ixix).and..not.(L_(996))
      else
         if(L0_exix.and..not.L0_axix) L_ixix=.not.L_ixix
      endif
      L0_axix=L0_exix
      L_(997)=.not.L_ixix
C 1762-06-0001_log.fgi( 198, 163):T �������
      L_evix=L_ixix
C 1762-06-0001_log.fgi( 228, 169):������,1762-06-0001_lamp43 
      if(L_ixix) then
         I_ivix=1
      else
         I_ivix=0
      endif
C 1762-06-0001_log.fgi( 212, 165):��������� LO->1
      L_(999)=.false.
C 1762-06-0001_log.fgi( 114, 161):��������� ���������� (�������)
      L_(998)=.false.
C 1762-06-0001_log.fgi( 114, 165):��������� ���������� (�������)
      L0_obox=R0_ebox.ne.R0_abox
      R0_abox=R0_ebox
C 1762-06-0001_log.fgi(  98, 163):���������� ������������� ������
      if(L_(998).or.L_(999)) then
         L_ubox=(L_(998).or.L_ubox).and..not.(L_(999))
      else
         if(L0_obox.and..not.L0_ibox) L_ubox=.not.L_ubox
      endif
      L0_ibox=L0_obox
      L_(1000)=.not.L_ubox
C 1762-06-0001_log.fgi( 120, 163):T �������
      L_oxix=L_ubox
C 1762-06-0001_log.fgi( 150, 169):������,1762-06-0001_lamp42 
      if(L_ubox) then
         I_uxix=1
      else
         I_uxix=0
      endif
C 1762-06-0001_log.fgi( 134, 165):��������� LO->1
      L_(1002)=.false.
C 1762-06-0001_log.fgi(  36, 161):��������� ���������� (�������)
      L_(1001)=.false.
C 1762-06-0001_log.fgi(  36, 165):��������� ���������� (�������)
      L0_afox=R0_odox.ne.R0_idox
      R0_idox=R0_odox
C 1762-06-0001_log.fgi(  20, 163):���������� ������������� ������
      if(L_(1001).or.L_(1002)) then
         L_efox=(L_(1001).or.L_efox).and..not.(L_(1002))
      else
         if(L0_afox.and..not.L0_udox) L_efox=.not.L_efox
      endif
      L0_udox=L0_afox
      L_(1003)=.not.L_efox
C 1762-06-0001_log.fgi(  42, 163):T �������
      L_adox=L_efox
C 1762-06-0001_log.fgi(  72, 169):������,1762-06-0001_lamp41 
      if(L_efox) then
         I_edox=1
      else
         I_edox=0
      endif
C 1762-06-0001_log.fgi(  56, 165):��������� LO->1
      L_(1005)=.false.
C 1762-06-0001_log.fgi( 268, 149):��������� ���������� (�������)
      L_(1004)=.false.
C 1762-06-0001_log.fgi( 268, 153):��������� ���������� (�������)
      L0_ikox=R0_akox.ne.R0_ufox
      R0_ufox=R0_akox
C 1762-06-0001_log.fgi( 252, 151):���������� ������������� ������
      if(L_(1004).or.L_(1005)) then
         L_okox=(L_(1004).or.L_okox).and..not.(L_(1005))
      else
         if(L0_ikox.and..not.L0_ekox) L_okox=.not.L_okox
      endif
      L0_ekox=L0_ikox
      L_(1006)=.not.L_okox
C 1762-06-0001_log.fgi( 274, 151):T �������
      L_ifox=L_okox
C 1762-06-0001_log.fgi( 304, 157):������,1762-06-0001_lamp40 
      if(L_okox) then
         I_ofox=1
      else
         I_ofox=0
      endif
C 1762-06-0001_log.fgi( 288, 153):��������� LO->1
      L_(1008)=.false.
C 1762-06-0001_log.fgi( 192, 149):��������� ���������� (�������)
      L_(1007)=.false.
C 1762-06-0001_log.fgi( 192, 153):��������� ���������� (�������)
      L0_ulox=R0_ilox.ne.R0_elox
      R0_elox=R0_ilox
C 1762-06-0001_log.fgi( 176, 151):���������� ������������� ������
      if(L_(1007).or.L_(1008)) then
         L_amox=(L_(1007).or.L_amox).and..not.(L_(1008))
      else
         if(L0_ulox.and..not.L0_olox) L_amox=.not.L_amox
      endif
      L0_olox=L0_ulox
      L_(1009)=.not.L_amox
C 1762-06-0001_log.fgi( 198, 151):T �������
      L_ukox=L_amox
C 1762-06-0001_log.fgi( 228, 157):������,1762-06-0001_lamp39 
      if(L_amox) then
         I_alox=1
      else
         I_alox=0
      endif
C 1762-06-0001_log.fgi( 212, 153):��������� LO->1
      L_(1011)=.false.
C 1762-06-0001_log.fgi( 114, 149):��������� ���������� (�������)
      L_(1010)=.false.
C 1762-06-0001_log.fgi( 114, 153):��������� ���������� (�������)
      L0_epox=R0_umox.ne.R0_omox
      R0_omox=R0_umox
C 1762-06-0001_log.fgi(  98, 151):���������� ������������� ������
      if(L_(1010).or.L_(1011)) then
         L_ipox=(L_(1010).or.L_ipox).and..not.(L_(1011))
      else
         if(L0_epox.and..not.L0_apox) L_ipox=.not.L_ipox
      endif
      L0_apox=L0_epox
      L_(1012)=.not.L_ipox
C 1762-06-0001_log.fgi( 120, 151):T �������
      L_emox=L_ipox
C 1762-06-0001_log.fgi( 150, 157):������,1762-06-0001_lamp38 
      if(L_ipox) then
         I_imox=1
      else
         I_imox=0
      endif
C 1762-06-0001_log.fgi( 134, 153):��������� LO->1
      L_(1014)=.false.
C 1762-06-0001_log.fgi(  36, 149):��������� ���������� (�������)
      L_(1013)=.false.
C 1762-06-0001_log.fgi(  36, 153):��������� ���������� (�������)
      L0_orox=R0_erox.ne.R0_arox
      R0_arox=R0_erox
C 1762-06-0001_log.fgi(  20, 151):���������� ������������� ������
      if(L_(1013).or.L_(1014)) then
         L_urox=(L_(1013).or.L_urox).and..not.(L_(1014))
      else
         if(L0_orox.and..not.L0_irox) L_urox=.not.L_urox
      endif
      L0_irox=L0_orox
      L_(1015)=.not.L_urox
C 1762-06-0001_log.fgi(  42, 151):T �������
      L_opox=L_urox
C 1762-06-0001_log.fgi(  72, 157):������,1762-06-0001_lamp37 
      if(L_urox) then
         I_upox=1
      else
         I_upox=0
      endif
C 1762-06-0001_log.fgi(  56, 153):��������� LO->1
      L_(1017)=.false.
C 1762-06-0001_log.fgi( 268, 136):��������� ���������� (�������)
      L_(1016)=.false.
C 1762-06-0001_log.fgi( 268, 140):��������� ���������� (�������)
      L0_atox=R0_osox.ne.R0_isox
      R0_isox=R0_osox
C 1762-06-0001_log.fgi( 252, 138):���������� ������������� ������
      if(L_(1016).or.L_(1017)) then
         L_etox=(L_(1016).or.L_etox).and..not.(L_(1017))
      else
         if(L0_atox.and..not.L0_usox) L_etox=.not.L_etox
      endif
      L0_usox=L0_atox
      L_(1018)=.not.L_etox
C 1762-06-0001_log.fgi( 274, 138):T �������
      L_asox=L_etox
C 1762-06-0001_log.fgi( 304, 144):������,1762-06-0001_lamp36 
      if(L_etox) then
         I_esox=1
      else
         I_esox=0
      endif
C 1762-06-0001_log.fgi( 288, 140):��������� LO->1
      L_(1020)=.false.
C 1762-06-0001_log.fgi( 192, 136):��������� ���������� (�������)
      L_(1019)=.false.
C 1762-06-0001_log.fgi( 192, 140):��������� ���������� (�������)
      L0_ivox=R0_avox.ne.R0_utox
      R0_utox=R0_avox
C 1762-06-0001_log.fgi( 176, 138):���������� ������������� ������
      if(L_(1019).or.L_(1020)) then
         L_ovox=(L_(1019).or.L_ovox).and..not.(L_(1020))
      else
         if(L0_ivox.and..not.L0_evox) L_ovox=.not.L_ovox
      endif
      L0_evox=L0_ivox
      L_(1021)=.not.L_ovox
C 1762-06-0001_log.fgi( 198, 138):T �������
      L_itox=L_ovox
C 1762-06-0001_log.fgi( 228, 144):������,1762-06-0001_lamp35 
      if(L_ovox) then
         I_otox=1
      else
         I_otox=0
      endif
C 1762-06-0001_log.fgi( 212, 140):��������� LO->1
      L_(1023)=.false.
C 1762-06-0001_log.fgi( 114, 136):��������� ���������� (�������)
      L_(1022)=.false.
C 1762-06-0001_log.fgi( 114, 140):��������� ���������� (�������)
      L0_uxox=R0_ixox.ne.R0_exox
      R0_exox=R0_ixox
C 1762-06-0001_log.fgi(  98, 138):���������� ������������� ������
      if(L_(1022).or.L_(1023)) then
         L_abux=(L_(1022).or.L_abux).and..not.(L_(1023))
      else
         if(L0_uxox.and..not.L0_oxox) L_abux=.not.L_abux
      endif
      L0_oxox=L0_uxox
      L_(1024)=.not.L_abux
C 1762-06-0001_log.fgi( 120, 138):T �������
      L_uvox=L_abux
C 1762-06-0001_log.fgi( 150, 144):������,1762-06-0001_lamp34 
      if(L_abux) then
         I_axox=1
      else
         I_axox=0
      endif
C 1762-06-0001_log.fgi( 134, 140):��������� LO->1
      L_(1026)=.false.
C 1762-06-0001_log.fgi(  36, 136):��������� ���������� (�������)
      L_(1025)=.false.
C 1762-06-0001_log.fgi(  36, 140):��������� ���������� (�������)
      L0_edux=R0_ubux.ne.R0_obux
      R0_obux=R0_ubux
C 1762-06-0001_log.fgi(  20, 138):���������� ������������� ������
      if(L_(1025).or.L_(1026)) then
         L_idux=(L_(1025).or.L_idux).and..not.(L_(1026))
      else
         if(L0_edux.and..not.L0_adux) L_idux=.not.L_idux
      endif
      L0_adux=L0_edux
      L_(1027)=.not.L_idux
C 1762-06-0001_log.fgi(  42, 138):T �������
      L_ebux=L_idux
C 1762-06-0001_log.fgi(  72, 144):������,1762-06-0001_lamp33 
      if(L_idux) then
         I_ibux=1
      else
         I_ibux=0
      endif
C 1762-06-0001_log.fgi(  56, 140):��������� LO->1
      L_(1029)=.false.
C 1762-06-0001_log.fgi( 268, 123):��������� ���������� (�������)
      L_(1028)=.false.
C 1762-06-0001_log.fgi( 268, 127):��������� ���������� (�������)
      L0_ofux=R0_efux.ne.R0_afux
      R0_afux=R0_efux
C 1762-06-0001_log.fgi( 252, 125):���������� ������������� ������
      if(L_(1028).or.L_(1029)) then
         L_ufux=(L_(1028).or.L_ufux).and..not.(L_(1029))
      else
         if(L0_ofux.and..not.L0_ifux) L_ufux=.not.L_ufux
      endif
      L0_ifux=L0_ofux
      L_(1030)=.not.L_ufux
C 1762-06-0001_log.fgi( 274, 125):T �������
      L_odux=L_ufux
C 1762-06-0001_log.fgi( 304, 131):������,1762-06-0001_lamp32 
      if(L_ufux) then
         I_udux=1
      else
         I_udux=0
      endif
C 1762-06-0001_log.fgi( 288, 127):��������� LO->1
      L_(1032)=.false.
C 1762-06-0001_log.fgi( 192, 123):��������� ���������� (�������)
      L_(1031)=.false.
C 1762-06-0001_log.fgi( 192, 127):��������� ���������� (�������)
      L0_alux=R0_okux.ne.R0_ikux
      R0_ikux=R0_okux
C 1762-06-0001_log.fgi( 176, 125):���������� ������������� ������
      if(L_(1031).or.L_(1032)) then
         L_elux=(L_(1031).or.L_elux).and..not.(L_(1032))
      else
         if(L0_alux.and..not.L0_ukux) L_elux=.not.L_elux
      endif
      L0_ukux=L0_alux
      L_(1033)=.not.L_elux
C 1762-06-0001_log.fgi( 198, 125):T �������
      L_akux=L_elux
C 1762-06-0001_log.fgi( 228, 131):������,1762-06-0001_lamp31 
      if(L_elux) then
         I_ekux=1
      else
         I_ekux=0
      endif
C 1762-06-0001_log.fgi( 212, 127):��������� LO->1
      L_(1035)=.false.
C 1762-06-0001_log.fgi( 114, 123):��������� ���������� (�������)
      L_(1034)=.false.
C 1762-06-0001_log.fgi( 114, 127):��������� ���������� (�������)
      L0_imux=R0_amux.ne.R0_ulux
      R0_ulux=R0_amux
C 1762-06-0001_log.fgi(  98, 125):���������� ������������� ������
      if(L_(1034).or.L_(1035)) then
         L_omux=(L_(1034).or.L_omux).and..not.(L_(1035))
      else
         if(L0_imux.and..not.L0_emux) L_omux=.not.L_omux
      endif
      L0_emux=L0_imux
      L_(1036)=.not.L_omux
C 1762-06-0001_log.fgi( 120, 125):T �������
      L_ilux=L_omux
C 1762-06-0001_log.fgi( 150, 131):������,1762-06-0001_lamp30 
      if(L_omux) then
         I_olux=1
      else
         I_olux=0
      endif
C 1762-06-0001_log.fgi( 134, 127):��������� LO->1
      L_(1038)=.false.
C 1762-06-0001_log.fgi(  36, 123):��������� ���������� (�������)
      L_(1037)=.false.
C 1762-06-0001_log.fgi(  36, 127):��������� ���������� (�������)
      L0_upux=R0_ipux.ne.R0_epux
      R0_epux=R0_ipux
C 1762-06-0001_log.fgi(  20, 125):���������� ������������� ������
      if(L_(1037).or.L_(1038)) then
         L_arux=(L_(1037).or.L_arux).and..not.(L_(1038))
      else
         if(L0_upux.and..not.L0_opux) L_arux=.not.L_arux
      endif
      L0_opux=L0_upux
      L_(1039)=.not.L_arux
C 1762-06-0001_log.fgi(  42, 125):T �������
      L_umux=L_arux
C 1762-06-0001_log.fgi(  72, 131):������,1762-06-0001_lamp29 
      if(L_arux) then
         I_apux=1
      else
         I_apux=0
      endif
C 1762-06-0001_log.fgi(  56, 127):��������� LO->1
      L_(1041)=.false.
C 1762-06-0001_log.fgi( 268, 111):��������� ���������� (�������)
      L_(1040)=.false.
C 1762-06-0001_log.fgi( 268, 115):��������� ���������� (�������)
      L0_esux=R0_urux.ne.R0_orux
      R0_orux=R0_urux
C 1762-06-0001_log.fgi( 252, 113):���������� ������������� ������
      if(L_(1040).or.L_(1041)) then
         L_isux=(L_(1040).or.L_isux).and..not.(L_(1041))
      else
         if(L0_esux.and..not.L0_asux) L_isux=.not.L_isux
      endif
      L0_asux=L0_esux
      L_(1042)=.not.L_isux
C 1762-06-0001_log.fgi( 274, 113):T �������
      L_erux=L_isux
C 1762-06-0001_log.fgi( 304, 119):������,1762-06-0001_lamp28 
      if(L_isux) then
         I_irux=1
      else
         I_irux=0
      endif
C 1762-06-0001_log.fgi( 288, 115):��������� LO->1
      L_(1044)=.false.
C 1762-06-0001_log.fgi( 192, 111):��������� ���������� (�������)
      L_(1043)=.false.
C 1762-06-0001_log.fgi( 192, 115):��������� ���������� (�������)
      L0_otux=R0_etux.ne.R0_atux
      R0_atux=R0_etux
C 1762-06-0001_log.fgi( 176, 113):���������� ������������� ������
      if(L_(1043).or.L_(1044)) then
         L_utux=(L_(1043).or.L_utux).and..not.(L_(1044))
      else
         if(L0_otux.and..not.L0_itux) L_utux=.not.L_utux
      endif
      L0_itux=L0_otux
      L_(1045)=.not.L_utux
C 1762-06-0001_log.fgi( 198, 113):T �������
      L_osux=L_utux
C 1762-06-0001_log.fgi( 228, 119):������,1762-06-0001_lamp27 
      if(L_utux) then
         I_usux=1
      else
         I_usux=0
      endif
C 1762-06-0001_log.fgi( 212, 115):��������� LO->1
      L_(1047)=.false.
C 1762-06-0001_log.fgi( 114, 111):��������� ���������� (�������)
      L_(1046)=.false.
C 1762-06-0001_log.fgi( 114, 115):��������� ���������� (�������)
      L0_axux=R0_ovux.ne.R0_ivux
      R0_ivux=R0_ovux
C 1762-06-0001_log.fgi(  98, 113):���������� ������������� ������
      if(L_(1046).or.L_(1047)) then
         L_exux=(L_(1046).or.L_exux).and..not.(L_(1047))
      else
         if(L0_axux.and..not.L0_uvux) L_exux=.not.L_exux
      endif
      L0_uvux=L0_axux
      L_(1048)=.not.L_exux
C 1762-06-0001_log.fgi( 120, 113):T �������
      L_avux=L_exux
C 1762-06-0001_log.fgi( 150, 119):������,1762-06-0001_lamp26 
      if(L_exux) then
         I_evux=1
      else
         I_evux=0
      endif
C 1762-06-0001_log.fgi( 134, 115):��������� LO->1
      L_(1050)=.false.
C 1762-06-0001_log.fgi(  36, 111):��������� ���������� (�������)
      L_(1049)=.false.
C 1762-06-0001_log.fgi(  36, 115):��������� ���������� (�������)
      L0_ibabe=R0_ababe.ne.R0_uxux
      R0_uxux=R0_ababe
C 1762-06-0001_log.fgi(  20, 113):���������� ������������� ������
      if(L_(1049).or.L_(1050)) then
         L_obabe=(L_(1049).or.L_obabe).and..not.(L_(1050)
     &)
      else
         if(L0_ibabe.and..not.L0_ebabe) L_obabe=.not.L_obabe
      endif
      L0_ebabe=L0_ibabe
      L_(1051)=.not.L_obabe
C 1762-06-0001_log.fgi(  42, 113):T �������
      L_ixux=L_obabe
C 1762-06-0001_log.fgi(  72, 119):������,1762-06-0001_lamp25 
      if(L_obabe) then
         I_oxux=1
      else
         I_oxux=0
      endif
C 1762-06-0001_log.fgi(  56, 115):��������� LO->1
      L_(1053)=.false.
C 1762-06-0001_log.fgi( 268,  98):��������� ���������� (�������)
      L_(1052)=.false.
C 1762-06-0001_log.fgi( 268, 102):��������� ���������� (�������)
      L0_udabe=R0_idabe.ne.R0_edabe
      R0_edabe=R0_idabe
C 1762-06-0001_log.fgi( 252, 100):���������� ������������� ������
      if(L_(1052).or.L_(1053)) then
         L_afabe=(L_(1052).or.L_afabe).and..not.(L_(1053)
     &)
      else
         if(L0_udabe.and..not.L0_odabe) L_afabe=.not.L_afabe
      endif
      L0_odabe=L0_udabe
      L_(1054)=.not.L_afabe
C 1762-06-0001_log.fgi( 274, 100):T �������
      L_ubabe=L_afabe
C 1762-06-0001_log.fgi( 304, 106):������,1762-06-0001_lamp24 
      if(L_afabe) then
         I_adabe=1
      else
         I_adabe=0
      endif
C 1762-06-0001_log.fgi( 288, 102):��������� LO->1
      L_(1056)=.false.
C 1762-06-0001_log.fgi( 192,  98):��������� ���������� (�������)
      L_(1055)=.false.
C 1762-06-0001_log.fgi( 192, 102):��������� ���������� (�������)
      L0_ekabe=R0_ufabe.ne.R0_ofabe
      R0_ofabe=R0_ufabe
C 1762-06-0001_log.fgi( 176, 100):���������� ������������� ������
      if(L_(1055).or.L_(1056)) then
         L_ikabe=(L_(1055).or.L_ikabe).and..not.(L_(1056)
     &)
      else
         if(L0_ekabe.and..not.L0_akabe) L_ikabe=.not.L_ikabe
      endif
      L0_akabe=L0_ekabe
      L_(1057)=.not.L_ikabe
C 1762-06-0001_log.fgi( 198, 100):T �������
      L_efabe=L_ikabe
C 1762-06-0001_log.fgi( 228, 106):������,1762-06-0001_lamp23 
      if(L_ikabe) then
         I_ifabe=1
      else
         I_ifabe=0
      endif
C 1762-06-0001_log.fgi( 212, 102):��������� LO->1
      L_(1059)=.false.
C 1762-06-0001_log.fgi( 114,  98):��������� ���������� (�������)
      L_(1058)=.false.
C 1762-06-0001_log.fgi( 114, 102):��������� ���������� (�������)
      L0_olabe=R0_elabe.ne.R0_alabe
      R0_alabe=R0_elabe
C 1762-06-0001_log.fgi(  98, 100):���������� ������������� ������
      if(L_(1058).or.L_(1059)) then
         L_ulabe=(L_(1058).or.L_ulabe).and..not.(L_(1059)
     &)
      else
         if(L0_olabe.and..not.L0_ilabe) L_ulabe=.not.L_ulabe
      endif
      L0_ilabe=L0_olabe
      L_(1060)=.not.L_ulabe
C 1762-06-0001_log.fgi( 120, 100):T �������
      L_okabe=L_ulabe
C 1762-06-0001_log.fgi( 150, 106):������,1762-06-0001_lamp22 
      if(L_ulabe) then
         I_ukabe=1
      else
         I_ukabe=0
      endif
C 1762-06-0001_log.fgi( 134, 102):��������� LO->1
      L_(1062)=.false.
C 1762-06-0001_log.fgi(  36,  98):��������� ���������� (�������)
      L_(1061)=.false.
C 1762-06-0001_log.fgi(  36, 102):��������� ���������� (�������)
      L0_apabe=R0_omabe.ne.R0_imabe
      R0_imabe=R0_omabe
C 1762-06-0001_log.fgi(  20, 100):���������� ������������� ������
      if(L_(1061).or.L_(1062)) then
         L_epabe=(L_(1061).or.L_epabe).and..not.(L_(1062)
     &)
      else
         if(L0_apabe.and..not.L0_umabe) L_epabe=.not.L_epabe
      endif
      L0_umabe=L0_apabe
      L_(1063)=.not.L_epabe
C 1762-06-0001_log.fgi(  42, 100):T �������
      L_amabe=L_epabe
C 1762-06-0001_log.fgi(  72, 106):������,1762-06-0001_lamp21 
      if(L_epabe) then
         I_emabe=1
      else
         I_emabe=0
      endif
C 1762-06-0001_log.fgi(  56, 102):��������� LO->1
      L_(1065)=.false.
C 1762-06-0001_log.fgi( 268,  86):��������� ���������� (�������)
      L_(1064)=.false.
C 1762-06-0001_log.fgi( 268,  90):��������� ���������� (�������)
      L0_irabe=R0_arabe.ne.R0_upabe
      R0_upabe=R0_arabe
C 1762-06-0001_log.fgi( 252,  88):���������� ������������� ������
      if(L_(1064).or.L_(1065)) then
         L_orabe=(L_(1064).or.L_orabe).and..not.(L_(1065)
     &)
      else
         if(L0_irabe.and..not.L0_erabe) L_orabe=.not.L_orabe
      endif
      L0_erabe=L0_irabe
      L_(1066)=.not.L_orabe
C 1762-06-0001_log.fgi( 274,  88):T �������
      L_ipabe=L_orabe
C 1762-06-0001_log.fgi( 304,  94):������,1762-06-0001_lamp20 
      if(L_orabe) then
         I_opabe=1
      else
         I_opabe=0
      endif
C 1762-06-0001_log.fgi( 288,  90):��������� LO->1
      L_(1068)=.false.
C 1762-06-0001_log.fgi( 192,  86):��������� ���������� (�������)
      L_(1067)=.false.
C 1762-06-0001_log.fgi( 192,  90):��������� ���������� (�������)
      L0_usabe=R0_isabe.ne.R0_esabe
      R0_esabe=R0_isabe
C 1762-06-0001_log.fgi( 176,  88):���������� ������������� ������
      if(L_(1067).or.L_(1068)) then
         L_atabe=(L_(1067).or.L_atabe).and..not.(L_(1068)
     &)
      else
         if(L0_usabe.and..not.L0_osabe) L_atabe=.not.L_atabe
      endif
      L0_osabe=L0_usabe
      L_(1069)=.not.L_atabe
C 1762-06-0001_log.fgi( 198,  88):T �������
      L_urabe=L_atabe
C 1762-06-0001_log.fgi( 228,  94):������,1762-06-0001_lamp19 
      if(L_atabe) then
         I_asabe=1
      else
         I_asabe=0
      endif
C 1762-06-0001_log.fgi( 212,  90):��������� LO->1
      L_(1071)=.false.
C 1762-06-0001_log.fgi( 114,  86):��������� ���������� (�������)
      L_(1070)=.false.
C 1762-06-0001_log.fgi( 114,  90):��������� ���������� (�������)
      L0_evabe=R0_utabe.ne.R0_otabe
      R0_otabe=R0_utabe
C 1762-06-0001_log.fgi(  98,  88):���������� ������������� ������
      if(L_(1070).or.L_(1071)) then
         L_ivabe=(L_(1070).or.L_ivabe).and..not.(L_(1071)
     &)
      else
         if(L0_evabe.and..not.L0_avabe) L_ivabe=.not.L_ivabe
      endif
      L0_avabe=L0_evabe
      L_(1072)=.not.L_ivabe
C 1762-06-0001_log.fgi( 120,  88):T �������
      L_etabe=L_ivabe
C 1762-06-0001_log.fgi( 150,  94):������,1762-06-0001_lamp18 
      if(L_ivabe) then
         I_itabe=1
      else
         I_itabe=0
      endif
C 1762-06-0001_log.fgi( 134,  90):��������� LO->1
      L_(1074)=.false.
C 1762-06-0001_log.fgi(  36,  86):��������� ���������� (�������)
      L_(1073)=.false.
C 1762-06-0001_log.fgi(  36,  90):��������� ���������� (�������)
      L0_oxabe=R0_exabe.ne.R0_axabe
      R0_axabe=R0_exabe
C 1762-06-0001_log.fgi(  20,  88):���������� ������������� ������
      if(L_(1073).or.L_(1074)) then
         L_uxabe=(L_(1073).or.L_uxabe).and..not.(L_(1074)
     &)
      else
         if(L0_oxabe.and..not.L0_ixabe) L_uxabe=.not.L_uxabe
      endif
      L0_ixabe=L0_oxabe
      L_(1075)=.not.L_uxabe
C 1762-06-0001_log.fgi(  42,  88):T �������
      L_ovabe=L_uxabe
C 1762-06-0001_log.fgi(  72,  94):������,1762-06-0001_lamp17 
      if(L_uxabe) then
         I_uvabe=1
      else
         I_uvabe=0
      endif
C 1762-06-0001_log.fgi(  56,  90):��������� LO->1
      L_(1077)=.false.
C 1762-06-0001_log.fgi( 268,  73):��������� ���������� (�������)
      L_(1076)=.false.
C 1762-06-0001_log.fgi( 268,  77):��������� ���������� (�������)
      L0_adebe=R0_obebe.ne.R0_ibebe
      R0_ibebe=R0_obebe
C 1762-06-0001_log.fgi( 252,  75):���������� ������������� ������
      if(L_(1076).or.L_(1077)) then
         L_edebe=(L_(1076).or.L_edebe).and..not.(L_(1077)
     &)
      else
         if(L0_adebe.and..not.L0_ubebe) L_edebe=.not.L_edebe
      endif
      L0_ubebe=L0_adebe
      L_(1078)=.not.L_edebe
C 1762-06-0001_log.fgi( 274,  75):T �������
      L_abebe=L_edebe
C 1762-06-0001_log.fgi( 304,  81):������,1762-06-0001_lamp16 
      if(L_edebe) then
         I_ebebe=1
      else
         I_ebebe=0
      endif
C 1762-06-0001_log.fgi( 288,  77):��������� LO->1
      L_(1080)=.false.
C 1762-06-0001_log.fgi( 192,  73):��������� ���������� (�������)
      L_(1079)=.false.
C 1762-06-0001_log.fgi( 192,  77):��������� ���������� (�������)
      L0_ifebe=R0_afebe.ne.R0_udebe
      R0_udebe=R0_afebe
C 1762-06-0001_log.fgi( 176,  75):���������� ������������� ������
      if(L_(1079).or.L_(1080)) then
         L_ofebe=(L_(1079).or.L_ofebe).and..not.(L_(1080)
     &)
      else
         if(L0_ifebe.and..not.L0_efebe) L_ofebe=.not.L_ofebe
      endif
      L0_efebe=L0_ifebe
      L_(1081)=.not.L_ofebe
C 1762-06-0001_log.fgi( 198,  75):T �������
      L_idebe=L_ofebe
C 1762-06-0001_log.fgi( 228,  81):������,1762-06-0001_lamp15 
      if(L_ofebe) then
         I_odebe=1
      else
         I_odebe=0
      endif
C 1762-06-0001_log.fgi( 212,  77):��������� LO->1
      L_(1083)=.false.
C 1762-06-0001_log.fgi( 114,  73):��������� ���������� (�������)
      L_(1082)=.false.
C 1762-06-0001_log.fgi( 114,  77):��������� ���������� (�������)
      L0_ukebe=R0_ikebe.ne.R0_ekebe
      R0_ekebe=R0_ikebe
C 1762-06-0001_log.fgi(  98,  75):���������� ������������� ������
      if(L_(1082).or.L_(1083)) then
         L_alebe=(L_(1082).or.L_alebe).and..not.(L_(1083)
     &)
      else
         if(L0_ukebe.and..not.L0_okebe) L_alebe=.not.L_alebe
      endif
      L0_okebe=L0_ukebe
      L_(1084)=.not.L_alebe
C 1762-06-0001_log.fgi( 120,  75):T �������
      L_ufebe=L_alebe
C 1762-06-0001_log.fgi( 150,  81):������,1762-06-0001_lamp14 
      if(L_alebe) then
         I_akebe=1
      else
         I_akebe=0
      endif
C 1762-06-0001_log.fgi( 134,  77):��������� LO->1
      L_(1086)=.false.
C 1762-06-0001_log.fgi(  36,  73):��������� ���������� (�������)
      L_(1085)=.false.
C 1762-06-0001_log.fgi(  36,  77):��������� ���������� (�������)
      L0_emebe=R0_ulebe.ne.R0_olebe
      R0_olebe=R0_ulebe
C 1762-06-0001_log.fgi(  20,  75):���������� ������������� ������
      if(L_(1085).or.L_(1086)) then
         L_imebe=(L_(1085).or.L_imebe).and..not.(L_(1086)
     &)
      else
         if(L0_emebe.and..not.L0_amebe) L_imebe=.not.L_imebe
      endif
      L0_amebe=L0_emebe
      L_(1087)=.not.L_imebe
C 1762-06-0001_log.fgi(  42,  75):T �������
      L_elebe=L_imebe
C 1762-06-0001_log.fgi(  72,  81):������,1762-06-0001_lamp13 
      if(L_imebe) then
         I_ilebe=1
      else
         I_ilebe=0
      endif
C 1762-06-0001_log.fgi(  56,  77):��������� LO->1
      L_(1089)=.false.
C 1762-06-0001_log.fgi( 268,  60):��������� ���������� (�������)
      L_(1088)=.false.
C 1762-06-0001_log.fgi( 268,  64):��������� ���������� (�������)
      L0_opebe=R0_epebe.ne.R0_apebe
      R0_apebe=R0_epebe
C 1762-06-0001_log.fgi( 252,  62):���������� ������������� ������
      if(L_(1088).or.L_(1089)) then
         L_upebe=(L_(1088).or.L_upebe).and..not.(L_(1089)
     &)
      else
         if(L0_opebe.and..not.L0_ipebe) L_upebe=.not.L_upebe
      endif
      L0_ipebe=L0_opebe
      L_(1090)=.not.L_upebe
C 1762-06-0001_log.fgi( 274,  62):T �������
      L_omebe=L_upebe
C 1762-06-0001_log.fgi( 304,  68):������,1762-06-0001_lamp12 
      if(L_upebe) then
         I_umebe=1
      else
         I_umebe=0
      endif
C 1762-06-0001_log.fgi( 288,  64):��������� LO->1
      L_(1092)=.false.
C 1762-06-0001_log.fgi( 192,  60):��������� ���������� (�������)
      L_(1091)=.false.
C 1762-06-0001_log.fgi( 192,  64):��������� ���������� (�������)
      L0_asebe=R0_orebe.ne.R0_irebe
      R0_irebe=R0_orebe
C 1762-06-0001_log.fgi( 176,  62):���������� ������������� ������
      if(L_(1091).or.L_(1092)) then
         L_esebe=(L_(1091).or.L_esebe).and..not.(L_(1092)
     &)
      else
         if(L0_asebe.and..not.L0_urebe) L_esebe=.not.L_esebe
      endif
      L0_urebe=L0_asebe
      L_(1093)=.not.L_esebe
C 1762-06-0001_log.fgi( 198,  62):T �������
      L_arebe=L_esebe
C 1762-06-0001_log.fgi( 228,  68):������,1762-06-0001_lamp11 
      if(L_esebe) then
         I_erebe=1
      else
         I_erebe=0
      endif
C 1762-06-0001_log.fgi( 212,  64):��������� LO->1
      L_(1095)=.false.
C 1762-06-0001_log.fgi( 114,  60):��������� ���������� (�������)
      L_(1094)=.false.
C 1762-06-0001_log.fgi( 114,  64):��������� ���������� (�������)
      L0_itebe=R0_atebe.ne.R0_usebe
      R0_usebe=R0_atebe
C 1762-06-0001_log.fgi(  98,  62):���������� ������������� ������
      if(L_(1094).or.L_(1095)) then
         L_otebe=(L_(1094).or.L_otebe).and..not.(L_(1095)
     &)
      else
         if(L0_itebe.and..not.L0_etebe) L_otebe=.not.L_otebe
      endif
      L0_etebe=L0_itebe
      L_(1096)=.not.L_otebe
C 1762-06-0001_log.fgi( 120,  62):T �������
      L_isebe=L_otebe
C 1762-06-0001_log.fgi( 150,  68):������,1762-06-0001_lamp10 
      if(L_otebe) then
         I_osebe=1
      else
         I_osebe=0
      endif
C 1762-06-0001_log.fgi( 134,  64):��������� LO->1
      L_(1098)=.false.
C 1762-06-0001_log.fgi(  36,  60):��������� ���������� (�������)
      L_(1097)=.false.
C 1762-06-0001_log.fgi(  36,  64):��������� ���������� (�������)
      L0_uvebe=R0_ivebe.ne.R0_evebe
      R0_evebe=R0_ivebe
C 1762-06-0001_log.fgi(  20,  62):���������� ������������� ������
      if(L_(1097).or.L_(1098)) then
         L_axebe=(L_(1097).or.L_axebe).and..not.(L_(1098)
     &)
      else
         if(L0_uvebe.and..not.L0_ovebe) L_axebe=.not.L_axebe
      endif
      L0_ovebe=L0_uvebe
      L_(1099)=.not.L_axebe
C 1762-06-0001_log.fgi(  42,  62):T �������
      L_utebe=L_axebe
C 1762-06-0001_log.fgi(  72,  68):������,1762-06-0001_lamp09 
      if(L_axebe) then
         I_avebe=1
      else
         I_avebe=0
      endif
C 1762-06-0001_log.fgi(  56,  64):��������� LO->1
      L_(1101)=.false.
C 1762-06-0001_log.fgi( 268,  48):��������� ���������� (�������)
      L_(1100)=.false.
C 1762-06-0001_log.fgi( 268,  52):��������� ���������� (�������)
      L0_ebibe=R0_uxebe.ne.R0_oxebe
      R0_oxebe=R0_uxebe
C 1762-06-0001_log.fgi( 252,  50):���������� ������������� ������
      if(L_(1100).or.L_(1101)) then
         L_ibibe=(L_(1100).or.L_ibibe).and..not.(L_(1101)
     &)
      else
         if(L0_ebibe.and..not.L0_abibe) L_ibibe=.not.L_ibibe
      endif
      L0_abibe=L0_ebibe
      L_(1102)=.not.L_ibibe
C 1762-06-0001_log.fgi( 274,  50):T �������
      L_exebe=L_ibibe
C 1762-06-0001_log.fgi( 304,  56):������,1762-06-0001_lamp08 
      if(L_ibibe) then
         I_ixebe=1
      else
         I_ixebe=0
      endif
C 1762-06-0001_log.fgi( 288,  52):��������� LO->1
      L_(1104)=.false.
C 1762-06-0001_log.fgi( 192,  48):��������� ���������� (�������)
      L_(1103)=.false.
C 1762-06-0001_log.fgi( 192,  52):��������� ���������� (�������)
      L0_odibe=R0_edibe.ne.R0_adibe
      R0_adibe=R0_edibe
C 1762-06-0001_log.fgi( 176,  50):���������� ������������� ������
      if(L_(1103).or.L_(1104)) then
         L_udibe=(L_(1103).or.L_udibe).and..not.(L_(1104)
     &)
      else
         if(L0_odibe.and..not.L0_idibe) L_udibe=.not.L_udibe
      endif
      L0_idibe=L0_odibe
      L_(1105)=.not.L_udibe
C 1762-06-0001_log.fgi( 198,  50):T �������
      L_obibe=L_udibe
C 1762-06-0001_log.fgi( 228,  56):������,1762-06-0001_lamp07 
      if(L_udibe) then
         I_ubibe=1
      else
         I_ubibe=0
      endif
C 1762-06-0001_log.fgi( 212,  52):��������� LO->1
      L_(1107)=.false.
C 1762-06-0001_log.fgi( 114,  48):��������� ���������� (�������)
      L_(1106)=.false.
C 1762-06-0001_log.fgi( 114,  52):��������� ���������� (�������)
      L0_akibe=R0_ofibe.ne.R0_ifibe
      R0_ifibe=R0_ofibe
C 1762-06-0001_log.fgi(  98,  50):���������� ������������� ������
      if(L_(1106).or.L_(1107)) then
         L_ekibe=(L_(1106).or.L_ekibe).and..not.(L_(1107)
     &)
      else
         if(L0_akibe.and..not.L0_ufibe) L_ekibe=.not.L_ekibe
      endif
      L0_ufibe=L0_akibe
      L_(1108)=.not.L_ekibe
C 1762-06-0001_log.fgi( 120,  50):T �������
      L_afibe=L_ekibe
C 1762-06-0001_log.fgi( 150,  56):������,1762-06-0001_lamp06 
      if(L_ekibe) then
         I_efibe=1
      else
         I_efibe=0
      endif
C 1762-06-0001_log.fgi( 134,  52):��������� LO->1
      L_(1110)=.false.
C 1762-06-0001_log.fgi(  36,  48):��������� ���������� (�������)
      L_(1109)=.false.
C 1762-06-0001_log.fgi(  36,  52):��������� ���������� (�������)
      L0_ilibe=R0_alibe.ne.R0_ukibe
      R0_ukibe=R0_alibe
C 1762-06-0001_log.fgi(  20,  50):���������� ������������� ������
      if(L_(1109).or.L_(1110)) then
         L_olibe=(L_(1109).or.L_olibe).and..not.(L_(1110)
     &)
      else
         if(L0_ilibe.and..not.L0_elibe) L_olibe=.not.L_olibe
      endif
      L0_elibe=L0_ilibe
      L_(1111)=.not.L_olibe
C 1762-06-0001_log.fgi(  42,  50):T �������
      L_ikibe=L_olibe
C 1762-06-0001_log.fgi(  72,  56):������,1762-06-0001_lamp05 
      if(L_olibe) then
         I_okibe=1
      else
         I_okibe=0
      endif
C 1762-06-0001_log.fgi(  56,  52):��������� LO->1
      L_(1113)=.false.
C 1762-06-0001_log.fgi( 268,  35):��������� ���������� (�������)
      L_(1112)=.false.
C 1762-06-0001_log.fgi( 268,  39):��������� ���������� (�������)
      L0_umibe=R0_imibe.ne.R0_emibe
      R0_emibe=R0_imibe
C 1762-06-0001_log.fgi( 252,  37):���������� ������������� ������
      if(L_(1112).or.L_(1113)) then
         L_apibe=(L_(1112).or.L_apibe).and..not.(L_(1113)
     &)
      else
         if(L0_umibe.and..not.L0_omibe) L_apibe=.not.L_apibe
      endif
      L0_omibe=L0_umibe
      L_(1114)=.not.L_apibe
C 1762-06-0001_log.fgi( 274,  37):T �������
      L_ulibe=L_apibe
C 1762-06-0001_log.fgi( 304,  43):������,1762-06-0001_lamp04 
      if(L_apibe) then
         I_amibe=1
      else
         I_amibe=0
      endif
C 1762-06-0001_log.fgi( 288,  39):��������� LO->1
      L_(1116)=.false.
C 1762-06-0001_log.fgi( 192,  35):��������� ���������� (�������)
      L_(1115)=.false.
C 1762-06-0001_log.fgi( 192,  39):��������� ���������� (�������)
      L0_eribe=R0_upibe.ne.R0_opibe
      R0_opibe=R0_upibe
C 1762-06-0001_log.fgi( 176,  37):���������� ������������� ������
      if(L_(1115).or.L_(1116)) then
         L_iribe=(L_(1115).or.L_iribe).and..not.(L_(1116)
     &)
      else
         if(L0_eribe.and..not.L0_aribe) L_iribe=.not.L_iribe
      endif
      L0_aribe=L0_eribe
      L_(1117)=.not.L_iribe
C 1762-06-0001_log.fgi( 198,  37):T �������
      L_epibe=L_iribe
C 1762-06-0001_log.fgi( 228,  43):������,1762-06-0001_lamp03 
      if(L_iribe) then
         I_ipibe=1
      else
         I_ipibe=0
      endif
C 1762-06-0001_log.fgi( 212,  39):��������� LO->1
      L_(1119)=.false.
C 1762-06-0001_log.fgi( 114,  35):��������� ���������� (�������)
      L_(1118)=.false.
C 1762-06-0001_log.fgi( 114,  39):��������� ���������� (�������)
      L0_osibe=R0_esibe.ne.R0_asibe
      R0_asibe=R0_esibe
C 1762-06-0001_log.fgi(  98,  37):���������� ������������� ������
      if(L_(1118).or.L_(1119)) then
         L_usibe=(L_(1118).or.L_usibe).and..not.(L_(1119)
     &)
      else
         if(L0_osibe.and..not.L0_isibe) L_usibe=.not.L_usibe
      endif
      L0_isibe=L0_osibe
      L_(1120)=.not.L_usibe
C 1762-06-0001_log.fgi( 120,  37):T �������
      L_oribe=L_usibe
C 1762-06-0001_log.fgi( 150,  43):������,1762-06-0001_lamp02 
      if(L_usibe) then
         I_uribe=1
      else
         I_uribe=0
      endif
C 1762-06-0001_log.fgi( 134,  39):��������� LO->1
      L_(1122)=.false.
C 1762-06-0001_log.fgi(  36,  35):��������� ���������� (�������)
      L_(1121)=.false.
C 1762-06-0001_log.fgi(  36,  39):��������� ���������� (�������)
      L0_avibe=R0_otibe.ne.R0_itibe
      R0_itibe=R0_otibe
C 1762-06-0001_log.fgi(  20,  37):���������� ������������� ������
      if(L_(1121).or.L_(1122)) then
         L_evibe=(L_(1121).or.L_evibe).and..not.(L_(1122)
     &)
      else
         if(L0_avibe.and..not.L0_utibe) L_evibe=.not.L_evibe
      endif
      L0_utibe=L0_avibe
      L_(1123)=.not.L_evibe
C 1762-06-0001_log.fgi(  42,  37):T �������
      L_atibe=L_evibe
C 1762-06-0001_log.fgi(  72,  43):������,1762-06-0001_lamp01 
      if(L_evibe) then
         I_etibe=1
      else
         I_etibe=0
      endif
C 1762-06-0001_log.fgi(  56,  39):��������� LO->1
      L_(1125)=.false.
C 1762-06-0001_log.fgi( 330,  32):��������� ���������� (�������)
      if(L_(1125)) then
         I0_ivibe=0
      endif
      I_ovibe=I0_ivibe
      L_(1124)=I0_ivibe.ne.0
C 1762-06-0001_log.fgi( 338,  34):���������� ��������� 
      R_obobe = R8_uvibe
C FDC_logic.fgi( 196, 211):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_efobe,4),REAL
     &(R_obobe,4),R_ixibe,
     & R_ifobe,REAL(1,4),REAL(R_abobe,4),
     & REAL(R_ebobe,4),REAL(R_exibe,4),
     & REAL(R_axibe,4),I_afobe,REAL(R_ubobe,4),L_adobe,
     & REAL(R_edobe,4),L_idobe,L_odobe,R_ibobe,REAL(R_uxibe
     &,4),REAL(R_oxibe,4),L_udobe)
      !}
C FDC_logic.fgi( 192, 221):���������� ������� ��������,20FDC34CP001XQ01
      R_olobe = R8_ufobe
C FDC_logic.fgi( 196, 232):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_epobe,4),REAL
     &(R_olobe,4),R_ikobe,
     & R_ipobe,REAL(1,4),REAL(R_alobe,4),
     & REAL(R_elobe,4),REAL(R_ekobe,4),
     & REAL(R_akobe,4),I_apobe,REAL(R_ulobe,4),L_amobe,
     & REAL(R_emobe,4),L_imobe,L_omobe,R_ilobe,REAL(R_ukobe
     &,4),REAL(R_okobe,4),L_umobe)
      !}
C FDC_logic.fgi( 192, 242):���������� ������� ��������,20FDC33CP001XQ01
      R_osobe = R8_upobe
C FDC_logic.fgi( 196, 252):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_evobe,4),REAL
     &(R_osobe,4),R_irobe,
     & R_ivobe,REAL(1,4),REAL(R_asobe,4),
     & REAL(R_esobe,4),REAL(R_erobe,4),
     & REAL(R_arobe,4),I_avobe,REAL(R_usobe,4),L_atobe,
     & REAL(R_etobe,4),L_itobe,L_otobe,R_isobe,REAL(R_urobe
     &,4),REAL(R_orobe,4),L_utobe)
      !}
C FDC_logic.fgi( 192, 262):���������� ������� ��������,20FDC32CP001XQ01
      R_obube = R8_uvobe
C FDC_logic.fgi( 196, 272):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_efube,4),REAL
     &(R_obube,4),R_ixobe,
     & R_ifube,REAL(1,4),REAL(R_abube,4),
     & REAL(R_ebube,4),REAL(R_exobe,4),
     & REAL(R_axobe,4),I_afube,REAL(R_ubube,4),L_adube,
     & REAL(R_edube,4),L_idube,L_odube,R_ibube,REAL(R_uxobe
     &,4),REAL(R_oxobe,4),L_udube)
      !}
C FDC_logic.fgi( 192, 282):���������� ������� ��������,20FDC31CP001XQ01
      !{Call KN_VLVR(deltat,REAL(R_upube,4),L_evube,L_ivube
C ,R8_ulube,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_upube,4),L_evube
     &,L_ivube,R8_ulube,C30_epube,
     & L_idofe,L_ifofe,L_arofe,I_etube,I_itube,R_ufube,
     & REAL(R_ekube,4),R_alube,REAL(R_ilube,4),
     & R_ikube,REAL(R_ukube,4),I_isube,I_otube,I_atube,I_usube
     &,
     & L_elube,L_okube,L_omube,L_imube,L_ovube,
     & L_akube,L_apube,L_uxube,L_emube,L_amube,L_oxube,L_abade
     &,
     & L_olube,L_utube,L_exube,L_avube,L_ipube,L_opube,
     & REAL(R8_evife,8),REAL(1.0,4),R8_efofe,L_ixube,R_esube
     &,
     & REAL(R_umube,4),L_arube,L_erube,L_irube,L_urube,L_asube
     &,L_orube)
      !}

      if(L_asube.or.L_urube.or.L_orube.or.L_irube.or.L_erube.or.L_arube
     &) then      
                  I_osube = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_osube = z'40000000'
      endif
C FDC_logic.fgi(  61, 279):���� ���������� ���������������� ��������,20FDC_K52
      !{Call KN_VLVR(deltat,REAL(R_elade,4),L_orade,L_urade
C ,R8_efade,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_elade,4),L_orade
     &,L_urade,R8_efade,C30_okade,
     & L_idofe,L_ifofe,L_arofe,I_opade,I_upade,R_ebade,
     & REAL(R_obade,4),R_idade,REAL(R_udade,4),
     & R_ubade,REAL(R_edade,4),I_umade,I_arade,I_ipade,I_epade
     &,
     & L_odade,L_adade,L_akade,L_ufade,L_asade,
     & L_ibade,L_ikade,L_etade,L_ofade,L_ifade,L_atade,L_itade
     &,
     & L_afade,L_erade,L_osade,L_irade,L_ukade,L_alade,
     & REAL(R8_evife,8),REAL(1.0,4),R8_efofe,L_usade,R_omade
     &,
     & REAL(R_ekade,4),L_ilade,L_olade,L_ulade,L_emade,L_imade
     &,L_amade)
      !}

      if(L_imade.or.L_emade.or.L_amade.or.L_ulade.or.L_olade.or.L_ilade
     &) then      
                  I_apade = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_apade = z'40000000'
      endif
C FDC_logic.fgi(  41, 279):���� ���������� ���������������� ��������,20FDC_K334
      !{Call KN_VLVR(deltat,REAL(R_odede,4),L_amede,L_emede
C ,R8_oxade,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_odede,4),L_amede
     &,L_emede,R8_oxade,C30_adede,
     & L_idofe,L_ifofe,L_arofe,I_alede,I_elede,R_otade,
     & REAL(R_avade,4),R_uvade,REAL(R_exade,4),
     & R_evade,REAL(R_ovade,4),I_ekede,I_ilede,I_ukede,I_okede
     &,
     & L_axade,L_ivade,L_ibede,L_ebede,L_imede,
     & L_utade,L_ubede,L_opede,L_abede,L_uxade,L_ipede,L_upede
     &,
     & L_ixade,L_olede,L_apede,L_ulede,L_edede,L_idede,
     & REAL(R8_evife,8),REAL(1.0,4),R8_efofe,L_epede,R_akede
     &,
     & REAL(R_obede,4),L_udede,L_afede,L_efede,L_ofede,L_ufede
     &,L_ifede)
      !}

      if(L_ufede.or.L_ofede.or.L_ifede.or.L_efede.or.L_afede.or.L_udede
     &) then      
                  I_ikede = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ikede = z'40000000'
      endif
C FDC_logic.fgi(  21, 279):���� ���������� ���������������� ��������,20FDC_K333
      !{Call KN_VLVR(deltat,REAL(R_axede,4),L_ifide,L_ofide
C ,R8_atede,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_axede,4),L_ifide
     &,L_ofide,R8_atede,C30_ivede,
     & L_idofe,L_ifofe,L_arofe,I_idide,I_odide,R_arede,
     & REAL(R_irede,4),R_esede,REAL(R_osede,4),
     & R_orede,REAL(R_asede,4),I_obide,I_udide,I_edide,I_adide
     &,
     & L_isede,L_urede,L_utede,L_otede,L_ufide,
     & L_erede,L_evede,L_alide,L_itede,L_etede,L_ukide,L_elide
     &,
     & L_usede,L_afide,L_ikide,L_efide,L_ovede,L_uvede,
     & REAL(R8_evife,8),REAL(1.0,4),R8_efofe,L_okide,R_ibide
     &,
     & REAL(R_avede,4),L_exede,L_ixede,L_oxede,L_abide,L_ebide
     &,L_uxede)
      !}

      if(L_ebide.or.L_abide.or.L_uxede.or.L_oxede.or.L_ixede.or.L_exede
     &) then      
                  I_ubide = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ubide = z'40000000'
      endif
C FDC_logic.fgi( 101, 279):���� ���������� ���������������� ��������,20FDC_K234
      !{Call KN_VLVR(deltat,REAL(R_iside,4),L_uxide,L_abode
C ,R8_ipide,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_iside,4),L_uxide
     &,L_abode,R8_ipide,C30_uride,
     & L_idofe,L_ifofe,L_arofe,I_uvide,I_axide,R_ilide,
     & REAL(R_ulide,4),R_omide,REAL(R_apide,4),
     & R_amide,REAL(R_imide,4),I_avide,I_exide,I_ovide,I_ivide
     &,
     & L_umide,L_emide,L_eride,L_aride,L_ebode,
     & L_olide,L_oride,L_idode,L_upide,L_opide,L_edode,L_odode
     &,
     & L_epide,L_ixide,L_ubode,L_oxide,L_aside,L_eside,
     & REAL(R8_evife,8),REAL(1.0,4),R8_efofe,L_adode,R_utide
     &,
     & REAL(R_iride,4),L_oside,L_uside,L_atide,L_itide,L_otide
     &,L_etide)
      !}

      if(L_otide.or.L_itide.or.L_etide.or.L_atide.or.L_uside.or.L_oside
     &) then      
                  I_evide = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_evide = z'40000000'
      endif
C FDC_logic.fgi(  81, 279):���� ���������� ���������������� ��������,20FDC_K233
      !{Call KN_VLVR(deltat,REAL(R_umode,4),L_etode,L_itode
C ,R8_ukode,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_umode,4),L_etode
     &,L_itode,R8_ukode,C30_emode,
     & L_idofe,L_ifofe,L_arofe,I_esode,I_isode,R_udode,
     & REAL(R_efode,4),R_akode,REAL(R_ikode,4),
     & R_ifode,REAL(R_ufode,4),I_irode,I_osode,I_asode,I_urode
     &,
     & L_ekode,L_ofode,L_olode,L_ilode,L_otode,
     & L_afode,L_amode,L_uvode,L_elode,L_alode,L_ovode,L_axode
     &,
     & L_okode,L_usode,L_evode,L_atode,L_imode,L_omode,
     & REAL(R8_evife,8),REAL(1.0,4),R8_efofe,L_ivode,R_erode
     &,
     & REAL(R_ulode,4),L_apode,L_epode,L_ipode,L_upode,L_arode
     &,L_opode)
      !}

      if(L_arode.or.L_upode.or.L_opode.or.L_ipode.or.L_epode.or.L_apode
     &) then      
                  I_orode = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_orode = z'40000000'
      endif
C FDC_logic.fgi(  41, 254):���� ���������� ���������������� ��������,20FDC_K428
      !{Call KN_VLVR(deltat,REAL(R_ekude,4),L_opude,L_upude
C ,R8_edude,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_ekude,4),L_opude
     &,L_upude,R8_edude,C30_ofude,
     & L_idofe,L_ifofe,L_arofe,I_omude,I_umude,R_exode,
     & REAL(R_oxode,4),R_ibude,REAL(R_ubude,4),
     & R_uxode,REAL(R_ebude,4),I_ulude,I_apude,I_imude,I_emude
     &,
     & L_obude,L_abude,L_afude,L_udude,L_arude,
     & L_ixode,L_ifude,L_esude,L_odude,L_idude,L_asude,L_isude
     &,
     & L_adude,L_epude,L_orude,L_ipude,L_ufude,L_akude,
     & REAL(R8_evife,8),REAL(1.0,4),R8_efofe,L_urude,R_olude
     &,
     & REAL(R_efude,4),L_ikude,L_okude,L_ukude,L_elude,L_ilude
     &,L_alude)
      !}

      if(L_ilude.or.L_elude.or.L_alude.or.L_ukude.or.L_okude.or.L_ikude
     &) then      
                  I_amude = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_amude = z'40000000'
      endif
C FDC_logic.fgi(  21, 254):���� ���������� ���������������� ��������,20FDC_K434
      !{Call KN_VLVR(deltat,REAL(R_obafe,4),L_alafe,L_elafe
C ,R8_ovude,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_obafe,4),L_alafe
     &,L_elafe,R8_ovude,C30_abafe,
     & L_idofe,L_ifofe,L_arofe,I_akafe,I_ekafe,R_osude,
     & REAL(R_atude,4),R_utude,REAL(R_evude,4),
     & R_etude,REAL(R_otude,4),I_efafe,I_ikafe,I_ufafe,I_ofafe
     &,
     & L_avude,L_itude,L_ixude,L_exude,L_ilafe,
     & L_usude,L_uxude,L_omafe,L_axude,L_uvude,L_imafe,L_umafe
     &,
     & L_ivude,L_okafe,L_amafe,L_ukafe,L_ebafe,L_ibafe,
     & REAL(R8_evife,8),REAL(1.0,4),R8_efofe,L_emafe,R_afafe
     &,
     & REAL(R_oxude,4),L_ubafe,L_adafe,L_edafe,L_odafe,L_udafe
     &,L_idafe)
      !}

      if(L_udafe.or.L_odafe.or.L_idafe.or.L_edafe.or.L_adafe.or.L_ubafe
     &) then      
                  I_ifafe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ifafe = z'40000000'
      endif
C FDC_logic.fgi( 141, 254):���� ���������� ���������������� ��������,20FDC_K430
      !{Call KN_VLVR(deltat,REAL(R_avafe,4),L_idefe,L_odefe
C ,R8_asafe,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_avafe,4),L_idefe
     &,L_odefe,R8_asafe,C30_itafe,
     & L_idofe,L_ifofe,L_arofe,I_ibefe,I_obefe,R_apafe,
     & REAL(R_ipafe,4),R_erafe,REAL(R_orafe,4),
     & R_opafe,REAL(R_arafe,4),I_oxafe,I_ubefe,I_ebefe,I_abefe
     &,
     & L_irafe,L_upafe,L_usafe,L_osafe,L_udefe,
     & L_epafe,L_etafe,L_akefe,L_isafe,L_esafe,L_ufefe,L_ekefe
     &,
     & L_urafe,L_adefe,L_ifefe,L_edefe,L_otafe,L_utafe,
     & REAL(R8_evife,8),REAL(1.0,4),R8_efofe,L_ofefe,R_ixafe
     &,
     & REAL(R_atafe,4),L_evafe,L_ivafe,L_ovafe,L_axafe,L_exafe
     &,L_uvafe)
      !}

      if(L_exafe.or.L_axafe.or.L_uvafe.or.L_ovafe.or.L_ivafe.or.L_evafe
     &) then      
                  I_uxafe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_uxafe = z'40000000'
      endif
C FDC_logic.fgi( 121, 254):���� ���������� ���������������� ��������,20FDC_K429
      !{Call KN_VLVR(deltat,REAL(R_irefe,4),L_uvefe,L_axefe
C ,R8_imefe,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_irefe,4),L_uvefe
     &,L_axefe,R8_imefe,C30_upefe,
     & L_idofe,L_ifofe,L_arofe,I_utefe,I_avefe,R_ikefe,
     & REAL(R_ukefe,4),R_olefe,REAL(R_amefe,4),
     & R_alefe,REAL(R_ilefe,4),I_atefe,I_evefe,I_otefe,I_itefe
     &,
     & L_ulefe,L_elefe,L_epefe,L_apefe,L_exefe,
     & L_okefe,L_opefe,L_ibife,L_umefe,L_omefe,L_ebife,L_obife
     &,
     & L_emefe,L_ivefe,L_uxefe,L_ovefe,L_arefe,L_erefe,
     & REAL(R8_evife,8),REAL(1.0,4),R8_efofe,L_abife,R_usefe
     &,
     & REAL(R_ipefe,4),L_orefe,L_urefe,L_asefe,L_isefe,L_osefe
     &,L_esefe)
      !}

      if(L_osefe.or.L_isefe.or.L_esefe.or.L_asefe.or.L_urefe.or.L_orefe
     &) then      
                  I_etefe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_etefe = z'40000000'
      endif
C FDC_logic.fgi( 101, 254):���� ���������� ���������������� ��������,20FDC_K433
      !{Call KN_VLVR(deltat,REAL(R_ulife,4),L_esife,L_isife
C ,R8_ufife,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_ulife,4),L_esife
     &,L_isife,R8_ufife,C30_elife,
     & L_idofe,L_ifofe,L_arofe,I_erife,I_irife,R_ubife,
     & REAL(R_edife,4),R_afife,REAL(R_ifife,4),
     & R_idife,REAL(R_udife,4),I_ipife,I_orife,I_arife,I_upife
     &,
     & L_efife,L_odife,L_okife,L_ikife,L_osife,
     & L_adife,L_alife,L_utife,L_ekife,L_akife,L_otife,L_avife
     &,
     & L_ofife,L_urife,L_etife,L_asife,L_ilife,L_olife,
     & REAL(R8_evife,8),REAL(1.0,4),R8_efofe,L_itife,R_epife
     &,
     & REAL(R_ukife,4),L_amife,L_emife,L_imife,L_umife,L_apife
     &,L_omife)
      !}

      if(L_apife.or.L_umife.or.L_omife.or.L_imife.or.L_emife.or.L_amife
     &) then      
                  I_opife = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_opife = z'40000000'
      endif
C FDC_logic.fgi(  81, 254):���� ���������� ���������������� ��������,20FDC_K432
      !{Call KN_VLVR(deltat,REAL(R_akofe,4),L_ipofe,L_opofe
C ,R8_ibofe,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_akofe,4),L_ipofe
     &,L_opofe,R8_ibofe,C30_afofe,
     & L_idofe,L_ifofe,L_arofe,I_imofe,I_omofe,R_ivife,
     & REAL(R_uvife,4),R_oxife,REAL(R_abofe,4),
     & R_axife,REAL(R_ixife,4),I_olofe,I_umofe,I_emofe,I_amofe
     &,
     & L_uxife,L_exife,L_edofe,L_adofe,L_upofe,
     & L_ovife,L_udofe,L_esofe,L_ubofe,L_obofe,L_asofe,L_isofe
     &,
     & L_ebofe,L_apofe,L_orofe,L_epofe,L_ofofe,L_ufofe,
     & REAL(R8_evife,8),REAL(1.0,4),R8_efofe,L_urofe,R_ilofe
     &,
     & REAL(R_odofe,4),L_ekofe,L_ikofe,L_okofe,L_alofe,L_elofe
     &,L_ukofe)
      !}

      if(L_elofe.or.L_alofe.or.L_ukofe.or.L_okofe.or.L_ikofe.or.L_ekofe
     &) then      
                  I_ulofe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ulofe = z'40000000'
      endif
C FDC_logic.fgi(  61, 254):���� ���������� ���������������� ��������,20FDC_K431
      if(L_(35).or.L0_efe) then
         L0_efe=(L_(35).or.L0_efe).and..not.(L0_efe)
      else
         if(L_(37).and..not.L0_afe) L0_efe=.not.L0_efe
      endif
      L0_afe=L_(37)
      L_(36)=.not.L0_efe
C jgt_log.fgi(  86, 124):T �������
C label 4666  try4666=try4666-1
      if(L0_efe) then
         I_(1)=1
      else
         I_(1)=0
      endif
C jgt_log.fgi(  95, 126):��������� LO->1
      if(L_ukes) then
         I0_okes=0
      endif
      I_(20)=I0_okes
      L_(739)=I0_okes.ne.0
C 1762-10-0001_log.fgi( 189,  65):���������� ��������� 
C label 4672  try4672=try4672-1
      if(L_(739).and..not.L0_akes) then
         R0_ofes=R0_ufes
      else
         R0_ofes=max(R_(39)-deltat,0.0)
      endif
      L_(738)=R0_ofes.gt.0.0
      L0_akes=L_(739)
C 1762-10-0001_log.fgi( 208,  58):������������  �� T
      if(.not.L_(738)) then
         R0_efes=0.0
      elseif(.not.L0_ifes) then
         R0_efes=R0_afes
      else
         R0_efes=max(R_(38)-deltat,0.0)
      endif
      L_ukes=L_(738).and.R0_efes.le.0.0
      L0_ifes=L_(738)
C 1762-10-0001_log.fgi( 219,  58):�������� ��������� ������
      if(L_(739)) then
         I0_ekes=I_(20)
      endif
      I_ikes=I0_ekes
C 1762-10-0001_log.fgi( 200,  64):�������-��������
      if(L_odip) then
         I0_idip=0
      endif
      I_(15)=I0_idip
      L_(578)=I0_idip.ne.0
C 1762-12-0001_log.fgi( 189,  65):���������� ��������� 
C label 4685  try4685=try4685-1
      if(L_(578).and..not.L0_ubip) then
         R0_ibip=R0_obip
      else
         R0_ibip=max(R_(37)-deltat,0.0)
      endif
      L_(577)=R0_ibip.gt.0.0
      L0_ubip=L_(578)
C 1762-12-0001_log.fgi( 208,  58):������������  �� T
      if(.not.L_(577)) then
         R0_abip=0.0
      elseif(.not.L0_ebip) then
         R0_abip=R0_uxep
      else
         R0_abip=max(R_(36)-deltat,0.0)
      endif
      L_odip=L_(577).and.R0_abip.le.0.0
      L0_ebip=L_(577)
C 1762-12-0001_log.fgi( 219,  58):�������� ��������� ������
      if(L_(578)) then
         I0_adip=I_(15)
      endif
      I_edip=I0_adip
C 1762-12-0001_log.fgi( 200,  64):�������-��������
      I_(2) = I_(1) + I_ude
C jgt_log.fgi( 103, 125):��������
C label 4698  try4698=try4698-1
      I_ude = I_(2)
C jgt_log.fgi( 108, 125):��������
C sav1=I_(2)
      I_(2) = I_(1) + I_ude
C jgt_log.fgi( 103, 125):recalc:��������
C if(sav1.ne.I_(2) .and. try4698.gt.0) goto 4698
      I_ame=I_ude
C jgt_log.fgi( 123, 125):������,nomer
      call putotable(I_ame,I_ule,R_ape,I_upe,I_ime,I_ale,I_ipe
     &,I_ume,I_ole,I_ope,I_eme,I_uke,I_epe,I_ome,I_ile,C255_ere
     &,L_ele,L_(37),C255_are)
C jgt_log.fgi( 127, 188):����� ���� � ������ �������,main
      if(L0_isos) then
         I_ores=I_ires
      else
         I_ores=I_ores
      endif
C 1762-10-0001_log.fgi(  52,  40):���� RE IN LO CH7
C label 4709  try4709=try4709-1
      if(L0_elip) then
         I_efip=I_afip
      else
         I_efip=I_efip
      endif
C 1762-12-0001_log.fgi(  52,  40):���� RE IN LO CH7
C label 4714  try4714=try4714-1
      End
