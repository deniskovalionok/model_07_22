       SUBROUTINE SPW_Init_tab(iTabVer)
       integer*4 iTabVer
       TYPE TSubr
         sequence
         integer*4 addr
         integer*4 period
         integer*4 phase
         integer*4 object
         integer*4 allowed
         integer*4 ncall
         real*8    time
         integer*4 individualScale
         integer*4 rezerv(2),flags,CPUTime(2)
         character(LEN=32) name
       END TYPE TSubr

       TYPE(TSubr) SPW_Table_Sub(3)
       Common/SPW_Table_Sub/ SPW_Table_Sub
       external FDC_01
       Data SPW_Table_Sub(1)/TSubr(0,1,0,2,0,0,0.,0,0,8,0,
     &   'FDC_01')/
       external X20FDC20
       Data SPW_Table_Sub(2)/TSubr(0,1,0,3,0,0,0.,0,0,8,0,
     &   'X20FDC20')/
       external FDC
       Data SPW_Table_Sub(3)/TSubr(0,1,0,4,0,0,0.,0,0,8,0,
     &   'FDC')/

       LOGICAL*1 SPW_0(0:6143)
       Common/SPW_0/ SPW_0 !
       CHARACTER*1 SPW_1(0:383)
       Common/SPW_1/ SPW_1 !
       LOGICAL*1 SPW_2(0:14335)
       Common/SPW_2/ SPW_2 !
       CHARACTER*1 SPW_3(0:16383)
       Common/SPW_3/ SPW_3 !
       LOGICAL*1 SPW_4(0:31)
       Common/SPW_4/ SPW_4 !
       CHARACTER*1 SPW_5(0:40959)
       Common/SPW_5/ SPW_5 !
       LOGICAL*1 SPW_6(0:15)
       Common/SPW_6/ SPW_6 !
       LOGICAL*1 SPW_7(0:95)
       Common/SPW_7/ SPW_7 !
       CHARACTER*1 SPW_8(0:63)
       Common/SPW_8/ SPW_8 !

       real*4 info_kwant
       integer*4 info_siz
       common/spw_info/ info_siz,info_kwant
       !MS$ATTRIBUTES DLLEXPORT::spw_info
       Data info_siz/8/,info_kwant/0.125/
       integer*4 dispStateHdr
       integer*4 stepCounter
       real*8    subInfo(12)
       common/spw_dispState/ dispStateHdr,stepCounter,subInfo

       character*(32) ver(4)
       integer*4 nver,versiz
       common/spw_versions/ nver,versiz,ver
       !MS$ATTRIBUTES DLLEXPORT::spw_versions
       Data nver/4/,versiz/32/
       Data ver(1)/''/
       Data ver(2)/'v_CD9C2065_FDC_01'/
       Data ver(3)/'v_E0C1613E_20FDC20'/
       Data ver(4)/'v_4C0E1047_FDC'/

       CALL SPW_SET_SIGN(787968374,0,-164965026)
       iTabVer=iTabVer*1000+1088

       CALL SPW_SET_UIDTR(-1088533433,-656563117)
       CALL SPW_SET_KWANT(REAL(0.125))
       CALL SPW_SET_NCMN(9)
       CALL SPW_ADD_CMN(LOC(SPW_0),5393)
       CALL SPW_ADD_CMN(LOC(SPW_1),340)
       CALL SPW_ADD_CMN(LOC(SPW_2),12494)
       CALL SPW_ADD_CMN(LOC(SPW_3),15765)
       CALL SPW_ADD_CMN(LOC(SPW_4),19)
       CALL SPW_ADD_CMN(LOC(SPW_5),36720)
       CALL SPW_ADD_CMN(LOC(SPW_6),16)
       CALL SPW_ADD_CMN(LOC(SPW_7),77)
       CALL SPW_ADD_CMN(LOC(SPW_8),40)
       CALL SPW_SET_NAMES(12061,
     +    'control_fdc_.vpt',
     +    'control_fdc_.D8DDA453.vpt')
       SPW_Table_Sub(1)%addr=LOC(FDC_01)
       SPW_Table_Sub(1)%allowed=LOC(SPW_0)+3198
       SPW_Table_Sub(1)%individualScale=LOC(SPW_0)+96
       SPW_Table_Sub(2)%addr=LOC(X20FDC20)
       SPW_Table_Sub(2)%allowed=LOC(SPW_2)+12492
       SPW_Table_Sub(2)%individualScale=LOC(SPW_2)+456
       SPW_Table_Sub(3)%addr=LOC(FDC)
       SPW_Table_Sub(3)%allowed=LOC(SPW_4)+18
       SPW_Table_Sub(3)%individualScale=LOC(SPW_4)+0
       CALL SPW_SET_SUB_LST(3,SPW_Table_Sub,stepCounter)
       end

       subroutine spw_initDisp
       integer*4 dispStateHdr
       integer*4 stepCounter
       real*8    subInfo(12)
       common/spw_dispState/ dispStateHdr,stepCounter,subInfo
       stepCounter=0
       subInfo=0
       end

       subroutine FDC_01_ConInQ
       end

       subroutine FDC_01_ConIn
       LOGICAL*1 SPW_0(0:6143)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:1535)
       equivalence(SPW_0,SPW_0_4)
        Call FDC_01_ConInQ
!beg ��������:20FDC70AN001
        SPW_0(4773)=.false.	!L_(1) O
!end ��������:20FDC70AN001
!beg �������:VP1
        SPW_0(4812)=.false.	!L_(40) O
!end �������:VP1
!beg �������:PC1
        SPW_0(4810)=.false.	!L_(38) O
!end �������:PC1
!beg �������:PC2
        SPW_0(4808)=.false.	!L_(36) O
!end �������:PC2
!beg �������:VE1
        SPW_0(4815)=.false.	!L_(43) O
!end �������:VE1
!beg �������:VE2
        SPW_0(4813)=.false.	!L_(41) O
!end �������:VE2
!beg �������:VP1
        SPW_0(4811)=.false.	!L_(39) O
!end �������:VP1
!beg �������:PC1
        SPW_0(4809)=.false.	!L_(37) O
!end �������:PC1
!beg �������:PC2
        SPW_0(4807)=.false.	!L_(35) O
!end �������:PC2
!beg ���������:20FDC70AN001
        SPW_0(4774)=.false.	!L_(2) O
!end ���������:20FDC70AN001
!beg �������:VE1
        SPW_0(4816)=.false.	!L_(44) O
!end �������:VE1
!beg �������:VE2
        SPW_0(4814)=.false.	!L_(42) O
!end �������:VE2
       end
