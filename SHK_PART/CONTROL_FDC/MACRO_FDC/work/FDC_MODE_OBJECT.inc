      Interface
      Subroutine FDC_MODE_OBJECT(ext_deltat,I_i,I_u,I_ad,I_ed
     &,I_id,I_ud,I_af,I1_of,I1_uf,I_ik,I_ok,I1_il,I_am,I_ap
     &,I1_ep)
C |I_i           |2 4 I|sel_mode_0_object|������� ������ ��������� "�������� ��������� ���� ������\n������ ������ �������"|0|
C |I_u           |2 4 O|bgl_object_mode0|���� ������ ����� ����������� ������ "������������������"||
C |I_ad          |2 4 O|bgu_object_mode0|���� ������� ����� ����������� ������ "������������������"||
C |I_ed          |2 4 O|bgl_object_mode2|���� ������ ����� ����������� ������ "���. ������."||
C |I_id          |2 4 O|bgu_object_mode2|���� ������� ����� ����������� ������ "���. ������."||
C |I_ud          |2 4 O|bgl_object_mode3|���� ������ ����� ����������� ������ "������"||
C |I_af          |2 4 O|bgu_object_mode3|���� ������� ����� ����������� ������ "������"||
C |I1_of         |2 1 O|object_mode0    |���� ���������� ������ "������������������"|0|
C |I1_uf         |2 1 O|object_mode2    |���� ���������� ������ "���. ������."|0|
C |I_ik          |2 4 K|_lcmp1          |�������� ������ �����������|0|
C |I_ok          |2 4 K|_lcmp3          |�������� ������ �����������|2|
C |I1_il         |2 1 O|object_mode3    |���� ���������� ������ "������"|0|
C |I_am          |2 4 K|_lcmp4          |�������� ������ �����������|3|
C |I_ap          |2 4 O|tbg_mode_object |���� ���� ������ ���������� ������ ������ �������||
C |I1_ep         |2 1 O|b_mode_object   |��������� ����� ������|1|

      IMPLICIT NONE
      REAL*4 ext_deltat
      INTEGER*4 I_i,I_u,I_ad,I_ed,I_id,I_ud,I_af
      INTEGER*1 I1_of,I1_uf
      INTEGER*4 I_ik,I_ok
      INTEGER*1 I1_il
      INTEGER*4 I_am,I_ap
      INTEGER*1 I1_ep
      End subroutine FDC_MODE_OBJECT
      End interface
