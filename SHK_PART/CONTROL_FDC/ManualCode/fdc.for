      subroutine fdc
      implicit none
      include 'fdc.fh'

!___________________________________________________________________________________
! 20FDC10
! ������� �������� �������� � ���������� ������������� �����
!___________________________________________________________________________________
      tl_mode_0_20FDC10 = ktl_mode_0_20FDC10(
     &                    max(min(b_mode_0_20FDC10,Nb_mode_0-1),0))

      tl_mode_20FDC10 = ktl_mode_20FDC10(
     &                  max(min(b_mode_20FDC10,Nb_mode-1),0))

      tl_mode_20FDC10AC301 = ktl_mode_20FDC10AC301(
     &                  max(min(b_mode_20FDC10AC301,Nb_mode_0-1),0))

      tl_steps_20FDC10 = ktl_steps_20FDC10(
     &                   max(min(step_number_20FDC10,Nstep_number-1),0))

      tl_steps_20FDC10AC301 = ktl_steps_20FDC10AC301(
     &            max(min(step_number_20FDC10AC301,Nstep_number-1),0))

      tl_steps_20FDC10_1 = ktl_steps_20FDC10_1(
     &            max(min(step_number_20FDC10_1,Nstep_number-1),0))

      tl_steps_1_20FDC10 = ktl_steps_1_20FDC10(
     &                     max(min(step_number_0_20FDC10,
     &                     Nstep_number_0-1),0))

      select case(lotok12_numb_20FDC10)
         case(1)
            tvel_numb_0_20FDC10 = tvel_numb_l1_20FDC10
         case(2)
            tvel_numb_0_20FDC10 = tvel_numb_l2_20FDC10
      end select

      select case(lotok34_numb_20FDC10)
         case(3)
            tvel_numb_1_20FDC10 = tvel_numb_l3_20FDC10
         case(4)
            tvel_numb_1_20FDC10 = tvel_numb_l4_20FDC10
      end select

!___________________________________________________________________________________
! 20FDC20
! ������� ������ � ������������ ������
!___________________________________________________________________________________
      tl_mode_0_20FDC20 = ktl_mode_0_20FDC20(
     &                    max(min(b_mode_0_20FDC20,Nb_mode_0-1),0))

      tl_mode_20FDC20 = ktl_mode_20FDC20(
     &                  max(min(b_mode_20FDC20,Nb_mode-1),0))

      tl_mode_20FDC20wm = ktl_mode_20FDC20wm(
     &                  max(min(b_mode_20FDC20wm,Nb_mode-1),0))

      tl_steps_20FDC20wm = ktl_steps_20FDC20wm(
     &            max(min(step_number_20FDC20wm,Nstep_number-1),0))

      tl_mode_20FDC20sm_1762_07 = ktl_mode_20FDC20sm_1762_07(
     &               max(min(b_mode_20FDC20sm_1762_07,Nb_mode-1),0))

      tl_steps_20FDC20sm_1762_07 = ktl_steps_20FDC20sm_1762_07(
     &      max(min(step_number_20FDC20sm_1762_07,Nstep_number-1),0))

      tl_mode_20FDC20db = ktl_mode_20FDC20db(
     &                  max(min(b_mode_20FDC20db,Nb_mode-1),0))

      tl_steps_20FDC20db = ktl_steps_20FDC20db(
     &            max(min(step_number_20FDC20db,Nstep_number-1),0))

      select case(lotok1234_numb_20FDC20db)
         case(1)
            tvel_numb_0_20FDC20db = tvel_numb_l1_20FDC20db
         case(2)
            tvel_numb_0_20FDC20db = tvel_numb_l2_20FDC20db
         case(3)
            tvel_numb_0_20FDC20db = tvel_numb_l3_20FDC20db
         case(4)
            tvel_numb_0_20FDC20db = tvel_numb_l4_20FDC20db
      end select

      tl_mode_20FDC20fem_1762_06 = ktl_mode_20FDC20fem_1762_06(
     &               max(min(b_mode_20FDC20fem_1762_06,Nb_mode-1),0))

      tl_steps_20FDC20fem_1762_06 = ktl_steps_20FDC20fem_1762_06(
     &     max(min(step_number_20FDC20fem_1762_06,Nstep_number-1),0))

      tl_mode_20FDC20fet_1762_11 = ktl_mode_20FDC20fet_1762_11(
     &               max(min(b_mode_20FDC20fet_1762_11,Nb_mode-1),0))

      tl_steps_20FDC20fet_1762_11 = ktl_steps_20FDC20fet_1762_11(
     &     max(min(step_number_20FDC20fet_1762_11,Nstep_number-1),0))


      tl_mode_20FDC20po = ktl_mode_20FDC20po(
     &               max(min(b_mode_20FDC20po,Nb_mode-1),0))

      tl_steps_20FDC20po = ktl_steps_20FDC20po(
     &     max(min(step_number_20FDC20po,Nstep_number-1),0))

      select case(lotok1234_numb_20FDC20po)
         case(1)
            tvel_numb_0_20FDC20po = tvel_numb_l1_20FDC20po
         case(2)
            tvel_numb_0_20FDC20po = tvel_numb_l2_20FDC20po
         case(3)
            tvel_numb_0_20FDC20po = tvel_numb_l3_20FDC20po
         case(4)
            tvel_numb_0_20FDC20po = tvel_numb_l4_20FDC20po
      end select

      tl_mode_20FDC20plb_1762_11 = ktl_mode_20FDC20plb_1762_11(
     &               max(min(b_mode_20FDC20plb_1762_11,Nb_mode-1),0))

      tl_steps_20FDC20plb_1762_11 = ktl_steps_20FDC20plb_1762_11(
     &     max(min(step_number_20FDC20plb_1762_11,Nstep_number-1),0))

!______________________________________________________________
! 20FDC30
! ������� ������������ ������
!______________________________________________________________
      tl_mode_0_20FDC30 = ktl_mode_0_20FDC30(
     &                    max(min(b_mode_0_20FDC30,Nb_mode_0-1),0))

      tl_mode_20FDC30 = ktl_mode_20FDC30(
     &                  max(min(b_mode_20FDC30,Nb_mode-1),0))

      ! �����-���� 20FDC31�
      !---------------------------------------------
      tl_mode_20FDC31 = ktl_mode_20FDC31(
     &                  max(min(b_mode_20FDC31,Nb_mode-1),0))

      tl_steps_20FDC31 = ktl_steps_20FDC31(
     &            max(min(step_number_20FDC31,Nstep_number-1),0))

      ! ����� ����������������� ������������ 20FDC34�
      !---------------------------------------------
      tl_mode_20FDC34 = ktl_mode_20FDC34(
     &                  max(min(b_mode_20FDC34,Nb_mode-1),0))

      tl_steps_20FDC34 = ktl_steps_20FDC34(
     &            max(min(step_number_20FDC34,Nstep_number-1),0))

      ! �����-���������� ����� 20FDC32�
      !---------------------------------------------
      tl_mode_20FDC32 = ktl_mode_20FDC32(
     &                  max(min(b_mode_20FDC32,Nb_mode-1),0))

      tl_steps_20FDC32 = ktl_steps_20FDC32(
     &            max(min(step_number_20FDC32,Nstep_number-1),0))

      select case(lotok1234_numb_20FDC32)
         case(1)
            tvel_numb_0_20FDC32 = tvel_numb_l1_20FDC32
         case(2)
            tvel_numb_0_20FDC32 = tvel_numb_l2_20FDC32
         case(3)
            tvel_numb_0_20FDC32 = tvel_numb_l3_20FDC32
         case(4)
            tvel_numb_0_20FDC32 = tvel_numb_l4_20FDC32
      end select

      ! �����-���������� ������ 20FDC33�
      !---------------------------------------------
      tl_mode_20FDC33 = ktl_mode_20FDC33(
     &                  max(min(b_mode_20FDC33,Nb_mode-1),0))

      tl_steps_20FDC33 = ktl_steps_20FDC33(
     &            max(min(step_number_20FDC33,Nstep_number-1),0))

      select case(lotok1234_numb_20FDC33)
         case(1)
            tvel_numb_0_20FDC33 = tvel_numb_l1_20FDC33
         case(2)
            tvel_numb_0_20FDC33 = tvel_numb_l2_20FDC33
         case(3)
            tvel_numb_0_20FDC33 = tvel_numb_l3_20FDC33
         case(4)
            tvel_numb_0_20FDC33 = tvel_numb_l4_20FDC33
      end select

!______________________________________________________________
! 20FDC
! ����������� ���������� �.62.690.000
!______________________________________________________________
      tl_mode_trna62690000 = ktl_mode_trna62690000(
     &                  max(min(b_mode_trna62690000,Nb_mode-1),0))

      tl_steps_trna62690000 = ktl_steps_trna62690000(
     &            max(min(step_number_trna62690000,Nstep_number-1),0))

!______________________________________________________________
! 20FDC40
! ������� �������������� � �������� ������������� 20FDC40
!______________________________________________________________
      tl_mode_0_20FDC40 = ktl_mode_0_20FDC40(
     &                    max(min(b_mode_0_20FDC40,Nb_mode_0-1),0))

      tl_mode_20FDC40 = ktl_mode_20FDC40(
     &                  max(min(b_mode_20FDC40,Nb_mode-1),0))

      ! ����-1�
      !---------------------------------------------
      tl_mode_UKG1 = ktl_mode_UKG1(
     &                  max(min(b_mode_UKG1,Nb_mode-1),0))

      tl_steps_UKG1 = ktl_steps_UKG1(
     &            max(min(step_number_UKG1,Nstep_number-1),0))

      ! ����-2�
      !---------------------------------------------
      tl_mode_UKG2 = ktl_mode_UKG2(
     &                  max(min(b_mode_UKG2,Nb_mode-1),0))

      tl_steps_UKG2 = ktl_steps_UKG2(
     &            max(min(step_number_UKG2,Nstep_number-1),0))

!______________________________________________________________
! 20FDC50
! ������� �������� ���������� ������, ��������� �������� ������ � ������ �������� 20FDC50
!______________________________________________________________
      tl_mode_0_20FDC50 = ktl_mode_0_20FDC50(
     &                    max(min(b_mode_0_20FDC50,Nb_mode_0-1),0))

      tl_mode_20FDC50 = ktl_mode_20FDC50(
     &                  max(min(b_mode_20FDC50,Nb_mode-1),0))

      ! ����� ��������� �����-�������������� 20FDC50AX001�
      !-----------------------------------------------------
      tl_mode_20FDC50AX001 = ktl_mode_20FDC50AX001(
     &                  max(min(b_mode_20FDC50AX001,Nb_mode-1),0))

      tl_steps_20FDC50AX001 = ktl_steps_20FDC50AX001(
     &            max(min(step_number_20FDC50AX001,Nstep_number-1),0))

      ! ����������� ������ � ������� ������������� 20FDC50FG101�
      !----------------------------------------------------------
      tl_mode_20FDC50FG101 = ktl_mode_20FDC50FG101(
     &                  max(min(b_mode_20FDC50FG101,Nb_mode-1),0))

      tl_steps_20FDC50FG101 = ktl_steps_20FDC50FG101(
     &            max(min(step_number_20FDC50FG101,Nstep_number-1),0))

      ! ������-�������������� 20FDC50CR101�
      !----------------------------------------------------------
      tl_mode_20FDC50CR101 = ktl_mode_20FDC50CR101(
     &                  max(min(b_mode_20FDC50CR101,Nb_mode-1),0))

      tl_steps_20FDC50CR101 = ktl_steps_20FDC50CR101(
     &            max(min(step_number_20FDC50CR101,Nstep_number-1),0))

      ! �������� ����� 20FDC50FQ101�
      !----------------------------------------------------------
      tl_mode_20FDC50FQ101 = ktl_mode_20FDC50FQ101(
     &                  max(min(b_mode_20FDC50FQ101,Nb_mode-1),0))

      tl_steps_20FDC50FQ101 = ktl_steps_20FDC50FQ101(
     &            max(min(step_number_20FDC50FQ101,Nstep_number-1),0))

      ! ���������� ������ �������� � �������� ��� � �������� ���������
      !-----------------------------------------------------------------
      tl_mode_20FDC50_1 = ktl_mode_20FDC50_1(
     &                  max(min(b_mode_20FDC50_1,Nb_mode-1),0))

      tl_steps_20FDC50_1 = ktl_steps_20FDC50_1(
     &            max(min(step_number_20FDC50_1,Nstep_number-1),0))

      ! ��������. ��������� 20FDC50CU101�
      !-----------------------------------------------------------------
      tl_mode_20FDC50CU101 = ktl_mode_20FDC50CU101(
     &                  max(min(b_mode_20FDC50CU101,Nb_mode-1),0))

      tl_steps_20FDC50CU101 = ktl_steps_20FDC50CU101(
     &            max(min(step_number_20FDC50CU101,Nstep_number-1),0))

      ! ���� 20FDC50CQ101�
      !-----------------------------------------------------------------
      tl_mode_20FDC50CQ101 = ktl_mode_20FDC50CQ101(
     &                  max(min(b_mode_20FDC50CQ101,Nb_mode-1),0))

      tl_steps_20FDC50CQ101 = ktl_steps_20FDC50CQ101(
     &            max(min(step_number_20FDC50CQ101,Nstep_number-1),0))

      ! ���� 20FDC50CW101�
      !-----------------------------------------------------------------
      tl_mode_20FDC50CW101 = ktl_mode_20FDC50CW101(
     &                  max(min(b_mode_20FDC50CW101,Nb_mode-1),0))

      tl_steps_20FDC50CW101 = ktl_steps_20FDC50CW101(
     &            max(min(step_number_20FDC50CW101,Nstep_number-1),0))


!______________________________________________________________
! 20FDC70
! ������� �������� ����������� ������ 20FDC70
!______________________________________________________________
      tl_mode_0_20FDC70 = ktl_mode_0_20FDC70(
     &                    max(min(b_mode_0_20FDC70,Nb_mode_0-1),0))

      tl_mode_20FDC70 = ktl_mode_20FDC70(
     &                  max(min(b_mode_20FDC70,Nb_mode-1),0))

      ! ����� �������, ������������ ��������, �������� ������ � ����� ��������
      !-------------------------------------------------------------------------
      tl_mode_20FDC70_1 = ktl_mode_20FDC70_1(
     &                  max(min(b_mode_20FDC70_1,Nb_mode-1),0))

      tl_steps_20FDC70_1 = ktl_steps_20FDC70_1(
     &            max(min(step_number_20FDC70_1,Nstep_number-1),0))

      end
