      Subroutine FDA_10boats(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDA_10boats.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I_erel=0
      I_irel=0
      I_arel=0
      I_upel=0
      I_opel=0
      I_imel=0
      I_emel=0
      I_amel=0
      I_ulel=0
      I_olel=0
      I_axuk=0
      I_uvuk=0
      I_ovuk=0
      I_ivuk=0
      I_evuk=0
      I_axal=0
      I_uval=0
      I_oval=0
      I_ival=0
      I_eval=0
      I_atuk=0
      I_usuk=0
      I_osuk=0
      I_isuk=0
      I_esuk=0
      I_atal=0
      I_usal=0
      I_osal=0
      I_isal=0
      I_esal=0
      I_aruk=0
      I_upuk=0
      I_opuk=0
      I_ipuk=0
      I_epuk=0
      I_aral=0
      I_upal=0
      I_opal=0
      I_ipal=0
      I_epal=0
      I_amuk=0
      I_uluk=0
      I_oluk=0
      I_iluk=0
      I_eluk=0
      I_amal=0
      I_ulal=0
      I_olal=0
      I_ilal=0
      I_elal=0
      I_akuk=0
      I_ufuk=0
      I_ofuk=0
      I_ifuk=0
      I_efuk=0
      I_akal=0
      I_ufal=0
      I_ofal=0
      I_ifal=0
      I_efal=0
      I_aduk=0
      I_ubuk=0
      I_obuk=0
      I_ibuk=0
      I_ebuk=0
      I_adal=0
      I_ubal=0
      I_obal=0
      I_ibal=0
      I_ebal=0
      R_esel=0
      R_isel=0
      R_asel=0
      R_urel=0
      R_orel=0
      R_ipel=0
      R_epel=0
      R_apel=0
      R_umel=0
      R_omel=0
      R_abal=0
      R_uxuk=0
      R_oxuk=0
      R_ixuk=0
      R_exuk=0
      R_abel=0
      R_uxal=0
      R_oxal=0
      R_ixal=0
      R_exal=0
      R_avuk=0
      R_utuk=0
      R_otuk=0
      R_ituk=0
      R_etuk=0
      R_aval=0
      R_utal=0
      R_otal=0
      R_ital=0
      R_etal=0
      R_asuk=0
      R_uruk=0
      R_oruk=0
      R_iruk=0
      R_eruk=0
      R_asal=0
      R_ural=0
      R_oral=0
      R_iral=0
      R_eral=0
      R_apuk=0
      R_umuk=0
      R_omuk=0
      R_imuk=0
      R_emuk=0
      R_apal=0
      R_umal=0
      R_omal=0
      R_imal=0
      R_emal=0
      R_aluk=0
      R_ukuk=0
      R_okuk=0
      R_ikuk=0
      R_ekuk=0
      R_alal=0
      R_ukal=0
      R_okal=0
      R_ikal=0
      R_ekal=0
      R_afuk=0
      R_uduk=0
      R_oduk=0
      R_iduk=0
      R_eduk=0
      R_afal=0
      R_udal=0
      R_odal=0
      R_idal=0
      R_edal=0
      I_utel=0
      R_avel=0
      I_ovel=0
      R_uvel=0
      I_itel=0
      R_otel=0
      I_evel=0
      R_ivel=0
      I_atel=0
      R_etel=0
      I_alel=0
      R_ilel=0
      I_ukel=0
      R_elel=0
      I_osel=0
      R_usel=0
      I_efel=0
      R_okel=0
      I_afel=0
      R_ikel=0
      I_udel=0
      R_ekel=0
      I_odel=0
      R_akel=0
      I_idel=0
      R_ufel=0
      I_edel=0
      R_ofel=0
      I_adel=0
      R_ifel=0
      I_obel=0
      R_ubel=0
      I_ebel=0
      R_ibel=0
      I_axel=0
      R_exel=0
C FDA_10boats.fgi(  40, 230):pre: ���������� ���������� ������� �� ������� FDA60,BOAT10
      I_(10)=I_e !CopyBack
C FDA_10boats.fgi(  39, 276):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT10:CR
      I_erel=0
      I_irel=0
      I_arel=0
      I_upel=0
      I_opel=0
      I_imel=0
      I_emel=0
      I_amel=0
      I_ulel=0
      I_olel=0
      I_axuk=0
      I_uvuk=0
      I_ovuk=0
      I_ivuk=0
      I_evuk=0
      I_axal=0
      I_uval=0
      I_oval=0
      I_ival=0
      I_eval=0
      I_atuk=0
      I_usuk=0
      I_osuk=0
      I_isuk=0
      I_esuk=0
      I_atal=0
      I_usal=0
      I_osal=0
      I_isal=0
      I_esal=0
      I_aruk=0
      I_upuk=0
      I_opuk=0
      I_ipuk=0
      I_epuk=0
      I_aral=0
      I_upal=0
      I_opal=0
      I_ipal=0
      I_epal=0
      I_amuk=0
      I_uluk=0
      I_oluk=0
      I_iluk=0
      I_eluk=0
      I_amal=0
      I_ulal=0
      I_olal=0
      I_ilal=0
      I_elal=0
      I_akuk=0
      I_ufuk=0
      I_ofuk=0
      I_ifuk=0
      I_efuk=0
      I_akal=0
      I_ufal=0
      I_ofal=0
      I_ifal=0
      I_efal=0
      I_aduk=0
      I_ubuk=0
      I_obuk=0
      I_ibuk=0
      I_ebuk=0
      I_adal=0
      I_ubal=0
      I_obal=0
      I_ibal=0
      I_ebal=0
      R_esel=0
      R_isel=0
      R_asel=0
      R_urel=0
      R_orel=0
      R_ipel=0
      R_epel=0
      R_apel=0
      R_umel=0
      R_omel=0
      R_abal=0
      R_uxuk=0
      R_oxuk=0
      R_ixuk=0
      R_exuk=0
      R_abel=0
      R_uxal=0
      R_oxal=0
      R_ixal=0
      R_exal=0
      R_avuk=0
      R_utuk=0
      R_otuk=0
      R_ituk=0
      R_etuk=0
      R_aval=0
      R_utal=0
      R_otal=0
      R_ital=0
      R_etal=0
      R_asuk=0
      R_uruk=0
      R_oruk=0
      R_iruk=0
      R_eruk=0
      R_asal=0
      R_ural=0
      R_oral=0
      R_iral=0
      R_eral=0
      R_apuk=0
      R_umuk=0
      R_omuk=0
      R_imuk=0
      R_emuk=0
      R_apal=0
      R_umal=0
      R_omal=0
      R_imal=0
      R_emal=0
      R_aluk=0
      R_ukuk=0
      R_okuk=0
      R_ikuk=0
      R_ekuk=0
      R_alal=0
      R_ukal=0
      R_okal=0
      R_ikal=0
      R_ekal=0
      R_afuk=0
      R_uduk=0
      R_oduk=0
      R_iduk=0
      R_eduk=0
      R_afal=0
      R_udal=0
      R_odal=0
      R_idal=0
      R_edal=0
      I_utel=0
      R_avel=0
      I_ovel=0
      R_uvel=0
      I_itel=0
      R_otel=0
      I_evel=0
      R_ivel=0
      I_atel=0
      R_etel=0
      I_alel=0
      R_ilel=0
      I_ukel=0
      R_elel=0
      I_osel=0
      R_usel=0
      I_efel=0
      R_okel=0
      I_afel=0
      R_ikel=0
      I_udel=0
      R_ekel=0
      I_odel=0
      R_akel=0
      I_idel=0
      R_ufel=0
      I_edel=0
      R_ofel=0
      I_adel=0
      R_ifel=0
      I_obel=0
      R_ubel=0
      I_ebel=0
      R_ibel=0
      I_axel=0
      R_exel=0
C FDA_10boats.fgi(  60, 230):pre: ���������� ���������� ������� �� ������� FDA60,BOAT09
      I_(9)=I_i !CopyBack
C FDA_10boats.fgi(  59, 276):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT09:CR
      I_erel=0
      I_irel=0
      I_arel=0
      I_upel=0
      I_opel=0
      I_imel=0
      I_emel=0
      I_amel=0
      I_ulel=0
      I_olel=0
      I_axuk=0
      I_uvuk=0
      I_ovuk=0
      I_ivuk=0
      I_evuk=0
      I_axal=0
      I_uval=0
      I_oval=0
      I_ival=0
      I_eval=0
      I_atuk=0
      I_usuk=0
      I_osuk=0
      I_isuk=0
      I_esuk=0
      I_atal=0
      I_usal=0
      I_osal=0
      I_isal=0
      I_esal=0
      I_aruk=0
      I_upuk=0
      I_opuk=0
      I_ipuk=0
      I_epuk=0
      I_aral=0
      I_upal=0
      I_opal=0
      I_ipal=0
      I_epal=0
      I_amuk=0
      I_uluk=0
      I_oluk=0
      I_iluk=0
      I_eluk=0
      I_amal=0
      I_ulal=0
      I_olal=0
      I_ilal=0
      I_elal=0
      I_akuk=0
      I_ufuk=0
      I_ofuk=0
      I_ifuk=0
      I_efuk=0
      I_akal=0
      I_ufal=0
      I_ofal=0
      I_ifal=0
      I_efal=0
      I_aduk=0
      I_ubuk=0
      I_obuk=0
      I_ibuk=0
      I_ebuk=0
      I_adal=0
      I_ubal=0
      I_obal=0
      I_ibal=0
      I_ebal=0
      R_esel=0
      R_isel=0
      R_asel=0
      R_urel=0
      R_orel=0
      R_ipel=0
      R_epel=0
      R_apel=0
      R_umel=0
      R_omel=0
      R_abal=0
      R_uxuk=0
      R_oxuk=0
      R_ixuk=0
      R_exuk=0
      R_abel=0
      R_uxal=0
      R_oxal=0
      R_ixal=0
      R_exal=0
      R_avuk=0
      R_utuk=0
      R_otuk=0
      R_ituk=0
      R_etuk=0
      R_aval=0
      R_utal=0
      R_otal=0
      R_ital=0
      R_etal=0
      R_asuk=0
      R_uruk=0
      R_oruk=0
      R_iruk=0
      R_eruk=0
      R_asal=0
      R_ural=0
      R_oral=0
      R_iral=0
      R_eral=0
      R_apuk=0
      R_umuk=0
      R_omuk=0
      R_imuk=0
      R_emuk=0
      R_apal=0
      R_umal=0
      R_omal=0
      R_imal=0
      R_emal=0
      R_aluk=0
      R_ukuk=0
      R_okuk=0
      R_ikuk=0
      R_ekuk=0
      R_alal=0
      R_ukal=0
      R_okal=0
      R_ikal=0
      R_ekal=0
      R_afuk=0
      R_uduk=0
      R_oduk=0
      R_iduk=0
      R_eduk=0
      R_afal=0
      R_udal=0
      R_odal=0
      R_idal=0
      R_edal=0
      I_utel=0
      R_avel=0
      I_ovel=0
      R_uvel=0
      I_itel=0
      R_otel=0
      I_evel=0
      R_ivel=0
      I_atel=0
      R_etel=0
      I_alel=0
      R_ilel=0
      I_ukel=0
      R_elel=0
      I_osel=0
      R_usel=0
      I_efel=0
      R_okel=0
      I_afel=0
      R_ikel=0
      I_udel=0
      R_ekel=0
      I_odel=0
      R_akel=0
      I_idel=0
      R_ufel=0
      I_edel=0
      R_ofel=0
      I_adel=0
      R_ifel=0
      I_obel=0
      R_ubel=0
      I_ebel=0
      R_ibel=0
      I_axel=0
      R_exel=0
C FDA_10boats.fgi(  80, 230):pre: ���������� ���������� ������� �� ������� FDA60,BOAT08
      I_(8)=I_o !CopyBack
C FDA_10boats.fgi(  79, 276):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT08:CR
      I_erel=0
      I_irel=0
      I_arel=0
      I_upel=0
      I_opel=0
      I_imel=0
      I_emel=0
      I_amel=0
      I_ulel=0
      I_olel=0
      I_axuk=0
      I_uvuk=0
      I_ovuk=0
      I_ivuk=0
      I_evuk=0
      I_axal=0
      I_uval=0
      I_oval=0
      I_ival=0
      I_eval=0
      I_atuk=0
      I_usuk=0
      I_osuk=0
      I_isuk=0
      I_esuk=0
      I_atal=0
      I_usal=0
      I_osal=0
      I_isal=0
      I_esal=0
      I_aruk=0
      I_upuk=0
      I_opuk=0
      I_ipuk=0
      I_epuk=0
      I_aral=0
      I_upal=0
      I_opal=0
      I_ipal=0
      I_epal=0
      I_amuk=0
      I_uluk=0
      I_oluk=0
      I_iluk=0
      I_eluk=0
      I_amal=0
      I_ulal=0
      I_olal=0
      I_ilal=0
      I_elal=0
      I_akuk=0
      I_ufuk=0
      I_ofuk=0
      I_ifuk=0
      I_efuk=0
      I_akal=0
      I_ufal=0
      I_ofal=0
      I_ifal=0
      I_efal=0
      I_aduk=0
      I_ubuk=0
      I_obuk=0
      I_ibuk=0
      I_ebuk=0
      I_adal=0
      I_ubal=0
      I_obal=0
      I_ibal=0
      I_ebal=0
      R_esel=0
      R_isel=0
      R_asel=0
      R_urel=0
      R_orel=0
      R_ipel=0
      R_epel=0
      R_apel=0
      R_umel=0
      R_omel=0
      R_abal=0
      R_uxuk=0
      R_oxuk=0
      R_ixuk=0
      R_exuk=0
      R_abel=0
      R_uxal=0
      R_oxal=0
      R_ixal=0
      R_exal=0
      R_avuk=0
      R_utuk=0
      R_otuk=0
      R_ituk=0
      R_etuk=0
      R_aval=0
      R_utal=0
      R_otal=0
      R_ital=0
      R_etal=0
      R_asuk=0
      R_uruk=0
      R_oruk=0
      R_iruk=0
      R_eruk=0
      R_asal=0
      R_ural=0
      R_oral=0
      R_iral=0
      R_eral=0
      R_apuk=0
      R_umuk=0
      R_omuk=0
      R_imuk=0
      R_emuk=0
      R_apal=0
      R_umal=0
      R_omal=0
      R_imal=0
      R_emal=0
      R_aluk=0
      R_ukuk=0
      R_okuk=0
      R_ikuk=0
      R_ekuk=0
      R_alal=0
      R_ukal=0
      R_okal=0
      R_ikal=0
      R_ekal=0
      R_afuk=0
      R_uduk=0
      R_oduk=0
      R_iduk=0
      R_eduk=0
      R_afal=0
      R_udal=0
      R_odal=0
      R_idal=0
      R_edal=0
      I_utel=0
      R_avel=0
      I_ovel=0
      R_uvel=0
      I_itel=0
      R_otel=0
      I_evel=0
      R_ivel=0
      I_atel=0
      R_etel=0
      I_alel=0
      R_ilel=0
      I_ukel=0
      R_elel=0
      I_osel=0
      R_usel=0
      I_efel=0
      R_okel=0
      I_afel=0
      R_ikel=0
      I_udel=0
      R_ekel=0
      I_odel=0
      R_akel=0
      I_idel=0
      R_ufel=0
      I_edel=0
      R_ofel=0
      I_adel=0
      R_ifel=0
      I_obel=0
      R_ubel=0
      I_ebel=0
      R_ibel=0
      I_axel=0
      R_exel=0
C FDA_10boats.fgi( 100, 230):pre: ���������� ���������� ������� �� ������� FDA60,BOAT07
      I_(7)=I_u !CopyBack
C FDA_10boats.fgi(  99, 276):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT07:CR
      I_erel=0
      I_irel=0
      I_arel=0
      I_upel=0
      I_opel=0
      I_imel=0
      I_emel=0
      I_amel=0
      I_ulel=0
      I_olel=0
      I_axuk=0
      I_uvuk=0
      I_ovuk=0
      I_ivuk=0
      I_evuk=0
      I_axal=0
      I_uval=0
      I_oval=0
      I_ival=0
      I_eval=0
      I_atuk=0
      I_usuk=0
      I_osuk=0
      I_isuk=0
      I_esuk=0
      I_atal=0
      I_usal=0
      I_osal=0
      I_isal=0
      I_esal=0
      I_aruk=0
      I_upuk=0
      I_opuk=0
      I_ipuk=0
      I_epuk=0
      I_aral=0
      I_upal=0
      I_opal=0
      I_ipal=0
      I_epal=0
      I_amuk=0
      I_uluk=0
      I_oluk=0
      I_iluk=0
      I_eluk=0
      I_amal=0
      I_ulal=0
      I_olal=0
      I_ilal=0
      I_elal=0
      I_akuk=0
      I_ufuk=0
      I_ofuk=0
      I_ifuk=0
      I_efuk=0
      I_akal=0
      I_ufal=0
      I_ofal=0
      I_ifal=0
      I_efal=0
      I_aduk=0
      I_ubuk=0
      I_obuk=0
      I_ibuk=0
      I_ebuk=0
      I_adal=0
      I_ubal=0
      I_obal=0
      I_ibal=0
      I_ebal=0
      R_esel=0
      R_isel=0
      R_asel=0
      R_urel=0
      R_orel=0
      R_ipel=0
      R_epel=0
      R_apel=0
      R_umel=0
      R_omel=0
      R_abal=0
      R_uxuk=0
      R_oxuk=0
      R_ixuk=0
      R_exuk=0
      R_abel=0
      R_uxal=0
      R_oxal=0
      R_ixal=0
      R_exal=0
      R_avuk=0
      R_utuk=0
      R_otuk=0
      R_ituk=0
      R_etuk=0
      R_aval=0
      R_utal=0
      R_otal=0
      R_ital=0
      R_etal=0
      R_asuk=0
      R_uruk=0
      R_oruk=0
      R_iruk=0
      R_eruk=0
      R_asal=0
      R_ural=0
      R_oral=0
      R_iral=0
      R_eral=0
      R_apuk=0
      R_umuk=0
      R_omuk=0
      R_imuk=0
      R_emuk=0
      R_apal=0
      R_umal=0
      R_omal=0
      R_imal=0
      R_emal=0
      R_aluk=0
      R_ukuk=0
      R_okuk=0
      R_ikuk=0
      R_ekuk=0
      R_alal=0
      R_ukal=0
      R_okal=0
      R_ikal=0
      R_ekal=0
      R_afuk=0
      R_uduk=0
      R_oduk=0
      R_iduk=0
      R_eduk=0
      R_afal=0
      R_udal=0
      R_odal=0
      R_idal=0
      R_edal=0
      I_utel=0
      R_avel=0
      I_ovel=0
      R_uvel=0
      I_itel=0
      R_otel=0
      I_evel=0
      R_ivel=0
      I_atel=0
      R_etel=0
      I_alel=0
      R_ilel=0
      I_ukel=0
      R_elel=0
      I_osel=0
      R_usel=0
      I_efel=0
      R_okel=0
      I_afel=0
      R_ikel=0
      I_udel=0
      R_ekel=0
      I_odel=0
      R_akel=0
      I_idel=0
      R_ufel=0
      I_edel=0
      R_ofel=0
      I_adel=0
      R_ifel=0
      I_obel=0
      R_ubel=0
      I_ebel=0
      R_ibel=0
      I_axel=0
      R_exel=0
C FDA_10boats.fgi( 120, 230):pre: ���������� ���������� ������� �� ������� FDA60,BOAT06
      I_(6)=I_ad !CopyBack
C FDA_10boats.fgi( 119, 276):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT06:CR
      I_erel=0
      I_irel=0
      I_arel=0
      I_upel=0
      I_opel=0
      I_imel=0
      I_emel=0
      I_amel=0
      I_ulel=0
      I_olel=0
      I_axuk=0
      I_uvuk=0
      I_ovuk=0
      I_ivuk=0
      I_evuk=0
      I_axal=0
      I_uval=0
      I_oval=0
      I_ival=0
      I_eval=0
      I_atuk=0
      I_usuk=0
      I_osuk=0
      I_isuk=0
      I_esuk=0
      I_atal=0
      I_usal=0
      I_osal=0
      I_isal=0
      I_esal=0
      I_aruk=0
      I_upuk=0
      I_opuk=0
      I_ipuk=0
      I_epuk=0
      I_aral=0
      I_upal=0
      I_opal=0
      I_ipal=0
      I_epal=0
      I_amuk=0
      I_uluk=0
      I_oluk=0
      I_iluk=0
      I_eluk=0
      I_amal=0
      I_ulal=0
      I_olal=0
      I_ilal=0
      I_elal=0
      I_akuk=0
      I_ufuk=0
      I_ofuk=0
      I_ifuk=0
      I_efuk=0
      I_akal=0
      I_ufal=0
      I_ofal=0
      I_ifal=0
      I_efal=0
      I_aduk=0
      I_ubuk=0
      I_obuk=0
      I_ibuk=0
      I_ebuk=0
      I_adal=0
      I_ubal=0
      I_obal=0
      I_ibal=0
      I_ebal=0
      R_esel=0
      R_isel=0
      R_asel=0
      R_urel=0
      R_orel=0
      R_ipel=0
      R_epel=0
      R_apel=0
      R_umel=0
      R_omel=0
      R_abal=0
      R_uxuk=0
      R_oxuk=0
      R_ixuk=0
      R_exuk=0
      R_abel=0
      R_uxal=0
      R_oxal=0
      R_ixal=0
      R_exal=0
      R_avuk=0
      R_utuk=0
      R_otuk=0
      R_ituk=0
      R_etuk=0
      R_aval=0
      R_utal=0
      R_otal=0
      R_ital=0
      R_etal=0
      R_asuk=0
      R_uruk=0
      R_oruk=0
      R_iruk=0
      R_eruk=0
      R_asal=0
      R_ural=0
      R_oral=0
      R_iral=0
      R_eral=0
      R_apuk=0
      R_umuk=0
      R_omuk=0
      R_imuk=0
      R_emuk=0
      R_apal=0
      R_umal=0
      R_omal=0
      R_imal=0
      R_emal=0
      R_aluk=0
      R_ukuk=0
      R_okuk=0
      R_ikuk=0
      R_ekuk=0
      R_alal=0
      R_ukal=0
      R_okal=0
      R_ikal=0
      R_ekal=0
      R_afuk=0
      R_uduk=0
      R_oduk=0
      R_iduk=0
      R_eduk=0
      R_afal=0
      R_udal=0
      R_odal=0
      R_idal=0
      R_edal=0
      I_utel=0
      R_avel=0
      I_ovel=0
      R_uvel=0
      I_itel=0
      R_otel=0
      I_evel=0
      R_ivel=0
      I_atel=0
      R_etel=0
      I_alel=0
      R_ilel=0
      I_ukel=0
      R_elel=0
      I_osel=0
      R_usel=0
      I_efel=0
      R_okel=0
      I_afel=0
      R_ikel=0
      I_udel=0
      R_ekel=0
      I_odel=0
      R_akel=0
      I_idel=0
      R_ufel=0
      I_edel=0
      R_ofel=0
      I_adel=0
      R_ifel=0
      I_obel=0
      R_ubel=0
      I_ebel=0
      R_ibel=0
      I_axel=0
      R_exel=0
C FDA_10boats.fgi( 140, 230):pre: ���������� ���������� ������� �� ������� FDA60,BOAT05
      I_(5)=I_ed !CopyBack
C FDA_10boats.fgi( 139, 276):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT05:CR
      I_erel=0
      I_irel=0
      I_arel=0
      I_upel=0
      I_opel=0
      I_imel=0
      I_emel=0
      I_amel=0
      I_ulel=0
      I_olel=0
      I_axuk=0
      I_uvuk=0
      I_ovuk=0
      I_ivuk=0
      I_evuk=0
      I_axal=0
      I_uval=0
      I_oval=0
      I_ival=0
      I_eval=0
      I_atuk=0
      I_usuk=0
      I_osuk=0
      I_isuk=0
      I_esuk=0
      I_atal=0
      I_usal=0
      I_osal=0
      I_isal=0
      I_esal=0
      I_aruk=0
      I_upuk=0
      I_opuk=0
      I_ipuk=0
      I_epuk=0
      I_aral=0
      I_upal=0
      I_opal=0
      I_ipal=0
      I_epal=0
      I_amuk=0
      I_uluk=0
      I_oluk=0
      I_iluk=0
      I_eluk=0
      I_amal=0
      I_ulal=0
      I_olal=0
      I_ilal=0
      I_elal=0
      I_akuk=0
      I_ufuk=0
      I_ofuk=0
      I_ifuk=0
      I_efuk=0
      I_akal=0
      I_ufal=0
      I_ofal=0
      I_ifal=0
      I_efal=0
      I_aduk=0
      I_ubuk=0
      I_obuk=0
      I_ibuk=0
      I_ebuk=0
      I_adal=0
      I_ubal=0
      I_obal=0
      I_ibal=0
      I_ebal=0
      R_esel=0
      R_isel=0
      R_asel=0
      R_urel=0
      R_orel=0
      R_ipel=0
      R_epel=0
      R_apel=0
      R_umel=0
      R_omel=0
      R_abal=0
      R_uxuk=0
      R_oxuk=0
      R_ixuk=0
      R_exuk=0
      R_abel=0
      R_uxal=0
      R_oxal=0
      R_ixal=0
      R_exal=0
      R_avuk=0
      R_utuk=0
      R_otuk=0
      R_ituk=0
      R_etuk=0
      R_aval=0
      R_utal=0
      R_otal=0
      R_ital=0
      R_etal=0
      R_asuk=0
      R_uruk=0
      R_oruk=0
      R_iruk=0
      R_eruk=0
      R_asal=0
      R_ural=0
      R_oral=0
      R_iral=0
      R_eral=0
      R_apuk=0
      R_umuk=0
      R_omuk=0
      R_imuk=0
      R_emuk=0
      R_apal=0
      R_umal=0
      R_omal=0
      R_imal=0
      R_emal=0
      R_aluk=0
      R_ukuk=0
      R_okuk=0
      R_ikuk=0
      R_ekuk=0
      R_alal=0
      R_ukal=0
      R_okal=0
      R_ikal=0
      R_ekal=0
      R_afuk=0
      R_uduk=0
      R_oduk=0
      R_iduk=0
      R_eduk=0
      R_afal=0
      R_udal=0
      R_odal=0
      R_idal=0
      R_edal=0
      I_utel=0
      R_avel=0
      I_ovel=0
      R_uvel=0
      I_itel=0
      R_otel=0
      I_evel=0
      R_ivel=0
      I_atel=0
      R_etel=0
      I_alel=0
      R_ilel=0
      I_ukel=0
      R_elel=0
      I_osel=0
      R_usel=0
      I_efel=0
      R_okel=0
      I_afel=0
      R_ikel=0
      I_udel=0
      R_ekel=0
      I_odel=0
      R_akel=0
      I_idel=0
      R_ufel=0
      I_edel=0
      R_ofel=0
      I_adel=0
      R_ifel=0
      I_obel=0
      R_ubel=0
      I_ebel=0
      R_ibel=0
      I_axel=0
      R_exel=0
C FDA_10boats.fgi( 160, 230):pre: ���������� ���������� ������� �� ������� FDA60,BOAT04
      I_(4)=I_id !CopyBack
C FDA_10boats.fgi( 159, 276):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT04:CR
      I_erel=0
      I_irel=0
      I_arel=0
      I_upel=0
      I_opel=0
      I_imel=0
      I_emel=0
      I_amel=0
      I_ulel=0
      I_olel=0
      I_axuk=0
      I_uvuk=0
      I_ovuk=0
      I_ivuk=0
      I_evuk=0
      I_axal=0
      I_uval=0
      I_oval=0
      I_ival=0
      I_eval=0
      I_atuk=0
      I_usuk=0
      I_osuk=0
      I_isuk=0
      I_esuk=0
      I_atal=0
      I_usal=0
      I_osal=0
      I_isal=0
      I_esal=0
      I_aruk=0
      I_upuk=0
      I_opuk=0
      I_ipuk=0
      I_epuk=0
      I_aral=0
      I_upal=0
      I_opal=0
      I_ipal=0
      I_epal=0
      I_amuk=0
      I_uluk=0
      I_oluk=0
      I_iluk=0
      I_eluk=0
      I_amal=0
      I_ulal=0
      I_olal=0
      I_ilal=0
      I_elal=0
      I_akuk=0
      I_ufuk=0
      I_ofuk=0
      I_ifuk=0
      I_efuk=0
      I_akal=0
      I_ufal=0
      I_ofal=0
      I_ifal=0
      I_efal=0
      I_aduk=0
      I_ubuk=0
      I_obuk=0
      I_ibuk=0
      I_ebuk=0
      I_adal=0
      I_ubal=0
      I_obal=0
      I_ibal=0
      I_ebal=0
      R_esel=0
      R_isel=0
      R_asel=0
      R_urel=0
      R_orel=0
      R_ipel=0
      R_epel=0
      R_apel=0
      R_umel=0
      R_omel=0
      R_abal=0
      R_uxuk=0
      R_oxuk=0
      R_ixuk=0
      R_exuk=0
      R_abel=0
      R_uxal=0
      R_oxal=0
      R_ixal=0
      R_exal=0
      R_avuk=0
      R_utuk=0
      R_otuk=0
      R_ituk=0
      R_etuk=0
      R_aval=0
      R_utal=0
      R_otal=0
      R_ital=0
      R_etal=0
      R_asuk=0
      R_uruk=0
      R_oruk=0
      R_iruk=0
      R_eruk=0
      R_asal=0
      R_ural=0
      R_oral=0
      R_iral=0
      R_eral=0
      R_apuk=0
      R_umuk=0
      R_omuk=0
      R_imuk=0
      R_emuk=0
      R_apal=0
      R_umal=0
      R_omal=0
      R_imal=0
      R_emal=0
      R_aluk=0
      R_ukuk=0
      R_okuk=0
      R_ikuk=0
      R_ekuk=0
      R_alal=0
      R_ukal=0
      R_okal=0
      R_ikal=0
      R_ekal=0
      R_afuk=0
      R_uduk=0
      R_oduk=0
      R_iduk=0
      R_eduk=0
      R_afal=0
      R_udal=0
      R_odal=0
      R_idal=0
      R_edal=0
      I_utel=0
      R_avel=0
      I_ovel=0
      R_uvel=0
      I_itel=0
      R_otel=0
      I_evel=0
      R_ivel=0
      I_atel=0
      R_etel=0
      I_alel=0
      R_ilel=0
      I_ukel=0
      R_elel=0
      I_osel=0
      R_usel=0
      I_efel=0
      R_okel=0
      I_afel=0
      R_ikel=0
      I_udel=0
      R_ekel=0
      I_odel=0
      R_akel=0
      I_idel=0
      R_ufel=0
      I_edel=0
      R_ofel=0
      I_adel=0
      R_ifel=0
      I_obel=0
      R_ubel=0
      I_ebel=0
      R_ibel=0
      I_axel=0
      R_exel=0
C FDA_10boats.fgi( 180, 230):pre: ���������� ���������� ������� �� ������� FDA60,BOAT03
      I_(3)=I_od !CopyBack
C FDA_10boats.fgi( 179, 276):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT03:CR
      I_erel=0
      I_irel=0
      I_arel=0
      I_upel=0
      I_opel=0
      I_imel=0
      I_emel=0
      I_amel=0
      I_ulel=0
      I_olel=0
      I_axuk=0
      I_uvuk=0
      I_ovuk=0
      I_ivuk=0
      I_evuk=0
      I_axal=0
      I_uval=0
      I_oval=0
      I_ival=0
      I_eval=0
      I_atuk=0
      I_usuk=0
      I_osuk=0
      I_isuk=0
      I_esuk=0
      I_atal=0
      I_usal=0
      I_osal=0
      I_isal=0
      I_esal=0
      I_aruk=0
      I_upuk=0
      I_opuk=0
      I_ipuk=0
      I_epuk=0
      I_aral=0
      I_upal=0
      I_opal=0
      I_ipal=0
      I_epal=0
      I_amuk=0
      I_uluk=0
      I_oluk=0
      I_iluk=0
      I_eluk=0
      I_amal=0
      I_ulal=0
      I_olal=0
      I_ilal=0
      I_elal=0
      I_akuk=0
      I_ufuk=0
      I_ofuk=0
      I_ifuk=0
      I_efuk=0
      I_akal=0
      I_ufal=0
      I_ofal=0
      I_ifal=0
      I_efal=0
      I_aduk=0
      I_ubuk=0
      I_obuk=0
      I_ibuk=0
      I_ebuk=0
      I_adal=0
      I_ubal=0
      I_obal=0
      I_ibal=0
      I_ebal=0
      R_esel=0
      R_isel=0
      R_asel=0
      R_urel=0
      R_orel=0
      R_ipel=0
      R_epel=0
      R_apel=0
      R_umel=0
      R_omel=0
      R_abal=0
      R_uxuk=0
      R_oxuk=0
      R_ixuk=0
      R_exuk=0
      R_abel=0
      R_uxal=0
      R_oxal=0
      R_ixal=0
      R_exal=0
      R_avuk=0
      R_utuk=0
      R_otuk=0
      R_ituk=0
      R_etuk=0
      R_aval=0
      R_utal=0
      R_otal=0
      R_ital=0
      R_etal=0
      R_asuk=0
      R_uruk=0
      R_oruk=0
      R_iruk=0
      R_eruk=0
      R_asal=0
      R_ural=0
      R_oral=0
      R_iral=0
      R_eral=0
      R_apuk=0
      R_umuk=0
      R_omuk=0
      R_imuk=0
      R_emuk=0
      R_apal=0
      R_umal=0
      R_omal=0
      R_imal=0
      R_emal=0
      R_aluk=0
      R_ukuk=0
      R_okuk=0
      R_ikuk=0
      R_ekuk=0
      R_alal=0
      R_ukal=0
      R_okal=0
      R_ikal=0
      R_ekal=0
      R_afuk=0
      R_uduk=0
      R_oduk=0
      R_iduk=0
      R_eduk=0
      R_afal=0
      R_udal=0
      R_odal=0
      R_idal=0
      R_edal=0
      I_utel=0
      R_avel=0
      I_ovel=0
      R_uvel=0
      I_itel=0
      R_otel=0
      I_evel=0
      R_ivel=0
      I_atel=0
      R_etel=0
      I_alel=0
      R_ilel=0
      I_ukel=0
      R_elel=0
      I_osel=0
      R_usel=0
      I_efel=0
      R_okel=0
      I_afel=0
      R_ikel=0
      I_udel=0
      R_ekel=0
      I_odel=0
      R_akel=0
      I_idel=0
      R_ufel=0
      I_edel=0
      R_ofel=0
      I_adel=0
      R_ifel=0
      I_obel=0
      R_ubel=0
      I_ebel=0
      R_ibel=0
      I_axel=0
      R_exel=0
C FDA_10boats.fgi( 200, 230):pre: ���������� ���������� ������� �� ������� FDA60,BOAT02
      I_(2)=I_ud !CopyBack
C FDA_10boats.fgi( 199, 276):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT02:CR
      I_erel=0
      I_irel=0
      I_arel=0
      I_upel=0
      I_opel=0
      I_imel=0
      I_emel=0
      I_amel=0
      I_ulel=0
      I_olel=0
      I_axuk=0
      I_uvuk=0
      I_ovuk=0
      I_ivuk=0
      I_evuk=0
      I_axal=0
      I_uval=0
      I_oval=0
      I_ival=0
      I_eval=0
      I_atuk=0
      I_usuk=0
      I_osuk=0
      I_isuk=0
      I_esuk=0
      I_atal=0
      I_usal=0
      I_osal=0
      I_isal=0
      I_esal=0
      I_aruk=0
      I_upuk=0
      I_opuk=0
      I_ipuk=0
      I_epuk=0
      I_aral=0
      I_upal=0
      I_opal=0
      I_ipal=0
      I_epal=0
      I_amuk=0
      I_uluk=0
      I_oluk=0
      I_iluk=0
      I_eluk=0
      I_amal=0
      I_ulal=0
      I_olal=0
      I_ilal=0
      I_elal=0
      I_akuk=0
      I_ufuk=0
      I_ofuk=0
      I_ifuk=0
      I_efuk=0
      I_akal=0
      I_ufal=0
      I_ofal=0
      I_ifal=0
      I_efal=0
      I_aduk=0
      I_ubuk=0
      I_obuk=0
      I_ibuk=0
      I_ebuk=0
      I_adal=0
      I_ubal=0
      I_obal=0
      I_ibal=0
      I_ebal=0
      R_esel=0
      R_isel=0
      R_asel=0
      R_urel=0
      R_orel=0
      R_ipel=0
      R_epel=0
      R_apel=0
      R_umel=0
      R_omel=0
      R_abal=0
      R_uxuk=0
      R_oxuk=0
      R_ixuk=0
      R_exuk=0
      R_abel=0
      R_uxal=0
      R_oxal=0
      R_ixal=0
      R_exal=0
      R_avuk=0
      R_utuk=0
      R_otuk=0
      R_ituk=0
      R_etuk=0
      R_aval=0
      R_utal=0
      R_otal=0
      R_ital=0
      R_etal=0
      R_asuk=0
      R_uruk=0
      R_oruk=0
      R_iruk=0
      R_eruk=0
      R_asal=0
      R_ural=0
      R_oral=0
      R_iral=0
      R_eral=0
      R_apuk=0
      R_umuk=0
      R_omuk=0
      R_imuk=0
      R_emuk=0
      R_apal=0
      R_umal=0
      R_omal=0
      R_imal=0
      R_emal=0
      R_aluk=0
      R_ukuk=0
      R_okuk=0
      R_ikuk=0
      R_ekuk=0
      R_alal=0
      R_ukal=0
      R_okal=0
      R_ikal=0
      R_ekal=0
      R_afuk=0
      R_uduk=0
      R_oduk=0
      R_iduk=0
      R_eduk=0
      R_afal=0
      R_udal=0
      R_odal=0
      R_idal=0
      R_edal=0
      I_utel=0
      R_avel=0
      I_ovel=0
      R_uvel=0
      I_itel=0
      R_otel=0
      I_evel=0
      R_ivel=0
      I_atel=0
      R_etel=0
      I_alel=0
      R_ilel=0
      I_ukel=0
      R_elel=0
      I_osel=0
      R_usel=0
      I_efel=0
      R_okel=0
      I_afel=0
      R_ikel=0
      I_udel=0
      R_ekel=0
      I_odel=0
      R_akel=0
      I_idel=0
      R_ufel=0
      I_edel=0
      R_ofel=0
      I_adel=0
      R_ifel=0
      I_obel=0
      R_ubel=0
      I_ebel=0
      R_ibel=0
      I_axel=0
      R_exel=0
C FDA_10boats.fgi( 220, 230):pre: ���������� ���������� ������� �� ������� FDA60,BOAT01
      I_(1)=I_af !CopyBack
C FDA_10boats.fgi( 219, 276):pre: ������-�������: ���������� ��� �������������� ������,cpy BOAT01:CR
      I_obor=z'1000021'
      I_ivir=z'1000021'
      I_esir=z'1000021'
      I_apir=z'1000021'
      I_ukir=z'1000021'
      I_odir=z'1000021'
      I_ixer=z'1000021'
      I_eter=z'1000021'
      I_user=z'1000021'
      I_ibor=z'1000021'
      I_ebor=z'1000021'
      I_abor=z'1000021'
      I_uxir=z'1000021'
      I_oxir=z'1000021'
      I_ixir=z'1000021'
      I_exir=z'1000021'
      I_axir=z'1000021'
      I_uvir=z'1000021'
      I_ovir=z'1000021'
      I_evir=z'1000021'
      I_avir=z'1000021'
      I_utir=z'1000021'
      I_otir=z'1000021'
      I_itir=z'1000021'
      I_etir=z'1000021'
      I_atir=z'1000021'
      I_usir=z'1000021'
      I_osir=z'1000021'
      I_isir=z'1000021'
      I_asir=z'1000021'
      I_urir=z'1000021'
      I_orir=z'1000021'
      I_irir=z'1000021'
      I_erir=z'1000021'
      I_arir=z'1000021'
      I_upir=z'1000021'
      I_opir=z'1000021'
      I_ipir=z'1000021'
      I_epir=z'1000021'
      I_umir=z'1000021'
      I_omir=z'1000021'
      I_imir=z'1000021'
      I_emir=z'1000021'
      I_amir=z'1000021'
      I_ulir=z'1000021'
      I_olir=z'1000021'
      I_ilir=z'1000021'
      I_elir=z'1000021'
      I_alir=z'1000021'
      I_okir=z'1000021'
      I_ikir=z'1000021'
      I_ekir=z'1000021'
      I_akir=z'1000021'
      I_ufir=z'1000021'
      I_ofir=z'1000021'
      I_ifir=z'1000021'
      I_efir=z'1000021'
      I_afir=z'1000021'
      I_udir=z'1000021'
      I_idir=z'1000021'
      I_edir=z'1000021'
      I_adir=z'1000021'
      I_ubir=z'1000021'
      I_obir=z'1000021'
      I_ibir=z'1000021'
      I_ebir=z'1000021'
      I_abir=z'1000021'
      I_uxer=z'1000021'
      I_oxer=z'1000021'
      I_exer=z'1000021'
      I_axer=z'1000021'
      I_uver=z'1000021'
      I_over=z'1000021'
      I_iver=z'1000021'
      I_ever=z'1000021'
      I_aver=z'1000021'
      I_uter=z'1000021'
      I_oter=z'1000021'
      I_iter=z'1000021'
      I_ater=z'1000021'
C FDA_10boats.fgi(  40, 244):pre: ���������� ���������� ������� �� ������� FDA50,BOAT10
      I_obor=z'1000021'
      I_ivir=z'1000021'
      I_esir=z'1000021'
      I_apir=z'1000021'
      I_ukir=z'1000021'
      I_odir=z'1000021'
      I_ixer=z'1000021'
      I_eter=z'1000021'
      I_user=z'1000021'
      I_ibor=z'1000021'
      I_ebor=z'1000021'
      I_abor=z'1000021'
      I_uxir=z'1000021'
      I_oxir=z'1000021'
      I_ixir=z'1000021'
      I_exir=z'1000021'
      I_axir=z'1000021'
      I_uvir=z'1000021'
      I_ovir=z'1000021'
      I_evir=z'1000021'
      I_avir=z'1000021'
      I_utir=z'1000021'
      I_otir=z'1000021'
      I_itir=z'1000021'
      I_etir=z'1000021'
      I_atir=z'1000021'
      I_usir=z'1000021'
      I_osir=z'1000021'
      I_isir=z'1000021'
      I_asir=z'1000021'
      I_urir=z'1000021'
      I_orir=z'1000021'
      I_irir=z'1000021'
      I_erir=z'1000021'
      I_arir=z'1000021'
      I_upir=z'1000021'
      I_opir=z'1000021'
      I_ipir=z'1000021'
      I_epir=z'1000021'
      I_umir=z'1000021'
      I_omir=z'1000021'
      I_imir=z'1000021'
      I_emir=z'1000021'
      I_amir=z'1000021'
      I_ulir=z'1000021'
      I_olir=z'1000021'
      I_ilir=z'1000021'
      I_elir=z'1000021'
      I_alir=z'1000021'
      I_okir=z'1000021'
      I_ikir=z'1000021'
      I_ekir=z'1000021'
      I_akir=z'1000021'
      I_ufir=z'1000021'
      I_ofir=z'1000021'
      I_ifir=z'1000021'
      I_efir=z'1000021'
      I_afir=z'1000021'
      I_udir=z'1000021'
      I_idir=z'1000021'
      I_edir=z'1000021'
      I_adir=z'1000021'
      I_ubir=z'1000021'
      I_obir=z'1000021'
      I_ibir=z'1000021'
      I_ebir=z'1000021'
      I_abir=z'1000021'
      I_uxer=z'1000021'
      I_oxer=z'1000021'
      I_exer=z'1000021'
      I_axer=z'1000021'
      I_uver=z'1000021'
      I_over=z'1000021'
      I_iver=z'1000021'
      I_ever=z'1000021'
      I_aver=z'1000021'
      I_uter=z'1000021'
      I_oter=z'1000021'
      I_iter=z'1000021'
      I_ater=z'1000021'
C FDA_10boats.fgi(  60, 244):pre: ���������� ���������� ������� �� ������� FDA50,BOAT09
      I_obor=z'1000021'
      I_ivir=z'1000021'
      I_esir=z'1000021'
      I_apir=z'1000021'
      I_ukir=z'1000021'
      I_odir=z'1000021'
      I_ixer=z'1000021'
      I_eter=z'1000021'
      I_user=z'1000021'
      I_ibor=z'1000021'
      I_ebor=z'1000021'
      I_abor=z'1000021'
      I_uxir=z'1000021'
      I_oxir=z'1000021'
      I_ixir=z'1000021'
      I_exir=z'1000021'
      I_axir=z'1000021'
      I_uvir=z'1000021'
      I_ovir=z'1000021'
      I_evir=z'1000021'
      I_avir=z'1000021'
      I_utir=z'1000021'
      I_otir=z'1000021'
      I_itir=z'1000021'
      I_etir=z'1000021'
      I_atir=z'1000021'
      I_usir=z'1000021'
      I_osir=z'1000021'
      I_isir=z'1000021'
      I_asir=z'1000021'
      I_urir=z'1000021'
      I_orir=z'1000021'
      I_irir=z'1000021'
      I_erir=z'1000021'
      I_arir=z'1000021'
      I_upir=z'1000021'
      I_opir=z'1000021'
      I_ipir=z'1000021'
      I_epir=z'1000021'
      I_umir=z'1000021'
      I_omir=z'1000021'
      I_imir=z'1000021'
      I_emir=z'1000021'
      I_amir=z'1000021'
      I_ulir=z'1000021'
      I_olir=z'1000021'
      I_ilir=z'1000021'
      I_elir=z'1000021'
      I_alir=z'1000021'
      I_okir=z'1000021'
      I_ikir=z'1000021'
      I_ekir=z'1000021'
      I_akir=z'1000021'
      I_ufir=z'1000021'
      I_ofir=z'1000021'
      I_ifir=z'1000021'
      I_efir=z'1000021'
      I_afir=z'1000021'
      I_udir=z'1000021'
      I_idir=z'1000021'
      I_edir=z'1000021'
      I_adir=z'1000021'
      I_ubir=z'1000021'
      I_obir=z'1000021'
      I_ibir=z'1000021'
      I_ebir=z'1000021'
      I_abir=z'1000021'
      I_uxer=z'1000021'
      I_oxer=z'1000021'
      I_exer=z'1000021'
      I_axer=z'1000021'
      I_uver=z'1000021'
      I_over=z'1000021'
      I_iver=z'1000021'
      I_ever=z'1000021'
      I_aver=z'1000021'
      I_uter=z'1000021'
      I_oter=z'1000021'
      I_iter=z'1000021'
      I_ater=z'1000021'
C FDA_10boats.fgi(  80, 244):pre: ���������� ���������� ������� �� ������� FDA50,BOAT08
      I_obor=z'1000021'
      I_ivir=z'1000021'
      I_esir=z'1000021'
      I_apir=z'1000021'
      I_ukir=z'1000021'
      I_odir=z'1000021'
      I_ixer=z'1000021'
      I_eter=z'1000021'
      I_user=z'1000021'
      I_ibor=z'1000021'
      I_ebor=z'1000021'
      I_abor=z'1000021'
      I_uxir=z'1000021'
      I_oxir=z'1000021'
      I_ixir=z'1000021'
      I_exir=z'1000021'
      I_axir=z'1000021'
      I_uvir=z'1000021'
      I_ovir=z'1000021'
      I_evir=z'1000021'
      I_avir=z'1000021'
      I_utir=z'1000021'
      I_otir=z'1000021'
      I_itir=z'1000021'
      I_etir=z'1000021'
      I_atir=z'1000021'
      I_usir=z'1000021'
      I_osir=z'1000021'
      I_isir=z'1000021'
      I_asir=z'1000021'
      I_urir=z'1000021'
      I_orir=z'1000021'
      I_irir=z'1000021'
      I_erir=z'1000021'
      I_arir=z'1000021'
      I_upir=z'1000021'
      I_opir=z'1000021'
      I_ipir=z'1000021'
      I_epir=z'1000021'
      I_umir=z'1000021'
      I_omir=z'1000021'
      I_imir=z'1000021'
      I_emir=z'1000021'
      I_amir=z'1000021'
      I_ulir=z'1000021'
      I_olir=z'1000021'
      I_ilir=z'1000021'
      I_elir=z'1000021'
      I_alir=z'1000021'
      I_okir=z'1000021'
      I_ikir=z'1000021'
      I_ekir=z'1000021'
      I_akir=z'1000021'
      I_ufir=z'1000021'
      I_ofir=z'1000021'
      I_ifir=z'1000021'
      I_efir=z'1000021'
      I_afir=z'1000021'
      I_udir=z'1000021'
      I_idir=z'1000021'
      I_edir=z'1000021'
      I_adir=z'1000021'
      I_ubir=z'1000021'
      I_obir=z'1000021'
      I_ibir=z'1000021'
      I_ebir=z'1000021'
      I_abir=z'1000021'
      I_uxer=z'1000021'
      I_oxer=z'1000021'
      I_exer=z'1000021'
      I_axer=z'1000021'
      I_uver=z'1000021'
      I_over=z'1000021'
      I_iver=z'1000021'
      I_ever=z'1000021'
      I_aver=z'1000021'
      I_uter=z'1000021'
      I_oter=z'1000021'
      I_iter=z'1000021'
      I_ater=z'1000021'
C FDA_10boats.fgi( 100, 244):pre: ���������� ���������� ������� �� ������� FDA50,BOAT07
      I_obor=z'1000021'
      I_ivir=z'1000021'
      I_esir=z'1000021'
      I_apir=z'1000021'
      I_ukir=z'1000021'
      I_odir=z'1000021'
      I_ixer=z'1000021'
      I_eter=z'1000021'
      I_user=z'1000021'
      I_ibor=z'1000021'
      I_ebor=z'1000021'
      I_abor=z'1000021'
      I_uxir=z'1000021'
      I_oxir=z'1000021'
      I_ixir=z'1000021'
      I_exir=z'1000021'
      I_axir=z'1000021'
      I_uvir=z'1000021'
      I_ovir=z'1000021'
      I_evir=z'1000021'
      I_avir=z'1000021'
      I_utir=z'1000021'
      I_otir=z'1000021'
      I_itir=z'1000021'
      I_etir=z'1000021'
      I_atir=z'1000021'
      I_usir=z'1000021'
      I_osir=z'1000021'
      I_isir=z'1000021'
      I_asir=z'1000021'
      I_urir=z'1000021'
      I_orir=z'1000021'
      I_irir=z'1000021'
      I_erir=z'1000021'
      I_arir=z'1000021'
      I_upir=z'1000021'
      I_opir=z'1000021'
      I_ipir=z'1000021'
      I_epir=z'1000021'
      I_umir=z'1000021'
      I_omir=z'1000021'
      I_imir=z'1000021'
      I_emir=z'1000021'
      I_amir=z'1000021'
      I_ulir=z'1000021'
      I_olir=z'1000021'
      I_ilir=z'1000021'
      I_elir=z'1000021'
      I_alir=z'1000021'
      I_okir=z'1000021'
      I_ikir=z'1000021'
      I_ekir=z'1000021'
      I_akir=z'1000021'
      I_ufir=z'1000021'
      I_ofir=z'1000021'
      I_ifir=z'1000021'
      I_efir=z'1000021'
      I_afir=z'1000021'
      I_udir=z'1000021'
      I_idir=z'1000021'
      I_edir=z'1000021'
      I_adir=z'1000021'
      I_ubir=z'1000021'
      I_obir=z'1000021'
      I_ibir=z'1000021'
      I_ebir=z'1000021'
      I_abir=z'1000021'
      I_uxer=z'1000021'
      I_oxer=z'1000021'
      I_exer=z'1000021'
      I_axer=z'1000021'
      I_uver=z'1000021'
      I_over=z'1000021'
      I_iver=z'1000021'
      I_ever=z'1000021'
      I_aver=z'1000021'
      I_uter=z'1000021'
      I_oter=z'1000021'
      I_iter=z'1000021'
      I_ater=z'1000021'
C FDA_10boats.fgi( 120, 244):pre: ���������� ���������� ������� �� ������� FDA50,BOAT06
      I_obor=z'1000021'
      I_ivir=z'1000021'
      I_esir=z'1000021'
      I_apir=z'1000021'
      I_ukir=z'1000021'
      I_odir=z'1000021'
      I_ixer=z'1000021'
      I_eter=z'1000021'
      I_user=z'1000021'
      I_ibor=z'1000021'
      I_ebor=z'1000021'
      I_abor=z'1000021'
      I_uxir=z'1000021'
      I_oxir=z'1000021'
      I_ixir=z'1000021'
      I_exir=z'1000021'
      I_axir=z'1000021'
      I_uvir=z'1000021'
      I_ovir=z'1000021'
      I_evir=z'1000021'
      I_avir=z'1000021'
      I_utir=z'1000021'
      I_otir=z'1000021'
      I_itir=z'1000021'
      I_etir=z'1000021'
      I_atir=z'1000021'
      I_usir=z'1000021'
      I_osir=z'1000021'
      I_isir=z'1000021'
      I_asir=z'1000021'
      I_urir=z'1000021'
      I_orir=z'1000021'
      I_irir=z'1000021'
      I_erir=z'1000021'
      I_arir=z'1000021'
      I_upir=z'1000021'
      I_opir=z'1000021'
      I_ipir=z'1000021'
      I_epir=z'1000021'
      I_umir=z'1000021'
      I_omir=z'1000021'
      I_imir=z'1000021'
      I_emir=z'1000021'
      I_amir=z'1000021'
      I_ulir=z'1000021'
      I_olir=z'1000021'
      I_ilir=z'1000021'
      I_elir=z'1000021'
      I_alir=z'1000021'
      I_okir=z'1000021'
      I_ikir=z'1000021'
      I_ekir=z'1000021'
      I_akir=z'1000021'
      I_ufir=z'1000021'
      I_ofir=z'1000021'
      I_ifir=z'1000021'
      I_efir=z'1000021'
      I_afir=z'1000021'
      I_udir=z'1000021'
      I_idir=z'1000021'
      I_edir=z'1000021'
      I_adir=z'1000021'
      I_ubir=z'1000021'
      I_obir=z'1000021'
      I_ibir=z'1000021'
      I_ebir=z'1000021'
      I_abir=z'1000021'
      I_uxer=z'1000021'
      I_oxer=z'1000021'
      I_exer=z'1000021'
      I_axer=z'1000021'
      I_uver=z'1000021'
      I_over=z'1000021'
      I_iver=z'1000021'
      I_ever=z'1000021'
      I_aver=z'1000021'
      I_uter=z'1000021'
      I_oter=z'1000021'
      I_iter=z'1000021'
      I_ater=z'1000021'
C FDA_10boats.fgi( 140, 244):pre: ���������� ���������� ������� �� ������� FDA50,BOAT05
      I_obor=z'1000021'
      I_ivir=z'1000021'
      I_esir=z'1000021'
      I_apir=z'1000021'
      I_ukir=z'1000021'
      I_odir=z'1000021'
      I_ixer=z'1000021'
      I_eter=z'1000021'
      I_user=z'1000021'
      I_ibor=z'1000021'
      I_ebor=z'1000021'
      I_abor=z'1000021'
      I_uxir=z'1000021'
      I_oxir=z'1000021'
      I_ixir=z'1000021'
      I_exir=z'1000021'
      I_axir=z'1000021'
      I_uvir=z'1000021'
      I_ovir=z'1000021'
      I_evir=z'1000021'
      I_avir=z'1000021'
      I_utir=z'1000021'
      I_otir=z'1000021'
      I_itir=z'1000021'
      I_etir=z'1000021'
      I_atir=z'1000021'
      I_usir=z'1000021'
      I_osir=z'1000021'
      I_isir=z'1000021'
      I_asir=z'1000021'
      I_urir=z'1000021'
      I_orir=z'1000021'
      I_irir=z'1000021'
      I_erir=z'1000021'
      I_arir=z'1000021'
      I_upir=z'1000021'
      I_opir=z'1000021'
      I_ipir=z'1000021'
      I_epir=z'1000021'
      I_umir=z'1000021'
      I_omir=z'1000021'
      I_imir=z'1000021'
      I_emir=z'1000021'
      I_amir=z'1000021'
      I_ulir=z'1000021'
      I_olir=z'1000021'
      I_ilir=z'1000021'
      I_elir=z'1000021'
      I_alir=z'1000021'
      I_okir=z'1000021'
      I_ikir=z'1000021'
      I_ekir=z'1000021'
      I_akir=z'1000021'
      I_ufir=z'1000021'
      I_ofir=z'1000021'
      I_ifir=z'1000021'
      I_efir=z'1000021'
      I_afir=z'1000021'
      I_udir=z'1000021'
      I_idir=z'1000021'
      I_edir=z'1000021'
      I_adir=z'1000021'
      I_ubir=z'1000021'
      I_obir=z'1000021'
      I_ibir=z'1000021'
      I_ebir=z'1000021'
      I_abir=z'1000021'
      I_uxer=z'1000021'
      I_oxer=z'1000021'
      I_exer=z'1000021'
      I_axer=z'1000021'
      I_uver=z'1000021'
      I_over=z'1000021'
      I_iver=z'1000021'
      I_ever=z'1000021'
      I_aver=z'1000021'
      I_uter=z'1000021'
      I_oter=z'1000021'
      I_iter=z'1000021'
      I_ater=z'1000021'
C FDA_10boats.fgi( 160, 244):pre: ���������� ���������� ������� �� ������� FDA50,BOAT04
      I_obor=z'1000021'
      I_ivir=z'1000021'
      I_esir=z'1000021'
      I_apir=z'1000021'
      I_ukir=z'1000021'
      I_odir=z'1000021'
      I_ixer=z'1000021'
      I_eter=z'1000021'
      I_user=z'1000021'
      I_ibor=z'1000021'
      I_ebor=z'1000021'
      I_abor=z'1000021'
      I_uxir=z'1000021'
      I_oxir=z'1000021'
      I_ixir=z'1000021'
      I_exir=z'1000021'
      I_axir=z'1000021'
      I_uvir=z'1000021'
      I_ovir=z'1000021'
      I_evir=z'1000021'
      I_avir=z'1000021'
      I_utir=z'1000021'
      I_otir=z'1000021'
      I_itir=z'1000021'
      I_etir=z'1000021'
      I_atir=z'1000021'
      I_usir=z'1000021'
      I_osir=z'1000021'
      I_isir=z'1000021'
      I_asir=z'1000021'
      I_urir=z'1000021'
      I_orir=z'1000021'
      I_irir=z'1000021'
      I_erir=z'1000021'
      I_arir=z'1000021'
      I_upir=z'1000021'
      I_opir=z'1000021'
      I_ipir=z'1000021'
      I_epir=z'1000021'
      I_umir=z'1000021'
      I_omir=z'1000021'
      I_imir=z'1000021'
      I_emir=z'1000021'
      I_amir=z'1000021'
      I_ulir=z'1000021'
      I_olir=z'1000021'
      I_ilir=z'1000021'
      I_elir=z'1000021'
      I_alir=z'1000021'
      I_okir=z'1000021'
      I_ikir=z'1000021'
      I_ekir=z'1000021'
      I_akir=z'1000021'
      I_ufir=z'1000021'
      I_ofir=z'1000021'
      I_ifir=z'1000021'
      I_efir=z'1000021'
      I_afir=z'1000021'
      I_udir=z'1000021'
      I_idir=z'1000021'
      I_edir=z'1000021'
      I_adir=z'1000021'
      I_ubir=z'1000021'
      I_obir=z'1000021'
      I_ibir=z'1000021'
      I_ebir=z'1000021'
      I_abir=z'1000021'
      I_uxer=z'1000021'
      I_oxer=z'1000021'
      I_exer=z'1000021'
      I_axer=z'1000021'
      I_uver=z'1000021'
      I_over=z'1000021'
      I_iver=z'1000021'
      I_ever=z'1000021'
      I_aver=z'1000021'
      I_uter=z'1000021'
      I_oter=z'1000021'
      I_iter=z'1000021'
      I_ater=z'1000021'
C FDA_10boats.fgi( 180, 244):pre: ���������� ���������� ������� �� ������� FDA50,BOAT03
      I_obor=z'1000021'
      I_ivir=z'1000021'
      I_esir=z'1000021'
      I_apir=z'1000021'
      I_ukir=z'1000021'
      I_odir=z'1000021'
      I_ixer=z'1000021'
      I_eter=z'1000021'
      I_user=z'1000021'
      I_ibor=z'1000021'
      I_ebor=z'1000021'
      I_abor=z'1000021'
      I_uxir=z'1000021'
      I_oxir=z'1000021'
      I_ixir=z'1000021'
      I_exir=z'1000021'
      I_axir=z'1000021'
      I_uvir=z'1000021'
      I_ovir=z'1000021'
      I_evir=z'1000021'
      I_avir=z'1000021'
      I_utir=z'1000021'
      I_otir=z'1000021'
      I_itir=z'1000021'
      I_etir=z'1000021'
      I_atir=z'1000021'
      I_usir=z'1000021'
      I_osir=z'1000021'
      I_isir=z'1000021'
      I_asir=z'1000021'
      I_urir=z'1000021'
      I_orir=z'1000021'
      I_irir=z'1000021'
      I_erir=z'1000021'
      I_arir=z'1000021'
      I_upir=z'1000021'
      I_opir=z'1000021'
      I_ipir=z'1000021'
      I_epir=z'1000021'
      I_umir=z'1000021'
      I_omir=z'1000021'
      I_imir=z'1000021'
      I_emir=z'1000021'
      I_amir=z'1000021'
      I_ulir=z'1000021'
      I_olir=z'1000021'
      I_ilir=z'1000021'
      I_elir=z'1000021'
      I_alir=z'1000021'
      I_okir=z'1000021'
      I_ikir=z'1000021'
      I_ekir=z'1000021'
      I_akir=z'1000021'
      I_ufir=z'1000021'
      I_ofir=z'1000021'
      I_ifir=z'1000021'
      I_efir=z'1000021'
      I_afir=z'1000021'
      I_udir=z'1000021'
      I_idir=z'1000021'
      I_edir=z'1000021'
      I_adir=z'1000021'
      I_ubir=z'1000021'
      I_obir=z'1000021'
      I_ibir=z'1000021'
      I_ebir=z'1000021'
      I_abir=z'1000021'
      I_uxer=z'1000021'
      I_oxer=z'1000021'
      I_exer=z'1000021'
      I_axer=z'1000021'
      I_uver=z'1000021'
      I_over=z'1000021'
      I_iver=z'1000021'
      I_ever=z'1000021'
      I_aver=z'1000021'
      I_uter=z'1000021'
      I_oter=z'1000021'
      I_iter=z'1000021'
      I_ater=z'1000021'
C FDA_10boats.fgi( 200, 244):pre: ���������� ���������� ������� �� ������� FDA50,BOAT02
      I_obor=z'1000021'
      I_ivir=z'1000021'
      I_esir=z'1000021'
      I_apir=z'1000021'
      I_ukir=z'1000021'
      I_odir=z'1000021'
      I_ixer=z'1000021'
      I_eter=z'1000021'
      I_user=z'1000021'
      I_ibor=z'1000021'
      I_ebor=z'1000021'
      I_abor=z'1000021'
      I_uxir=z'1000021'
      I_oxir=z'1000021'
      I_ixir=z'1000021'
      I_exir=z'1000021'
      I_axir=z'1000021'
      I_uvir=z'1000021'
      I_ovir=z'1000021'
      I_evir=z'1000021'
      I_avir=z'1000021'
      I_utir=z'1000021'
      I_otir=z'1000021'
      I_itir=z'1000021'
      I_etir=z'1000021'
      I_atir=z'1000021'
      I_usir=z'1000021'
      I_osir=z'1000021'
      I_isir=z'1000021'
      I_asir=z'1000021'
      I_urir=z'1000021'
      I_orir=z'1000021'
      I_irir=z'1000021'
      I_erir=z'1000021'
      I_arir=z'1000021'
      I_upir=z'1000021'
      I_opir=z'1000021'
      I_ipir=z'1000021'
      I_epir=z'1000021'
      I_umir=z'1000021'
      I_omir=z'1000021'
      I_imir=z'1000021'
      I_emir=z'1000021'
      I_amir=z'1000021'
      I_ulir=z'1000021'
      I_olir=z'1000021'
      I_ilir=z'1000021'
      I_elir=z'1000021'
      I_alir=z'1000021'
      I_okir=z'1000021'
      I_ikir=z'1000021'
      I_ekir=z'1000021'
      I_akir=z'1000021'
      I_ufir=z'1000021'
      I_ofir=z'1000021'
      I_ifir=z'1000021'
      I_efir=z'1000021'
      I_afir=z'1000021'
      I_udir=z'1000021'
      I_idir=z'1000021'
      I_edir=z'1000021'
      I_adir=z'1000021'
      I_ubir=z'1000021'
      I_obir=z'1000021'
      I_ibir=z'1000021'
      I_ebir=z'1000021'
      I_abir=z'1000021'
      I_uxer=z'1000021'
      I_oxer=z'1000021'
      I_exer=z'1000021'
      I_axer=z'1000021'
      I_uver=z'1000021'
      I_over=z'1000021'
      I_iver=z'1000021'
      I_ever=z'1000021'
      I_aver=z'1000021'
      I_uter=z'1000021'
      I_oter=z'1000021'
      I_iter=z'1000021'
      I_ater=z'1000021'
C FDA_10boats.fgi( 220, 244):pre: ���������� ���������� ������� �� ������� FDA50,BOAT01
      Call LODOCHKA_HANDLER_KTS(deltat,L_et,L_it,
     & L_ot,L_ut,L_av,
     & L_ev,L_iv,L_ov,L_uv,
     & L_ax,L_epe,L_ipe,
     & L_ope,L_upe,L_are,
     & L_ere,L_ire,L_ore,L_ure,
     & L_ase,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_ep,L_ip,
     & L_op,L_up,L_ar,
     & L_er,L_ir,L_or,
     & L_ur,L_as,L_es,
     & L_is,L_os,L_us,
     & L_at,L_eke,L_ike,
     & L_oke,L_uke,L_ale,
     & L_ele,L_ile,L_ole,
     & L_ule,L_ame,L_eme,
     & L_ime,L_ome,L_ume,
     & L_ape,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ek,L_ik,
     & L_ok,L_al,L_el,L_em,
     & L_im,L_om,L_um,L_ap,
     & L_ef,L_if,L_of,L_uf,
     & L_ak,L_uk,L_il,L_ol,
     & L_ul,L_am,L_ebe,L_ibe,
     & L_obe,L_ade,L_ede,L_efe,
     & L_ife,L_ofe,L_ufe,L_ake,
     & L_ex,L_ix,L_ox,L_ux,
     & L_abe,L_ube,L_ide,L_ode,
     & L_ude,L_afe,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,I_(1))
C FDA_10boats.fgi( 220, 214):���������� ������� � ���� ���,BOAT01
      Call LODOCHKA_HANDLER_KTS(deltat,L_et,L_it,
     & L_ot,L_ut,L_av,
     & L_ev,L_iv,L_ov,L_uv,
     & L_ax,L_epe,L_ipe,
     & L_ope,L_upe,L_are,
     & L_ere,L_ire,L_ore,L_ure,
     & L_ase,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_ep,L_ip,
     & L_op,L_up,L_ar,
     & L_er,L_ir,L_or,
     & L_ur,L_as,L_es,
     & L_is,L_os,L_us,
     & L_at,L_eke,L_ike,
     & L_oke,L_uke,L_ale,
     & L_ele,L_ile,L_ole,
     & L_ule,L_ame,L_eme,
     & L_ime,L_ome,L_ume,
     & L_ape,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ek,L_ik,
     & L_ok,L_al,L_el,L_em,
     & L_im,L_om,L_um,L_ap,
     & L_ef,L_if,L_of,L_uf,
     & L_ak,L_uk,L_il,L_ol,
     & L_ul,L_am,L_ebe,L_ibe,
     & L_obe,L_ade,L_ede,L_efe,
     & L_ife,L_ofe,L_ufe,L_ake,
     & L_ex,L_ix,L_ox,L_ux,
     & L_abe,L_ube,L_ide,L_ode,
     & L_ude,L_afe,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,I_(2))
C FDA_10boats.fgi( 200, 214):���������� ������� � ���� ���,BOAT02
      Call LODOCHKA_HANDLER_KTS(deltat,L_et,L_it,
     & L_ot,L_ut,L_av,
     & L_ev,L_iv,L_ov,L_uv,
     & L_ax,L_epe,L_ipe,
     & L_ope,L_upe,L_are,
     & L_ere,L_ire,L_ore,L_ure,
     & L_ase,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_ep,L_ip,
     & L_op,L_up,L_ar,
     & L_er,L_ir,L_or,
     & L_ur,L_as,L_es,
     & L_is,L_os,L_us,
     & L_at,L_eke,L_ike,
     & L_oke,L_uke,L_ale,
     & L_ele,L_ile,L_ole,
     & L_ule,L_ame,L_eme,
     & L_ime,L_ome,L_ume,
     & L_ape,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ek,L_ik,
     & L_ok,L_al,L_el,L_em,
     & L_im,L_om,L_um,L_ap,
     & L_ef,L_if,L_of,L_uf,
     & L_ak,L_uk,L_il,L_ol,
     & L_ul,L_am,L_ebe,L_ibe,
     & L_obe,L_ade,L_ede,L_efe,
     & L_ife,L_ofe,L_ufe,L_ake,
     & L_ex,L_ix,L_ox,L_ux,
     & L_abe,L_ube,L_ide,L_ode,
     & L_ude,L_afe,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,I_(3))
C FDA_10boats.fgi( 180, 214):���������� ������� � ���� ���,BOAT03
      Call LODOCHKA_HANDLER_KTS(deltat,L_et,L_it,
     & L_ot,L_ut,L_av,
     & L_ev,L_iv,L_ov,L_uv,
     & L_ax,L_epe,L_ipe,
     & L_ope,L_upe,L_are,
     & L_ere,L_ire,L_ore,L_ure,
     & L_ase,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_ep,L_ip,
     & L_op,L_up,L_ar,
     & L_er,L_ir,L_or,
     & L_ur,L_as,L_es,
     & L_is,L_os,L_us,
     & L_at,L_eke,L_ike,
     & L_oke,L_uke,L_ale,
     & L_ele,L_ile,L_ole,
     & L_ule,L_ame,L_eme,
     & L_ime,L_ome,L_ume,
     & L_ape,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ek,L_ik,
     & L_ok,L_al,L_el,L_em,
     & L_im,L_om,L_um,L_ap,
     & L_ef,L_if,L_of,L_uf,
     & L_ak,L_uk,L_il,L_ol,
     & L_ul,L_am,L_ebe,L_ibe,
     & L_obe,L_ade,L_ede,L_efe,
     & L_ife,L_ofe,L_ufe,L_ake,
     & L_ex,L_ix,L_ox,L_ux,
     & L_abe,L_ube,L_ide,L_ode,
     & L_ude,L_afe,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,I_(4))
C FDA_10boats.fgi( 160, 214):���������� ������� � ���� ���,BOAT04
      Call LODOCHKA_HANDLER_KTS(deltat,L_et,L_it,
     & L_ot,L_ut,L_av,
     & L_ev,L_iv,L_ov,L_uv,
     & L_ax,L_epe,L_ipe,
     & L_ope,L_upe,L_are,
     & L_ere,L_ire,L_ore,L_ure,
     & L_ase,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_ep,L_ip,
     & L_op,L_up,L_ar,
     & L_er,L_ir,L_or,
     & L_ur,L_as,L_es,
     & L_is,L_os,L_us,
     & L_at,L_eke,L_ike,
     & L_oke,L_uke,L_ale,
     & L_ele,L_ile,L_ole,
     & L_ule,L_ame,L_eme,
     & L_ime,L_ome,L_ume,
     & L_ape,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ek,L_ik,
     & L_ok,L_al,L_el,L_em,
     & L_im,L_om,L_um,L_ap,
     & L_ef,L_if,L_of,L_uf,
     & L_ak,L_uk,L_il,L_ol,
     & L_ul,L_am,L_ebe,L_ibe,
     & L_obe,L_ade,L_ede,L_efe,
     & L_ife,L_ofe,L_ufe,L_ake,
     & L_ex,L_ix,L_ox,L_ux,
     & L_abe,L_ube,L_ide,L_ode,
     & L_ude,L_afe,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,I_(5))
C FDA_10boats.fgi( 140, 214):���������� ������� � ���� ���,BOAT05
      Call LODOCHKA_HANDLER_KTS(deltat,L_et,L_it,
     & L_ot,L_ut,L_av,
     & L_ev,L_iv,L_ov,L_uv,
     & L_ax,L_epe,L_ipe,
     & L_ope,L_upe,L_are,
     & L_ere,L_ire,L_ore,L_ure,
     & L_ase,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_ep,L_ip,
     & L_op,L_up,L_ar,
     & L_er,L_ir,L_or,
     & L_ur,L_as,L_es,
     & L_is,L_os,L_us,
     & L_at,L_eke,L_ike,
     & L_oke,L_uke,L_ale,
     & L_ele,L_ile,L_ole,
     & L_ule,L_ame,L_eme,
     & L_ime,L_ome,L_ume,
     & L_ape,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ek,L_ik,
     & L_ok,L_al,L_el,L_em,
     & L_im,L_om,L_um,L_ap,
     & L_ef,L_if,L_of,L_uf,
     & L_ak,L_uk,L_il,L_ol,
     & L_ul,L_am,L_ebe,L_ibe,
     & L_obe,L_ade,L_ede,L_efe,
     & L_ife,L_ofe,L_ufe,L_ake,
     & L_ex,L_ix,L_ox,L_ux,
     & L_abe,L_ube,L_ide,L_ode,
     & L_ude,L_afe,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,I_(6))
C FDA_10boats.fgi( 120, 214):���������� ������� � ���� ���,BOAT06
      Call LODOCHKA_HANDLER_KTS(deltat,L_et,L_it,
     & L_ot,L_ut,L_av,
     & L_ev,L_iv,L_ov,L_uv,
     & L_ax,L_epe,L_ipe,
     & L_ope,L_upe,L_are,
     & L_ere,L_ire,L_ore,L_ure,
     & L_ase,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_ep,L_ip,
     & L_op,L_up,L_ar,
     & L_er,L_ir,L_or,
     & L_ur,L_as,L_es,
     & L_is,L_os,L_us,
     & L_at,L_eke,L_ike,
     & L_oke,L_uke,L_ale,
     & L_ele,L_ile,L_ole,
     & L_ule,L_ame,L_eme,
     & L_ime,L_ome,L_ume,
     & L_ape,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ek,L_ik,
     & L_ok,L_al,L_el,L_em,
     & L_im,L_om,L_um,L_ap,
     & L_ef,L_if,L_of,L_uf,
     & L_ak,L_uk,L_il,L_ol,
     & L_ul,L_am,L_ebe,L_ibe,
     & L_obe,L_ade,L_ede,L_efe,
     & L_ife,L_ofe,L_ufe,L_ake,
     & L_ex,L_ix,L_ox,L_ux,
     & L_abe,L_ube,L_ide,L_ode,
     & L_ude,L_afe,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,I_(7))
C FDA_10boats.fgi( 100, 214):���������� ������� � ���� ���,BOAT07
      Call LODOCHKA_HANDLER_KTS(deltat,L_et,L_it,
     & L_ot,L_ut,L_av,
     & L_ev,L_iv,L_ov,L_uv,
     & L_ax,L_epe,L_ipe,
     & L_ope,L_upe,L_are,
     & L_ere,L_ire,L_ore,L_ure,
     & L_ase,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_ep,L_ip,
     & L_op,L_up,L_ar,
     & L_er,L_ir,L_or,
     & L_ur,L_as,L_es,
     & L_is,L_os,L_us,
     & L_at,L_eke,L_ike,
     & L_oke,L_uke,L_ale,
     & L_ele,L_ile,L_ole,
     & L_ule,L_ame,L_eme,
     & L_ime,L_ome,L_ume,
     & L_ape,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ek,L_ik,
     & L_ok,L_al,L_el,L_em,
     & L_im,L_om,L_um,L_ap,
     & L_ef,L_if,L_of,L_uf,
     & L_ak,L_uk,L_il,L_ol,
     & L_ul,L_am,L_ebe,L_ibe,
     & L_obe,L_ade,L_ede,L_efe,
     & L_ife,L_ofe,L_ufe,L_ake,
     & L_ex,L_ix,L_ox,L_ux,
     & L_abe,L_ube,L_ide,L_ode,
     & L_ude,L_afe,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,I_(8))
C FDA_10boats.fgi(  80, 214):���������� ������� � ���� ���,BOAT08
      Call LODOCHKA_HANDLER_KTS(deltat,L_et,L_it,
     & L_ot,L_ut,L_av,
     & L_ev,L_iv,L_ov,L_uv,
     & L_ax,L_epe,L_ipe,
     & L_ope,L_upe,L_are,
     & L_ere,L_ire,L_ore,L_ure,
     & L_ase,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_ep,L_ip,
     & L_op,L_up,L_ar,
     & L_er,L_ir,L_or,
     & L_ur,L_as,L_es,
     & L_is,L_os,L_us,
     & L_at,L_eke,L_ike,
     & L_oke,L_uke,L_ale,
     & L_ele,L_ile,L_ole,
     & L_ule,L_ame,L_eme,
     & L_ime,L_ome,L_ume,
     & L_ape,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ek,L_ik,
     & L_ok,L_al,L_el,L_em,
     & L_im,L_om,L_um,L_ap,
     & L_ef,L_if,L_of,L_uf,
     & L_ak,L_uk,L_il,L_ol,
     & L_ul,L_am,L_ebe,L_ibe,
     & L_obe,L_ade,L_ede,L_efe,
     & L_ife,L_ofe,L_ufe,L_ake,
     & L_ex,L_ix,L_ox,L_ux,
     & L_abe,L_ube,L_ide,L_ode,
     & L_ude,L_afe,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,I_(9))
C FDA_10boats.fgi(  60, 214):���������� ������� � ���� ���,BOAT09
      Call LODOCHKA_HANDLER_KTS(deltat,L_et,L_it,
     & L_ot,L_ut,L_av,
     & L_ev,L_iv,L_ov,L_uv,
     & L_ax,L_epe,L_ipe,
     & L_ope,L_upe,L_are,
     & L_ere,L_ire,L_ore,L_ure,
     & L_ase,L_eki,L_iki,
     & L_oki,L_uki,L_ali,
     & L_eli,L_ili,L_oli,L_uli,
     & L_ami,L_ebo,L_ibo,
     & L_obo,L_ubo,L_ado,
     & L_edo,L_ido,L_odo,L_udo,
     & L_afo,L_eto,L_ito,
     & L_oto,L_uto,L_avo,
     & L_evo,L_ivo,L_ovo,L_uvo,
     & L_axo,L_ep,L_ip,
     & L_op,L_up,L_ar,
     & L_er,L_ir,L_or,
     & L_ur,L_as,L_es,
     & L_is,L_os,L_us,
     & L_at,L_eke,L_ike,
     & L_oke,L_uke,L_ale,
     & L_ele,L_ile,L_ole,
     & L_ule,L_ame,L_eme,
     & L_ime,L_ome,L_ume,
     & L_ape,L_ebi,L_ibi,
     & L_obi,L_ubi,L_adi,
     & L_edi,L_idi,L_odi,
     & L_udi,L_afi,L_efi,
     & L_ifi,L_ofi,L_ufi,
     & L_aki,L_epo,L_ipo,
     & L_opo,L_upo,L_aro,
     & L_ero,L_iro,L_oro,
     & L_uro,L_aso,L_eso,
     & L_iso,L_oso,L_uso,
     & L_ato,L_eti,L_iti,
     & L_oti,L_uti,L_avi,
     & L_evi,L_ivi,L_ovi,
     & L_uvi,L_axi,L_exi,
     & L_ixi,L_oxi,L_uxi,
     & L_abo,L_epu,L_ipu,
     & L_opu,L_upu,L_aru,
     & L_eru,L_iru,L_oru,L_uru,
     & L_asu,L_eku,L_iku,
     & L_oku,L_uku,L_alu,
     & L_elu,L_ilu,L_olu,
     & L_ulu,L_amu,L_emu,
     & L_imu,L_omu,L_umu,
     & L_apu,L_ek,L_ik,
     & L_ok,L_al,L_el,L_em,
     & L_im,L_om,L_um,L_ap,
     & L_ef,L_if,L_of,L_uf,
     & L_ak,L_uk,L_il,L_ol,
     & L_ul,L_am,L_ebe,L_ibe,
     & L_obe,L_ade,L_ede,L_efe,
     & L_ife,L_ofe,L_ufe,L_ake,
     & L_ex,L_ix,L_ox,L_ux,
     & L_abe,L_ube,L_ide,L_ode,
     & L_ude,L_afe,L_ete,L_ite,
     & L_ote,L_ave,L_eve,L_exe,
     & L_ixe,L_oxe,L_uxe,L_abi,
     & L_ese,L_ise,L_ose,L_use,
     & L_ate,L_ute,L_ive,L_ove,
     & L_uve,L_axe,L_epi,L_ipi,
     & L_opi,L_ari,L_eri,L_esi,
     & L_isi,L_osi,L_usi,L_ati,
     & L_emi,L_imi,L_omi,L_umi,
     & L_api,L_upi,L_iri,L_ori,
     & L_uri,L_asi,L_eko,L_iko,
     & L_oko,L_alo,L_elo,L_emo,
     & L_imo,L_omo,L_umo,L_apo,
     & L_efo,L_ifo,L_ofo,L_ufo,
     & L_ako,L_uko,L_ilo,L_olo,
     & L_ulo,L_amo,L_ebu,L_ibu,
     & L_obu,L_adu,L_edu,L_efu,
     & L_ifu,L_ofu,L_ufu,L_aku,
     & L_exo,L_ixo,L_oxo,L_uxo,
     & L_abu,L_ubu,L_idu,L_odu,
     & L_udu,L_afu,I_(10))
C FDA_10boats.fgi(  40, 214):���������� ������� � ���� ���,BOAT10
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_ater,I_alol
     &,
     & I_iter,I_elol,I_oter,I_ilol,I_uter,
     & I_olol,I_aver,I_ulol,I_ever,I_amol,
     & I_iver,I_emol,I_over,I_imol,I_omol,
     & I_umol,I_alir,I_apol,I_elir,I_epol,
     & I_ilir,I_ipol,I_olir,I_opol,I_ulir,
     & I_upol,I_amir,I_arol,I_emir,I_erol,
     & I_imir,I_irol,I_omir,I_orol,I_umir,
     & I_urol,I_epir,I_asol,I_ipir,I_esol,
     & I_opir,I_isol,I_upir,I_osol,I_arir,
     & I_usol,I_erir,I_atol,I_irir,I_etol,
     & I_orir,I_itol,I_urir,I_otol,I_asir,
     & I_utol,I_isir,I_osir,I_usir,I_avol,
     & I_atir,I_evol,I_akir,I_ivol,I_ekir,
     & I_ovol,I_ikir,I_uvol,I_okir,I_axol,
     & I_uver,I_exol,I_axer,I_ixol,I_exer,
     & I_oxol,I_oxer,I_uxol,I_uxer,I_abul,
     & I_abir,I_ebul,I_ebir,I_ibul,I_ibir,
     & I_obul,I_obir,I_ubul,I_ubir,I_adul,
     & I_adir,I_edul,I_edir,I_idul,I_idir,
     & I_odul,I_udir,I_udul,I_afir,I_aful,
     & I_efir,I_eful,I_ifir,I_iful,I_ofir,
     & I_oful,I_ufir,I_uful,I_etir,I_akul,
     & I_itir,I_ekul,I_otir,I_ikul,I_utir,
     & I_okul,I_avir,I_ukul,I_evir,I_alul,
     & I_ovir,I_elul,I_uvir,I_ilul,I_axir,
     & I_olul,I_exir,I_ulul,I_ixir,I_amul,
     & I_oxir,I_emul,I_uxir,I_imul,I_abor,
     & I_omul,I_ebor,I_umul,I_ibor,I_apul,
     & I_user,I_epul,I_eter,I_ipul,I_ixer,
     & I_opul,I_odir,I_upul,I_ukir,I_arul,
     & I_apir,I_erul,I_esir,I_irul,I_ivir,
     & I_orul,I_urul,I_asul,I_esul,I_obor,
     & INT(I_ukol,4),I_isul,I_osul,I_usul,I_atul,
     & L_iser,L_eper,L_urer,
     & L_oser,L_iper,L_oper,
     & L_aper,L_oler,L_uler,
     & L_amer,L_emer,L_imer,
     & L_omer,L_umer,L_uper,
     & L_arer,L_erer,L_irer,
     & L_orer,L_aser,L_orur,
     & L_eser,I_(1))
C FDA_10boats.fgi( 220, 244):���������� ���������� ������� �� ������� FDA50,BOAT01
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_ater,I_itul
     &,
     & I_iter,I_otul,I_oter,I_utul,I_uter,
     & I_avul,I_aver,I_evul,I_ever,I_ivul,
     & I_iver,I_ovul,I_over,I_uvul,I_axul,
     & I_exul,I_alir,I_ixul,I_elir,I_oxul,
     & I_ilir,I_uxul,I_olir,I_abam,I_ulir,
     & I_ebam,I_amir,I_ibam,I_emir,I_obam,
     & I_imir,I_ubam,I_omir,I_adam,I_umir,
     & I_edam,I_epir,I_idam,I_ipir,I_odam,
     & I_opir,I_udam,I_upir,I_afam,I_arir,
     & I_efam,I_erir,I_ifam,I_irir,I_ofam,
     & I_orir,I_ufam,I_urir,I_akam,I_asir,
     & I_ekam,I_isir,I_osir,I_usir,I_ikam,
     & I_atir,I_okam,I_akir,I_ukam,I_ekir,
     & I_alam,I_ikir,I_elam,I_okir,I_ilam,
     & I_uver,I_olam,I_axer,I_ulam,I_exer,
     & I_amam,I_oxer,I_emam,I_uxer,I_imam,
     & I_abir,I_omam,I_ebir,I_umam,I_ibir,
     & I_apam,I_obir,I_epam,I_ubir,I_ipam,
     & I_adir,I_opam,I_edir,I_upam,I_idir,
     & I_aram,I_udir,I_eram,I_afir,I_iram,
     & I_efir,I_oram,I_ifir,I_uram,I_ofir,
     & I_asam,I_ufir,I_esam,I_etir,I_isam,
     & I_itir,I_osam,I_otir,I_usam,I_utir,
     & I_atam,I_avir,I_etam,I_evir,I_itam,
     & I_ovir,I_otam,I_uvir,I_utam,I_axir,
     & I_avam,I_exir,I_evam,I_ixir,I_ivam,
     & I_oxir,I_ovam,I_uxir,I_uvam,I_abor,
     & I_axam,I_ebor,I_exam,I_ibor,I_ixam,
     & I_user,I_oxam,I_eter,I_uxam,I_ixer,
     & I_abem,I_odir,I_ebem,I_ukir,I_ibem,
     & I_apir,I_obem,I_esir,I_ubem,I_ivir,
     & I_adem,I_edem,I_idem,I_odem,I_obor,
     & INT(I_etul,4),I_udem,I_afem,I_efem,I_ifem,
     & L_iser,L_eper,L_urer,
     & L_oser,L_iper,L_oper,
     & L_aper,L_oler,L_uler,
     & L_amer,L_emer,L_imer,
     & L_omer,L_umer,L_uper,
     & L_arer,L_erer,L_irer,
     & L_orer,L_aser,L_orur,
     & L_eser,I_(2))
C FDA_10boats.fgi( 200, 244):���������� ���������� ������� �� ������� FDA50,BOAT02
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_ater,I_ufem
     &,
     & I_iter,I_akem,I_oter,I_ekem,I_uter,
     & I_ikem,I_aver,I_okem,I_ever,I_ukem,
     & I_iver,I_alem,I_over,I_elem,I_ilem,
     & I_olem,I_alir,I_ulem,I_elir,I_amem,
     & I_ilir,I_emem,I_olir,I_imem,I_ulir,
     & I_omem,I_amir,I_umem,I_emir,I_apem,
     & I_imir,I_epem,I_omir,I_ipem,I_umir,
     & I_opem,I_epir,I_upem,I_ipir,I_arem,
     & I_opir,I_erem,I_upir,I_irem,I_arir,
     & I_orem,I_erir,I_urem,I_irir,I_asem,
     & I_orir,I_esem,I_urir,I_isem,I_asir,
     & I_osem,I_isir,I_osir,I_usir,I_usem,
     & I_atir,I_atem,I_akir,I_etem,I_ekir,
     & I_item,I_ikir,I_otem,I_okir,I_utem,
     & I_uver,I_avem,I_axer,I_evem,I_exer,
     & I_ivem,I_oxer,I_ovem,I_uxer,I_uvem,
     & I_abir,I_axem,I_ebir,I_exem,I_ibir,
     & I_ixem,I_obir,I_oxem,I_ubir,I_uxem,
     & I_adir,I_abim,I_edir,I_ebim,I_idir,
     & I_ibim,I_udir,I_obim,I_afir,I_ubim,
     & I_efir,I_adim,I_ifir,I_edim,I_ofir,
     & I_idim,I_ufir,I_odim,I_etir,I_udim,
     & I_itir,I_afim,I_otir,I_efim,I_utir,
     & I_ifim,I_avir,I_ofim,I_evir,I_ufim,
     & I_ovir,I_akim,I_uvir,I_ekim,I_axir,
     & I_ikim,I_exir,I_okim,I_ixir,I_ukim,
     & I_oxir,I_alim,I_uxir,I_elim,I_abor,
     & I_ilim,I_ebor,I_olim,I_ibor,I_ulim,
     & I_user,I_amim,I_eter,I_emim,I_ixer,
     & I_imim,I_odir,I_omim,I_ukir,I_umim,
     & I_apir,I_apim,I_esir,I_epim,I_ivir,
     & I_ipim,I_opim,I_upim,I_arim,I_obor,
     & INT(I_ofem,4),I_erim,I_irim,I_orim,I_urim,
     & L_iser,L_eper,L_urer,
     & L_oser,L_iper,L_oper,
     & L_aper,L_oler,L_uler,
     & L_amer,L_emer,L_imer,
     & L_omer,L_umer,L_uper,
     & L_arer,L_erer,L_irer,
     & L_orer,L_aser,L_orur,
     & L_eser,I_(3))
C FDA_10boats.fgi( 180, 244):���������� ���������� ������� �� ������� FDA50,BOAT03
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_ater,I_esim
     &,
     & I_iter,I_isim,I_oter,I_osim,I_uter,
     & I_usim,I_aver,I_atim,I_ever,I_etim,
     & I_iver,I_itim,I_over,I_otim,I_utim,
     & I_avim,I_alir,I_evim,I_elir,I_ivim,
     & I_ilir,I_ovim,I_olir,I_uvim,I_ulir,
     & I_axim,I_amir,I_exim,I_emir,I_ixim,
     & I_imir,I_oxim,I_omir,I_uxim,I_umir,
     & I_abom,I_epir,I_ebom,I_ipir,I_ibom,
     & I_opir,I_obom,I_upir,I_ubom,I_arir,
     & I_adom,I_erir,I_edom,I_irir,I_idom,
     & I_orir,I_odom,I_urir,I_udom,I_asir,
     & I_afom,I_isir,I_osir,I_usir,I_efom,
     & I_atir,I_ifom,I_akir,I_ofom,I_ekir,
     & I_ufom,I_ikir,I_akom,I_okir,I_ekom,
     & I_uver,I_ikom,I_axer,I_okom,I_exer,
     & I_ukom,I_oxer,I_alom,I_uxer,I_elom,
     & I_abir,I_ilom,I_ebir,I_olom,I_ibir,
     & I_ulom,I_obir,I_amom,I_ubir,I_emom,
     & I_adir,I_imom,I_edir,I_omom,I_idir,
     & I_umom,I_udir,I_apom,I_afir,I_epom,
     & I_efir,I_ipom,I_ifir,I_opom,I_ofir,
     & I_upom,I_ufir,I_arom,I_etir,I_erom,
     & I_itir,I_irom,I_otir,I_orom,I_utir,
     & I_urom,I_avir,I_asom,I_evir,I_esom,
     & I_ovir,I_isom,I_uvir,I_osom,I_axir,
     & I_usom,I_exir,I_atom,I_ixir,I_etom,
     & I_oxir,I_itom,I_uxir,I_otom,I_abor,
     & I_utom,I_ebor,I_avom,I_ibor,I_evom,
     & I_user,I_ivom,I_eter,I_ovom,I_ixer,
     & I_uvom,I_odir,I_axom,I_ukir,I_exom,
     & I_apir,I_ixom,I_esir,I_oxom,I_ivir,
     & I_uxom,I_abum,I_ebum,I_ibum,I_obor,
     & INT(I_asim,4),I_obum,I_ubum,I_adum,I_edum,
     & L_iser,L_eper,L_urer,
     & L_oser,L_iper,L_oper,
     & L_aper,L_oler,L_uler,
     & L_amer,L_emer,L_imer,
     & L_omer,L_umer,L_uper,
     & L_arer,L_erer,L_irer,
     & L_orer,L_aser,L_orur,
     & L_eser,I_(4))
C FDA_10boats.fgi( 160, 244):���������� ���������� ������� �� ������� FDA50,BOAT04
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_ater,I_odum
     &,
     & I_iter,I_udum,I_oter,I_afum,I_uter,
     & I_efum,I_aver,I_ifum,I_ever,I_ofum,
     & I_iver,I_ufum,I_over,I_akum,I_ekum,
     & I_ikum,I_alir,I_okum,I_elir,I_ukum,
     & I_ilir,I_alum,I_olir,I_elum,I_ulir,
     & I_ilum,I_amir,I_olum,I_emir,I_ulum,
     & I_imir,I_amum,I_omir,I_emum,I_umir,
     & I_imum,I_epir,I_omum,I_ipir,I_umum,
     & I_opir,I_apum,I_upir,I_epum,I_arir,
     & I_ipum,I_erir,I_opum,I_irir,I_upum,
     & I_orir,I_arum,I_urir,I_erum,I_asir,
     & I_irum,I_isir,I_osir,I_usir,I_orum,
     & I_atir,I_urum,I_akir,I_asum,I_ekir,
     & I_esum,I_ikir,I_isum,I_okir,I_osum,
     & I_uver,I_usum,I_axer,I_atum,I_exer,
     & I_etum,I_oxer,I_itum,I_uxer,I_otum,
     & I_abir,I_utum,I_ebir,I_avum,I_ibir,
     & I_evum,I_obir,I_ivum,I_ubir,I_ovum,
     & I_adir,I_uvum,I_edir,I_axum,I_idir,
     & I_exum,I_udir,I_ixum,I_afir,I_oxum,
     & I_efir,I_uxum,I_ifir,I_abap,I_ofir,
     & I_ebap,I_ufir,I_ibap,I_etir,I_obap,
     & I_itir,I_ubap,I_otir,I_adap,I_utir,
     & I_edap,I_avir,I_idap,I_evir,I_odap,
     & I_ovir,I_udap,I_uvir,I_afap,I_axir,
     & I_efap,I_exir,I_ifap,I_ixir,I_ofap,
     & I_oxir,I_ufap,I_uxir,I_akap,I_abor,
     & I_ekap,I_ebor,I_ikap,I_ibor,I_okap,
     & I_user,I_ukap,I_eter,I_alap,I_ixer,
     & I_elap,I_odir,I_ilap,I_ukir,I_olap,
     & I_apir,I_ulap,I_esir,I_amap,I_ivir,
     & I_emap,I_imap,I_omap,I_umap,I_obor,
     & INT(I_idum,4),I_apap,I_epap,I_ipap,I_opap,
     & L_iser,L_eper,L_urer,
     & L_oser,L_iper,L_oper,
     & L_aper,L_oler,L_uler,
     & L_amer,L_emer,L_imer,
     & L_omer,L_umer,L_uper,
     & L_arer,L_erer,L_irer,
     & L_orer,L_aser,L_orur,
     & L_eser,I_(5))
C FDA_10boats.fgi( 140, 244):���������� ���������� ������� �� ������� FDA50,BOAT05
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_ater,I_arap
     &,
     & I_iter,I_erap,I_oter,I_irap,I_uter,
     & I_orap,I_aver,I_urap,I_ever,I_asap,
     & I_iver,I_esap,I_over,I_isap,I_osap,
     & I_usap,I_alir,I_atap,I_elir,I_etap,
     & I_ilir,I_itap,I_olir,I_otap,I_ulir,
     & I_utap,I_amir,I_avap,I_emir,I_evap,
     & I_imir,I_ivap,I_omir,I_ovap,I_umir,
     & I_uvap,I_epir,I_axap,I_ipir,I_exap,
     & I_opir,I_ixap,I_upir,I_oxap,I_arir,
     & I_uxap,I_erir,I_abep,I_irir,I_ebep,
     & I_orir,I_ibep,I_urir,I_obep,I_asir,
     & I_ubep,I_isir,I_osir,I_usir,I_adep,
     & I_atir,I_edep,I_akir,I_idep,I_ekir,
     & I_odep,I_ikir,I_udep,I_okir,I_afep,
     & I_uver,I_efep,I_axer,I_ifep,I_exer,
     & I_ofep,I_oxer,I_ufep,I_uxer,I_akep,
     & I_abir,I_ekep,I_ebir,I_ikep,I_ibir,
     & I_okep,I_obir,I_ukep,I_ubir,I_alep,
     & I_adir,I_elep,I_edir,I_ilep,I_idir,
     & I_olep,I_udir,I_ulep,I_afir,I_amep,
     & I_efir,I_emep,I_ifir,I_imep,I_ofir,
     & I_omep,I_ufir,I_umep,I_etir,I_apep,
     & I_itir,I_epep,I_otir,I_ipep,I_utir,
     & I_opep,I_avir,I_upep,I_evir,I_arep,
     & I_ovir,I_erep,I_uvir,I_irep,I_axir,
     & I_orep,I_exir,I_urep,I_ixir,I_asep,
     & I_oxir,I_esep,I_uxir,I_isep,I_abor,
     & I_osep,I_ebor,I_usep,I_ibor,I_atep,
     & I_user,I_etep,I_eter,I_itep,I_ixer,
     & I_otep,I_odir,I_utep,I_ukir,I_avep,
     & I_apir,I_evep,I_esir,I_ivep,I_ivir,
     & I_ovep,I_uvep,I_axep,I_exep,I_obor,
     & INT(I_upap,4),I_ixep,I_oxep,I_uxep,I_abip,
     & L_iser,L_eper,L_urer,
     & L_oser,L_iper,L_oper,
     & L_aper,L_oler,L_uler,
     & L_amer,L_emer,L_imer,
     & L_omer,L_umer,L_uper,
     & L_arer,L_erer,L_irer,
     & L_orer,L_aser,L_orur,
     & L_eser,I_(6))
C FDA_10boats.fgi( 120, 244):���������� ���������� ������� �� ������� FDA50,BOAT06
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_ater,I_ibip
     &,
     & I_iter,I_obip,I_oter,I_ubip,I_uter,
     & I_adip,I_aver,I_edip,I_ever,I_idip,
     & I_iver,I_odip,I_over,I_udip,I_afip,
     & I_efip,I_alir,I_ifip,I_elir,I_ofip,
     & I_ilir,I_ufip,I_olir,I_akip,I_ulir,
     & I_ekip,I_amir,I_ikip,I_emir,I_okip,
     & I_imir,I_ukip,I_omir,I_alip,I_umir,
     & I_elip,I_epir,I_ilip,I_ipir,I_olip,
     & I_opir,I_ulip,I_upir,I_amip,I_arir,
     & I_emip,I_erir,I_imip,I_irir,I_omip,
     & I_orir,I_umip,I_urir,I_apip,I_asir,
     & I_epip,I_isir,I_osir,I_usir,I_ipip,
     & I_atir,I_opip,I_akir,I_upip,I_ekir,
     & I_arip,I_ikir,I_erip,I_okir,I_irip,
     & I_uver,I_orip,I_axer,I_urip,I_exer,
     & I_asip,I_oxer,I_esip,I_uxer,I_isip,
     & I_abir,I_osip,I_ebir,I_usip,I_ibir,
     & I_atip,I_obir,I_etip,I_ubir,I_itip,
     & I_adir,I_otip,I_edir,I_utip,I_idir,
     & I_avip,I_udir,I_evip,I_afir,I_ivip,
     & I_efir,I_ovip,I_ifir,I_uvip,I_ofir,
     & I_axip,I_ufir,I_exip,I_etir,I_ixip,
     & I_itir,I_oxip,I_otir,I_uxip,I_utir,
     & I_abop,I_avir,I_ebop,I_evir,I_ibop,
     & I_ovir,I_obop,I_uvir,I_ubop,I_axir,
     & I_adop,I_exir,I_edop,I_ixir,I_idop,
     & I_oxir,I_odop,I_uxir,I_udop,I_abor,
     & I_afop,I_ebor,I_efop,I_ibor,I_ifop,
     & I_user,I_ofop,I_eter,I_ufop,I_ixer,
     & I_akop,I_odir,I_ekop,I_ukir,I_ikop,
     & I_apir,I_okop,I_esir,I_ukop,I_ivir,
     & I_alop,I_elop,I_ilop,I_olop,I_obor,
     & INT(I_ebip,4),I_ulop,I_amop,I_emop,I_imop,
     & L_iser,L_eper,L_urer,
     & L_oser,L_iper,L_oper,
     & L_aper,L_oler,L_uler,
     & L_amer,L_emer,L_imer,
     & L_omer,L_umer,L_uper,
     & L_arer,L_erer,L_irer,
     & L_orer,L_aser,L_orur,
     & L_eser,I_(7))
C FDA_10boats.fgi( 100, 244):���������� ���������� ������� �� ������� FDA50,BOAT07
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_ater,I_umop
     &,
     & I_iter,I_apop,I_oter,I_epop,I_uter,
     & I_ipop,I_aver,I_opop,I_ever,I_upop,
     & I_iver,I_arop,I_over,I_erop,I_irop,
     & I_orop,I_alir,I_urop,I_elir,I_asop,
     & I_ilir,I_esop,I_olir,I_isop,I_ulir,
     & I_osop,I_amir,I_usop,I_emir,I_atop,
     & I_imir,I_etop,I_omir,I_itop,I_umir,
     & I_otop,I_epir,I_utop,I_ipir,I_avop,
     & I_opir,I_evop,I_upir,I_ivop,I_arir,
     & I_ovop,I_erir,I_uvop,I_irir,I_axop,
     & I_orir,I_exop,I_urir,I_ixop,I_asir,
     & I_oxop,I_isir,I_osir,I_usir,I_uxop,
     & I_atir,I_abup,I_akir,I_ebup,I_ekir,
     & I_ibup,I_ikir,I_obup,I_okir,I_ubup,
     & I_uver,I_adup,I_axer,I_edup,I_exer,
     & I_idup,I_oxer,I_odup,I_uxer,I_udup,
     & I_abir,I_afup,I_ebir,I_efup,I_ibir,
     & I_ifup,I_obir,I_ofup,I_ubir,I_ufup,
     & I_adir,I_akup,I_edir,I_ekup,I_idir,
     & I_ikup,I_udir,I_okup,I_afir,I_ukup,
     & I_efir,I_alup,I_ifir,I_elup,I_ofir,
     & I_ilup,I_ufir,I_olup,I_etir,I_ulup,
     & I_itir,I_amup,I_otir,I_emup,I_utir,
     & I_imup,I_avir,I_omup,I_evir,I_umup,
     & I_ovir,I_apup,I_uvir,I_epup,I_axir,
     & I_ipup,I_exir,I_opup,I_ixir,I_upup,
     & I_oxir,I_arup,I_uxir,I_erup,I_abor,
     & I_irup,I_ebor,I_orup,I_ibor,I_urup,
     & I_user,I_asup,I_eter,I_esup,I_ixer,
     & I_isup,I_odir,I_osup,I_ukir,I_usup,
     & I_apir,I_atup,I_esir,I_etup,I_ivir,
     & I_itup,I_otup,I_utup,I_avup,I_obor,
     & INT(I_omop,4),I_evup,I_ivup,I_ovup,I_uvup,
     & L_iser,L_eper,L_urer,
     & L_oser,L_iper,L_oper,
     & L_aper,L_oler,L_uler,
     & L_amer,L_emer,L_imer,
     & L_omer,L_umer,L_uper,
     & L_arer,L_erer,L_irer,
     & L_orer,L_aser,L_orur,
     & L_eser,I_(8))
C FDA_10boats.fgi(  80, 244):���������� ���������� ������� �� ������� FDA50,BOAT08
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_ater,I_exup
     &,
     & I_iter,I_ixup,I_oter,I_oxup,I_uter,
     & I_uxup,I_aver,I_abar,I_ever,I_ebar,
     & I_iver,I_ibar,I_over,I_obar,I_ubar,
     & I_adar,I_alir,I_edar,I_elir,I_idar,
     & I_ilir,I_odar,I_olir,I_udar,I_ulir,
     & I_afar,I_amir,I_efar,I_emir,I_ifar,
     & I_imir,I_ofar,I_omir,I_ufar,I_umir,
     & I_akar,I_epir,I_ekar,I_ipir,I_ikar,
     & I_opir,I_okar,I_upir,I_ukar,I_arir,
     & I_alar,I_erir,I_elar,I_irir,I_ilar,
     & I_orir,I_olar,I_urir,I_ular,I_asir,
     & I_amar,I_isir,I_osir,I_usir,I_emar,
     & I_atir,I_imar,I_akir,I_omar,I_ekir,
     & I_umar,I_ikir,I_apar,I_okir,I_epar,
     & I_uver,I_ipar,I_axer,I_opar,I_exer,
     & I_upar,I_oxer,I_arar,I_uxer,I_erar,
     & I_abir,I_irar,I_ebir,I_orar,I_ibir,
     & I_urar,I_obir,I_asar,I_ubir,I_esar,
     & I_adir,I_isar,I_edir,I_osar,I_idir,
     & I_usar,I_udir,I_atar,I_afir,I_etar,
     & I_efir,I_itar,I_ifir,I_otar,I_ofir,
     & I_utar,I_ufir,I_avar,I_etir,I_evar,
     & I_itir,I_ivar,I_otir,I_ovar,I_utir,
     & I_uvar,I_avir,I_axar,I_evir,I_exar,
     & I_ovir,I_ixar,I_uvir,I_oxar,I_axir,
     & I_uxar,I_exir,I_aber,I_ixir,I_eber,
     & I_oxir,I_iber,I_uxir,I_ober,I_abor,
     & I_uber,I_ebor,I_ader,I_ibor,I_eder,
     & I_user,I_ider,I_eter,I_oder,I_ixer,
     & I_uder,I_odir,I_afer,I_ukir,I_efer,
     & I_apir,I_ifer,I_esir,I_ofer,I_ivir,
     & I_ufer,I_aker,I_eker,I_iker,I_obor,
     & INT(I_axup,4),I_oker,I_uker,I_aler,I_eler,
     & L_iser,L_eper,L_urer,
     & L_oser,L_iper,L_oper,
     & L_aper,L_oler,L_uler,
     & L_amer,L_emer,L_imer,
     & L_omer,L_umer,L_uper,
     & L_arer,L_erer,L_irer,
     & L_orer,L_aser,L_orur,
     & L_eser,I_(9))
C FDA_10boats.fgi(  60, 244):���������� ���������� ������� �� ������� FDA50,BOAT09
      Call LODOCHKA_HANDLER_FDA50_COORD(deltat,I_ater,I_ubor
     &,
     & I_iter,I_ador,I_oter,I_edor,I_uter,
     & I_idor,I_aver,I_odor,I_ever,I_udor,
     & I_iver,I_afor,I_over,I_efor,I_ifor,
     & I_ofor,I_alir,I_ufor,I_elir,I_akor,
     & I_ilir,I_ekor,I_olir,I_ikor,I_ulir,
     & I_okor,I_amir,I_ukor,I_emir,I_alor,
     & I_imir,I_elor,I_omir,I_ilor,I_umir,
     & I_olor,I_epir,I_ulor,I_ipir,I_amor,
     & I_opir,I_emor,I_upir,I_imor,I_arir,
     & I_omor,I_erir,I_umor,I_irir,I_apor,
     & I_orir,I_epor,I_urir,I_ipor,I_asir,
     & I_opor,I_isir,I_osir,I_usir,I_upor,
     & I_atir,I_aror,I_akir,I_eror,I_ekir,
     & I_iror,I_ikir,I_oror,I_okir,I_uror,
     & I_uver,I_asor,I_axer,I_esor,I_exer,
     & I_isor,I_oxer,I_osor,I_uxer,I_usor,
     & I_abir,I_ator,I_ebir,I_etor,I_ibir,
     & I_itor,I_obir,I_otor,I_ubir,I_utor,
     & I_adir,I_avor,I_edir,I_evor,I_idir,
     & I_ivor,I_udir,I_ovor,I_afir,I_uvor,
     & I_efir,I_axor,I_ifir,I_exor,I_ofir,
     & I_ixor,I_ufir,I_oxor,I_etir,I_uxor,
     & I_itir,I_abur,I_otir,I_ebur,I_utir,
     & I_ibur,I_avir,I_obur,I_evir,I_ubur,
     & I_ovir,I_adur,I_uvir,I_edur,I_axir,
     & I_idur,I_exir,I_odur,I_ixir,I_udur,
     & I_oxir,I_afur,I_uxir,I_efur,I_abor,
     & I_ifur,I_ebor,I_ofur,I_ibor,I_ufur,
     & I_user,I_akur,I_eter,I_ekur,I_ixer,
     & I_ikur,I_odir,I_okur,I_ukir,I_ukur,
     & I_apir,I_alur,I_esir,I_elur,I_ivir,
     & I_ilur,I_olur,I_ulur,I_amur,I_obor,
     & INT(I_iler,4),I_emur,I_imur,I_omur,I_umur,
     & L_iser,L_eper,L_urer,
     & L_oser,L_iper,L_oper,
     & L_aper,L_oler,L_uler,
     & L_amer,L_emer,L_imer,
     & L_omer,L_umer,L_uper,
     & L_arer,L_erer,L_irer,
     & L_orer,L_aser,L_orur,
     & L_eser,I_(10))
C FDA_10boats.fgi(  40, 244):���������� ���������� ������� �� ������� FDA50,BOAT10
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_apur,
     & L_opur,L_arur,L_irur,
     & L_orur,L_upur,L_urur,
     & L_erur,L_epur,L_opabe,L_ipur,
     & L_osabe,L_otabe,L_orabe,I_(1))
C FDA_10boats.fgi( 220, 260):���������� ���������� ������� �� ������� FDA90,BOAT01
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_apur,
     & L_opur,L_arur,L_irur,
     & L_orur,L_upur,L_urur,
     & L_erur,L_epur,L_opabe,L_ipur,
     & L_osabe,L_otabe,L_orabe,I_(2))
C FDA_10boats.fgi( 200, 260):���������� ���������� ������� �� ������� FDA90,BOAT02
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_apur,
     & L_opur,L_arur,L_irur,
     & L_orur,L_upur,L_urur,
     & L_erur,L_epur,L_opabe,L_ipur,
     & L_osabe,L_otabe,L_orabe,I_(3))
C FDA_10boats.fgi( 180, 260):���������� ���������� ������� �� ������� FDA90,BOAT03
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_apur,
     & L_opur,L_arur,L_irur,
     & L_orur,L_upur,L_urur,
     & L_erur,L_epur,L_opabe,L_ipur,
     & L_osabe,L_otabe,L_orabe,I_(4))
C FDA_10boats.fgi( 160, 260):���������� ���������� ������� �� ������� FDA90,BOAT04
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_apur,
     & L_opur,L_arur,L_irur,
     & L_orur,L_upur,L_urur,
     & L_erur,L_epur,L_opabe,L_ipur,
     & L_osabe,L_otabe,L_orabe,I_(5))
C FDA_10boats.fgi( 140, 260):���������� ���������� ������� �� ������� FDA90,BOAT05
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_apur,
     & L_opur,L_arur,L_irur,
     & L_orur,L_upur,L_urur,
     & L_erur,L_epur,L_opabe,L_ipur,
     & L_osabe,L_otabe,L_orabe,I_(6))
C FDA_10boats.fgi( 120, 260):���������� ���������� ������� �� ������� FDA90,BOAT06
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_apur,
     & L_opur,L_arur,L_irur,
     & L_orur,L_upur,L_urur,
     & L_erur,L_epur,L_opabe,L_ipur,
     & L_osabe,L_otabe,L_orabe,I_(7))
C FDA_10boats.fgi( 100, 260):���������� ���������� ������� �� ������� FDA90,BOAT07
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_apur,
     & L_opur,L_arur,L_irur,
     & L_orur,L_upur,L_urur,
     & L_erur,L_epur,L_opabe,L_ipur,
     & L_osabe,L_otabe,L_orabe,I_(8))
C FDA_10boats.fgi(  80, 260):���������� ���������� ������� �� ������� FDA90,BOAT08
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_apur,
     & L_opur,L_arur,L_irur,
     & L_orur,L_upur,L_urur,
     & L_erur,L_epur,L_opabe,L_ipur,
     & L_osabe,L_otabe,L_orabe,I_(9))
C FDA_10boats.fgi(  60, 260):���������� ���������� ������� �� ������� FDA90,BOAT09
      Call LODOCHKA_HANDLER_FDA90_COORD(deltat,L_apur,
     & L_opur,L_arur,L_irur,
     & L_orur,L_upur,L_urur,
     & L_erur,L_epur,L_opabe,L_ipur,
     & L_osabe,L_otabe,L_orabe,I_(10))
C FDA_10boats.fgi(  40, 260):���������� ���������� ������� �� ������� FDA90,BOAT10
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_utas,R_isas,L_udes,L_emebe,
     & L_omabe,L_imabe,R_avas,R_osas,
     & L_afes,L_emabe,R_evas,R_usas,
     & L_efes,I_ifes,I_ofes,I_ufes,I_akes,I_ekes,
     & INT(I_akebe,4),L_ekebe,L_emas,
     & L_imas,L_omas,L_umas,
     & L_apas,L_epas,L_ipas,
     & L_opas,L_upas,L_aras,I_(1),I_ufas,
     & R_ikes,REAL(R_afebe,4),R_okes,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_ukes,R_ales,
     & R_eles,REAL(R_ikebe,4),R_iles,
     & REAL(R_okebe,4),R_etas,R_uras,L_edes,L_atas,
     & L_imebe,REAL(R_omebe,4),R_ofas,R_ules,
     & R_ames,R_emes,REAL(R_uvabe,4),R_imes,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_omes,
     & REAL(R_avabe,4),R_umes,R_itas,R_asas,
     & L_ides,R_apes,R_epes,R_ipes,L_elas,
     & R_opes,R_upes,REAL(R_erabe,4),R_ares,R_otas,
     & R_esas,L_odes,R_eres,R_ires,R_ores,R_ures,
     & R_ases,REAL(R_esabe,4),R_eses,L_ilas,
     & L_ivas,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_ises,R_oses,R_uses,REAL(R_etabe,4),R_ates,
     & L_olas,L_ovas,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_etes,R_ites,
     & R_otes,R_utes,REAL(R_epabe,4),R_aves,
     & L_alas,L_arabe,L_orabe,L_uvas,
     & L_axas,L_apabe,R_usur,R_uxas,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_abes,I_ekas,R_ebas,R_ebes,R_eves,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_oles,R_ixur,C20_obas
     &)
C FDA_10boats.fgi( 219, 276):���������� �������,BOAT01
      I_af=I_(1)
C FDA_10boats.fgi( 219, 276):������-�������: ���������� ��� �������������� ������,cpy BOAT01:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_ifed,R_abuk
     &,
     & I_uxok,R_exel,I_ofed,I_axel,I_ufed,
     & I_adel,R_ifel,I_edel,
     & R_ofel,I_idel,R_ufel,
     & I_odel,R_akel,I_udel,
     & R_ekel,I_afel,R_ikel,
     & I_efel,R_okel,I_ovu,
     & R_ibel,I_ebel,I_uvu,
     & R_ubel,I_obel,R_usel,
     & I_osel,I_axu,R_elel,
     & I_ukel,I_exu,R_ilel,
     & I_alel,I_ixu,R_etel,I_atel,
     & I_oxu,R_ivel,I_evel,I_uxu,
     & R_otel,I_itel,I_abad,
     & R_uvel,I_ovel,I_ebad,
     & R_avel,I_utel,I_ibad,I_obad,
     & I_ubad,I_adad,I_edad,I_idad,I_odad,I_udad,
     & I_afad,I_efad,I_ifad,I_ofad,I_ufad,I_akad,
     & I_ekad,I_ikad,I_okad,I_ukad,I_alad,I_elad,
     & I_ilad,I_olad,I_ulad,I_amad,I_emad,I_imad,
     & I_omad,I_umad,I_apad,I_epad,I_ipad,I_opad,
     & I_upad,I_arad,I_erad,I_irad,I_orad,I_urad,
     & I_asad,I_esad,I_isad,I_osad,I_usad,I_atad,
     & I_etad,I_itad,I_otad,I_utad,I_avad,I_evad,
     & I_ivad,I_ovad,I_uvad,I_axad,I_exad,I_ixad,
     & I_oxad,I_uxad,I_abed,I_ebed,I_ibed,I_obed,
     & I_ubed,I_aded,I_eded,I_ided,I_oded,I_uded,
     & I_afed,I_efed,INT(I_af,4),R_afuk,R_uduk,
     & R_oduk,R_iduk,R_eduk,R_afal,
     & R_udal,R_odal,R_idal,R_edal,R_ekal,
     & R_ikal,R_okal,R_ukal,R_alal,
     & R_ekuk,R_ikuk,R_okuk,R_ukuk,
     & R_aluk,R_emal,R_imal,R_omal,
     & R_umal,R_apal,R_emuk,R_imuk,
     & R_omuk,R_umuk,R_apuk,R_isel,
     & R_esel,R_asel,R_urel,
     & R_orel,R_ipel,R_epel,
     & R_apel,R_umel,R_omel,
     & R_eral,R_iral,R_oral,R_ural,R_asal,
     & R_eruk,R_iruk,R_oruk,R_uruk,
     & R_asuk,R_avuk,R_utuk,R_otuk,
     & R_ituk,R_etuk,R_aval,R_utal,
     & R_otal,R_ital,R_etal,R_exal,R_ixal,
     & R_oxal,R_uxal,R_abel,R_exuk,
     & R_ixuk,R_oxuk,R_uxuk,REAL(R_ofas,4),
     & R_abal,L_utu,L_otu,L_itu,L_avu,L_evu,L_ivu,
     & L_osu,L_isu,L_esu,L_usu,L_atu,L_etu,I_aduk,
     & I_ubuk,I_ebal,I_ibal,I_obal,I_ubal,I_adal,
     & I_ebuk,I_ibuk,I_obuk,I_akuk,I_ufuk,
     & I_efal,I_ifal,I_ofal,I_ufal,I_akal,I_efuk,
     & I_ifuk,I_ofuk,I_amuk,I_uluk,I_elal,
     & I_ilal,I_olal,I_ulal,I_amal,I_eluk,I_iluk,
     & I_oluk,I_aruk,I_upuk,I_epal,I_ipal,
     & I_opal,I_upal,I_aral,I_epuk,I_ipuk,
     & I_opuk,I_atuk,I_usuk,I_esal,I_isal,
     & I_osal,I_usal,I_atal,I_esuk,I_isuk,
     & I_osuk,I_axuk,I_uvuk,I_eval,I_ival,
     & I_oval,I_uval,I_axal,I_evuk,I_ivuk,
     & I_ovuk,I_olel,I_ulel,I_amel,
     & I_emel,I_imel,1,I_opel,
     & I_upel,I_arel,I_erel,
     & I_irel)
C FDA_10boats.fgi( 220, 230):���������� ���������� ������� �� ������� FDA60,BOAT01
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_obos,R_exis,L_olos,L_emebe,
     & L_omabe,L_imabe,R_ubos,R_ixis,
     & L_ulos,L_emabe,R_ados,R_oxis,
     & L_amos,I_emos,I_imos,I_omos,I_umos,I_apos,
     & INT(I_akebe,4),L_ekebe,L_asis,
     & L_esis,L_isis,L_osis,
     & L_usis,L_atis,L_etis,
     & L_itis,L_otis,L_utis,I_(2),I_omis,
     & R_epos,REAL(R_afebe,4),R_ipos,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_opos,R_upos,
     & R_aros,REAL(R_ikebe,4),R_eros,
     & REAL(R_okebe,4),R_abos,R_ovis,L_alos,L_uxis,
     & L_imebe,REAL(R_omebe,4),R_imis,R_oros,
     & R_uros,R_asos,REAL(R_uvabe,4),R_esos,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_isos,
     & REAL(R_avabe,4),R_osos,R_ebos,R_uvis,
     & L_elos,R_usos,R_atos,R_etos,L_aris,
     & R_itos,R_otos,REAL(R_erabe,4),R_utos,R_ibos,
     & R_axis,L_ilos,R_avos,R_evos,R_ivos,R_ovos,
     & R_uvos,REAL(R_esabe,4),R_axos,L_eris,
     & L_edos,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_exos,R_ixos,R_oxos,REAL(R_etabe,4),R_uxos,
     & L_iris,L_idos,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_abus,R_ebus,
     & R_ibus,R_obus,REAL(R_epabe,4),R_ubus,
     & L_upis,L_arabe,L_orabe,L_odos,
     & L_udos,L_apabe,R_oxes,R_ofos,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_ufos,I_apis,R_akis,R_akos,R_adus,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_iros,R_efis,C20_ikis
     &)
C FDA_10boats.fgi( 199, 276):���������� �������,BOAT02
      I_ud=I_(2)
C FDA_10boats.fgi( 199, 276):������-�������: ���������� ��� �������������� ������,cpy BOAT02:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_etid,R_abuk
     &,
     & I_uxok,R_exel,I_itid,I_axel,I_otid,
     & I_adel,R_ifel,I_edel,
     & R_ofel,I_idel,R_ufel,
     & I_odel,R_akel,I_udel,
     & R_ekel,I_afel,R_ikel,
     & I_efel,R_okel,I_imed,
     & R_ibel,I_ebel,I_omed,
     & R_ubel,I_obel,R_usel,
     & I_osel,I_umed,R_elel,
     & I_ukel,I_aped,R_ilel,
     & I_alel,I_eped,R_etel,I_atel,
     & I_iped,R_ivel,I_evel,I_oped,
     & R_otel,I_itel,I_uped,
     & R_uvel,I_ovel,I_ared,
     & R_avel,I_utel,I_ered,I_ired,
     & I_ored,I_ured,I_ased,I_esed,I_ised,I_osed,
     & I_used,I_ated,I_eted,I_ited,I_oted,I_uted,
     & I_aved,I_eved,I_ived,I_oved,I_uved,I_axed,
     & I_exed,I_ixed,I_oxed,I_uxed,I_abid,I_ebid,
     & I_ibid,I_obid,I_ubid,I_adid,I_edid,I_idid,
     & I_odid,I_udid,I_afid,I_efid,I_ifid,I_ofid,
     & I_ufid,I_akid,I_ekid,I_ikid,I_okid,I_ukid,
     & I_alid,I_elid,I_ilid,I_olid,I_ulid,I_amid,
     & I_emid,I_imid,I_omid,I_umid,I_apid,I_epid,
     & I_ipid,I_opid,I_upid,I_arid,I_erid,I_irid,
     & I_orid,I_urid,I_asid,I_esid,I_isid,I_osid,
     & I_usid,I_atid,INT(I_ud,4),R_afuk,R_uduk,
     & R_oduk,R_iduk,R_eduk,R_afal,
     & R_udal,R_odal,R_idal,R_edal,R_ekal,
     & R_ikal,R_okal,R_ukal,R_alal,
     & R_ekuk,R_ikuk,R_okuk,R_ukuk,
     & R_aluk,R_emal,R_imal,R_omal,
     & R_umal,R_apal,R_emuk,R_imuk,
     & R_omuk,R_umuk,R_apuk,R_isel,
     & R_esel,R_asel,R_urel,
     & R_orel,R_ipel,R_epel,
     & R_apel,R_umel,R_omel,
     & R_eral,R_iral,R_oral,R_ural,R_asal,
     & R_eruk,R_iruk,R_oruk,R_uruk,
     & R_asuk,R_avuk,R_utuk,R_otuk,
     & R_ituk,R_etuk,R_aval,R_utal,
     & R_otal,R_ital,R_etal,R_exal,R_ixal,
     & R_oxal,R_uxal,R_abel,R_exuk,
     & R_ixuk,R_oxuk,R_uxuk,REAL(R_imis,4),
     & R_abal,L_oled,L_iled,L_eled,L_uled,L_amed,L_emed,
     & L_iked,L_eked,L_aked,L_oked,L_uked,L_aled,I_aduk,
     & I_ubuk,I_ebal,I_ibal,I_obal,I_ubal,I_adal,
     & I_ebuk,I_ibuk,I_obuk,I_akuk,I_ufuk,
     & I_efal,I_ifal,I_ofal,I_ufal,I_akal,I_efuk,
     & I_ifuk,I_ofuk,I_amuk,I_uluk,I_elal,
     & I_ilal,I_olal,I_ulal,I_amal,I_eluk,I_iluk,
     & I_oluk,I_aruk,I_upuk,I_epal,I_ipal,
     & I_opal,I_upal,I_aral,I_epuk,I_ipuk,
     & I_opuk,I_atuk,I_usuk,I_esal,I_isal,
     & I_osal,I_usal,I_atal,I_esuk,I_isuk,
     & I_osuk,I_axuk,I_uvuk,I_eval,I_ival,
     & I_oval,I_uval,I_axal,I_evuk,I_ivuk,
     & I_ovuk,I_olel,I_ulel,I_amel,
     & I_emel,I_imel,2,I_opel,
     & I_upel,I_arel,I_erel,
     & I_irel)
C FDA_10boats.fgi( 200, 230):���������� ���������� ������� �� ������� FDA60,BOAT02
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_ikat,R_afat,L_irat,L_emebe,
     & L_omabe,L_imabe,R_okat,R_efat,
     & L_orat,L_emabe,R_ukat,R_ifat,
     & L_urat,I_asat,I_esat,I_isat,I_osat,I_usat,
     & INT(I_akebe,4),L_ekebe,L_uvus,
     & L_axus,L_exus,L_ixus,
     & L_oxus,L_uxus,L_abat,
     & L_ebat,L_ibat,L_obat,I_(3),I_isus,
     & R_atat,REAL(R_afebe,4),R_etat,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_itat,R_otat,
     & R_utat,REAL(R_ikebe,4),R_avat,
     & REAL(R_okebe,4),R_ufat,R_idat,L_upat,L_ofat,
     & L_imebe,REAL(R_omebe,4),R_esus,R_ivat,
     & R_ovat,R_uvat,REAL(R_uvabe,4),R_axat,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_exat,
     & REAL(R_avabe,4),R_ixat,R_akat,R_odat,
     & L_arat,R_oxat,R_uxat,R_abet,L_utus,
     & R_ebet,R_ibet,REAL(R_erabe,4),R_obet,R_ekat,
     & R_udat,L_erat,R_ubet,R_adet,R_edet,R_idet,
     & R_odet,REAL(R_esabe,4),R_udet,L_avus,
     & L_alat,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_afet,R_efet,R_ifet,REAL(R_etabe,4),R_ofet,
     & L_evus,L_elat,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_ufet,R_aket,
     & R_eket,R_iket,REAL(R_epabe,4),R_oket,
     & L_otus,L_arabe,L_orabe,L_ilat,
     & L_olat,L_apabe,R_ifus,R_imat,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_omat,I_usus,R_umus,R_umat,R_uket,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_evat,R_amus,C20_epus
     &)
C FDA_10boats.fgi( 179, 276):���������� �������,BOAT03
      I_od=I_(3)
C FDA_10boats.fgi( 179, 276):������-�������: ���������� ��� �������������� ������,cpy BOAT03:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_alud,R_abuk
     &,
     & I_uxok,R_exel,I_elud,I_axel,I_ilud,
     & I_adel,R_ifel,I_edel,
     & R_ofel,I_idel,R_ufel,
     & I_odel,R_akel,I_udel,
     & R_ekel,I_afel,R_ikel,
     & I_efel,R_okel,I_ebod,
     & R_ibel,I_ebel,I_ibod,
     & R_ubel,I_obel,R_usel,
     & I_osel,I_obod,R_elel,
     & I_ukel,I_ubod,R_ilel,
     & I_alel,I_adod,R_etel,I_atel,
     & I_edod,R_ivel,I_evel,I_idod,
     & R_otel,I_itel,I_odod,
     & R_uvel,I_ovel,I_udod,
     & R_avel,I_utel,I_afod,I_efod,
     & I_ifod,I_ofod,I_ufod,I_akod,I_ekod,I_ikod,
     & I_okod,I_ukod,I_alod,I_elod,I_ilod,I_olod,
     & I_ulod,I_amod,I_emod,I_imod,I_omod,I_umod,
     & I_apod,I_epod,I_ipod,I_opod,I_upod,I_arod,
     & I_erod,I_irod,I_orod,I_urod,I_asod,I_esod,
     & I_isod,I_osod,I_usod,I_atod,I_etod,I_itod,
     & I_otod,I_utod,I_avod,I_evod,I_ivod,I_ovod,
     & I_uvod,I_axod,I_exod,I_ixod,I_oxod,I_uxod,
     & I_abud,I_ebud,I_ibud,I_obud,I_ubud,I_adud,
     & I_edud,I_idud,I_odud,I_udud,I_afud,I_efud,
     & I_ifud,I_ofud,I_ufud,I_akud,I_ekud,I_ikud,
     & I_okud,I_ukud,INT(I_od,4),R_afuk,R_uduk,
     & R_oduk,R_iduk,R_eduk,R_afal,
     & R_udal,R_odal,R_idal,R_edal,R_ekal,
     & R_ikal,R_okal,R_ukal,R_alal,
     & R_ekuk,R_ikuk,R_okuk,R_ukuk,
     & R_aluk,R_emal,R_imal,R_omal,
     & R_umal,R_apal,R_emuk,R_imuk,
     & R_omuk,R_umuk,R_apuk,R_isel,
     & R_esel,R_asel,R_urel,
     & R_orel,R_ipel,R_epel,
     & R_apel,R_umel,R_omel,
     & R_eral,R_iral,R_oral,R_ural,R_asal,
     & R_eruk,R_iruk,R_oruk,R_uruk,
     & R_asuk,R_avuk,R_utuk,R_otuk,
     & R_ituk,R_etuk,R_aval,R_utal,
     & R_otal,R_ital,R_etal,R_exal,R_ixal,
     & R_oxal,R_uxal,R_abel,R_exuk,
     & R_ixuk,R_oxuk,R_uxuk,REAL(R_esus,4),
     & R_abal,L_ixid,L_exid,L_axid,L_oxid,L_uxid,L_abod,
     & L_evid,L_avid,L_utid,L_ivid,L_ovid,L_uvid,I_aduk,
     & I_ubuk,I_ebal,I_ibal,I_obal,I_ubal,I_adal,
     & I_ebuk,I_ibuk,I_obuk,I_akuk,I_ufuk,
     & I_efal,I_ifal,I_ofal,I_ufal,I_akal,I_efuk,
     & I_ifuk,I_ofuk,I_amuk,I_uluk,I_elal,
     & I_ilal,I_olal,I_ulal,I_amal,I_eluk,I_iluk,
     & I_oluk,I_aruk,I_upuk,I_epal,I_ipal,
     & I_opal,I_upal,I_aral,I_epuk,I_ipuk,
     & I_opuk,I_atuk,I_usuk,I_esal,I_isal,
     & I_osal,I_usal,I_atal,I_esuk,I_isuk,
     & I_osuk,I_axuk,I_uvuk,I_eval,I_ival,
     & I_oval,I_uval,I_axal,I_evuk,I_ivuk,
     & I_ovuk,I_olel,I_ulel,I_amel,
     & I_emel,I_imel,3,I_opel,
     & I_upel,I_arel,I_erel,
     & I_irel)
C FDA_10boats.fgi( 180, 230):���������� ���������� ������� �� ������� FDA60,BOAT03
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_epit,R_ulit,L_evit,L_emebe,
     & L_omabe,L_imabe,R_ipit,R_amit,
     & L_ivit,L_emabe,R_opit,R_emit,
     & L_ovit,I_uvit,I_axit,I_exit,I_ixit,I_oxit,
     & INT(I_akebe,4),L_ekebe,L_odit,
     & L_udit,L_afit,L_efit,
     & L_ifit,L_ofit,L_ufit,
     & L_akit,L_ekit,L_ikit,I_(4),I_exet,
     & R_uxit,REAL(R_afebe,4),R_abot,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_ebot,R_ibot,
     & R_obot,REAL(R_ikebe,4),R_ubot,
     & REAL(R_okebe,4),R_omit,R_elit,L_otit,L_imit,
     & L_imebe,REAL(R_omebe,4),R_axet,R_edot,
     & R_idot,R_odot,REAL(R_uvabe,4),R_udot,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_afot,
     & REAL(R_avabe,4),R_efot,R_umit,R_ilit,
     & L_utit,R_ifot,R_ofot,R_ufot,L_obit,
     & R_akot,R_ekot,REAL(R_erabe,4),R_ikot,R_apit,
     & R_olit,L_avit,R_okot,R_ukot,R_alot,R_elot,
     & R_ilot,REAL(R_esabe,4),R_olot,L_ubit,
     & L_upit,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_ulot,R_amot,R_emot,REAL(R_etabe,4),R_imot,
     & L_adit,L_arit,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_omot,R_umot,
     & R_apot,R_epot,REAL(R_epabe,4),R_ipot,
     & L_ibit,L_arabe,L_orabe,L_erit,
     & L_irit,L_apabe,R_emet,R_esit,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_isit,I_oxet,R_oset,R_osit,R_opot,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_adot,R_uret,C20_atet
     &)
C FDA_10boats.fgi( 159, 276):���������� �������,BOAT04
      I_id=I_(4)
C FDA_10boats.fgi( 159, 276):������-�������: ���������� ��� �������������� ������,cpy BOAT04:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_uvaf,R_abuk
     &,
     & I_uxok,R_exel,I_axaf,I_axel,I_exaf,
     & I_adel,R_ifel,I_edel,
     & R_ofel,I_idel,R_ufel,
     & I_odel,R_akel,I_udel,
     & R_ekel,I_afel,R_ikel,
     & I_efel,R_okel,I_arud,
     & R_ibel,I_ebel,I_erud,
     & R_ubel,I_obel,R_usel,
     & I_osel,I_irud,R_elel,
     & I_ukel,I_orud,R_ilel,
     & I_alel,I_urud,R_etel,I_atel,
     & I_asud,R_ivel,I_evel,I_esud,
     & R_otel,I_itel,I_isud,
     & R_uvel,I_ovel,I_osud,
     & R_avel,I_utel,I_usud,I_atud,
     & I_etud,I_itud,I_otud,I_utud,I_avud,I_evud,
     & I_ivud,I_ovud,I_uvud,I_axud,I_exud,I_ixud,
     & I_oxud,I_uxud,I_abaf,I_ebaf,I_ibaf,I_obaf,
     & I_ubaf,I_adaf,I_edaf,I_idaf,I_odaf,I_udaf,
     & I_afaf,I_efaf,I_ifaf,I_ofaf,I_ufaf,I_akaf,
     & I_ekaf,I_ikaf,I_okaf,I_ukaf,I_alaf,I_elaf,
     & I_ilaf,I_olaf,I_ulaf,I_amaf,I_emaf,I_imaf,
     & I_omaf,I_umaf,I_apaf,I_epaf,I_ipaf,I_opaf,
     & I_upaf,I_araf,I_eraf,I_iraf,I_oraf,I_uraf,
     & I_asaf,I_esaf,I_isaf,I_osaf,I_usaf,I_ataf,
     & I_etaf,I_itaf,I_otaf,I_utaf,I_avaf,I_evaf,
     & I_ivaf,I_ovaf,INT(I_id,4),R_afuk,R_uduk,
     & R_oduk,R_iduk,R_eduk,R_afal,
     & R_udal,R_odal,R_idal,R_edal,R_ekal,
     & R_ikal,R_okal,R_ukal,R_alal,
     & R_ekuk,R_ikuk,R_okuk,R_ukuk,
     & R_aluk,R_emal,R_imal,R_omal,
     & R_umal,R_apal,R_emuk,R_imuk,
     & R_omuk,R_umuk,R_apuk,R_isel,
     & R_esel,R_asel,R_urel,
     & R_orel,R_ipel,R_epel,
     & R_apel,R_umel,R_omel,
     & R_eral,R_iral,R_oral,R_ural,R_asal,
     & R_eruk,R_iruk,R_oruk,R_uruk,
     & R_asuk,R_avuk,R_utuk,R_otuk,
     & R_ituk,R_etuk,R_aval,R_utal,
     & R_otal,R_ital,R_etal,R_exal,R_ixal,
     & R_oxal,R_uxal,R_abel,R_exuk,
     & R_ixuk,R_oxuk,R_uxuk,REAL(R_axet,4),
     & R_abal,L_epud,L_apud,L_umud,L_ipud,L_opud,L_upud,
     & L_amud,L_ulud,L_olud,L_emud,L_imud,L_omud,I_aduk,
     & I_ubuk,I_ebal,I_ibal,I_obal,I_ubal,I_adal,
     & I_ebuk,I_ibuk,I_obuk,I_akuk,I_ufuk,
     & I_efal,I_ifal,I_ofal,I_ufal,I_akal,I_efuk,
     & I_ifuk,I_ofuk,I_amuk,I_uluk,I_elal,
     & I_ilal,I_olal,I_ulal,I_amal,I_eluk,I_iluk,
     & I_oluk,I_aruk,I_upuk,I_epal,I_ipal,
     & I_opal,I_upal,I_aral,I_epuk,I_ipuk,
     & I_opuk,I_atuk,I_usuk,I_esal,I_isal,
     & I_osal,I_usal,I_atal,I_esuk,I_isuk,
     & I_osuk,I_axuk,I_uvuk,I_eval,I_ival,
     & I_oval,I_uval,I_axal,I_evuk,I_ivuk,
     & I_ovuk,I_olel,I_ulel,I_amel,
     & I_emel,I_imel,4,I_opel,
     & I_upel,I_arel,I_erel,
     & I_irel)
C FDA_10boats.fgi( 160, 230):���������� ���������� ������� �� ������� FDA60,BOAT04
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_atut,R_orut,L_adav,L_emebe,
     & L_omabe,L_imabe,R_etut,R_urut,
     & L_edav,L_emabe,R_itut,R_asut,
     & L_idav,I_odav,I_udav,I_afav,I_efav,I_ifav,
     & INT(I_akebe,4),L_ekebe,L_ilut,
     & L_olut,L_ulut,L_amut,
     & L_emut,L_imut,L_omut,
     & L_umut,L_aput,L_eput,I_(5),I_afut,
     & R_ofav,REAL(R_afebe,4),R_ufav,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_akav,R_ekav,
     & R_ikav,REAL(R_ikebe,4),R_okav,
     & REAL(R_okebe,4),R_isut,R_arut,L_ibav,L_esut,
     & L_imebe,REAL(R_omebe,4),R_udut,R_alav,
     & R_elav,R_ilav,REAL(R_uvabe,4),R_olav,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_ulav,
     & REAL(R_avabe,4),R_amav,R_osut,R_erut,
     & L_obav,R_emav,R_imav,R_omav,L_ikut,
     & R_umav,R_apav,REAL(R_erabe,4),R_epav,R_usut,
     & R_irut,L_ubav,R_ipav,R_opav,R_upav,R_arav,
     & R_erav,REAL(R_esabe,4),R_irav,L_okut,
     & L_otut,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_orav,R_urav,R_asav,REAL(R_etabe,4),R_esav,
     & L_ukut,L_utut,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_isav,R_osav,
     & R_usav,R_atav,REAL(R_epabe,4),R_etav,
     & L_ekut,L_arabe,L_orabe,L_avut,
     & L_evut,L_apabe,R_asot,R_axut,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_exut,I_ifut,R_ixot,R_ixut,R_itav,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_ukav,R_ovot,C20_uxot
     &)
C FDA_10boats.fgi( 139, 276):���������� �������,BOAT05
      I_ed=I_(5)
C FDA_10boats.fgi( 139, 276):������-�������: ���������� ��� �������������� ������,cpy BOAT05:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_omif,R_abuk
     &,
     & I_uxok,R_exel,I_umif,I_axel,I_apif,
     & I_adel,R_ifel,I_edel,
     & R_ofel,I_idel,R_ufel,
     & I_odel,R_akel,I_udel,
     & R_ekel,I_afel,R_ikel,
     & I_efel,R_okel,I_udef,
     & R_ibel,I_ebel,I_afef,
     & R_ubel,I_obel,R_usel,
     & I_osel,I_efef,R_elel,
     & I_ukel,I_ifef,R_ilel,
     & I_alel,I_ofef,R_etel,I_atel,
     & I_ufef,R_ivel,I_evel,I_akef,
     & R_otel,I_itel,I_ekef,
     & R_uvel,I_ovel,I_ikef,
     & R_avel,I_utel,I_okef,I_ukef,
     & I_alef,I_elef,I_ilef,I_olef,I_ulef,I_amef,
     & I_emef,I_imef,I_omef,I_umef,I_apef,I_epef,
     & I_ipef,I_opef,I_upef,I_aref,I_eref,I_iref,
     & I_oref,I_uref,I_asef,I_esef,I_isef,I_osef,
     & I_usef,I_atef,I_etef,I_itef,I_otef,I_utef,
     & I_avef,I_evef,I_ivef,I_ovef,I_uvef,I_axef,
     & I_exef,I_ixef,I_oxef,I_uxef,I_abif,I_ebif,
     & I_ibif,I_obif,I_ubif,I_adif,I_edif,I_idif,
     & I_odif,I_udif,I_afif,I_efif,I_ifif,I_ofif,
     & I_ufif,I_akif,I_ekif,I_ikif,I_okif,I_ukif,
     & I_alif,I_elif,I_ilif,I_olif,I_ulif,I_amif,
     & I_emif,I_imif,INT(I_ed,4),R_afuk,R_uduk,
     & R_oduk,R_iduk,R_eduk,R_afal,
     & R_udal,R_odal,R_idal,R_edal,R_ekal,
     & R_ikal,R_okal,R_ukal,R_alal,
     & R_ekuk,R_ikuk,R_okuk,R_ukuk,
     & R_aluk,R_emal,R_imal,R_omal,
     & R_umal,R_apal,R_emuk,R_imuk,
     & R_omuk,R_umuk,R_apuk,R_isel,
     & R_esel,R_asel,R_urel,
     & R_orel,R_ipel,R_epel,
     & R_apel,R_umel,R_omel,
     & R_eral,R_iral,R_oral,R_ural,R_asal,
     & R_eruk,R_iruk,R_oruk,R_uruk,
     & R_asuk,R_avuk,R_utuk,R_otuk,
     & R_ituk,R_etuk,R_aval,R_utal,
     & R_otal,R_ital,R_etal,R_exal,R_ixal,
     & R_oxal,R_uxal,R_abel,R_exuk,
     & R_ixuk,R_oxuk,R_uxuk,REAL(R_udut,4),
     & R_abal,L_adef,L_ubef,L_obef,L_edef,L_idef,L_odef,
     & L_uxaf,L_oxaf,L_ixaf,L_abef,L_ebef,L_ibef,I_aduk,
     & I_ubuk,I_ebal,I_ibal,I_obal,I_ubal,I_adal,
     & I_ebuk,I_ibuk,I_obuk,I_akuk,I_ufuk,
     & I_efal,I_ifal,I_ofal,I_ufal,I_akal,I_efuk,
     & I_ifuk,I_ofuk,I_amuk,I_uluk,I_elal,
     & I_ilal,I_olal,I_ulal,I_amal,I_eluk,I_iluk,
     & I_oluk,I_aruk,I_upuk,I_epal,I_ipal,
     & I_opal,I_upal,I_aral,I_epuk,I_ipuk,
     & I_opuk,I_atuk,I_usuk,I_esal,I_isal,
     & I_osal,I_usal,I_atal,I_esuk,I_isuk,
     & I_osuk,I_axuk,I_uvuk,I_eval,I_ival,
     & I_oval,I_uval,I_axal,I_evuk,I_ivuk,
     & I_ovuk,I_olel,I_ulel,I_amel,
     & I_emel,I_imel,5,I_opel,
     & I_upel,I_arel,I_erel,
     & I_irel)
C FDA_10boats.fgi( 140, 230):���������� ���������� ������� �� ������� FDA60,BOAT05
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_uxev,R_ivev,L_ukiv,L_emebe,
     & L_omabe,L_imabe,R_abiv,R_ovev,
     & L_aliv,L_emabe,R_ebiv,R_uvev,
     & L_eliv,I_iliv,I_oliv,I_uliv,I_amiv,I_emiv,
     & INT(I_akebe,4),L_ekebe,L_erev,
     & L_irev,L_orev,L_urev,
     & L_asev,L_esev,L_isev,
     & L_osev,L_usev,L_atev,I_(6),I_ulev,
     & R_imiv,REAL(R_afebe,4),R_omiv,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_umiv,R_apiv,
     & R_epiv,REAL(R_ikebe,4),R_ipiv,
     & REAL(R_okebe,4),R_exev,R_utev,L_ekiv,L_axev,
     & L_imebe,REAL(R_omebe,4),R_olev,R_upiv,
     & R_ariv,R_eriv,REAL(R_uvabe,4),R_iriv,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_oriv,
     & REAL(R_avabe,4),R_uriv,R_ixev,R_avev,
     & L_ikiv,R_asiv,R_esiv,R_isiv,L_epev,
     & R_osiv,R_usiv,REAL(R_erabe,4),R_ativ,R_oxev,
     & R_evev,L_okiv,R_etiv,R_itiv,R_otiv,R_utiv,
     & R_aviv,REAL(R_esabe,4),R_eviv,L_ipev,
     & L_ibiv,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_iviv,R_oviv,R_uviv,REAL(R_etabe,4),R_axiv,
     & L_opev,L_obiv,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_exiv,R_ixiv,
     & R_oxiv,R_uxiv,REAL(R_epabe,4),R_abov,
     & L_apev,L_arabe,L_orabe,L_ubiv,
     & L_adiv,L_apabe,R_uvav,R_udiv,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_afiv,I_emev,R_efev,R_efiv,R_ebov,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_opiv,R_idev,C20_ofev
     &)
C FDA_10boats.fgi( 119, 276):���������� �������,BOAT06
      I_ad=I_(6)
C FDA_10boats.fgi( 119, 276):������-�������: ���������� ��� �������������� ������,cpy BOAT06:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_ibuf,R_abuk
     &,
     & I_uxok,R_exel,I_obuf,I_axel,I_ubuf,
     & I_adel,R_ifel,I_edel,
     & R_ofel,I_idel,R_ufel,
     & I_odel,R_akel,I_udel,
     & R_ekel,I_afel,R_ikel,
     & I_efel,R_okel,I_osif,
     & R_ibel,I_ebel,I_usif,
     & R_ubel,I_obel,R_usel,
     & I_osel,I_atif,R_elel,
     & I_ukel,I_etif,R_ilel,
     & I_alel,I_itif,R_etel,I_atel,
     & I_otif,R_ivel,I_evel,I_utif,
     & R_otel,I_itel,I_avif,
     & R_uvel,I_ovel,I_evif,
     & R_avel,I_utel,I_ivif,I_ovif,
     & I_uvif,I_axif,I_exif,I_ixif,I_oxif,I_uxif,
     & I_abof,I_ebof,I_ibof,I_obof,I_ubof,I_adof,
     & I_edof,I_idof,I_odof,I_udof,I_afof,I_efof,
     & I_ifof,I_ofof,I_ufof,I_akof,I_ekof,I_ikof,
     & I_okof,I_ukof,I_alof,I_elof,I_ilof,I_olof,
     & I_ulof,I_amof,I_emof,I_imof,I_omof,I_umof,
     & I_apof,I_epof,I_ipof,I_opof,I_upof,I_arof,
     & I_erof,I_irof,I_orof,I_urof,I_asof,I_esof,
     & I_isof,I_osof,I_usof,I_atof,I_etof,I_itof,
     & I_otof,I_utof,I_avof,I_evof,I_ivof,I_ovof,
     & I_uvof,I_axof,I_exof,I_ixof,I_oxof,I_uxof,
     & I_abuf,I_ebuf,INT(I_ad,4),R_afuk,R_uduk,
     & R_oduk,R_iduk,R_eduk,R_afal,
     & R_udal,R_odal,R_idal,R_edal,R_ekal,
     & R_ikal,R_okal,R_ukal,R_alal,
     & R_ekuk,R_ikuk,R_okuk,R_ukuk,
     & R_aluk,R_emal,R_imal,R_omal,
     & R_umal,R_apal,R_emuk,R_imuk,
     & R_omuk,R_umuk,R_apuk,R_isel,
     & R_esel,R_asel,R_urel,
     & R_orel,R_ipel,R_epel,
     & R_apel,R_umel,R_omel,
     & R_eral,R_iral,R_oral,R_ural,R_asal,
     & R_eruk,R_iruk,R_oruk,R_uruk,
     & R_asuk,R_avuk,R_utuk,R_otuk,
     & R_ituk,R_etuk,R_aval,R_utal,
     & R_otal,R_ital,R_etal,R_exal,R_ixal,
     & R_oxal,R_uxal,R_abel,R_exuk,
     & R_ixuk,R_oxuk,R_uxuk,REAL(R_olev,4),
     & R_abal,L_urif,L_orif,L_irif,L_asif,L_esif,L_isif,
     & L_opif,L_ipif,L_epif,L_upif,L_arif,L_erif,I_aduk,
     & I_ubuk,I_ebal,I_ibal,I_obal,I_ubal,I_adal,
     & I_ebuk,I_ibuk,I_obuk,I_akuk,I_ufuk,
     & I_efal,I_ifal,I_ofal,I_ufal,I_akal,I_efuk,
     & I_ifuk,I_ofuk,I_amuk,I_uluk,I_elal,
     & I_ilal,I_olal,I_ulal,I_amal,I_eluk,I_iluk,
     & I_oluk,I_aruk,I_upuk,I_epal,I_ipal,
     & I_opal,I_upal,I_aral,I_epuk,I_ipuk,
     & I_opuk,I_atuk,I_usuk,I_esal,I_isal,
     & I_osal,I_usal,I_atal,I_esuk,I_isuk,
     & I_osuk,I_axuk,I_uvuk,I_eval,I_ival,
     & I_oval,I_uval,I_axal,I_evuk,I_ivuk,
     & I_ovuk,I_olel,I_ulel,I_amel,
     & I_emel,I_imel,6,I_opel,
     & I_upel,I_arel,I_erel,
     & I_irel)
C FDA_10boats.fgi( 120, 230):���������� ���������� ������� �� ������� FDA60,BOAT06
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_ofuv,R_eduv,L_opuv,L_emebe,
     & L_omabe,L_imabe,R_ufuv,R_iduv,
     & L_upuv,L_emabe,R_akuv,R_oduv,
     & L_aruv,I_eruv,I_iruv,I_oruv,I_uruv,I_asuv,
     & INT(I_akebe,4),L_ekebe,L_avov,
     & L_evov,L_ivov,L_ovov,
     & L_uvov,L_axov,L_exov,
     & L_ixov,L_oxov,L_uxov,I_(7),I_orov,
     & R_esuv,REAL(R_afebe,4),R_isuv,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_osuv,R_usuv,
     & R_atuv,REAL(R_ikebe,4),R_etuv,
     & REAL(R_okebe,4),R_afuv,R_obuv,L_apuv,L_uduv,
     & L_imebe,REAL(R_omebe,4),R_irov,R_otuv,
     & R_utuv,R_avuv,REAL(R_uvabe,4),R_evuv,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_ivuv,
     & REAL(R_avabe,4),R_ovuv,R_efuv,R_ubuv,
     & L_epuv,R_uvuv,R_axuv,R_exuv,L_atov,
     & R_ixuv,R_oxuv,REAL(R_erabe,4),R_uxuv,R_ifuv,
     & R_aduv,L_ipuv,R_abax,R_ebax,R_ibax,R_obax,
     & R_ubax,REAL(R_esabe,4),R_adax,L_etov,
     & L_ekuv,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_edax,R_idax,R_odax,REAL(R_etabe,4),R_udax,
     & L_itov,L_ikuv,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_afax,R_efax,
     & R_ifax,R_ofax,REAL(R_epabe,4),R_ufax,
     & L_usov,L_arabe,L_orabe,L_okuv,
     & L_ukuv,L_apabe,R_odov,R_oluv,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_uluv,I_asov,R_amov,R_amuv,R_akax,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_ituv,R_elov,C20_imov
     &)
C FDA_10boats.fgi(  99, 276):���������� �������,BOAT07
      I_u=I_(7)
C FDA_10boats.fgi(  99, 276):������-�������: ���������� ��� �������������� ������,cpy BOAT07:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_erak,R_abuk
     &,
     & I_uxok,R_exel,I_irak,I_axel,I_orak,
     & I_adel,R_ifel,I_edel,
     & R_ofel,I_idel,R_ufel,
     & I_odel,R_akel,I_udel,
     & R_ekel,I_afel,R_ikel,
     & I_efel,R_okel,I_ikuf,
     & R_ibel,I_ebel,I_okuf,
     & R_ubel,I_obel,R_usel,
     & I_osel,I_ukuf,R_elel,
     & I_ukel,I_aluf,R_ilel,
     & I_alel,I_eluf,R_etel,I_atel,
     & I_iluf,R_ivel,I_evel,I_oluf,
     & R_otel,I_itel,I_uluf,
     & R_uvel,I_ovel,I_amuf,
     & R_avel,I_utel,I_emuf,I_imuf,
     & I_omuf,I_umuf,I_apuf,I_epuf,I_ipuf,I_opuf,
     & I_upuf,I_aruf,I_eruf,I_iruf,I_oruf,I_uruf,
     & I_asuf,I_esuf,I_isuf,I_osuf,I_usuf,I_atuf,
     & I_etuf,I_ituf,I_otuf,I_utuf,I_avuf,I_evuf,
     & I_ivuf,I_ovuf,I_uvuf,I_axuf,I_exuf,I_ixuf,
     & I_oxuf,I_uxuf,I_abak,I_ebak,I_ibak,I_obak,
     & I_ubak,I_adak,I_edak,I_idak,I_odak,I_udak,
     & I_afak,I_efak,I_ifak,I_ofak,I_ufak,I_akak,
     & I_ekak,I_ikak,I_okak,I_ukak,I_alak,I_elak,
     & I_ilak,I_olak,I_ulak,I_amak,I_emak,I_imak,
     & I_omak,I_umak,I_apak,I_epak,I_ipak,I_opak,
     & I_upak,I_arak,INT(I_u,4),R_afuk,R_uduk,
     & R_oduk,R_iduk,R_eduk,R_afal,
     & R_udal,R_odal,R_idal,R_edal,R_ekal,
     & R_ikal,R_okal,R_ukal,R_alal,
     & R_ekuk,R_ikuk,R_okuk,R_ukuk,
     & R_aluk,R_emal,R_imal,R_omal,
     & R_umal,R_apal,R_emuk,R_imuk,
     & R_omuk,R_umuk,R_apuk,R_isel,
     & R_esel,R_asel,R_urel,
     & R_orel,R_ipel,R_epel,
     & R_apel,R_umel,R_omel,
     & R_eral,R_iral,R_oral,R_ural,R_asal,
     & R_eruk,R_iruk,R_oruk,R_uruk,
     & R_asuk,R_avuk,R_utuk,R_otuk,
     & R_ituk,R_etuk,R_aval,R_utal,
     & R_otal,R_ital,R_etal,R_exal,R_ixal,
     & R_oxal,R_uxal,R_abel,R_exuk,
     & R_ixuk,R_oxuk,R_uxuk,REAL(R_irov,4),
     & R_abal,L_ofuf,L_ifuf,L_efuf,L_ufuf,L_akuf,L_ekuf,
     & L_iduf,L_eduf,L_aduf,L_oduf,L_uduf,L_afuf,I_aduk,
     & I_ubuk,I_ebal,I_ibal,I_obal,I_ubal,I_adal,
     & I_ebuk,I_ibuk,I_obuk,I_akuk,I_ufuk,
     & I_efal,I_ifal,I_ofal,I_ufal,I_akal,I_efuk,
     & I_ifuk,I_ofuk,I_amuk,I_uluk,I_elal,
     & I_ilal,I_olal,I_ulal,I_amal,I_eluk,I_iluk,
     & I_oluk,I_aruk,I_upuk,I_epal,I_ipal,
     & I_opal,I_upal,I_aral,I_epuk,I_ipuk,
     & I_opuk,I_atuk,I_usuk,I_esal,I_isal,
     & I_osal,I_usal,I_atal,I_esuk,I_isuk,
     & I_osuk,I_axuk,I_uvuk,I_eval,I_ival,
     & I_oval,I_uval,I_axal,I_evuk,I_ivuk,
     & I_ovuk,I_olel,I_ulel,I_amel,
     & I_emel,I_imel,7,I_opel,
     & I_upel,I_arel,I_erel,
     & I_irel)
C FDA_10boats.fgi( 100, 230):���������� ���������� ������� �� ������� FDA60,BOAT07
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_imex,R_alex,L_itex,L_emebe,
     & L_omabe,L_imabe,R_omex,R_elex,
     & L_otex,L_emabe,R_umex,R_ilex,
     & L_utex,I_avex,I_evex,I_ivex,I_ovex,I_uvex,
     & INT(I_akebe,4),L_ekebe,L_ubex,
     & L_adex,L_edex,L_idex,
     & L_odex,L_udex,L_afex,
     & L_efex,L_ifex,L_ofex,I_(8),I_ivax,
     & R_axex,REAL(R_afebe,4),R_exex,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_ixex,R_oxex,
     & R_uxex,REAL(R_ikebe,4),R_abix,
     & REAL(R_okebe,4),R_ulex,R_ikex,L_usex,L_olex,
     & L_imebe,REAL(R_omebe,4),R_evax,R_ibix,
     & R_obix,R_ubix,REAL(R_uvabe,4),R_adix,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_edix,
     & REAL(R_avabe,4),R_idix,R_amex,R_okex,
     & L_atex,R_odix,R_udix,R_afix,L_uxax,
     & R_efix,R_ifix,REAL(R_erabe,4),R_ofix,R_emex,
     & R_ukex,L_etex,R_ufix,R_akix,R_ekix,R_ikix,
     & R_okix,REAL(R_esabe,4),R_ukix,L_abex,
     & L_apex,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_alix,R_elix,R_ilix,REAL(R_etabe,4),R_olix,
     & L_ebex,L_epex,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_ulix,R_amix,
     & R_emix,R_imix,REAL(R_epabe,4),R_omix,
     & L_oxax,L_arabe,L_orabe,L_ipex,
     & L_opex,L_apabe,R_ilax,R_irex,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_orex,I_uvax,R_urax,R_urex,R_umix,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_ebix,R_arax,C20_esax
     &)
C FDA_10boats.fgi(  79, 276):���������� �������,BOAT08
      I_o=I_(8)
C FDA_10boats.fgi(  79, 276):������-�������: ���������� ��� �������������� ������,cpy BOAT08:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_afik,R_abuk
     &,
     & I_uxok,R_exel,I_efik,I_axel,I_ifik,
     & I_adel,R_ifel,I_edel,
     & R_ofel,I_idel,R_ufel,
     & I_odel,R_akel,I_udel,
     & R_ekel,I_afel,R_ikel,
     & I_efel,R_okel,I_evak,
     & R_ibel,I_ebel,I_ivak,
     & R_ubel,I_obel,R_usel,
     & I_osel,I_ovak,R_elel,
     & I_ukel,I_uvak,R_ilel,
     & I_alel,I_axak,R_etel,I_atel,
     & I_exak,R_ivel,I_evel,I_ixak,
     & R_otel,I_itel,I_oxak,
     & R_uvel,I_ovel,I_uxak,
     & R_avel,I_utel,I_abek,I_ebek,
     & I_ibek,I_obek,I_ubek,I_adek,I_edek,I_idek,
     & I_odek,I_udek,I_afek,I_efek,I_ifek,I_ofek,
     & I_ufek,I_akek,I_ekek,I_ikek,I_okek,I_ukek,
     & I_alek,I_elek,I_ilek,I_olek,I_ulek,I_amek,
     & I_emek,I_imek,I_omek,I_umek,I_apek,I_epek,
     & I_ipek,I_opek,I_upek,I_arek,I_erek,I_irek,
     & I_orek,I_urek,I_asek,I_esek,I_isek,I_osek,
     & I_usek,I_atek,I_etek,I_itek,I_otek,I_utek,
     & I_avek,I_evek,I_ivek,I_ovek,I_uvek,I_axek,
     & I_exek,I_ixek,I_oxek,I_uxek,I_abik,I_ebik,
     & I_ibik,I_obik,I_ubik,I_adik,I_edik,I_idik,
     & I_odik,I_udik,INT(I_o,4),R_afuk,R_uduk,
     & R_oduk,R_iduk,R_eduk,R_afal,
     & R_udal,R_odal,R_idal,R_edal,R_ekal,
     & R_ikal,R_okal,R_ukal,R_alal,
     & R_ekuk,R_ikuk,R_okuk,R_ukuk,
     & R_aluk,R_emal,R_imal,R_omal,
     & R_umal,R_apal,R_emuk,R_imuk,
     & R_omuk,R_umuk,R_apuk,R_isel,
     & R_esel,R_asel,R_urel,
     & R_orel,R_ipel,R_epel,
     & R_apel,R_umel,R_omel,
     & R_eral,R_iral,R_oral,R_ural,R_asal,
     & R_eruk,R_iruk,R_oruk,R_uruk,
     & R_asuk,R_avuk,R_utuk,R_otuk,
     & R_ituk,R_etuk,R_aval,R_utal,
     & R_otal,R_ital,R_etal,R_exal,R_ixal,
     & R_oxal,R_uxal,R_abel,R_exuk,
     & R_ixuk,R_oxuk,R_uxuk,REAL(R_evax,4),
     & R_abal,L_itak,L_etak,L_atak,L_otak,L_utak,L_avak,
     & L_esak,L_asak,L_urak,L_isak,L_osak,L_usak,I_aduk,
     & I_ubuk,I_ebal,I_ibal,I_obal,I_ubal,I_adal,
     & I_ebuk,I_ibuk,I_obuk,I_akuk,I_ufuk,
     & I_efal,I_ifal,I_ofal,I_ufal,I_akal,I_efuk,
     & I_ifuk,I_ofuk,I_amuk,I_uluk,I_elal,
     & I_ilal,I_olal,I_ulal,I_amal,I_eluk,I_iluk,
     & I_oluk,I_aruk,I_upuk,I_epal,I_ipal,
     & I_opal,I_upal,I_aral,I_epuk,I_ipuk,
     & I_opuk,I_atuk,I_usuk,I_esal,I_isal,
     & I_osal,I_usal,I_atal,I_esuk,I_isuk,
     & I_osuk,I_axuk,I_uvuk,I_eval,I_ival,
     & I_oval,I_uval,I_axal,I_evuk,I_ivuk,
     & I_ovuk,I_olel,I_ulel,I_amel,
     & I_emel,I_imel,8,I_opel,
     & I_upel,I_arel,I_erel,
     & I_irel)
C FDA_10boats.fgi(  80, 230):���������� ���������� ������� �� ������� FDA60,BOAT08
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_esox,R_upox,L_ebux,L_emebe,
     & L_omabe,L_imabe,R_isox,R_arox,
     & L_ibux,L_emabe,R_osox,R_erox,
     & L_obux,I_ubux,I_adux,I_edux,I_idux,I_odux,
     & INT(I_akebe,4),L_ekebe,L_okox,
     & L_ukox,L_alox,L_elox,
     & L_ilox,L_olox,L_ulox,
     & L_amox,L_emox,L_imox,I_(9),I_edox,
     & R_udux,REAL(R_afebe,4),R_afux,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_efux,R_ifux,
     & R_ofux,REAL(R_ikebe,4),R_ufux,
     & REAL(R_okebe,4),R_orox,R_epox,L_oxox,L_irox,
     & L_imebe,REAL(R_omebe,4),R_adox,R_ekux,
     & R_ikux,R_okux,REAL(R_uvabe,4),R_ukux,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_alux,
     & REAL(R_avabe,4),R_elux,R_urox,R_ipox,
     & L_uxox,R_ilux,R_olux,R_ulux,L_ofox,
     & R_amux,R_emux,REAL(R_erabe,4),R_imux,R_asox,
     & R_opox,L_abux,R_omux,R_umux,R_apux,R_epux,
     & R_ipux,REAL(R_esabe,4),R_opux,L_ufox,
     & L_usox,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_upux,R_arux,R_erux,REAL(R_etabe,4),R_irux,
     & L_akox,L_atox,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_orux,R_urux,
     & R_asux,R_esux,REAL(R_epabe,4),R_isux,
     & L_ifox,L_arabe,L_orabe,L_etox,
     & L_itox,L_apabe,R_erix,R_evox,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_ivox,I_odox,R_ovix,R_ovox,R_osux,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_akux,R_utix,C20_axix
     &)
C FDA_10boats.fgi(  59, 276):���������� �������,BOAT09
      I_i=I_(9)
C FDA_10boats.fgi(  59, 276):������-�������: ���������� ��� �������������� ������,cpy BOAT09:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_usok,R_abuk
     &,
     & I_uxok,R_exel,I_atok,I_axel,I_etok,
     & I_adel,R_ifel,I_edel,
     & R_ofel,I_idel,R_ufel,
     & I_odel,R_akel,I_udel,
     & R_ekel,I_afel,R_ikel,
     & I_efel,R_okel,I_amik,
     & R_ibel,I_ebel,I_emik,
     & R_ubel,I_obel,R_usel,
     & I_osel,I_imik,R_elel,
     & I_ukel,I_omik,R_ilel,
     & I_alel,I_umik,R_etel,I_atel,
     & I_apik,R_ivel,I_evel,I_epik,
     & R_otel,I_itel,I_ipik,
     & R_uvel,I_ovel,I_opik,
     & R_avel,I_utel,I_upik,I_arik,
     & I_erik,I_irik,I_orik,I_urik,I_asik,I_esik,
     & I_isik,I_osik,I_usik,I_atik,I_etik,I_itik,
     & I_otik,I_utik,I_avik,I_evik,I_ivik,I_ovik,
     & I_uvik,I_axik,I_exik,I_ixik,I_oxik,I_uxik,
     & I_abok,I_ebok,I_ibok,I_obok,I_ubok,I_adok,
     & I_edok,I_idok,I_odok,I_udok,I_afok,I_efok,
     & I_ifok,I_ofok,I_ufok,I_akok,I_ekok,I_ikok,
     & I_okok,I_ukok,I_alok,I_elok,I_ilok,I_olok,
     & I_ulok,I_amok,I_emok,I_imok,I_omok,I_umok,
     & I_apok,I_epok,I_ipok,I_opok,I_upok,I_arok,
     & I_erok,I_irok,I_orok,I_urok,I_asok,I_esok,
     & I_isok,I_osok,INT(I_i,4),R_afuk,R_uduk,
     & R_oduk,R_iduk,R_eduk,R_afal,
     & R_udal,R_odal,R_idal,R_edal,R_ekal,
     & R_ikal,R_okal,R_ukal,R_alal,
     & R_ekuk,R_ikuk,R_okuk,R_ukuk,
     & R_aluk,R_emal,R_imal,R_omal,
     & R_umal,R_apal,R_emuk,R_imuk,
     & R_omuk,R_umuk,R_apuk,R_isel,
     & R_esel,R_asel,R_urel,
     & R_orel,R_ipel,R_epel,
     & R_apel,R_umel,R_omel,
     & R_eral,R_iral,R_oral,R_ural,R_asal,
     & R_eruk,R_iruk,R_oruk,R_uruk,
     & R_asuk,R_avuk,R_utuk,R_otuk,
     & R_ituk,R_etuk,R_aval,R_utal,
     & R_otal,R_ital,R_etal,R_exal,R_ixal,
     & R_oxal,R_uxal,R_abel,R_exuk,
     & R_ixuk,R_oxuk,R_uxuk,REAL(R_adox,4),
     & R_abal,L_elik,L_alik,L_ukik,L_ilik,L_olik,L_ulik,
     & L_akik,L_ufik,L_ofik,L_ekik,L_ikik,L_okik,I_aduk,
     & I_ubuk,I_ebal,I_ibal,I_obal,I_ubal,I_adal,
     & I_ebuk,I_ibuk,I_obuk,I_akuk,I_ufuk,
     & I_efal,I_ifal,I_ofal,I_ufal,I_akal,I_efuk,
     & I_ifuk,I_ofuk,I_amuk,I_uluk,I_elal,
     & I_ilal,I_olal,I_ulal,I_amal,I_eluk,I_iluk,
     & I_oluk,I_aruk,I_upuk,I_epal,I_ipal,
     & I_opal,I_upal,I_aral,I_epuk,I_ipuk,
     & I_opuk,I_atuk,I_usuk,I_esal,I_isal,
     & I_osal,I_usal,I_atal,I_esuk,I_isuk,
     & I_osuk,I_axuk,I_uvuk,I_eval,I_ival,
     & I_oval,I_uval,I_axal,I_evuk,I_ivuk,
     & I_ovuk,I_olel,I_ulel,I_amel,
     & I_emel,I_imel,9,I_opel,
     & I_upel,I_arel,I_erel,
     & I_irel)
C FDA_10boats.fgi(  60, 230):���������� ���������� ������� �� ������� FDA60,BOAT09
      Call LODOCHKA_HANDLER(deltat,REAL(R_umebe,4),
     & L_olebe,L_alebe,
     & L_elebe,L_ulebe,L_ukebe,
     & L_ilebe,R_osebe,R_erebe,L_obibe,L_emebe,
     & L_omabe,L_imabe,R_usebe,R_irebe,
     & L_ubibe,L_emabe,R_atebe,R_orebe,
     & L_adibe,I_edibe,I_idibe,I_odibe,I_udibe,I_afibe,
     & INT(I_akebe,4),L_ekebe,L_ixabe,
     & L_oxabe,L_uxabe,L_abebe,
     & L_ebebe,L_ibebe,L_obebe,
     & L_ubebe,L_adebe,L_edebe,I_(10),I_alabe,
     & R_efibe,REAL(R_afebe,4),R_ifibe,
     & REAL(R_udebe,4),REAL(R_odebe,4),R_ofibe,R_ufibe,
     & R_akibe,REAL(R_ikebe,4),R_ekibe,
     & REAL(R_okebe,4),R_asebe,R_opebe,L_abibe,L_urebe,
     & L_imebe,REAL(R_omebe,4),R_ukabe,R_okibe,
     & R_ukibe,R_alibe,REAL(R_uvabe,4),R_elibe,
     & REAL(R_ivabe,4),REAL(R_utabe,4),R_ilibe,
     & REAL(R_avabe,4),R_olibe,R_esebe,R_upebe,
     & L_ebibe,R_ulibe,R_amibe,R_emibe,L_irabe,
     & R_imibe,R_omibe,REAL(R_erabe,4),R_umibe,R_isebe,
     & R_arebe,L_ibibe,R_apibe,R_epibe,R_ipibe,R_opibe,
     & R_upibe,REAL(R_esabe,4),R_aribe,L_isabe,
     & L_etebe,L_asabe,REAL(R_urabe,4),L_osabe,
     & R_eribe,R_iribe,R_oribe,REAL(R_etabe,4),R_uribe,
     & L_itabe,L_itebe,L_atabe,
     & REAL(R_usabe,4),L_otabe,R_asibe,R_esibe,
     & R_isibe,R_osibe,REAL(R_epabe,4),R_usibe,
     & L_ipabe,L_arabe,L_orabe,L_otebe,
     & L_utebe,L_apabe,R_avux,R_ovebe,
     & REAL(R_upabe,4),REAL(R_umabe,4),L_opabe,
     & R_uvebe,I_ilabe,R_idabe,R_axebe,R_atibe,
     & REAL(R_efebe,4),REAL(R_ofebe,4),R_ikibe,R_obabe,C20_udabe
     &)
C FDA_10boats.fgi(  39, 276):���������� �������,BOAT10
      I_e=I_(10)
C FDA_10boats.fgi(  39, 276):������-�������: ���������� ��� �������������� ������,cpy BOAT10:CR
      Call LODOCHKA_HANDLER_FDA60_COORD(deltat,I_ekol,R_abuk
     &,
     & I_uxok,R_exel,I_ikol,I_axel,I_okol,
     & I_adel,R_ifel,I_edel,
     & R_ofel,I_idel,R_ufel,
     & I_odel,R_akel,I_udel,
     & R_ekel,I_afel,R_ikel,
     & I_efel,R_okel,I_ixel,
     & R_ibel,I_ebel,I_oxel,
     & R_ubel,I_obel,R_usel,
     & I_osel,I_uxel,R_elel,
     & I_ukel,I_abil,R_ilel,
     & I_alel,I_ebil,R_etel,I_atel,
     & I_ibil,R_ivel,I_evel,I_obil,
     & R_otel,I_itel,I_ubil,
     & R_uvel,I_ovel,I_adil,
     & R_avel,I_utel,I_edil,I_idil,
     & I_odil,I_udil,I_afil,I_efil,I_ifil,I_ofil,
     & I_ufil,I_akil,I_ekil,I_ikil,I_okil,I_ukil,
     & I_alil,I_elil,I_ilil,I_olil,I_ulil,I_amil,
     & I_emil,I_imil,I_omil,I_umil,I_apil,I_epil,
     & I_ipil,I_opil,I_upil,I_aril,I_eril,I_iril,
     & I_oril,I_uril,I_asil,I_esil,I_isil,I_osil,
     & I_usil,I_atil,I_etil,I_itil,I_otil,I_util,
     & I_avil,I_evil,I_ivil,I_ovil,I_uvil,I_axil,
     & I_exil,I_ixil,I_oxil,I_uxil,I_abol,I_ebol,
     & I_ibol,I_obol,I_ubol,I_adol,I_edol,I_idol,
     & I_odol,I_udol,I_afol,I_efol,I_ifol,I_ofol,
     & I_ufol,I_akol,INT(I_e,4),R_afuk,R_uduk,
     & R_oduk,R_iduk,R_eduk,R_afal,
     & R_udal,R_odal,R_idal,R_edal,R_ekal,
     & R_ikal,R_okal,R_ukal,R_alal,
     & R_ekuk,R_ikuk,R_okuk,R_ukuk,
     & R_aluk,R_emal,R_imal,R_omal,
     & R_umal,R_apal,R_emuk,R_imuk,
     & R_omuk,R_umuk,R_apuk,R_isel,
     & R_esel,R_asel,R_urel,
     & R_orel,R_ipel,R_epel,
     & R_apel,R_umel,R_omel,
     & R_eral,R_iral,R_oral,R_ural,R_asal,
     & R_eruk,R_iruk,R_oruk,R_uruk,
     & R_asuk,R_avuk,R_utuk,R_otuk,
     & R_ituk,R_etuk,R_aval,R_utal,
     & R_otal,R_ital,R_etal,R_exal,R_ixal,
     & R_oxal,R_uxal,R_abel,R_exuk,
     & R_ixuk,R_oxuk,R_uxuk,REAL(R_ukabe,4),
     & R_abal,L_axok,L_uvok,L_ovok,L_exok,L_ixok,L_oxok,
     & L_utok,L_otok,L_itok,L_avok,L_evok,L_ivok,I_aduk,
     & I_ubuk,I_ebal,I_ibal,I_obal,I_ubal,I_adal,
     & I_ebuk,I_ibuk,I_obuk,I_akuk,I_ufuk,
     & I_efal,I_ifal,I_ofal,I_ufal,I_akal,I_efuk,
     & I_ifuk,I_ofuk,I_amuk,I_uluk,I_elal,
     & I_ilal,I_olal,I_ulal,I_amal,I_eluk,I_iluk,
     & I_oluk,I_aruk,I_upuk,I_epal,I_ipal,
     & I_opal,I_upal,I_aral,I_epuk,I_ipuk,
     & I_opuk,I_atuk,I_usuk,I_esal,I_isal,
     & I_osal,I_usal,I_atal,I_esuk,I_isuk,
     & I_osuk,I_axuk,I_uvuk,I_eval,I_ival,
     & I_oval,I_uval,I_axal,I_evuk,I_ivuk,
     & I_ovuk,I_olel,I_ulel,I_amel,
     & I_emel,I_imel,10,I_opel,
     & I_upel,I_arel,I_erel,
     & I_irel)
C FDA_10boats.fgi(  40, 230):���������� ���������� ������� �� ������� FDA60,BOAT10
      End
