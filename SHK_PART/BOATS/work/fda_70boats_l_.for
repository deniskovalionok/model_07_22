       SUBROUTINE SPW_Init_tab(iTabVer)
       integer*4 iTabVer
       TYPE TSubr
         sequence
         integer*4 addr
         integer*4 period
         integer*4 phase
         integer*4 object
         integer*4 allowed
         integer*4 ncall
         real*8    time
         integer*4 individualScale
         integer*4 rezerv(2),flags,CPUTime(2)
         character(LEN=32) name
       END TYPE TSubr

       TYPE(TSubr) SPW_Table_Sub(1)
       Common/SPW_Table_Sub/ SPW_Table_Sub
       external FDA_70BOATS
       Data SPW_Table_Sub(1)/TSubr(0,1,0,2,0,0,0.,0,0,8,0,
     &   'FDA_70BOATS')/

       LOGICAL*1 SPW_0(0:14335)
       Common/SPW_0/ SPW_0 !
       CHARACTER*1 SPW_1(0:255)
       Common/SPW_1/ SPW_1 !

       real*4 info_kwant
       integer*4 info_siz
       common/spw_info/ info_siz,info_kwant
       !MS$ATTRIBUTES DLLEXPORT::spw_info
       Data info_siz/8/,info_kwant/0.125/
       integer*4 dispStateHdr
       integer*4 stepCounter
       real*8    subInfo(4)
       common/spw_dispState/ dispStateHdr,stepCounter,subInfo

       character*(32) ver(2)
       integer*4 nver,versiz
       common/spw_versions/ nver,versiz,ver
       !MS$ATTRIBUTES DLLEXPORT::spw_versions
       Data nver/2/,versiz/32/
       Data ver(1)/''/
       Data ver(2)/'v_0414FB2C_FDA_70BOATS'/

       CALL SPW_SET_SIGN(865693333,0,-1979069454)
       iTabVer=iTabVer*1000+1088

       CALL SPW_SET_UIDTR(106761177,-1181702884)
       CALL SPW_SET_KWANT(REAL(0.125))
       CALL SPW_SET_NCMN(2)
       CALL SPW_ADD_CMN(LOC(SPW_0),12663)
       CALL SPW_ADD_CMN(LOC(SPW_1),200)
       CALL SPW_SET_NAMES(7540,
     +    'fda_70boats_l_.vpt',
     +    'fda_70boats_l_.B990A51C.vpt')
       SPW_Table_Sub(1)%addr=LOC(FDA_70BOATS)
       SPW_Table_Sub(1)%allowed=LOC(SPW_0)+12662
       SPW_Table_Sub(1)%individualScale=LOC(SPW_0)+0
       CALL SPW_SET_SUB_LST(1,SPW_Table_Sub,stepCounter)
       end

       subroutine spw_initDisp
       integer*4 dispStateHdr
       integer*4 stepCounter
       real*8    subInfo(4)
       common/spw_dispState/ dispStateHdr,stepCounter,subInfo
       stepCounter=0
       subInfo=0
       end
