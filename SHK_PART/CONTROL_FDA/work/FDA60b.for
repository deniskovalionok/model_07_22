      Subroutine FDA60b(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDA60b.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      !{
      Call DAT_ANA_2_HANDLER(deltat,R_opod,R_otod,REAL(1,4
     &),
     & REAL(R_urod,4),I_itod,REAL(R_erod,4),
     & REAL(R_irod,4),REAL(R_ipod,4),
     & REAL(R_epod,4),REAL(R_esod,4),L_isod,REAL(R_osod,4
     &),L_usod,
     & L_atod,R_orod,REAL(R_arod,4),REAL(R_upod,4),L_etod
     &)
      !}
C FDA60b1_vlv.fgi( 361, 173):���������� ������� ��� 2,20FDA66CQ008XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_evod,R_edud,REAL(1,4
     &),
     & REAL(R_ixod,4),I_adud,REAL(R_uvod,4),
     & REAL(R_axod,4),REAL(R_avod,4),
     & REAL(R_utod,4),REAL(R_uxod,4),L_abud,REAL(R_ebud,4
     &),L_ibud,
     & L_obud,R_exod,REAL(R_ovod,4),REAL(R_ivod,4),L_ubud
     &)
      !}
C FDA60b1_vlv.fgi( 361, 156):���������� ������� ��� 2,20FDA66CM006XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_emud,R_esud,REAL(1,4
     &),
     & REAL(R_ipud,4),I_asud,REAL(R_umud,4),
     & REAL(R_apud,4),REAL(R_amud,4),
     & REAL(R_ulud,4),REAL(R_upud,4),L_arud,REAL(R_erud,4
     &),L_irud,
     & L_orud,R_epud,REAL(R_omud,4),REAL(R_imud,4),L_urud
     &)
      !}
C FDA60b1_vlv.fgi( 333, 173):���������� ������� ��� 2,20FDA65CQ008XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_usud,R_uxud,REAL(1,4
     &),
     & REAL(R_avud,4),I_oxud,REAL(R_itud,4),
     & REAL(R_otud,4),REAL(R_osud,4),
     & REAL(R_isud,4),REAL(R_ivud,4),L_ovud,REAL(R_uvud,4
     &),L_axud,
     & L_exud,R_utud,REAL(R_etud,4),REAL(R_atud,4),L_ixud
     &)
      !}
C FDA60b1_vlv.fgi( 333, 156):���������� ������� ��� 2,20FDA65CM006XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ukaf,R_upaf,REAL(1,4
     &),
     & REAL(R_amaf,4),I_opaf,REAL(R_ilaf,4),
     & REAL(R_olaf,4),REAL(R_okaf,4),
     & REAL(R_ikaf,4),REAL(R_imaf,4),L_omaf,REAL(R_umaf,4
     &),L_apaf,
     & L_epaf,R_ulaf,REAL(R_elaf,4),REAL(R_alaf,4),L_ipaf
     &)
      !}
C FDA60b1_vlv.fgi( 305, 173):���������� ������� ��� 2,20FDA64CQ008XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_iraf,R_ivaf,REAL(1,4
     &),
     & REAL(R_osaf,4),I_evaf,REAL(R_asaf,4),
     & REAL(R_esaf,4),REAL(R_eraf,4),
     & REAL(R_araf,4),REAL(R_ataf,4),L_etaf,REAL(R_itaf,4
     &),L_otaf,
     & L_utaf,R_isaf,REAL(R_uraf,4),REAL(R_oraf,4),L_avaf
     &)
      !}
C FDA60b1_vlv.fgi( 305, 156):���������� ������� ��� 2,20FDA64CM006XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ifef,R_imef,REAL(1,4
     &),
     & REAL(R_okef,4),I_emef,REAL(R_akef,4),
     & REAL(R_ekef,4),REAL(R_efef,4),
     & REAL(R_afef,4),REAL(R_alef,4),L_elef,REAL(R_ilef,4
     &),L_olef,
     & L_ulef,R_ikef,REAL(R_ufef,4),REAL(R_ofef,4),L_amef
     &)
      !}
C FDA60b1_vlv.fgi( 277, 173):���������� ������� ��� 2,20FDA63CQ008XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_apef,R_atef,REAL(1,4
     &),
     & REAL(R_eref,4),I_usef,REAL(R_opef,4),
     & REAL(R_upef,4),REAL(R_umef,4),
     & REAL(R_omef,4),REAL(R_oref,4),L_uref,REAL(R_asef,4
     &),L_esef,
     & L_isef,R_aref,REAL(R_ipef,4),REAL(R_epef,4),L_osef
     &)
      !}
C FDA60b1_vlv.fgi( 277, 156):���������� ������� ��� 2,20FDA63CM006XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_adif,R_alif,REAL(1,4
     &),
     & REAL(R_efif,4),I_ukif,REAL(R_odif,4),
     & REAL(R_udif,4),REAL(R_ubif,4),
     & REAL(R_obif,4),REAL(R_ofif,4),L_ufif,REAL(R_akif,4
     &),L_ekif,
     & L_ikif,R_afif,REAL(R_idif,4),REAL(R_edif,4),L_okif
     &)
      !}
C FDA60b1_vlv.fgi( 249, 173):���������� ������� ��� 2,20FDA62CQ008XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_olif,R_orif,REAL(1,4
     &),
     & REAL(R_umif,4),I_irif,REAL(R_emif,4),
     & REAL(R_imif,4),REAL(R_ilif,4),
     & REAL(R_elif,4),REAL(R_epif,4),L_ipif,REAL(R_opif,4
     &),L_upif,
     & L_arif,R_omif,REAL(R_amif,4),REAL(R_ulif,4),L_erif
     &)
      !}
C FDA60b1_vlv.fgi( 249, 156):���������� ������� ��� 2,20FDA62CM006XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_oxif,R_ofof,REAL(1,4
     &),
     & REAL(R_ubof,4),I_ifof,REAL(R_ebof,4),
     & REAL(R_ibof,4),REAL(R_ixif,4),
     & REAL(R_exif,4),REAL(R_edof,4),L_idof,REAL(R_odof,4
     &),L_udof,
     & L_afof,R_obof,REAL(R_abof,4),REAL(R_uxif,4),L_efof
     &)
      !}
C FDA60b1_vlv.fgi( 221, 173):���������� ������� ��� 2,20FDA61CQ008XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ekof,R_epof,REAL(1,4
     &),
     & REAL(R_ilof,4),I_apof,REAL(R_ukof,4),
     & REAL(R_alof,4),REAL(R_akof,4),
     & REAL(R_ufof,4),REAL(R_ulof,4),L_amof,REAL(R_emof,4
     &),L_imof,
     & L_omof,R_elof,REAL(R_okof,4),REAL(R_ikof,4),L_umof
     &)
      !}
C FDA60b1_vlv.fgi( 221, 156):���������� ������� ��� 2,20FDA61CM006XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_evof,R_eduf,REAL(1,4
     &),
     & REAL(R_ixof,4),I_aduf,REAL(R_uvof,4),
     & REAL(R_axof,4),REAL(R_avof,4),
     & REAL(R_utof,4),REAL(R_uxof,4),L_abuf,REAL(R_ebuf,4
     &),L_ibuf,
     & L_obuf,R_exof,REAL(R_ovof,4),REAL(R_ivof,4),L_ubuf
     &)
      !}
C FDA60b1_vlv.fgi( 361, 190):���������� ������� ��� 2,20FDA66CF018XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uduf,R_uluf,REAL(1,4
     &),
     & REAL(R_akuf,4),I_oluf,REAL(R_ifuf,4),
     & REAL(R_ofuf,4),REAL(R_oduf,4),
     & REAL(R_iduf,4),REAL(R_ikuf,4),L_okuf,REAL(R_ukuf,4
     &),L_aluf,
     & L_eluf,R_ufuf,REAL(R_efuf,4),REAL(R_afuf,4),L_iluf
     &)
      !}
C FDA60b1_vlv.fgi( 361, 206):���������� ������� ��� 2,20FDA66CF025XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imuf,R_isuf,REAL(1,4
     &),
     & REAL(R_opuf,4),I_esuf,REAL(R_apuf,4),
     & REAL(R_epuf,4),REAL(R_emuf,4),
     & REAL(R_amuf,4),REAL(R_aruf,4),L_eruf,REAL(R_iruf,4
     &),L_oruf,
     & L_uruf,R_ipuf,REAL(R_umuf,4),REAL(R_omuf,4),L_asuf
     &)
      !}
C FDA60b1_vlv.fgi( 361, 224):���������� ������� ��� 2,20FDA66CF024XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_atuf,R_abak,REAL(1,4
     &),
     & REAL(R_evuf,4),I_uxuf,REAL(R_otuf,4),
     & REAL(R_utuf,4),REAL(R_usuf,4),
     & REAL(R_osuf,4),REAL(R_ovuf,4),L_uvuf,REAL(R_axuf,4
     &),L_exuf,
     & L_ixuf,R_avuf,REAL(R_ituf,4),REAL(R_etuf,4),L_oxuf
     &)
      !}
C FDA60b1_vlv.fgi( 333, 190):���������� ������� ��� 2,20FDA65CF018XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obak,R_okak,REAL(1,4
     &),
     & REAL(R_udak,4),I_ikak,REAL(R_edak,4),
     & REAL(R_idak,4),REAL(R_ibak,4),
     & REAL(R_ebak,4),REAL(R_efak,4),L_ifak,REAL(R_ofak,4
     &),L_ufak,
     & L_akak,R_odak,REAL(R_adak,4),REAL(R_ubak,4),L_ekak
     &)
      !}
C FDA60b1_vlv.fgi( 333, 206):���������� ������� ��� 2,20FDA65CF025XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_elak,R_erak,REAL(1,4
     &),
     & REAL(R_imak,4),I_arak,REAL(R_ulak,4),
     & REAL(R_amak,4),REAL(R_alak,4),
     & REAL(R_ukak,4),REAL(R_umak,4),L_apak,REAL(R_epak,4
     &),L_ipak,
     & L_opak,R_emak,REAL(R_olak,4),REAL(R_ilak,4),L_upak
     &)
      !}
C FDA60b1_vlv.fgi( 333, 224):���������� ������� ��� 2,20FDA65CF024XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_urak,R_uvak,REAL(1,4
     &),
     & REAL(R_atak,4),I_ovak,REAL(R_isak,4),
     & REAL(R_osak,4),REAL(R_orak,4),
     & REAL(R_irak,4),REAL(R_itak,4),L_otak,REAL(R_utak,4
     &),L_avak,
     & L_evak,R_usak,REAL(R_esak,4),REAL(R_asak,4),L_ivak
     &)
      !}
C FDA60b1_vlv.fgi( 305, 190):���������� ������� ��� 2,20FDA64CF018XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ixak,R_ifek,REAL(1,4
     &),
     & REAL(R_obek,4),I_efek,REAL(R_abek,4),
     & REAL(R_ebek,4),REAL(R_exak,4),
     & REAL(R_axak,4),REAL(R_adek,4),L_edek,REAL(R_idek,4
     &),L_odek,
     & L_udek,R_ibek,REAL(R_uxak,4),REAL(R_oxak,4),L_afek
     &)
      !}
C FDA60b1_vlv.fgi( 305, 206):���������� ������� ��� 2,20FDA64CF025XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_akek,R_apek,REAL(1,4
     &),
     & REAL(R_elek,4),I_umek,REAL(R_okek,4),
     & REAL(R_ukek,4),REAL(R_ufek,4),
     & REAL(R_ofek,4),REAL(R_olek,4),L_ulek,REAL(R_amek,4
     &),L_emek,
     & L_imek,R_alek,REAL(R_ikek,4),REAL(R_ekek,4),L_omek
     &)
      !}
C FDA60b1_vlv.fgi( 305, 224):���������� ������� ��� 2,20FDA64CF024XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_opek,R_otek,REAL(1,4
     &),
     & REAL(R_urek,4),I_itek,REAL(R_erek,4),
     & REAL(R_irek,4),REAL(R_ipek,4),
     & REAL(R_epek,4),REAL(R_esek,4),L_isek,REAL(R_osek,4
     &),L_usek,
     & L_atek,R_orek,REAL(R_arek,4),REAL(R_upek,4),L_etek
     &)
      !}
C FDA60b1_vlv.fgi( 277, 190):���������� ������� ��� 2,20FDA63CF018XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_evek,R_edik,REAL(1,4
     &),
     & REAL(R_ixek,4),I_adik,REAL(R_uvek,4),
     & REAL(R_axek,4),REAL(R_avek,4),
     & REAL(R_utek,4),REAL(R_uxek,4),L_abik,REAL(R_ebik,4
     &),L_ibik,
     & L_obik,R_exek,REAL(R_ovek,4),REAL(R_ivek,4),L_ubik
     &)
      !}
C FDA60b1_vlv.fgi( 277, 206):���������� ������� ��� 2,20FDA63CF025XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_udik,R_ulik,REAL(1,4
     &),
     & REAL(R_akik,4),I_olik,REAL(R_ifik,4),
     & REAL(R_ofik,4),REAL(R_odik,4),
     & REAL(R_idik,4),REAL(R_ikik,4),L_okik,REAL(R_ukik,4
     &),L_alik,
     & L_elik,R_ufik,REAL(R_efik,4),REAL(R_afik,4),L_ilik
     &)
      !}
C FDA60b1_vlv.fgi( 277, 224):���������� ������� ��� 2,20FDA63CF024XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imik,R_isik,REAL(1,4
     &),
     & REAL(R_opik,4),I_esik,REAL(R_apik,4),
     & REAL(R_epik,4),REAL(R_emik,4),
     & REAL(R_amik,4),REAL(R_arik,4),L_erik,REAL(R_irik,4
     &),L_orik,
     & L_urik,R_ipik,REAL(R_umik,4),REAL(R_omik,4),L_asik
     &)
      !}
C FDA60b1_vlv.fgi( 249, 190):���������� ������� ��� 2,20FDA62CF018XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_atik,R_abok,REAL(1,4
     &),
     & REAL(R_evik,4),I_uxik,REAL(R_otik,4),
     & REAL(R_utik,4),REAL(R_usik,4),
     & REAL(R_osik,4),REAL(R_ovik,4),L_uvik,REAL(R_axik,4
     &),L_exik,
     & L_ixik,R_avik,REAL(R_itik,4),REAL(R_etik,4),L_oxik
     &)
      !}
C FDA60b1_vlv.fgi( 249, 206):���������� ������� ��� 2,20FDA62CF025XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obok,R_okok,REAL(1,4
     &),
     & REAL(R_udok,4),I_ikok,REAL(R_edok,4),
     & REAL(R_idok,4),REAL(R_ibok,4),
     & REAL(R_ebok,4),REAL(R_efok,4),L_ifok,REAL(R_ofok,4
     &),L_ufok,
     & L_akok,R_odok,REAL(R_adok,4),REAL(R_ubok,4),L_ekok
     &)
      !}
C FDA60b1_vlv.fgi( 249, 224):���������� ������� ��� 2,20FDA62CF024XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_elok,R_erok,REAL(1,4
     &),
     & REAL(R_imok,4),I_arok,REAL(R_ulok,4),
     & REAL(R_amok,4),REAL(R_alok,4),
     & REAL(R_ukok,4),REAL(R_umok,4),L_apok,REAL(R_epok,4
     &),L_ipok,
     & L_opok,R_emok,REAL(R_olok,4),REAL(R_ilok,4),L_upok
     &)
      !}
C FDA60b1_vlv.fgi( 221, 190):���������� ������� ��� 2,20FDA61CF018XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_urok,R_uvok,REAL(1,4
     &),
     & REAL(R_atok,4),I_ovok,REAL(R_isok,4),
     & REAL(R_osok,4),REAL(R_orok,4),
     & REAL(R_irok,4),REAL(R_itok,4),L_otok,REAL(R_utok,4
     &),L_avok,
     & L_evok,R_usok,REAL(R_esok,4),REAL(R_asok,4),L_ivok
     &)
      !}
C FDA60b1_vlv.fgi( 221, 206):���������� ������� ��� 2,20FDA61CF025XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ixok,R_ifuk,REAL(1,4
     &),
     & REAL(R_obuk,4),I_efuk,REAL(R_abuk,4),
     & REAL(R_ebuk,4),REAL(R_exok,4),
     & REAL(R_axok,4),REAL(R_aduk,4),L_eduk,REAL(R_iduk,4
     &),L_oduk,
     & L_uduk,R_ibuk,REAL(R_uxok,4),REAL(R_oxok,4),L_afuk
     &)
      !}
C FDA60b1_vlv.fgi( 221, 224):���������� ������� ��� 2,20FDA61CF024XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_evode,R_edude,REAL(1
     &,4),
     & REAL(R_ixode,4),I_adude,REAL(R_uvode,4),
     & REAL(R_axode,4),REAL(R_avode,4),
     & REAL(R_utode,4),REAL(R_uxode,4),L_abude,REAL(R_ebude
     &,4),L_ibude,
     & L_obude,R_exode,REAL(R_ovode,4),REAL(R_ivode,4),L_ubude
     &)
      !}
C FDA60b1_vlv.fgi( 334, 453):���������� ������� ��� 2,20FDA66CT003XQ01 
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_udude,R_ulude,REAL(1
     &,4),
     & REAL(R_akude,4),I_olude,REAL(R_ifude,4),
     & REAL(R_ofude,4),REAL(R_odude,4),
     & REAL(R_idude,4),REAL(R_ikude,4),L_okude,REAL(R_ukude
     &,4),L_alude,
     & L_elude,R_ufude,REAL(R_efude,4),REAL(R_afude,4),L_ilude
     &)
      !}
C FDA60b1_vlv.fgi( 334, 468):���������� ������� ��� 2,20FDA66CT002XQ01 
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imude,R_isude,REAL(1
     &,4),
     & REAL(R_opude,4),I_esude,REAL(R_apude,4),
     & REAL(R_epude,4),REAL(R_emude,4),
     & REAL(R_amude,4),REAL(R_arude,4),L_erude,REAL(R_irude
     &,4),L_orude,
     & L_urude,R_ipude,REAL(R_umude,4),REAL(R_omude,4),L_asude
     &)
      !}
C FDA60b1_vlv.fgi( 334, 483):���������� ������� ��� 2,20FDA66CT001XQ01 
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_atude,R_abafe,REAL(1
     &,4),
     & REAL(R_evude,4),I_uxude,REAL(R_otude,4),
     & REAL(R_utude,4),REAL(R_usude,4),
     & REAL(R_osude,4),REAL(R_ovude,4),L_uvude,REAL(R_axude
     &,4),L_exude,
     & L_ixude,R_avude,REAL(R_itude,4),REAL(R_etude,4),L_oxude
     &)
      !}
C FDA60b1_vlv.fgi( 301, 452):���������� ������� ��� 2,20FDA65CT003XQ01 
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obafe,R_okafe,REAL(1
     &,4),
     & REAL(R_udafe,4),I_ikafe,REAL(R_edafe,4),
     & REAL(R_idafe,4),REAL(R_ibafe,4),
     & REAL(R_ebafe,4),REAL(R_efafe,4),L_ifafe,REAL(R_ofafe
     &,4),L_ufafe,
     & L_akafe,R_odafe,REAL(R_adafe,4),REAL(R_ubafe,4),L_ekafe
     &)
      !}
C FDA60b1_vlv.fgi( 301, 468):���������� ������� ��� 2,20FDA65CT002XQ01 
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_elafe,R_erafe,REAL(1
     &,4),
     & REAL(R_imafe,4),I_arafe,REAL(R_ulafe,4),
     & REAL(R_amafe,4),REAL(R_alafe,4),
     & REAL(R_ukafe,4),REAL(R_umafe,4),L_apafe,REAL(R_epafe
     &,4),L_ipafe,
     & L_opafe,R_emafe,REAL(R_olafe,4),REAL(R_ilafe,4),L_upafe
     &)
      !}
C FDA60b1_vlv.fgi( 301, 484):���������� ������� ��� 2,20FDA65CT001XQ01 
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_urafe,R_uvafe,REAL(1
     &,4),
     & REAL(R_atafe,4),I_ovafe,REAL(R_isafe,4),
     & REAL(R_osafe,4),REAL(R_orafe,4),
     & REAL(R_irafe,4),REAL(R_itafe,4),L_otafe,REAL(R_utafe
     &,4),L_avafe,
     & L_evafe,R_usafe,REAL(R_esafe,4),REAL(R_asafe,4),L_ivafe
     &)
      !}
C FDA60b1_vlv.fgi( 333, 500):���������� ������� ��� 2,20FDA64CT003XQ01 
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ixafe,R_ifefe,REAL(1
     &,4),
     & REAL(R_obefe,4),I_efefe,REAL(R_abefe,4),
     & REAL(R_ebefe,4),REAL(R_exafe,4),
     & REAL(R_axafe,4),REAL(R_adefe,4),L_edefe,REAL(R_idefe
     &,4),L_odefe,
     & L_udefe,R_ibefe,REAL(R_uxafe,4),REAL(R_oxafe,4),L_afefe
     &)
      !}
C FDA60b1_vlv.fgi( 333, 516):���������� ������� ��� 2,20FDA64CT002XQ01 
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_akefe,R_apefe,REAL(1
     &,4),
     & REAL(R_elefe,4),I_umefe,REAL(R_okefe,4),
     & REAL(R_ukefe,4),REAL(R_ufefe,4),
     & REAL(R_ofefe,4),REAL(R_olefe,4),L_ulefe,REAL(R_amefe
     &,4),L_emefe,
     & L_imefe,R_alefe,REAL(R_ikefe,4),REAL(R_ekefe,4),L_omefe
     &)
      !}
C FDA60b1_vlv.fgi( 333, 532):���������� ������� ��� 2,20FDA64CT001XQ01 
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_opefe,R_otefe,REAL(1
     &,4),
     & REAL(R_urefe,4),I_itefe,REAL(R_erefe,4),
     & REAL(R_irefe,4),REAL(R_ipefe,4),
     & REAL(R_epefe,4),REAL(R_esefe,4),L_isefe,REAL(R_osefe
     &,4),L_usefe,
     & L_atefe,R_orefe,REAL(R_arefe,4),REAL(R_upefe,4),L_etefe
     &)
      !}
C FDA60b1_vlv.fgi( 302, 500):���������� ������� ��� 2,20FDA63CT003XQ01 
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_evefe,R_edife,REAL(1
     &,4),
     & REAL(R_ixefe,4),I_adife,REAL(R_uvefe,4),
     & REAL(R_axefe,4),REAL(R_avefe,4),
     & REAL(R_utefe,4),REAL(R_uxefe,4),L_abife,REAL(R_ebife
     &,4),L_ibife,
     & L_obife,R_exefe,REAL(R_ovefe,4),REAL(R_ivefe,4),L_ubife
     &)
      !}
C FDA60b1_vlv.fgi( 302, 516):���������� ������� ��� 2,20FDA63CT002XQ01 
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_udife,R_ulife,REAL(1
     &,4),
     & REAL(R_akife,4),I_olife,REAL(R_ifife,4),
     & REAL(R_ofife,4),REAL(R_odife,4),
     & REAL(R_idife,4),REAL(R_ikife,4),L_okife,REAL(R_ukife
     &,4),L_alife,
     & L_elife,R_ufife,REAL(R_efife,4),REAL(R_afife,4),L_ilife
     &)
      !}
C FDA60b1_vlv.fgi( 302, 532):���������� ������� ��� 2,20FDA63CT001XQ01 
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imife,R_isife,REAL(1
     &,4),
     & REAL(R_opife,4),I_esife,REAL(R_apife,4),
     & REAL(R_epife,4),REAL(R_emife,4),
     & REAL(R_amife,4),REAL(R_arife,4),L_erife,REAL(R_irife
     &,4),L_orife,
     & L_urife,R_ipife,REAL(R_umife,4),REAL(R_omife,4),L_asife
     &)
      !}
C FDA60b1_vlv.fgi( 332, 549):���������� ������� ��� 2,20FDA62CT003XQ01 
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_atife,R_abofe,REAL(1
     &,4),
     & REAL(R_evife,4),I_uxife,REAL(R_otife,4),
     & REAL(R_utife,4),REAL(R_usife,4),
     & REAL(R_osife,4),REAL(R_ovife,4),L_uvife,REAL(R_axife
     &,4),L_exife,
     & L_ixife,R_avife,REAL(R_itife,4),REAL(R_etife,4),L_oxife
     &)
      !}
C FDA60b1_vlv.fgi( 332, 564):���������� ������� ��� 2,20FDA62CT002XQ01 
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obofe,R_okofe,REAL(1
     &,4),
     & REAL(R_udofe,4),I_ikofe,REAL(R_edofe,4),
     & REAL(R_idofe,4),REAL(R_ibofe,4),
     & REAL(R_ebofe,4),REAL(R_efofe,4),L_ifofe,REAL(R_ofofe
     &,4),L_ufofe,
     & L_akofe,R_odofe,REAL(R_adofe,4),REAL(R_ubofe,4),L_ekofe
     &)
      !}
C FDA60b1_vlv.fgi( 332, 579):���������� ������� ��� 2,20FDA62CT001XQ01 
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_elofe,R_erofe,REAL(1
     &,4),
     & REAL(R_imofe,4),I_arofe,REAL(R_ulofe,4),
     & REAL(R_amofe,4),REAL(R_alofe,4),
     & REAL(R_ukofe,4),REAL(R_umofe,4),L_apofe,REAL(R_epofe
     &,4),L_ipofe,
     & L_opofe,R_emofe,REAL(R_olofe,4),REAL(R_ilofe,4),L_upofe
     &)
      !}
C FDA60b1_vlv.fgi( 302, 548):���������� ������� ��� 2,20FDA61CT003XQ01 
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_urofe,R_uvofe,REAL(1
     &,4),
     & REAL(R_atofe,4),I_ovofe,REAL(R_isofe,4),
     & REAL(R_osofe,4),REAL(R_orofe,4),
     & REAL(R_irofe,4),REAL(R_itofe,4),L_otofe,REAL(R_utofe
     &,4),L_avofe,
     & L_evofe,R_usofe,REAL(R_esofe,4),REAL(R_asofe,4),L_ivofe
     &)
      !}
C FDA60b1_vlv.fgi( 302, 564):���������� ������� ��� 2,20FDA61CT002XQ01 
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ixofe,R_ifufe,REAL(1
     &,4),
     & REAL(R_obufe,4),I_efufe,REAL(R_abufe,4),
     & REAL(R_ebufe,4),REAL(R_exofe,4),
     & REAL(R_axofe,4),REAL(R_adufe,4),L_edufe,REAL(R_idufe
     &,4),L_odufe,
     & L_udufe,R_ibufe,REAL(R_uxofe,4),REAL(R_oxofe,4),L_afufe
     &)
      !}
C FDA60b1_vlv.fgi( 302, 579):���������� ������� ��� 2,20FDA61CT001XQ01 
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_akufe,R_apufe,REAL(1
     &,4),
     & REAL(R_elufe,4),I_umufe,REAL(R_okufe,4),
     & REAL(R_ukufe,4),REAL(R_ufufe,4),
     & REAL(R_ofufe,4),REAL(R_olufe,4),L_ulufe,REAL(R_amufe
     &,4),L_emufe,
     & L_imufe,R_alufe,REAL(R_ikufe,4),REAL(R_ekufe,4),L_omufe
     &)
      !}
C FDA60b1_vlv.fgi( 399, 311):���������� ������� ��� 2,20FDA66AC003XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_opufe,R_otufe,REAL(1
     &,4),
     & REAL(R_urufe,4),I_itufe,REAL(R_erufe,4),
     & REAL(R_irufe,4),REAL(R_ipufe,4),
     & REAL(R_epufe,4),REAL(R_esufe,4),L_isufe,REAL(R_osufe
     &,4),L_usufe,
     & L_atufe,R_orufe,REAL(R_arufe,4),REAL(R_upufe,4),L_etufe
     &)
      !}
C FDA60b1_vlv.fgi( 399, 326):���������� ������� ��� 2,20FDA66AC002XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_evufe,R_edake,REAL(1
     &,4),
     & REAL(R_ixufe,4),I_adake,REAL(R_uvufe,4),
     & REAL(R_axufe,4),REAL(R_avufe,4),
     & REAL(R_utufe,4),REAL(R_uxufe,4),L_abake,REAL(R_ebake
     &,4),L_ibake,
     & L_obake,R_exufe,REAL(R_ovufe,4),REAL(R_ivufe,4),L_ubake
     &)
      !}
C FDA60b1_vlv.fgi( 399, 341):���������� ������� ��� 2,20FDA66AC001XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_udake,R_ulake,REAL(1
     &,4),
     & REAL(R_akake,4),I_olake,REAL(R_ifake,4),
     & REAL(R_ofake,4),REAL(R_odake,4),
     & REAL(R_idake,4),REAL(R_ikake,4),L_okake,REAL(R_ukake
     &,4),L_alake,
     & L_elake,R_ufake,REAL(R_efake,4),REAL(R_afake,4),L_ilake
     &)
      !}
C FDA60b1_vlv.fgi( 399, 356):���������� ������� ��� 2,20FDA66AC003XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imake,R_isake,REAL(1
     &,4),
     & REAL(R_opake,4),I_esake,REAL(R_apake,4),
     & REAL(R_epake,4),REAL(R_emake,4),
     & REAL(R_amake,4),REAL(R_arake,4),L_erake,REAL(R_irake
     &,4),L_orake,
     & L_urake,R_ipake,REAL(R_umake,4),REAL(R_omake,4),L_asake
     &)
      !}
C FDA60b1_vlv.fgi( 399, 371):���������� ������� ��� 2,20FDA66AC002XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_atake,R_abeke,REAL(1
     &,4),
     & REAL(R_evake,4),I_uxake,REAL(R_otake,4),
     & REAL(R_utake,4),REAL(R_usake,4),
     & REAL(R_osake,4),REAL(R_ovake,4),L_uvake,REAL(R_axake
     &,4),L_exake,
     & L_ixake,R_avake,REAL(R_itake,4),REAL(R_etake,4),L_oxake
     &)
      !}
C FDA60b1_vlv.fgi( 399, 388):���������� ������� ��� 2,20FDA66AC001XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obeke,R_okeke,REAL(1
     &,4),
     & REAL(R_udeke,4),I_ikeke,REAL(R_edeke,4),
     & REAL(R_ideke,4),REAL(R_ibeke,4),
     & REAL(R_ebeke,4),REAL(R_efeke,4),L_ifeke,REAL(R_ofeke
     &,4),L_ufeke,
     & L_akeke,R_odeke,REAL(R_adeke,4),REAL(R_ubeke,4),L_ekeke
     &)
      !}
C FDA60b1_vlv.fgi( 367, 309):���������� ������� ��� 2,20FDA65AC003XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_eleke,R_ereke,REAL(1
     &,4),
     & REAL(R_imeke,4),I_areke,REAL(R_uleke,4),
     & REAL(R_ameke,4),REAL(R_aleke,4),
     & REAL(R_ukeke,4),REAL(R_umeke,4),L_apeke,REAL(R_epeke
     &,4),L_ipeke,
     & L_opeke,R_emeke,REAL(R_oleke,4),REAL(R_ileke,4),L_upeke
     &)
      !}
C FDA60b1_vlv.fgi( 367, 326):���������� ������� ��� 2,20FDA65AC002XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ureke,R_uveke,REAL(1
     &,4),
     & REAL(R_ateke,4),I_oveke,REAL(R_iseke,4),
     & REAL(R_oseke,4),REAL(R_oreke,4),
     & REAL(R_ireke,4),REAL(R_iteke,4),L_oteke,REAL(R_uteke
     &,4),L_aveke,
     & L_eveke,R_useke,REAL(R_eseke,4),REAL(R_aseke,4),L_iveke
     &)
      !}
C FDA60b1_vlv.fgi( 367, 341):���������� ������� ��� 2,20FDA65AC001XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ixeke,R_ifike,REAL(1
     &,4),
     & REAL(R_obike,4),I_efike,REAL(R_abike,4),
     & REAL(R_ebike,4),REAL(R_exeke,4),
     & REAL(R_axeke,4),REAL(R_adike,4),L_edike,REAL(R_idike
     &,4),L_odike,
     & L_udike,R_ibike,REAL(R_uxeke,4),REAL(R_oxeke,4),L_afike
     &)
      !}
C FDA60b1_vlv.fgi( 367, 356):���������� ������� ��� 2,20FDA65AC003XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_akike,R_apike,REAL(1
     &,4),
     & REAL(R_elike,4),I_umike,REAL(R_okike,4),
     & REAL(R_ukike,4),REAL(R_ufike,4),
     & REAL(R_ofike,4),REAL(R_olike,4),L_ulike,REAL(R_amike
     &,4),L_emike,
     & L_imike,R_alike,REAL(R_ikike,4),REAL(R_ekike,4),L_omike
     &)
      !}
C FDA60b1_vlv.fgi( 367, 371):���������� ������� ��� 2,20FDA65AC002XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_opike,R_otike,REAL(1
     &,4),
     & REAL(R_urike,4),I_itike,REAL(R_erike,4),
     & REAL(R_irike,4),REAL(R_ipike,4),
     & REAL(R_epike,4),REAL(R_esike,4),L_isike,REAL(R_osike
     &,4),L_usike,
     & L_atike,R_orike,REAL(R_arike,4),REAL(R_upike,4),L_etike
     &)
      !}
C FDA60b1_vlv.fgi( 367, 388):���������� ������� ��� 2,20FDA65AC001XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_evike,R_edoke,REAL(1
     &,4),
     & REAL(R_ixike,4),I_adoke,REAL(R_uvike,4),
     & REAL(R_axike,4),REAL(R_avike,4),
     & REAL(R_utike,4),REAL(R_uxike,4),L_aboke,REAL(R_eboke
     &,4),L_iboke,
     & L_oboke,R_exike,REAL(R_ovike,4),REAL(R_ivike,4),L_uboke
     &)
      !}
C FDA60b1_vlv.fgi( 399, 404):���������� ������� ��� 2,20FDA64AC003XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_udoke,R_uloke,REAL(1
     &,4),
     & REAL(R_akoke,4),I_oloke,REAL(R_ifoke,4),
     & REAL(R_ofoke,4),REAL(R_odoke,4),
     & REAL(R_idoke,4),REAL(R_ikoke,4),L_okoke,REAL(R_ukoke
     &,4),L_aloke,
     & L_eloke,R_ufoke,REAL(R_efoke,4),REAL(R_afoke,4),L_iloke
     &)
      !}
C FDA60b1_vlv.fgi( 399, 421):���������� ������� ��� 2,20FDA64AC002XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imoke,R_isoke,REAL(1
     &,4),
     & REAL(R_opoke,4),I_esoke,REAL(R_apoke,4),
     & REAL(R_epoke,4),REAL(R_emoke,4),
     & REAL(R_amoke,4),REAL(R_aroke,4),L_eroke,REAL(R_iroke
     &,4),L_oroke,
     & L_uroke,R_ipoke,REAL(R_umoke,4),REAL(R_omoke,4),L_asoke
     &)
      !}
C FDA60b1_vlv.fgi( 399, 436):���������� ������� ��� 2,20FDA64AC001XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_atoke,R_abuke,REAL(1
     &,4),
     & REAL(R_evoke,4),I_uxoke,REAL(R_otoke,4),
     & REAL(R_utoke,4),REAL(R_usoke,4),
     & REAL(R_osoke,4),REAL(R_ovoke,4),L_uvoke,REAL(R_axoke
     &,4),L_exoke,
     & L_ixoke,R_avoke,REAL(R_itoke,4),REAL(R_etoke,4),L_oxoke
     &)
      !}
C FDA60b1_vlv.fgi( 399, 451):���������� ������� ��� 2,20FDA64AC003XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obuke,R_okuke,REAL(1
     &,4),
     & REAL(R_uduke,4),I_ikuke,REAL(R_eduke,4),
     & REAL(R_iduke,4),REAL(R_ibuke,4),
     & REAL(R_ebuke,4),REAL(R_efuke,4),L_ifuke,REAL(R_ofuke
     &,4),L_ufuke,
     & L_akuke,R_oduke,REAL(R_aduke,4),REAL(R_ubuke,4),L_ekuke
     &)
      !}
C FDA60b1_vlv.fgi( 399, 466):���������� ������� ��� 2,20FDA64AC002XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_eluke,R_eruke,REAL(1
     &,4),
     & REAL(R_imuke,4),I_aruke,REAL(R_uluke,4),
     & REAL(R_amuke,4),REAL(R_aluke,4),
     & REAL(R_ukuke,4),REAL(R_umuke,4),L_apuke,REAL(R_epuke
     &,4),L_ipuke,
     & L_opuke,R_emuke,REAL(R_oluke,4),REAL(R_iluke,4),L_upuke
     &)
      !}
C FDA60b1_vlv.fgi( 399, 483):���������� ������� ��� 2,20FDA64AC001XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uruke,R_uvuke,REAL(1
     &,4),
     & REAL(R_atuke,4),I_ovuke,REAL(R_isuke,4),
     & REAL(R_osuke,4),REAL(R_oruke,4),
     & REAL(R_iruke,4),REAL(R_ituke,4),L_otuke,REAL(R_utuke
     &,4),L_avuke,
     & L_evuke,R_usuke,REAL(R_esuke,4),REAL(R_asuke,4),L_ivuke
     &)
      !}
C FDA60b1_vlv.fgi( 367, 404):���������� ������� ��� 2,20FDA63AC003XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ixuke,R_ifale,REAL(1
     &,4),
     & REAL(R_obale,4),I_efale,REAL(R_abale,4),
     & REAL(R_ebale,4),REAL(R_exuke,4),
     & REAL(R_axuke,4),REAL(R_adale,4),L_edale,REAL(R_idale
     &,4),L_odale,
     & L_udale,R_ibale,REAL(R_uxuke,4),REAL(R_oxuke,4),L_afale
     &)
      !}
C FDA60b1_vlv.fgi( 367, 421):���������� ������� ��� 2,20FDA63AC002XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_akale,R_apale,REAL(1
     &,4),
     & REAL(R_elale,4),I_umale,REAL(R_okale,4),
     & REAL(R_ukale,4),REAL(R_ufale,4),
     & REAL(R_ofale,4),REAL(R_olale,4),L_ulale,REAL(R_amale
     &,4),L_emale,
     & L_imale,R_alale,REAL(R_ikale,4),REAL(R_ekale,4),L_omale
     &)
      !}
C FDA60b1_vlv.fgi( 367, 436):���������� ������� ��� 2,20FDA63AC001XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_opale,R_otale,REAL(1
     &,4),
     & REAL(R_urale,4),I_itale,REAL(R_erale,4),
     & REAL(R_irale,4),REAL(R_ipale,4),
     & REAL(R_epale,4),REAL(R_esale,4),L_isale,REAL(R_osale
     &,4),L_usale,
     & L_atale,R_orale,REAL(R_arale,4),REAL(R_upale,4),L_etale
     &)
      !}
C FDA60b1_vlv.fgi( 367, 451):���������� ������� ��� 2,20FDA63AC003XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_evale,R_edele,REAL(1
     &,4),
     & REAL(R_ixale,4),I_adele,REAL(R_uvale,4),
     & REAL(R_axale,4),REAL(R_avale,4),
     & REAL(R_utale,4),REAL(R_uxale,4),L_abele,REAL(R_ebele
     &,4),L_ibele,
     & L_obele,R_exale,REAL(R_ovale,4),REAL(R_ivale,4),L_ubele
     &)
      !}
C FDA60b1_vlv.fgi( 367, 466):���������� ������� ��� 2,20FDA63AC002XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_udele,R_ulele,REAL(1
     &,4),
     & REAL(R_akele,4),I_olele,REAL(R_ifele,4),
     & REAL(R_ofele,4),REAL(R_odele,4),
     & REAL(R_idele,4),REAL(R_ikele,4),L_okele,REAL(R_ukele
     &,4),L_alele,
     & L_elele,R_ufele,REAL(R_efele,4),REAL(R_afele,4),L_ilele
     &)
      !}
C FDA60b1_vlv.fgi( 367, 483):���������� ������� ��� 2,20FDA63AC001XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imele,R_isele,REAL(1
     &,4),
     & REAL(R_opele,4),I_esele,REAL(R_apele,4),
     & REAL(R_epele,4),REAL(R_emele,4),
     & REAL(R_amele,4),REAL(R_arele,4),L_erele,REAL(R_irele
     &,4),L_orele,
     & L_urele,R_ipele,REAL(R_umele,4),REAL(R_omele,4),L_asele
     &)
      !}
C FDA60b1_vlv.fgi( 399, 500):���������� ������� ��� 2,20FDA62AC003XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_atele,R_abile,REAL(1
     &,4),
     & REAL(R_evele,4),I_uxele,REAL(R_otele,4),
     & REAL(R_utele,4),REAL(R_usele,4),
     & REAL(R_osele,4),REAL(R_ovele,4),L_uvele,REAL(R_axele
     &,4),L_exele,
     & L_ixele,R_avele,REAL(R_itele,4),REAL(R_etele,4),L_oxele
     &)
      !}
C FDA60b1_vlv.fgi( 399, 517):���������� ������� ��� 2,20FDA62AC002XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obile,R_okile,REAL(1
     &,4),
     & REAL(R_udile,4),I_ikile,REAL(R_edile,4),
     & REAL(R_idile,4),REAL(R_ibile,4),
     & REAL(R_ebile,4),REAL(R_efile,4),L_ifile,REAL(R_ofile
     &,4),L_ufile,
     & L_akile,R_odile,REAL(R_adile,4),REAL(R_ubile,4),L_ekile
     &)
      !}
C FDA60b1_vlv.fgi( 399, 532):���������� ������� ��� 2,20FDA62AC001XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_elile,R_erile,REAL(1
     &,4),
     & REAL(R_imile,4),I_arile,REAL(R_ulile,4),
     & REAL(R_amile,4),REAL(R_alile,4),
     & REAL(R_ukile,4),REAL(R_umile,4),L_apile,REAL(R_epile
     &,4),L_ipile,
     & L_opile,R_emile,REAL(R_olile,4),REAL(R_ilile,4),L_upile
     &)
      !}
C FDA60b1_vlv.fgi( 399, 547):���������� ������� ��� 2,20FDA62AC003XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_urile,R_uvile,REAL(1
     &,4),
     & REAL(R_atile,4),I_ovile,REAL(R_isile,4),
     & REAL(R_osile,4),REAL(R_orile,4),
     & REAL(R_irile,4),REAL(R_itile,4),L_otile,REAL(R_utile
     &,4),L_avile,
     & L_evile,R_usile,REAL(R_esile,4),REAL(R_asile,4),L_ivile
     &)
      !}
C FDA60b1_vlv.fgi( 399, 562):���������� ������� ��� 2,20FDA62AC002XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ixile,R_ifole,REAL(1
     &,4),
     & REAL(R_obole,4),I_efole,REAL(R_abole,4),
     & REAL(R_ebole,4),REAL(R_exile,4),
     & REAL(R_axile,4),REAL(R_adole,4),L_edole,REAL(R_idole
     &,4),L_odole,
     & L_udole,R_ibole,REAL(R_uxile,4),REAL(R_oxile,4),L_afole
     &)
      !}
C FDA60b1_vlv.fgi( 399, 579):���������� ������� ��� 2,20FDA62AC001XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_akole,R_apole,REAL(1
     &,4),
     & REAL(R_elole,4),I_umole,REAL(R_okole,4),
     & REAL(R_ukole,4),REAL(R_ufole,4),
     & REAL(R_ofole,4),REAL(R_olole,4),L_ulole,REAL(R_amole
     &,4),L_emole,
     & L_imole,R_alole,REAL(R_ikole,4),REAL(R_ekole,4),L_omole
     &)
      !}
C FDA60b1_vlv.fgi( 367, 500):���������� ������� ��� 2,20FDA61AC003XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_opole,R_otole,REAL(1
     &,4),
     & REAL(R_urole,4),I_itole,REAL(R_erole,4),
     & REAL(R_irole,4),REAL(R_ipole,4),
     & REAL(R_epole,4),REAL(R_esole,4),L_isole,REAL(R_osole
     &,4),L_usole,
     & L_atole,R_orole,REAL(R_arole,4),REAL(R_upole,4),L_etole
     &)
      !}
C FDA60b1_vlv.fgi( 367, 517):���������� ������� ��� 2,20FDA61AC002XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_evole,R_edule,REAL(1
     &,4),
     & REAL(R_ixole,4),I_adule,REAL(R_uvole,4),
     & REAL(R_axole,4),REAL(R_avole,4),
     & REAL(R_utole,4),REAL(R_uxole,4),L_abule,REAL(R_ebule
     &,4),L_ibule,
     & L_obule,R_exole,REAL(R_ovole,4),REAL(R_ivole,4),L_ubule
     &)
      !}
C FDA60b1_vlv.fgi( 367, 532):���������� ������� ��� 2,20FDA61AC001XQ02
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_udule,R_ulule,REAL(1
     &,4),
     & REAL(R_akule,4),I_olule,REAL(R_ifule,4),
     & REAL(R_ofule,4),REAL(R_odule,4),
     & REAL(R_idule,4),REAL(R_ikule,4),L_okule,REAL(R_ukule
     &,4),L_alule,
     & L_elule,R_ufule,REAL(R_efule,4),REAL(R_afule,4),L_ilule
     &)
      !}
C FDA60b1_vlv.fgi( 367, 547):���������� ������� ��� 2,20FDA61AC003XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imule,R_isule,REAL(1
     &,4),
     & REAL(R_opule,4),I_esule,REAL(R_apule,4),
     & REAL(R_epule,4),REAL(R_emule,4),
     & REAL(R_amule,4),REAL(R_arule,4),L_erule,REAL(R_irule
     &,4),L_orule,
     & L_urule,R_ipule,REAL(R_umule,4),REAL(R_omule,4),L_asule
     &)
      !}
C FDA60b1_vlv.fgi( 367, 562):���������� ������� ��� 2,20FDA61AC002XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_atule,R_abame,REAL(1
     &,4),
     & REAL(R_evule,4),I_uxule,REAL(R_otule,4),
     & REAL(R_utule,4),REAL(R_usule,4),
     & REAL(R_osule,4),REAL(R_ovule,4),L_uvule,REAL(R_axule
     &,4),L_exule,
     & L_ixule,R_avule,REAL(R_itule,4),REAL(R_etule,4),L_oxule
     &)
      !}
C FDA60b1_vlv.fgi( 367, 579):���������� ������� ��� 2,20FDA61AC001XQ01
      R_amise = R8_emise
C FDA60_sens.fgi( 350, 350):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_o,R_ik,REAL(1,4),
     & REAL(R_amise,4),I_ek,REAL(R_ed,4),
     & REAL(R_id,4),REAL(R_i,4),
     & REAL(R_e,4),REAL(R_af,4),L_ef,REAL(R_if,4),L_of,
     & L_uf,R_od,REAL(R_ad,4),REAL(R_u,4),L_ak)
      !}
C FDA60b1_vlv.fgi( 363,  62):���������� ������� ��� 2,20FDA66CQ009XQ01
      R_imise = R8_omise
C FDA60_sens.fgi( 350, 354):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ate,R_uxe,REAL(1,4)
     &,
     & REAL(R_imise,4),I_oxe,REAL(R_ote,4),
     & REAL(R_ute,4),REAL(R_use,4),
     & REAL(R_ose,4),REAL(R_ive,4),L_ove,REAL(R_uve,4),L_axe
     &,
     & L_exe,R_ave,REAL(R_ite,4),REAL(R_ete,4),L_ixe)
      !}
C FDA60b1_vlv.fgi( 363,  78):���������� ������� ��� 2,20FDA66CM007XQ01
      R_umise = R8_apise
C FDA60_sens.fgi( 350, 358):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_al,R_up,REAL(1,4),
     & REAL(R_umise,4),I_op,REAL(R_ol,4),
     & REAL(R_ul,4),REAL(R_uk,4),
     & REAL(R_ok,4),REAL(R_im,4),L_om,REAL(R_um,4),L_ap,
     & L_ep,R_am,REAL(R_il,4),REAL(R_el,4),L_ip)
      !}
C FDA60b1_vlv.fgi( 335,  62):���������� ������� ��� 2,20FDA65CQ009XQ01
      R_epise = R8_ipise
C FDA60_sens.fgi( 350, 362):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ibi,R_eki,REAL(1,4)
     &,
     & REAL(R_epise,4),I_aki,REAL(R_adi,4),
     & REAL(R_edi,4),REAL(R_ebi,4),
     & REAL(R_abi,4),REAL(R_udi,4),L_afi,REAL(R_efi,4),L_ifi
     &,
     & L_ofi,R_idi,REAL(R_ubi,4),REAL(R_obi,4),L_ufi)
      !}
C FDA60b1_vlv.fgi( 334,  78):���������� ������� ��� 2,20FDA65CM007XQ01
      R_opise = R8_upise
C FDA60_sens.fgi( 350, 366):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ir,R_ev,REAL(1,4),
     & REAL(R_opise,4),I_av,REAL(R_as,4),
     & REAL(R_es,4),REAL(R_er,4),
     & REAL(R_ar,4),REAL(R_us,4),L_at,REAL(R_et,4),L_it,
     & L_ot,R_is,REAL(R_ur,4),REAL(R_or,4),L_ut)
      !}
C FDA60b1_vlv.fgi( 307,  62):���������� ������� ��� 2,20FDA64CQ009XQ01
      R_arise = R8_erise
C FDA60_sens.fgi( 350, 370):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uki,R_opi,REAL(1,4)
     &,
     & REAL(R_arise,4),I_ipi,REAL(R_ili,4),
     & REAL(R_oli,4),REAL(R_oki,4),
     & REAL(R_iki,4),REAL(R_emi,4),L_imi,REAL(R_omi,4),L_umi
     &,
     & L_api,R_uli,REAL(R_eli,4),REAL(R_ali,4),L_epi)
      !}
C FDA60b1_vlv.fgi( 307,  78):���������� ������� ��� 2,20FDA64CM007XQ01
      R_irise = R8_orise
C FDA60_sens.fgi( 350, 374):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uv,R_ode,REAL(1,4),
     & REAL(R_irise,4),I_ide,REAL(R_ix,4),
     & REAL(R_ox,4),REAL(R_ov,4),
     & REAL(R_iv,4),REAL(R_ebe,4),L_ibe,REAL(R_obe,4),L_ube
     &,
     & L_ade,R_ux,REAL(R_ex,4),REAL(R_ax,4),L_ede)
      !}
C FDA60b1_vlv.fgi( 279,  62):���������� ������� ��� 2,20FDA63CQ009XQ01
      R_urise = R8_asise
C FDA60_sens.fgi( 350, 378):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_eri,R_avi,REAL(1,4)
     &,
     & REAL(R_urise,4),I_uti,REAL(R_uri,4),
     & REAL(R_asi,4),REAL(R_ari,4),
     & REAL(R_upi,4),REAL(R_osi,4),L_usi,REAL(R_ati,4),L_eti
     &,
     & L_iti,R_esi,REAL(R_ori,4),REAL(R_iri,4),L_oti)
      !}
C FDA60b1_vlv.fgi( 278,  78):���������� ������� ��� 2,20FDA63CM007XQ01
      R_esise = R8_isise
C FDA60_sens.fgi( 350, 382):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efe,R_ame,REAL(1,4)
     &,
     & REAL(R_esise,4),I_ule,REAL(R_ufe,4),
     & REAL(R_ake,4),REAL(R_afe,4),
     & REAL(R_ude,4),REAL(R_oke,4),L_uke,REAL(R_ale,4),L_ele
     &,
     & L_ile,R_eke,REAL(R_ofe,4),REAL(R_ife,4),L_ole)
      !}
C FDA60b1_vlv.fgi( 249,  62):���������� ������� ��� 2,20FDA62CQ009XQ01
      R_osise = R8_usise
C FDA60_sens.fgi( 350, 386):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovi,R_ido,REAL(1,4)
     &,
     & REAL(R_osise,4),I_edo,REAL(R_exi,4),
     & REAL(R_ixi,4),REAL(R_ivi,4),
     & REAL(R_evi,4),REAL(R_abo,4),L_ebo,REAL(R_ibo,4),L_obo
     &,
     & L_ubo,R_oxi,REAL(R_axi,4),REAL(R_uvi,4),L_ado)
      !}
C FDA60b1_vlv.fgi( 249,  78):���������� ������� ��� 2,20FDA62CM007XQ01
      R_atise = R8_etise
C FDA60_sens.fgi( 350, 390):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ome,R_ise,REAL(1,4)
     &,
     & REAL(R_atise,4),I_ese,REAL(R_epe,4),
     & REAL(R_ipe,4),REAL(R_ime,4),
     & REAL(R_eme,4),REAL(R_are,4),L_ere,REAL(R_ire,4),L_ore
     &,
     & L_ure,R_ope,REAL(R_ape,4),REAL(R_ume,4),L_ase)
      !}
C FDA60b1_vlv.fgi( 220,  62):���������� ������� ��� 2,20FDA61CQ009XQ01
      R_itise = R8_otise
C FDA60_sens.fgi( 350, 394):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_afo,R_ulo,REAL(1,4)
     &,
     & REAL(R_itise,4),I_olo,REAL(R_ofo,4),
     & REAL(R_ufo,4),REAL(R_udo,4),
     & REAL(R_odo,4),REAL(R_iko,4),L_oko,REAL(R_uko,4),L_alo
     &,
     & L_elo,R_ako,REAL(R_ifo,4),REAL(R_efo,4),L_ilo)
      !}
C FDA60b1_vlv.fgi( 221,  78):���������� ������� ��� 2,20FDA61CM007XQ01
      R_utise = R8_avise
C FDA60_sens.fgi( 250, 330):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imo,R_eso,REAL(1,4)
     &,
     & REAL(R_utise,4),I_aso,REAL(R_apo,4),
     & REAL(R_epo,4),REAL(R_emo,4),
     & REAL(R_amo,4),REAL(R_upo,4),L_aro,REAL(R_ero,4),L_iro
     &,
     & L_oro,R_ipo,REAL(R_umo,4),REAL(R_omo,4),L_uro)
      !}
C FDA60b1_vlv.fgi( 362,  93):���������� ������� ��� 2,20FDA66CF023XQ01
      R_evise = R8_ivise
C FDA60_sens.fgi( 250, 334):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uso,R_oxo,REAL(1,4)
     &,
     & REAL(R_evise,4),I_ixo,REAL(R_ito,4),
     & REAL(R_oto,4),REAL(R_oso,4),
     & REAL(R_iso,4),REAL(R_evo,4),L_ivo,REAL(R_ovo,4),L_uvo
     &,
     & L_axo,R_uto,REAL(R_eto,4),REAL(R_ato,4),L_exo)
      !}
C FDA60b1_vlv.fgi( 334,  93):���������� ������� ��� 2,20FDA65CF023XQ01
      R_ovise = R8_uvise
C FDA60_sens.fgi( 250, 338):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ebu,R_aku,REAL(1,4)
     &,
     & REAL(R_ovise,4),I_ufu,REAL(R_ubu,4),
     & REAL(R_adu,4),REAL(R_abu,4),
     & REAL(R_uxo,4),REAL(R_odu,4),L_udu,REAL(R_afu,4),L_efu
     &,
     & L_ifu,R_edu,REAL(R_obu,4),REAL(R_ibu,4),L_ofu)
      !}
C FDA60b1_vlv.fgi( 306,  93):���������� ������� ��� 2,20FDA64CF023XQ01
      R_axise = R8_exise
C FDA60_sens.fgi( 250, 342):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_oku,R_ipu,REAL(1,4)
     &,
     & REAL(R_axise,4),I_epu,REAL(R_elu,4),
     & REAL(R_ilu,4),REAL(R_iku,4),
     & REAL(R_eku,4),REAL(R_amu,4),L_emu,REAL(R_imu,4),L_omu
     &,
     & L_umu,R_olu,REAL(R_alu,4),REAL(R_uku,4),L_apu)
      !}
C FDA60b1_vlv.fgi( 278,  93):���������� ������� ��� 2,20FDA63CF023XQ01
      R_ixise = R8_oxise
C FDA60_sens.fgi( 250, 346):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_aru,R_utu,REAL(1,4)
     &,
     & REAL(R_ixise,4),I_otu,REAL(R_oru,4),
     & REAL(R_uru,4),REAL(R_upu,4),
     & REAL(R_opu,4),REAL(R_isu,4),L_osu,REAL(R_usu,4),L_atu
     &,
     & L_etu,R_asu,REAL(R_iru,4),REAL(R_eru,4),L_itu)
      !}
C FDA60b1_vlv.fgi( 249,  93):���������� ������� ��� 2,20FDA62CF023XQ01
      R_uxise = R8_abose
C FDA60_sens.fgi( 250, 350):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ivu,R_edad,REAL(1,4
     &),
     & REAL(R_uxise,4),I_adad,REAL(R_axu,4),
     & REAL(R_exu,4),REAL(R_evu,4),
     & REAL(R_avu,4),REAL(R_uxu,4),L_abad,REAL(R_ebad,4),L_ibad
     &,
     & L_obad,R_ixu,REAL(R_uvu,4),REAL(R_ovu,4),L_ubad)
      !}
C FDA60b1_vlv.fgi( 221,  93):���������� ������� ��� 2,20FDA61CF023XQ01
      R_ebose = R8_ibose
C FDA60_sens.fgi( 250, 304):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_udad,R_olad,REAL(1,4
     &),
     & REAL(R_ebose,4),I_ilad,REAL(R_ifad,4),
     & REAL(R_ofad,4),REAL(R_odad,4),
     & REAL(R_idad,4),REAL(R_ekad,4),L_ikad,REAL(R_okad,4
     &),L_ukad,
     & L_alad,R_ufad,REAL(R_efad,4),REAL(R_afad,4),L_elad
     &)
      !}
C FDA60b1_vlv.fgi( 362, 109):���������� ������� ��� 2,20FDA66CF022XQ01
      R_obose = R8_ubose
C FDA60_sens.fgi( 250, 308):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_emad,R_asad,REAL(1,4
     &),
     & REAL(R_obose,4),I_urad,REAL(R_umad,4),
     & REAL(R_apad,4),REAL(R_amad,4),
     & REAL(R_ulad,4),REAL(R_opad,4),L_upad,REAL(R_arad,4
     &),L_erad,
     & L_irad,R_epad,REAL(R_omad,4),REAL(R_imad,4),L_orad
     &)
      !}
C FDA60b1_vlv.fgi( 334, 109):���������� ������� ��� 2,20FDA65CF022XQ01
      R_adose = R8_edose
C FDA60_sens.fgi( 250, 312):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_osad,R_ixad,REAL(1,4
     &),
     & REAL(R_adose,4),I_exad,REAL(R_etad,4),
     & REAL(R_itad,4),REAL(R_isad,4),
     & REAL(R_esad,4),REAL(R_avad,4),L_evad,REAL(R_ivad,4
     &),L_ovad,
     & L_uvad,R_otad,REAL(R_atad,4),REAL(R_usad,4),L_axad
     &)
      !}
C FDA60b1_vlv.fgi( 306, 109):���������� ������� ��� 2,20FDA64CF022XQ01
      R_idose = R8_odose
C FDA60_sens.fgi( 250, 316):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_abed,R_ufed,REAL(1,4
     &),
     & REAL(R_idose,4),I_ofed,REAL(R_obed,4),
     & REAL(R_ubed,4),REAL(R_uxad,4),
     & REAL(R_oxad,4),REAL(R_ided,4),L_oded,REAL(R_uded,4
     &),L_afed,
     & L_efed,R_aded,REAL(R_ibed,4),REAL(R_ebed,4),L_ifed
     &)
      !}
C FDA60b1_vlv.fgi( 278, 109):���������� ������� ��� 2,20FDA63CF022XQ01
      R_udose = R8_afose
C FDA60_sens.fgi( 250, 320):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_iked,R_eped,REAL(1,4
     &),
     & REAL(R_udose,4),I_aped,REAL(R_aled,4),
     & REAL(R_eled,4),REAL(R_eked,4),
     & REAL(R_aked,4),REAL(R_uled,4),L_amed,REAL(R_emed,4
     &),L_imed,
     & L_omed,R_iled,REAL(R_uked,4),REAL(R_oked,4),L_umed
     &)
      !}
C FDA60b1_vlv.fgi( 249, 109):���������� ������� ��� 2,20FDA62CF022XQ01
      R_efose = R8_ifose
C FDA60_sens.fgi( 250, 324):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uped,R_oted,REAL(1,4
     &),
     & REAL(R_efose,4),I_ited,REAL(R_ired,4),
     & REAL(R_ored,4),REAL(R_oped,4),
     & REAL(R_iped,4),REAL(R_esed,4),L_ised,REAL(R_osed,4
     &),L_used,
     & L_ated,R_ured,REAL(R_ered,4),REAL(R_ared,4),L_eted
     &)
      !}
C FDA60b1_vlv.fgi( 221, 109):���������� ������� ��� 2,20FDA61CF022XQ01
      R_ofose = R8_ufose
C FDA60_sens.fgi( 153, 325):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_eved,R_adid,REAL(1e
     &-3,4),
     & REAL(R_ofose,4),I_ubid,REAL(R_uved,4),
     & REAL(R_axed,4),REAL(R_aved,4),
     & REAL(R_uted,4),REAL(R_oxed,4),L_uxed,REAL(R_abid,4
     &),L_ebid,
     & L_ibid,R_exed,REAL(R_oved,4),REAL(R_ived,4),L_obid
     &)
      !}
C FDA60b1_vlv.fgi( 362, 125):���������� ������� ��� 2,20FDA66CP022XQ01
      R_akose = R8_ekose
C FDA60_sens.fgi( 153, 328):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_odid,R_ilid,REAL(1e
     &-3,4),
     & REAL(R_akose,4),I_elid,REAL(R_efid,4),
     & REAL(R_ifid,4),REAL(R_idid,4),
     & REAL(R_edid,4),REAL(R_akid,4),L_ekid,REAL(R_ikid,4
     &),L_okid,
     & L_ukid,R_ofid,REAL(R_afid,4),REAL(R_udid,4),L_alid
     &)
      !}
C FDA60b1_vlv.fgi( 334, 125):���������� ������� ��� 2,20FDA65CP022XQ01
      R_ikose = R8_okose
C FDA60_sens.fgi( 153, 331):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_amid,R_urid,REAL(1e
     &-3,4),
     & REAL(R_ikose,4),I_orid,REAL(R_omid,4),
     & REAL(R_umid,4),REAL(R_ulid,4),
     & REAL(R_olid,4),REAL(R_ipid,4),L_opid,REAL(R_upid,4
     &),L_arid,
     & L_erid,R_apid,REAL(R_imid,4),REAL(R_emid,4),L_irid
     &)
      !}
C FDA60b1_vlv.fgi( 306, 125):���������� ������� ��� 2,20FDA64CP022XQ01
      R_ukose = R8_alose
C FDA60_sens.fgi( 153, 334):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_isid,R_exid,REAL(1e
     &-3,4),
     & REAL(R_ukose,4),I_axid,REAL(R_atid,4),
     & REAL(R_etid,4),REAL(R_esid,4),
     & REAL(R_asid,4),REAL(R_utid,4),L_avid,REAL(R_evid,4
     &),L_ivid,
     & L_ovid,R_itid,REAL(R_usid,4),REAL(R_osid,4),L_uvid
     &)
      !}
C FDA60b1_vlv.fgi( 278, 125):���������� ������� ��� 2,20FDA63CP022XQ01
      R_elose = R8_ilose
C FDA60_sens.fgi( 153, 337):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uxid,R_ofod,REAL(1e
     &-3,4),
     & REAL(R_elose,4),I_ifod,REAL(R_ibod,4),
     & REAL(R_obod,4),REAL(R_oxid,4),
     & REAL(R_ixid,4),REAL(R_edod,4),L_idod,REAL(R_odod,4
     &),L_udod,
     & L_afod,R_ubod,REAL(R_ebod,4),REAL(R_abod,4),L_efod
     &)
      !}
C FDA60b1_vlv.fgi( 249, 125):���������� ������� ��� 2,20FDA62CP022XQ01
      R_olose = R8_ulose
C FDA60_sens.fgi( 153, 340):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ekod,R_apod,REAL(1e
     &-3,4),
     & REAL(R_olose,4),I_umod,REAL(R_ukod,4),
     & REAL(R_alod,4),REAL(R_akod,4),
     & REAL(R_ufod,4),REAL(R_olod,4),L_ulod,REAL(R_amod,4
     &),L_emod,
     & L_imod,R_elod,REAL(R_okod,4),REAL(R_ikod,4),L_omod
     &)
      !}
C FDA60b1_vlv.fgi( 221, 125):���������� ������� ��� 2,20FDA61CP022XQ01
      R_amose = R8_emose
C FDA60_sens.fgi(  60, 305):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ixal,R_efel,REAL(1,4
     &),
     & REAL(R_amose,4),I_afel,REAL(R_abel,4),
     & REAL(R_ebel,4),REAL(R_exal,4),
     & REAL(R_axal,4),REAL(R_ubel,4),L_adel,REAL(R_edel,4
     &),L_idel,
     & L_odel,R_ibel,REAL(R_uxal,4),REAL(R_oxal,4),L_udel
     &)
      !}
C FDA60b1_vlv.fgi( 360, 259):���������� ������� ��� 2,20FDA66CP021XQ01
      R_imose = R8_omose
C FDA60_sens.fgi(  60, 308):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_epel,R_atel,REAL(1,4
     &),
     & REAL(R_imose,4),I_usel,REAL(R_upel,4),
     & REAL(R_arel,4),REAL(R_apel,4),
     & REAL(R_umel,4),REAL(R_orel,4),L_urel,REAL(R_asel,4
     &),L_esel,
     & L_isel,R_erel,REAL(R_opel,4),REAL(R_ipel,4),L_osel
     &)
      !}
C FDA60b1_vlv.fgi( 332, 259):���������� ������� ��� 2,20FDA65CP021XQ01
      R_umose = R8_apose
C FDA60_sens.fgi(  60, 311):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_adil,R_ukil,REAL(1,4
     &),
     & REAL(R_umose,4),I_okil,REAL(R_odil,4),
     & REAL(R_udil,4),REAL(R_ubil,4),
     & REAL(R_obil,4),REAL(R_ifil,4),L_ofil,REAL(R_ufil,4
     &),L_akil,
     & L_ekil,R_afil,REAL(R_idil,4),REAL(R_edil,4),L_ikil
     &)
      !}
C FDA60b1_vlv.fgi( 304, 259):���������� ������� ��� 2,20FDA64CP021XQ01
      R_epose = R8_ipose
C FDA60_sens.fgi(  60, 314):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uril,R_ovil,REAL(1,4
     &),
     & REAL(R_epose,4),I_ivil,REAL(R_isil,4),
     & REAL(R_osil,4),REAL(R_oril,4),
     & REAL(R_iril,4),REAL(R_etil,4),L_itil,REAL(R_otil,4
     &),L_util,
     & L_avil,R_usil,REAL(R_esil,4),REAL(R_asil,4),L_evil
     &)
      !}
C FDA60b1_vlv.fgi( 276, 259):���������� ������� ��� 2,20FDA63CP021XQ01
      R_opose = R8_upose
C FDA60_sens.fgi(  60, 317):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ofol,R_imol,REAL(1,4
     &),
     & REAL(R_opose,4),I_emol,REAL(R_ekol,4),
     & REAL(R_ikol,4),REAL(R_ifol,4),
     & REAL(R_efol,4),REAL(R_alol,4),L_elol,REAL(R_ilol,4
     &),L_olol,
     & L_ulol,R_okol,REAL(R_akol,4),REAL(R_ufol,4),L_amol
     &)
      !}
C FDA60b1_vlv.fgi( 248, 259):���������� ������� ��� 2,20FDA62CP021XQ01
      R_arose = R8_erose
C FDA60_sens.fgi(  60, 320):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itol,R_ebul,REAL(1,4
     &),
     & REAL(R_arose,4),I_abul,REAL(R_avol,4),
     & REAL(R_evol,4),REAL(R_etol,4),
     & REAL(R_atol,4),REAL(R_uvol,4),L_axol,REAL(R_exol,4
     &),L_ixol,
     & L_oxol,R_ivol,REAL(R_utol,4),REAL(R_otol,4),L_uxol
     &)
      !}
C FDA60b1_vlv.fgi( 220, 259):���������� ������� ��� 2,20FDA61CP021XQ01
      R_irose = R8_orose
C FDA60_sens.fgi(  60, 323):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ufel,R_omel,REAL(1,4
     &),
     & REAL(R_irose,4),I_imel,REAL(R_ikel,4),
     & REAL(R_okel,4),REAL(R_ofel,4),
     & REAL(R_ifel,4),REAL(R_elel,4),L_ilel,REAL(R_olel,4
     &),L_ulel,
     & L_amel,R_ukel,REAL(R_ekel,4),REAL(R_akel,4),L_emel
     &)
      !}
C FDA60b1_vlv.fgi( 360, 243):���������� ������� ��� 2,20FDA66CP020XQ01
      R_urose = R8_asose
C FDA60_sens.fgi(  60, 326):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_otel,R_ibil,REAL(1,4
     &),
     & REAL(R_urose,4),I_ebil,REAL(R_evel,4),
     & REAL(R_ivel,4),REAL(R_itel,4),
     & REAL(R_etel,4),REAL(R_axel,4),L_exel,REAL(R_ixel,4
     &),L_oxel,
     & L_uxel,R_ovel,REAL(R_avel,4),REAL(R_utel,4),L_abil
     &)
      !}
C FDA60b1_vlv.fgi( 332, 243):���������� ������� ��� 2,20FDA65CP020XQ01
      R_esose = R8_isose
C FDA60_sens.fgi(  60, 329):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ilil,R_eril,REAL(1,4
     &),
     & REAL(R_esose,4),I_aril,REAL(R_amil,4),
     & REAL(R_emil,4),REAL(R_elil,4),
     & REAL(R_alil,4),REAL(R_umil,4),L_apil,REAL(R_epil,4
     &),L_ipil,
     & L_opil,R_imil,REAL(R_ulil,4),REAL(R_olil,4),L_upil
     &)
      !}
C FDA60b1_vlv.fgi( 304, 243):���������� ������� ��� 2,20FDA64CP020XQ01
      R_osose = R8_usose
C FDA60_sens.fgi(  60, 332):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_exil,R_afol,REAL(1,4
     &),
     & REAL(R_osose,4),I_udol,REAL(R_uxil,4),
     & REAL(R_abol,4),REAL(R_axil,4),
     & REAL(R_uvil,4),REAL(R_obol,4),L_ubol,REAL(R_adol,4
     &),L_edol,
     & L_idol,R_ebol,REAL(R_oxil,4),REAL(R_ixil,4),L_odol
     &)
      !}
C FDA60b1_vlv.fgi( 276, 243):���������� ������� ��� 2,20FDA63CP020XQ01
      R_atose = R8_etose
C FDA60_sens.fgi(  60, 335):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_apol,R_usol,REAL(1,4
     &),
     & REAL(R_atose,4),I_osol,REAL(R_opol,4),
     & REAL(R_upol,4),REAL(R_umol,4),
     & REAL(R_omol,4),REAL(R_irol,4),L_orol,REAL(R_urol,4
     &),L_asol,
     & L_esol,R_arol,REAL(R_ipol,4),REAL(R_epol,4),L_isol
     &)
      !}
C FDA60b1_vlv.fgi( 248, 243):���������� ������� ��� 2,20FDA62CP020XQ01
      R_itose = R8_otose
C FDA60_sens.fgi(  60, 338):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ubul,R_okul,REAL(1,4
     &),
     & REAL(R_itose,4),I_ikul,REAL(R_idul,4),
     & REAL(R_odul,4),REAL(R_obul,4),
     & REAL(R_ibul,4),REAL(R_eful,4),L_iful,REAL(R_oful,4
     &),L_uful,
     & L_akul,R_udul,REAL(R_edul,4),REAL(R_adul,4),L_ekul
     &)
      !}
C FDA60b1_vlv.fgi( 220, 243):���������� ������� ��� 2,20FDA61CP020XQ01
      R_utose = R8_avose
C FDA60_sens.fgi( 153, 302):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ivul,4),REAL
     &(R_utose,4),R_urul,
     & R_ovul,REAL(1,4),REAL(R_isul,4),
     & REAL(R_osul,4),REAL(R_orul,4),
     & REAL(R_irul,4),I_evul,REAL(R_atul,4),L_etul,
     & REAL(R_itul,4),L_otul,L_utul,R_usul,REAL(R_esul,4)
     &,REAL(R_asul,4),L_avul)
      !}
C FDA60b1_vlv.fgi( 159,  46):���������� ������� ��������,20FDA66CP019XQ01
      R_evose = R8_ivose
C FDA60_sens.fgi( 153, 305):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_omam,4),REAL
     &(R_evose,4),R_akam,
     & R_umam,REAL(1,4),REAL(R_okam,4),
     & REAL(R_ukam,4),REAL(R_ufam,4),
     & REAL(R_ofam,4),I_imam,REAL(R_elam,4),L_ilam,
     & REAL(R_olam,4),L_ulam,L_amam,R_alam,REAL(R_ikam,4)
     &,REAL(R_ekam,4),L_emam)
      !}
C FDA60b1_vlv.fgi( 131,  46):���������� ������� ��������,20FDA65CP019XQ01
      R_ovose = R8_uvose
C FDA60_sens.fgi( 153, 308):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ubem,4),REAL
     &(R_ovose,4),R_evam,
     & R_adem,REAL(1,4),REAL(R_uvam,4),
     & REAL(R_axam,4),REAL(R_avam,4),
     & REAL(R_utam,4),I_obem,REAL(R_ixam,4),L_oxam,
     & REAL(R_uxam,4),L_abem,L_ebem,R_exam,REAL(R_ovam,4)
     &,REAL(R_ivam,4),L_ibem)
      !}
C FDA60b1_vlv.fgi( 103,  46):���������� ������� ��������,20FDA64CP019XQ01
      R_axose = R8_exose
C FDA60_sens.fgi( 153, 311):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_asem,4),REAL
     &(R_axose,4),R_imem,
     & R_esem,REAL(1,4),REAL(R_apem,4),
     & REAL(R_epem,4),REAL(R_emem,4),
     & REAL(R_amem,4),I_urem,REAL(R_opem,4),L_upem,
     & REAL(R_arem,4),L_erem,L_irem,R_ipem,REAL(R_umem,4)
     &,REAL(R_omem,4),L_orem)
      !}
C FDA60b1_vlv.fgi(  75,  46):���������� ������� ��������,20FDA63CP019XQ01
      R_ixose = R8_oxose
C FDA60_sens.fgi( 153, 314):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ekim,4),REAL
     &(R_ixose,4),R_obim,
     & R_ikim,REAL(1,4),REAL(R_edim,4),
     & REAL(R_idim,4),REAL(R_ibim,4),
     & REAL(R_ebim,4),I_akim,REAL(R_udim,4),L_afim,
     & REAL(R_efim,4),L_ifim,L_ofim,R_odim,REAL(R_adim,4)
     &,REAL(R_ubim,4),L_ufim)
      !}
C FDA60b1_vlv.fgi(  47,  46):���������� ������� ��������,20FDA62CP019XQ01
      R_uxose = R8_abuse
C FDA60_sens.fgi( 153, 317):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ivim,4),REAL
     &(R_uxose,4),R_urim,
     & R_ovim,REAL(1,4),REAL(R_isim,4),
     & REAL(R_osim,4),REAL(R_orim,4),
     & REAL(R_irim,4),I_evim,REAL(R_atim,4),L_etim,
     & REAL(R_itim,4),L_otim,L_utim,R_usim,REAL(R_esim,4)
     &,REAL(R_asim,4),L_avim)
      !}
C FDA60b1_vlv.fgi(  19,  46):���������� ������� ��������,20FDA61CP019XQ01
      R_ebuse = R8_ibuse
C FDA60_sens.fgi( 494, 260):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_urox,R_ovox,REAL(16.6666
     &,4),
     & REAL(R_ebuse,4),I_ivox,REAL(R_isox,4),
     & REAL(R_osox,4),REAL(R_orox,4),
     & REAL(R_irox,4),REAL(R_etox,4),L_itox,REAL(R_otox,4
     &),L_utox,
     & L_avox,R_usox,REAL(R_esox,4),REAL(R_asox,4),L_evox
     &)
      !}
C FDA60b1_vlv.fgi( 158, 336):���������� ������� ��� 2,20FDA66CF012XQ01
      R_aduse = R8_eduse
C FDA60_sens.fgi( 494, 270):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ofux,R_imux,REAL(16.6666
     &,4),
     & REAL(R_aduse,4),I_emux,REAL(R_ekux,4),
     & REAL(R_ikux,4),REAL(R_ifux,4),
     & REAL(R_efux,4),REAL(R_alux,4),L_elux,REAL(R_ilux,4
     &),L_olux,
     & L_ulux,R_okux,REAL(R_akux,4),REAL(R_ufux,4),L_amux
     &)
      !}
C FDA60b1_vlv.fgi( 130, 336):���������� ������� ��� 2,20FDA65CF012XQ01
      R_uduse = R8_afuse
C FDA60_sens.fgi( 494, 280):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itux,R_ebabe,REAL(16.6666
     &,4),
     & REAL(R_uduse,4),I_ababe,REAL(R_avux,4),
     & REAL(R_evux,4),REAL(R_etux,4),
     & REAL(R_atux,4),REAL(R_uvux,4),L_axux,REAL(R_exux,4
     &),L_ixux,
     & L_oxux,R_ivux,REAL(R_utux,4),REAL(R_otux,4),L_uxux
     &)
      !}
C FDA60b1_vlv.fgi( 104, 336):���������� ������� ��� 2,20FDA64CF012XQ01
      R_ofuse = R8_ufuse
C FDA60_sens.fgi( 494, 290):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_elabe,R_arabe,REAL(16.6666
     &,4),
     & REAL(R_ofuse,4),I_upabe,REAL(R_ulabe,4),
     & REAL(R_amabe,4),REAL(R_alabe,4),
     & REAL(R_ukabe,4),REAL(R_omabe,4),L_umabe,REAL(R_apabe
     &,4),L_epabe,
     & L_ipabe,R_emabe,REAL(R_olabe,4),REAL(R_ilabe,4),L_opabe
     &)
      !}
C FDA60b1_vlv.fgi(  76, 336):���������� ������� ��� 2,20FDA63CF012XQ01
      R_ikuse = R8_okuse
C FDA60_sens.fgi( 494, 300):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_axabe,R_udebe,REAL(16.6666
     &,4),
     & REAL(R_ikuse,4),I_odebe,REAL(R_oxabe,4),
     & REAL(R_uxabe,4),REAL(R_uvabe,4),
     & REAL(R_ovabe,4),REAL(R_ibebe,4),L_obebe,REAL(R_ubebe
     &,4),L_adebe,
     & L_edebe,R_abebe,REAL(R_ixabe,4),REAL(R_exabe,4),L_idebe
     &)
      !}
C FDA60b1_vlv.fgi(  48, 336):���������� ������� ��� 2,20FDA62CF012XQ01
      R_eluse = R8_iluse
C FDA60_sens.fgi( 494, 310):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umebe,R_osebe,REAL(16.6666
     &,4),
     & REAL(R_eluse,4),I_isebe,REAL(R_ipebe,4),
     & REAL(R_opebe,4),REAL(R_omebe,4),
     & REAL(R_imebe,4),REAL(R_erebe,4),L_irebe,REAL(R_orebe
     &,4),L_urebe,
     & L_asebe,R_upebe,REAL(R_epebe,4),REAL(R_apebe,4),L_esebe
     &)
      !}
C FDA60b1_vlv.fgi(  22, 336):���������� ������� ��� 2,20FDA61CF012XQ01
      R_umute = R8_apute
C FDA60_sens.fgi( 218, 182):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imade,R_esade,REAL(16.6666
     &,4),
     & REAL(R_umute,4),I_asade,REAL(R_apade,4),
     & REAL(R_epade,4),REAL(R_emade,4),
     & REAL(R_amade,4),REAL(R_upade,4),L_arade,REAL(R_erade
     &,4),L_irade,
     & L_orade,R_ipade,REAL(R_umade,4),REAL(R_omade,4),L_urade
     &)
      !}
C FDA60b1_vlv.fgi( 158, 397):���������� ������� ��� 2,20FDA66CF011XQ01
      R_epute = R8_ipute
C FDA60_sens.fgi( 218, 186):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_usade,R_oxade,REAL(16.6666
     &,4),
     & REAL(R_epute,4),I_ixade,REAL(R_itade,4),
     & REAL(R_otade,4),REAL(R_osade,4),
     & REAL(R_isade,4),REAL(R_evade,4),L_ivade,REAL(R_ovade
     &,4),L_uvade,
     & L_axade,R_utade,REAL(R_etade,4),REAL(R_atade,4),L_exade
     &)
      !}
C FDA60b1_vlv.fgi( 130, 397):���������� ������� ��� 2,20FDA65CF011XQ01
      R_opute = R8_upute
C FDA60_sens.fgi( 218, 190):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ebede,R_akede,REAL(16.6666
     &,4),
     & REAL(R_opute,4),I_ufede,REAL(R_ubede,4),
     & REAL(R_adede,4),REAL(R_abede,4),
     & REAL(R_uxade,4),REAL(R_odede,4),L_udede,REAL(R_afede
     &,4),L_efede,
     & L_ifede,R_edede,REAL(R_obede,4),REAL(R_ibede,4),L_ofede
     &)
      !}
C FDA60b1_vlv.fgi( 104, 397):���������� ������� ��� 2,20FDA64CF011XQ01
      R_arute = R8_erute
C FDA60_sens.fgi( 218, 194):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_okede,R_ipede,REAL(16.6666
     &,4),
     & REAL(R_arute,4),I_epede,REAL(R_elede,4),
     & REAL(R_ilede,4),REAL(R_ikede,4),
     & REAL(R_ekede,4),REAL(R_amede,4),L_emede,REAL(R_imede
     &,4),L_omede,
     & L_umede,R_olede,REAL(R_alede,4),REAL(R_ukede,4),L_apede
     &)
      !}
C FDA60b1_vlv.fgi(  76, 397):���������� ������� ��� 2,20FDA63CF011XQ01
      R_irute = R8_orute
C FDA60_sens.fgi( 218, 198):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_arede,R_utede,REAL(16.6666
     &,4),
     & REAL(R_irute,4),I_otede,REAL(R_orede,4),
     & REAL(R_urede,4),REAL(R_upede,4),
     & REAL(R_opede,4),REAL(R_isede,4),L_osede,REAL(R_usede
     &,4),L_atede,
     & L_etede,R_asede,REAL(R_irede,4),REAL(R_erede,4),L_itede
     &)
      !}
C FDA60b1_vlv.fgi(  48, 397):���������� ������� ��� 2,20FDA62CF011XQ01
      R_urute = R8_asute
C FDA60_sens.fgi( 218, 202):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ivede,R_edide,REAL(16.6666
     &,4),
     & REAL(R_urute,4),I_adide,REAL(R_axede,4),
     & REAL(R_exede,4),REAL(R_evede,4),
     & REAL(R_avede,4),REAL(R_uxede,4),L_abide,REAL(R_ebide
     &,4),L_ibide,
     & L_obide,R_ixede,REAL(R_uvede,4),REAL(R_ovede,4),L_ubide
     &)
      !}
C FDA60b1_vlv.fgi(  22, 397):���������� ������� ��� 2,20FDA61CF011XQ01
      R_epeve = R8_ipeve
C FDA60_sens.fgi(  53, 198):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_udide,R_olide,REAL(16.6666
     &,4),
     & REAL(R_epeve,4),I_ilide,REAL(R_ifide,4),
     & REAL(R_ofide,4),REAL(R_odide,4),
     & REAL(R_idide,4),REAL(R_ekide,4),L_ikide,REAL(R_okide
     &,4),L_ukide,
     & L_alide,R_ufide,REAL(R_efide,4),REAL(R_afide,4),L_elide
     &)
      !}
C FDA60b1_vlv.fgi( 158, 412):���������� ������� ��� 2,20FDA66CF010XQ01
      R_opeve = R8_upeve
C FDA60_sens.fgi(  53, 202):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_emide,R_aside,REAL(16.6666
     &,4),
     & REAL(R_opeve,4),I_uride,REAL(R_umide,4),
     & REAL(R_apide,4),REAL(R_amide,4),
     & REAL(R_ulide,4),REAL(R_opide,4),L_upide,REAL(R_aride
     &,4),L_eride,
     & L_iride,R_epide,REAL(R_omide,4),REAL(R_imide,4),L_oride
     &)
      !}
C FDA60b1_vlv.fgi( 130, 412):���������� ������� ��� 2,20FDA65CF010XQ01
      R_areve = R8_ereve
C FDA60_sens.fgi(  53, 206):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_oside,R_ixide,REAL(16.6666
     &,4),
     & REAL(R_areve,4),I_exide,REAL(R_etide,4),
     & REAL(R_itide,4),REAL(R_iside,4),
     & REAL(R_eside,4),REAL(R_avide,4),L_evide,REAL(R_ivide
     &,4),L_ovide,
     & L_uvide,R_otide,REAL(R_atide,4),REAL(R_uside,4),L_axide
     &)
      !}
C FDA60b1_vlv.fgi( 104, 412):���������� ������� ��� 2,20FDA64CF010XQ01
      R_ireve = R8_oreve
C FDA60_sens.fgi(  53, 210):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_abode,R_ufode,REAL(16.6666
     &,4),
     & REAL(R_ireve,4),I_ofode,REAL(R_obode,4),
     & REAL(R_ubode,4),REAL(R_uxide,4),
     & REAL(R_oxide,4),REAL(R_idode,4),L_odode,REAL(R_udode
     &,4),L_afode,
     & L_efode,R_adode,REAL(R_ibode,4),REAL(R_ebode,4),L_ifode
     &)
      !}
C FDA60b1_vlv.fgi(  76, 412):���������� ������� ��� 2,20FDA63CF010XQ01
      R_ureve = R8_aseve
C FDA60_sens.fgi(  53, 214):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ikode,R_epode,REAL(16.6666
     &,4),
     & REAL(R_ureve,4),I_apode,REAL(R_alode,4),
     & REAL(R_elode,4),REAL(R_ekode,4),
     & REAL(R_akode,4),REAL(R_ulode,4),L_amode,REAL(R_emode
     &,4),L_imode,
     & L_omode,R_ilode,REAL(R_ukode,4),REAL(R_okode,4),L_umode
     &)
      !}
C FDA60b1_vlv.fgi(  48, 412):���������� ������� ��� 2,20FDA62CF010XQ01
      R_eseve = R8_iseve
C FDA60_sens.fgi(  53, 218):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_upode,R_otode,REAL(16.6666
     &,4),
     & REAL(R_eseve,4),I_itode,REAL(R_irode,4),
     & REAL(R_orode,4),REAL(R_opode,4),
     & REAL(R_ipode,4),REAL(R_esode,4),L_isode,REAL(R_osode
     &,4),L_usode,
     & L_atode,R_urode,REAL(R_erode,4),REAL(R_arode,4),L_etode
     &)
      !}
C FDA60b1_vlv.fgi(  22, 412):���������� ������� ��� 2,20FDA61CF010XQ01
      R_uxote = R8_abute
C FDA60_sens.fgi( 305, 288):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_upare,4),REAL
     &(R_uxote,4),R_elare,
     & R_arare,REAL(1e-3,4),REAL(R_ulare,4),
     & REAL(R_amare,4),REAL(R_alare,4),
     & REAL(R_ukare,4),I_opare,REAL(R_imare,4),L_omare,
     & REAL(R_umare,4),L_apare,L_epare,R_emare,REAL(R_olare
     &,4),REAL(R_ilare,4),L_ipare)
      !}
C FDA60b1_vlv.fgi(  20, 508):���������� ������� ��������,20FDA61CP001XQ01
      R_ixote = R8_oxote
C FDA60_sens.fgi( 305, 283):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_etope,4),REAL
     &(R_ixote,4),R_opope,
     & R_itope,REAL(1e-3,4),REAL(R_erope,4),
     & REAL(R_irope,4),REAL(R_ipope,4),
     & REAL(R_epope,4),I_atope,REAL(R_urope,4),L_asope,
     & REAL(R_esope,4),L_isope,L_osope,R_orope,REAL(R_arope
     &,4),REAL(R_upope,4),L_usope)
      !}
C FDA60b1_vlv.fgi(  20, 493):���������� ������� ��������,20FDA61CP002XQ01
      R_axote = R8_exote
C FDA60_sens.fgi( 305, 278):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ekare,4),REAL
     &(R_axote,4),R_obare,
     & R_ikare,REAL(1e-3,4),REAL(R_edare,4),
     & REAL(R_idare,4),REAL(R_ibare,4),
     & REAL(R_ebare,4),I_akare,REAL(R_udare,4),L_afare,
     & REAL(R_efare,4),L_ifare,L_ofare,R_odare,REAL(R_adare
     &,4),REAL(R_ubare,4),L_ufare)
      !}
C FDA60b1_vlv.fgi(  51, 508):���������� ������� ��������,20FDA62CP001XQ01
      R_ovote = R8_uvote
C FDA60_sens.fgi( 305, 273):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_omope,4),REAL
     &(R_ovote,4),R_akope,
     & R_umope,REAL(1e-3,4),REAL(R_okope,4),
     & REAL(R_ukope,4),REAL(R_ufope,4),
     & REAL(R_ofope,4),I_imope,REAL(R_elope,4),L_ilope,
     & REAL(R_olope,4),L_ulope,L_amope,R_alope,REAL(R_ikope
     &,4),REAL(R_ekope,4),L_emope)
      !}
C FDA60b1_vlv.fgi(  51, 493):���������� ������� ��������,20FDA62CP002XQ01
      R_evote = R8_ivote
C FDA60_sens.fgi( 305, 268):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_oxupe,4),REAL
     &(R_evote,4),R_atupe,
     & R_uxupe,REAL(1e-3,4),REAL(R_otupe,4),
     & REAL(R_utupe,4),REAL(R_usupe,4),
     & REAL(R_osupe,4),I_ixupe,REAL(R_evupe,4),L_ivupe,
     & REAL(R_ovupe,4),L_uvupe,L_axupe,R_avupe,REAL(R_itupe
     &,4),REAL(R_etupe,4),L_exupe)
      !}
C FDA60b1_vlv.fgi(  79, 508):���������� ������� ��������,20FDA63CP001XQ01
      R_utote = R8_avote
C FDA60_sens.fgi( 305, 263):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_afope,4),REAL
     &(R_utote,4),R_ixipe,
     & R_efope,REAL(1e-3,4),REAL(R_abope,4),
     & REAL(R_ebope,4),REAL(R_exipe,4),
     & REAL(R_axipe,4),I_udope,REAL(R_obope,4),L_ubope,
     & REAL(R_adope,4),L_edope,L_idope,R_ibope,REAL(R_uxipe
     &,4),REAL(R_oxipe,4),L_odope)
      !}
C FDA60b1_vlv.fgi(  79, 493):���������� ������� ��������,20FDA63CP002XQ01
      R_itote = R8_otote
C FDA60_sens.fgi( 305, 258):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_asupe,4),REAL
     &(R_itote,4),R_imupe,
     & R_esupe,REAL(1e-3,4),REAL(R_apupe,4),
     & REAL(R_epupe,4),REAL(R_emupe,4),
     & REAL(R_amupe,4),I_urupe,REAL(R_opupe,4),L_upupe,
     & REAL(R_arupe,4),L_erupe,L_irupe,R_ipupe,REAL(R_umupe
     &,4),REAL(R_omupe,4),L_orupe)
      !}
C FDA60b1_vlv.fgi( 109, 508):���������� ������� ��������,20FDA64CP001XQ01
      R_atote = R8_etote
C FDA60_sens.fgi( 305, 253):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ivipe,4),REAL
     &(R_atote,4),R_uripe,
     & R_ovipe,REAL(1e-3,4),REAL(R_isipe,4),
     & REAL(R_osipe,4),REAL(R_oripe,4),
     & REAL(R_iripe,4),I_evipe,REAL(R_atipe,4),L_etipe,
     & REAL(R_itipe,4),L_otipe,L_utipe,R_usipe,REAL(R_esipe
     &,4),REAL(R_asipe,4),L_avipe)
      !}
C FDA60b1_vlv.fgi( 109, 493):���������� ������� ��������,20FDA64CP002XQ01
      R_osote = R8_usote
C FDA60_sens.fgi( 305, 248):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ilupe,4),REAL
     &(R_osote,4),R_udupe,
     & R_olupe,REAL(1e-3,4),REAL(R_ifupe,4),
     & REAL(R_ofupe,4),REAL(R_odupe,4),
     & REAL(R_idupe,4),I_elupe,REAL(R_akupe,4),L_ekupe,
     & REAL(R_ikupe,4),L_okupe,L_ukupe,R_ufupe,REAL(R_efupe
     &,4),REAL(R_afupe,4),L_alupe)
      !}
C FDA60b1_vlv.fgi( 139, 508):���������� ������� ��������,20FDA65CP001XQ01
      R_esote = R8_isote
C FDA60_sens.fgi( 305, 243):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_upipe,4),REAL
     &(R_esote,4),R_elipe,
     & R_aripe,REAL(1e-3,4),REAL(R_ulipe,4),
     & REAL(R_amipe,4),REAL(R_alipe,4),
     & REAL(R_ukipe,4),I_opipe,REAL(R_imipe,4),L_omipe,
     & REAL(R_umipe,4),L_apipe,L_epipe,R_emipe,REAL(R_olipe
     &,4),REAL(R_ilipe,4),L_ipipe)
      !}
C FDA60b1_vlv.fgi( 139, 493):���������� ������� ��������,20FDA65CP002XQ01
      R_urote = R8_asote
C FDA60_sens.fgi( 305, 238):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ubupe,4),REAL
     &(R_urote,4),R_evope,
     & R_adupe,REAL(1e-3,4),REAL(R_uvope,4),
     & REAL(R_axope,4),REAL(R_avope,4),
     & REAL(R_utope,4),I_obupe,REAL(R_ixope,4),L_oxope,
     & REAL(R_uxope,4),L_abupe,L_ebupe,R_exope,REAL(R_ovope
     &,4),REAL(R_ivope,4),L_ibupe)
      !}
C FDA60b1_vlv.fgi( 167, 508):���������� ������� ��������,20FDA66CP001XQ01
      R_irote = R8_orote
C FDA60_sens.fgi( 305, 233):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ekipe,4),REAL
     &(R_irote,4),R_obipe,
     & R_ikipe,REAL(1e-3,4),REAL(R_edipe,4),
     & REAL(R_idipe,4),REAL(R_ibipe,4),
     & REAL(R_ebipe,4),I_akipe,REAL(R_udipe,4),L_afipe,
     & REAL(R_efipe,4),L_ifipe,L_ofipe,R_odipe,REAL(R_adipe
     &,4),REAL(R_ubipe,4),L_ufipe)
      !}
C FDA60b1_vlv.fgi( 167, 493):���������� ������� ��������,20FDA66CP002XQ01
      R_erote = R8_ovite
C FDA60_sens.fgi( 305, 228):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ekape,4),REAL
     &(R_erote,4),R_obape,
     & R_ikape,REAL(1e-3,4),REAL(R_edape,4),
     & REAL(R_idape,4),REAL(R_ibape,4),
     & REAL(R_ebape,4),I_akape,REAL(R_udape,4),L_afape,
     & REAL(R_efape,4),L_ifape,L_ofape,R_odape,REAL(R_adape
     &,4),REAL(R_ubape,4),L_ufape)
      !}
C FDA60b1_vlv.fgi(  20, 461):���������� ������� ��������,20FDA61CP004XQ01
      R_upote = R8_arote
C FDA60_sens.fgi( 305, 223):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_oxume,4),REAL
     &(R_upote,4),R_atume,
     & R_uxume,REAL(1e-3,4),REAL(R_otume,4),
     & REAL(R_utume,4),REAL(R_usume,4),
     & REAL(R_osume,4),I_ixume,REAL(R_evume,4),L_ivume,
     & REAL(R_ovume,4),L_uvume,L_axume,R_avume,REAL(R_itume
     &,4),REAL(R_etume,4),L_exume)
      !}
C FDA60b1_vlv.fgi(  51, 461):���������� ������� ��������,20FDA62CP004XQ01
      R_ipote = R8_opote
C FDA60_sens.fgi( 305, 218):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_asume,4),REAL
     &(R_ipote,4),R_imume,
     & R_esume,REAL(1e-3,4),REAL(R_apume,4),
     & REAL(R_epume,4),REAL(R_emume,4),
     & REAL(R_amume,4),I_urume,REAL(R_opume,4),L_upume,
     & REAL(R_arume,4),L_erume,L_irume,R_ipume,REAL(R_umume
     &,4),REAL(R_omume,4),L_orume)
      !}
C FDA60b1_vlv.fgi(  80, 461):���������� ������� ��������,20FDA63CP004XQ01
      R_apote = R8_epote
C FDA60_sens.fgi( 305, 213):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ilume,4),REAL
     &(R_apote,4),R_udume,
     & R_olume,REAL(1e-3,4),REAL(R_ifume,4),
     & REAL(R_ofume,4),REAL(R_odume,4),
     & REAL(R_idume,4),I_elume,REAL(R_akume,4),L_ekume,
     & REAL(R_ikume,4),L_okume,L_ukume,R_ufume,REAL(R_efume
     &,4),REAL(R_afume,4),L_alume)
      !}
C FDA60b1_vlv.fgi( 111, 461):���������� ������� ��������,20FDA64CP004XQ01
      R_omote = R8_umote
C FDA60_sens.fgi( 305, 208):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ubume,4),REAL
     &(R_omote,4),R_evome,
     & R_adume,REAL(1e-3,4),REAL(R_uvome,4),
     & REAL(R_axome,4),REAL(R_avome,4),
     & REAL(R_utome,4),I_obume,REAL(R_ixome,4),L_oxome,
     & REAL(R_uxome,4),L_abume,L_ebume,R_exome,REAL(R_ovome
     &,4),REAL(R_ivome,4),L_ibume)
      !}
C FDA60b1_vlv.fgi( 140, 461):���������� ������� ��������,20FDA65CP004XQ01
      R_emote = R8_imote
C FDA60_sens.fgi( 305, 203):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_etome,4),REAL
     &(R_emote,4),R_opome,
     & R_itome,REAL(1e-3,4),REAL(R_erome,4),
     & REAL(R_irome,4),REAL(R_ipome,4),
     & REAL(R_epome,4),I_atome,REAL(R_urome,4),L_asome,
     & REAL(R_esome,4),L_isome,L_osome,R_orome,REAL(R_arome
     &,4),REAL(R_upome,4),L_usome)
      !}
C FDA60b1_vlv.fgi( 167, 461):���������� ������� ��������,20FDA66CP004XQ01
      R_obuse = R8_ubuse
C FDA60_sens.fgi( 494, 264):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_exox,R_afux,REAL(1,4
     &),
     & REAL(R_obuse,4),I_udux,REAL(R_uxox,4),
     & REAL(R_abux,4),REAL(R_axox,4),
     & REAL(R_uvox,4),REAL(R_obux,4),L_ubux,REAL(R_adux,4
     &),L_edux,
     & L_idux,R_ebux,REAL(R_oxox,4),REAL(R_ixox,4),L_odux
     &)
      !}
C FDA60b1_vlv.fgi( 158, 352):���������� ������� ��� 2,20FDA66CT009XQ01
      R_iduse = R8_oduse
C FDA60_sens.fgi( 494, 274):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_apux,R_usux,REAL(1,4
     &),
     & REAL(R_iduse,4),I_osux,REAL(R_opux,4),
     & REAL(R_upux,4),REAL(R_umux,4),
     & REAL(R_omux,4),REAL(R_irux,4),L_orux,REAL(R_urux,4
     &),L_asux,
     & L_esux,R_arux,REAL(R_ipux,4),REAL(R_epux,4),L_isux
     &)
      !}
C FDA60b1_vlv.fgi( 131, 352):���������� ������� ��� 2,20FDA65CT009XQ01
      R_efuse = R8_ifuse
C FDA60_sens.fgi( 494, 284):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ubabe,R_okabe,REAL(1
     &,4),
     & REAL(R_efuse,4),I_ikabe,REAL(R_idabe,4),
     & REAL(R_odabe,4),REAL(R_obabe,4),
     & REAL(R_ibabe,4),REAL(R_efabe,4),L_ifabe,REAL(R_ofabe
     &,4),L_ufabe,
     & L_akabe,R_udabe,REAL(R_edabe,4),REAL(R_adabe,4),L_ekabe
     &)
      !}
C FDA60b1_vlv.fgi( 104, 352):���������� ������� ��� 2,20FDA64CT009XQ01
      R_akuse = R8_ekuse
C FDA60_sens.fgi( 494, 294):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_orabe,R_ivabe,REAL(1
     &,4),
     & REAL(R_akuse,4),I_evabe,REAL(R_esabe,4),
     & REAL(R_isabe,4),REAL(R_irabe,4),
     & REAL(R_erabe,4),REAL(R_atabe,4),L_etabe,REAL(R_itabe
     &,4),L_otabe,
     & L_utabe,R_osabe,REAL(R_asabe,4),REAL(R_urabe,4),L_avabe
     &)
      !}
C FDA60b1_vlv.fgi(  76, 352):���������� ������� ��� 2,20FDA63CT009XQ01
      R_ukuse = R8_aluse
C FDA60_sens.fgi( 494, 304):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ifebe,R_emebe,REAL(1
     &,4),
     & REAL(R_ukuse,4),I_amebe,REAL(R_akebe,4),
     & REAL(R_ekebe,4),REAL(R_efebe,4),
     & REAL(R_afebe,4),REAL(R_ukebe,4),L_alebe,REAL(R_elebe
     &,4),L_ilebe,
     & L_olebe,R_ikebe,REAL(R_ufebe,4),REAL(R_ofebe,4),L_ulebe
     &)
      !}
C FDA60b1_vlv.fgi(  49, 352):���������� ������� ��� 2,20FDA62CT009XQ01
      R_oluse = R8_uluse
C FDA60_sens.fgi( 494, 314):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_etebe,R_abibe,REAL(1
     &,4),
     & REAL(R_oluse,4),I_uxebe,REAL(R_utebe,4),
     & REAL(R_avebe,4),REAL(R_atebe,4),
     & REAL(R_usebe,4),REAL(R_ovebe,4),L_uvebe,REAL(R_axebe
     &,4),L_exebe,
     & L_ixebe,R_evebe,REAL(R_otebe,4),REAL(R_itebe,4),L_oxebe
     &)
      !}
C FDA60b1_vlv.fgi(  22, 352):���������� ������� ��� 2,20FDA61CT009XQ01
      R_amuse = R8_emuse
C FDA60_sens.fgi( 494,  67):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_urum,R_ovum,REAL(1,4
     &),
     & REAL(R_amuse,4),I_ivum,REAL(R_isum,4),
     & REAL(R_osum,4),REAL(R_orum,4),
     & REAL(R_irum,4),REAL(R_etum,4),L_itum,REAL(R_otum,4
     &),L_utum,
     & L_avum,R_usum,REAL(R_esum,4),REAL(R_asum,4),L_evum
     &)
      !}
C FDA60b1_vlv.fgi( 159,  76):���������� ������� ��� 2,20FDA66CT022XQ01
      R_imuse = R8_omuse
C FDA60_sens.fgi( 494,  70):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_atup,R_uxup,REAL(1,4
     &),
     & REAL(R_imuse,4),I_oxup,REAL(R_otup,4),
     & REAL(R_utup,4),REAL(R_usup,4),
     & REAL(R_osup,4),REAL(R_ivup,4),L_ovup,REAL(R_uvup,4
     &),L_axup,
     & L_exup,R_avup,REAL(R_itup,4),REAL(R_etup,4),L_ixup
     &)
      !}
C FDA60b1_vlv.fgi( 158, 121):���������� ������� ��� 2,20FDA66CT021XQ01
      R_umuse = R8_apuse
C FDA60_sens.fgi( 494,  73):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ukar,R_opar,REAL(1,4
     &),
     & REAL(R_umuse,4),I_ipar,REAL(R_ilar,4),
     & REAL(R_olar,4),REAL(R_okar,4),
     & REAL(R_ikar,4),REAL(R_emar,4),L_imar,REAL(R_omar,4
     &),L_umar,
     & L_apar,R_ular,REAL(R_elar,4),REAL(R_alar,4),L_epar
     &)
      !}
C FDA60b1_vlv.fgi( 158, 150):���������� ������� ��� 2,20FDA66CT020XQ01
      R_epuse = R8_ipuse
C FDA60_sens.fgi( 494,  76):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ibar,R_ekar,REAL(1,4
     &),
     & REAL(R_epuse,4),I_akar,REAL(R_adar,4),
     & REAL(R_edar,4),REAL(R_ebar,4),
     & REAL(R_abar,4),REAL(R_udar,4),L_afar,REAL(R_efar,4
     &),L_ifar,
     & L_ofar,R_idar,REAL(R_ubar,4),REAL(R_obar,4),L_ufar
     &)
      !}
C FDA60b1_vlv.fgi( 158, 135):���������� ������� ��� 2,20FDA66CT019XQ01
      R_opuse = R8_upuse
C FDA60_sens.fgi( 494,  79):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_erar,R_avar,REAL(1,4
     &),
     & REAL(R_opuse,4),I_utar,REAL(R_urar,4),
     & REAL(R_asar,4),REAL(R_arar,4),
     & REAL(R_upar,4),REAL(R_osar,4),L_usar,REAL(R_atar,4
     &),L_etar,
     & L_itar,R_esar,REAL(R_orar,4),REAL(R_irar,4),L_otar
     &)
      !}
C FDA60b1_vlv.fgi( 158, 165):���������� ������� ��� 2,20FDA66CT018XQ01
      R_aruse = R8_eruse
C FDA60_sens.fgi( 494,  82):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovar,R_ider,REAL(1,4
     &),
     & REAL(R_aruse,4),I_eder,REAL(R_exar,4),
     & REAL(R_ixar,4),REAL(R_ivar,4),
     & REAL(R_evar,4),REAL(R_aber,4),L_eber,REAL(R_iber,4
     &),L_ober,
     & L_uber,R_oxar,REAL(R_axar,4),REAL(R_uvar,4),L_ader
     &)
      !}
C FDA60b1_vlv.fgi( 158, 180):���������� ������� ��� 2,20FDA66CT017XQ01
      R_iruse = R8_oruse
C FDA60_sens.fgi( 494,  85):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imer,R_eser,REAL(1,4
     &),
     & REAL(R_iruse,4),I_aser,REAL(R_aper,4),
     & REAL(R_eper,4),REAL(R_emer,4),
     & REAL(R_amer,4),REAL(R_uper,4),L_arer,REAL(R_erer,4
     &),L_irer,
     & L_orer,R_iper,REAL(R_umer,4),REAL(R_omer,4),L_urer
     &)
      !}
C FDA60b1_vlv.fgi( 158, 210):���������� ������� ��� 2,20FDA66CT016XQ01
      R_uruse = R8_asuse
C FDA60_sens.fgi( 494,  88):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_afer,R_uler,REAL(1,4
     &),
     & REAL(R_uruse,4),I_oler,REAL(R_ofer,4),
     & REAL(R_ufer,4),REAL(R_uder,4),
     & REAL(R_oder,4),REAL(R_iker,4),L_oker,REAL(R_uker,4
     &),L_aler,
     & L_eler,R_aker,REAL(R_ifer,4),REAL(R_efer,4),L_iler
     &)
      !}
C FDA60b1_vlv.fgi( 158, 195):���������� ������� ��� 2,20FDA66CT015XQ01
      R_esuse = R8_isuse
C FDA60_sens.fgi( 494,  92):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_exum,R_afap,REAL(1,4
     &),
     & REAL(R_esuse,4),I_udap,REAL(R_uxum,4),
     & REAL(R_abap,4),REAL(R_axum,4),
     & REAL(R_uvum,4),REAL(R_obap,4),L_ubap,REAL(R_adap,4
     &),L_edap,
     & L_idap,R_ebap,REAL(R_oxum,4),REAL(R_ixum,4),L_odap
     &)
      !}
C FDA60b1_vlv.fgi( 131,  76):���������� ������� ��� 2,20FDA65CT022XQ01
      R_osuse = R8_ususe
C FDA60_sens.fgi( 494,  95):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_user,R_oxer,REAL(1,4
     &),
     & REAL(R_osuse,4),I_ixer,REAL(R_iter,4),
     & REAL(R_oter,4),REAL(R_oser,4),
     & REAL(R_iser,4),REAL(R_ever,4),L_iver,REAL(R_over,4
     &),L_uver,
     & L_axer,R_uter,REAL(R_eter,4),REAL(R_ater,4),L_exer
     &)
      !}
C FDA60b1_vlv.fgi( 130, 121):���������� ������� ��� 2,20FDA65CT021XQ01
      R_atuse = R8_etuse
C FDA60_sens.fgi( 494,  98):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_okir,R_ipir,REAL(1,4
     &),
     & REAL(R_atuse,4),I_epir,REAL(R_elir,4),
     & REAL(R_ilir,4),REAL(R_ikir,4),
     & REAL(R_ekir,4),REAL(R_amir,4),L_emir,REAL(R_imir,4
     &),L_omir,
     & L_umir,R_olir,REAL(R_alir,4),REAL(R_ukir,4),L_apir
     &)
      !}
C FDA60b1_vlv.fgi( 130, 150):���������� ������� ��� 2,20FDA65CT020XQ01
      R_ituse = R8_otuse
C FDA60_sens.fgi( 494, 101):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ebir,R_akir,REAL(1,4
     &),
     & REAL(R_ituse,4),I_ufir,REAL(R_ubir,4),
     & REAL(R_adir,4),REAL(R_abir,4),
     & REAL(R_uxer,4),REAL(R_odir,4),L_udir,REAL(R_afir,4
     &),L_efir,
     & L_ifir,R_edir,REAL(R_obir,4),REAL(R_ibir,4),L_ofir
     &)
      !}
C FDA60b1_vlv.fgi( 130, 135):���������� ������� ��� 2,20FDA65CT019XQ01
      R_utuse = R8_avuse
C FDA60_sens.fgi( 494, 104):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_arir,R_utir,REAL(1,4
     &),
     & REAL(R_utuse,4),I_otir,REAL(R_orir,4),
     & REAL(R_urir,4),REAL(R_upir,4),
     & REAL(R_opir,4),REAL(R_isir,4),L_osir,REAL(R_usir,4
     &),L_atir,
     & L_etir,R_asir,REAL(R_irir,4),REAL(R_erir,4),L_itir
     &)
      !}
C FDA60b1_vlv.fgi( 130, 165):���������� ������� ��� 2,20FDA65CT018XQ01
      R_evuse = R8_ivuse
C FDA60_sens.fgi( 494, 107):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ivir,R_edor,REAL(1,4
     &),
     & REAL(R_evuse,4),I_ador,REAL(R_axir,4),
     & REAL(R_exir,4),REAL(R_evir,4),
     & REAL(R_avir,4),REAL(R_uxir,4),L_abor,REAL(R_ebor,4
     &),L_ibor,
     & L_obor,R_ixir,REAL(R_uvir,4),REAL(R_ovir,4),L_ubor
     &)
      !}
C FDA60b1_vlv.fgi( 130, 180):���������� ������� ��� 2,20FDA65CT017XQ01
      R_ovuse = R8_uvuse
C FDA60_sens.fgi( 494, 110):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_emor,R_asor,REAL(1,4
     &),
     & REAL(R_ovuse,4),I_uror,REAL(R_umor,4),
     & REAL(R_apor,4),REAL(R_amor,4),
     & REAL(R_ulor,4),REAL(R_opor,4),L_upor,REAL(R_aror,4
     &),L_eror,
     & L_iror,R_epor,REAL(R_omor,4),REAL(R_imor,4),L_oror
     &)
      !}
C FDA60b1_vlv.fgi( 130, 210):���������� ������� ��� 2,20FDA65CT016XQ01
      R_axuse = R8_exuse
C FDA60_sens.fgi( 494, 113):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_udor,R_olor,REAL(1,4
     &),
     & REAL(R_axuse,4),I_ilor,REAL(R_ifor,4),
     & REAL(R_ofor,4),REAL(R_odor,4),
     & REAL(R_idor,4),REAL(R_ekor,4),L_ikor,REAL(R_okor,4
     &),L_ukor,
     & L_alor,R_ufor,REAL(R_efor,4),REAL(R_afor,4),L_elor
     &)
      !}
C FDA60b1_vlv.fgi( 130, 195):���������� ������� ��� 2,20FDA65CT015XQ01
      R_ixuse = R8_oxuse
C FDA60_sens.fgi( 494, 119):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ofap,R_imap,REAL(1,4
     &),
     & REAL(R_ixuse,4),I_emap,REAL(R_ekap,4),
     & REAL(R_ikap,4),REAL(R_ifap,4),
     & REAL(R_efap,4),REAL(R_alap,4),L_elap,REAL(R_ilap,4
     &),L_olap,
     & L_ulap,R_okap,REAL(R_akap,4),REAL(R_ufap,4),L_amap
     &)
      !}
C FDA60b1_vlv.fgi( 103,  76):���������� ������� ��� 2,20FDA64CT022XQ01
      R_uxuse = R8_abate
C FDA60_sens.fgi( 494, 122):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_osor,R_ixor,REAL(1,4
     &),
     & REAL(R_uxuse,4),I_exor,REAL(R_etor,4),
     & REAL(R_itor,4),REAL(R_isor,4),
     & REAL(R_esor,4),REAL(R_avor,4),L_evor,REAL(R_ivor,4
     &),L_ovor,
     & L_uvor,R_otor,REAL(R_ator,4),REAL(R_usor,4),L_axor
     &)
      !}
C FDA60b1_vlv.fgi( 102, 121):���������� ������� ��� 2,20FDA64CT021XQ01
      R_ebate = R8_ibate
C FDA60_sens.fgi( 494, 125):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ikur,R_epur,REAL(1,4
     &),
     & REAL(R_ebate,4),I_apur,REAL(R_alur,4),
     & REAL(R_elur,4),REAL(R_ekur,4),
     & REAL(R_akur,4),REAL(R_ulur,4),L_amur,REAL(R_emur,4
     &),L_imur,
     & L_omur,R_ilur,REAL(R_ukur,4),REAL(R_okur,4),L_umur
     &)
      !}
C FDA60b1_vlv.fgi( 102, 150):���������� ������� ��� 2,20FDA64CT020XQ01
      R_obate = R8_ubate
C FDA60_sens.fgi( 494, 128):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_abur,R_ufur,REAL(1,4
     &),
     & REAL(R_obate,4),I_ofur,REAL(R_obur,4),
     & REAL(R_ubur,4),REAL(R_uxor,4),
     & REAL(R_oxor,4),REAL(R_idur,4),L_odur,REAL(R_udur,4
     &),L_afur,
     & L_efur,R_adur,REAL(R_ibur,4),REAL(R_ebur,4),L_ifur
     &)
      !}
C FDA60b1_vlv.fgi( 102, 135):���������� ������� ��� 2,20FDA64CT019XQ01
      R_adate = R8_edate
C FDA60_sens.fgi( 494, 131):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_upur,R_otur,REAL(1,4
     &),
     & REAL(R_adate,4),I_itur,REAL(R_irur,4),
     & REAL(R_orur,4),REAL(R_opur,4),
     & REAL(R_ipur,4),REAL(R_esur,4),L_isur,REAL(R_osur,4
     &),L_usur,
     & L_atur,R_urur,REAL(R_erur,4),REAL(R_arur,4),L_etur
     &)
      !}
C FDA60b1_vlv.fgi( 102, 165):���������� ������� ��� 2,20FDA64CT018XQ01
      R_idate = R8_odate
C FDA60_sens.fgi( 494, 134):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_evur,R_adas,REAL(1,4
     &),
     & REAL(R_idate,4),I_ubas,REAL(R_uvur,4),
     & REAL(R_axur,4),REAL(R_avur,4),
     & REAL(R_utur,4),REAL(R_oxur,4),L_uxur,REAL(R_abas,4
     &),L_ebas,
     & L_ibas,R_exur,REAL(R_ovur,4),REAL(R_ivur,4),L_obas
     &)
      !}
C FDA60b1_vlv.fgi( 102, 180):���������� ������� ��� 2,20FDA64CT017XQ01
      R_udate = R8_afate
C FDA60_sens.fgi( 494, 137):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_amas,R_uras,REAL(1,4
     &),
     & REAL(R_udate,4),I_oras,REAL(R_omas,4),
     & REAL(R_umas,4),REAL(R_ulas,4),
     & REAL(R_olas,4),REAL(R_ipas,4),L_opas,REAL(R_upas,4
     &),L_aras,
     & L_eras,R_apas,REAL(R_imas,4),REAL(R_emas,4),L_iras
     &)
      !}
C FDA60b1_vlv.fgi( 102, 210):���������� ������� ��� 2,20FDA64CT016XQ01
      R_efate = R8_ifate
C FDA60_sens.fgi( 494, 140):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_odas,R_ilas,REAL(1,4
     &),
     & REAL(R_efate,4),I_elas,REAL(R_efas,4),
     & REAL(R_ifas,4),REAL(R_idas,4),
     & REAL(R_edas,4),REAL(R_akas,4),L_ekas,REAL(R_ikas,4
     &),L_okas,
     & L_ukas,R_ofas,REAL(R_afas,4),REAL(R_udas,4),L_alas
     &)
      !}
C FDA60b1_vlv.fgi( 102, 195):���������� ������� ��� 2,20FDA64CT015XQ01
      R_ofate = R8_ufate
C FDA60_sens.fgi( 494, 146):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_apap,R_usap,REAL(1,4
     &),
     & REAL(R_ofate,4),I_osap,REAL(R_opap,4),
     & REAL(R_upap,4),REAL(R_umap,4),
     & REAL(R_omap,4),REAL(R_irap,4),L_orap,REAL(R_urap,4
     &),L_asap,
     & L_esap,R_arap,REAL(R_ipap,4),REAL(R_epap,4),L_isap
     &)
      !}
C FDA60b1_vlv.fgi(  75,  76):���������� ������� ��� 2,20FDA63CT022XQ01
      R_akate = R8_ekate
C FDA60_sens.fgi( 494, 149):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_isas,R_exas,REAL(1,4
     &),
     & REAL(R_akate,4),I_axas,REAL(R_atas,4),
     & REAL(R_etas,4),REAL(R_esas,4),
     & REAL(R_asas,4),REAL(R_utas,4),L_avas,REAL(R_evas,4
     &),L_ivas,
     & L_ovas,R_itas,REAL(R_usas,4),REAL(R_osas,4),L_uvas
     &)
      !}
C FDA60b1_vlv.fgi(  76, 121):���������� ������� ��� 2,20FDA63CT021XQ01
      R_ikate = R8_okate
C FDA60_sens.fgi( 494, 152):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ekes,R_apes,REAL(1,4
     &),
     & REAL(R_ikate,4),I_umes,REAL(R_ukes,4),
     & REAL(R_ales,4),REAL(R_akes,4),
     & REAL(R_ufes,4),REAL(R_oles,4),L_ules,REAL(R_ames,4
     &),L_emes,
     & L_imes,R_eles,REAL(R_okes,4),REAL(R_ikes,4),L_omes
     &)
      !}
C FDA60b1_vlv.fgi(  76, 150):���������� ������� ��� 2,20FDA63CT020XQ01
      R_ukate = R8_alate
C FDA60_sens.fgi( 494, 155):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uxas,R_ofes,REAL(1,4
     &),
     & REAL(R_ukate,4),I_ifes,REAL(R_ibes,4),
     & REAL(R_obes,4),REAL(R_oxas,4),
     & REAL(R_ixas,4),REAL(R_edes,4),L_ides,REAL(R_odes,4
     &),L_udes,
     & L_afes,R_ubes,REAL(R_ebes,4),REAL(R_abes,4),L_efes
     &)
      !}
C FDA60b1_vlv.fgi(  76, 135):���������� ������� ��� 2,20FDA63CT019XQ01
      R_elate = R8_ilate
C FDA60_sens.fgi( 494, 158):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_opes,R_ites,REAL(1,4
     &),
     & REAL(R_elate,4),I_etes,REAL(R_eres,4),
     & REAL(R_ires,4),REAL(R_ipes,4),
     & REAL(R_epes,4),REAL(R_ases,4),L_eses,REAL(R_ises,4
     &),L_oses,
     & L_uses,R_ores,REAL(R_ares,4),REAL(R_upes,4),L_ates
     &)
      !}
C FDA60b1_vlv.fgi(  76, 165):���������� ������� ��� 2,20FDA63CT018XQ01
      R_olate = R8_ulate
C FDA60_sens.fgi( 494, 161):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_aves,R_ubis,REAL(1,4
     &),
     & REAL(R_olate,4),I_obis,REAL(R_oves,4),
     & REAL(R_uves,4),REAL(R_utes,4),
     & REAL(R_otes,4),REAL(R_ixes,4),L_oxes,REAL(R_uxes,4
     &),L_abis,
     & L_ebis,R_axes,REAL(R_ives,4),REAL(R_eves,4),L_ibis
     &)
      !}
C FDA60b1_vlv.fgi(  76, 180):���������� ������� ��� 2,20FDA63CT017XQ01
      R_amate = R8_emate
C FDA60_sens.fgi( 494, 164):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ulis,R_oris,REAL(1,4
     &),
     & REAL(R_amate,4),I_iris,REAL(R_imis,4),
     & REAL(R_omis,4),REAL(R_olis,4),
     & REAL(R_ilis,4),REAL(R_epis,4),L_ipis,REAL(R_opis,4
     &),L_upis,
     & L_aris,R_umis,REAL(R_emis,4),REAL(R_amis,4),L_eris
     &)
      !}
C FDA60b1_vlv.fgi(  76, 210):���������� ������� ��� 2,20FDA63CT016XQ01
      R_imate = R8_omate
C FDA60_sens.fgi( 494, 167):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_idis,R_elis,REAL(1,4
     &),
     & REAL(R_imate,4),I_alis,REAL(R_afis,4),
     & REAL(R_efis,4),REAL(R_edis,4),
     & REAL(R_adis,4),REAL(R_ufis,4),L_akis,REAL(R_ekis,4
     &),L_ikis,
     & L_okis,R_ifis,REAL(R_udis,4),REAL(R_odis,4),L_ukis
     &)
      !}
C FDA60b1_vlv.fgi(  76, 195):���������� ������� ��� 2,20FDA63CT015XQ01
      R_umate = R8_apate
C FDA60_sens.fgi( 494, 172):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itap,R_ebep,REAL(1,4
     &),
     & REAL(R_umate,4),I_abep,REAL(R_avap,4),
     & REAL(R_evap,4),REAL(R_etap,4),
     & REAL(R_atap,4),REAL(R_uvap,4),L_axap,REAL(R_exap,4
     &),L_ixap,
     & L_oxap,R_ivap,REAL(R_utap,4),REAL(R_otap,4),L_uxap
     &)
      !}
C FDA60b1_vlv.fgi(  47,  76):���������� ������� ��� 2,20FDA62CT022XQ01
      R_epate = R8_ipate
C FDA60_sens.fgi( 494, 175):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_esis,R_axis,REAL(1,4
     &),
     & REAL(R_epate,4),I_uvis,REAL(R_usis,4),
     & REAL(R_atis,4),REAL(R_asis,4),
     & REAL(R_uris,4),REAL(R_otis,4),L_utis,REAL(R_avis,4
     &),L_evis,
     & L_ivis,R_etis,REAL(R_osis,4),REAL(R_isis,4),L_ovis
     &)
      !}
C FDA60b1_vlv.fgi(  48, 121):���������� ������� ��� 2,20FDA62CT021XQ01
      R_opate = R8_upate
C FDA60_sens.fgi( 494, 178):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_akos,R_umos,REAL(1,4
     &),
     & REAL(R_opate,4),I_omos,REAL(R_okos,4),
     & REAL(R_ukos,4),REAL(R_ufos,4),
     & REAL(R_ofos,4),REAL(R_ilos,4),L_olos,REAL(R_ulos,4
     &),L_amos,
     & L_emos,R_alos,REAL(R_ikos,4),REAL(R_ekos,4),L_imos
     &)
      !}
C FDA60b1_vlv.fgi(  48, 150):���������� ������� ��� 2,20FDA62CT020XQ01
      R_arate = R8_erate
C FDA60_sens.fgi( 494, 181):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_oxis,R_ifos,REAL(1,4
     &),
     & REAL(R_arate,4),I_efos,REAL(R_ebos,4),
     & REAL(R_ibos,4),REAL(R_ixis,4),
     & REAL(R_exis,4),REAL(R_ados,4),L_edos,REAL(R_idos,4
     &),L_odos,
     & L_udos,R_obos,REAL(R_abos,4),REAL(R_uxis,4),L_afos
     &)
      !}
C FDA60b1_vlv.fgi(  48, 135):���������� ������� ��� 2,20FDA62CT019XQ01
      R_irate = R8_orate
C FDA60_sens.fgi( 494, 184):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ipos,R_etos,REAL(1,4
     &),
     & REAL(R_irate,4),I_atos,REAL(R_aros,4),
     & REAL(R_eros,4),REAL(R_epos,4),
     & REAL(R_apos,4),REAL(R_uros,4),L_asos,REAL(R_esos,4
     &),L_isos,
     & L_osos,R_iros,REAL(R_upos,4),REAL(R_opos,4),L_usos
     &)
      !}
C FDA60b1_vlv.fgi(  48, 165):���������� ������� ��� 2,20FDA62CT018XQ01
      R_urate = R8_asate
C FDA60_sens.fgi( 494, 187):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_utos,R_obus,REAL(1,4
     &),
     & REAL(R_urate,4),I_ibus,REAL(R_ivos,4),
     & REAL(R_ovos,4),REAL(R_otos,4),
     & REAL(R_itos,4),REAL(R_exos,4),L_ixos,REAL(R_oxos,4
     &),L_uxos,
     & L_abus,R_uvos,REAL(R_evos,4),REAL(R_avos,4),L_ebus
     &)
      !}
C FDA60b1_vlv.fgi(  48, 180):���������� ������� ��� 2,20FDA62CT017XQ01
      R_esate = R8_isate
C FDA60_sens.fgi( 494, 190):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_olus,R_irus,REAL(1,4
     &),
     & REAL(R_esate,4),I_erus,REAL(R_emus,4),
     & REAL(R_imus,4),REAL(R_ilus,4),
     & REAL(R_elus,4),REAL(R_apus,4),L_epus,REAL(R_ipus,4
     &),L_opus,
     & L_upus,R_omus,REAL(R_amus,4),REAL(R_ulus,4),L_arus
     &)
      !}
C FDA60b1_vlv.fgi(  48, 210):���������� ������� ��� 2,20FDA62CT016XQ01
      R_osate = R8_usate
C FDA60_sens.fgi( 494, 193):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_edus,R_alus,REAL(1,4
     &),
     & REAL(R_osate,4),I_ukus,REAL(R_udus,4),
     & REAL(R_afus,4),REAL(R_adus,4),
     & REAL(R_ubus,4),REAL(R_ofus,4),L_ufus,REAL(R_akus,4
     &),L_ekus,
     & L_ikus,R_efus,REAL(R_odus,4),REAL(R_idus,4),L_okus
     &)
      !}
C FDA60b1_vlv.fgi(  48, 195):���������� ������� ��� 2,20FDA62CT015XQ01
      R_atate = R8_etate
C FDA60_sens.fgi( 494, 199):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ubep,R_okep,REAL(1,4
     &),
     & REAL(R_atate,4),I_ikep,REAL(R_idep,4),
     & REAL(R_odep,4),REAL(R_obep,4),
     & REAL(R_ibep,4),REAL(R_efep,4),L_ifep,REAL(R_ofep,4
     &),L_ufep,
     & L_akep,R_udep,REAL(R_edep,4),REAL(R_adep,4),L_ekep
     &)
      !}
C FDA60b1_vlv.fgi(  19,  76):���������� ������� ��� 2,20FDA61CT022XQ01
      R_itate = R8_otate
C FDA60_sens.fgi( 494, 202):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_asus,R_uvus,REAL(1,4
     &),
     & REAL(R_itate,4),I_ovus,REAL(R_osus,4),
     & REAL(R_usus,4),REAL(R_urus,4),
     & REAL(R_orus,4),REAL(R_itus,4),L_otus,REAL(R_utus,4
     &),L_avus,
     & L_evus,R_atus,REAL(R_isus,4),REAL(R_esus,4),L_ivus
     &)
      !}
C FDA60b1_vlv.fgi(  20, 121):���������� ������� ��� 2,20FDA61CT021XQ01
      R_utate = R8_avate
C FDA60_sens.fgi( 494, 205):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ufat,R_omat,REAL(1,4
     &),
     & REAL(R_utate,4),I_imat,REAL(R_ikat,4),
     & REAL(R_okat,4),REAL(R_ofat,4),
     & REAL(R_ifat,4),REAL(R_elat,4),L_ilat,REAL(R_olat,4
     &),L_ulat,
     & L_amat,R_ukat,REAL(R_ekat,4),REAL(R_akat,4),L_emat
     &)
      !}
C FDA60b1_vlv.fgi(  20, 150):���������� ������� ��� 2,20FDA61CT020XQ01
      R_evate = R8_ivate
C FDA60_sens.fgi( 494, 208):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ixus,R_efat,REAL(1,4
     &),
     & REAL(R_evate,4),I_afat,REAL(R_abat,4),
     & REAL(R_ebat,4),REAL(R_exus,4),
     & REAL(R_axus,4),REAL(R_ubat,4),L_adat,REAL(R_edat,4
     &),L_idat,
     & L_odat,R_ibat,REAL(R_uxus,4),REAL(R_oxus,4),L_udat
     &)
      !}
C FDA60b1_vlv.fgi(  20, 135):���������� ������� ��� 2,20FDA61CT019XQ01
      R_ovate = R8_uvate
C FDA60_sens.fgi( 494, 211):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_epat,R_atat,REAL(1,4
     &),
     & REAL(R_ovate,4),I_usat,REAL(R_upat,4),
     & REAL(R_arat,4),REAL(R_apat,4),
     & REAL(R_umat,4),REAL(R_orat,4),L_urat,REAL(R_asat,4
     &),L_esat,
     & L_isat,R_erat,REAL(R_opat,4),REAL(R_ipat,4),L_osat
     &)
      !}
C FDA60b1_vlv.fgi(  20, 165):���������� ������� ��� 2,20FDA61CT018XQ01
      R_axate = R8_exate
C FDA60_sens.fgi( 494, 214):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_otat,R_ibet,REAL(1,4
     &),
     & REAL(R_axate,4),I_ebet,REAL(R_evat,4),
     & REAL(R_ivat,4),REAL(R_itat,4),
     & REAL(R_etat,4),REAL(R_axat,4),L_exat,REAL(R_ixat,4
     &),L_oxat,
     & L_uxat,R_ovat,REAL(R_avat,4),REAL(R_utat,4),L_abet
     &)
      !}
C FDA60b1_vlv.fgi(  20, 180):���������� ������� ��� 2,20FDA61CT017XQ01
      R_ixate = R8_oxate
C FDA60_sens.fgi( 494, 217):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ilet,R_eret,REAL(1,4
     &),
     & REAL(R_ixate,4),I_aret,REAL(R_amet,4),
     & REAL(R_emet,4),REAL(R_elet,4),
     & REAL(R_alet,4),REAL(R_umet,4),L_apet,REAL(R_epet,4
     &),L_ipet,
     & L_opet,R_imet,REAL(R_ulet,4),REAL(R_olet,4),L_upet
     &)
      !}
C FDA60b1_vlv.fgi(  20, 210):���������� ������� ��� 2,20FDA61CT016XQ01
      R_uxate = R8_abete
C FDA60_sens.fgi( 494, 220):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_adet,R_uket,REAL(1,4
     &),
     & REAL(R_uxate,4),I_oket,REAL(R_odet,4),
     & REAL(R_udet,4),REAL(R_ubet,4),
     & REAL(R_obet,4),REAL(R_ifet,4),L_ofet,REAL(R_ufet,4
     &),L_aket,
     & L_eket,R_afet,REAL(R_idet,4),REAL(R_edet,4),L_iket
     &)
      !}
C FDA60b1_vlv.fgi(  20, 195):���������� ������� ��� 2,20FDA61CT015XQ01
      R_ebete = R8_ibete
C FDA60_sens.fgi( 494,  25):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_akuk,R_umuk,REAL(1,4
     &),
     & REAL(R_ebete,4),I_omuk,REAL(R_okuk,4),
     & REAL(R_ukuk,4),REAL(R_ufuk,4),
     & REAL(R_ofuk,4),REAL(R_iluk,4),L_oluk,REAL(R_uluk,4
     &),L_amuk,
     & L_emuk,R_aluk,REAL(R_ikuk,4),REAL(R_ekuk,4),L_imuk
     &)
      !}
C FDA60b1_vlv.fgi( 160,  15):���������� ������� ��� 2,20FDA66CT023XQ01
      R_obete = R8_ubete
C FDA60_sens.fgi( 494,  30):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_upul,4),REAL
     &(R_obete,4),R_elul,
     & R_arul,REAL(1,4),REAL(R_ulul,4),
     & REAL(R_amul,4),REAL(R_alul,4),
     & REAL(R_ukul,4),I_opul,REAL(R_imul,4),L_omul,
     & REAL(R_umul,4),L_apul,L_epul,R_emul,REAL(R_olul,4)
     &,REAL(R_ilul,4),L_ipul)
      !}
C FDA60b1_vlv.fgi( 159,  31):���������� ������� ��������,20FDA66CP018XQ01
      R_adete = R8_edete
C FDA60_sens.fgi( 494,  33):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ipuk,R_etuk,REAL(1,4
     &),
     & REAL(R_adete,4),I_atuk,REAL(R_aruk,4),
     & REAL(R_eruk,4),REAL(R_epuk,4),
     & REAL(R_apuk,4),REAL(R_uruk,4),L_asuk,REAL(R_esuk,4
     &),L_isuk,
     & L_osuk,R_iruk,REAL(R_upuk,4),REAL(R_opuk,4),L_usuk
     &)
      !}
C FDA60b1_vlv.fgi( 132,  15):���������� ������� ��� 2,20FDA65CT023XQ01
      R_idete = R8_odete
C FDA60_sens.fgi( 494,  38):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_afam,4),REAL
     &(R_idete,4),R_ixul,
     & R_efam,REAL(1,4),REAL(R_abam,4),
     & REAL(R_ebam,4),REAL(R_exul,4),
     & REAL(R_axul,4),I_udam,REAL(R_obam,4),L_ubam,
     & REAL(R_adam,4),L_edam,L_idam,R_ibam,REAL(R_uxul,4)
     &,REAL(R_oxul,4),L_odam)
      !}
C FDA60b1_vlv.fgi( 131,  31):���������� ������� ��������,20FDA65CP018XQ01
      R_udete = R8_afete
C FDA60_sens.fgi( 494,  41):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_utuk,R_obal,REAL(1,4
     &),
     & REAL(R_udete,4),I_ibal,REAL(R_ivuk,4),
     & REAL(R_ovuk,4),REAL(R_otuk,4),
     & REAL(R_ituk,4),REAL(R_exuk,4),L_ixuk,REAL(R_oxuk,4
     &),L_uxuk,
     & L_abal,R_uvuk,REAL(R_evuk,4),REAL(R_avuk,4),L_ebal
     &)
      !}
C FDA60b1_vlv.fgi( 104,  15):���������� ������� ��� 2,20FDA64CT023XQ01
      R_efete = R8_ifete
C FDA60_sens.fgi( 494,  46):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_etam,4),REAL
     &(R_efete,4),R_opam,
     & R_itam,REAL(1,4),REAL(R_eram,4),
     & REAL(R_iram,4),REAL(R_ipam,4),
     & REAL(R_epam,4),I_atam,REAL(R_uram,4),L_asam,
     & REAL(R_esam,4),L_isam,L_osam,R_oram,REAL(R_aram,4)
     &,REAL(R_upam,4),L_usam)
      !}
C FDA60b1_vlv.fgi( 103,  31):���������� ������� ��������,20FDA64CP018XQ01
      R_ofete = R8_ufete
C FDA60_sens.fgi( 494,  50):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_edal,R_alal,REAL(1,4
     &),
     & REAL(R_ofete,4),I_ukal,REAL(R_udal,4),
     & REAL(R_afal,4),REAL(R_adal,4),
     & REAL(R_ubal,4),REAL(R_ofal,4),L_ufal,REAL(R_akal,4
     &),L_ekal,
     & L_ikal,R_efal,REAL(R_odal,4),REAL(R_idal,4),L_okal
     &)
      !}
C FDA60b1_vlv.fgi(  76,  15):���������� ������� ��� 2,20FDA63CT023XQ01
      R_akete = R8_ekete
C FDA60_sens.fgi( 494,  55):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ilem,4),REAL
     &(R_akete,4),R_udem,
     & R_olem,REAL(1,4),REAL(R_ifem,4),
     & REAL(R_ofem,4),REAL(R_odem,4),
     & REAL(R_idem,4),I_elem,REAL(R_akem,4),L_ekem,
     & REAL(R_ikem,4),L_okem,L_ukem,R_ufem,REAL(R_efem,4)
     &,REAL(R_afem,4),L_alem)
      !}
C FDA60b1_vlv.fgi(  75,  31):���������� ������� ��������,20FDA63CP018XQ01
      R_ikete = R8_okete
C FDA60_sens.fgi( 494,  58):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_olal,R_iral,REAL(1,4
     &),
     & REAL(R_ikete,4),I_eral,REAL(R_emal,4),
     & REAL(R_imal,4),REAL(R_ilal,4),
     & REAL(R_elal,4),REAL(R_apal,4),L_epal,REAL(R_ipal,4
     &),L_opal,
     & L_upal,R_omal,REAL(R_amal,4),REAL(R_ulal,4),L_aral
     &)
      !}
C FDA60b1_vlv.fgi(  48,  15):���������� ������� ��� 2,20FDA62CT023XQ01
      R_ukete = R8_alete
C FDA60_sens.fgi( 494,  63):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_oxem,4),REAL
     &(R_ukete,4),R_atem,
     & R_uxem,REAL(1,4),REAL(R_otem,4),
     & REAL(R_utem,4),REAL(R_usem,4),
     & REAL(R_osem,4),I_ixem,REAL(R_evem,4),L_ivem,
     & REAL(R_ovem,4),L_uvem,L_axem,R_avem,REAL(R_item,4)
     &,REAL(R_etem,4),L_exem)
      !}
C FDA60b1_vlv.fgi(  47,  31):���������� ������� ��������,20FDA62CP018XQ01
      R_elete = R8_ilete
C FDA60_sens.fgi( 399,  55):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_urare,R_ovare,REAL(1
     &,4),
     & REAL(R_elete,4),I_ivare,REAL(R_isare,4),
     & REAL(R_osare,4),REAL(R_orare,4),
     & REAL(R_irare,4),REAL(R_etare,4),L_itare,REAL(R_otare
     &,4),L_utare,
     & L_avare,R_usare,REAL(R_esare,4),REAL(R_asare,4),L_evare
     &)
      !}
C FDA60b1_vlv.fgi( 168, 523):���������� ������� ��� 2,20FDA66CF004XQ01
      R_olete = R8_ulete
C FDA60_sens.fgi( 399,  71):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_exare,R_afere,REAL(1
     &,4),
     & REAL(R_olete,4),I_udere,REAL(R_uxare,4),
     & REAL(R_abere,4),REAL(R_axare,4),
     & REAL(R_uvare,4),REAL(R_obere,4),L_ubere,REAL(R_adere
     &,4),L_edere,
     & L_idere,R_ebere,REAL(R_oxare,4),REAL(R_ixare,4),L_odere
     &)
      !}
C FDA60b1_vlv.fgi( 138, 523):���������� ������� ��� 2,20FDA65CF004XQ01
      R_amete = R8_emete
C FDA60_sens.fgi( 399,  87):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ofere,R_imere,REAL(1
     &,4),
     & REAL(R_amete,4),I_emere,REAL(R_ekere,4),
     & REAL(R_ikere,4),REAL(R_ifere,4),
     & REAL(R_efere,4),REAL(R_alere,4),L_elere,REAL(R_ilere
     &,4),L_olere,
     & L_ulere,R_okere,REAL(R_akere,4),REAL(R_ufere,4),L_amere
     &)
      !}
C FDA60b1_vlv.fgi( 108, 523):���������� ������� ��� 2,20FDA64CF004XQ01
      R_imete = R8_omete
C FDA60_sens.fgi( 399, 103):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_apere,R_usere,REAL(1
     &,4),
     & REAL(R_imete,4),I_osere,REAL(R_opere,4),
     & REAL(R_upere,4),REAL(R_umere,4),
     & REAL(R_omere,4),REAL(R_irere,4),L_orere,REAL(R_urere
     &,4),L_asere,
     & L_esere,R_arere,REAL(R_ipere,4),REAL(R_epere,4),L_isere
     &)
      !}
C FDA60b1_vlv.fgi(  80, 523):���������� ������� ��� 2,20FDA63CF004XQ01
      R_umete = R8_apete
C FDA60_sens.fgi( 399, 119):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itere,R_ebire,REAL(1
     &,4),
     & REAL(R_umete,4),I_abire,REAL(R_avere,4),
     & REAL(R_evere,4),REAL(R_etere,4),
     & REAL(R_atere,4),REAL(R_uvere,4),L_axere,REAL(R_exere
     &,4),L_ixere,
     & L_oxere,R_ivere,REAL(R_utere,4),REAL(R_otere,4),L_uxere
     &)
      !}
C FDA60b1_vlv.fgi(  50, 523):���������� ������� ��� 2,20FDA62CF004XQ01
      R_epete = R8_ipete
C FDA60_sens.fgi( 399, 135):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ubire,R_okire,REAL(1
     &,4),
     & REAL(R_epete,4),I_ikire,REAL(R_idire,4),
     & REAL(R_odire,4),REAL(R_obire,4),
     & REAL(R_ibire,4),REAL(R_efire,4),L_ifire,REAL(R_ofire
     &,4),L_ufire,
     & L_akire,R_udire,REAL(R_edire,4),REAL(R_adire,4),L_ekire
     &)
      !}
C FDA60b1_vlv.fgi(  20, 523):���������� ������� ��� 2,20FDA61CF004XQ01
      R_opete = R8_upete
C FDA60_sens.fgi( 399,  59):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_elire,R_arire,REAL(1
     &,4),
     & REAL(R_opete,4),I_upire,REAL(R_ulire,4),
     & REAL(R_amire,4),REAL(R_alire,4),
     & REAL(R_ukire,4),REAL(R_omire,4),L_umire,REAL(R_apire
     &,4),L_epire,
     & L_ipire,R_emire,REAL(R_olire,4),REAL(R_ilire,4),L_opire
     &)
      !}
C FDA60b1_vlv.fgi( 168, 539):���������� ������� ��� 2,20FDA66CF003XQ01
      R_arete = R8_erete
C FDA60_sens.fgi( 399,  63):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_orire,R_ivire,REAL(1
     &,4),
     & REAL(R_arete,4),I_evire,REAL(R_esire,4),
     & REAL(R_isire,4),REAL(R_irire,4),
     & REAL(R_erire,4),REAL(R_atire,4),L_etire,REAL(R_itire
     &,4),L_otire,
     & L_utire,R_osire,REAL(R_asire,4),REAL(R_urire,4),L_avire
     &)
      !}
C FDA60b1_vlv.fgi( 168, 555):���������� ������� ��� 2,20FDA66CF002XQ01
      R_irete = R8_orete
C FDA60_sens.fgi( 399,  67):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_atase,R_uxase,REAL(1
     &,4),
     & REAL(R_irete,4),I_oxase,REAL(R_otase,4),
     & REAL(R_utase,4),REAL(R_usase,4),
     & REAL(R_osase,4),REAL(R_ivase,4),L_ovase,REAL(R_uvase
     &,4),L_axase,
     & L_exase,R_avase,REAL(R_itase,4),REAL(R_etase,4),L_ixase
     &)
      !}
C FDA60b1_vlv.fgi( 168, 571):���������� ������� ��� 2,20FDA66CF001XQ01
      R_urete = R8_asete
C FDA60_sens.fgi( 399,  75):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_axire,R_udore,REAL(1
     &,4),
     & REAL(R_urete,4),I_odore,REAL(R_oxire,4),
     & REAL(R_uxire,4),REAL(R_uvire,4),
     & REAL(R_ovire,4),REAL(R_ibore,4),L_obore,REAL(R_ubore
     &,4),L_adore,
     & L_edore,R_abore,REAL(R_ixire,4),REAL(R_exire,4),L_idore
     &)
      !}
C FDA60b1_vlv.fgi( 138, 539):���������� ������� ��� 2,20FDA65CF003XQ01
      R_esete = R8_isete
C FDA60_sens.fgi( 399,  79):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ifore,R_emore,REAL(1
     &,4),
     & REAL(R_esete,4),I_amore,REAL(R_akore,4),
     & REAL(R_ekore,4),REAL(R_efore,4),
     & REAL(R_afore,4),REAL(R_ukore,4),L_alore,REAL(R_elore
     &,4),L_ilore,
     & L_olore,R_ikore,REAL(R_ufore,4),REAL(R_ofore,4),L_ulore
     &)
      !}
C FDA60b1_vlv.fgi( 138, 555):���������� ������� ��� 2,20FDA65CF002XQ01
      R_osete = R8_usete
C FDA60_sens.fgi( 399,  83):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ibese,R_ekese,REAL(1
     &,4),
     & REAL(R_osete,4),I_akese,REAL(R_adese,4),
     & REAL(R_edese,4),REAL(R_ebese,4),
     & REAL(R_abese,4),REAL(R_udese,4),L_afese,REAL(R_efese
     &,4),L_ifese,
     & L_ofese,R_idese,REAL(R_ubese,4),REAL(R_obese,4),L_ufese
     &)
      !}
C FDA60b1_vlv.fgi( 138, 571):���������� ������� ��� 2,20FDA65CF001XQ01
      R_atete = R8_etete
C FDA60_sens.fgi( 399,  91):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umore,R_osore,REAL(1
     &,4),
     & REAL(R_atete,4),I_isore,REAL(R_ipore,4),
     & REAL(R_opore,4),REAL(R_omore,4),
     & REAL(R_imore,4),REAL(R_erore,4),L_irore,REAL(R_orore
     &,4),L_urore,
     & L_asore,R_upore,REAL(R_epore,4),REAL(R_apore,4),L_esore
     &)
      !}
C FDA60b1_vlv.fgi( 108, 539):���������� ������� ��� 2,20FDA64CF003XQ01
      R_itete = R8_otete
C FDA60_sens.fgi( 399,  95):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_etore,R_abure,REAL(1
     &,4),
     & REAL(R_itete,4),I_uxore,REAL(R_utore,4),
     & REAL(R_avore,4),REAL(R_atore,4),
     & REAL(R_usore,4),REAL(R_ovore,4),L_uvore,REAL(R_axore
     &,4),L_exore,
     & L_ixore,R_evore,REAL(R_otore,4),REAL(R_itore,4),L_oxore
     &)
      !}
C FDA60b1_vlv.fgi( 108, 555):���������� ������� ��� 2,20FDA64CF002XQ01
      R_utete = R8_avete
C FDA60_sens.fgi( 399,  99):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ukese,R_opese,REAL(1
     &,4),
     & REAL(R_utete,4),I_ipese,REAL(R_ilese,4),
     & REAL(R_olese,4),REAL(R_okese,4),
     & REAL(R_ikese,4),REAL(R_emese,4),L_imese,REAL(R_omese
     &,4),L_umese,
     & L_apese,R_ulese,REAL(R_elese,4),REAL(R_alese,4),L_epese
     &)
      !}
C FDA60b1_vlv.fgi( 108, 571):���������� ������� ��� 2,20FDA64CF001XQ01
      R_evete = R8_ivete
C FDA60_sens.fgi( 399, 107):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obure,R_ikure,REAL(1
     &,4),
     & REAL(R_evete,4),I_ekure,REAL(R_edure,4),
     & REAL(R_idure,4),REAL(R_ibure,4),
     & REAL(R_ebure,4),REAL(R_afure,4),L_efure,REAL(R_ifure
     &,4),L_ofure,
     & L_ufure,R_odure,REAL(R_adure,4),REAL(R_ubure,4),L_akure
     &)
      !}
C FDA60b1_vlv.fgi(  80, 539):���������� ������� ��� 2,20FDA63CF003XQ01
      R_ovete = R8_uvete
C FDA60_sens.fgi( 399, 111):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_alure,R_upure,REAL(1
     &,4),
     & REAL(R_ovete,4),I_opure,REAL(R_olure,4),
     & REAL(R_ulure,4),REAL(R_ukure,4),
     & REAL(R_okure,4),REAL(R_imure,4),L_omure,REAL(R_umure
     &,4),L_apure,
     & L_epure,R_amure,REAL(R_ilure,4),REAL(R_elure,4),L_ipure
     &)
      !}
C FDA60b1_vlv.fgi(  80, 555):���������� ������� ��� 2,20FDA63CF002XQ01
      R_axete = R8_exete
C FDA60_sens.fgi( 399, 115):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_erese,R_avese,REAL(1
     &,4),
     & REAL(R_axete,4),I_utese,REAL(R_urese,4),
     & REAL(R_asese,4),REAL(R_arese,4),
     & REAL(R_upese,4),REAL(R_osese,4),L_usese,REAL(R_atese
     &,4),L_etese,
     & L_itese,R_esese,REAL(R_orese,4),REAL(R_irese,4),L_otese
     &)
      !}
C FDA60b1_vlv.fgi(  80, 571):���������� ������� ��� 2,20FDA63CF001XQ01
      R_ixete = R8_oxete
C FDA60_sens.fgi( 399, 123):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_irure,R_evure,REAL(1
     &,4),
     & REAL(R_ixete,4),I_avure,REAL(R_asure,4),
     & REAL(R_esure,4),REAL(R_erure,4),
     & REAL(R_arure,4),REAL(R_usure,4),L_ature,REAL(R_eture
     &,4),L_iture,
     & L_oture,R_isure,REAL(R_urure,4),REAL(R_orure,4),L_uture
     &)
      !}
C FDA60b1_vlv.fgi(  50, 539):���������� ������� ��� 2,20FDA62CF003XQ01
      R_uxete = R8_abite
C FDA60_sens.fgi( 399, 127):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uvure,R_odase,REAL(1
     &,4),
     & REAL(R_uxete,4),I_idase,REAL(R_ixure,4),
     & REAL(R_oxure,4),REAL(R_ovure,4),
     & REAL(R_ivure,4),REAL(R_ebase,4),L_ibase,REAL(R_obase
     &,4),L_ubase,
     & L_adase,R_uxure,REAL(R_exure,4),REAL(R_axure,4),L_edase
     &)
      !}
C FDA60b1_vlv.fgi(  50, 555):���������� ������� ��� 2,20FDA62CF002XQ01
      R_ebite = R8_ibite
C FDA60_sens.fgi( 399, 131):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovese,R_idise,REAL(1
     &,4),
     & REAL(R_ebite,4),I_edise,REAL(R_exese,4),
     & REAL(R_ixese,4),REAL(R_ivese,4),
     & REAL(R_evese,4),REAL(R_abise,4),L_ebise,REAL(R_ibise
     &,4),L_obise,
     & L_ubise,R_oxese,REAL(R_axese,4),REAL(R_uvese,4),L_adise
     &)
      !}
C FDA60b1_vlv.fgi(  50, 571):���������� ������� ��� 2,20FDA62CF001XQ01
      R_obite = R8_ubite
C FDA60_sens.fgi( 399, 139):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efase,R_amase,REAL(1
     &,4),
     & REAL(R_obite,4),I_ulase,REAL(R_ufase,4),
     & REAL(R_akase,4),REAL(R_afase,4),
     & REAL(R_udase,4),REAL(R_okase,4),L_ukase,REAL(R_alase
     &,4),L_elase,
     & L_ilase,R_ekase,REAL(R_ofase,4),REAL(R_ifase,4),L_olase
     &)
      !}
C FDA60b1_vlv.fgi(  20, 539):���������� ������� ��� 2,20FDA61CF003XQ01
      R_adite = R8_edite
C FDA60_sens.fgi( 399, 143):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_omase,R_isase,REAL(1
     &,4),
     & REAL(R_adite,4),I_esase,REAL(R_epase,4),
     & REAL(R_ipase,4),REAL(R_imase,4),
     & REAL(R_emase,4),REAL(R_arase,4),L_erase,REAL(R_irase
     &,4),L_orase,
     & L_urase,R_opase,REAL(R_apase,4),REAL(R_umase,4),L_asase
     &)
      !}
C FDA60b1_vlv.fgi(  20, 555):���������� ������� ��� 2,20FDA61CF002XQ01
      R_idite = R8_odite
C FDA60_sens.fgi( 399, 147):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_afise,R_ulise,REAL(1
     &,4),
     & REAL(R_idite,4),I_olise,REAL(R_ofise,4),
     & REAL(R_ufise,4),REAL(R_udise,4),
     & REAL(R_odise,4),REAL(R_ikise,4),L_okise,REAL(R_ukise
     &,4),L_alise,
     & L_elise,R_akise,REAL(R_ifise,4),REAL(R_efise,4),L_ilise
     &)
      !}
C FDA60b1_vlv.fgi(  20, 571):���������� ������� ��� 2,20FDA61CF001XQ01
      R_udite = R8_afite
C FDA60_sens.fgi( 399, 152):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_asal,R_uval,REAL(1,4
     &),
     & REAL(R_udite,4),I_oval,REAL(R_osal,4),
     & REAL(R_usal,4),REAL(R_ural,4),
     & REAL(R_oral,4),REAL(R_ital,4),L_otal,REAL(R_utal,4
     &),L_aval,
     & L_eval,R_atal,REAL(R_isal,4),REAL(R_esal,4),L_ival
     &)
      !}
C FDA60b1_vlv.fgi(  20,  15):���������� ������� ��� 2,20FDA61CT023XQ01
      R_efite = R8_ifite
C FDA60_sens.fgi( 399, 157):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_upim,4),REAL
     &(R_efite,4),R_elim,
     & R_arim,REAL(1,4),REAL(R_ulim,4),
     & REAL(R_amim,4),REAL(R_alim,4),
     & REAL(R_ukim,4),I_opim,REAL(R_imim,4),L_omim,
     & REAL(R_umim,4),L_apim,L_epim,R_emim,REAL(R_olim,4)
     &,REAL(R_ilim,4),L_ipim)
      !}
C FDA60b1_vlv.fgi(  19,  31):���������� ������� ��������,20FDA61CP018XQ01
      R_ofite = R8_ufite
C FDA60_sens.fgi( 399, 161):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uret,R_ovet,REAL(1,4
     &),
     & REAL(R_ofite,4),I_ivet,REAL(R_iset,4),
     & REAL(R_oset,4),REAL(R_oret,4),
     & REAL(R_iret,4),REAL(R_etet,4),L_itet,REAL(R_otet,4
     &),L_utet,
     & L_avet,R_uset,REAL(R_eset,4),REAL(R_aset,4),L_evet
     &)
      !}
C FDA60b1_vlv.fgi( 156, 275):���������� ������� ��� 2,20FDA66CU028XQ02
      R_(1) = 60
C FDA60_sens.fgi( 396, 165):��������� (RE4) (�������)
      R_akite = R8_ekite * R_(1)
C FDA60_sens.fgi( 399, 167):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ofit,R_imit,REAL(1,4
     &),
     & REAL(R_akite,4),I_emit,REAL(R_ekit,4),
     & REAL(R_ikit,4),REAL(R_ifit,4),
     & REAL(R_efit,4),REAL(R_alit,4),L_elit,REAL(R_ilit,4
     &),L_olit,
     & L_ulit,R_okit,REAL(R_akit,4),REAL(R_ufit,4),L_amit
     &)
      !}
C FDA60b1_vlv.fgi( 156, 242):���������� ������� ��� 2,20FDA66CU028XQ01
      R_ikite = R8_okite
C FDA60_sens.fgi( 399, 173):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_exet,R_afit,REAL(1,4
     &),
     & REAL(R_ikite,4),I_udit,REAL(R_uxet,4),
     & REAL(R_abit,4),REAL(R_axet,4),
     & REAL(R_uvet,4),REAL(R_obit,4),L_ubit,REAL(R_adit,4
     &),L_edit,
     & L_idit,R_ebit,REAL(R_oxet,4),REAL(R_ixet,4),L_odit
     &)
      !}
C FDA60b1_vlv.fgi( 156, 258):���������� ������� ��� 2,20FDA66CU027XQ02
      R_(2) = 60
C FDA60_sens.fgi( 396, 177):��������� (RE4) (�������)
      R_ukite = R8_alite * R_(2)
C FDA60_sens.fgi( 399, 179):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_apit,R_usit,REAL(1,4
     &),
     & REAL(R_ukite,4),I_osit,REAL(R_opit,4),
     & REAL(R_upit,4),REAL(R_umit,4),
     & REAL(R_omit,4),REAL(R_irit,4),L_orit,REAL(R_urit,4
     &),L_asit,
     & L_esit,R_arit,REAL(R_ipit,4),REAL(R_epit,4),L_isit
     &)
      !}
C FDA60b1_vlv.fgi( 156, 226):���������� ������� ��� 2,20FDA66CU027XQ01
      R_elite = R8_ilite
C FDA60_sens.fgi( 399, 184):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itit,R_ebot,REAL(1,4
     &),
     & REAL(R_elite,4),I_abot,REAL(R_avit,4),
     & REAL(R_evit,4),REAL(R_etit,4),
     & REAL(R_atit,4),REAL(R_uvit,4),L_axit,REAL(R_exit,4
     &),L_ixit,
     & L_oxit,R_ivit,REAL(R_utit,4),REAL(R_otit,4),L_uxit
     &)
      !}
C FDA60b1_vlv.fgi( 130, 275):���������� ������� ��� 2,20FDA65CU028XQ02
      R_(3) = 60
C FDA60_sens.fgi( 396, 188):��������� (RE4) (�������)
      R_olite = R8_ulite * R_(3)
C FDA60_sens.fgi( 399, 190):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_elot,R_arot,REAL(1,4
     &),
     & REAL(R_olite,4),I_upot,REAL(R_ulot,4),
     & REAL(R_amot,4),REAL(R_alot,4),
     & REAL(R_ukot,4),REAL(R_omot,4),L_umot,REAL(R_apot,4
     &),L_epot,
     & L_ipot,R_emot,REAL(R_olot,4),REAL(R_ilot,4),L_opot
     &)
      !}
C FDA60b1_vlv.fgi( 130, 242):���������� ������� ��� 2,20FDA65CU028XQ01
      R_amite = R8_emite
C FDA60_sens.fgi( 399, 196):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ubot,R_okot,REAL(1,4
     &),
     & REAL(R_amite,4),I_ikot,REAL(R_idot,4),
     & REAL(R_odot,4),REAL(R_obot,4),
     & REAL(R_ibot,4),REAL(R_efot,4),L_ifot,REAL(R_ofot,4
     &),L_ufot,
     & L_akot,R_udot,REAL(R_edot,4),REAL(R_adot,4),L_ekot
     &)
      !}
C FDA60b1_vlv.fgi( 130, 258):���������� ������� ��� 2,20FDA65CU027XQ02
      R_(4) = 60
C FDA60_sens.fgi( 396, 200):��������� (RE4) (�������)
      R_imite = R8_omite * R_(4)
C FDA60_sens.fgi( 399, 202):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_orot,R_ivot,REAL(1,4
     &),
     & REAL(R_imite,4),I_evot,REAL(R_esot,4),
     & REAL(R_isot,4),REAL(R_irot,4),
     & REAL(R_erot,4),REAL(R_atot,4),L_etot,REAL(R_itot,4
     &),L_otot,
     & L_utot,R_osot,REAL(R_asot,4),REAL(R_urot,4),L_avot
     &)
      !}
C FDA60b1_vlv.fgi( 130, 226):���������� ������� ��� 2,20FDA65CU027XQ01
      R_umite = R8_apite
C FDA60_sens.fgi( 399, 211):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_axot,R_udut,REAL(1,4
     &),
     & REAL(R_umite,4),I_odut,REAL(R_oxot,4),
     & REAL(R_uxot,4),REAL(R_uvot,4),
     & REAL(R_ovot,4),REAL(R_ibut,4),L_obut,REAL(R_ubut,4
     &),L_adut,
     & L_edut,R_abut,REAL(R_ixot,4),REAL(R_exot,4),L_idut
     &)
      !}
C FDA60b1_vlv.fgi( 102, 275):���������� ������� ��� 2,20FDA64CU028XQ02
      R_(5) = 60
C FDA60_sens.fgi( 396, 215):��������� (RE4) (�������)
      R_epite = R8_ipite * R_(5)
C FDA60_sens.fgi( 399, 217):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umut,R_osut,REAL(1,4
     &),
     & REAL(R_epite,4),I_isut,REAL(R_iput,4),
     & REAL(R_oput,4),REAL(R_omut,4),
     & REAL(R_imut,4),REAL(R_erut,4),L_irut,REAL(R_orut,4
     &),L_urut,
     & L_asut,R_uput,REAL(R_eput,4),REAL(R_aput,4),L_esut
     &)
      !}
C FDA60b1_vlv.fgi( 102, 242):���������� ������� ��� 2,20FDA64CU028XQ01
      R_opite = R8_upite
C FDA60_sens.fgi( 399, 223):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ifut,R_emut,REAL(1,4
     &),
     & REAL(R_opite,4),I_amut,REAL(R_akut,4),
     & REAL(R_ekut,4),REAL(R_efut,4),
     & REAL(R_afut,4),REAL(R_ukut,4),L_alut,REAL(R_elut,4
     &),L_ilut,
     & L_olut,R_ikut,REAL(R_ufut,4),REAL(R_ofut,4),L_ulut
     &)
      !}
C FDA60b1_vlv.fgi( 102, 258):���������� ������� ��� 2,20FDA64CU027XQ02
      R_(6) = 60
C FDA60_sens.fgi( 396, 227):��������� (RE4) (�������)
      R_arite = R8_erite * R_(6)
C FDA60_sens.fgi( 399, 229):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_etut,R_abav,REAL(1,4
     &),
     & REAL(R_arite,4),I_uxut,REAL(R_utut,4),
     & REAL(R_avut,4),REAL(R_atut,4),
     & REAL(R_usut,4),REAL(R_ovut,4),L_uvut,REAL(R_axut,4
     &),L_exut,
     & L_ixut,R_evut,REAL(R_otut,4),REAL(R_itut,4),L_oxut
     &)
      !}
C FDA60b1_vlv.fgi( 102, 226):���������� ������� ��� 2,20FDA64CU027XQ01
      R_irite = R8_orite
C FDA60_sens.fgi( 398, 238):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obav,R_ikav,REAL(1,4
     &),
     & REAL(R_irite,4),I_ekav,REAL(R_edav,4),
     & REAL(R_idav,4),REAL(R_ibav,4),
     & REAL(R_ebav,4),REAL(R_afav,4),L_efav,REAL(R_ifav,4
     &),L_ofav,
     & L_ufav,R_odav,REAL(R_adav,4),REAL(R_ubav,4),L_akav
     &)
      !}
C FDA60b1_vlv.fgi(  74, 275):���������� ������� ��� 2,20FDA63CU028XQ02
      R_(7) = 60
C FDA60_sens.fgi( 395, 242):��������� (RE4) (�������)
      R_urite = R8_asite * R_(7)
C FDA60_sens.fgi( 398, 244):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_irav,R_evav,REAL(1,4
     &),
     & REAL(R_urite,4),I_avav,REAL(R_asav,4),
     & REAL(R_esav,4),REAL(R_erav,4),
     & REAL(R_arav,4),REAL(R_usav,4),L_atav,REAL(R_etav,4
     &),L_itav,
     & L_otav,R_isav,REAL(R_urav,4),REAL(R_orav,4),L_utav
     &)
      !}
C FDA60b1_vlv.fgi(  74, 242):���������� ������� ��� 2,20FDA63CU028XQ01
      R_esite = R8_isite
C FDA60_sens.fgi( 398, 250):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_alav,R_upav,REAL(1,4
     &),
     & REAL(R_esite,4),I_opav,REAL(R_olav,4),
     & REAL(R_ulav,4),REAL(R_ukav,4),
     & REAL(R_okav,4),REAL(R_imav,4),L_omav,REAL(R_umav,4
     &),L_apav,
     & L_epav,R_amav,REAL(R_ilav,4),REAL(R_elav,4),L_ipav
     &)
      !}
C FDA60b1_vlv.fgi(  74, 258):���������� ������� ��� 2,20FDA63CU027XQ02
      R_(8) = 60
C FDA60_sens.fgi( 395, 254):��������� (RE4) (�������)
      R_osite = R8_usite * R_(8)
C FDA60_sens.fgi( 398, 256):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uvav,R_odev,REAL(1,4
     &),
     & REAL(R_osite,4),I_idev,REAL(R_ixav,4),
     & REAL(R_oxav,4),REAL(R_ovav,4),
     & REAL(R_ivav,4),REAL(R_ebev,4),L_ibev,REAL(R_obev,4
     &),L_ubev,
     & L_adev,R_uxav,REAL(R_exav,4),REAL(R_axav,4),L_edev
     &)
      !}
C FDA60b1_vlv.fgi(  74, 226):���������� ������� ��� 2,20FDA63CU027XQ01
      R_atite = R8_etite
C FDA60_sens.fgi( 398, 265):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efev,R_amev,REAL(1,4
     &),
     & REAL(R_atite,4),I_ulev,REAL(R_ufev,4),
     & REAL(R_akev,4),REAL(R_afev,4),
     & REAL(R_udev,4),REAL(R_okev,4),L_ukev,REAL(R_alev,4
     &),L_elev,
     & L_ilev,R_ekev,REAL(R_ofev,4),REAL(R_ifev,4),L_olev
     &)
      !}
C FDA60b1_vlv.fgi(  48, 275):���������� ������� ��� 2,20FDA62CU028XQ02
      R_(9) = 60
C FDA60_sens.fgi( 395, 269):��������� (RE4) (�������)
      R_itite = R8_otite * R_(9)
C FDA60_sens.fgi( 398, 271):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_atev,R_uxev,REAL(1,4
     &),
     & REAL(R_itite,4),I_oxev,REAL(R_otev,4),
     & REAL(R_utev,4),REAL(R_usev,4),
     & REAL(R_osev,4),REAL(R_ivev,4),L_ovev,REAL(R_uvev,4
     &),L_axev,
     & L_exev,R_avev,REAL(R_itev,4),REAL(R_etev,4),L_ixev
     &)
      !}
C FDA60b1_vlv.fgi(  48, 242):���������� ������� ��� 2,20FDA62CU028XQ01
      R_utite = R8_avite
C FDA60_sens.fgi( 398, 277):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_omev,R_isev,REAL(1,4
     &),
     & REAL(R_utite,4),I_esev,REAL(R_epev,4),
     & REAL(R_ipev,4),REAL(R_imev,4),
     & REAL(R_emev,4),REAL(R_arev,4),L_erev,REAL(R_irev,4
     &),L_orev,
     & L_urev,R_opev,REAL(R_apev,4),REAL(R_umev,4),L_asev
     &)
      !}
C FDA60b1_vlv.fgi(  48, 258):���������� ������� ��� 2,20FDA62CU027XQ02
      R_(10) = 60
C FDA60_sens.fgi( 395, 281):��������� (RE4) (�������)
      R_evite = R8_ivite * R_(10)
C FDA60_sens.fgi( 398, 283):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ibiv,R_ekiv,REAL(1,4
     &),
     & REAL(R_evite,4),I_akiv,REAL(R_adiv,4),
     & REAL(R_ediv,4),REAL(R_ebiv,4),
     & REAL(R_abiv,4),REAL(R_udiv,4),L_afiv,REAL(R_efiv,4
     &),L_ifiv,
     & L_ofiv,R_idiv,REAL(R_ubiv,4),REAL(R_obiv,4),L_ufiv
     &)
      !}
C FDA60b1_vlv.fgi(  48, 226):���������� ������� ��� 2,20FDA62CU027XQ01
      R_uvite = R8_axite
C FDA60_sens.fgi( 398, 291):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ukiv,R_opiv,REAL(1,4
     &),
     & REAL(R_uvite,4),I_ipiv,REAL(R_iliv,4),
     & REAL(R_oliv,4),REAL(R_okiv,4),
     & REAL(R_ikiv,4),REAL(R_emiv,4),L_imiv,REAL(R_omiv,4
     &),L_umiv,
     & L_apiv,R_uliv,REAL(R_eliv,4),REAL(R_aliv,4),L_epiv
     &)
      !}
C FDA60b1_vlv.fgi(  20, 275):���������� ������� ��� 2,20FDA61CU028XQ02
      R_(11) = 60
C FDA60_sens.fgi( 395, 295):��������� (RE4) (�������)
      R_exite = R8_ixite * R_(11)
C FDA60_sens.fgi( 398, 297):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_oviv,R_idov,REAL(1,4
     &),
     & REAL(R_exite,4),I_edov,REAL(R_exiv,4),
     & REAL(R_ixiv,4),REAL(R_iviv,4),
     & REAL(R_eviv,4),REAL(R_abov,4),L_ebov,REAL(R_ibov,4
     &),L_obov,
     & L_ubov,R_oxiv,REAL(R_axiv,4),REAL(R_uviv,4),L_adov
     &)
      !}
C FDA60b1_vlv.fgi(  20, 242):���������� ������� ��� 2,20FDA61CU028XQ01
      R_oxite = R8_uxite
C FDA60_sens.fgi( 398, 303):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_eriv,R_aviv,REAL(1,4
     &),
     & REAL(R_oxite,4),I_utiv,REAL(R_uriv,4),
     & REAL(R_asiv,4),REAL(R_ariv,4),
     & REAL(R_upiv,4),REAL(R_osiv,4),L_usiv,REAL(R_ativ,4
     &),L_etiv,
     & L_itiv,R_esiv,REAL(R_oriv,4),REAL(R_iriv,4),L_otiv
     &)
      !}
C FDA60b1_vlv.fgi(  20, 258):���������� ������� ��� 2,20FDA61CU027XQ02
      R_(12) = 60
C FDA60_sens.fgi( 395, 307):��������� (RE4) (�������)
      R_abote = R8_ebote * R_(12)
C FDA60_sens.fgi( 398, 309):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_afov,R_ulov,REAL(1,4
     &),
     & REAL(R_abote,4),I_olov,REAL(R_ofov,4),
     & REAL(R_ufov,4),REAL(R_udov,4),
     & REAL(R_odov,4),REAL(R_ikov,4),L_okov,REAL(R_ukov,4
     &),L_alov,
     & L_elov,R_akov,REAL(R_ifov,4),REAL(R_efov,4),L_ilov
     &)
      !}
C FDA60b1_vlv.fgi(  20, 226):���������� ������� ��� 2,20FDA61CU027XQ01
      R_ibote = R8_obote
C FDA60_sens.fgi( 305, 149):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obibe,R_ikibe,REAL(1
     &,4),
     & REAL(R_ibote,4),I_ekibe,REAL(R_edibe,4),
     & REAL(R_idibe,4),REAL(R_ibibe,4),
     & REAL(R_ebibe,4),REAL(R_afibe,4),L_efibe,REAL(R_ifibe
     &,4),L_ofibe,
     & L_ufibe,R_odibe,REAL(R_adibe,4),REAL(R_ubibe,4),L_akibe
     &)
      !}
C FDA60b1_vlv.fgi( 158, 367):���������� ������� ��� 2,20FDA66CQ001XQ01
      R_ubote = R8_adote
C FDA60_sens.fgi( 305, 153):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_alibe,R_upibe,REAL(1
     &,4),
     & REAL(R_ubote,4),I_opibe,REAL(R_olibe,4),
     & REAL(R_ulibe,4),REAL(R_ukibe,4),
     & REAL(R_okibe,4),REAL(R_imibe,4),L_omibe,REAL(R_umibe
     &,4),L_apibe,
     & L_epibe,R_amibe,REAL(R_ilibe,4),REAL(R_elibe,4),L_ipibe
     &)
      !}
C FDA60b1_vlv.fgi( 158, 382):���������� ������� ��� 2,20FDA66CM001XQ01
      R_edote = R8_idote
C FDA60_sens.fgi( 305, 158):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_iribe,R_evibe,REAL(1
     &,4),
     & REAL(R_edote,4),I_avibe,REAL(R_asibe,4),
     & REAL(R_esibe,4),REAL(R_eribe,4),
     & REAL(R_aribe,4),REAL(R_usibe,4),L_atibe,REAL(R_etibe
     &,4),L_itibe,
     & L_otibe,R_isibe,REAL(R_uribe,4),REAL(R_oribe,4),L_utibe
     &)
      !}
C FDA60b1_vlv.fgi( 131, 367):���������� ������� ��� 2,20FDA65CQ001XQ01
      R_odote = R8_udote
C FDA60_sens.fgi( 305, 162):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uvibe,R_odobe,REAL(1
     &,4),
     & REAL(R_odote,4),I_idobe,REAL(R_ixibe,4),
     & REAL(R_oxibe,4),REAL(R_ovibe,4),
     & REAL(R_ivibe,4),REAL(R_ebobe,4),L_ibobe,REAL(R_obobe
     &,4),L_ubobe,
     & L_adobe,R_uxibe,REAL(R_exibe,4),REAL(R_axibe,4),L_edobe
     &)
      !}
C FDA60b1_vlv.fgi( 131, 382):���������� ������� ��� 2,20FDA65CM001XQ01
      R_afote = R8_efote
C FDA60_sens.fgi( 305, 167):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efobe,R_amobe,REAL(1
     &,4),
     & REAL(R_afote,4),I_ulobe,REAL(R_ufobe,4),
     & REAL(R_akobe,4),REAL(R_afobe,4),
     & REAL(R_udobe,4),REAL(R_okobe,4),L_ukobe,REAL(R_alobe
     &,4),L_elobe,
     & L_ilobe,R_ekobe,REAL(R_ofobe,4),REAL(R_ifobe,4),L_olobe
     &)
      !}
C FDA60b1_vlv.fgi( 104, 367):���������� ������� ��� 2,20FDA64CQ001XQ01
      R_ifote = R8_ofote
C FDA60_sens.fgi( 305, 171):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_omobe,R_isobe,REAL(1
     &,4),
     & REAL(R_ifote,4),I_esobe,REAL(R_epobe,4),
     & REAL(R_ipobe,4),REAL(R_imobe,4),
     & REAL(R_emobe,4),REAL(R_arobe,4),L_erobe,REAL(R_irobe
     &,4),L_orobe,
     & L_urobe,R_opobe,REAL(R_apobe,4),REAL(R_umobe,4),L_asobe
     &)
      !}
C FDA60b1_vlv.fgi( 104, 382):���������� ������� ��� 2,20FDA64CM001XQ01
      R_ufote = R8_akote
C FDA60_sens.fgi( 305, 176):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_atobe,R_uxobe,REAL(1
     &,4),
     & REAL(R_ufote,4),I_oxobe,REAL(R_otobe,4),
     & REAL(R_utobe,4),REAL(R_usobe,4),
     & REAL(R_osobe,4),REAL(R_ivobe,4),L_ovobe,REAL(R_uvobe
     &,4),L_axobe,
     & L_exobe,R_avobe,REAL(R_itobe,4),REAL(R_etobe,4),L_ixobe
     &)
      !}
C FDA60b1_vlv.fgi(  76, 367):���������� ������� ��� 2,20FDA63CQ001XQ01
      R_ekote = R8_ikote
C FDA60_sens.fgi( 305, 180):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ibube,R_ekube,REAL(1
     &,4),
     & REAL(R_ekote,4),I_akube,REAL(R_adube,4),
     & REAL(R_edube,4),REAL(R_ebube,4),
     & REAL(R_abube,4),REAL(R_udube,4),L_afube,REAL(R_efube
     &,4),L_ifube,
     & L_ofube,R_idube,REAL(R_ubube,4),REAL(R_obube,4),L_ufube
     &)
      !}
C FDA60b1_vlv.fgi(  76, 382):���������� ������� ��� 2,20FDA63CM001XQ01
      R_okote = R8_ukote
C FDA60_sens.fgi( 305, 185):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ukube,R_opube,REAL(1
     &,4),
     & REAL(R_okote,4),I_ipube,REAL(R_ilube,4),
     & REAL(R_olube,4),REAL(R_okube,4),
     & REAL(R_ikube,4),REAL(R_emube,4),L_imube,REAL(R_omube
     &,4),L_umube,
     & L_apube,R_ulube,REAL(R_elube,4),REAL(R_alube,4),L_epube
     &)
      !}
C FDA60b1_vlv.fgi(  49, 367):���������� ������� ��� 2,20FDA62CQ001XQ01
      R_alote = R8_elote
C FDA60_sens.fgi( 305, 189):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_erube,R_avube,REAL(1
     &,4),
     & REAL(R_alote,4),I_utube,REAL(R_urube,4),
     & REAL(R_asube,4),REAL(R_arube,4),
     & REAL(R_upube,4),REAL(R_osube,4),L_usube,REAL(R_atube
     &,4),L_etube,
     & L_itube,R_esube,REAL(R_orube,4),REAL(R_irube,4),L_otube
     &)
      !}
C FDA60b1_vlv.fgi(  49, 382):���������� ������� ��� 2,20FDA62CM001XQ01
      R_ilote = R8_olote
C FDA60_sens.fgi( 305, 193):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovube,R_idade,REAL(1
     &,4),
     & REAL(R_ilote,4),I_edade,REAL(R_exube,4),
     & REAL(R_ixube,4),REAL(R_ivube,4),
     & REAL(R_evube,4),REAL(R_abade,4),L_ebade,REAL(R_ibade
     &,4),L_obade,
     & L_ubade,R_oxube,REAL(R_axube,4),REAL(R_uvube,4),L_adade
     &)
      !}
C FDA60b1_vlv.fgi(  22, 367):���������� ������� ��� 2,20FDA61CQ001XQ01
      R_ulote = R8_amote
C FDA60_sens.fgi( 305, 197):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_afade,R_ulade,REAL(1
     &,4),
     & REAL(R_ulote,4),I_olade,REAL(R_ofade,4),
     & REAL(R_ufade,4),REAL(R_udade,4),
     & REAL(R_odade,4),REAL(R_ikade,4),L_okade,REAL(R_ukade
     &,4),L_alade,
     & L_elade,R_akade,REAL(R_ifade,4),REAL(R_efade,4),L_ilade
     &)
      !}
C FDA60b1_vlv.fgi(  22, 382):���������� ������� ��� 2,20FDA61CM001XQ01
      R_ebute = R8_ibute
C FDA60_sens.fgi( 141, 113):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obame,R_ikame,REAL(1
     &,4),
     & REAL(R_ebute,4),I_ekame,REAL(R_edame,4),
     & REAL(R_idame,4),REAL(R_ibame,4),
     & REAL(R_ebame,4),REAL(R_afame,4),L_efame,REAL(R_ifame
     &,4),L_ofame,
     & L_ufame,R_odame,REAL(R_adame,4),REAL(R_ubame,4),L_akame
     &)
      !}
C FDA60b1_vlv.fgi( 168, 428):���������� ������� ��� 2,20FDA66CP006XQ01
      R_obute = R8_ubute
C FDA60_sens.fgi( 141, 116):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_oxeme,4),REAL
     &(R_obute,4),R_ateme,
     & R_uxeme,REAL(1,4),REAL(R_oteme,4),
     & REAL(R_uteme,4),REAL(R_useme,4),
     & REAL(R_oseme,4),I_ixeme,REAL(R_eveme,4),L_iveme,
     & REAL(R_oveme,4),L_uveme,L_axeme,R_aveme,REAL(R_iteme
     &,4),REAL(R_eteme,4),L_exeme)
      !}
C FDA60b1_vlv.fgi( 168, 445):���������� ������� ��������,20FDA66CP005XQ01
      R_adute = R8_edute
C FDA60_sens.fgi( 141, 119):��������
      R_idute = R8_odute
C FDA60_sens.fgi( 141, 122):��������
      R_udute = R8_afute
C FDA60_sens.fgi( 141, 125):��������
      R_efute = R8_ifute
C FDA60_sens.fgi( 141, 129):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_alame,R_upame,REAL(1
     &,4),
     & REAL(R_efute,4),I_opame,REAL(R_olame,4),
     & REAL(R_ulame,4),REAL(R_ukame,4),
     & REAL(R_okame,4),REAL(R_imame,4),L_omame,REAL(R_umame
     &,4),L_apame,
     & L_epame,R_amame,REAL(R_ilame,4),REAL(R_elame,4),L_ipame
     &)
      !}
C FDA60b1_vlv.fgi( 140, 428):���������� ������� ��� 2,20FDA65CP006XQ01
      R_ofute = R8_ufute
C FDA60_sens.fgi( 141, 132):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ekime,4),REAL
     &(R_ofute,4),R_obime,
     & R_ikime,REAL(1,4),REAL(R_edime,4),
     & REAL(R_idime,4),REAL(R_ibime,4),
     & REAL(R_ebime,4),I_akime,REAL(R_udime,4),L_afime,
     & REAL(R_efime,4),L_ifime,L_ofime,R_odime,REAL(R_adime
     &,4),REAL(R_ubime,4),L_ufime)
      !}
C FDA60b1_vlv.fgi( 140, 445):���������� ������� ��������,20FDA65CP005XQ01
      R_akute = R8_ekute
C FDA60_sens.fgi( 141, 135):��������
      R_ikute = R8_okute
C FDA60_sens.fgi( 141, 138):��������
      R_ukute = R8_alute
C FDA60_sens.fgi( 141, 141):��������
      R_elute = 12.2
C FDA60_sens.fgi( 219, 154):��������� (RE4) (�������)
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_orape,R_ivape,REAL(1
     &,4),
     & REAL(R_elute,4),I_evape,REAL(R_esape,4),
     & REAL(R_isape,4),REAL(R_irape,4),
     & REAL(R_erape,4),REAL(R_atape,4),L_etape,REAL(R_itape
     &,4),L_otape,
     & L_utape,R_osape,REAL(R_asape,4),REAL(R_urape,4),L_avape
     &)
      !}
C FDA60b1_vlv.fgi( 139, 477):���������� ������� ��� 2,20FDA65CP003XQ01
      R_ilute = 12.2
C FDA60_sens.fgi( 219, 150):��������� (RE4) (�������)
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_elape,R_arape,REAL(1
     &,4),
     & REAL(R_ilute,4),I_upape,REAL(R_ulape,4),
     & REAL(R_amape,4),REAL(R_alape,4),
     & REAL(R_ukape,4),REAL(R_omape,4),L_umape,REAL(R_apape
     &,4),L_epape,
     & L_ipape,R_emape,REAL(R_olape,4),REAL(R_ilape,4),L_opape
     &)
      !}
C FDA60b1_vlv.fgi( 167, 477):���������� ������� ��� 2,20FDA66CP003XQ01
      R_olute = 12.2
C FDA60_sens.fgi( 219, 162):��������� (RE4) (�������)
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ifepe,R_emepe,REAL(1
     &,4),
     & REAL(R_olute,4),I_amepe,REAL(R_akepe,4),
     & REAL(R_ekepe,4),REAL(R_efepe,4),
     & REAL(R_afepe,4),REAL(R_ukepe,4),L_alepe,REAL(R_elepe
     &,4),L_ilepe,
     & L_olepe,R_ikepe,REAL(R_ufepe,4),REAL(R_ofepe,4),L_ulepe
     &)
      !}
C FDA60b1_vlv.fgi(  80, 477):���������� ������� ��� 2,20FDA63CP003XQ01
      R_ulute = 12.2
C FDA60_sens.fgi( 219, 158):��������� (RE4) (�������)
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_axape,R_udepe,REAL(1
     &,4),
     & REAL(R_ulute,4),I_odepe,REAL(R_oxape,4),
     & REAL(R_uxape,4),REAL(R_uvape,4),
     & REAL(R_ovape,4),REAL(R_ibepe,4),L_obepe,REAL(R_ubepe
     &,4),L_adepe,
     & L_edepe,R_abepe,REAL(R_ixape,4),REAL(R_exape,4),L_idepe
     &)
      !}
C FDA60b1_vlv.fgi( 110, 477):���������� ������� ��� 2,20FDA64CP003XQ01
      R_amute = 12.2
C FDA60_sens.fgi( 219, 170):��������� (RE4) (�������)
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_etepe,R_abipe,REAL(1
     &,4),
     & REAL(R_amute,4),I_uxepe,REAL(R_utepe,4),
     & REAL(R_avepe,4),REAL(R_atepe,4),
     & REAL(R_usepe,4),REAL(R_ovepe,4),L_uvepe,REAL(R_axepe
     &,4),L_exepe,
     & L_ixepe,R_evepe,REAL(R_otepe,4),REAL(R_itepe,4),L_oxepe
     &)
      !}
C FDA60b1_vlv.fgi(  20, 477):���������� ������� ��� 2,20FDA61CP003XQ01
      R_emute = 12.2
C FDA60_sens.fgi( 219, 166):��������� (RE4) (�������)
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umepe,R_osepe,REAL(1
     &,4),
     & REAL(R_emute,4),I_isepe,REAL(R_ipepe,4),
     & REAL(R_opepe,4),REAL(R_omepe,4),
     & REAL(R_imepe,4),REAL(R_erepe,4),L_irepe,REAL(R_orepe
     &,4),L_urepe,
     & L_asepe,R_upepe,REAL(R_epepe,4),REAL(R_apepe,4),L_esepe
     &)
      !}
C FDA60b1_vlv.fgi(  51, 477):���������� ������� ��� 2,20FDA62CP003XQ01
      R_imute = R8_omute
C FDA60_sens.fgi( 218, 176):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ibuv,R_ekuv,REAL(1,4
     &),
     & REAL(R_imute,4),I_akuv,REAL(R_aduv,4),
     & REAL(R_eduv,4),REAL(R_ebuv,4),
     & REAL(R_abuv,4),REAL(R_uduv,4),L_afuv,REAL(R_efuv,4
     &),L_ifuv,
     & L_ofuv,R_iduv,REAL(R_ubuv,4),REAL(R_obuv,4),L_ufuv
     &)
      !}
C FDA60b1_vlv.fgi( 157, 321):���������� ������� ��� 2,20FDA66CT008XQ01
      R_esute = R8_isute
C FDA60_sens.fgi( 218, 207):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_udud,R_olud,REAL(1e
     &-4,4),
     & REAL(R_esute,4),I_ilud,REAL(R_ifud,4),
     & REAL(R_ofud,4),REAL(R_odud,4),
     & REAL(R_idud,4),REAL(R_ekud,4),L_ikud,REAL(R_okud,4
     &),L_ukud,
     & L_alud,R_ufud,REAL(R_efud,4),REAL(R_afud,4),L_elud
     &)
      !}
C FDA60b1_vlv.fgi( 361, 141):���������� ������� ��� 2,20FDA66CQ007XQ01
      R_osute = R8_usute
C FDA60_sens.fgi( 218, 217):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ixov,4),REAL
     &(R_osute,4),R_usov,
     & R_oxov,REAL(1,4),REAL(R_itov,4),
     & REAL(R_otov,4),REAL(R_osov,4),
     & REAL(R_isov,4),I_exov,REAL(R_avov,4),L_evov,
     & REAL(R_ivov,4),L_ovov,L_uvov,R_utov,REAL(R_etov,4)
     &,REAL(R_atov,4),L_axov)
      !}
C FDA60b1_vlv.fgi( 157, 305):���������� ������� ��������,20FDA66CP011XQ01
      R_atute = R8_etute
C FDA60_sens.fgi( 218, 221):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ixim,R_efom,REAL(1,4
     &),
     & REAL(R_atute,4),I_afom,REAL(R_abom,4),
     & REAL(R_ebom,4),REAL(R_exim,4),
     & REAL(R_axim,4),REAL(R_ubom,4),L_adom,REAL(R_edom,4
     &),L_idom,
     & L_odom,R_ibom,REAL(R_uxim,4),REAL(R_oxim,4),L_udom
     &)
      !}
C FDA60b1_vlv.fgi( 159,  61):���������� ������� ��� 2,20FDA66CP017XQ01
      R_itute = R8_otute
C FDA60_sens.fgi( 218, 225):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imov,R_esov,REAL(1,4
     &),
     & REAL(R_itute,4),I_asov,REAL(R_apov,4),
     & REAL(R_epov,4),REAL(R_emov,4),
     & REAL(R_amov,4),REAL(R_upov,4),L_arov,REAL(R_erov,4
     &),L_irov,
     & L_orov,R_ipov,REAL(R_umov,4),REAL(R_omov,4),L_urov
     &)
      !}
C FDA60b1_vlv.fgi( 157, 290):���������� ������� ��� 2,20FDA66CP012XQ01
      R_utute = R8_avute
C FDA60_sens.fgi( 218, 272):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_orep,R_ivep,REAL(1,4
     &),
     & REAL(R_utute,4),I_evep,REAL(R_esep,4),
     & REAL(R_isep,4),REAL(R_irep,4),
     & REAL(R_erep,4),REAL(R_atep,4),L_etep,REAL(R_itep,4
     &),L_otep,
     & L_utep,R_osep,REAL(R_asep,4),REAL(R_urep,4),L_avep
     &)
      !}
C FDA60b1_vlv.fgi( 158, 106):���������� ������� ��� 2,20FDA66CT014XQ01
      R_evute = R8_ivute
C FDA60_sens.fgi( 139, 156):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_irame,R_evame,REAL(1
     &,4),
     & REAL(R_evute,4),I_avame,REAL(R_asame,4),
     & REAL(R_esame,4),REAL(R_erame,4),
     & REAL(R_arame,4),REAL(R_usame,4),L_atame,REAL(R_etame
     &,4),L_itame,
     & L_otame,R_isame,REAL(R_urame,4),REAL(R_orame,4),L_utame
     &)
      !}
C FDA60b1_vlv.fgi( 111, 428):���������� ������� ��� 2,20FDA64CP006XQ01
      R_ovute = R8_uvute
C FDA60_sens.fgi( 139, 159):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_upime,4),REAL
     &(R_ovute,4),R_elime,
     & R_arime,REAL(1,4),REAL(R_ulime,4),
     & REAL(R_amime,4),REAL(R_alime,4),
     & REAL(R_ukime,4),I_opime,REAL(R_imime,4),L_omime,
     & REAL(R_umime,4),L_apime,L_epime,R_emime,REAL(R_olime
     &,4),REAL(R_ilime,4),L_ipime)
      !}
C FDA60b1_vlv.fgi( 111, 445):���������� ������� ��������,20FDA64CP005XQ01
      R_axute = R8_exute
C FDA60_sens.fgi( 139, 162):��������
      R_ixute = R8_oxute
C FDA60_sens.fgi( 139, 165):��������
      R_uxute = R8_abave
C FDA60_sens.fgi( 139, 168):��������
      R_ebave = R8_ibave
C FDA60_sens.fgi( 139, 172):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uvame,R_odeme,REAL(1
     &,4),
     & REAL(R_ebave,4),I_ideme,REAL(R_ixame,4),
     & REAL(R_oxame,4),REAL(R_ovame,4),
     & REAL(R_ivame,4),REAL(R_ebeme,4),L_ibeme,REAL(R_obeme
     &,4),L_ubeme,
     & L_ademe,R_uxame,REAL(R_exame,4),REAL(R_axame,4),L_edeme
     &)
      !}
C FDA60b1_vlv.fgi(  80, 428):���������� ������� ��� 2,20FDA63CP006XQ01
      R_obave = R8_ubave
C FDA60_sens.fgi( 139, 175):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ivime,4),REAL
     &(R_obave,4),R_urime,
     & R_ovime,REAL(1,4),REAL(R_isime,4),
     & REAL(R_osime,4),REAL(R_orime,4),
     & REAL(R_irime,4),I_evime,REAL(R_atime,4),L_etime,
     & REAL(R_itime,4),L_otime,L_utime,R_usime,REAL(R_esime
     &,4),REAL(R_asime,4),L_avime)
      !}
C FDA60b1_vlv.fgi(  80, 445):���������� ������� ��������,20FDA63CP005XQ01
      R_adave = R8_edave
C FDA60_sens.fgi( 139, 178):��������
      R_idave = R8_odave
C FDA60_sens.fgi( 139, 181):��������
      R_udave = R8_afave
C FDA60_sens.fgi( 139, 184):��������
      R_efave = R8_ifave
C FDA60_sens.fgi( 139, 188):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efeme,R_ameme,REAL(1
     &,4),
     & REAL(R_efave,4),I_uleme,REAL(R_ufeme,4),
     & REAL(R_akeme,4),REAL(R_afeme,4),
     & REAL(R_udeme,4),REAL(R_okeme,4),L_ukeme,REAL(R_aleme
     &,4),L_eleme,
     & L_ileme,R_ekeme,REAL(R_ofeme,4),REAL(R_ifeme,4),L_oleme
     &)
      !}
C FDA60b1_vlv.fgi(  51, 428):���������� ������� ��� 2,20FDA62CP006XQ01
      R_ofave = R8_ufave
C FDA60_sens.fgi( 139, 191):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_afome,4),REAL
     &(R_ofave,4),R_ixime,
     & R_efome,REAL(1,4),REAL(R_abome,4),
     & REAL(R_ebome,4),REAL(R_exime,4),
     & REAL(R_axime,4),I_udome,REAL(R_obome,4),L_ubome,
     & REAL(R_adome,4),L_edome,L_idome,R_ibome,REAL(R_uxime
     &,4),REAL(R_oxime,4),L_odome)
      !}
C FDA60b1_vlv.fgi(  51, 445):���������� ������� ��������,20FDA62CP005XQ01
      R_akave = R8_ekave
C FDA60_sens.fgi( 139, 194):��������
      R_ikave = R8_okave
C FDA60_sens.fgi( 139, 197):��������
      R_ukave = R8_alave
C FDA60_sens.fgi( 139, 200):��������
      R_elave = R8_ilave
C FDA60_sens.fgi( 139, 204):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_omeme,R_iseme,REAL(1
     &,4),
     & REAL(R_elave,4),I_eseme,REAL(R_epeme,4),
     & REAL(R_ipeme,4),REAL(R_imeme,4),
     & REAL(R_ememe,4),REAL(R_areme,4),L_ereme,REAL(R_ireme
     &,4),L_oreme,
     & L_ureme,R_opeme,REAL(R_apeme,4),REAL(R_umeme,4),L_aseme
     &)
      !}
C FDA60b1_vlv.fgi(  21, 428):���������� ������� ��� 2,20FDA61CP006XQ01
      R_olave = R8_ulave
C FDA60_sens.fgi( 139, 207):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_omome,4),REAL
     &(R_olave,4),R_akome,
     & R_umome,REAL(1,4),REAL(R_okome,4),
     & REAL(R_ukome,4),REAL(R_ufome,4),
     & REAL(R_ofome,4),I_imome,REAL(R_elome,4),L_ilome,
     & REAL(R_olome,4),L_ulome,L_amome,R_alome,REAL(R_ikome
     &,4),REAL(R_ekome,4),L_emome)
      !}
C FDA60b1_vlv.fgi(  20, 445):���������� ������� ��������,20FDA61CP005XQ01
      R_amave = R8_emave
C FDA60_sens.fgi( 139, 210):��������
      R_imave = R8_omave
C FDA60_sens.fgi( 139, 213):��������
      R_umave = R8_apave
C FDA60_sens.fgi( 139, 216):��������
      R_epave = R8_ipave
C FDA60_sens.fgi( 139, 220):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ukuv,R_opuv,REAL(1,4
     &),
     & REAL(R_epave,4),I_ipuv,REAL(R_iluv,4),
     & REAL(R_oluv,4),REAL(R_okuv,4),
     & REAL(R_ikuv,4),REAL(R_emuv,4),L_imuv,REAL(R_omuv,4
     &),L_umuv,
     & L_apuv,R_uluv,REAL(R_eluv,4),REAL(R_aluv,4),L_epuv
     &)
      !}
C FDA60b1_vlv.fgi( 130, 290):���������� ������� ��� 2,20FDA65CP012XQ01
      R_opave = R8_upave
C FDA60_sens.fgi( 139, 223):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efax,R_amax,REAL(1,4
     &),
     & REAL(R_opave,4),I_ulax,REAL(R_ufax,4),
     & REAL(R_akax,4),REAL(R_afax,4),
     & REAL(R_udax,4),REAL(R_okax,4),L_ukax,REAL(R_alax,4
     &),L_elax,
     & L_ilax,R_ekax,REAL(R_ofax,4),REAL(R_ifax,4),L_olax
     &)
      !}
C FDA60b1_vlv.fgi( 103, 290):���������� ������� ��� 2,20FDA64CP012XQ01
      R_arave = R8_erave
C FDA60_sens.fgi( 139, 226):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obex,R_ikex,REAL(1,4
     &),
     & REAL(R_arave,4),I_ekex,REAL(R_edex,4),
     & REAL(R_idex,4),REAL(R_ibex,4),
     & REAL(R_ebex,4),REAL(R_afex,4),L_efex,REAL(R_ifex,4
     &),L_ofex,
     & L_ufex,R_odex,REAL(R_adex,4),REAL(R_ubex,4),L_akex
     &)
      !}
C FDA60b1_vlv.fgi(  75, 290):���������� ������� ��� 2,20FDA63CP012XQ01
      R_irave = R8_orave
C FDA60_sens.fgi( 139, 229):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_axex,R_udix,REAL(1,4
     &),
     & REAL(R_irave,4),I_odix,REAL(R_oxex,4),
     & REAL(R_uxex,4),REAL(R_uvex,4),
     & REAL(R_ovex,4),REAL(R_ibix,4),L_obix,REAL(R_ubix,4
     &),L_adix,
     & L_edix,R_abix,REAL(R_ixex,4),REAL(R_exex,4),L_idix
     &)
      !}
C FDA60b1_vlv.fgi(  48, 290):���������� ������� ��� 2,20FDA62CP012XQ01
      R_urave = R8_asave
C FDA60_sens.fgi( 139, 232):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itix,R_ebox,REAL(1,4
     &),
     & REAL(R_urave,4),I_abox,REAL(R_avix,4),
     & REAL(R_evix,4),REAL(R_etix,4),
     & REAL(R_atix,4),REAL(R_uvix,4),L_axix,REAL(R_exix,4
     &),L_ixix,
     & L_oxix,R_ivix,REAL(R_utix,4),REAL(R_otix,4),L_uxix
     &)
      !}
C FDA60b1_vlv.fgi(  21, 290):���������� ������� ��� 2,20FDA61CP012XQ01
      R_esave = R8_isave
C FDA60_sens.fgi( 139, 235):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uvuv,R_odax,REAL(1,4
     &),
     & REAL(R_esave,4),I_idax,REAL(R_ixuv,4),
     & REAL(R_oxuv,4),REAL(R_ovuv,4),
     & REAL(R_ivuv,4),REAL(R_ebax,4),L_ibax,REAL(R_obax,4
     &),L_ubax,
     & L_adax,R_uxuv,REAL(R_exuv,4),REAL(R_axuv,4),L_edax
     &)
      !}
C FDA60b1_vlv.fgi( 130, 321):���������� ������� ��� 2,20FDA65CT008XQ01
      R_osave = R8_usave
C FDA60_sens.fgi( 139, 238):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_etax,R_abex,REAL(1,4
     &),
     & REAL(R_osave,4),I_uxax,REAL(R_utax,4),
     & REAL(R_avax,4),REAL(R_atax,4),
     & REAL(R_usax,4),REAL(R_ovax,4),L_uvax,REAL(R_axax,4
     &),L_exax,
     & L_ixax,R_evax,REAL(R_otax,4),REAL(R_itax,4),L_oxax
     &)
      !}
C FDA60b1_vlv.fgi( 103, 321):���������� ������� ��� 2,20FDA64CT008XQ01
      R_atave = R8_etave
C FDA60_sens.fgi( 139, 241):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_orex,R_ivex,REAL(1,4
     &),
     & REAL(R_atave,4),I_evex,REAL(R_esex,4),
     & REAL(R_isex,4),REAL(R_irex,4),
     & REAL(R_erex,4),REAL(R_atex,4),L_etex,REAL(R_itex,4
     &),L_otex,
     & L_utex,R_osex,REAL(R_asex,4),REAL(R_urex,4),L_avex
     &)
      !}
C FDA60b1_vlv.fgi(  75, 321):���������� ������� ��� 2,20FDA63CT008XQ01
      R_itave = R8_otave
C FDA60_sens.fgi( 139, 244):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_apix,R_usix,REAL(1,4
     &),
     & REAL(R_itave,4),I_osix,REAL(R_opix,4),
     & REAL(R_upix,4),REAL(R_umix,4),
     & REAL(R_omix,4),REAL(R_irix,4),L_orix,REAL(R_urix,4
     &),L_asix,
     & L_esix,R_arix,REAL(R_ipix,4),REAL(R_epix,4),L_isix
     &)
      !}
C FDA60b1_vlv.fgi(  48, 321):���������� ������� ��� 2,20FDA62CT008XQ01
      R_utave = R8_avave
C FDA60_sens.fgi( 139, 247):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ilox,R_erox,REAL(1,4
     &),
     & REAL(R_utave,4),I_arox,REAL(R_amox,4),
     & REAL(R_emox,4),REAL(R_elox,4),
     & REAL(R_alox,4),REAL(R_umox,4),L_apox,REAL(R_epox,4
     &),L_ipox,
     & L_opox,R_imox,REAL(R_ulox,4),REAL(R_olox,4),L_upox
     &)
      !}
C FDA60b1_vlv.fgi(  21, 321):���������� ������� ��� 2,20FDA61CT008XQ01
      R_evave = R8_ivave
C FDA60_sens.fgi( 139, 250):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_utuv,4),REAL
     &(R_evave,4),R_eruv,
     & R_avuv,REAL(1,4),REAL(R_uruv,4),
     & REAL(R_asuv,4),REAL(R_aruv,4),
     & REAL(R_upuv,4),I_otuv,REAL(R_isuv,4),L_osuv,
     & REAL(R_usuv,4),L_atuv,L_etuv,R_esuv,REAL(R_oruv,4)
     &,REAL(R_iruv,4),L_ituv)
      !}
C FDA60b1_vlv.fgi( 130, 305):���������� ������� ��������,20FDA65CP011XQ01
      R_ovave = R8_uvave
C FDA60_sens.fgi( 139, 253):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_esax,4),REAL
     &(R_ovave,4),R_omax,
     & R_isax,REAL(1,4),REAL(R_epax,4),
     & REAL(R_ipax,4),REAL(R_imax,4),
     & REAL(R_emax,4),I_asax,REAL(R_upax,4),L_arax,
     & REAL(R_erax,4),L_irax,L_orax,R_opax,REAL(R_apax,4)
     &,REAL(R_umax,4),L_urax)
      !}
C FDA60b1_vlv.fgi( 103, 305):���������� ������� ��������,20FDA64CP011XQ01
      R_axave = R8_exave
C FDA60_sens.fgi( 139, 256):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_opex,4),REAL
     &(R_axave,4),R_alex,
     & R_upex,REAL(1,4),REAL(R_olex,4),
     & REAL(R_ulex,4),REAL(R_ukex,4),
     & REAL(R_okex,4),I_ipex,REAL(R_emex,4),L_imex,
     & REAL(R_omex,4),L_umex,L_apex,R_amex,REAL(R_ilex,4)
     &,REAL(R_elex,4),L_epex)
      !}
C FDA60b1_vlv.fgi(  75, 305):���������� ������� ��������,20FDA63CP011XQ01
      R_ixave = R8_oxave
C FDA60_sens.fgi( 139, 259):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_amix,4),REAL
     &(R_ixave,4),R_ifix,
     & R_emix,REAL(1,4),REAL(R_akix,4),
     & REAL(R_ekix,4),REAL(R_efix,4),
     & REAL(R_afix,4),I_ulix,REAL(R_okix,4),L_ukix,
     & REAL(R_alix,4),L_elix,L_ilix,R_ikix,REAL(R_ufix,4)
     &,REAL(R_ofix,4),L_olix)
      !}
C FDA60b1_vlv.fgi(  48, 305):���������� ������� ��������,20FDA62CP011XQ01
      R_uxave = R8_abeve
C FDA60_sens.fgi( 139, 262):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ikox,4),REAL
     &(R_uxave,4),R_ubox,
     & R_okox,REAL(1,4),REAL(R_idox,4),
     & REAL(R_odox,4),REAL(R_obox,4),
     & REAL(R_ibox,4),I_ekox,REAL(R_afox,4),L_efox,
     & REAL(R_ifox,4),L_ofox,L_ufox,R_udox,REAL(R_edox,4)
     &,REAL(R_adox,4),L_akox)
      !}
C FDA60b1_vlv.fgi(  21, 305):���������� ������� ��������,20FDA61CP011XQ01
      R_ebeve = R8_ibeve
C FDA60_sens.fgi( 139, 265):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ufom,R_omom,REAL(1,4
     &),
     & REAL(R_ebeve,4),I_imom,REAL(R_ikom,4),
     & REAL(R_okom,4),REAL(R_ofom,4),
     & REAL(R_ifom,4),REAL(R_elom,4),L_ilom,REAL(R_olom,4
     &),L_ulom,
     & L_amom,R_ukom,REAL(R_ekom,4),REAL(R_akom,4),L_emom
     &)
      !}
C FDA60b1_vlv.fgi( 131,  61):���������� ������� ��� 2,20FDA65CP017XQ01
      R_obeve = R8_ubeve
C FDA60_sens.fgi( 139, 268):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_epom,R_atom,REAL(1,4
     &),
     & REAL(R_obeve,4),I_usom,REAL(R_upom,4),
     & REAL(R_arom,4),REAL(R_apom,4),
     & REAL(R_umom,4),REAL(R_orom,4),L_urom,REAL(R_asom,4
     &),L_esom,
     & L_isom,R_erom,REAL(R_opom,4),REAL(R_ipom,4),L_osom
     &)
      !}
C FDA60b1_vlv.fgi( 103,  61):���������� ������� ��� 2,20FDA64CP017XQ01
      R_adeve = R8_edeve
C FDA60_sens.fgi( 139, 271):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_otom,R_ibum,REAL(1,4
     &),
     & REAL(R_adeve,4),I_ebum,REAL(R_evom,4),
     & REAL(R_ivom,4),REAL(R_itom,4),
     & REAL(R_etom,4),REAL(R_axom,4),L_exom,REAL(R_ixom,4
     &),L_oxom,
     & L_uxom,R_ovom,REAL(R_avom,4),REAL(R_utom,4),L_abum
     &)
      !}
C FDA60b1_vlv.fgi(  75,  61):���������� ������� ��� 2,20FDA63CP017XQ01
      R_ideve = R8_odeve
C FDA60_sens.fgi( 139, 274):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_adum,R_ukum,REAL(1,4
     &),
     & REAL(R_ideve,4),I_okum,REAL(R_odum,4),
     & REAL(R_udum,4),REAL(R_ubum,4),
     & REAL(R_obum,4),REAL(R_ifum,4),L_ofum,REAL(R_ufum,4
     &),L_akum,
     & L_ekum,R_afum,REAL(R_idum,4),REAL(R_edum,4),L_ikum
     &)
      !}
C FDA60b1_vlv.fgi(  47,  61):���������� ������� ��� 2,20FDA62CP017XQ01
      R_udeve = R8_afeve
C FDA60_sens.fgi( 139, 277):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ilum,R_erum,REAL(1,4
     &),
     & REAL(R_udeve,4),I_arum,REAL(R_amum,4),
     & REAL(R_emum,4),REAL(R_elum,4),
     & REAL(R_alum,4),REAL(R_umum,4),L_apum,REAL(R_epum,4
     &),L_ipum,
     & L_opum,R_imum,REAL(R_ulum,4),REAL(R_olum,4),L_upum
     &)
      !}
C FDA60b1_vlv.fgi(  19,  61):���������� ������� ��� 2,20FDA61CP017XQ01
      R_efeve = R8_ifeve
C FDA60_sens.fgi( 139, 281):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ibaf,R_ekaf,REAL(1e
     &-4,4),
     & REAL(R_efeve,4),I_akaf,REAL(R_adaf,4),
     & REAL(R_edaf,4),REAL(R_ebaf,4),
     & REAL(R_abaf,4),REAL(R_udaf,4),L_afaf,REAL(R_efaf,4
     &),L_ifaf,
     & L_ofaf,R_idaf,REAL(R_ubaf,4),REAL(R_obaf,4),L_ufaf
     &)
      !}
C FDA60b1_vlv.fgi( 333, 141):���������� ������� ��� 2,20FDA65CQ007XQ01
      R_ofeve = R8_ufeve
C FDA60_sens.fgi( 218, 231):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_axaf,R_udef,REAL(1e
     &-4,4),
     & REAL(R_ofeve,4),I_odef,REAL(R_oxaf,4),
     & REAL(R_uxaf,4),REAL(R_uvaf,4),
     & REAL(R_ovaf,4),REAL(R_ibef,4),L_obef,REAL(R_ubef,4
     &),L_adef,
     & L_edef,R_abef,REAL(R_ixaf,4),REAL(R_exaf,4),L_idef
     &)
      !}
C FDA60b1_vlv.fgi( 305, 141):���������� ������� ��� 2,20FDA64CQ007XQ01
      R_akeve = R8_ekeve
C FDA60_sens.fgi( 218, 241):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_otef,R_ibif,REAL(1e
     &-4,4),
     & REAL(R_akeve,4),I_ebif,REAL(R_evef,4),
     & REAL(R_ivef,4),REAL(R_itef,4),
     & REAL(R_etef,4),REAL(R_axef,4),L_exef,REAL(R_ixef,4
     &),L_oxef,
     & L_uxef,R_ovef,REAL(R_avef,4),REAL(R_utef,4),L_abif
     &)
      !}
C FDA60b1_vlv.fgi( 277, 141):���������� ������� ��� 2,20FDA63CQ007XQ01
      R_ikeve = R8_okeve
C FDA60_sens.fgi( 218, 251):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_esif,R_axif,REAL(1e
     &-4,4),
     & REAL(R_ikeve,4),I_uvif,REAL(R_usif,4),
     & REAL(R_atif,4),REAL(R_asif,4),
     & REAL(R_urif,4),REAL(R_otif,4),L_utif,REAL(R_avif,4
     &),L_evif,
     & L_ivif,R_etif,REAL(R_osif,4),REAL(R_isif,4),L_ovif
     &)
      !}
C FDA60b1_vlv.fgi( 249, 141):���������� ������� ��� 2,20FDA62CQ007XQ01
      R_ukeve = R8_aleve
C FDA60_sens.fgi( 218, 261):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_upof,R_otof,REAL(1e
     &-4,4),
     & REAL(R_ukeve,4),I_itof,REAL(R_irof,4),
     & REAL(R_orof,4),REAL(R_opof,4),
     & REAL(R_ipof,4),REAL(R_esof,4),L_isof,REAL(R_osof,4
     &),L_usof,
     & L_atof,R_urof,REAL(R_erof,4),REAL(R_arof,4),L_etof
     &)
      !}
C FDA60b1_vlv.fgi( 221, 141):���������� ������� ��� 2,20FDA61CQ007XQ01
      R_eleve = R8_ileve
C FDA60_sens.fgi( 218, 275):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ifip,R_emip,REAL(1,4
     &),
     & REAL(R_eleve,4),I_amip,REAL(R_akip,4),
     & REAL(R_ekip,4),REAL(R_efip,4),
     & REAL(R_afip,4),REAL(R_ukip,4),L_alip,REAL(R_elip,4
     &),L_ilip,
     & L_olip,R_ikip,REAL(R_ufip,4),REAL(R_ofip,4),L_ulip
     &)
      !}
C FDA60b1_vlv.fgi( 130, 106):���������� ������� ��� 2,20FDA65CT014XQ01
      R_oleve = R8_uleve
C FDA60_sens.fgi( 218, 278):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_etip,R_abop,REAL(1,4
     &),
     & REAL(R_oleve,4),I_uxip,REAL(R_utip,4),
     & REAL(R_avip,4),REAL(R_atip,4),
     & REAL(R_usip,4),REAL(R_ovip,4),L_uvip,REAL(R_axip,4
     &),L_exip,
     & L_ixip,R_evip,REAL(R_otip,4),REAL(R_itip,4),L_oxip
     &)
      !}
C FDA60b1_vlv.fgi( 102, 106):���������� ������� ��� 2,20FDA64CT014XQ01
      R_ameve = R8_emeve
C FDA60_sens.fgi( 218, 281):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_alop,R_upop,REAL(1,4
     &),
     & REAL(R_ameve,4),I_opop,REAL(R_olop,4),
     & REAL(R_ulop,4),REAL(R_ukop,4),
     & REAL(R_okop,4),REAL(R_imop,4),L_omop,REAL(R_umop,4
     &),L_apop,
     & L_epop,R_amop,REAL(R_ilop,4),REAL(R_elop,4),L_ipop
     &)
      !}
C FDA60b1_vlv.fgi(  76, 106):���������� ������� ��� 2,20FDA63CT014XQ01
      R_imeve = R8_omeve
C FDA60_sens.fgi( 218, 284):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uvop,R_odup,REAL(1,4
     &),
     & REAL(R_imeve,4),I_idup,REAL(R_ixop,4),
     & REAL(R_oxop,4),REAL(R_ovop,4),
     & REAL(R_ivop,4),REAL(R_ebup,4),L_ibup,REAL(R_obup,4
     &),L_ubup,
     & L_adup,R_uxop,REAL(R_exop,4),REAL(R_axop,4),L_edup
     &)
      !}
C FDA60b1_vlv.fgi(  48, 106):���������� ������� ��� 2,20FDA62CT014XQ01
      R_umeve = R8_apeve
C FDA60_sens.fgi( 218, 287):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_omup,R_isup,REAL(1,4
     &),
     & REAL(R_umeve,4),I_esup,REAL(R_epup,4),
     & REAL(R_ipup,4),REAL(R_imup,4),
     & REAL(R_emup,4),REAL(R_arup,4),L_erup,REAL(R_irup,4
     &),L_orup,
     & L_urup,R_opup,REAL(R_apup,4),REAL(R_umup,4),L_asup
     &)
      !}
C FDA60b1_vlv.fgi(  20, 106):���������� ������� ��� 2,20FDA61CT014XQ01
      R_(13) = 60
C FDA60_sens.fgi(  53, 221):��������� (RE4) (�������)
      R_oseve = R8_useve * R_(13)
C FDA60_sens.fgi(  56, 223):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_elep,R_arep,REAL(1,4
     &),
     & REAL(R_oseve,4),I_upep,REAL(R_ulep,4),
     & REAL(R_amep,4),REAL(R_alep,4),
     & REAL(R_ukep,4),REAL(R_omep,4),L_umep,REAL(R_apep,4
     &),L_epep,
     & L_ipep,R_emep,REAL(R_olep,4),REAL(R_ilep,4),L_opep
     &)
      !}
C FDA60b1_vlv.fgi( 158,  91):���������� ������� ��� 2,20FDA66CF019XQ01
      R_(14) = 60
C FDA60_sens.fgi(  53, 225):��������� (RE4) (�������)
      R_ateve = R8_eteve * R_(14)
C FDA60_sens.fgi(  56, 227):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_axep,R_udip,REAL(1,4
     &),
     & REAL(R_ateve,4),I_odip,REAL(R_oxep,4),
     & REAL(R_uxep,4),REAL(R_uvep,4),
     & REAL(R_ovep,4),REAL(R_ibip,4),L_obip,REAL(R_ubip,4
     &),L_adip,
     & L_edip,R_abip,REAL(R_ixep,4),REAL(R_exep,4),L_idip
     &)
      !}
C FDA60b1_vlv.fgi( 130,  91):���������� ������� ��� 2,20FDA65CF019XQ01
      R_(15) = 60
C FDA60_sens.fgi(  53, 229):��������� (RE4) (�������)
      R_iteve = R8_oteve * R_(15)
C FDA60_sens.fgi(  56, 231):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umip,R_osip,REAL(1,4
     &),
     & REAL(R_iteve,4),I_isip,REAL(R_ipip,4),
     & REAL(R_opip,4),REAL(R_omip,4),
     & REAL(R_imip,4),REAL(R_erip,4),L_irip,REAL(R_orip,4
     &),L_urip,
     & L_asip,R_upip,REAL(R_epip,4),REAL(R_apip,4),L_esip
     &)
      !}
C FDA60b1_vlv.fgi( 102,  91):���������� ������� ��� 2,20FDA64CF019XQ01
      R_(16) = 60
C FDA60_sens.fgi(  53, 233):��������� (RE4) (�������)
      R_uteve = R8_aveve * R_(16)
C FDA60_sens.fgi(  56, 235):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obop,R_ikop,REAL(1,4
     &),
     & REAL(R_uteve,4),I_ekop,REAL(R_edop,4),
     & REAL(R_idop,4),REAL(R_ibop,4),
     & REAL(R_ebop,4),REAL(R_afop,4),L_efop,REAL(R_ifop,4
     &),L_ofop,
     & L_ufop,R_odop,REAL(R_adop,4),REAL(R_ubop,4),L_akop
     &)
      !}
C FDA60b1_vlv.fgi(  76,  91):���������� ������� ��� 2,20FDA63CF019XQ01
      R_(17) = 60
C FDA60_sens.fgi(  53, 237):��������� (RE4) (�������)
      R_eveve = R8_iveve * R_(17)
C FDA60_sens.fgi(  56, 239):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_irop,R_evop,REAL(1,4
     &),
     & REAL(R_eveve,4),I_avop,REAL(R_asop,4),
     & REAL(R_esop,4),REAL(R_erop,4),
     & REAL(R_arop,4),REAL(R_usop,4),L_atop,REAL(R_etop,4
     &),L_itop,
     & L_otop,R_isop,REAL(R_urop,4),REAL(R_orop,4),L_utop
     &)
      !}
C FDA60b1_vlv.fgi(  48,  91):���������� ������� ��� 2,20FDA62CF019XQ01
      R_(18) = 60
C FDA60_sens.fgi(  53, 241):��������� (RE4) (�������)
      R_oveve = R8_uveve * R_(18)
C FDA60_sens.fgi(  56, 243):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efup,R_amup,REAL(1,4
     &),
     & REAL(R_oveve,4),I_ulup,REAL(R_ufup,4),
     & REAL(R_akup,4),REAL(R_afup,4),
     & REAL(R_udup,4),REAL(R_okup,4),L_ukup,REAL(R_alup,4
     &),L_elup,
     & L_ilup,R_ekup,REAL(R_ofup,4),REAL(R_ifup,4),L_olup
     &)
      !}
C FDA60b1_vlv.fgi(  20,  91):���������� ������� ��� 2,20FDA61CF019XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ixeve,R_ifive,REAL(1
     &,4),
     & REAL(R_obive,4),I_efive,REAL(R_abive,4),
     & REAL(R_ebive,4),REAL(R_exeve,4),
     & REAL(R_axeve,4),REAL(R_adive,4),L_edive,REAL(R_idive
     &,4),L_odive,
     & L_udive,R_ibive,REAL(R_uxeve,4),REAL(R_oxeve,4),L_afive
     &)
      !}
C FDA60b_vlv.fgi( 346, 429):���������� ������� ��� 2,20FDA60CQ008XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_akive,R_apive,REAL(1
     &,4),
     & REAL(R_elive,4),I_umive,REAL(R_okive,4),
     & REAL(R_ukive,4),REAL(R_ufive,4),
     & REAL(R_ofive,4),REAL(R_olive,4),L_ulive,REAL(R_amive
     &,4),L_emive,
     & L_imive,R_alive,REAL(R_ikive,4),REAL(R_ekive,4),L_omive
     &)
      !}
C FDA60b_vlv.fgi( 346, 445):���������� ������� ��� 2,20FDA60CM008XQ01
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_itive,4),REAL
     &(R_urive,4),R_opive,
     & R_otive,REAL(1,4),REAL(R_erive,4),
     & REAL(R_irive,4),REAL(R_ipive,4),
     & REAL(R_epive,4),I_etive,REAL(R_asive,4),L_esive,
     & REAL(R_isive,4),L_osive,L_usive,R_orive,REAL(R_arive
     &,4),REAL(R_upive,4),L_ative)
      !}
C FDA60b_vlv.fgi( 404, 477):���������� ������� ��������,20FDA60CP031XQ01
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_edove,4),REAL
     &(R_oxive,4),R_ivive,
     & R_idove,REAL(1,4),REAL(R_axive,4),
     & REAL(R_exive,4),REAL(R_evive,4),
     & REAL(R_avive,4),I_adove,REAL(R_uxive,4),L_above,
     & REAL(R_ebove,4),L_ibove,L_obove,R_ixive,REAL(R_uvive
     &,4),REAL(R_ovive,4),L_ubove)
      !}
C FDA60b_vlv.fgi( 404, 462):���������� ������� ��������,20FDA60CP029XQ01
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_amove,4),REAL
     &(R_ikove,4),R_efove,
     & R_emove,REAL(1.e-3,4),REAL(R_ufove,4),
     & REAL(R_akove,4),REAL(R_afove,4),
     & REAL(R_udove,4),I_ulove,REAL(R_okove,4),L_ukove,
     & REAL(R_alove,4),L_elove,L_ilove,R_ekove,REAL(R_ofove
     &,4),REAL(R_ifove,4),L_olove)
      !}
C FDA60b_vlv.fgi( 492, 493):���������� ������� ��������,20FDA60CP007XQ01
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_usove,4),REAL
     &(R_erove,4),R_apove,
     & R_atove,REAL(1.e-3,4),REAL(R_opove,4),
     & REAL(R_upove,4),REAL(R_umove,4),
     & REAL(R_omove,4),I_osove,REAL(R_irove,4),L_orove,
     & REAL(R_urove,4),L_asove,L_esove,R_arove,REAL(R_ipove
     &,4),REAL(R_epove,4),L_isove)
      !}
C FDA60b_vlv.fgi( 492, 509):���������� ������� ��������,20FDA60CP006XQ01
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_obuve,4),REAL
     &(R_axove,4),R_utove,
     & R_ubuve,REAL(1.e-3,4),REAL(R_ivove,4),
     & REAL(R_ovove,4),REAL(R_otove,4),
     & REAL(R_itove,4),I_ibuve,REAL(R_exove,4),L_ixove,
     & REAL(R_oxove,4),L_uxove,L_abuve,R_uvove,REAL(R_evove
     &,4),REAL(R_avove,4),L_ebuve)
      !}
C FDA60b_vlv.fgi( 492, 524):���������� ������� ��������,20FDA60CP005XQ01
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_iluve,4),REAL
     &(R_ufuve,4),R_oduve,
     & R_oluve,REAL(1.e-6,4),REAL(R_efuve,4),
     & REAL(R_ifuve,4),REAL(R_iduve,4),
     & REAL(R_eduve,4),I_eluve,REAL(R_akuve,4),L_ekuve,
     & REAL(R_ikuve,4),L_okuve,L_ukuve,R_ofuve,REAL(R_afuve
     &,4),REAL(R_uduve,4),L_aluve)
      !}
C FDA60b_vlv.fgi( 465, 492):���������� ������� ��������,20FDA60CP003XQ01
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_esuve,4),REAL
     &(R_opuve,4),R_imuve,
     & R_isuve,REAL(1.e-6,4),REAL(R_apuve,4),
     & REAL(R_epuve,4),REAL(R_emuve,4),
     & REAL(R_amuve,4),I_asuve,REAL(R_upuve,4),L_aruve,
     & REAL(R_eruve,4),L_iruve,L_oruve,R_ipuve,REAL(R_umuve
     &,4),REAL(R_omuve,4),L_uruve)
      !}
C FDA60b_vlv.fgi( 465, 508):���������� ������� ��������,20FDA60CP002XQ01
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_abaxe,4),REAL
     &(R_ivuve,4),R_etuve,
     & R_ebaxe,REAL(1.e-6,4),REAL(R_utuve,4),
     & REAL(R_avuve,4),REAL(R_atuve,4),
     & REAL(R_usuve,4),I_uxuve,REAL(R_ovuve,4),L_uvuve,
     & REAL(R_axuve,4),L_exuve,L_ixuve,R_evuve,REAL(R_otuve
     &,4),REAL(R_ituve,4),L_oxuve)
      !}
C FDA60b_vlv.fgi( 465, 524):���������� ������� ��������,20FDA60CP001XQ01
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ukaxe,4),REAL
     &(R_efaxe,4),R_adaxe,
     & R_alaxe,REAL(1,4),REAL(R_odaxe,4),
     & REAL(R_udaxe,4),REAL(R_ubaxe,4),
     & REAL(R_obaxe,4),I_okaxe,REAL(R_ifaxe,4),L_ofaxe,
     & REAL(R_ufaxe,4),L_akaxe,L_ekaxe,R_afaxe,REAL(R_idaxe
     &,4),REAL(R_edaxe,4),L_ikaxe)
      !}
C FDA60b_vlv.fgi( 317, 397):���������� ������� ��������,20FDA60CP025XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ulaxe,R_uraxe,REAL(1
     &,4),
     & REAL(R_apaxe,4),I_oraxe,REAL(R_imaxe,4),
     & REAL(R_omaxe,4),REAL(R_olaxe,4),
     & REAL(R_ilaxe,4),REAL(R_ipaxe,4),L_opaxe,REAL(R_upaxe
     &,4),L_araxe,
     & L_eraxe,R_umaxe,REAL(R_emaxe,4),REAL(R_amaxe,4),L_iraxe
     &)
      !}
C FDA60b_vlv.fgi(  50, 477):���������� ������� ��� 2,20FDA60DF005ZQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_isaxe,R_ixaxe,REAL(1
     &,4),
     & REAL(R_otaxe,4),I_exaxe,REAL(R_ataxe,4),
     & REAL(R_etaxe,4),REAL(R_esaxe,4),
     & REAL(R_asaxe,4),REAL(R_avaxe,4),L_evaxe,REAL(R_ivaxe
     &,4),L_ovaxe,
     & L_uvaxe,R_itaxe,REAL(R_usaxe,4),REAL(R_osaxe,4),L_axaxe
     &)
      !}
C FDA60b_vlv.fgi(  50, 462):���������� ������� ��� 2,20FDA60DF006ZQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_abexe,R_akexe,REAL(1
     &,4),
     & REAL(R_edexe,4),I_ufexe,REAL(R_obexe,4),
     & REAL(R_ubexe,4),REAL(R_uxaxe,4),
     & REAL(R_oxaxe,4),REAL(R_odexe,4),L_udexe,REAL(R_afexe
     &,4),L_efexe,
     & L_ifexe,R_adexe,REAL(R_ibexe,4),REAL(R_ebexe,4),L_ofexe
     &)
      !}
C FDA60b_vlv.fgi(  50, 492):���������� ������� ��� 2,20FDA60DF004ZQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_okexe,R_opexe,REAL(1
     &,4),
     & REAL(R_ulexe,4),I_ipexe,REAL(R_elexe,4),
     & REAL(R_ilexe,4),REAL(R_ikexe,4),
     & REAL(R_ekexe,4),REAL(R_emexe,4),L_imexe,REAL(R_omexe
     &,4),L_umexe,
     & L_apexe,R_olexe,REAL(R_alexe,4),REAL(R_ukexe,4),L_epexe
     &)
      !}
C FDA60b_vlv.fgi(  20, 477):���������� ������� ��� 2,20FDA60DF002ZQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_erexe,R_evexe,REAL(1
     &,4),
     & REAL(R_isexe,4),I_avexe,REAL(R_urexe,4),
     & REAL(R_asexe,4),REAL(R_arexe,4),
     & REAL(R_upexe,4),REAL(R_usexe,4),L_atexe,REAL(R_etexe
     &,4),L_itexe,
     & L_otexe,R_esexe,REAL(R_orexe,4),REAL(R_irexe,4),L_utexe
     &)
      !}
C FDA60b_vlv.fgi(  20, 462):���������� ������� ��� 2,20FDA60DF001ZQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uvexe,R_udixe,REAL(1
     &,4),
     & REAL(R_abixe,4),I_odixe,REAL(R_ixexe,4),
     & REAL(R_oxexe,4),REAL(R_ovexe,4),
     & REAL(R_ivexe,4),REAL(R_ibixe,4),L_obixe,REAL(R_ubixe
     &,4),L_adixe,
     & L_edixe,R_uxexe,REAL(R_exexe,4),REAL(R_axexe,4),L_idixe
     &)
      !}
C FDA60b_vlv.fgi(  20, 492):���������� ������� ��� 2,20FDA60DF003ZQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ifixe,R_imixe,REAL(1
     &,4),
     & REAL(R_okixe,4),I_emixe,REAL(R_akixe,4),
     & REAL(R_ekixe,4),REAL(R_efixe,4),
     & REAL(R_afixe,4),REAL(R_alixe,4),L_elixe,REAL(R_ilixe
     &,4),L_olixe,
     & L_ulixe,R_ikixe,REAL(R_ufixe,4),REAL(R_ofixe,4),L_amixe
     &)
      !}
C FDA60b_vlv.fgi( 202, 523):���������� ������� ��� 2,20FDA60CM004XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_apixe,R_atixe,REAL(1
     &,4),
     & REAL(R_erixe,4),I_usixe,REAL(R_opixe,4),
     & REAL(R_upixe,4),REAL(R_umixe,4),
     & REAL(R_omixe,4),REAL(R_orixe,4),L_urixe,REAL(R_asixe
     &,4),L_esixe,
     & L_isixe,R_arixe,REAL(R_ipixe,4),REAL(R_epixe,4),L_osixe
     &)
      !}
C FDA60b_vlv.fgi( 144, 523):���������� ������� ��� 2,20FDA60CT001XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_otixe,R_oboxe,REAL(1
     &,4),
     & REAL(R_uvixe,4),I_iboxe,REAL(R_evixe,4),
     & REAL(R_ivixe,4),REAL(R_itixe,4),
     & REAL(R_etixe,4),REAL(R_exixe,4),L_ixixe,REAL(R_oxixe
     &,4),L_uxixe,
     & L_aboxe,R_ovixe,REAL(R_avixe,4),REAL(R_utixe,4),L_eboxe
     &)
      !}
C FDA60b_vlv.fgi( 173, 523):���������� ������� ��� 2,20FDA60CQ004XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_edoxe,R_aloxe,REAL(1,4
     &),
     & REAL(R_udoxe,4),REAL(R_afoxe,4),
     & REAL(R_adoxe,4),REAL(R_uboxe,4),I_ukoxe,
     & REAL(R_ofoxe,4),L_ufoxe,REAL(R_akoxe,4),L_ekoxe,L_ikoxe
     &,R_efoxe,
     & REAL(R_odoxe,4),REAL(R_idoxe,4),L_okoxe,REAL(R_ifoxe
     &,4))
      !}
C FDA60b_vlv.fgi( 144, 507):���������� �������,20FDA60CW001ZQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_oloxe,R_oroxe,REAL(1
     &,4),
     & REAL(R_umoxe,4),I_iroxe,REAL(R_emoxe,4),
     & REAL(R_imoxe,4),REAL(R_iloxe,4),
     & REAL(R_eloxe,4),REAL(R_epoxe,4),L_ipoxe,REAL(R_opoxe
     &,4),L_upoxe,
     & L_aroxe,R_omoxe,REAL(R_amoxe,4),REAL(R_uloxe,4),L_eroxe
     &)
      !}
C FDA60b_vlv.fgi( 822, 237):���������� ������� ��� 2,20FDA66CP024XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_esoxe,R_exoxe,REAL(1
     &,4),
     & REAL(R_itoxe,4),I_axoxe,REAL(R_usoxe,4),
     & REAL(R_atoxe,4),REAL(R_asoxe,4),
     & REAL(R_uroxe,4),REAL(R_utoxe,4),L_avoxe,REAL(R_evoxe
     &,4),L_ivoxe,
     & L_ovoxe,R_etoxe,REAL(R_osoxe,4),REAL(R_isoxe,4),L_uvoxe
     &)
      !}
C FDA60b_vlv.fgi( 822, 252):���������� ������� ��� 2,20FDA66CP023XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uxoxe,R_ufuxe,REAL(1
     &,4),
     & REAL(R_aduxe,4),I_ofuxe,REAL(R_ibuxe,4),
     & REAL(R_obuxe,4),REAL(R_oxoxe,4),
     & REAL(R_ixoxe,4),REAL(R_iduxe,4),L_oduxe,REAL(R_uduxe
     &,4),L_afuxe,
     & L_efuxe,R_ubuxe,REAL(R_ebuxe,4),REAL(R_abuxe,4),L_ifuxe
     &)
      !}
C FDA60b_vlv.fgi( 822, 177):���������� ������� ��� 2,20FDA66CF028XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ikuxe,R_ipuxe,REAL(1
     &,4),
     & REAL(R_oluxe,4),I_epuxe,REAL(R_aluxe,4),
     & REAL(R_eluxe,4),REAL(R_ekuxe,4),
     & REAL(R_akuxe,4),REAL(R_amuxe,4),L_emuxe,REAL(R_imuxe
     &,4),L_omuxe,
     & L_umuxe,R_iluxe,REAL(R_ukuxe,4),REAL(R_okuxe,4),L_apuxe
     &)
      !}
C FDA60b_vlv.fgi( 822, 192):���������� ������� ��� 2,20FDA66CF026XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_aruxe,R_avuxe,REAL(1
     &,4),
     & REAL(R_esuxe,4),I_utuxe,REAL(R_oruxe,4),
     & REAL(R_uruxe,4),REAL(R_upuxe,4),
     & REAL(R_opuxe,4),REAL(R_osuxe,4),L_usuxe,REAL(R_atuxe
     &,4),L_etuxe,
     & L_ituxe,R_asuxe,REAL(R_iruxe,4),REAL(R_eruxe,4),L_otuxe
     &)
      !}
C FDA60b_vlv.fgi( 822, 207):���������� ������� ��� 2,20FDA66CF029XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovuxe,R_odabi,REAL(1
     &,4),
     & REAL(R_uxuxe,4),I_idabi,REAL(R_exuxe,4),
     & REAL(R_ixuxe,4),REAL(R_ivuxe,4),
     & REAL(R_evuxe,4),REAL(R_ebabi,4),L_ibabi,REAL(R_obabi
     &,4),L_ubabi,
     & L_adabi,R_oxuxe,REAL(R_axuxe,4),REAL(R_uvuxe,4),L_edabi
     &)
      !}
C FDA60b_vlv.fgi( 822, 222):���������� ������� ��� 2,20FDA66CF027XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efabi,R_emabi,REAL(1
     &,4),
     & REAL(R_ikabi,4),I_amabi,REAL(R_ufabi,4),
     & REAL(R_akabi,4),REAL(R_afabi,4),
     & REAL(R_udabi,4),REAL(R_ukabi,4),L_alabi,REAL(R_elabi
     &,4),L_ilabi,
     & L_olabi,R_ekabi,REAL(R_ofabi,4),REAL(R_ifabi,4),L_ulabi
     &)
      !}
C FDA60b_vlv.fgi( 854, 237):���������� ������� ��� 2,20FDA65CP024XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umabi,R_usabi,REAL(1
     &,4),
     & REAL(R_arabi,4),I_osabi,REAL(R_ipabi,4),
     & REAL(R_opabi,4),REAL(R_omabi,4),
     & REAL(R_imabi,4),REAL(R_irabi,4),L_orabi,REAL(R_urabi
     &,4),L_asabi,
     & L_esabi,R_upabi,REAL(R_epabi,4),REAL(R_apabi,4),L_isabi
     &)
      !}
C FDA60b_vlv.fgi( 854, 252):���������� ������� ��� 2,20FDA65CP023XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itabi,R_ibebi,REAL(1
     &,4),
     & REAL(R_ovabi,4),I_ebebi,REAL(R_avabi,4),
     & REAL(R_evabi,4),REAL(R_etabi,4),
     & REAL(R_atabi,4),REAL(R_axabi,4),L_exabi,REAL(R_ixabi
     &,4),L_oxabi,
     & L_uxabi,R_ivabi,REAL(R_utabi,4),REAL(R_otabi,4),L_abebi
     &)
      !}
C FDA60b_vlv.fgi( 854, 177):���������� ������� ��� 2,20FDA65CF028XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_adebi,R_alebi,REAL(1
     &,4),
     & REAL(R_efebi,4),I_ukebi,REAL(R_odebi,4),
     & REAL(R_udebi,4),REAL(R_ubebi,4),
     & REAL(R_obebi,4),REAL(R_ofebi,4),L_ufebi,REAL(R_akebi
     &,4),L_ekebi,
     & L_ikebi,R_afebi,REAL(R_idebi,4),REAL(R_edebi,4),L_okebi
     &)
      !}
C FDA60b_vlv.fgi( 854, 192):���������� ������� ��� 2,20FDA65CF026XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_olebi,R_orebi,REAL(1
     &,4),
     & REAL(R_umebi,4),I_irebi,REAL(R_emebi,4),
     & REAL(R_imebi,4),REAL(R_ilebi,4),
     & REAL(R_elebi,4),REAL(R_epebi,4),L_ipebi,REAL(R_opebi
     &,4),L_upebi,
     & L_arebi,R_omebi,REAL(R_amebi,4),REAL(R_ulebi,4),L_erebi
     &)
      !}
C FDA60b_vlv.fgi( 854, 207):���������� ������� ��� 2,20FDA65CF029XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_esebi,R_exebi,REAL(1
     &,4),
     & REAL(R_itebi,4),I_axebi,REAL(R_usebi,4),
     & REAL(R_atebi,4),REAL(R_asebi,4),
     & REAL(R_urebi,4),REAL(R_utebi,4),L_avebi,REAL(R_evebi
     &,4),L_ivebi,
     & L_ovebi,R_etebi,REAL(R_osebi,4),REAL(R_isebi,4),L_uvebi
     &)
      !}
C FDA60b_vlv.fgi( 854, 222):���������� ������� ��� 2,20FDA65CF027XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uxebi,R_ufibi,REAL(1
     &,4),
     & REAL(R_adibi,4),I_ofibi,REAL(R_ibibi,4),
     & REAL(R_obibi,4),REAL(R_oxebi,4),
     & REAL(R_ixebi,4),REAL(R_idibi,4),L_odibi,REAL(R_udibi
     &,4),L_afibi,
     & L_efibi,R_ubibi,REAL(R_ebibi,4),REAL(R_abibi,4),L_ifibi
     &)
      !}
C FDA60b_vlv.fgi( 786, 237):���������� ������� ��� 2,20FDA64CP024XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ikibi,R_ipibi,REAL(1
     &,4),
     & REAL(R_olibi,4),I_epibi,REAL(R_alibi,4),
     & REAL(R_elibi,4),REAL(R_ekibi,4),
     & REAL(R_akibi,4),REAL(R_amibi,4),L_emibi,REAL(R_imibi
     &,4),L_omibi,
     & L_umibi,R_ilibi,REAL(R_ukibi,4),REAL(R_okibi,4),L_apibi
     &)
      !}
C FDA60b_vlv.fgi( 786, 252):���������� ������� ��� 2,20FDA64CP023XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_aribi,R_avibi,REAL(1
     &,4),
     & REAL(R_esibi,4),I_utibi,REAL(R_oribi,4),
     & REAL(R_uribi,4),REAL(R_upibi,4),
     & REAL(R_opibi,4),REAL(R_osibi,4),L_usibi,REAL(R_atibi
     &,4),L_etibi,
     & L_itibi,R_asibi,REAL(R_iribi,4),REAL(R_eribi,4),L_otibi
     &)
      !}
C FDA60b_vlv.fgi( 788, 177):���������� ������� ��� 2,20FDA64CF028XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovibi,R_odobi,REAL(1
     &,4),
     & REAL(R_uxibi,4),I_idobi,REAL(R_exibi,4),
     & REAL(R_ixibi,4),REAL(R_ivibi,4),
     & REAL(R_evibi,4),REAL(R_ebobi,4),L_ibobi,REAL(R_obobi
     &,4),L_ubobi,
     & L_adobi,R_oxibi,REAL(R_axibi,4),REAL(R_uvibi,4),L_edobi
     &)
      !}
C FDA60b_vlv.fgi( 788, 192):���������� ������� ��� 2,20FDA64CF026XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efobi,R_emobi,REAL(1
     &,4),
     & REAL(R_ikobi,4),I_amobi,REAL(R_ufobi,4),
     & REAL(R_akobi,4),REAL(R_afobi,4),
     & REAL(R_udobi,4),REAL(R_ukobi,4),L_alobi,REAL(R_elobi
     &,4),L_ilobi,
     & L_olobi,R_ekobi,REAL(R_ofobi,4),REAL(R_ifobi,4),L_ulobi
     &)
      !}
C FDA60b_vlv.fgi( 788, 207):���������� ������� ��� 2,20FDA64CF029XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umobi,R_usobi,REAL(1
     &,4),
     & REAL(R_arobi,4),I_osobi,REAL(R_ipobi,4),
     & REAL(R_opobi,4),REAL(R_omobi,4),
     & REAL(R_imobi,4),REAL(R_irobi,4),L_orobi,REAL(R_urobi
     &,4),L_asobi,
     & L_esobi,R_upobi,REAL(R_epobi,4),REAL(R_apobi,4),L_isobi
     &)
      !}
C FDA60b_vlv.fgi( 788, 222):���������� ������� ��� 2,20FDA64CF027XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itobi,R_ibubi,REAL(1
     &,4),
     & REAL(R_ovobi,4),I_ebubi,REAL(R_avobi,4),
     & REAL(R_evobi,4),REAL(R_etobi,4),
     & REAL(R_atobi,4),REAL(R_axobi,4),L_exobi,REAL(R_ixobi
     &,4),L_oxobi,
     & L_uxobi,R_ivobi,REAL(R_utobi,4),REAL(R_otobi,4),L_abubi
     &)
      !}
C FDA60b_vlv.fgi( 754, 237):���������� ������� ��� 2,20FDA63CP024XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_adubi,R_alubi,REAL(1
     &,4),
     & REAL(R_efubi,4),I_ukubi,REAL(R_odubi,4),
     & REAL(R_udubi,4),REAL(R_ububi,4),
     & REAL(R_obubi,4),REAL(R_ofubi,4),L_ufubi,REAL(R_akubi
     &,4),L_ekubi,
     & L_ikubi,R_afubi,REAL(R_idubi,4),REAL(R_edubi,4),L_okubi
     &)
      !}
C FDA60b_vlv.fgi( 754, 252):���������� ������� ��� 2,20FDA63CP023XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_olubi,R_orubi,REAL(1
     &,4),
     & REAL(R_umubi,4),I_irubi,REAL(R_emubi,4),
     & REAL(R_imubi,4),REAL(R_ilubi,4),
     & REAL(R_elubi,4),REAL(R_epubi,4),L_ipubi,REAL(R_opubi
     &,4),L_upubi,
     & L_arubi,R_omubi,REAL(R_amubi,4),REAL(R_ulubi,4),L_erubi
     &)
      !}
C FDA60b_vlv.fgi( 754, 177):���������� ������� ��� 2,20FDA63CF028XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_esubi,R_exubi,REAL(1
     &,4),
     & REAL(R_itubi,4),I_axubi,REAL(R_usubi,4),
     & REAL(R_atubi,4),REAL(R_asubi,4),
     & REAL(R_urubi,4),REAL(R_utubi,4),L_avubi,REAL(R_evubi
     &,4),L_ivubi,
     & L_ovubi,R_etubi,REAL(R_osubi,4),REAL(R_isubi,4),L_uvubi
     &)
      !}
C FDA60b_vlv.fgi( 754, 192):���������� ������� ��� 2,20FDA63CF026XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uxubi,R_ufadi,REAL(1
     &,4),
     & REAL(R_adadi,4),I_ofadi,REAL(R_ibadi,4),
     & REAL(R_obadi,4),REAL(R_oxubi,4),
     & REAL(R_ixubi,4),REAL(R_idadi,4),L_odadi,REAL(R_udadi
     &,4),L_afadi,
     & L_efadi,R_ubadi,REAL(R_ebadi,4),REAL(R_abadi,4),L_ifadi
     &)
      !}
C FDA60b_vlv.fgi( 754, 207):���������� ������� ��� 2,20FDA63CF029XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ikadi,R_ipadi,REAL(1
     &,4),
     & REAL(R_oladi,4),I_epadi,REAL(R_aladi,4),
     & REAL(R_eladi,4),REAL(R_ekadi,4),
     & REAL(R_akadi,4),REAL(R_amadi,4),L_emadi,REAL(R_imadi
     &,4),L_omadi,
     & L_umadi,R_iladi,REAL(R_ukadi,4),REAL(R_okadi,4),L_apadi
     &)
      !}
C FDA60b_vlv.fgi( 754, 222):���������� ������� ��� 2,20FDA63CF027XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_aradi,R_avadi,REAL(1
     &,4),
     & REAL(R_esadi,4),I_utadi,REAL(R_oradi,4),
     & REAL(R_uradi,4),REAL(R_upadi,4),
     & REAL(R_opadi,4),REAL(R_osadi,4),L_usadi,REAL(R_atadi
     &,4),L_etadi,
     & L_itadi,R_asadi,REAL(R_iradi,4),REAL(R_eradi,4),L_otadi
     &)
      !}
C FDA60b_vlv.fgi( 720, 237):���������� ������� ��� 2,20FDA62CP024XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovadi,R_odedi,REAL(1
     &,4),
     & REAL(R_uxadi,4),I_idedi,REAL(R_exadi,4),
     & REAL(R_ixadi,4),REAL(R_ivadi,4),
     & REAL(R_evadi,4),REAL(R_ebedi,4),L_ibedi,REAL(R_obedi
     &,4),L_ubedi,
     & L_adedi,R_oxadi,REAL(R_axadi,4),REAL(R_uvadi,4),L_ededi
     &)
      !}
C FDA60b_vlv.fgi( 720, 252):���������� ������� ��� 2,20FDA62CP023XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efedi,R_emedi,REAL(1
     &,4),
     & REAL(R_ikedi,4),I_amedi,REAL(R_ufedi,4),
     & REAL(R_akedi,4),REAL(R_afedi,4),
     & REAL(R_udedi,4),REAL(R_ukedi,4),L_aledi,REAL(R_eledi
     &,4),L_iledi,
     & L_oledi,R_ekedi,REAL(R_ofedi,4),REAL(R_ifedi,4),L_uledi
     &)
      !}
C FDA60b_vlv.fgi( 720, 177):���������� ������� ��� 2,20FDA62CF028XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umedi,R_usedi,REAL(1
     &,4),
     & REAL(R_aredi,4),I_osedi,REAL(R_ipedi,4),
     & REAL(R_opedi,4),REAL(R_omedi,4),
     & REAL(R_imedi,4),REAL(R_iredi,4),L_oredi,REAL(R_uredi
     &,4),L_asedi,
     & L_esedi,R_upedi,REAL(R_epedi,4),REAL(R_apedi,4),L_isedi
     &)
      !}
C FDA60b_vlv.fgi( 720, 192):���������� ������� ��� 2,20FDA62CF026XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itedi,R_ibidi,REAL(1
     &,4),
     & REAL(R_ovedi,4),I_ebidi,REAL(R_avedi,4),
     & REAL(R_evedi,4),REAL(R_etedi,4),
     & REAL(R_atedi,4),REAL(R_axedi,4),L_exedi,REAL(R_ixedi
     &,4),L_oxedi,
     & L_uxedi,R_ivedi,REAL(R_utedi,4),REAL(R_otedi,4),L_abidi
     &)
      !}
C FDA60b_vlv.fgi( 720, 207):���������� ������� ��� 2,20FDA62CF029XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_adidi,R_alidi,REAL(1
     &,4),
     & REAL(R_efidi,4),I_ukidi,REAL(R_odidi,4),
     & REAL(R_udidi,4),REAL(R_ubidi,4),
     & REAL(R_obidi,4),REAL(R_ofidi,4),L_ufidi,REAL(R_akidi
     &,4),L_ekidi,
     & L_ikidi,R_afidi,REAL(R_ididi,4),REAL(R_edidi,4),L_okidi
     &)
      !}
C FDA60b_vlv.fgi( 720, 222):���������� ������� ��� 2,20FDA62CF027XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_olidi,R_oridi,REAL(1
     &,4),
     & REAL(R_umidi,4),I_iridi,REAL(R_emidi,4),
     & REAL(R_imidi,4),REAL(R_ilidi,4),
     & REAL(R_elidi,4),REAL(R_epidi,4),L_ipidi,REAL(R_opidi
     &,4),L_upidi,
     & L_aridi,R_omidi,REAL(R_amidi,4),REAL(R_ulidi,4),L_eridi
     &)
      !}
C FDA60b_vlv.fgi( 688, 236):���������� ������� ��� 2,20FDA61CP024XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_esidi,R_exidi,REAL(1
     &,4),
     & REAL(R_itidi,4),I_axidi,REAL(R_usidi,4),
     & REAL(R_atidi,4),REAL(R_asidi,4),
     & REAL(R_uridi,4),REAL(R_utidi,4),L_avidi,REAL(R_evidi
     &,4),L_ividi,
     & L_ovidi,R_etidi,REAL(R_osidi,4),REAL(R_isidi,4),L_uvidi
     &)
      !}
C FDA60b_vlv.fgi( 688, 251):���������� ������� ��� 2,20FDA61CP023XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uxidi,R_ufodi,REAL(1
     &,4),
     & REAL(R_adodi,4),I_ofodi,REAL(R_ibodi,4),
     & REAL(R_obodi,4),REAL(R_oxidi,4),
     & REAL(R_ixidi,4),REAL(R_idodi,4),L_ododi,REAL(R_udodi
     &,4),L_afodi,
     & L_efodi,R_ubodi,REAL(R_ebodi,4),REAL(R_abodi,4),L_ifodi
     &)
      !}
C FDA60b_vlv.fgi( 690, 176):���������� ������� ��� 2,20FDA61CF028XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ikodi,R_ipodi,REAL(1
     &,4),
     & REAL(R_olodi,4),I_epodi,REAL(R_alodi,4),
     & REAL(R_elodi,4),REAL(R_ekodi,4),
     & REAL(R_akodi,4),REAL(R_amodi,4),L_emodi,REAL(R_imodi
     &,4),L_omodi,
     & L_umodi,R_ilodi,REAL(R_ukodi,4),REAL(R_okodi,4),L_apodi
     &)
      !}
C FDA60b_vlv.fgi( 690, 191):���������� ������� ��� 2,20FDA61CF026XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_arodi,R_avodi,REAL(1
     &,4),
     & REAL(R_esodi,4),I_utodi,REAL(R_orodi,4),
     & REAL(R_urodi,4),REAL(R_upodi,4),
     & REAL(R_opodi,4),REAL(R_osodi,4),L_usodi,REAL(R_atodi
     &,4),L_etodi,
     & L_itodi,R_asodi,REAL(R_irodi,4),REAL(R_erodi,4),L_otodi
     &)
      !}
C FDA60b_vlv.fgi( 690, 206):���������� ������� ��� 2,20FDA61CF029XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovodi,R_odudi,REAL(1
     &,4),
     & REAL(R_uxodi,4),I_idudi,REAL(R_exodi,4),
     & REAL(R_ixodi,4),REAL(R_ivodi,4),
     & REAL(R_evodi,4),REAL(R_ebudi,4),L_ibudi,REAL(R_obudi
     &,4),L_ubudi,
     & L_adudi,R_oxodi,REAL(R_axodi,4),REAL(R_uvodi,4),L_edudi
     &)
      !}
C FDA60b_vlv.fgi( 690, 221):���������� ������� ��� 2,20FDA61CF027XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efudi,R_emudi,REAL(1
     &,4),
     & REAL(R_ikudi,4),I_amudi,REAL(R_ufudi,4),
     & REAL(R_akudi,4),REAL(R_afudi,4),
     & REAL(R_ududi,4),REAL(R_ukudi,4),L_aludi,REAL(R_eludi
     &,4),L_iludi,
     & L_oludi,R_ekudi,REAL(R_ofudi,4),REAL(R_ifudi,4),L_uludi
     &)
      !}
C FDA60b_vlv.fgi( 494, 445):���������� ������� ��� 2,20FDA60CM003XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umudi,R_usudi,REAL(1
     &,4),
     & REAL(R_arudi,4),I_osudi,REAL(R_ipudi,4),
     & REAL(R_opudi,4),REAL(R_omudi,4),
     & REAL(R_imudi,4),REAL(R_irudi,4),L_orudi,REAL(R_urudi
     &,4),L_asudi,
     & L_esudi,R_upudi,REAL(R_epudi,4),REAL(R_apudi,4),L_isudi
     &)
      !}
C FDA60b_vlv.fgi( 494, 462):���������� ������� ��� 2,20FDA60CM002XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itudi,R_ibafi,REAL(1
     &,4),
     & REAL(R_ovudi,4),I_ebafi,REAL(R_avudi,4),
     & REAL(R_evudi,4),REAL(R_etudi,4),
     & REAL(R_atudi,4),REAL(R_axudi,4),L_exudi,REAL(R_ixudi
     &,4),L_oxudi,
     & L_uxudi,R_ivudi,REAL(R_utudi,4),REAL(R_otudi,4),L_abafi
     &)
      !}
C FDA60b_vlv.fgi( 494, 477):���������� ������� ��� 2,20FDA60CM001XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_adafi,R_alafi,REAL(1
     &,4),
     & REAL(R_efafi,4),I_ukafi,REAL(R_odafi,4),
     & REAL(R_udafi,4),REAL(R_ubafi,4),
     & REAL(R_obafi,4),REAL(R_ofafi,4),L_ufafi,REAL(R_akafi
     &,4),L_ekafi,
     & L_ikafi,R_afafi,REAL(R_idafi,4),REAL(R_edafi,4),L_okafi
     &)
      !}
C FDA60b_vlv.fgi( 465, 445):���������� ������� ��� 2,20FDA60CQ003XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_olafi,R_orafi,REAL(1
     &,4),
     & REAL(R_umafi,4),I_irafi,REAL(R_emafi,4),
     & REAL(R_imafi,4),REAL(R_ilafi,4),
     & REAL(R_elafi,4),REAL(R_epafi,4),L_ipafi,REAL(R_opafi
     &,4),L_upafi,
     & L_arafi,R_omafi,REAL(R_amafi,4),REAL(R_ulafi,4),L_erafi
     &)
      !}
C FDA60b_vlv.fgi( 465, 462):���������� ������� ��� 2,20FDA60CQ002XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_esafi,R_exafi,REAL(1
     &,4),
     & REAL(R_itafi,4),I_axafi,REAL(R_usafi,4),
     & REAL(R_atafi,4),REAL(R_asafi,4),
     & REAL(R_urafi,4),REAL(R_utafi,4),L_avafi,REAL(R_evafi
     &,4),L_ivafi,
     & L_ovafi,R_etafi,REAL(R_osafi,4),REAL(R_isafi,4),L_uvafi
     &)
      !}
C FDA60b_vlv.fgi( 465, 477):���������� ������� ��� 2,20FDA60CQ001XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uxafi,R_ufefi,REAL(1
     &,4),
     & REAL(R_adefi,4),I_ofefi,REAL(R_ibefi,4),
     & REAL(R_obefi,4),REAL(R_oxafi,4),
     & REAL(R_ixafi,4),REAL(R_idefi,4),L_odefi,REAL(R_udefi
     &,4),L_afefi,
     & L_efefi,R_ubefi,REAL(R_ebefi,4),REAL(R_abefi,4),L_ifefi
     &)
      !}
C FDA60b_vlv.fgi( 435, 445):���������� ������� ��� 2,20FDA60CF037XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ikefi,R_ipefi,REAL(1
     &,4),
     & REAL(R_olefi,4),I_epefi,REAL(R_alefi,4),
     & REAL(R_elefi,4),REAL(R_ekefi,4),
     & REAL(R_akefi,4),REAL(R_amefi,4),L_emefi,REAL(R_imefi
     &,4),L_omefi,
     & L_umefi,R_ilefi,REAL(R_ukefi,4),REAL(R_okefi,4),L_apefi
     &)
      !}
C FDA60b_vlv.fgi( 435, 462):���������� ������� ��� 2,20FDA60CF036XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_arefi,R_avefi,REAL(1
     &,4),
     & REAL(R_esefi,4),I_utefi,REAL(R_orefi,4),
     & REAL(R_urefi,4),REAL(R_upefi,4),
     & REAL(R_opefi,4),REAL(R_osefi,4),L_usefi,REAL(R_atefi
     &,4),L_etefi,
     & L_itefi,R_asefi,REAL(R_irefi,4),REAL(R_erefi,4),L_otefi
     &)
      !}
C FDA60b_vlv.fgi( 435, 477):���������� ������� ��� 2,20FDA60CF035XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovefi,R_odifi,REAL(1
     &,4),
     & REAL(R_uxefi,4),I_idifi,REAL(R_exefi,4),
     & REAL(R_ixefi,4),REAL(R_ivefi,4),
     & REAL(R_evefi,4),REAL(R_ebifi,4),L_ibifi,REAL(R_obifi
     &,4),L_ubifi,
     & L_adifi,R_oxefi,REAL(R_axefi,4),REAL(R_uvefi,4),L_edifi
     &)
      !}
C FDA60b_vlv.fgi( 435, 492):���������� ������� ��� 2,20FDA60CM023XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efifi,R_emifi,REAL(1
     &,4),
     & REAL(R_ikifi,4),I_amifi,REAL(R_ufifi,4),
     & REAL(R_akifi,4),REAL(R_afifi,4),
     & REAL(R_udifi,4),REAL(R_ukifi,4),L_alifi,REAL(R_elifi
     &,4),L_ilifi,
     & L_olifi,R_ekifi,REAL(R_ofifi,4),REAL(R_ififi,4),L_ulifi
     &)
      !}
C FDA60b_vlv.fgi( 435, 507):���������� ������� ��� 2,20FDA60CQ023XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umifi,R_usifi,REAL(1
     &,4),
     & REAL(R_arifi,4),I_osifi,REAL(R_ipifi,4),
     & REAL(R_opifi,4),REAL(R_omifi,4),
     & REAL(R_imifi,4),REAL(R_irifi,4),L_orifi,REAL(R_urifi
     &,4),L_asifi,
     & L_esifi,R_upifi,REAL(R_epifi,4),REAL(R_apifi,4),L_isifi
     &)
      !}
C FDA60b_vlv.fgi( 435, 523):���������� ������� ��� 2,20FDA60CF023XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itifi,R_ibofi,REAL(1
     &,4),
     & REAL(R_ovifi,4),I_ebofi,REAL(R_avifi,4),
     & REAL(R_evifi,4),REAL(R_etifi,4),
     & REAL(R_atifi,4),REAL(R_axifi,4),L_exifi,REAL(R_ixifi
     &,4),L_oxifi,
     & L_uxifi,R_ivifi,REAL(R_utifi,4),REAL(R_otifi,4),L_abofi
     &)
      !}
C FDA60b_vlv.fgi( 404, 492):���������� ������� ��� 2,20FDA60CF025XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_adofi,R_alofi,REAL(1
     &,4),
     & REAL(R_efofi,4),I_ukofi,REAL(R_odofi,4),
     & REAL(R_udofi,4),REAL(R_ubofi,4),
     & REAL(R_obofi,4),REAL(R_ofofi,4),L_ufofi,REAL(R_akofi
     &,4),L_ekofi,
     & L_ikofi,R_afofi,REAL(R_idofi,4),REAL(R_edofi,4),L_okofi
     &)
      !}
C FDA60b_vlv.fgi( 404, 507):���������� ������� ��� 2,20FDA60CP030XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_olofi,R_orofi,REAL(1
     &,4),
     & REAL(R_umofi,4),I_irofi,REAL(R_emofi,4),
     & REAL(R_imofi,4),REAL(R_ilofi,4),
     & REAL(R_elofi,4),REAL(R_epofi,4),L_ipofi,REAL(R_opofi
     &,4),L_upofi,
     & L_arofi,R_omofi,REAL(R_amofi,4),REAL(R_ulofi,4),L_erofi
     &)
      !}
C FDA60b_vlv.fgi( 404, 523):���������� ������� ��� 2,20FDA60CT027XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_esofi,R_exofi,REAL(1
     &,4),
     & REAL(R_itofi,4),I_axofi,REAL(R_usofi,4),
     & REAL(R_atofi,4),REAL(R_asofi,4),
     & REAL(R_urofi,4),REAL(R_utofi,4),L_avofi,REAL(R_evofi
     &,4),L_ivofi,
     & L_ovofi,R_etofi,REAL(R_osofi,4),REAL(R_isofi,4),L_uvofi
     &)
      !}
C FDA60b_vlv.fgi( 375, 445):���������� ������� ��� 2,20FDA60CT022XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uxofi,R_ufufi,REAL(1
     &,4),
     & REAL(R_adufi,4),I_ofufi,REAL(R_ibufi,4),
     & REAL(R_obufi,4),REAL(R_oxofi,4),
     & REAL(R_ixofi,4),REAL(R_idufi,4),L_odufi,REAL(R_udufi
     &,4),L_afufi,
     & L_efufi,R_ubufi,REAL(R_ebufi,4),REAL(R_abufi,4),L_ifufi
     &)
      !}
C FDA60b_vlv.fgi( 375, 462):���������� ������� ��� 2,20FDA60CT021XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ikufi,R_ipufi,REAL(1
     &,4),
     & REAL(R_olufi,4),I_epufi,REAL(R_alufi,4),
     & REAL(R_elufi,4),REAL(R_ekufi,4),
     & REAL(R_akufi,4),REAL(R_amufi,4),L_emufi,REAL(R_imufi
     &,4),L_omufi,
     & L_umufi,R_ilufi,REAL(R_ukufi,4),REAL(R_okufi,4),L_apufi
     &)
      !}
C FDA60b_vlv.fgi( 375, 477):���������� ������� ��� 2,20FDA60CT020XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_arufi,R_avufi,REAL(1
     &,4),
     & REAL(R_esufi,4),I_utufi,REAL(R_orufi,4),
     & REAL(R_urufi,4),REAL(R_upufi,4),
     & REAL(R_opufi,4),REAL(R_osufi,4),L_usufi,REAL(R_atufi
     &,4),L_etufi,
     & L_itufi,R_asufi,REAL(R_irufi,4),REAL(R_erufi,4),L_otufi
     &)
      !}
C FDA60b_vlv.fgi( 375, 492):���������� ������� ��� 2,20FDA60CT019XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovufi,R_odaki,REAL(1
     &,4),
     & REAL(R_uxufi,4),I_idaki,REAL(R_exufi,4),
     & REAL(R_ixufi,4),REAL(R_ivufi,4),
     & REAL(R_evufi,4),REAL(R_ebaki,4),L_ibaki,REAL(R_obaki
     &,4),L_ubaki,
     & L_adaki,R_oxufi,REAL(R_axufi,4),REAL(R_uvufi,4),L_edaki
     &)
      !}
C FDA60b_vlv.fgi( 375, 507):���������� ������� ��� 2,20FDA60CT018XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efaki,R_emaki,REAL(1
     &,4),
     & REAL(R_ikaki,4),I_amaki,REAL(R_ufaki,4),
     & REAL(R_akaki,4),REAL(R_afaki,4),
     & REAL(R_udaki,4),REAL(R_ukaki,4),L_alaki,REAL(R_elaki
     &,4),L_ilaki,
     & L_olaki,R_ekaki,REAL(R_ofaki,4),REAL(R_ifaki,4),L_ulaki
     &)
      !}
C FDA60b_vlv.fgi( 375, 523):���������� ������� ��� 2,20FDA60CT017XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umaki,R_usaki,REAL(1
     &,4),
     & REAL(R_araki,4),I_osaki,REAL(R_ipaki,4),
     & REAL(R_opaki,4),REAL(R_omaki,4),
     & REAL(R_imaki,4),REAL(R_iraki,4),L_oraki,REAL(R_uraki
     &,4),L_asaki,
     & L_esaki,R_upaki,REAL(R_epaki,4),REAL(R_apaki,4),L_isaki
     &)
      !}
C FDA60b_vlv.fgi( 346, 492):���������� ������� ��� 2,20FDA60CF017XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itaki,R_ibeki,REAL(1
     &,4),
     & REAL(R_ovaki,4),I_ebeki,REAL(R_avaki,4),
     & REAL(R_evaki,4),REAL(R_etaki,4),
     & REAL(R_ataki,4),REAL(R_axaki,4),L_exaki,REAL(R_ixaki
     &,4),L_oxaki,
     & L_uxaki,R_ivaki,REAL(R_utaki,4),REAL(R_otaki,4),L_abeki
     &)
      !}
C FDA60b_vlv.fgi( 346, 507):���������� ������� ��� 2,20FDA60CP024XQ01
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ukeki,4),REAL
     &(R_efeki,4),R_adeki,
     & R_aleki,REAL(1,4),REAL(R_odeki,4),
     & REAL(R_udeki,4),REAL(R_ubeki,4),
     & REAL(R_obeki,4),I_okeki,REAL(R_ifeki,4),L_ofeki,
     & REAL(R_ufeki,4),L_akeki,L_ekeki,R_afeki,REAL(R_ideki
     &,4),REAL(R_edeki,4),L_ikeki)
      !}
C FDA60b_vlv.fgi( 346, 523):���������� ������� ��������,20FDA60CP023XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uleki,R_ureki,REAL(1
     &,4),
     & REAL(R_apeki,4),I_oreki,REAL(R_imeki,4),
     & REAL(R_omeki,4),REAL(R_oleki,4),
     & REAL(R_ileki,4),REAL(R_ipeki,4),L_opeki,REAL(R_upeki
     &,4),L_areki,
     & L_ereki,R_umeki,REAL(R_emeki,4),REAL(R_ameki,4),L_ireki
     &)
      !}
C FDA60b_vlv.fgi( 317, 414):���������� ������� ��� 2,20FDA60CF015XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_iseki,R_ixeki,REAL(1
     &,4),
     & REAL(R_oteki,4),I_exeki,REAL(R_ateki,4),
     & REAL(R_eteki,4),REAL(R_eseki,4),
     & REAL(R_aseki,4),REAL(R_aveki,4),L_eveki,REAL(R_iveki
     &,4),L_oveki,
     & L_uveki,R_iteki,REAL(R_useki,4),REAL(R_oseki,4),L_axeki
     &)
      !}
C FDA60b_vlv.fgi( 317, 429):���������� ������� ��� 2,20FDA60CQ018XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_abiki,R_akiki,REAL(1
     &,4),
     & REAL(R_ediki,4),I_ufiki,REAL(R_obiki,4),
     & REAL(R_ubiki,4),REAL(R_uxeki,4),
     & REAL(R_oxeki,4),REAL(R_odiki,4),L_udiki,REAL(R_afiki
     &,4),L_efiki,
     & L_ifiki,R_adiki,REAL(R_ibiki,4),REAL(R_ebiki,4),L_ofiki
     &)
      !}
C FDA60b_vlv.fgi( 317, 445):���������� ������� ��� 2,20FDA60CM018XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_okiki,R_opiki,REAL(1
     &,4),
     & REAL(R_uliki,4),I_ipiki,REAL(R_eliki,4),
     & REAL(R_iliki,4),REAL(R_ikiki,4),
     & REAL(R_ekiki,4),REAL(R_emiki,4),L_imiki,REAL(R_omiki
     &,4),L_umiki,
     & L_apiki,R_oliki,REAL(R_aliki,4),REAL(R_ukiki,4),L_epiki
     &)
      !}
C FDA60b_vlv.fgi( 317, 462):���������� ������� ��� 2,20FDA60CP037XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_eriki,R_eviki,REAL(1
     &,4),
     & REAL(R_isiki,4),I_aviki,REAL(R_uriki,4),
     & REAL(R_asiki,4),REAL(R_ariki,4),
     & REAL(R_upiki,4),REAL(R_usiki,4),L_atiki,REAL(R_etiki
     &,4),L_itiki,
     & L_otiki,R_esiki,REAL(R_oriki,4),REAL(R_iriki,4),L_utiki
     &)
      !}
C FDA60b_vlv.fgi( 317, 476):���������� ������� ��� 2,20FDA60CF034XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uviki,R_udoki,REAL(1
     &,4),
     & REAL(R_aboki,4),I_odoki,REAL(R_ixiki,4),
     & REAL(R_oxiki,4),REAL(R_oviki,4),
     & REAL(R_iviki,4),REAL(R_iboki,4),L_oboki,REAL(R_uboki
     &,4),L_adoki,
     & L_edoki,R_uxiki,REAL(R_exiki,4),REAL(R_axiki,4),L_idoki
     &)
      !}
C FDA60b_vlv.fgi( 317, 492):���������� ������� ��� 2,20FDA60CF033XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ifoki,R_imoki,REAL(1
     &,4),
     & REAL(R_okoki,4),I_emoki,REAL(R_akoki,4),
     & REAL(R_ekoki,4),REAL(R_efoki,4),
     & REAL(R_afoki,4),REAL(R_aloki,4),L_eloki,REAL(R_iloki
     &,4),L_oloki,
     & L_uloki,R_ikoki,REAL(R_ufoki,4),REAL(R_ofoki,4),L_amoki
     &)
      !}
C FDA60b_vlv.fgi( 317, 507):���������� ������� ��� 2,20FDA60CT032XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_apoki,R_atoki,REAL(1
     &,4),
     & REAL(R_eroki,4),I_usoki,REAL(R_opoki,4),
     & REAL(R_upoki,4),REAL(R_umoki,4),
     & REAL(R_omoki,4),REAL(R_oroki,4),L_uroki,REAL(R_asoki
     &,4),L_esoki,
     & L_isoki,R_aroki,REAL(R_ipoki,4),REAL(R_epoki,4),L_osoki
     &)
      !}
C FDA60b_vlv.fgi( 317, 523):���������� ������� ��� 2,20FDA60CQ028XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_otoki,R_obuki,REAL(1
     &,4),
     & REAL(R_uvoki,4),I_ibuki,REAL(R_evoki,4),
     & REAL(R_ivoki,4),REAL(R_itoki,4),
     & REAL(R_etoki,4),REAL(R_exoki,4),L_ixoki,REAL(R_oxoki
     &,4),L_uxoki,
     & L_abuki,R_ovoki,REAL(R_avoki,4),REAL(R_utoki,4),L_ebuki
     &)
      !}
C FDA60b_vlv.fgi( 287, 398):���������� ������� ��� 2,20FDA60CP036XQ01
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_aluki,4),REAL
     &(R_ifuki,4),R_eduki,
     & R_eluki,REAL(1,4),REAL(R_uduki,4),
     & REAL(R_afuki,4),REAL(R_aduki,4),
     & REAL(R_ubuki,4),I_ukuki,REAL(R_ofuki,4),L_ufuki,
     & REAL(R_akuki,4),L_ekuki,L_ikuki,R_efuki,REAL(R_oduki
     &,4),REAL(R_iduki,4),L_okuki)
      !}
C FDA60b_vlv.fgi( 287, 414):���������� ������� ��������,20FDA60CP035XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_amuki,R_asuki,REAL(1
     &,4),
     & REAL(R_epuki,4),I_uruki,REAL(R_omuki,4),
     & REAL(R_umuki,4),REAL(R_uluki,4),
     & REAL(R_oluki,4),REAL(R_opuki,4),L_upuki,REAL(R_aruki
     &,4),L_eruki,
     & L_iruki,R_apuki,REAL(R_imuki,4),REAL(R_emuki,4),L_oruki
     &)
      !}
C FDA60b_vlv.fgi( 287, 429):���������� ������� ��� 2,20FDA60CF031XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_osuki,R_oxuki,REAL(1
     &,4),
     & REAL(R_utuki,4),I_ixuki,REAL(R_etuki,4),
     & REAL(R_ituki,4),REAL(R_isuki,4),
     & REAL(R_esuki,4),REAL(R_evuki,4),L_ivuki,REAL(R_ovuki
     &,4),L_uvuki,
     & L_axuki,R_otuki,REAL(R_atuki,4),REAL(R_usuki,4),L_exuki
     &)
      !}
C FDA60b_vlv.fgi( 287, 445):���������� ������� ��� 2,20FDA60CM028XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ebali,R_ekali,REAL(1
     &,4),
     & REAL(R_idali,4),I_akali,REAL(R_ubali,4),
     & REAL(R_adali,4),REAL(R_abali,4),
     & REAL(R_uxuki,4),REAL(R_udali,4),L_afali,REAL(R_efali
     &,4),L_ifali,
     & L_ofali,R_edali,REAL(R_obali,4),REAL(R_ibali,4),L_ufali
     &)
      !}
C FDA60b_vlv.fgi( 287, 462):���������� ������� ��� 2,20FDA60CP018XQ01
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_opali,4),REAL
     &(R_amali,4),R_ukali,
     & R_upali,REAL(1,4),REAL(R_ilali,4),
     & REAL(R_olali,4),REAL(R_okali,4),
     & REAL(R_ikali,4),I_ipali,REAL(R_emali,4),L_imali,
     & REAL(R_omali,4),L_umali,L_apali,R_ulali,REAL(R_elali
     &,4),REAL(R_alali,4),L_epali)
      !}
C FDA60b_vlv.fgi( 287, 476):���������� ������� ��������,20FDA60CP017XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_orali,R_ovali,REAL(1
     &,4),
     & REAL(R_usali,4),I_ivali,REAL(R_esali,4),
     & REAL(R_isali,4),REAL(R_irali,4),
     & REAL(R_erali,4),REAL(R_etali,4),L_itali,REAL(R_otali
     &,4),L_utali,
     & L_avali,R_osali,REAL(R_asali,4),REAL(R_urali,4),L_evali
     &)
      !}
C FDA60b_vlv.fgi( 287, 492):���������� ������� ��� 2,20FDA60CF009XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_exali,R_efeli,REAL(1
     &,4),
     & REAL(R_ibeli,4),I_afeli,REAL(R_uxali,4),
     & REAL(R_abeli,4),REAL(R_axali,4),
     & REAL(R_uvali,4),REAL(R_ubeli,4),L_adeli,REAL(R_edeli
     &,4),L_ideli,
     & L_odeli,R_ebeli,REAL(R_oxali,4),REAL(R_ixali,4),L_udeli
     &)
      !}
C FDA60b_vlv.fgi( 287, 507):���������� ������� ��� 2,20FDA60CQ013XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ufeli,R_umeli,REAL(1
     &,4),
     & REAL(R_aleli,4),I_omeli,REAL(R_ikeli,4),
     & REAL(R_okeli,4),REAL(R_ofeli,4),
     & REAL(R_ifeli,4),REAL(R_ileli,4),L_oleli,REAL(R_uleli
     &,4),L_ameli,
     & L_emeli,R_ukeli,REAL(R_ekeli,4),REAL(R_akeli,4),L_imeli
     &)
      !}
C FDA60b_vlv.fgi( 287, 523):���������� ������� ��� 2,20FDA60CM013XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ipeli,R_iteli,REAL(1
     &,4),
     & REAL(R_oreli,4),I_eteli,REAL(R_areli,4),
     & REAL(R_ereli,4),REAL(R_epeli,4),
     & REAL(R_apeli,4),REAL(R_aseli,4),L_eseli,REAL(R_iseli
     &,4),L_oseli,
     & L_useli,R_ireli,REAL(R_upeli,4),REAL(R_opeli,4),L_ateli
     &)
      !}
C FDA60b_vlv.fgi( 258, 398):���������� ������� ��� 2,20FDA60CT012XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_aveli,R_adili,REAL(1
     &,4),
     & REAL(R_exeli,4),I_ubili,REAL(R_oveli,4),
     & REAL(R_uveli,4),REAL(R_uteli,4),
     & REAL(R_oteli,4),REAL(R_oxeli,4),L_uxeli,REAL(R_abili
     &,4),L_ebili,
     & L_ibili,R_axeli,REAL(R_iveli,4),REAL(R_eveli,4),L_obili
     &)
      !}
C FDA60b_vlv.fgi( 258, 414):���������� ������� ��� 2,20FDA60CF011XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_odili,R_olili,REAL(1
     &,4),
     & REAL(R_ufili,4),I_ilili,REAL(R_efili,4),
     & REAL(R_ifili,4),REAL(R_idili,4),
     & REAL(R_edili,4),REAL(R_ekili,4),L_ikili,REAL(R_okili
     &,4),L_ukili,
     & L_alili,R_ofili,REAL(R_afili,4),REAL(R_udili,4),L_elili
     &)
      !}
C FDA60b_vlv.fgi( 258, 429):���������� ������� ��� 2,20FDA60CF012XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_emili,R_esili,REAL(1
     &,4),
     & REAL(R_ipili,4),I_asili,REAL(R_umili,4),
     & REAL(R_apili,4),REAL(R_amili,4),
     & REAL(R_ulili,4),REAL(R_upili,4),L_arili,REAL(R_erili
     &,4),L_irili,
     & L_orili,R_epili,REAL(R_omili,4),REAL(R_imili,4),L_urili
     &)
      !}
C FDA60b_vlv.fgi( 258, 445):���������� ������� ��� 2,20FDA60CP019XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_usili,R_uxili,REAL(1
     &,4),
     & REAL(R_avili,4),I_oxili,REAL(R_itili,4),
     & REAL(R_otili,4),REAL(R_osili,4),
     & REAL(R_isili,4),REAL(R_ivili,4),L_ovili,REAL(R_uvili
     &,4),L_axili,
     & L_exili,R_utili,REAL(R_etili,4),REAL(R_atili,4),L_ixili
     &)
      !}
C FDA60b_vlv.fgi( 258, 462):���������� ������� ��� 2,20FDA60CT007XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_iboli,R_ikoli,REAL(1
     &,4),
     & REAL(R_odoli,4),I_ekoli,REAL(R_adoli,4),
     & REAL(R_edoli,4),REAL(R_eboli,4),
     & REAL(R_aboli,4),REAL(R_afoli,4),L_efoli,REAL(R_ifoli
     &,4),L_ofoli,
     & L_ufoli,R_idoli,REAL(R_uboli,4),REAL(R_oboli,4),L_akoli
     &)
      !}
C FDA60b_vlv.fgi( 258, 476):���������� ������� ��� 2,20FDA60CF004XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_aloli,R_aroli,REAL(1
     &,4),
     & REAL(R_emoli,4),I_upoli,REAL(R_ololi,4),
     & REAL(R_uloli,4),REAL(R_ukoli,4),
     & REAL(R_okoli,4),REAL(R_omoli,4),L_umoli,REAL(R_apoli
     &,4),L_epoli,
     & L_ipoli,R_amoli,REAL(R_iloli,4),REAL(R_eloli,4),L_opoli
     &)
      !}
C FDA60b_vlv.fgi( 258, 492):���������� ������� ��� 2,20FDA60CF003XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_oroli,R_ovoli,REAL(1
     &,4),
     & REAL(R_usoli,4),I_ivoli,REAL(R_esoli,4),
     & REAL(R_isoli,4),REAL(R_iroli,4),
     & REAL(R_eroli,4),REAL(R_etoli,4),L_itoli,REAL(R_otoli
     &,4),L_utoli,
     & L_avoli,R_osoli,REAL(R_asoli,4),REAL(R_uroli,4),L_evoli
     &)
      !}
C FDA60b_vlv.fgi( 258, 507):���������� ������� ��� 2,20FDA60CP013XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_exoli,R_efuli,REAL(1
     &,4),
     & REAL(R_ibuli,4),I_afuli,REAL(R_uxoli,4),
     & REAL(R_abuli,4),REAL(R_axoli,4),
     & REAL(R_uvoli,4),REAL(R_ubuli,4),L_aduli,REAL(R_eduli
     &,4),L_iduli,
     & L_oduli,R_ebuli,REAL(R_oxoli,4),REAL(R_ixoli,4),L_uduli
     &)
      !}
C FDA60b_vlv.fgi( 258, 523):���������� ������� ��� 2,20FDA60CF007XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ufuli,R_umuli,REAL(1
     &,4),
     & REAL(R_aluli,4),I_omuli,REAL(R_ikuli,4),
     & REAL(R_okuli,4),REAL(R_ofuli,4),
     & REAL(R_ifuli,4),REAL(R_iluli,4),L_oluli,REAL(R_ululi
     &,4),L_amuli,
     & L_emuli,R_ukuli,REAL(R_ekuli,4),REAL(R_akuli,4),L_imuli
     &)
      !}
C FDA60b_vlv.fgi( 228, 398):���������� ������� ��� 2,20FDA60CF008XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ipuli,R_ituli,REAL(1
     &,4),
     & REAL(R_oruli,4),I_etuli,REAL(R_aruli,4),
     & REAL(R_eruli,4),REAL(R_epuli,4),
     & REAL(R_apuli,4),REAL(R_asuli,4),L_esuli,REAL(R_isuli
     &,4),L_osuli,
     & L_usuli,R_iruli,REAL(R_upuli,4),REAL(R_opuli,4),L_atuli
     &)
      !}
C FDA60b_vlv.fgi( 228, 429):���������� ������� ��� 2,20FDA60CP014XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_avuli,R_adami,REAL(1
     &,4),
     & REAL(R_exuli,4),I_ubami,REAL(R_ovuli,4),
     & REAL(R_uvuli,4),REAL(R_utuli,4),
     & REAL(R_otuli,4),REAL(R_oxuli,4),L_uxuli,REAL(R_abami
     &,4),L_ebami,
     & L_ibami,R_axuli,REAL(R_ivuli,4),REAL(R_evuli,4),L_obami
     &)
      !}
C FDA60b_vlv.fgi( 228, 414):���������� ������� ��� 2,20FDA60CF005XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_odami,R_olami,REAL(1
     &,4),
     & REAL(R_ufami,4),I_ilami,REAL(R_efami,4),
     & REAL(R_ifami,4),REAL(R_idami,4),
     & REAL(R_edami,4),REAL(R_ekami,4),L_ikami,REAL(R_okami
     &,4),L_ukami,
     & L_alami,R_ofami,REAL(R_afami,4),REAL(R_udami,4),L_elami
     &)
      !}
C FDA60b_vlv.fgi( 228, 445):���������� ������� ��� 2,20FDA60CF006XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_emami,R_esami,REAL(1
     &,4),
     & REAL(R_ipami,4),I_asami,REAL(R_umami,4),
     & REAL(R_apami,4),REAL(R_amami,4),
     & REAL(R_ulami,4),REAL(R_upami,4),L_arami,REAL(R_erami
     &,4),L_irami,
     & L_orami,R_epami,REAL(R_omami,4),REAL(R_imami,4),L_urami
     &)
      !}
C FDA60b_vlv.fgi( 228, 462):���������� ������� ��� 2,20FDA60CP012XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_usami,R_uxami,REAL(1
     &,4),
     & REAL(R_avami,4),I_oxami,REAL(R_itami,4),
     & REAL(R_otami,4),REAL(R_osami,4),
     & REAL(R_isami,4),REAL(R_ivami,4),L_ovami,REAL(R_uvami
     &,4),L_axami,
     & L_exami,R_utami,REAL(R_etami,4),REAL(R_atami,4),L_ixami
     &)
      !}
C FDA60b_vlv.fgi( 228, 477):���������� ������� ��� 2,20FDA60CT006XQ01
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ibemi,R_ikemi,REAL(1
     &,4),
     & REAL(R_odemi,4),I_ekemi,REAL(R_ademi,4),
     & REAL(R_edemi,4),REAL(R_ebemi,4),
     & REAL(R_abemi,4),REAL(R_afemi,4),L_efemi,REAL(R_ifemi
     &,4),L_ofemi,
     & L_ufemi,R_idemi,REAL(R_ubemi,4),REAL(R_obemi,4),L_akemi
     &)
      !}
C FDA60b_vlv.fgi( 228, 492):���������� ������� ��� 2,20FDA60CP010XQ01
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_upemi,4),REAL
     &(R_ememi,4),R_alemi,
     & R_aremi,REAL(1,4),REAL(R_olemi,4),
     & REAL(R_ulemi,4),REAL(R_ukemi,4),
     & REAL(R_okemi,4),I_opemi,REAL(R_imemi,4),L_omemi,
     & REAL(R_umemi,4),L_apemi,L_epemi,R_amemi,REAL(R_ilemi
     &,4),REAL(R_elemi,4),L_ipemi)
      !}
C FDA60b_vlv.fgi( 172, 507):���������� ������� ��������,20FDA60CP009XQ01
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ovemi,4),REAL
     &(R_atemi,4),R_uremi,
     & R_uvemi,REAL(1,4),REAL(R_isemi,4),
     & REAL(R_osemi,4),REAL(R_oremi,4),
     & REAL(R_iremi,4),I_ivemi,REAL(R_etemi,4),L_itemi,
     & REAL(R_otemi,4),L_utemi,L_avemi,R_usemi,REAL(R_esemi
     &,4),REAL(R_asemi,4),L_evemi)
      !}
C FDA60b_vlv.fgi( 228, 523):���������� ������� ��������,20FDA60CP011XQ01
      End
