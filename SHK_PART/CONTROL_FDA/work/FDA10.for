      Subroutine FDA10(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDA10.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      Call FDA10_ConIn
      L_(57)=f
C FDA10_logic.fgi( 264, 421):pull: ���������� ������� ���������,20FDA13EC001
      R_(59)=R0_atuv
C FDA10_logic.fgi( 213, 674):pre: �������� ��������� ������
      R_(29)=R8_ox
C FDA10_vent_log.fgi( 229, 420):pre: �������������� �����  
      R_(98)=R0_etax
C FDA10_logic.fgi( 242, 404):pre: �������� ��������� ������
      R_(109)=R0_abex
C FDA10_logic.fgi( 143, 495):pre: �������� ��������� ������
      R_(108)=R0_oxax
C FDA10_logic.fgi( 143, 476):pre: �������� ��������� ������
      R_(110)=R0_ibex
C FDA10_logic.fgi( 143, 516):pre: �������� ��������� ������
      R_(127)=R0_akix
C FDA10_logic.fgi(  51, 751):pre: ������������  �� T
      R_(126)=R0_ifix
C FDA10_logic.fgi(  51, 744):pre: ������������  �� T
      R_(125)=R0_udix
C FDA10_logic.fgi(  51, 736):pre: ������������  �� T
      R_(124)=R0_edix
C FDA10_logic.fgi(  51, 729):pre: ������������  �� T
      R_(123)=R0_obix
C FDA10_logic.fgi(  51, 722):pre: ������������  �� T
      R_(76)=R0_upax
C FDA10_logic.fgi(  68, 366):pre: ������������  �� T
      R_(64)=R0_ulax
C FDA10_logic.fgi(  62, 307):pre: ������������  �� T
      R_(20)=R8_is
C FDA10_vent_log.fgi( 340, 471):pre: �������������� �����  
      R_(44)=R8_upe
C FDA10_vent_log.fgi(  62, 471):pre: �������������� �����  
      R_(43)=R8_ume
C FDA10_vent_log.fgi(  67, 394):pre: �������������� �����  
      R_(36)=R8_ife
C FDA10_vent_log.fgi( 158, 471):pre: �������������� �����  
      R_(26)=R8_ov
C FDA10_vent_log.fgi( 252, 471):pre: �������������� �����  
      R_(1)=R0_i
C FDA10_vent_log.fgi( 262, 359):pre: �������� ��������� ������
      R_(16)=R8_ap
C FDA10_vent_log.fgi( 398, 336):pre: �������������� �����  
      R_(2)=R8_ed
C FDA10_vent_log.fgi( 435, 471):pre: �������������� �����  
      R_(6)=R8_of
C FDA10_vent_log.fgi( 421, 420):pre: �������������� �����  
      R_(5)=R8_ef
C FDA10_vent_log.fgi( 421, 404):pre: �������������� �����  
      !{
      Call ZATVOR_MAN(deltat,REAL(R_iso,4),R8_ipo,I_uvo,I_ixo
     &,I_ovo,C8_aro,
     & I_exo,R_uro,R_oro,R_ilo,REAL(R_ulo,4),
     & R_omo,REAL(R_apo,4),R_amo,
     & REAL(R_imo,4),I_evo,I_oxo,I_axo,I_avo,L_epo,L_abu,
     & L_adu,L_umo,L_upo,L_opo,L_ibu,
     & L_emo,L_olo,L_iro,L_odu,L_aso,L_eso,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_(17),L_oko,L_
     &(16),L_uko,
     & L_ebu,I_uxo,L_edu,R_uto,REAL(R_ero,4),L_idu,L_alo,L_udu
     &,
     & L_elo,L_oso,L_uso,L_ato,L_ito,L_oto,L_eto)
      !}

      if(L_oto.or.L_ito.or.L_eto.or.L_ato.or.L_uso.or.L_oso
     &) then      
                  I_ivo = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_ivo = z'40000000'
      endif
C FDA10_vlv.fgi( 366, 149):���� ���������� ��������,20FDA13AA101
      !{
      Call ZATVOR_MAN(deltat,REAL(R_ital,4),R8_opax,I_uxal
     &,I_ibel,I_oxal,C8_asal,
     & I_ebel,R_usal,R_osal,R_omal,REAL(R_apal,4),
     & R_upal,REAL(R_eral,4),R_epal,
     & REAL(R_opal,4),I_exal,I_obel,I_abel,I_axal,L_iral,L_adel
     &,
     & L_afel,L_aral,L_ural,L_oral,L_idel,
     & L_ipal,L_umal,L_isal,L_ofel,L_atal,L_etal,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_(19),L_ulal,L_
     &(18),L_amal,
     & L_edel,I_ubel,L_efel,R_uval,REAL(R_esal,4),L_ifel,L_emal
     &,L_ufel,
     & L_imal,L_otal,L_utal,L_aval,L_ival,L_oval,L_eval)
      !}

      if(L_oval.or.L_ival.or.L_eval.or.L_aval.or.L_utal.or.L_otal
     &) then      
                  I_ixal = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ixal = z'40000000'
      endif
C FDA10_vlv.fgi( 349, 149):���� ���������� ��������,20FDA13AA202
      R_(74) = R8_opax
C FDA10_logic.fgi(  49, 338):��������
      !{
      Call ZATVOR_MAN(deltat,REAL(R_orel,4),R8_orax,I_avel
     &,I_ovel,I_utel,C8_epel,
     & I_ivel,R_arel,R_upel,R_ukel,REAL(R_elel,4),
     & R_amel,REAL(R_imel,4),R_ilel,
     & REAL(R_ulel,4),I_itel,I_uvel,I_evel,I_etel,L_omel,L_exel
     &,
     & L_ebil,L_emel,L_apel,L_umel,L_oxel,
     & L_olel,L_alel,L_opel,L_ubil,L_erel,L_irel,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_(21),L_akel,L_
     &(20),L_ekel,
     & L_ixel,I_axel,L_ibil,R_atel,REAL(R_ipel,4),L_obil,L_ikel
     &,L_adil,
     & L_okel,L_urel,L_asel,L_esel,L_osel,L_usel,L_isel)
      !}

      if(L_usel.or.L_osel.or.L_isel.or.L_esel.or.L_asel.or.L_urel
     &) then      
                  I_otel = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_otel = z'40000000'
      endif
C FDA10_vlv.fgi( 332, 149):���� ���������� ��������,20FDA13AA201
      R_(75) = R8_orax
C FDA10_logic.fgi(  49, 342):��������
      R_(78) = R_(75) * R_(74)
C FDA10_logic.fgi(  61, 341):����������
      R_udo=R_upel
C FDA10_init.fgi( 326, 177):������,20FDA13CG001XQ01_input
      !{
      Call DAT_ANA_HANDLER(deltat,R_obo,R_iko,REAL(1,4),
     & REAL(R_edo,4),REAL(R_ido,4),
     & REAL(R_ibo,4),REAL(R_ebo,4),I_eko,
     & REAL(R_afo,4),L_efo,REAL(R_ifo,4),L_ofo,L_ufo,R_odo
     &,
     & REAL(R_ado,4),REAL(R_ubo,4),L_ako,REAL(R_udo,4))
      !}
C FDA10_vlv.fgi( 143,  56):���������� �������,20FDA13CG001XQ01
      !��������� R_(4) = FDA10_vent_logC?? /0.0/
      R_(4)=R0_ud
C FDA10_vent_log.fgi( 421, 472):���������
      !��������� R_(10) = FDA10_vent_logC?? /0.001/
      R_(10)=R0_ok
C FDA10_vent_log.fgi( 406, 403):���������
      !��������� R_(11) = FDA10_vent_logC?? /0.0/
      R_(11)=R0_uk
C FDA10_vent_log.fgi( 406, 405):���������
      if(L_op) then
         R_(7)=R_(10)
      else
         R_(7)=R_(11)
      endif
C FDA10_vent_log.fgi( 409, 403):���� RE IN LO CH7
      R8_ef=(R0_af*R_(5)+deltat*R_(7))/(R0_af+deltat)
C FDA10_vent_log.fgi( 421, 404):�������������� �����  
      R8_id=R8_ef
C FDA10_vent_log.fgi( 438, 404):������,F_FDA15CU002_G
      !��������� R_(12) = FDA10_vent_logC?? /0.001/
      R_(12)=R0_al
C FDA10_vent_log.fgi( 406, 419):���������
      !��������� R_(13) = FDA10_vent_logC?? /0.0/
      R_(13)=R0_el
C FDA10_vent_log.fgi( 406, 421):���������
      if(L_up) then
         R_(8)=R_(12)
      else
         R_(8)=R_(13)
      endif
C FDA10_vent_log.fgi( 409, 419):���� RE IN LO CH7
      R8_of=(R0_if*R_(6)+deltat*R_(8))/(R0_if+deltat)
C FDA10_vent_log.fgi( 421, 420):�������������� �����  
      R8_od=R8_of
C FDA10_vent_log.fgi( 438, 420):������,F_FDA15CU001_G
      L_ol=L_or.or.(L_ol.and..not.(L_il))
      L_(2)=.not.L_ol
C FDA10_vent_log.fgi( 404, 357):RS �������
      L_as=L_ol
C FDA10_vent_log.fgi( 418, 359):������,20FDA14AH801_CB
      !��������� R_(14) = FDA10_vent_logC?? /0.003/
      R_(14)=R0_ul
C FDA10_vent_log.fgi( 402, 463):���������
      L_(1)=R8_ar.gt.R_(14)
C FDA10_vent_log.fgi( 406, 464):���������� >
      R_(15) = R8_ir * R8_er
C FDA10_vent_log.fgi( 402, 470):����������
      if(R_(15).le.R0_ik) then
         R_(9)=R0_ek
      elseif(R_(15).gt.R0_ak) then
         R_(9)=R0_uf
      else
         R_(9)=R0_ek+(R_(15)-(R0_ik))*(R0_uf-(R0_ek))/(R0_ak
     &-(R0_ik))
      endif
C FDA10_vent_log.fgi( 409, 471):��������������� ���������
      if(L_(1)) then
         R_(3)=R_(9)
      else
         R_(3)=R_(4)
      endif
C FDA10_vent_log.fgi( 423, 470):���� RE IN LO CH7
      R8_ed=(R0_ad*R_(2)+deltat*R_(3))/(R0_ad+deltat)
C FDA10_vent_log.fgi( 435, 471):�������������� �����  
      R8_u=R8_ed
C FDA10_vent_log.fgi( 450, 471):������,20FDA15CF501_G
      !��������� R_(18) = FDA10_vent_logC?? /10000/
      R_(18)=R0_ep
C FDA10_vent_log.fgi( 385, 335):���������
      !��������� R_(19) = FDA10_vent_logC?? /0.0/
      R_(19)=R0_ip
C FDA10_vent_log.fgi( 385, 337):���������
      if(L_as) then
         R_(17)=R_(18)
      else
         R_(17)=R_(19)
      endif
C FDA10_vent_log.fgi( 388, 335):���� RE IN LO CH7
      R8_ap=(R0_um*R_(16)+deltat*R_(17))/(R0_um+deltat)
C FDA10_vent_log.fgi( 398, 336):�������������� �����  
      R8_ur=R8_ap
C FDA10_vent_log.fgi( 415, 336):������,20FDA14AH801_QIn
      if(R8_ur.le.R0_om) then
         R_usav=R0_im
      elseif(R8_ur.gt.R0_em) then
         R_usav=R0_am
      else
         R_usav=R0_im+(R8_ur-(R0_om))*(R0_am-(R0_im))/(R0_em
     &-(R0_om))
      endif
C FDA10_vent_log.fgi( 401, 319):��������������� ���������
      !{
      Call DAT_ANA_HANDLER(deltat,R_orav,R_ivav,REAL(1,4)
     &,
     & REAL(R_esav,4),REAL(R_isav,4),
     & REAL(R_irav,4),REAL(R_erav,4),I_evav,
     & REAL(R_atav,4),L_etav,REAL(R_itav,4),L_otav,L_utav
     &,R_osav,
     & REAL(R_asav,4),REAL(R_urav,4),L_avav,REAL(R_usav,4
     &))
      !}
C FDA10_vlv.fgi( 142, 166):���������� �������,20FDA14CE002XQ01
      !��������� R_(22) = FDA10_vent_logC?? /0.0/
      R_(22)=R0_os
C FDA10_vent_log.fgi( 326, 472):���������
      !��������� R_(24) = FDA10_vent_logC?? /0.003/
      R_(24)=R0_ot
C FDA10_vent_log.fgi( 307, 463):���������
      L_(3)=R8_ut.gt.R_(24)
C FDA10_vent_log.fgi( 311, 464):���������� >
      L0_uv=L_ube.or.(L0_uv.and..not.(L_obe))
      L_(4)=.not.L0_uv
C FDA10_vent_log.fgi( 247, 357):RS �������
      if(.not.L0_uv) then
         R0_i=0.0
      elseif(.not.L0_o) then
         R0_i=R0_e
      else
         R0_i=max(R_(1)-deltat,0.0)
      endif
      L_ev=L0_uv.and.R0_i.le.0.0
      L0_o=L0_uv
C FDA10_vent_log.fgi( 262, 359):�������� ��������� ������
      !��������� R_(28) = FDA10_vent_logC?? /0.0/
      R_(28)=R0_ax
C FDA10_vent_log.fgi( 238, 472):���������
      !��������� R_(31) = FDA10_vent_logC?? /0.003/
      R_(31)=R0_ade
C FDA10_vent_log.fgi( 219, 463):���������
      L_(5)=R8_ode.gt.R_(31)
C FDA10_vent_log.fgi( 223, 464):���������� >
      R_(32) = R8_afe * R8_ude
C FDA10_vent_log.fgi( 219, 470):����������
      if(R_(32).le.R0_ibe) then
         R_(30)=R0_ebe
      elseif(R_(32).gt.R0_abe) then
         R_(30)=R0_ux
      else
         R_(30)=R0_ebe+(R_(32)-(R0_ibe))*(R0_ux-(R0_ebe))
     &/(R0_abe-(R0_ibe))
      endif
C FDA10_vent_log.fgi( 226, 471):��������������� ���������
      if(L_(5)) then
         R_(27)=R_(30)
      else
         R_(27)=R_(28)
      endif
C FDA10_vent_log.fgi( 240, 470):���� RE IN LO CH7
      R8_ov=(R0_iv*R_(26)+deltat*R_(27))/(R0_iv+deltat)
C FDA10_vent_log.fgi( 252, 471):�������������� �����  
      R8_av=R8_ov
C FDA10_vent_log.fgi( 266, 471):������,20FDA13CF501_G
      !��������� R_(34) = FDA10_vent_logC?? /0.001/
      R_(34)=R0_ede
C FDA10_vent_log.fgi( 214, 419):���������
      !��������� R_(35) = FDA10_vent_logC?? /0.0/
      R_(35)=R0_ide
C FDA10_vent_log.fgi( 214, 421):���������
      !��������� R_(38) = FDA10_vent_logC?? /0.0/
      R_(38)=R0_ofe
C FDA10_vent_log.fgi( 144, 472):���������
      !��������� R_(40) = FDA10_vent_logC?? /0.003/
      R_(40)=R0_oke
C FDA10_vent_log.fgi( 125, 463):���������
      L_(6)=R8_uke.gt.R_(40)
C FDA10_vent_log.fgi( 129, 464):���������� >
      R_(41) = R8_ele * R8_ale
C FDA10_vent_log.fgi( 125, 470):����������
      if(R_(41).le.R0_ike) then
         R_(39)=R0_eke
      elseif(R_(41).gt.R0_ake) then
         R_(39)=R0_ufe
      else
         R_(39)=R0_eke+(R_(41)-(R0_ike))*(R0_ufe-(R0_eke)
     &)/(R0_ake-(R0_ike))
      endif
C FDA10_vent_log.fgi( 132, 471):��������������� ���������
      if(L_(6)) then
         R_(37)=R_(39)
      else
         R_(37)=R_(38)
      endif
C FDA10_vent_log.fgi( 146, 470):���� RE IN LO CH7
      R8_ife=(R0_efe*R_(36)+deltat*R_(37))/(R0_efe+deltat
     &)
C FDA10_vent_log.fgi( 158, 471):�������������� �����  
      R8_ebi=R8_ife
C FDA10_vent_log.fgi( 172, 471):������,20FDA12CF501_G
      R_osif = R8_ebi
C FDA10_init.fgi( 133, 186):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_irif,R_evif,REAL(1,4)
     &,
     & REAL(R_asif,4),REAL(R_esif,4),
     & REAL(R_erif,4),REAL(R_arif,4),I_avif,
     & REAL(R_usif,4),L_atif,REAL(R_etif,4),L_itif,L_otif
     &,R_isif,
     & REAL(R_urif,4),REAL(R_orif,4),L_utif,REAL(R_osif,4
     &))
      !}
C FDA10_vlv.fgi(  33,  87):���������� �������,20FDA12CF501XQ01
      !��������� R_(42) = FDA10_vent_logC?? /-50000/
      R_(42)=R0_ame
C FDA10_vent_log.fgi(  82, 351):���������
      R_(47) = 1.0
C FDA10_vent_log.fgi(  54, 392):��������� (RE4) (�������)
      R_(48) = 0.0
C FDA10_vent_log.fgi(  54, 394):��������� (RE4) (�������)
      !��������� R_(49) = FDA10_vent_logC?? /500/
      R_(49)=R0_ere
C FDA10_vent_log.fgi(  54, 352):���������
      !��������� R_(50) = FDA10_vent_logC?? /0.0/
      R_(50)=R0_ire
C FDA10_vent_log.fgi(  54, 354):���������
      !��������� R_(51) = FDA10_vent_logC?? /1/
      R_(51)=R0_ore
C FDA10_vent_log.fgi(  53, 372):���������
      L_(10)=R8_ete.lt.R_(51)
C FDA10_vent_log.fgi(  57, 373):���������� <
      L_ole=L_epe.or.(L_ole.and..not.(L_(10)))
      L_(7)=.not.L_ole
C FDA10_vent_log.fgi(  87, 375):RS �������
      L_ape=L_ole
C FDA10_vent_log.fgi( 101, 377):������,20FDA13CW001_OUT
      !��������� R_(53) = FDA10_vent_logC?? /2.6/
      R_(53)=R0_ure
C FDA10_vent_log.fgi(  49, 413):���������
      L_(11)=R_ave.gt.R_(53)
C FDA10_vent_log.fgi(  53, 414):���������� >
      L_(9) = L_(11).OR.L_are
C FDA10_vent_log.fgi(  78, 413):���
      L_ule=L_ipe.or.(L_ule.and..not.(L_(9)))
      L_(8)=.not.L_ule
C FDA10_vent_log.fgi(  84, 415):RS �������
      L_ote=L_ule
C FDA10_vent_log.fgi(  98, 417):������,FDA13dust_start
      if(L_ote) then
         R_(45)=R_(47)
      else
         R_(45)=R_(48)
      endif
C FDA10_vent_log.fgi(  57, 393):���� RE IN LO CH7
      R8_ume=(R0_ome*R_(43)+deltat*R_(45))/(R0_ome+deltat
     &)
C FDA10_vent_log.fgi(  67, 394):�������������� �����  
      R8_eme=R8_ume
C FDA10_vent_log.fgi(  86, 394):������,20FDA13AA001
      !��������� R_(54) = FDA10_vent_logC?? /0.0/
      R_(54)=R0_ase
C FDA10_vent_log.fgi(  48, 472):���������
      !��������� R_(55) = FDA10_vent_logC?? /0.0005/
      R_(55)=R0_ese
C FDA10_vent_log.fgi(  44, 347):���������
      L_(12)=R8_ite.gt.R_(55)
C FDA10_vent_log.fgi(  48, 348):���������� >
      if(L_(12)) then
         R_(46)=R_(49)
      else
         R_(46)=R_(50)
      endif
C FDA10_vent_log.fgi(  57, 352):���� RE IN LO CH7
      if(L_ape) then
         R8_ile=R_(42)
      else
         R8_ile=R_(46)
      endif
C FDA10_vent_log.fgi(  85, 351):���� RE IN LO CH7
      !��������� R_(57) = FDA10_vent_logC?? /0.003/
      R_(57)=R0_ute
C FDA10_vent_log.fgi(  29, 463):���������
      L_(13)=R8_eve.gt.R_(57)
C FDA10_vent_log.fgi(  33, 464):���������� >
      R_oku = R8_ive
C FDA10_init.fgi( 305, 182):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ifu,R_emu,REAL(1,4),
     & REAL(R_aku,4),REAL(R_eku,4),
     & REAL(R_efu,4),REAL(R_afu,4),I_amu,
     & REAL(R_uku,4),L_alu,REAL(R_elu,4),L_ilu,L_olu,R_iku
     &,
     & REAL(R_ufu,4),REAL(R_ofu,4),L_ulu,REAL(R_oku,4))
      !}
C FDA10_vlv.fgi( 117,  56):���������� �������,20FDA13CT001XQ01
      R_efad = R8_ove
C FDA10_init.fgi( 305, 186):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_adad,R_ukad,REAL(1,4)
     &,
     & REAL(R_odad,4),REAL(R_udad,4),
     & REAL(R_ubad,4),REAL(R_obad,4),I_okad,
     & REAL(R_ifad,4),L_ofad,REAL(R_ufad,4),L_akad,L_ekad
     &,R_afad,
     & REAL(R_idad,4),REAL(R_edad,4),L_ikad,REAL(R_efad,4
     &))
      !}
C FDA10_vlv.fgi(  33,  56):���������� �������,20FDA13CF001XQ01
      R_aru = R8_uve
C FDA10_init.fgi( 305, 194):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_osu,4),REAL
     &(R_aru,4),R_umu,
     & R_usu,REAL(-1,4),REAL(R_ipu,4),
     & REAL(R_opu,4),REAL(R_omu,4),
     & REAL(R_imu,4),I_isu,REAL(R_eru,4),L_iru,
     & REAL(R_oru,4),L_uru,L_asu,R_upu,REAL(R_epu,4),REAL
     &(R_apu,4),L_esu)
      !}
C FDA10_vlv.fgi(  89,  56):���������� ������� ��������,20FDA13CP001XQ01
      R_uvu = R8_axe
C FDA10_init.fgi( 305, 190):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_otu,R_ibad,REAL(1,4),
     & REAL(R_evu,4),REAL(R_ivu,4),
     & REAL(R_itu,4),REAL(R_etu,4),I_ebad,
     & REAL(R_axu,4),L_exu,REAL(R_ixu,4),L_oxu,L_uxu,R_ovu
     &,
     & REAL(R_avu,4),REAL(R_utu,4),L_abad,REAL(R_uvu,4))
      !}
C FDA10_vlv.fgi(  61,  56):���������� �������,20FDA13CP002XQ01
      R_asaf = R8_exe
C FDA10_init.fgi( 217, 182):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_upaf,R_otaf,REAL(1,4)
     &,
     & REAL(R_iraf,4),REAL(R_oraf,4),
     & REAL(R_opaf,4),REAL(R_ipaf,4),I_itaf,
     & REAL(R_esaf,4),L_isaf,REAL(R_osaf,4),L_usaf,L_ataf
     &,R_uraf,
     & REAL(R_eraf,4),REAL(R_araf,4),L_etaf,REAL(R_asaf,4
     &))
      !}
C FDA10_vlv.fgi( 117,  72):���������� �������,20FDA14CT001XQ01
      R_ixaf = R8_oxe
C FDA10_init.fgi( 217, 194):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_adef,4),REAL
     &(R_ixaf,4),R_evaf,
     & R_edef,REAL(-1,4),REAL(R_uvaf,4),
     & REAL(R_axaf,4),REAL(R_avaf,4),
     & REAL(R_utaf,4),I_ubef,REAL(R_oxaf,4),L_uxaf,
     & REAL(R_abef,4),L_ebef,L_ibef,R_exaf,REAL(R_ovaf,4)
     &,REAL(R_ivaf,4),L_obef)
      !}
C FDA10_vlv.fgi(  89,  72):���������� ������� ��������,20FDA14CP001XQ01
      R_ekef = R8_uxe
C FDA10_init.fgi( 217, 190):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_afef,R_ulef,REAL(1,4)
     &,
     & REAL(R_ofef,4),REAL(R_ufef,4),
     & REAL(R_udef,4),REAL(R_odef,4),I_olef,
     & REAL(R_ikef,4),L_okef,REAL(R_ukef,4),L_alef,L_elef
     &,R_akef,
     & REAL(R_ifef,4),REAL(R_efef,4),L_ilef,REAL(R_ekef,4
     &))
      !}
C FDA10_vlv.fgi(  61,  72):���������� �������,20FDA14CP002XQ01
      R_avef = R8_abi
C FDA10_init.fgi( 133, 182):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_usef,R_oxef,REAL(1,4)
     &,
     & REAL(R_itef,4),REAL(R_otef,4),
     & REAL(R_osef,4),REAL(R_isef,4),I_ixef,
     & REAL(R_evef,4),L_ivef,REAL(R_ovef,4),L_uvef,L_axef
     &,R_utef,
     & REAL(R_etef,4),REAL(R_atef,4),L_exef,REAL(R_avef,4
     &))
      !}
C FDA10_vlv.fgi( 117,  87):���������� �������,20FDA12CT001XQ01
      R_idif = R8_ibi
C FDA10_init.fgi( 133, 194):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_akif,4),REAL
     &(R_idif,4),R_ebif,
     & R_ekif,REAL(-1,4),REAL(R_ubif,4),
     & REAL(R_adif,4),REAL(R_abif,4),
     & REAL(R_uxef,4),I_ufif,REAL(R_odif,4),L_udif,
     & REAL(R_afif,4),L_efif,L_ifif,R_edif,REAL(R_obif,4)
     &,REAL(R_ibif,4),L_ofif)
      !}
C FDA10_vlv.fgi(  89,  87):���������� ������� ��������,20FDA12CP001XQ01
      R_emif = R8_obi
C FDA10_init.fgi( 133, 190):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_alif,R_upif,REAL(1,4)
     &,
     & REAL(R_olif,4),REAL(R_ulif,4),
     & REAL(R_ukif,4),REAL(R_okif,4),I_opif,
     & REAL(R_imif,4),L_omif,REAL(R_umif,4),L_apif,L_epif
     &,R_amif,
     & REAL(R_ilif,4),REAL(R_elif,4),L_ipif,REAL(R_emif,4
     &))
      !}
C FDA10_vlv.fgi(  61,  87):���������� �������,20FDA12CP002XQ01
      R_adam = R8_ubi
C FDA10_init.fgi(  46, 182):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_uxul,R_ofam,REAL(1,4)
     &,
     & REAL(R_ibam,4),REAL(R_obam,4),
     & REAL(R_oxul,4),REAL(R_ixul,4),I_ifam,
     & REAL(R_edam,4),L_idam,REAL(R_odam,4),L_udam,L_afam
     &,R_ubam,
     & REAL(R_ebam,4),REAL(R_abam,4),L_efam,REAL(R_adam,4
     &))
      !}
C FDA10_vlv.fgi( 117, 102):���������� �������,20FDA11CT001XQ01
      R_ilam = R8_edi
C FDA10_init.fgi(  46, 194):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_apam,4),REAL
     &(R_ilam,4),R_ekam,
     & R_epam,REAL(-1,4),REAL(R_ukam,4),
     & REAL(R_alam,4),REAL(R_akam,4),
     & REAL(R_ufam,4),I_umam,REAL(R_olam,4),L_ulam,
     & REAL(R_amam,4),L_emam,L_imam,R_elam,REAL(R_okam,4)
     &,REAL(R_ikam,4),L_omam)
      !}
C FDA10_vlv.fgi(  89, 102):���������� ������� ��������,20FDA11CP001XQ01
      R_esam = R8_idi
C FDA10_init.fgi(  46, 190):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_aram,R_utam,REAL(1,4)
     &,
     & REAL(R_oram,4),REAL(R_uram,4),
     & REAL(R_upam,4),REAL(R_opam,4),I_otam,
     & REAL(R_isam,4),L_osam,REAL(R_usam,4),L_atam,L_etam
     &,R_asam,
     & REAL(R_iram,4),REAL(R_eram,4),L_itam,REAL(R_esam,4
     &))
      !}
C FDA10_vlv.fgi(  61, 102):���������� �������,20FDA11CP002XQ01
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_usad,4),R8_upad
     &,I_exad,I_uxad,I_axad,
     & C8_irad,I_oxad,R_esad,R_asad,R_ulad,
     & REAL(R_emad,4),R_apad,REAL(R_ipad,4),
     & R_imad,REAL(R_umad,4),I_ovad,I_abed,I_ixad,I_ivad,L_opad
     &,
     & L_ibed,L_ided,L_epad,L_erad,L_arad,L_ubed,
     & L_omad,L_amad,L_urad,L_afed,L_isad,L_osad,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_alad,L_elad,L_obed
     &,I_ebed,
     & L_oded,R_evad,REAL(R_orad,4),L_uded,L_ilad,L_efed,L_olad
     &,L_atad,
     & L_etad,L_itad,L_utad,L_avad,L_otad)
      !}

      if(L_avad.or.L_utad.or.L_otad.or.L_itad.or.L_etad.or.L_atad
     &) then      
                  I_uvad = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_uvad = z'40000000'
      endif
C FDA10_vlv.fgi( 381,  82):���� ���������� �������� � �����������������,20FDA14AA203
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_ered,4),R8_emed
     &,I_oted,I_eved,I_ited,
     & C8_umed,I_aved,R_oped,R_iped,R_eked,
     & REAL(R_oked,4),R_iled,REAL(R_uled,4),
     & R_uked,REAL(R_eled,4),I_ated,I_ived,I_uted,I_used,L_amed
     &,
     & L_uved,L_uxed,L_oled,L_omed,L_imed,L_exed,
     & L_aled,L_iked,L_eped,L_ibid,L_uped,L_ared,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_ifed,L_ofed,L_axed
     &,I_oved,
     & L_abid,R_osed,REAL(R_aped,4),L_ebid,L_ufed,L_obid,L_aked
     &,L_ired,
     & L_ored,L_ured,L_esed,L_ised,L_ased)
      !}

      if(L_ised.or.L_esed.or.L_ased.or.L_ured.or.L_ored.or.L_ired
     &) then      
                  I_eted = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_eted = z'40000000'
      endif
C FDA10_vlv.fgi( 352,  82):���� ���������� �������� � �����������������,20FDA14AA201
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_omid,4),R8_okid
     &,I_asid,I_osid,I_urid,
     & C8_elid,I_isid,R_amid,R_ulid,R_odid,
     & REAL(R_afid,4),R_ufid,REAL(R_ekid,4),
     & R_efid,REAL(R_ofid,4),I_irid,I_usid,I_esid,I_erid,L_ikid
     &,
     & L_etid,L_evid,L_akid,L_alid,L_ukid,L_otid,
     & L_ifid,L_udid,L_olid,L_uvid,L_emid,L_imid,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_ubid,L_adid,L_itid
     &,I_atid,
     & L_ivid,R_arid,REAL(R_ilid,4),L_ovid,L_edid,L_axid,L_idid
     &,L_umid,
     & L_apid,L_epid,L_opid,L_upid,L_ipid)
      !}

      if(L_upid.or.L_opid.or.L_ipid.or.L_epid.or.L_apid.or.L_umid
     &) then      
                  I_orid = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_orid = z'40000000'
      endif
C FDA10_vlv.fgi( 367,  82):���� ���������� �������� � �����������������,20FDA14AA202
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_alod,4),R8_afod
     &,I_ipod,I_arod,I_epod,
     & C8_ofod,I_upod,R_ikod,R_ekod,R_abod,
     & REAL(R_ibod,4),R_edod,REAL(R_odod,4),
     & R_obod,REAL(R_adod,4),I_umod,I_erod,I_opod,I_omod,L_udod
     &,
     & L_orod,L_osod,L_idod,L_ifod,L_efod,L_asod,
     & L_ubod,L_ebod,L_akod,L_etod,L_okod,L_ukod,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_exid,L_ixid,L_urod
     &,I_irod,
     & L_usod,R_imod,REAL(R_ufod,4),L_atod,L_oxid,L_itod,L_uxid
     &,L_elod,
     & L_ilod,L_olod,L_amod,L_emod,L_ulod)
      !}

      if(L_emod.or.L_amod.or.L_ulod.or.L_olod.or.L_ilod.or.L_elod
     &) then      
                  I_apod = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_apod = z'40000000'
      endif
C FDA10_vlv.fgi( 381, 109):���� ���������� �������� � �����������������,20FDA12AA203
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_ifud,4),R8_ibud
     &,I_ulud,I_imud,I_olud,
     & C8_adud,I_emud,R_udud,R_odud,R_ivod,
     & REAL(R_uvod,4),R_oxod,REAL(R_abud,4),
     & R_axod,REAL(R_ixod,4),I_elud,I_omud,I_amud,I_alud,L_ebud
     &,
     & L_apud,L_arud,L_uxod,L_ubud,L_obud,L_ipud,
     & L_exod,L_ovod,L_idud,L_orud,L_afud,L_efud,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_otod,L_utod,L_epud
     &,I_umud,
     & L_erud,R_ukud,REAL(R_edud,4),L_irud,L_avod,L_urud,L_evod
     &,L_ofud,
     & L_ufud,L_akud,L_ikud,L_okud,L_ekud)
      !}

      if(L_okud.or.L_ikud.or.L_ekud.or.L_akud.or.L_ufud.or.L_ofud
     &) then      
                  I_ilud = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ilud = z'40000000'
      endif
C FDA10_vlv.fgi( 352, 109):���� ���������� �������� � �����������������,20FDA12AA201
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_ubaf,4),R8_uvud
     &,I_ekaf,I_ukaf,I_akaf,
     & C8_ixud,I_okaf,R_ebaf,R_abaf,R_usud,
     & REAL(R_etud,4),R_avud,REAL(R_ivud,4),
     & R_itud,REAL(R_utud,4),I_ofaf,I_alaf,I_ikaf,I_ifaf,L_ovud
     &,
     & L_ilaf,L_imaf,L_evud,L_exud,L_axud,L_ulaf,
     & L_otud,L_atud,L_uxud,L_apaf,L_ibaf,L_obaf,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_asud,L_esud,L_olaf
     &,I_elaf,
     & L_omaf,R_efaf,REAL(R_oxud,4),L_umaf,L_isud,L_epaf,L_osud
     &,L_adaf,
     & L_edaf,L_idaf,L_udaf,L_afaf,L_odaf)
      !}

      if(L_afaf.or.L_udaf.or.L_odaf.or.L_idaf.or.L_edaf.or.L_adaf
     &) then      
                  I_ufaf = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ufaf = z'40000000'
      endif
C FDA10_vlv.fgi( 367, 109):���� ���������� �������� � �����������������,20FDA12AA202
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_ekof,4),R8_edof
     &,I_omof,I_epof,I_imof,
     & C8_udof,I_apof,R_ofof,R_ifof,R_exif,
     & REAL(R_oxif,4),R_ibof,REAL(R_ubof,4),
     & R_uxif,REAL(R_ebof,4),I_amof,I_ipof,I_umof,I_ulof,L_adof
     &,
     & L_upof,L_urof,L_obof,L_odof,L_idof,L_erof,
     & L_abof,L_ixif,L_efof,L_isof,L_ufof,L_akof,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_ivif,L_ovif,L_arof
     &,I_opof,
     & L_asof,R_olof,REAL(R_afof,4),L_esof,L_uvif,L_osof,L_axif
     &,L_ikof,
     & L_okof,L_ukof,L_elof,L_ilof,L_alof)
      !}

      if(L_ilof.or.L_elof.or.L_alof.or.L_ukof.or.L_okof.or.L_ikof
     &) then      
                  I_emof = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_emof = z'40000000'
      endif
C FDA10_vlv.fgi( 284, 126):���� ���������� �������� � �����������������,20FDA11AA201
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_oduf,4),R8_oxof
     &,I_aluf,I_oluf,I_ukuf,
     & C8_ebuf,I_iluf,R_aduf,R_ubuf,R_otof,
     & REAL(R_avof,4),R_uvof,REAL(R_exof,4),
     & R_evof,REAL(R_ovof,4),I_ikuf,I_uluf,I_eluf,I_ekuf,L_ixof
     &,
     & L_emuf,L_epuf,L_axof,L_abuf,L_uxof,L_omuf,
     & L_ivof,L_utof,L_obuf,L_upuf,L_eduf,L_iduf,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_usof,L_atof,L_imuf
     &,I_amuf,
     & L_ipuf,R_akuf,REAL(R_ibuf,4),L_opuf,L_etof,L_aruf,L_itof
     &,L_uduf,
     & L_afuf,L_efuf,L_ofuf,L_ufuf,L_ifuf)
      !}

      if(L_ufuf.or.L_ofuf.or.L_ifuf.or.L_efuf.or.L_afuf.or.L_uduf
     &) then      
                  I_okuf = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_okuf = z'40000000'
      endif
C FDA10_vlv.fgi( 300, 126):���� ���������� �������� � �����������������,20FDA11AA203
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_ivuf,4),REAL
     &(R_usuf,4),R_oruf,
     & R_ovuf,REAL(0.001,4),REAL(R_esuf,4),
     & REAL(R_isuf,4),REAL(R_iruf,4),
     & REAL(R_eruf,4),I_evuf,REAL(R_atuf,4),L_etuf,
     & REAL(R_ituf,4),L_otuf,L_utuf,R_osuf,REAL(R_asuf,4)
     &,REAL(R_uruf,4),L_avuf)
      !}
C FDA10_vlv.fgi( 142, 124):���������� ������� ��������,20FDA14CP003XQ01
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_efak,4),REAL
     &(R_obak,4),R_ixuf,
     & R_ifak,REAL(0.001,4),REAL(R_abak,4),
     & REAL(R_ebak,4),REAL(R_exuf,4),
     & REAL(R_axuf,4),I_afak,REAL(R_ubak,4),L_adak,
     & REAL(R_edak,4),L_idak,L_odak,R_ibak,REAL(R_uxuf,4)
     &,REAL(R_oxuf,4),L_udak)
      !}
C FDA10_vlv.fgi( 115, 124):���������� ������� ��������,20FDA14CP004XQ01
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_umek,4),R8_ukek
     &,I_esek,I_usek,I_asek,
     & C8_ilek,I_osek,R_emek,R_amek,R_udek,
     & REAL(R_efek,4),R_akek,REAL(R_ikek,4),
     & R_ifek,REAL(R_ufek,4),I_orek,I_atek,I_isek,I_irek,L_okek
     &,
     & L_itek,L_ivek,L_ekek,L_elek,L_alek,L_utek,
     & L_ofek,L_afek,L_ulek,L_axek,L_imek,L_omek,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_adek,L_edek,L_otek
     &,I_etek,
     & L_ovek,R_erek,REAL(R_olek,4),L_uvek,L_idek,L_exek,L_odek
     &,L_apek,
     & L_epek,L_ipek,L_upek,L_arek,L_opek)
      !}

      if(L_arek.or.L_upek.or.L_opek.or.L_ipek.or.L_epek.or.L_apek
     &) then      
                  I_urek = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_urek = z'40000000'
      endif
C FDA10_vlv.fgi( 403, 177):���� ���������� �������� � �����������������,20FDA14AN001KA05
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_elik,4),R8_efik
     &,I_opik,I_erik,I_ipik,
     & C8_ufik,I_arik,R_okik,R_ikik,R_ebik,
     & REAL(R_obik,4),R_idik,REAL(R_udik,4),
     & R_ubik,REAL(R_edik,4),I_apik,I_irik,I_upik,I_umik,L_afik
     &,
     & L_urik,L_usik,L_odik,L_ofik,L_ifik,L_esik,
     & L_adik,L_ibik,L_ekik,L_itik,L_ukik,L_alik,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_ixek,L_oxek,L_asik
     &,I_orik,
     & L_atik,R_omik,REAL(R_akik,4),L_etik,L_uxek,L_otik,L_abik
     &,L_ilik,
     & L_olik,L_ulik,L_emik,L_imik,L_amik)
      !}

      if(L_imik.or.L_emik.or.L_amik.or.L_ulik.or.L_olik.or.L_ilik
     &) then      
                  I_epik = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_epik = z'40000000'
      endif
C FDA10_vlv.fgi( 384, 177):���� ���������� �������� � �����������������,20FDA14AN001KA04
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_ubuk,4),R8_uvok
     &,I_ekuk,I_ukuk,I_akuk,
     & C8_ixok,I_okuk,R_ebuk,R_abuk,R_usok,
     & REAL(R_etok,4),R_avok,REAL(R_ivok,4),
     & R_itok,REAL(R_utok,4),I_ofuk,I_aluk,I_ikuk,I_ifuk,L_ovok
     &,
     & L_iluk,L_imuk,L_evok,L_exok,L_axok,L_uluk,
     & L_otok,L_atok,L_uxok,L_apuk,L_ibuk,L_obuk,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_asok,L_esok,L_oluk
     &,I_eluk,
     & L_omuk,R_efuk,REAL(R_oxok,4),L_umuk,L_isok,L_epuk,L_osok
     &,L_aduk,
     & L_eduk,L_iduk,L_uduk,L_afuk,L_oduk)
      !}

      if(L_afuk.or.L_uduk.or.L_oduk.or.L_iduk.or.L_eduk.or.L_aduk
     &) then      
                  I_ufuk = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ufuk = z'40000000'
      endif
C FDA10_vlv.fgi( 349, 177):���� ���������� �������� � �����������������,20FDA14AN001KA02
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_exuk,4),R8_etuk
     &,I_odal,I_efal,I_idal,
     & C8_utuk,I_afal,R_ovuk,R_ivuk,R_eruk,
     & REAL(R_oruk,4),R_isuk,REAL(R_usuk,4),
     & R_uruk,REAL(R_esuk,4),I_adal,I_ifal,I_udal,I_ubal,L_atuk
     &,
     & L_ufal,L_ukal,L_osuk,L_otuk,L_ituk,L_ekal,
     & L_asuk,L_iruk,L_evuk,L_ilal,L_uvuk,L_axuk,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_ipuk,L_opuk,L_akal
     &,I_ofal,
     & L_alal,R_obal,REAL(R_avuk,4),L_elal,L_upuk,L_olal,L_aruk
     &,L_ixuk,
     & L_oxuk,L_uxuk,L_ebal,L_ibal,L_abal)
      !}

      if(L_ibal.or.L_ebal.or.L_abal.or.L_uxuk.or.L_oxuk.or.L_ixuk
     &) then      
                  I_edal = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_edal = z'40000000'
      endif
C FDA10_vlv.fgi( 331, 177):���� ���������� �������� � �����������������,20FDA14AN001KA01
      !{
      Call DAT_ANA_HANDLER(deltat,R_odil,R_ilil,REAL(1,4)
     &,
     & REAL(R_efil,4),REAL(R_ifil,4),
     & REAL(R_idil,4),REAL(R_edil,4),I_elil,
     & REAL(R_akil,4),L_ekil,REAL(R_ikil,4),L_okil,L_ukil
     &,R_ofil,
     & REAL(R_afil,4),REAL(R_udil,4),L_alil,REAL(R_ufil,4
     &))
      !}
C FDA10_vlv.fgi( 114, 138):���������� �������,20FDA13CW001XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_amil,R_uril,REAL(1,4)
     &,
     & REAL(R_omil,4),REAL(R_umil,4),
     & REAL(R_ulil,4),REAL(R_olil,4),I_oril,
     & REAL(R_ipil,4),L_opil,REAL(R_upil,4),L_aril,L_eril
     &,R_apil,
     & REAL(R_imil,4),REAL(R_emil,4),L_iril,REAL(R_epil,4
     &))
      !}
C FDA10_vlv.fgi(  87, 124):���������� �������,20FDA15CM002XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_isil,R_exil,REAL(1,4)
     &,
     & REAL(R_atil,4),REAL(R_etil,4),
     & REAL(R_esil,4),REAL(R_asil,4),I_axil,
     & REAL(R_util,4),L_avil,REAL(R_evil,4),L_ivil,L_ovil
     &,R_itil,
     & REAL(R_usil,4),REAL(R_osil,4),L_uvil,REAL(R_otil,4
     &))
      !}
C FDA10_vlv.fgi(  87, 138):���������� �������,20FDA15CU002XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_uxil,R_ofol,REAL(1,4)
     &,
     & REAL(R_ibol,4),REAL(R_obol,4),
     & REAL(R_oxil,4),REAL(R_ixil,4),I_ifol,
     & REAL(R_edol,4),L_idol,REAL(R_odol,4),L_udol,L_afol
     &,R_ubol,
     & REAL(R_ebol,4),REAL(R_abol,4),L_efol,REAL(R_adol,4
     &))
      !}
C FDA10_vlv.fgi(  60, 124):���������� �������,20FDA15CM001XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_ekol,R_apol,REAL(1,4)
     &,
     & REAL(R_ukol,4),REAL(R_alol,4),
     & REAL(R_akol,4),REAL(R_ufol,4),I_umol,
     & REAL(R_olol,4),L_ulol,REAL(R_amol,4),L_emol,L_imol
     &,R_elol,
     & REAL(R_okol,4),REAL(R_ikol,4),L_omol,REAL(R_ilol,4
     &))
      !}
C FDA10_vlv.fgi(  60, 138):���������� �������,20FDA15CU001XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_opol,R_itol,REAL(1,4)
     &,
     & REAL(R_erol,4),REAL(R_irol,4),
     & REAL(R_ipol,4),REAL(R_epol,4),I_etol,
     & REAL(R_asol,4),L_esol,REAL(R_isol,4),L_osol,L_usol
     &,R_orol,
     & REAL(R_arol,4),REAL(R_upol,4),L_atol,REAL(R_urol,4
     &))
      !}
C FDA10_vlv.fgi(  33, 124):���������� �������,20FDA13CM001XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_avol,R_ubul,REAL(1,4)
     &,
     & REAL(R_ovol,4),REAL(R_uvol,4),
     & REAL(R_utol,4),REAL(R_otol,4),I_obul,
     & REAL(R_ixol,4),L_oxol,REAL(R_uxol,4),L_abul,L_ebul
     &,R_axol,
     & REAL(R_ivol,4),REAL(R_evol,4),L_ibul,REAL(R_exol,4
     &))
      !}
C FDA10_vlv.fgi(  33, 138):���������� �������,20FDA13CU001XQ01
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_umul,4),R8_ukul
     &,I_esul,I_usul,I_asul,
     & C8_ilul,I_osul,R_emul,R_amul,R_udul,
     & REAL(R_eful,4),R_akul,REAL(R_ikul,4),
     & R_iful,REAL(R_uful,4),I_orul,I_atul,I_isul,I_irul,L_okul
     &,
     & L_itul,L_ivul,L_ekul,L_elul,L_alul,L_utul,
     & L_oful,L_aful,L_ulul,L_axul,L_imul,L_omul,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_adul,L_edul,L_otul
     &,I_etul,
     & L_ovul,R_erul,REAL(R_olul,4),L_uvul,L_idul,L_exul,L_odul
     &,L_apul,
     & L_epul,L_ipul,L_upul,L_arul,L_opul)
      !}

      if(L_arul.or.L_upul.or.L_opul.or.L_ipul.or.L_epul.or.L_apul
     &) then      
                  I_urul = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_urul = z'40000000'
      endif
C FDA10_vlv.fgi( 300, 153):���� ���������� �������� � �����������������,20FDA11AA202
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_ufem,4),R8_ubem
     &,I_emem,I_umem,I_amem,
     & C8_idem,I_omem,R_efem,R_afem,R_uvam,
     & REAL(R_exam,4),R_abem,REAL(R_ibem,4),
     & R_ixam,REAL(R_uxam,4),I_olem,I_apem,I_imem,I_ilem,L_obem
     &,
     & L_ipem,L_irem,L_ebem,L_edem,L_adem,L_upem,
     & L_oxam,L_axam,L_udem,L_asem,L_ifem,L_ofem,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_avam,L_evam,L_opem
     &,I_epem,
     & L_orem,R_elem,REAL(R_odem,4),L_urem,L_ivam,L_esem,L_ovam
     &,L_akem,
     & L_ekem,L_ikem,L_ukem,L_alem,L_okem)
      !}

      if(L_alem.or.L_ukem.or.L_okem.or.L_ikem.or.L_ekem.or.L_akem
     &) then      
                  I_ulem = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ulem = z'40000000'
      endif
C FDA10_vlv.fgi( 234, 179):���� ���������� �������� � �����������������,20FDA11AX001KA03
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_edim,4),R8_exem
     &,I_okim,I_elim,I_ikim,
     & C8_uxem,I_alim,R_obim,R_ibim,R_etem,
     & REAL(R_otem,4),R_ivem,REAL(R_uvem,4),
     & R_utem,REAL(R_evem,4),I_akim,I_ilim,I_ukim,I_ufim,L_axem
     &,
     & L_ulim,L_umim,L_ovem,L_oxem,L_ixem,L_emim,
     & L_avem,L_item,L_ebim,L_ipim,L_ubim,L_adim,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_isem,L_osem,L_amim
     &,I_olim,
     & L_apim,R_ofim,REAL(R_abim,4),L_epim,L_usem,L_opim,L_atem
     &,L_idim,
     & L_odim,L_udim,L_efim,L_ifim,L_afim)
      !}

      if(L_ifim.or.L_efim.or.L_afim.or.L_udim.or.L_odim.or.L_idim
     &) then      
                  I_ekim = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ekim = z'40000000'
      endif
C FDA10_vlv.fgi( 217, 179):���� ���������� �������� � �����������������,20FDA11AX001KA02
      R_(58) = R8_exem * R8_ubem
C FDA10_vent_log.fgi(  29, 470):����������
      if(R_(58).le.R0_ate) then
         R_(56)=R0_use
      elseif(R_(58).gt.R0_ose) then
         R_(56)=R0_ise
      else
         R_(56)=R0_use+(R_(58)-(R0_ate))*(R0_ise-(R0_use)
     &)/(R0_ose-(R0_ate))
      endif
C FDA10_vent_log.fgi(  36, 471):��������������� ���������
      if(L_(13)) then
         R_(52)=R_(56)
      else
         R_(52)=R_(54)
      endif
C FDA10_vent_log.fgi(  50, 470):���� RE IN LO CH7
      R8_upe=(R0_ope*R_(44)+deltat*R_(52))/(R0_ope+deltat
     &)
C FDA10_vent_log.fgi(  62, 471):�������������� �����  
      R8_adi=R8_upe
C FDA10_vent_log.fgi(  76, 471):������,20FDA11CF501_G
      R_avev = R8_adi
C FDA10_init.fgi(  46, 186):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_usev,R_oxev,REAL(1,4)
     &,
     & REAL(R_itev,4),REAL(R_otev,4),
     & REAL(R_osev,4),REAL(R_isev,4),I_ixev,
     & REAL(R_evev,4),L_ivev,REAL(R_ovev,4),L_uvev,L_axev
     &,R_utev,
     & REAL(R_etev,4),REAL(R_atev,4),L_exev,REAL(R_avev,4
     &))
      !}
C FDA10_vlv.fgi(  33, 102):���������� �������,20FDA11CF501XQ01
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_oxim,4),R8_otim
     &,I_afom,I_ofom,I_udom,
     & C8_evim,I_ifom,R_axim,R_uvim,R_orim,
     & REAL(R_asim,4),R_usim,REAL(R_etim,4),
     & R_esim,REAL(R_osim,4),I_idom,I_ufom,I_efom,I_edom,L_itim
     &,
     & L_ekom,L_elom,L_atim,L_avim,L_utim,L_okom,
     & L_isim,L_urim,L_ovim,L_ulom,L_exim,L_ixim,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_upim,L_arim,L_ikom
     &,I_akom,
     & L_ilom,R_adom,REAL(R_ivim,4),L_olom,L_erim,L_amom,L_irim
     &,L_uxim,
     & L_abom,L_ebom,L_obom,L_ubom,L_ibom)
      !}

      if(L_ubom.or.L_obom.or.L_ibom.or.L_ebom.or.L_abom.or.L_uxim
     &) then      
                  I_odom = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_odom = z'40000000'
      endif
C FDA10_vlv.fgi( 199, 179):���� ���������� �������� � �����������������,20FDA11AX001KA01
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_avom,4),R8_asom
     &,I_ibum,I_adum,I_ebum,
     & C8_osom,I_ubum,R_itom,R_etom,R_apom,
     & REAL(R_ipom,4),R_erom,REAL(R_orom,4),
     & R_opom,REAL(R_arom,4),I_uxom,I_edum,I_obum,I_oxom,L_urom
     &,
     & L_odum,L_ofum,L_irom,L_isom,L_esom,L_afum,
     & L_upom,L_epom,L_atom,L_ekum,L_otom,L_utom,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_emom,L_imom,L_udum
     &,I_idum,
     & L_ufum,R_ixom,REAL(R_usom,4),L_akum,L_omom,L_ikum,L_umom
     &,L_evom,
     & L_ivom,L_ovom,L_axom,L_exom,L_uvom)
      !}

      if(L_exom.or.L_axom.or.L_uvom.or.L_ovom.or.L_ivom.or.L_evom
     &) then      
                  I_abum = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_abum = z'40000000'
      endif
C FDA10_vlv.fgi( 284, 153):���� ���������� �������� � �����������������,20FDA11AA501
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_isum,4),R8_ipum
     &,I_uvum,I_ixum,I_ovum,
     & C8_arum,I_exum,R_urum,R_orum,R_ilum,
     & REAL(R_ulum,4),R_omum,REAL(R_apum,4),
     & R_amum,REAL(R_imum,4),I_evum,I_oxum,I_axum,I_avum,L_epum
     &,
     & L_abap,L_adap,L_umum,L_upum,L_opum,L_ibap,
     & L_emum,L_olum,L_irum,L_odap,L_asum,L_esum,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_okum,L_ukum,L_ebap
     &,I_uxum,
     & L_edap,R_utum,REAL(R_erum,4),L_idap,L_alum,L_udap,L_elum
     &,L_osum,
     & L_usum,L_atum,L_itum,L_otum,L_etum)
      !}

      if(L_otum.or.L_itum.or.L_etum.or.L_atum.or.L_usum.or.L_osum
     &) then      
                  I_ivum = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ivum = z'40000000'
      endif
C FDA10_vlv.fgi( 268, 153):���� ���������� �������� � �����������������,20FDA11AA003
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_upap,4),R8_ulap
     &,I_etap,I_utap,I_atap,
     & C8_imap,I_otap,R_epap,R_apap,R_ufap,
     & REAL(R_ekap,4),R_alap,REAL(R_ilap,4),
     & R_ikap,REAL(R_ukap,4),I_osap,I_avap,I_itap,I_isap,L_olap
     &,
     & L_ivap,L_ixap,L_elap,L_emap,L_amap,L_uvap,
     & L_okap,L_akap,L_umap,L_abep,L_ipap,L_opap,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_afap,L_efap,L_ovap
     &,I_evap,
     & L_oxap,R_esap,REAL(R_omap,4),L_uxap,L_ifap,L_ebep,L_ofap
     &,L_arap,
     & L_erap,L_irap,L_urap,L_asap,L_orap)
      !}

      if(L_asap.or.L_urap.or.L_orap.or.L_irap.or.L_erap.or.L_arap
     &) then      
                  I_usap = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_usap = z'40000000'
      endif
C FDA10_vlv.fgi( 251, 153):���� ���������� �������� � �����������������,20FDA11AA004
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_emep,4),R8_ekep
     &,I_orep,I_esep,I_irep,
     & C8_ukep,I_asep,R_olep,R_ilep,R_edep,
     & REAL(R_odep,4),R_ifep,REAL(R_ufep,4),
     & R_udep,REAL(R_efep,4),I_arep,I_isep,I_urep,I_upep,L_akep
     &,
     & L_usep,L_utep,L_ofep,L_okep,L_ikep,L_etep,
     & L_afep,L_idep,L_elep,L_ivep,L_ulep,L_amep,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_ibep,L_obep,L_atep
     &,I_osep,
     & L_avep,R_opep,REAL(R_alep,4),L_evep,L_ubep,L_ovep,L_adep
     &,L_imep,
     & L_omep,L_umep,L_epep,L_ipep,L_apep)
      !}

      if(L_ipep.or.L_epep.or.L_apep.or.L_umep.or.L_omep.or.L_imep
     &) then      
                  I_erep = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_erep = z'40000000'
      endif
C FDA10_vlv.fgi( 234, 153):���� ���������� �������� � �����������������,20FDA11AA001
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_okip,4),R8_odip
     &,I_apip,I_opip,I_umip,
     & C8_efip,I_ipip,R_akip,R_ufip,R_oxep,
     & REAL(R_abip,4),R_ubip,REAL(R_edip,4),
     & R_ebip,REAL(R_obip,4),I_imip,I_upip,I_epip,I_emip,L_idip
     &,
     & L_erip,L_esip,L_adip,L_afip,L_udip,L_orip,
     & L_ibip,L_uxep,L_ofip,L_usip,L_ekip,L_ikip,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_uvep,L_axep,L_irip
     &,I_arip,
     & L_isip,R_amip,REAL(R_ifip,4),L_osip,L_exep,L_atip,L_ixep
     &,L_ukip,
     & L_alip,L_elip,L_olip,L_ulip,L_ilip)
      !}

      if(L_ulip.or.L_olip.or.L_ilip.or.L_elip.or.L_alip.or.L_ukip
     &) then      
                  I_omip = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_omip = z'40000000'
      endif
C FDA10_vlv.fgi( 217, 153):���� ���������� �������� � �����������������,20FDA11AA005
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_afop,4),R8_abop
     &,I_ilop,I_amop,I_elop,
     & C8_obop,I_ulop,R_idop,R_edop,R_avip,
     & REAL(R_ivip,4),R_exip,REAL(R_oxip,4),
     & R_ovip,REAL(R_axip,4),I_ukop,I_emop,I_olop,I_okop,L_uxip
     &,
     & L_omop,L_opop,L_ixip,L_ibop,L_ebop,L_apop,
     & L_uvip,L_evip,L_adop,L_erop,L_odop,L_udop,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_etip,L_itip,L_umop
     &,I_imop,
     & L_upop,R_ikop,REAL(R_ubop,4),L_arop,L_otip,L_irop,L_utip
     &,L_efop,
     & L_ifop,L_ofop,L_akop,L_ekop,L_ufop)
      !}

      if(L_ekop.or.L_akop.or.L_ufop.or.L_ofop.or.L_ifop.or.L_efop
     &) then      
                  I_alop = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_alop = z'40000000'
      endif
C FDA10_vlv.fgi( 199, 153):���� ���������� �������� � �����������������,20FDA11AA006
      !{
      Call DAT_ANA_HANDLER(deltat,R_asop,R_uvop,REAL(1,4)
     &,
     & REAL(R_osop,4),REAL(R_usop,4),
     & REAL(R_urop,4),REAL(R_orop,4),I_ovop,
     & REAL(R_itop,4),L_otop,REAL(R_utop,4),L_avop,L_evop
     &,R_atop,
     & REAL(R_isop,4),REAL(R_esop,4),L_ivop,REAL(R_etop,4
     &))
      !}
C FDA10_vlv.fgi(  87, 152):���������� �������,20FDA15AF001XQ03
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_arup,4),R8_amup
     &,I_itup,I_avup,I_etup,
     & C8_omup,I_utup,R_ipup,R_epup,R_akup,
     & REAL(R_ikup,4),R_elup,REAL(R_olup,4),
     & R_okup,REAL(R_alup,4),I_usup,I_evup,I_otup,I_osup,L_ulup
     &,
     & L_ovup,L_oxup,L_ilup,L_imup,L_emup,L_axup,
     & L_ukup,L_ekup,L_apup,L_ebar,L_opup,L_upup,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_efup,L_ifup,L_uvup
     &,I_ivup,
     & L_uxup,R_isup,REAL(R_umup,4),L_abar,L_ofup,L_ibar,L_ufup
     &,L_erup,
     & L_irup,L_orup,L_asup,L_esup,L_urup)
      !}

      if(L_esup.or.L_asup.or.L_urup.or.L_orup.or.L_irup.or.L_erup
     &) then      
                  I_atup = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_atup = z'40000000'
      endif
C FDA10_vlv.fgi( 235,  19):���� ���������� �������� � �����������������,20FDA14AN001KA21
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_imar,4),R8_ikar
     &,I_urar,I_isar,I_orar,
     & C8_alar,I_esar,R_ular,R_olar,R_idar,
     & REAL(R_udar,4),R_ofar,REAL(R_akar,4),
     & R_afar,REAL(R_ifar,4),I_erar,I_osar,I_asar,I_arar,L_ekar
     &,
     & L_atar,L_avar,L_ufar,L_ukar,L_okar,L_itar,
     & L_efar,L_odar,L_ilar,L_ovar,L_amar,L_emar,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_obar,L_ubar,L_etar
     &,I_usar,
     & L_evar,R_upar,REAL(R_elar,4),L_ivar,L_adar,L_uvar,L_edar
     &,L_omar,
     & L_umar,L_apar,L_ipar,L_opar,L_epar)
      !}

      if(L_opar.or.L_ipar.or.L_epar.or.L_apar.or.L_umar.or.L_omar
     &) then      
                  I_irar = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_irar = z'40000000'
      endif
C FDA10_vlv.fgi( 218,  19):���� ���������� �������� � �����������������,20FDA14AN001KA20
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_uker,4),R8_uder
     &,I_eper,I_uper,I_aper,
     & C8_ifer,I_oper,R_eker,R_aker,R_uxar,
     & REAL(R_eber,4),R_ader,REAL(R_ider,4),
     & R_iber,REAL(R_uber,4),I_omer,I_arer,I_iper,I_imer,L_oder
     &,
     & L_irer,L_iser,L_eder,L_efer,L_afer,L_urer,
     & L_ober,L_aber,L_ufer,L_ater,L_iker,L_oker,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_axar,L_exar,L_orer
     &,I_erer,
     & L_oser,R_emer,REAL(R_ofer,4),L_user,L_ixar,L_eter,L_oxar
     &,L_aler,
     & L_eler,L_iler,L_uler,L_amer,L_oler)
      !}

      if(L_amer.or.L_uler.or.L_oler.or.L_iler.or.L_eler.or.L_aler
     &) then      
                  I_umer = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_umer = z'40000000'
      endif
C FDA10_vlv.fgi( 200,  19):���� ���������� �������� � �����������������,20FDA14AN001KA19
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_aras,4),R8_amas
     &,I_itas,I_avas,I_etas,
     & C8_omas,I_utas,R_ipas,R_epas,R_akas,
     & REAL(R_ikas,4),R_elas,REAL(R_olas,4),
     & R_okas,REAL(R_alas,4),I_usas,I_evas,I_otas,I_osas,L_ulas
     &,
     & L_ovas,L_oxas,L_ilas,L_imas,L_emas,L_axas,
     & L_ukas,L_ekas,L_apas,L_ebes,L_opas,L_upas,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_efas,L_ifas,L_uvas
     &,I_ivas,
     & L_uxas,R_isas,REAL(R_umas,4),L_abes,L_ofas,L_ibes,L_ufas
     &,L_eras,
     & L_iras,L_oras,L_asas,L_esas,L_uras)
      !}

      if(L_esas.or.L_asas.or.L_uras.or.L_oras.or.L_iras.or.L_eras
     &) then      
                  I_atas = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_atas = z'40000000'
      endif
C FDA10_vlv.fgi( 218,  44):���� ���������� �������� � �����������������,20FDA14AN001KA14
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_imes,4),R8_ikes
     &,I_ures,I_ises,I_ores,
     & C8_ales,I_eses,R_ules,R_oles,R_ides,
     & REAL(R_udes,4),R_ofes,REAL(R_akes,4),
     & R_afes,REAL(R_ifes,4),I_eres,I_oses,I_ases,I_ares,L_ekes
     &,
     & L_ates,L_aves,L_ufes,L_ukes,L_okes,L_ites,
     & L_efes,L_odes,L_iles,L_oves,L_ames,L_emes,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_obes,L_ubes,L_etes
     &,I_uses,
     & L_eves,R_upes,REAL(R_eles,4),L_ives,L_ades,L_uves,L_edes
     &,L_omes,
     & L_umes,L_apes,L_ipes,L_opes,L_epes)
      !}

      if(L_opes.or.L_ipes.or.L_epes.or.L_apes.or.L_umes.or.L_omes
     &) then      
                  I_ires = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ires = z'40000000'
      endif
C FDA10_vlv.fgi( 200,  44):���� ���������� �������� � �����������������,20FDA14AN001KA13
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_ukis,4),R8_udis
     &,I_epis,I_upis,I_apis,
     & C8_ifis,I_opis,R_ekis,R_akis,R_uxes,
     & REAL(R_ebis,4),R_adis,REAL(R_idis,4),
     & R_ibis,REAL(R_ubis,4),I_omis,I_aris,I_ipis,I_imis,L_odis
     &,
     & L_iris,L_isis,L_edis,L_efis,L_afis,L_uris,
     & L_obis,L_abis,L_ufis,L_atis,L_ikis,L_okis,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_axes,L_exes,L_oris
     &,I_eris,
     & L_osis,R_emis,REAL(R_ofis,4),L_usis,L_ixes,L_etis,L_oxes
     &,L_alis,
     & L_elis,L_ilis,L_ulis,L_amis,L_olis)
      !}

      if(L_amis.or.L_ulis.or.L_olis.or.L_ilis.or.L_elis.or.L_alis
     &) then      
                  I_umis = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_umis = z'40000000'
      endif
C FDA10_vlv.fgi( 291,  70):���� ���������� �������� � �����������������,20FDA14AN001KA12
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_efos,4),R8_ebos
     &,I_olos,I_emos,I_ilos,
     & C8_ubos,I_amos,R_odos,R_idos,R_evis,
     & REAL(R_ovis,4),R_ixis,REAL(R_uxis,4),
     & R_uvis,REAL(R_exis,4),I_alos,I_imos,I_ulos,I_ukos,L_abos
     &,
     & L_umos,L_upos,L_oxis,L_obos,L_ibos,L_epos,
     & L_axis,L_ivis,L_edos,L_iros,L_udos,L_afos,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_itis,L_otis,L_apos
     &,I_omos,
     & L_aros,R_okos,REAL(R_ados,4),L_eros,L_utis,L_oros,L_avis
     &,L_ifos,
     & L_ofos,L_ufos,L_ekos,L_ikos,L_akos)
      !}

      if(L_ikos.or.L_ekos.or.L_akos.or.L_ufos.or.L_ofos.or.L_ifos
     &) then      
                  I_elos = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_elos = z'40000000'
      endif
C FDA10_vlv.fgi( 273,  70):���� ���������� �������� � �����������������,20FDA14AN001KA11
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_obus,4),R8_ovos
     &,I_akus,I_okus,I_ufus,
     & C8_exos,I_ikus,R_abus,R_uxos,R_osos,
     & REAL(R_atos,4),R_utos,REAL(R_evos,4),
     & R_etos,REAL(R_otos,4),I_ifus,I_ukus,I_ekus,I_efus,L_ivos
     &,
     & L_elus,L_emus,L_avos,L_axos,L_uvos,L_olus,
     & L_itos,L_usos,L_oxos,L_umus,L_ebus,L_ibus,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_uros,L_asos,L_ilus
     &,I_alus,
     & L_imus,R_afus,REAL(R_ixos,4),L_omus,L_esos,L_apus,L_isos
     &,L_ubus,
     & L_adus,L_edus,L_odus,L_udus,L_idus)
      !}

      if(L_udus.or.L_odus.or.L_idus.or.L_edus.or.L_adus.or.L_ubus
     &) then      
                  I_ofus = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ofus = z'40000000'
      endif
C FDA10_vlv.fgi( 254,  70):���� ���������� �������� � �����������������,20FDA14AN001KA10
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_axus,4),R8_atus
     &,I_idat,I_afat,I_edat,
     & C8_otus,I_udat,R_ivus,R_evus,R_arus,
     & REAL(R_irus,4),R_esus,REAL(R_osus,4),
     & R_orus,REAL(R_asus,4),I_ubat,I_efat,I_odat,I_obat,L_usus
     &,
     & L_ofat,L_okat,L_isus,L_itus,L_etus,L_akat,
     & L_urus,L_erus,L_avus,L_elat,L_ovus,L_uvus,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_epus,L_ipus,L_ufat
     &,I_ifat,
     & L_ukat,R_ibat,REAL(R_utus,4),L_alat,L_opus,L_ilat,L_upus
     &,L_exus,
     & L_ixus,L_oxus,L_abat,L_ebat,L_uxus)
      !}

      if(L_ebat.or.L_abat.or.L_uxus.or.L_oxus.or.L_ixus.or.L_exus
     &) then      
                  I_adat = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_adat = z'40000000'
      endif
C FDA10_vlv.fgi( 235,  70):���� ���������� �������� � �����������������,20FDA14AN001KA09
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_oret,4),R8_omet
     &,I_avet,I_ovet,I_utet,
     & C8_epet,I_ivet,R_aret,R_upet,R_oket,
     & REAL(R_alet,4),R_ulet,REAL(R_emet,4),
     & R_elet,REAL(R_olet,4),I_itet,I_uvet,I_evet,I_etet,L_imet
     &,
     & L_exet,L_ebit,L_amet,L_apet,L_umet,L_oxet,
     & L_ilet,L_uket,L_opet,L_ubit,L_eret,L_iret,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_ufet,L_aket,L_ixet
     &,I_axet,
     & L_ibit,R_atet,REAL(R_ipet,4),L_obit,L_eket,L_adit,L_iket
     &,L_uret,
     & L_aset,L_eset,L_oset,L_uset,L_iset)
      !}

      if(L_uset.or.L_oset.or.L_iset.or.L_eset.or.L_aset.or.L_uret
     &) then      
                  I_otet = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_otet = z'40000000'
      endif
C FDA10_vlv.fgi( 200,  70):���� ���������� �������� � �����������������,20FDA14AN001KA07
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_apit,4),R8_alit
     &,I_isit,I_atit,I_esit,
     & C8_olit,I_usit,R_imit,R_emit,R_afit,
     & REAL(R_ifit,4),R_ekit,REAL(R_okit,4),
     & R_ofit,REAL(R_akit,4),I_urit,I_etit,I_osit,I_orit,L_ukit
     &,
     & L_otit,L_ovit,L_ikit,L_ilit,L_elit,L_avit,
     & L_ufit,L_efit,L_amit,L_exit,L_omit,L_umit,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_edit,L_idit,L_utit
     &,I_itit,
     & L_uvit,R_irit,REAL(R_ulit,4),L_axit,L_odit,L_ixit,L_udit
     &,L_epit,
     & L_ipit,L_opit,L_arit,L_erit,L_upit)
      !}

      if(L_erit.or.L_arit.or.L_upit.or.L_opit.or.L_ipit.or.L_epit
     &) then      
                  I_asit = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_asit = z'40000000'
      endif
C FDA10_vlv.fgi( 251, 122):���� ���������� �������� � �����������������,20FDA14AX001KA04
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_ilot,4),R8_ifot
     &,I_upot,I_irot,I_opot,
     & C8_akot,I_erot,R_ukot,R_okot,R_ibot,
     & REAL(R_ubot,4),R_odot,REAL(R_afot,4),
     & R_adot,REAL(R_idot,4),I_epot,I_orot,I_arot,I_apot,L_efot
     &,
     & L_asot,L_atot,L_udot,L_ufot,L_ofot,L_isot,
     & L_edot,L_obot,L_ikot,L_otot,L_alot,L_elot,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_oxit,L_uxit,L_esot
     &,I_urot,
     & L_etot,R_umot,REAL(R_ekot,4),L_itot,L_abot,L_utot,L_ebot
     &,L_olot,
     & L_ulot,L_amot,L_imot,L_omot,L_emot)
      !}

      if(L_omot.or.L_imot.or.L_emot.or.L_amot.or.L_ulot.or.L_olot
     &) then      
                  I_ipot = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ipot = z'40000000'
      endif
C FDA10_vlv.fgi( 234, 122):���� ���������� �������� � �����������������,20FDA14AX001KA03
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_ufut,4),R8_ubut
     &,I_emut,I_umut,I_amut,
     & C8_idut,I_omut,R_efut,R_afut,R_uvot,
     & REAL(R_exot,4),R_abut,REAL(R_ibut,4),
     & R_ixot,REAL(R_uxot,4),I_olut,I_aput,I_imut,I_ilut,L_obut
     &,
     & L_iput,L_irut,L_ebut,L_edut,L_adut,L_uput,
     & L_oxot,L_axot,L_udut,L_asut,L_ifut,L_ofut,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_avot,L_evot,L_oput
     &,I_eput,
     & L_orut,R_elut,REAL(R_odut,4),L_urut,L_ivot,L_esut,L_ovot
     &,L_akut,
     & L_ekut,L_ikut,L_ukut,L_alut,L_okut)
      !}

      if(L_alut.or.L_ukut.or.L_okut.or.L_ikut.or.L_ekut.or.L_akut
     &) then      
                  I_ulut = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ulut = z'40000000'
      endif
C FDA10_vlv.fgi( 217, 122):���� ���������� �������� � �����������������,20FDA14AX001KA02
      R_(25) = R8_ubut * R8_ifot
C FDA10_vent_log.fgi( 307, 470):����������
      if(R_(25).le.R0_it) then
         R_(23)=R0_et
      elseif(R_(25).gt.R0_at) then
         R_(23)=R0_us
      else
         R_(23)=R0_et+(R_(25)-(R0_it))*(R0_us-(R0_et))/(R0_at
     &-(R0_it))
      endif
C FDA10_vent_log.fgi( 314, 471):��������������� ���������
      if(L_(3)) then
         R_(21)=R_(23)
      else
         R_(21)=R_(22)
      endif
C FDA10_vent_log.fgi( 328, 470):���� RE IN LO CH7
      R8_is=(R0_es*R_(20)+deltat*R_(21))/(R0_es+deltat)
C FDA10_vent_log.fgi( 340, 471):�������������� �����  
      R8_ixe=R8_is
C FDA10_vent_log.fgi( 354, 471):������,20FDA14CF501_G
      R_opef = R8_ixe
C FDA10_init.fgi( 217, 186):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_imef,R_esef,REAL(1,4)
     &,
     & REAL(R_apef,4),REAL(R_epef,4),
     & REAL(R_emef,4),REAL(R_amef,4),I_asef,
     & REAL(R_upef,4),L_aref,REAL(R_eref,4),L_iref,L_oref
     &,R_ipef,
     & REAL(R_umef,4),REAL(R_omef,4),L_uref,REAL(R_opef,4
     &))
      !}
C FDA10_vlv.fgi(  33,  72):���������� �������,20FDA14CF501XQ01
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_odav,4),R8_ixut
     &,I_alav,I_olav,I_ukav,
     & C8_abav,I_ilav,R_adav,R_ubav,R_itut,
     & REAL(R_utut,4),R_ovut,REAL(R_axut,4),
     & R_avut,REAL(R_ivut,4),I_ikav,I_ulav,I_elav,I_ekav,L_exut
     &,
     & L_emav,L_epav,L_uvut,L_uxut,L_oxut,L_omav,
     & L_evut,L_otut,L_ibav,L_upav,L_edav,L_idav,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_isut,L_osut,L_imav
     &,I_amav,
     & L_ipav,R_akav,REAL(R_ebav,4),L_opav,L_usut,L_arav,L_atut
     &,L_udav,
     & L_afav,L_efav,L_ofav,L_ufav,L_ifav)
      !}

      if(L_ufav.or.L_ofav.or.L_ifav.or.L_efav.or.L_afav.or.L_udav
     &) then      
                  I_okav = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_okav = z'40000000'
      endif
C FDA10_vlv.fgi( 199, 122):���� ���������� �������� � �����������������,20FDA14AX001KA01
      !{
      Call DAT_ANA_HANDLER(deltat,R_axav,R_udev,REAL(1,4)
     &,
     & REAL(R_oxav,4),REAL(R_uxav,4),
     & REAL(R_uvav,4),REAL(R_ovav,4),I_odev,
     & REAL(R_ibev,4),L_obev,REAL(R_ubev,4),L_adev,L_edev
     &,R_abev,
     & REAL(R_ixav,4),REAL(R_exav,4),L_idev,REAL(R_ebev,4
     &))
      !}
C FDA10_vlv.fgi( 142, 180):���������� �������,20FDA14CT002XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_ebiv,R_akiv,REAL(1,4)
     &,
     & REAL(R_ubiv,4),REAL(R_adiv,4),
     & REAL(R_abiv,4),REAL(R_uxev,4),I_ufiv,
     & REAL(R_odiv,4),L_udiv,REAL(R_afiv,4),L_efiv,L_ifiv
     &,R_ediv,
     & REAL(R_obiv,4),REAL(R_ibiv,4),L_ofiv,REAL(R_idiv,4
     &))
      !}
C FDA10_vlv.fgi( 142, 138):���������� �������,20FDA12CW001XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_upiv,R_otiv,REAL(1,4)
     &,
     & REAL(R_iriv,4),REAL(R_oriv,4),
     & REAL(R_opiv,4),REAL(R_ipiv,4),I_itiv,
     & REAL(R_esiv,4),L_isiv,REAL(R_osiv,4),L_usiv,L_ativ
     &,R_uriv,
     & REAL(R_eriv,4),REAL(R_ariv,4),L_etiv,REAL(R_asiv,4
     &))
      !}
C FDA10_vlv.fgi(  33, 152):���������� �������,20FDA15CP007XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_eviv,R_ubov,REAL(1,4)
     &,
     & REAL(R_uviv,4),REAL(R_axiv,4),
     & REAL(R_aviv,4),REAL(R_utiv,4),I_obov,
     & REAL(R_ixiv,4),L_oxiv,REAL(R_uxiv,4),L_abov,L_ebov
     &,R_udox,
     & REAL(R_oviv,4),REAL(R_iviv,4),L_ibov,REAL(R_exiv,4
     &))
      !}
C FDA10_vlv.fgi(  60, 152):���������� �������,20FDA15CT003XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_idov,R_elov,REAL(1,4)
     &,
     & REAL(R_afov,4),REAL(R_efov,4),
     & REAL(R_edov,4),REAL(R_adov,4),I_alov,
     & REAL(R_ufov,4),L_akov,REAL(R_ekov,4),L_ikov,L_okov
     &,R_ifov,
     & REAL(R_udov,4),REAL(R_odov,4),L_ukov,REAL(R_ofov,4
     &))
      !}
C FDA10_vlv.fgi(  60, 166):���������� �������,20FDA15CT005XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_ulov,R_orov,REAL(1,4)
     &,
     & REAL(R_imov,4),REAL(R_omov,4),
     & REAL(R_olov,4),REAL(R_ilov,4),I_irov,
     & REAL(R_epov,4),L_ipov,REAL(R_opov,4),L_upov,L_arov
     &,R_umov,
     & REAL(R_emov,4),REAL(R_amov,4),L_erov,REAL(R_apov,4
     &))
      !}
C FDA10_vlv.fgi(  60, 180):���������� �������,20FDA15CF004XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_esov,R_axov,REAL(1,4)
     &,
     & REAL(R_usov,4),REAL(R_atov,4),
     & REAL(R_asov,4),REAL(R_urov,4),I_uvov,
     & REAL(R_otov,4),L_utov,REAL(R_avov,4),L_evov,L_ivov
     &,R_etov,
     & REAL(R_osov,4),REAL(R_isov,4),L_ovov,REAL(R_itov,4
     &))
      !}
C FDA10_vlv.fgi(  33, 166):���������� �������,20FDA15CT004XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_oxov,R_ifuv,REAL(1,4)
     &,
     & REAL(R_ebuv,4),REAL(R_ibuv,4),
     & REAL(R_ixov,4),REAL(R_exov,4),I_efuv,
     & REAL(R_aduv,4),L_eduv,REAL(R_iduv,4),L_oduv,L_uduv
     &,R_obuv,
     & REAL(R_abuv,4),REAL(R_uxov,4),L_afuv,REAL(R_ubuv,4
     &))
      !}
C FDA10_vlv.fgi(  33, 180):���������� �������,20FDA15CF003XQ01
      L_(24)=I_ofuv.ne.0
C FDA10_logic.fgi( 219, 304):��������� 1->LO
      L_(26)=I_ufuv.ne.0
C FDA10_logic.fgi( 219, 308):��������� 1->LO
      L_eluv=L_(26).or.(L_eluv.and..not.(L_(24)))
      L_(25)=.not.L_eluv
C FDA10_logic.fgi( 231, 306):RS �������
      !{
      Call DAT_DISCR_HANDLER(deltat,I_ekuv,L_ukuv,R_okuv,
     & REAL(R_aluv,4),L_eluv,L_akuv,I_ikuv)
      !}
C FDA10_logic.fgi( 248, 306):���������� ������� ���������,20FDA13CW001XM01
      L_(28)=.true.
C FDA10_logic.fgi( 202, 572):��������� ���������� (�������)
      !{
      Call DAT_DISCR_HANDLER(deltat,I_upuv,L_iruv,R_eruv,
     & REAL(R_oruv,4),L_(28),L_opuv,I_aruv)
      !}
C FDA10_logic.fgi( 212, 570):���������� ������� ���������,20FDA15AB002QB01
      R_(60) = 9.0
C FDA10_logic.fgi( 236, 471):��������� (RE4) (�������)
      R_(61) = 0.0
C FDA10_logic.fgi( 236, 473):��������� (RE4) (�������)
      R_(62) = 9.0
C FDA10_logic.fgi( 214, 631):��������� (RE4) (�������)
      R_(63) = 0.0
C FDA10_logic.fgi( 214, 633):��������� (RE4) (�������)
      !�����. �������� I0_afax = FDA10_logicC?? /z'0100000A'
C /
C FDA10_logic.fgi(  71, 252):��������� �����
      !�����. �������� I0_efax = FDA10_logicC?? /z'01000003'
C /
C FDA10_logic.fgi(  71, 254):��������� �����
      L_(35)=I_ofax.ne.0
C FDA10_logic.fgi(  52, 249):��������� 1->LO
      L0_ifax=L_(35).or.(L0_ifax.and..not.(L_omax))
      L_(34)=.not.L0_ifax
C FDA10_logic.fgi(  63, 247):RS �������
      if(L0_ifax) then
         I_udax=I0_afax
      else
         I_udax=I0_efax
      endif
C FDA10_logic.fgi(  75, 252):���� RE IN LO CH7
      !�����. �������� I0_akax = FDA10_logicC?? /z'0100000A'
C /
C FDA10_logic.fgi(  71, 266):��������� �����
      !�����. �������� I0_ekax = FDA10_logicC?? /z'01000003'
C /
C FDA10_logic.fgi(  71, 268):��������� �����
      L_(37)=I_okax.ne.0
C FDA10_logic.fgi(  52, 263):��������� 1->LO
      L0_ikax=L_(37).or.(L0_ikax.and..not.(L_omax))
      L_(36)=.not.L0_ikax
C FDA10_logic.fgi(  63, 261):RS �������
      if(L0_ikax) then
         I_ufax=I0_akax
      else
         I_ufax=I0_ekax
      endif
C FDA10_logic.fgi(  75, 266):���� RE IN LO CH7
      !�����. �������� I0_alax = FDA10_logicC?? /z'0100000A'
C /
C FDA10_logic.fgi(  71, 281):��������� �����
      !�����. �������� I0_elax = FDA10_logicC?? /z'01000003'
C /
C FDA10_logic.fgi(  71, 283):��������� �����
      L_(39)=I_olax.ne.0
C FDA10_logic.fgi(  52, 278):��������� 1->LO
      L0_ilax=L_(39).or.(L0_ilax.and..not.(L_omax))
      L_(38)=.not.L0_ilax
C FDA10_logic.fgi(  63, 276):RS �������
      if(L0_ilax) then
         I_ukax=I0_alax
      else
         I_ukax=I0_elax
      endif
C FDA10_logic.fgi(  75, 281):���� RE IN LO CH7
      L_(41)=I_etix.ne.0
C FDA10_logic.fgi(  52, 307):��������� 1->LO
      if(L_(41).and..not.L0_emax) then
         R0_ulax=R_amax
      else
         R0_ulax=max(R_(64)-deltat,0.0)
      endif
      L_(40)=R0_ulax.gt.0.0
      L0_emax=L_(41)
C FDA10_logic.fgi(  62, 307):������������  �� T
      if(L_(40)) then
         R_(77)=1
      else
         R_(77)=0
      endif
C FDA10_logic.fgi(  70, 307):��������� LO->1
      R_(65) = 0.5
C FDA10_logic.fgi(  88, 310):��������� (RE4) (�������)
      R_(68) = 1.0
C FDA10_logic.fgi( 102, 316):��������� (RE4) (�������)
      L_(43)=.false.
C FDA10_logic.fgi( 105, 303):��������� ���������� (�������)
      R_(71) = 0.0
C FDA10_logic.fgi( 104, 316):��������� (RE4) (�������)
      R_(67) = 9.0
C FDA10_logic.fgi( 110, 316):��������� (RE4) (�������)
      R_(69) = 0.0
C FDA10_logic.fgi( 108, 316):��������� (RE4) (�������)
      R_(97) = 10.0
C FDA10_logic.fgi( 240, 408):��������� (RE4) (�������)
      L_(47)=I_irax.ne.0
C FDA10_logic.fgi(  58, 366):��������� 1->LO
      if(L_(47).and..not.L0_erax) then
         R0_upax=R_arax
      else
         R0_upax=max(R_(76)-deltat,0.0)
      endif
      L_(46)=R0_upax.gt.0.0
      L0_erax=L_(47)
C FDA10_logic.fgi(  68, 366):������������  �� T
      if(L_(46)) then
         R_(87)=1
      else
         R_(87)=0
      endif
C FDA10_logic.fgi(  76, 366):��������� LO->1
      R_(83) = 1.0
C FDA10_logic.fgi( 102, 350):��������� (RE4) (�������)
      R_(79) = 1.0
C FDA10_logic.fgi(  74, 342):��������� (RE4) (�������)
      R_(80) = R_(79) * R_(78) * R_(77)
C FDA10_logic.fgi(  79, 341):����������
      L_(48)=.false.
C FDA10_logic.fgi( 103, 337):��������� ���������� (�������)
      L_(49)=.false.
C FDA10_logic.fgi( 105, 337):��������� ���������� (�������)
      R_(86) = 0.0
C FDA10_logic.fgi( 104, 350):��������� (RE4) (�������)
      R_(82) = 950.0
C FDA10_logic.fgi( 110, 350):��������� (RE4) (�������)
      R_(84) = 0.0
C FDA10_logic.fgi( 108, 350):��������� (RE4) (�������)
      L_(50)=.false.
C FDA10_logic.fgi( 103, 371):��������� ���������� (�������)
      L_(51)=.false.
C FDA10_logic.fgi( 105, 371):��������� ���������� (�������)
      R_(93) = 0.0
C FDA10_logic.fgi( 104, 384):��������� (RE4) (�������)
      R_(89) = 950.0
C FDA10_logic.fgi( 110, 384):��������� (RE4) (�������)
      R_(91) = 0.0
C FDA10_logic.fgi( 108, 384):��������� (RE4) (�������)
      R_(90) = 1.0
C FDA10_logic.fgi( 102, 384):��������� (RE4) (�������)
      R_(95) = 0.0
C FDA10_logic.fgi(  86, 385):��������� (RE4) (�������)
      R_(96) = 6.0
C FDA10_logic.fgi(  86, 383):��������� (RE4) (�������)
      L_(62)=I_avax.ne.0
C FDA10_logic.fgi( 314, 378):��������� 1->LO
      L_(60)=I_otix.ne.0
C FDA10_logic.fgi( 252, 427):��������� 1->LO
      if (.not.L_isax.and.(L_osax.or.L_(60))) then
          I_usax = 0
          L_isax = .true.
          L_(56) = .true.
      endif
      if (I_usax .gt. 0) then
         L_(56) = .false.
      endif
      if(L_(57))then
          I_usax = 0
          L_isax = .false.
          L_(56) = .false.
      endif
C FDA10_logic.fgi( 264, 421):���������� ������� ���������,20FDA13EC001
      L_(63)=.false.
C FDA10_logic.fgi( 103, 402):��������� ���������� (�������)
      R_(99) = -3.0
C FDA10_logic.fgi(  87, 407):��������� (RE4) (�������)
      L_(64)=.false.
C FDA10_logic.fgi( 105, 402):��������� ���������� (�������)
      R_(104) = 0.0
C FDA10_logic.fgi( 104, 415):��������� (RE4) (�������)
      R_(100) = 60.0
C FDA10_logic.fgi( 110, 415):��������� (RE4) (�������)
      R_(102) = 0.0
C FDA10_logic.fgi( 108, 415):��������� (RE4) (�������)
      R_(101) = 1.0
C FDA10_logic.fgi( 102, 415):��������� (RE4) (�������)
      R_(106) = 0.0
C FDA10_logic.fgi(  87, 417):��������� (RE4) (�������)
      R_(107) = 3.0
C FDA10_logic.fgi(  87, 415):��������� (RE4) (�������)
      L0_axax=R0_ixax.ne.R0_exax
      R0_exax=R0_ixax
C FDA10_logic.fgi(  28, 413):���������� ������������� ������
      if(L0_axax) then
         R_(105)=R_(107)
      else
         R_(105)=R_(106)
      endif
C FDA10_logic.fgi(  90, 416):���� RE IN LO CH7
      R_(113) = 0.0
C FDA10_logic.fgi( 315, 520):��������� (RE4) (�������)
      R_(114) = 60.0
C FDA10_logic.fgi( 126, 480):��������� (RE4) (�������)
      R_(115) = R_idex * R_(114)
C FDA10_logic.fgi( 134, 482):����������
      R_(116) = 60.0
C FDA10_logic.fgi( 126, 499):��������� (RE4) (�������)
      R_(117) = R_afex * R_(116)
C FDA10_logic.fgi( 134, 501):����������
      R_(118) = 60.0
C FDA10_logic.fgi( 125, 520):��������� (RE4) (�������)
      R_(119) = R_ufex * R_(118)
C FDA10_logic.fgi( 134, 522):����������
      R_(122) = 0.0
C FDA10_logic.fgi( 231, 520):��������� (RE4) (�������)
      L0_omex=R0_apex.ne.R0_umex
      R0_umex=R0_apex
C FDA10_logic.fgi( 168, 577):���������� ������������� ������
      L0_epex=R0_opex.ne.R0_ipex
      R0_ipex=R0_opex
C FDA10_logic.fgi( 168, 590):���������� ������������� ������
      L_imex=L0_epex.or.(L_imex.and..not.(L0_omex))
      L_(72)=.not.L_imex
C FDA10_logic.fgi( 189, 584):RS �������
      !{
      Call DAT_DISCR_HANDLER(deltat,I_alex,L_olex,R_ilex,
     & REAL(R_ulex,4),L_imex,L_ukex,I_elex)
      !}
C FDA10_logic.fgi( 212, 584):���������� ������� ���������,20FDA15AB001QB01
      I_(1) = z'0100000A'
C FDA10_logic.fgi(  94, 534):��������� ������������� IN (�������)
      I_(2) = z'01000002'
C FDA10_logic.fgi(  94, 536):��������� ������������� IN (�������)
      if(L_upox.and..not.L0_adix) then
         R0_obix=R0_ubix
      else
         R0_obix=max(R_(123)-deltat,0.0)
      endif
      L_erox=R0_obix.gt.0.0
      L0_adix=L_upox
C FDA10_logic.fgi(  51, 722):������������  �� T
      L_arox=L_erox
C FDA10_logic.fgi(  86, 722):������,20FDA14AN001KA03YA22
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_ifok,4),R8_ibok
     &,I_ulok,I_imok,I_olok,
     & C8_adok,I_emok,R_udok,R_odok,R_ivik,
     & REAL(R_uvik,4),R_oxik,REAL(R_abok,4),
     & R_axik,REAL(R_ixik,4),I_elok,I_omok,I_amok,I_alok,L_ebok
     &,
     & L_apok,L_arok,L_uxik,L_ubok,L_obok,L_ipok,
     & L_exik,L_ovik,L_idok,L_orok,L_afok,L_efok,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_arox,L_utik,L_epok
     &,I_umok,
     & L_erok,R_ukok,REAL(R_edok,4),L_irok,L_avik,L_urok,L_evik
     &,L_ofok,
     & L_ufok,L_akok,L_ikok,L_okok,L_ekok)
      !}

      if(L_okok.or.L_ikok.or.L_ekok.or.L_akok.or.L_ufok.or.L_ofok
     &) then      
                  I_ilok = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ilok = z'40000000'
      endif
C FDA10_vlv.fgi( 366, 177):���� ���������� �������� � �����������������,20FDA14AN001KA03
      R8_ime = R8_ibok * R8_ikes
C FDA10_vent_log.fgi(  69, 278):����������
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_irak,4),R8_imak
     &,I_utak,I_ivak,I_otak,
     & C8_apak,I_evak,R_upak,R_opak,R_ikak,
     & REAL(R_ukak,4),R_olak,REAL(R_amak,4),
     & R_alak,REAL(R_ilak,4),I_etak,I_ovak,I_avak,I_atak,L_emak
     &,
     & L_axak,L_abek,L_ulak,L_umak,L_omak,L_ixak,
     & L_elak,L_okak,L_ipak,L_obek,L_arak,L_erak,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_erox,L_ufak,L_exak
     &,I_uvak,
     & L_ebek,R_usak,REAL(R_epak,4),L_ibek,L_akak,L_ubek,L_ekak
     &,L_orak,
     & L_urak,L_asak,L_isak,L_osak,L_esak)
      !}

      if(L_osak.or.L_isak.or.L_esak.or.L_asak.or.L_urak.or.L_orak
     &) then      
                  I_itak = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_itak = z'40000000'
      endif
C FDA10_vlv.fgi( 421, 177):���� ���������� �������� � �����������������,20FDA14AN001KA06
      if(L_opox.and..not.L0_odix) then
         R0_edix=R0_idix
      else
         R0_edix=max(R_(124)-deltat,0.0)
      endif
      L_(119)=R0_edix.gt.0.0
      L0_odix=L_opox
C FDA10_logic.fgi(  51, 729):������������  �� T
      if(L_umox.and..not.L0_efix) then
         R0_udix=R0_afix
      else
         R0_udix=max(R_(125)-deltat,0.0)
      endif
      L_(118)=R0_udix.gt.0.0
      L0_efix=L_umox
C FDA10_logic.fgi(  51, 736):������������  �� T
      L_ipox = L_(119).OR.L_(118)
C FDA10_logic.fgi(  68, 728):���
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_osur,4),R8_opur
     &,I_axur,I_oxur,I_uvur,
     & C8_erur,I_ixur,R_asur,R_urur,R_olur,
     & REAL(R_amur,4),R_umur,REAL(R_epur,4),
     & R_emur,REAL(R_omur,4),I_ivur,I_uxur,I_exur,I_evur,L_ipur
     &,
     & L_ebas,L_edas,L_apur,L_arur,L_upur,L_obas,
     & L_imur,L_ulur,L_orur,L_udas,L_esur,L_isur,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_ipox,L_alur,L_ibas
     &,I_abas,
     & L_idas,R_avur,REAL(R_irur,4),L_odas,L_elur,L_afas,L_ilur
     &,L_usur,
     & L_atur,L_etur,L_otur,L_utur,L_itur)
      !}

      if(L_utur.or.L_otur.or.L_itur.or.L_etur.or.L_atur.or.L_usur
     &) then      
                  I_ovur = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ovur = z'40000000'
      endif
C FDA10_vlv.fgi( 235,  44):���� ���������� �������� � �����������������,20FDA14AN001KA15
      if(L_urox.and..not.L0_ufix) then
         R0_ifix=R0_ofix
      else
         R0_ifix=max(R_(126)-deltat,0.0)
      endif
      L_(120)=R0_ifix.gt.0.0
      L0_ufix=L_urox
C FDA10_logic.fgi(  51, 744):������������  �� T
      L_irox = L_(120).OR.L_(119).OR.L_(118)
C FDA10_logic.fgi(  68, 738):���
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_afir,4),R8_abir
     &,I_ilir,I_amir,I_elir,
     & C8_obir,I_ulir,R_idir,R_edir,R_aver,
     & REAL(R_iver,4),R_exer,REAL(R_oxer,4),
     & R_over,REAL(R_axer,4),I_ukir,I_emir,I_olir,I_okir,L_uxer
     &,
     & L_omir,L_opir,L_ixer,L_ibir,L_ebir,L_apir,
     & L_uver,L_ever,L_adir,L_erir,L_odir,L_udir,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_irox,L_iter,L_umir
     &,I_imir,
     & L_upir,R_ikir,REAL(R_ubir,4),L_arir,L_oter,L_irir,L_uter
     &,L_efir,
     & L_ifir,L_ofir,L_akir,L_ekir,L_ufir)
      !}

      if(L_ekir.or.L_akir.or.L_ufir.or.L_ofir.or.L_ifir.or.L_efir
     &) then      
                  I_alir = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_alir = z'40000000'
      endif
C FDA10_vlv.fgi( 291,  44):���� ���������� �������� � �����������������,20FDA14AN001KA18
      if(L_asox.and..not.L0_ikix) then
         R0_akix=R0_ekix
      else
         R0_akix=max(R_(127)-deltat,0.0)
      endif
      L_(121)=R0_akix.gt.0.0
      L0_ikix=L_asox
C FDA10_logic.fgi(  51, 751):������������  �� T
      L_esox = L_(121).OR.L_(120)
C FDA10_logic.fgi(  68, 750):���
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_ivor,4),R8_isor
     &,I_ubur,I_idur,I_obur,
     & C8_ator,I_edur,R_utor,R_otor,R_ipor,
     & REAL(R_upor,4),R_oror,REAL(R_asor,4),
     & R_aror,REAL(R_iror,4),I_ebur,I_odur,I_adur,I_abur,L_esor
     &,
     & L_afur,L_akur,L_uror,L_usor,L_osor,L_ifur,
     & L_eror,L_opor,L_itor,L_okur,L_avor,L_evor,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_esox,L_umor,L_efur
     &,I_udur,
     & L_ekur,R_uxor,REAL(R_etor,4),L_ikur,L_apor,L_ukur,L_epor
     &,L_ovor,
     & L_uvor,L_axor,L_ixor,L_oxor,L_exor)
      !}

      if(L_oxor.or.L_ixor.or.L_exor.or.L_axor.or.L_uvor.or.L_ovor
     &) then      
                  I_ibur = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ibur = z'40000000'
      endif
C FDA10_vlv.fgi( 254,  44):���� ���������� �������� � �����������������,20FDA14AN001KA16
      R_(128) = 0.0
C FDA10_logic.fgi( 343, 706):��������� (RE4) (�������)
      L_(89)=I_etix.ne.0
C FDA10_logic.fgi( 300, 730):��������� 1->LO
      L_(92)=I_etix.ne.0
C FDA10_logic.fgi( 296, 671):��������� 1->LO
      L_(96)=I_etix.ne.0
C FDA10_logic.fgi( 296, 645):��������� 1->LO
      I_(3) = z'0100000A'
C FDA10_logic.fgi( 349, 671):��������� ������������� IN (�������)
      I_(4) = z'01000002'
C FDA10_logic.fgi( 349, 673):��������� ������������� IN (�������)
      I_(5) = z'0100000A'
C FDA10_logic.fgi( 349, 687):��������� ������������� IN (�������)
      I_(6) = z'01000002'
C FDA10_logic.fgi( 349, 689):��������� ������������� IN (�������)
      R_(129) = 15.8
C FDA10_logic.fgi( 343, 726):��������� (RE4) (�������)
      R_(133) = 5
C FDA10_logic.fgi( 322, 707):��������� (RE4) (�������)
      R_(134) = 0.0
C FDA10_logic.fgi( 322, 709):��������� (RE4) (�������)
      L_(104)=I_atix.ne.0
C FDA10_logic.fgi( 295, 699):��������� 1->LO
      L_(105)=I_etix.ne.0
C FDA10_logic.fgi( 295, 703):��������� 1->LO
      R_(136) = 20
C FDA10_logic.fgi( 328, 742):��������� (RE4) (�������)
      R_(137) = 0.0
C FDA10_logic.fgi( 328, 744):��������� (RE4) (�������)
      L_(109)=I_itix.ne.0
C FDA10_logic.fgi( 300, 734):��������� 1->LO
      L_(110)=I_otix.ne.0
C FDA10_logic.fgi( 300, 738):��������� 1->LO
      L0_okix=(L_(104).or.L0_okix).and..not.(L_(110))
      L_(85)=.not.L0_okix
C FDA10_logic.fgi( 317, 694):RS �������
      !��������� R_(138) = FDA10_logicC?? /2000.0/
      R_(138)=R0_utix
C FDA10_logic.fgi(  84, 591):���������
      R_(143) = 1950.0
C FDA10_logic.fgi(  92, 599):��������� (RE4) (�������)
      R_(144) = 0.0
C FDA10_logic.fgi(  92, 601):��������� (RE4) (�������)
      R_(140) = 100.0
C FDA10_logic.fgi(  92, 543):��������� (RE4) (�������)
      R_(141) = 0.0
C FDA10_logic.fgi(  92, 545):��������� (RE4) (�������)
      L_efox=R0_ofox.ne.R0_ifox
      R0_ifox=R0_ofox
C FDA10_logic.fgi(  37, 528):���������� ������������� ������
      L0_ufox=R0_ekox.ne.R0_akox
      R0_akox=R0_ekox
C FDA10_logic.fgi(  37, 541):���������� ������������� ������
      I_(7) = z'0100000A'
C FDA10_logic.fgi( 214, 703):��������� ������������� IN (�������)
      I_(8) = z'01000002'
C FDA10_logic.fgi( 214, 705):��������� ������������� IN (�������)
      I_(9) = z'0100000A'
C FDA10_logic.fgi( 214, 714):��������� ������������� IN (�������)
      I_(10) = z'01000002'
C FDA10_logic.fgi( 214, 716):��������� ������������� IN (�������)
      L_(116)=I_ukox.ne.0
C FDA10_logic.fgi( 158, 726):��������� 1->LO
      L_(117)=I_alox.ne.0
C FDA10_logic.fgi( 158, 737):��������� 1->LO
      R_(146) = 200
C FDA10_logic.fgi( 192, 748):��������� (RE4) (�������)
      R_(149) = 0.0
C FDA10_logic.fgi( 200, 739):��������� (RE4) (�������)
      L_apox=L_epox
C FDA10_logic.fgi(  86, 711):������,20FDA14AN001KA08YA22
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_etat,4),R8_erat
     &,I_oxat,I_ebet,I_ixat,
     & C8_urat,I_abet,R_osat,R_isat,R_emat,
     & REAL(R_omat,4),R_ipat,REAL(R_upat,4),
     & R_umat,REAL(R_epat,4),I_axat,I_ibet,I_uxat,I_uvat,L_arat
     &,
     & L_ubet,L_udet,L_opat,L_orat,L_irat,L_edet,
     & L_apat,L_imat,L_esat,L_ifet,L_usat,L_atat,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_apox,L_olat,L_adet
     &,I_obet,
     & L_afet,R_ovat,REAL(R_asat,4),L_efet,L_ulat,L_ofet,L_amat
     &,L_itat,
     & L_otat,L_utat,L_evat,L_ivat,L_avat)
      !}

      if(L_ivat.or.L_evat.or.L_avat.or.L_utat.or.L_otat.or.L_itat
     &) then      
                  I_exat = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_exat = z'40000000'
      endif
C FDA10_vlv.fgi( 218,  70):���� ���������� �������� � �����������������,20FDA14AN001KA08
      L_orox=L_urox
C FDA10_logic.fgi(  86, 744):������,20FDA14AN001KA17YA22
      !{
      Call PNEUMO_KLAPAN_MAN(deltat,REAL(R_ebor,4),R8_evir
     &,I_ofor,I_ekor,I_ifor,
     & C8_uvir,I_akor,R_oxir,R_ixir,R_esir,
     & REAL(R_osir,4),R_itir,REAL(R_utir,4),
     & R_usir,REAL(R_etir,4),I_afor,I_ikor,I_ufor,I_udor,L_avir
     &,
     & L_ukor,L_ulor,L_otir,L_ovir,L_ivir,L_elor,
     & L_atir,L_isir,L_exir,L_imor,L_uxir,L_abor,
     & REAL(R8_etut,8),REAL(1.0,4),R8_obav,L_orox,L_orir,L_alor
     &,I_okor,
     & L_amor,R_odor,REAL(R_axir,4),L_emor,L_urir,L_omor,L_asir
     &,L_ibor,
     & L_obor,L_ubor,L_edor,L_idor,L_ador)
      !}

      if(L_idor.or.L_edor.or.L_ador.or.L_ubor.or.L_obor.or.L_ibor
     &) then      
                  I_efor = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_efor = z'40000000'
      endif
C FDA10_vlv.fgi( 273,  44):���� ���������� �������� � �����������������,20FDA14AN001KA17
      if(.not.L_akex) then
         R0_ibex=0.0
      elseif(.not.L0_obex) then
         R0_ibex=R_(119)
      else
         R0_ibex=max(R_(110)-deltat,0.0)
      endif
      L_(70)=L_akex.and.R0_ibex.le.0.0
      L0_obex=L_akex
C FDA10_logic.fgi( 143, 516):�������� ��������� ������
C label 537  try537=try537-1
      if(.not.L_udex) then
         R0_oxax=0.0
      elseif(.not.L0_uxax) then
         R0_oxax=R_(115)
      else
         R0_oxax=max(R_(108)-deltat,0.0)
      endif
      L_(66)=L_udex.and.R0_oxax.le.0.0
      L0_uxax=L_udex
C FDA10_logic.fgi( 143, 476):�������� ��������� ������
      if(.not.L_ifex) then
         R0_abex=0.0
      elseif(.not.L0_ebex) then
         R0_abex=R_(117)
      else
         R0_abex=max(R_(109)-deltat,0.0)
      endif
      L_(68)=L_ifex.and.R0_abex.le.0.0
      L0_ebex=L_ifex
C FDA10_logic.fgi( 143, 495):�������� ��������� ������
      if (.not.L_ekex.and.(L_ikex.or.L_adox)) then
          I_okex = 0
          L_ekex = .true.
          L_(71) = .true.
      endif
      if (I_okex .gt. 0) then
         L_(71) = .false.
      endif
      if(L_efox)then
          I_okex = 0
          L_ekex = .false.
          L_(71) = .false.
      endif
C FDA10_logic.fgi( 172, 533):���������� ������� ���������,20FDA15EC001
      if(L_ekex) then
          if (L_(71)) then
              I_okex = 01
              L_akex = .true.
              L_(69) = .false.
          endif
          if (L_(70)) then
              L_(69) = .true.
              L_akex = .false.
          endif
          if (I_okex.ne.01) then
              L_akex = .false.
              L_(69) = .false.
          endif
      else
          L_(69) = .false.
      endif
C FDA10_logic.fgi( 172, 516):��� ������� ���������,20FDA15EC001
C sav1=L_(70)
      if(.not.L_akex) then
         R0_ibex=0.0
      elseif(.not.L0_obex) then
         R0_ibex=R_(119)
      else
         R0_ibex=max(R_(110)-deltat,0.0)
      endif
      L_(70)=L_akex.and.R0_ibex.le.0.0
      L0_obex=L_akex
C FDA10_logic.fgi( 143, 516):recalc:�������� ��������� ������
C if(sav1.ne.L_(70) .and. try537.gt.0) goto 537
      if(L_ekex) then
          if (L_(69)) then
              I_okex = 02
              L_ifex = .true.
              L_(67) = .false.
          endif
          if (L_(68)) then
              L_(67) = .true.
              L_ifex = .false.
          endif
          if (I_okex.ne.02) then
              L_ifex = .false.
              L_(67) = .false.
          endif
      else
          L_(67) = .false.
      endif
C FDA10_logic.fgi( 172, 495):��� ������� ���������,20FDA15EC001
C sav1=L_(68)
      if(.not.L_ifex) then
         R0_abex=0.0
      elseif(.not.L0_ebex) then
         R0_abex=R_(117)
      else
         R0_abex=max(R_(109)-deltat,0.0)
      endif
      L_(68)=L_ifex.and.R0_abex.le.0.0
      L0_ebex=L_ifex
C FDA10_logic.fgi( 143, 495):recalc:�������� ��������� ������
C if(sav1.ne.L_(68) .and. try541.gt.0) goto 541
      if(L_ekex) then
          if (L_(67)) then
              I_okex = 03
              L_udex = .true.
              L_(65) = .false.
          endif
          if (L_(66)) then
              L_(65) = .true.
              L_udex = .false.
          endif
          if (I_okex.ne.03) then
              L_udex = .false.
              L_(65) = .false.
          endif
      else
          L_(65) = .false.
      endif
C FDA10_logic.fgi( 172, 476):��� ������� ���������,20FDA15EC001
C sav1=L_(66)
      if(.not.L_udex) then
         R0_oxax=0.0
      elseif(.not.L0_uxax) then
         R0_oxax=R_(115)
      else
         R0_oxax=max(R_(108)-deltat,0.0)
      endif
      L_(66)=L_udex.and.R0_oxax.le.0.0
      L0_uxax=L_udex
C FDA10_logic.fgi( 143, 476):recalc:�������� ��������� ������
C if(sav1.ne.L_(66) .and. try539.gt.0) goto 539
      L_(22) = L_efox.OR.L_(65)
C FDA10_logic.fgi(  68, 537):���
      L_adox=L0_ufox.or.(L_adox.and..not.(L_(22)))
      L_(23)=.not.L_adox
C FDA10_logic.fgi(  75, 539):RS �������
C sav1=L_(71)
      if (.not.L_ekex.and.(L_ikex.or.L_adox)) then
          I_okex = 0
          L_ekex = .true.
          L_(71) = .true.
      endif
      if (I_okex .gt. 0) then
         L_(71) = .false.
      endif
      if(L_efox)then
          I_okex = 0
          L_ekex = .false.
          L_(71) = .false.
      endif
C FDA10_logic.fgi( 172, 533):recalc:���������� ������� ���������,20FDA15EC001
C if(sav1.ne.L_(71) .and. try543.gt.0) goto 543
      if(L_adox) then
         R_ubox=R_(140)
      else
         R_ubox=R_(141)
      endif
C FDA10_logic.fgi(  97, 544):���� RE IN LO CH7
      !{
      Call DAT_ANA_HANDLER(deltat,R_ixop,R_afup,REAL(1,4)
     &,
     & REAL(R_abup,4),REAL(R_ebup,4),
     & REAL(R_exop,4),REAL(R_axop,4),I_udup,
     & REAL(R_obup,4),L_ubup,REAL(R_adup,4),L_edup,L_idup
     &,R_ibup,
     & REAL(R_uxop,4),REAL(R_oxop,4),L_odup,REAL(R_ubox,4
     &))
      !}
C FDA10_vlv.fgi(  87, 166):���������� �������,20FDA15AF001XQ02
      if(L_adox) then
         I_upex=I_(1)
      else
         I_upex=I_(2)
      endif
C FDA10_logic.fgi(  97, 534):���� RE IN LO CH7
      if(L_(65)) then
         I_okex=0
         L_ekex=.false.
      endif
C FDA10_logic.fgi( 172, 457):����� ������� ���������,20FDA15EC001
      L0_obax=L_(65).or.(L0_obax.and..not.(L_adax))
      L_(32)=.not.L0_obax
C FDA10_logic.fgi( 230, 463):RS �������
      if(L0_obax) then
         R_ubax=R_(60)
      else
         R_ubax=R_(61)
      endif
C FDA10_logic.fgi( 240, 472):���� RE IN LO CH7
      if(L_akex) then
         R_(112)=R_edex
      else
         R_(112)=R_(113)
      endif
C FDA10_logic.fgi( 324, 519):���� RE IN LO CH7
      if(L_ifex) then
         R_(111)=R_adex
      else
         R_(111)=R_(112)
      endif
C FDA10_logic.fgi( 324, 498):���� RE IN LO CH7
      if(L_udex) then
         R_obox=R_ubex
      else
         R_obox=R_(111)
      endif
C FDA10_logic.fgi( 324, 479):���� RE IN LO CH7
      !{
      Call DAT_ANA_HANDLER(deltat,R_okiv,R_epiv,REAL(1,4)
     &,
     & REAL(R_eliv,4),REAL(R_iliv,4),
     & REAL(R_ikiv,4),REAL(R_ekiv,4),I_apiv,
     & REAL(R_uliv,4),L_amiv,REAL(R_emiv,4),L_imiv,L_omiv
     &,R_oliv,
     & REAL(R_aliv,4),REAL(R_ukiv,4),L_umiv,REAL(R_obox,4
     &))
      !}
C FDA10_vlv.fgi(  87, 180):���������� �������,20FDA15AF001XQ01
      if(L_akex) then
         R_(121)=R_ofex
      else
         R_(121)=R_(122)
      endif
C FDA10_logic.fgi( 240, 519):���� RE IN LO CH7
      if(L_ifex) then
         R_(120)=R_efex
      else
         R_(120)=R_(121)
      endif
C FDA10_logic.fgi( 240, 498):���� RE IN LO CH7
      if(L_udex) then
         R_afox=R_odex
      else
         R_afox=R_(120)
      endif
C FDA10_logic.fgi( 240, 479):���� RE IN LO CH7
      R_(145) = R_afox + (-R_udox)
C FDA10_logic.fgi(  46, 560):��������
      L_(115)=R_(145).gt.R0_odox
C FDA10_logic.fgi(  59, 564):���������� >
      L_(114)=R_(145).lt.R0_idox
C FDA10_logic.fgi(  59, 556):���������� <
      L0_edox=L_(115).or.(L0_edox.and..not.(L_(114)))
      L_(113)=.not.L0_edox
C FDA10_logic.fgi(  74, 560):RS �������
      L_(112) = L0_edox.AND.L_adox
C FDA10_logic.fgi(  86, 561):�
      if(L_(112)) then
         R_(142)=R_(143)
      else
         R_(142)=R_(144)
      endif
C FDA10_logic.fgi(  97, 600):���� RE IN LO CH7
      L_(111) = L_(115).AND.L_(114)
C FDA10_logic.fgi(  86, 566):�
      Call C_PID(deltat,L_ixix,R8_ivix,REAL(R_ovix,4),REAL
     &(R_evix,4),R_uxix,
     & REAL(R_uvix,4),REAL(R_avix,4),REAL(R_axix,4),R_oxix
     &,REAL(R_exix,4),R_ebox,R_abox,
     & REAL(R_(145),4))
C FDA10_logic.fgi(  69, 586):��������� �����������,20FDA15DT003
      R_(139) = R8_ivix * R_(138)
C FDA10_logic.fgi(  90, 592):����������
      if(L_(111)) then
         R8_ibox=R_(139)
      else
         R8_ibox=R_(142)
      endif
C FDA10_logic.fgi(  97, 592):���� RE IN LO CH7
      if(.not.L_utax) then
         R0_etax=0.0
      elseif(.not.L0_itax) then
         R0_etax=R_(97)
      else
         R0_etax=max(R_(98)-deltat,0.0)
      endif
      L_(59)=L_utax.and.R0_etax.le.0.0
      L0_itax=L_utax
C FDA10_logic.fgi( 242, 404):�������� ��������� ������
C label 624  try624=try624-1
      L_(61)=L_utax.and..not.L0_otax
      L0_otax=L_utax
C FDA10_logic.fgi( 294, 404):������������  �� 1 ���
      if (.not.L_(62).and.L_(61)) then
          I_ivax = I_ivax+1
      endif
      if (L_(62)) then
          I_ivax = 0
      endif
C FDA10_logic.fgi( 320, 378):�������
      L_(58)=I_ivax.lt.I_atax
C FDA10_logic.fgi( 352, 364):���������� �������������
      L_(54) = L_(56).OR.L_(58)
C FDA10_logic.fgi( 272, 412):���
      if(L_isax) then
          if (L_(54)) then
              I_usax = 01
              L_utax = .true.
              L_(55) = .false.
          endif
          if (L_(59)) then
              L_(55) = .true.
              L_utax = .false.
          endif
          if (I_usax.ne.01) then
              L_utax = .false.
              L_(55) = .false.
          endif
      else
          L_(55) = .false.
      endif
C FDA10_logic.fgi( 264, 404):��� ������� ���������,20FDA13EC001
C sav1=L_(59)
      if(.not.L_utax) then
         R0_etax=0.0
      elseif(.not.L0_itax) then
         R0_etax=R_(97)
      else
         R0_etax=max(R_(98)-deltat,0.0)
      endif
      L_(59)=L_utax.and.R0_etax.le.0.0
      L0_itax=L_utax
C FDA10_logic.fgi( 242, 404):recalc:�������� ��������� ������
C if(sav1.ne.L_(59) .and. try624.gt.0) goto 624
      L_ovax=L_utax
C FDA10_logic.fgi( 304, 421):������,20FDA13CW002XC02
      if(L_ovax) then
         R_(103)=R_(99)
      else
         R_(103)=R_(105)
      endif
C FDA10_logic.fgi(  90, 408):���� RE IN LO CH7
      if(L_(63)) then
         R_uvax=R_(104)
      elseif(L_(64)) then
         R_uvax=R_uvax
      else
         R_uvax=R_uvax+deltat/R_(101)*R_(103)
      endif
      if(R_uvax.gt.R_(100)) then
         R_uvax=R_(100)
      elseif(R_uvax.lt.R_(102)) then
         R_uvax=R_(102)
      endif
C FDA10_logic.fgi( 108, 409):����������
      R_emix=R_uvax
C FDA10_logic.fgi( 138, 409):������,20FDA13CW002XQ01_input
      L_(95)=R_emix.gt.R0_ulix
C FDA10_logic.fgi( 314, 654):���������� >
      L_(91)=R_emix.gt.R0_elix
C FDA10_logic.fgi( 314, 680):���������� >
      !{
      Call DAT_ANA_HANDLER(deltat,R_omev,R_esev,REAL(1,4)
     &,
     & REAL(R_epev,4),REAL(R_ipev,4),
     & REAL(R_imev,4),REAL(R_emev,4),I_asev,
     & REAL(R_upev,4),L_arev,REAL(R_erev,4),L_irev,L_orev
     &,R_opev,
     & REAL(R_apev,4),REAL(R_umev,4),L_urev,REAL(R_emix,4
     &))
      !}
C FDA10_vlv.fgi( 114, 180):���������� �������,20FDA13CW002XQ01
      L_(45)=R_uvax.gt.R0_ipax
C FDA10_logic.fgi( 118, 401):���������� >
      if(L_(45)) then
         R_(73)=1
      else
         R_(73)=0
      endif
C FDA10_logic.fgi( 118, 394):��������� LO->1
      if(L_ovax) then
         R_(94)=R_(96)
      else
         R_(94)=R_(95)
      endif
C FDA10_logic.fgi(  89, 384):���� RE IN LO CH7
      R_(88) = R_(73) * R_(94)
C FDA10_logic.fgi(  95, 379):����������
      R_(92) = R_(88) + (-R_(87))
C FDA10_logic.fgi(  99, 378):��������
      if(L_(50)) then
         R_esax=R_(93)
      elseif(L_(51)) then
         R_esax=R_esax
      else
         R_esax=R_esax+deltat/R_(90)*R_(92)
      endif
      if(R_esax.gt.R_(89)) then
         R_esax=R_(89)
      elseif(R_esax.lt.R_(91)) then
         R_esax=R_(91)
      endif
C FDA10_logic.fgi( 108, 378):����������
      R_ipix=R_esax
C FDA10_logic.fgi( 138, 378):������,20FDA13CL001XQ01_input
      L_(98)=R_ipix.lt.R0_imix
C FDA10_logic.fgi( 296, 693):���������� <
      L_(101) = L_(104).OR.L_(98)
C FDA10_logic.fgi( 302, 698):���
      L_(102)=L_(101).and..not.L0_urix
      L0_urix=L_(101)
C FDA10_logic.fgi( 310, 699):������������  �� 1 ���
      L0_asix=(L_(105).or.L0_asix).and..not.(L_(102))
      L_(103)=.not.L0_asix
C FDA10_logic.fgi( 317, 701):RS �������
      if(L0_asix) then
         R_(132)=R_(133)
      else
         R_(132)=R_(134)
      endif
C FDA10_logic.fgi( 325, 708):���� RE IN LO CH7
      R0_apix=R0_apix+deltat/R0_umix*R_(132)
      if(R0_apix.gt.R0_omix) then
         R0_apix=R0_omix
      elseif(R0_apix.lt.R0_epix) then
         R0_apix=R0_epix
      endif
C FDA10_logic.fgi( 336, 707):����������
      L0_amix=(L_(101).or.L0_amix).and..not.(L_(96))
      L_(97)=.not.L0_amix
C FDA10_logic.fgi( 313, 647):RS �������
      L0_ilix=(L_(101).or.L0_ilix).and..not.(L_(92))
      L_(93)=.not.L0_ilix
C FDA10_logic.fgi( 313, 673):RS �������
      L_(94)=R_ipix.gt.R0_olix
C FDA10_logic.fgi( 313, 638):���������� >
      L_(99) = L_(95).AND.L0_amix.AND.L_(94)
C FDA10_logic.fgi( 327, 652):�
      if(L_(99)) then
         I_opix=I_(3)
      else
         I_opix=I_(4)
      endif
C FDA10_logic.fgi( 352, 671):���� RE IN LO CH7
      L_(90)=R_ipix.lt.R0_alix
C FDA10_logic.fgi( 313, 664):���������� <
      L_(100) = L_(91).AND.L0_ilix.AND.L_(90)
C FDA10_logic.fgi( 327, 678):�
      if(L_(100)) then
         I_upix=I_(5)
      else
         I_upix=I_(6)
      endif
C FDA10_logic.fgi( 352, 687):���� RE IN LO CH7
      !{
      Call DAT_ANA_HANDLER(deltat,R_ifev,R_amev,REAL(1,4)
     &,
     & REAL(R_akev,4),REAL(R_ekev,4),
     & REAL(R_efev,4),REAL(R_afev,4),I_ulev,
     & REAL(R_okev,4),L_ukev,REAL(R_alev,4),L_elev,L_ilev
     &,R_ikev,
     & REAL(R_ufev,4),REAL(R_ofev,4),L_olev,REAL(R_ipix,4
     &))
      !}
C FDA10_vlv.fgi( 114, 166):���������� �������,20FDA13CL001XQ01
      L_(88)=R_ipix.gt.R0_ukix
C FDA10_logic.fgi( 301, 724):���������� >
      L_(106) = L_(109).OR.L_(89).OR.L_(88)
C FDA10_logic.fgi( 306, 732):���
      L_(107)=L_(106).and..not.L0_osix
      L0_osix=L_(106)
C FDA10_logic.fgi( 314, 734):������������  �� 1 ���
      L0_usix=(L_(110).or.L0_usix).and..not.(L_(107))
      L_(108)=.not.L0_usix
C FDA10_logic.fgi( 323, 736):RS �������
      if(L0_usix) then
         R_(135)=R_(136)
      else
         R_(135)=R_(137)
      endif
C FDA10_logic.fgi( 331, 743):���� RE IN LO CH7
      L_(86) = L0_usix.OR.L_(94)
C FDA10_logic.fgi( 333, 694):���
      L_(87) = L_(86).AND.(.NOT.L0_asix).AND.(.NOT.L0_okix
     &)
C FDA10_logic.fgi( 340, 698):�
      if(L_(87)) then
         R_orix=R_(128)
      else
         R_orix=R0_apix
      endif
C FDA10_logic.fgi( 346, 707):���� RE IN LO CH7
      R_(130) = R_(129) * R_orix
C FDA10_logic.fgi( 346, 726):����������
      R_(131) = R_(135) + (-R_(130))
C FDA10_logic.fgi( 338, 743):��������
      R_isix=R_isix+deltat/R0_erix*R_(131)
      if(R_isix.gt.R0_arix) then
         R_isix=R0_arix
      elseif(R_isix.lt.R0_irix) then
         R_isix=R0_irix
      endif
C FDA10_logic.fgi( 346, 741):����������
      R_esix=R_isix
C FDA10_logic.fgi( 368, 743):������,_____20FDA13CL001XQ01_input
      L_(44)=R_esax.gt.R0_epax
C FDA10_logic.fgi( 118, 369):���������� >
      if(L_(44)) then
         R_(72)=1
      else
         R_(72)=0
      endif
C FDA10_logic.fgi( 118, 362):��������� LO->1
      R_(81) = R_(72) * R_(87)
C FDA10_logic.fgi(  91, 347):����������
      R_(85) = R_(81) + (-R_(80))
C FDA10_logic.fgi(  99, 344):��������
      if(L_(48)) then
         R_asax=R_(86)
      elseif(L_(49)) then
         R_asax=R_asax
      else
         R_asax=R_asax+deltat/R_(83)*R_(85)
      endif
      if(R_asax.gt.R_(82)) then
         R_asax=R_(82)
      elseif(R_asax.lt.R_(84)) then
         R_asax=R_(84)
      endif
C FDA10_logic.fgi( 108, 344):����������
      R_urax=R_asax
C FDA10_logic.fgi( 138, 344):������,__20FDA13CL001XQ01_input
      L_(42)=R_asax.gt.R0_imax
C FDA10_logic.fgi( 118, 335):���������� >
      if(L_(42)) then
         R_(66)=1
      else
         R_(66)=0
      endif
C FDA10_logic.fgi( 118, 328):��������� LO->1
      R_(70) = R_(66) * R_(65) * R_(80) * R_(77)
C FDA10_logic.fgi(  91, 310):����������
      if(L_omax) then
         R_apax=R_(71)
      elseif(L_(43)) then
         R_apax=R_apax
      else
         R_apax=R_apax+deltat/R_(68)*R_(70)
      endif
      if(R_apax.gt.R_(67)) then
         R_apax=R_(67)
      elseif(R_apax.lt.R_(69)) then
         R_apax=R_(69)
      endif
C FDA10_logic.fgi( 108, 310):����������
      R_umax=R_apax
C FDA10_logic.fgi( 138, 310):������,20FDA10_UO2_MASS
      L_(27)=R_asax.gt.R0_ipuv
C FDA10_logic.fgi( 136, 337):���������� >
      !{
      Call DAT_DISCR_HANDLER(deltat,I_amuv,L_omuv,R_imuv,
     & REAL(R_umuv,4),L_(27),L_uluv,I_emuv)
      !}
C FDA10_logic.fgi( 170, 336):���������� ������� ���������,20FDA13AM003XM01
      I_evax=I_ivax
C FDA10_logic.fgi( 346, 374):������,20FDA13AM001_COUNT_CYCLE
      L_(53)=I_ivax.eq.I_atax
C FDA10_logic.fgi( 363, 360):���������� �������������
      L_(52) = L_(55).AND.L_(53)
C FDA10_logic.fgi( 272, 392):�
      if(L_(52)) then
         I_usax=0
         L_isax=.false.
      endif
C FDA10_logic.fgi( 264, 381):����� ������� ���������,20FDA13EC001
      Call PUMP_HANDLER(deltat,C30_ili,I_epi,L_eli,L_uli,L_oti
     &,
     & I_ipi,I_api,R_aki,REAL(R_iki,4),
     & R_ifi,REAL(R_ufi,4),I_opi,L_eti,L_esi,L_exi,
     & L_ixi,L_imi,L_iri,L_uri,L_ori,L_asi,L_usi,L_efi,
     & L_eki,L_afi,L_ofi,L_ovi,L_(15),
     & L_uvi,L_(14),L_udi,L_odi,L_omi,I_upi,R_uti,R_avi,
     & L_abo,L_ati,L_axi,REAL(R8_etut,8),L_osi,
     & REAL(R8_isi,8),R_oxi,REAL(R_ari,4),R_eri,REAL(R8_emi
     &,8),R_ami,
     & R8_obav,R_uxi,R8_isi,REAL(R_oki,4),REAL(R_uki,4))
C FDA10_vlv.fgi( 385, 150):���������� ���������� �������,20FDA13CU001KN01
C label 776  try776=try776-1
C sav1=R_uxi
C sav2=R_oxi
C sav3=L_eti
C sav4=L_usi
C sav5=L_esi
C sav6=R8_isi
C sav7=R_eri
C sav8=I_upi
C sav9=I_opi
C sav10=I_ipi
C sav11=I_epi
C sav12=I_api
C sav13=I_umi
C sav14=L_omi
C sav15=L_imi
C sav16=R_ami
C sav17=C30_ili
C sav18=L_eki
C sav19=L_ofi
      Call PUMP_HANDLER(deltat,C30_ili,I_epi,L_eli,L_uli,L_oti
     &,
     & I_ipi,I_api,R_aki,REAL(R_iki,4),
     & R_ifi,REAL(R_ufi,4),I_opi,L_eti,L_esi,L_exi,
     & L_ixi,L_imi,L_iri,L_uri,L_ori,L_asi,L_usi,L_efi,
     & L_eki,L_afi,L_ofi,L_ovi,L_(15),
     & L_uvi,L_(14),L_udi,L_odi,L_omi,I_upi,R_uti,R_avi,
     & L_abo,L_ati,L_axi,REAL(R8_etut,8),L_osi,
     & REAL(R8_isi,8),R_oxi,REAL(R_ari,4),R_eri,REAL(R8_emi
     &,8),R_ami,
     & R8_obav,R_uxi,R8_isi,REAL(R_oki,4),REAL(R_uki,4))
C FDA10_vlv.fgi( 385, 150):recalc:���������� ���������� �������,20FDA13CU001KN01
C if(sav1.ne.R_uxi .and. try776.gt.0) goto 776
C if(sav2.ne.R_oxi .and. try776.gt.0) goto 776
C if(sav3.ne.L_eti .and. try776.gt.0) goto 776
C if(sav4.ne.L_usi .and. try776.gt.0) goto 776
C if(sav5.ne.L_esi .and. try776.gt.0) goto 776
C if(sav6.ne.R8_isi .and. try776.gt.0) goto 776
C if(sav7.ne.R_eri .and. try776.gt.0) goto 776
C if(sav8.ne.I_upi .and. try776.gt.0) goto 776
C if(sav9.ne.I_opi .and. try776.gt.0) goto 776
C if(sav10.ne.I_ipi .and. try776.gt.0) goto 776
C if(sav11.ne.I_epi .and. try776.gt.0) goto 776
C if(sav12.ne.I_api .and. try776.gt.0) goto 776
C if(sav13.ne.I_umi .and. try776.gt.0) goto 776
C if(sav14.ne.L_omi .and. try776.gt.0) goto 776
C if(sav15.ne.L_imi .and. try776.gt.0) goto 776
C if(sav16.ne.R_ami .and. try776.gt.0) goto 776
C if(sav17.ne.C30_ili .and. try776.gt.0) goto 776
C if(sav18.ne.L_eki .and. try776.gt.0) goto 776
C if(sav19.ne.L_ofi .and. try776.gt.0) goto 776
      if(L_usi) then
         R_(33)=R_(34)
      else
         R_(33)=R_(35)
      endif
C FDA10_vent_log.fgi( 217, 419):���� RE IN LO CH7
      R8_ox=(R0_ix*R_(29)+deltat*R_(33))/(R0_ix+deltat)
C FDA10_vent_log.fgi( 229, 420):�������������� �����  
      R8_ex=R8_ox
C FDA10_vent_log.fgi( 246, 420):������,F_FDA13CU001_G
      L_(78)=R_imox.lt.R0_irex
C FDA10_logic.fgi( 176, 660):���������� <
C label 784  try784=try784-1
      L_(77)=R_imox.lt.R0_erex
C FDA10_logic.fgi( 176, 649):���������� <
      L_(76)=R_imox.gt.R0_arex
C FDA10_logic.fgi( 176, 643):���������� >
      L_(79) = L_(77).AND.L_(76)
C FDA10_logic.fgi( 182, 648):�
      L_(75)=L_(79).and..not.L0_esuv
      L0_esuv=L_(79)
C FDA10_logic.fgi( 168, 688):������������  �� 1 ���
      L_(74)=L_(78).and..not.L0_osuv
      L0_osuv=L_(78)
C FDA10_logic.fgi( 164, 688):������������  �� 1 ���
      L_(80)=R_imox.gt.R0_orex
C FDA10_logic.fgi( 176, 635):���������� >
      L_(73)=L_(80).and..not.L0_isuv
      L0_isuv=L_(80)
C FDA10_logic.fgi( 166, 688):������������  �� 1 ���
      L_(83) = L_(116).OR.L_(75).OR.L_(74).OR.L_(73)
C FDA10_logic.fgi( 168, 733):���
      L0_amox=L_(117).or.(L0_amox.and..not.(L_(83)))
      L_(84)=.not.L0_amox
C FDA10_logic.fgi( 179, 735):RS �������
      if(L0_amox) then
         R_(148)=R_(146)
      else
         R_(148)=R_(149)
      endif
C FDA10_logic.fgi( 204, 749):���� RE IN LO CH7
      L_(81) = L_(117).OR.L_(75).OR.L_(74).OR.L_(73)
C FDA10_logic.fgi( 173, 720):���
      L0_ulox=L_(116).or.(L0_ulox.and..not.(L_(81)))
      L_(82)=.not.L0_ulox
C FDA10_logic.fgi( 179, 724):RS �������
      if(L0_ulox) then
         R_(147)=R_(146)
      else
         R_(147)=R_(149)
      endif
C FDA10_logic.fgi( 204, 731):���� RE IN LO CH7
      R_emox = R_(148) + (-R_(147))
C FDA10_logic.fgi( 210, 742):��������
      R_omox=R_omox+deltat/R0_ilox*R_emox
      if(R_omox.gt.R0_elox) then
         R_omox=R0_elox
      elseif(R_omox.lt.R0_olox) then
         R_omox=R0_olox
      endif
C FDA10_logic.fgi( 225, 726):����������
      R_imox=R_omox
C FDA10_logic.fgi( 250, 728):������,20FDA16AE401VX01
      if(L0_ulox) then
         I_ikox=I_(7)
      else
         I_ikox=I_(8)
      endif
C FDA10_logic.fgi( 217, 703):���� RE IN LO CH7
      L_(31) = L0_ulox.OR.L0_amox
C FDA10_logic.fgi( 189, 688):���
      !{
      Call DAT_DISCR_HANDLER(deltat,I_exuv,L_uxuv,R_oxuv,
     & REAL(R_abax,4),L_(31),L_axuv,I_ixuv)
      !}
C FDA10_logic.fgi( 230, 686):���������� ������� ���������,20FDA16AE402QB01
      L_(30) = (.NOT.L_(31))
C FDA10_logic.fgi( 203, 674):���
      if(.not.L_(30)) then
         R0_atuv=0.0
      elseif(.not.L0_etuv) then
         R0_atuv=R0_usuv
      else
         R0_atuv=max(R_(59)-deltat,0.0)
      endif
      L_(29)=L_(30).and.R0_atuv.le.0.0
      L0_etuv=L_(30)
C FDA10_logic.fgi( 213, 674):�������� ��������� ������
      !{
      Call DAT_DISCR_HANDLER(deltat,I_otuv,L_evuv,R_avuv,
     & REAL(R_ivuv,4),L_(29),L_ituv,I_utuv)
      !}
C FDA10_logic.fgi( 230, 672):���������� ������� ���������,20FDA16AE402QB02
      if(L0_amox) then
         I_okox=I_(9)
      else
         I_okox=I_(10)
      endif
C FDA10_logic.fgi( 217, 714):���� RE IN LO CH7
      !{
      Call DAT_DISCR_HANDLER(deltat,I_exex,L_uxex,R_oxex,
     & REAL(R_abix,4),L_(80),L_axex,I_ixex)
      !}
C FDA10_logic.fgi( 196, 634):���������� ������� ���������,20FDA16AE401QB03
      L0_odax=L_(80).or.(L0_odax.and..not.(L_idax))
      L_(33)=.not.L0_odax
C FDA10_logic.fgi( 208, 623):RS �������
      if(L0_odax) then
         R_edax=R_(62)
      else
         R_edax=R_(63)
      endif
C FDA10_logic.fgi( 217, 632):���� RE IN LO CH7
      !{
      Call DAT_DISCR_HANDLER(deltat,I_otex,L_evex,R_avex,
     & REAL(R_ivex,4),L_(79),L_itex,I_utex)
      !}
C FDA10_logic.fgi( 196, 646):���������� ������� ���������,20FDA16AE401QB02
      !{
      Call DAT_DISCR_HANDLER(deltat,I_asex,L_osex,R_isex,
     & REAL(R_usex,4),L_(78),L_urex,I_esex)
      !}
C FDA10_logic.fgi( 196, 658):���������� ������� ���������,20FDA16AE401QB01
      End
