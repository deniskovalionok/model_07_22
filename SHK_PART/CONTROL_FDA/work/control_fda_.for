       SUBROUTINE SPW_Init_tab(iTabVer)
       integer*4 iTabVer
       TYPE TSubr
         sequence
         integer*4 addr
         integer*4 period
         integer*4 phase
         integer*4 object
         integer*4 allowed
         integer*4 ncall
         real*8    time
         integer*4 individualScale
         integer*4 rezerv(2),flags,CPUTime(2)
         character(LEN=32) name
       END TYPE TSubr

       TYPE(TSubr) SPW_Table_Sub(10)
       Common/SPW_Table_Sub/ SPW_Table_Sub
       external FDA_GENERAL
       Data SPW_Table_Sub(1)/TSubr(0,1,0,2,0,0,0.,0,0,8,0,
     &   'FDA_GENERAL')/
       external FDA10
       Data SPW_Table_Sub(2)/TSubr(0,1,0,3,0,0,0.,0,0,8,0,
     &   'FDA10')/
       external FDA20
       Data SPW_Table_Sub(3)/TSubr(0,1,0,4,0,0,0.,0,0,8,0,
     &   'FDA20')/
       external FDA30
       Data SPW_Table_Sub(4)/TSubr(0,1,0,5,0,0,0.,0,0,8,0,
     &   'FDA30')/
       external FDA40
       Data SPW_Table_Sub(5)/TSubr(0,1,0,6,0,0,0.,0,0,8,0,
     &   'FDA40')/
       external FDA50
       Data SPW_Table_Sub(6)/TSubr(0,1,0,7,0,0,0.,0,0,8,0,
     &   'FDA50')/
       external FDA60
       Data SPW_Table_Sub(7)/TSubr(0,1,0,8,0,0,0.,0,0,8,0,
     &   'FDA60')/
       external FDA60B
       Data SPW_Table_Sub(8)/TSubr(0,1,0,9,0,0,0.,0,0,8,0,
     &   'FDA60B')/
       external FDA70
       Data SPW_Table_Sub(9)/TSubr(0,1,0,10,0,0,0.,0,0,8,0,
     &   'FDA70')/
       external FDA90
       Data SPW_Table_Sub(10)/TSubr(0,1,0,11,0,0,0.,0,0,8,0,
     &   'FDA90')/

       LOGICAL*1 SPW_0(0:196607)
       Common/SPW_0/ SPW_0 !
       CHARACTER*1 SPW_1(0:20479)
       Common/SPW_1/ SPW_1 !
       LOGICAL*1 SPW_2(0:65535)
       Common/SPW_2/ SPW_2 !
       CHARACTER*1 SPW_3(0:5119)
       Common/SPW_3/ SPW_3 !
       LOGICAL*1 SPW_4(0:28671)
       Common/SPW_4/ SPW_4 !
       CHARACTER*1 SPW_5(0:1279)
       Common/SPW_5/ SPW_5 !
       LOGICAL*1 SPW_6(0:28671)
       Common/SPW_6/ SPW_6 !
       CHARACTER*1 SPW_7(0:8191)
       Common/SPW_7/ SPW_7 !
       LOGICAL*1 SPW_8(0:14335)
       Common/SPW_8/ SPW_8 !
       CHARACTER*1 SPW_9(0:2559)
       Common/SPW_9/ SPW_9 !
       LOGICAL*1 SPW_10(0:4095)
       Common/SPW_10/ SPW_10 !
       CHARACTER*1 SPW_11(0:1791)
       Common/SPW_11/ SPW_11 !
       LOGICAL*1 SPW_12(0:57343)
       Common/SPW_12/ SPW_12 !
       CHARACTER*1 SPW_13(0:2047)
       Common/SPW_13/ SPW_13 !
       LOGICAL*1 SPW_14(0:32767)
       Common/SPW_14/ SPW_14 !
       LOGICAL*1 SPW_15(0:3583)
       Common/SPW_15/ SPW_15 !
       CHARACTER*1 SPW_16(0:191)
       Common/SPW_16/ SPW_16 !
       LOGICAL*1 SPW_17(0:14335)
       Common/SPW_17/ SPW_17 !
       CHARACTER*1 SPW_18(0:639)
       Common/SPW_18/ SPW_18 !
       LOGICAL*1 SPW_19(0:15)
       Common/SPW_19/ SPW_19 !
       LOGICAL*1 SPW_20(0:511)
       Common/SPW_20/ SPW_20 !
       LOGICAL*1 SPW_21(0:15)
       Common/SPW_21/ SPW_21 !
       CHARACTER*1 SPW_22(0:31)
       Common/SPW_22/ SPW_22 !
       LOGICAL*1 SPW_23(0:15)
       Common/SPW_23/ SPW_23 !
       LOGICAL*1 SPW_24(0:511)
       Common/SPW_24/ SPW_24 !
       LOGICAL*1 SPW_25(0:31)
       Common/SPW_25/ SPW_25 !
       LOGICAL*1 SPW_26(0:15)
       Common/SPW_26/ SPW_26 !
       LOGICAL*1 SPW_27(0:15)
       Common/SPW_27/ SPW_27 !
       LOGICAL*1 SPW_28(0:15)
       Common/SPW_28/ SPW_28 !
       LOGICAL*1 SPW_29(0:15)
       Common/SPW_29/ SPW_29 !
       LOGICAL*1 SPW_30(0:15)
       Common/SPW_30/ SPW_30 !
       LOGICAL*1 SPW_31(0:15)
       Common/SPW_31/ SPW_31 !
       LOGICAL*1 SPW_32(0:767)
       Common/SPW_32/ SPW_32 !
       LOGICAL*1 SPW_33(0:31)
       Common/SPW_33/ SPW_33 !

       real*4 info_kwant
       integer*4 info_siz
       common/spw_info/ info_siz,info_kwant
       !MS$ATTRIBUTES DLLEXPORT::spw_info
       Data info_siz/8/,info_kwant/0.125/
       integer*4 dispStateHdr
       integer*4 stepCounter
       real*8    subInfo(40)
       common/spw_dispState/ dispStateHdr,stepCounter,subInfo

       character*(32) ver(11)
       integer*4 nver,versiz
       common/spw_versions/ nver,versiz,ver
       !MS$ATTRIBUTES DLLEXPORT::spw_versions
       Data nver/11/,versiz/32/
       Data ver(1)/''/
       Data ver(2)/'v_47FCA184_FDA_GENERAL'/
       Data ver(3)/'v_8787D5E0_FDA10'/
       Data ver(4)/'v_B8333160_FDA20'/
       Data ver(5)/'v_37AFBFEF_FDA30'/
       Data ver(6)/'v_F101DCAE_FDA40'/
       Data ver(7)/'v_312D77A2_FDA50'/
       Data ver(8)/'v_5413EA2A_FDA60'/
       Data ver(9)/'v_028C3848_FDA60B'/
       Data ver(10)/'v_96816E8B_FDA70'/
       Data ver(11)/'v_FA0BA506_FDA90'/

       CALL SPW_SET_SIGN(787968374,0,-80532086)
       iTabVer=iTabVer*1000+1088

       CALL SPW_SET_UIDTR(-1604603056,-708551545)
       CALL SPW_SET_KWANT(REAL(0.125))
       CALL SPW_SET_NCMN(34)
       CALL SPW_ADD_CMN(LOC(SPW_0),175369)
       CALL SPW_ADD_CMN(LOC(SPW_1),16980)
       CALL SPW_ADD_CMN(LOC(SPW_2),62165)
       CALL SPW_ADD_CMN(LOC(SPW_3),4510)
       CALL SPW_ADD_CMN(LOC(SPW_4),24769)
       CALL SPW_ADD_CMN(LOC(SPW_5),1256)
       CALL SPW_ADD_CMN(LOC(SPW_6),26845)
       CALL SPW_ADD_CMN(LOC(SPW_7),7394)
       CALL SPW_ADD_CMN(LOC(SPW_8),12693)
       CALL SPW_ADD_CMN(LOC(SPW_9),2420)
       CALL SPW_ADD_CMN(LOC(SPW_10),4069)
       CALL SPW_ADD_CMN(LOC(SPW_11),1618)
       CALL SPW_ADD_CMN(LOC(SPW_12),53749)
       CALL SPW_ADD_CMN(LOC(SPW_13),1982)
       CALL SPW_ADD_CMN(LOC(SPW_14),30058)
       CALL SPW_ADD_CMN(LOC(SPW_15),3098)
       CALL SPW_ADD_CMN(LOC(SPW_16),158)
       CALL SPW_ADD_CMN(LOC(SPW_17),13623)
       CALL SPW_ADD_CMN(LOC(SPW_18),632)
       CALL SPW_ADD_CMN(LOC(SPW_19),7)
       CALL SPW_ADD_CMN(LOC(SPW_20),435)
       CALL SPW_ADD_CMN(LOC(SPW_21),16)
       CALL SPW_ADD_CMN(LOC(SPW_22),30)
       CALL SPW_ADD_CMN(LOC(SPW_23),4)
       CALL SPW_ADD_CMN(LOC(SPW_24),414)
       CALL SPW_ADD_CMN(LOC(SPW_25),19)
       CALL SPW_ADD_CMN(LOC(SPW_26),11)
       CALL SPW_ADD_CMN(LOC(SPW_27),15)
       CALL SPW_ADD_CMN(LOC(SPW_28),7)
       CALL SPW_ADD_CMN(LOC(SPW_29),4)
       CALL SPW_ADD_CMN(LOC(SPW_30),4)
       CALL SPW_ADD_CMN(LOC(SPW_31),3)
       CALL SPW_ADD_CMN(LOC(SPW_32),708)
       CALL SPW_ADD_CMN(LOC(SPW_33),18)
       CALL SPW_SET_NAMES(142846,
     +    'control_fda_.vpt',
     +    'control_fda_.D5C45C87.vpt')
       SPW_Table_Sub(1)%addr=LOC(FDA_GENERAL)
       SPW_Table_Sub(1)%allowed=LOC(SPW_0)+47458
       SPW_Table_Sub(1)%individualScale=LOC(SPW_0)+2552
       SPW_Table_Sub(2)%addr=LOC(FDA10)
       SPW_Table_Sub(2)%allowed=LOC(SPW_2)+10181
       SPW_Table_Sub(2)%individualScale=LOC(SPW_2)+784
       SPW_Table_Sub(3)%addr=LOC(FDA20)
       SPW_Table_Sub(3)%allowed=LOC(SPW_4)+13959
       SPW_Table_Sub(3)%individualScale=LOC(SPW_4)+616
       SPW_Table_Sub(4)%addr=LOC(FDA30)
       SPW_Table_Sub(4)%allowed=LOC(SPW_6)+23564
       SPW_Table_Sub(4)%individualScale=LOC(SPW_6)+1224
       SPW_Table_Sub(5)%addr=LOC(FDA40)
       SPW_Table_Sub(5)%allowed=LOC(SPW_8)+12684
       SPW_Table_Sub(5)%individualScale=LOC(SPW_8)+640
       SPW_Table_Sub(6)%addr=LOC(FDA50)
       SPW_Table_Sub(6)%allowed=LOC(SPW_10)+4067
       SPW_Table_Sub(6)%individualScale=LOC(SPW_10)+184
       SPW_Table_Sub(7)%addr=LOC(FDA60)
       SPW_Table_Sub(7)%allowed=LOC(SPW_12)+52754
       SPW_Table_Sub(7)%individualScale=LOC(SPW_12)+5432
       SPW_Table_Sub(8)%addr=LOC(FDA60B)
       SPW_Table_Sub(8)%allowed=LOC(SPW_14)+30057
       SPW_Table_Sub(8)%individualScale=LOC(SPW_14)+2256
       SPW_Table_Sub(9)%addr=LOC(FDA70)
       SPW_Table_Sub(9)%allowed=LOC(SPW_15)+3097
       SPW_Table_Sub(9)%individualScale=LOC(SPW_15)+336
       SPW_Table_Sub(10)%addr=LOC(FDA90)
       SPW_Table_Sub(10)%allowed=LOC(SPW_17)+13620
       SPW_Table_Sub(10)%individualScale=LOC(SPW_17)+488
       CALL SPW_SET_SUB_LST(10,SPW_Table_Sub,stepCounter)
       end

       subroutine spw_initDisp
       integer*4 dispStateHdr
       integer*4 stepCounter
       real*8    subInfo(40)
       common/spw_dispState/ dispStateHdr,stepCounter,subInfo
       stepCounter=0
       subInfo=0
       end

       subroutine FDA_GENERAL_ConInQ
       end

       subroutine FDA_GENERAL_ConIn
       LOGICAL*1 SPW_0(0:196607)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:49151)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:65535)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:16383)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:28671)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:7167)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_6(0:28671)
       Common/SPW_6/ SPW_6 !
       integer*4 SPW_6_4(0:7167)
       equivalence(SPW_6,SPW_6_4)
        Call FDA_GENERAL_ConInQ
!beg �����:20FDA20TRAN04_C6
        SPW_0(163122)=.false.	!L_(498) O
!end �����:20FDA20TRAN04_C6
!beg �����:20FDA20TRAN04_C7
        SPW_0(163119)=.false.	!L_(495) O
!end �����:20FDA20TRAN04_C7
!beg �������:20FDA91AB001
        SPW_0(162712)=SPW_0(154414)	!L_(88) O L_(323)
        SPW_0(162712)=SPW_0(162712).or.SPW_0(154649)	!L_(88) O L_(558)
        SPW_0(162712)=SPW_0(162712).or.SPW_0(154893)	!L_(88) O L_(802)
        SPW_0(162712)=SPW_0(162712).or.SPW_0(155023)	!L_(88) O L_(932)
        SPW_0(162712)=SPW_0(162712).or.SPW_0(155166)	!L_(88) O L_(1075)
        SPW_0(162712)=SPW_0(162712).or.SPW_0(155308)	!L_(88) O L_(1217)
!end �������:20FDA91AB001
!beg �������:20FDA91AB002
        SPW_0(162708)=SPW_0(155052)	!L_(84) O L_(961)
        SPW_0(162708)=SPW_0(162708).or.SPW_0(155260)	!L_(84) O L_(1169)
!end �������:20FDA91AB002
!beg �������:20FDA91AB003
        SPW_0(162704)=SPW_0(154691)	!L_(80) O L_(600)
        SPW_0(162704)=SPW_0(162704).or.SPW_0(154825)	!L_(80) O L_(734)
!end �������:20FDA91AB003
!beg �����:U
        SPW_0(162786)=.false.	!L_(162) O
!end �����:U
!beg �������:20FDA91AB004
        SPW_0(162706)=SPW_0(154702)	!L_(82) O L_(611)
        SPW_0(162706)=SPW_0(162706).or.SPW_0(154796)	!L_(82) O L_(705)
!end �������:20FDA91AB004
!beg �������:20FDA33AE002
        SPW_0(162719)=.false.	!L_(95) O
!end �������:20FDA33AE002
!beg �������:20FDA91AB005
        SPW_0(162710)=SPW_0(154446)	!L_(86) O L_(355)
        SPW_0(162710)=SPW_0(162710).or.SPW_0(155123)	!L_(86) O L_(1032)
!end �������:20FDA91AB005
!beg ������:U
        SPW_0(162785)=.false.	!L_(161) O
!end ������:U
!beg �����:20FDA20TRAN09_C3
        SPW_0(163041)=.false.	!L_(417) O
!end �����:20FDA20TRAN09_C3
!beg �����:20FDA20TRAN09_C4
        SPW_0(163038)=.false.	!L_(414) O
!end �����:20FDA20TRAN09_C4
!beg �����:20FDA20TRAN09_C5
        SPW_0(163035)=.false.	!L_(411) O
!end �����:20FDA20TRAN09_C5
!beg �����:20FDA20TRAN09_C6
        SPW_0(163032)=.false.	!L_(408) O
!end �����:20FDA20TRAN09_C6
!beg ����:20FDA66AE500
        SPW_0(162660)=.false.	!L_(36) O
!end ����:20FDA66AE500
!beg ����:20FDA20TRAN05_C3
        SPW_0(163099)=.false.	!L_(475) O
!end ����:20FDA20TRAN05_C3
!beg ����:20FDA20TRAN05_C4
        SPW_0(163096)=.false.	!L_(472) O
!end ����:20FDA20TRAN05_C4
!beg ����:20FDA20TRAN05_C5
        SPW_0(163093)=.false.	!L_(469) O
!end ����:20FDA20TRAN05_C5
!beg ����:20FDA20TRAN05_C6
        SPW_0(163090)=.false.	!L_(466) O
!end ����:20FDA20TRAN05_C6
!beg ����:20FDA20TRAN05_C7
        SPW_0(163075)=.false.	!L_(451) O
!end ����:20FDA20TRAN05_C7
!beg ����:20FDA20TRAN05_C8
        SPW_0(163072)=.false.	!L_(448) O
!end ����:20FDA20TRAN05_C8
!beg ������:20FDA64AE500
        SPW_0(162667)=SPW_0(173925)	!L_(43) O L_(1914)
        SPW_0(162667)=SPW_0(162667).or.SPW_0(173941)	!L_(43) O L_(1930)
        SPW_0(162667)=SPW_0(162667).or.SPW_0(173956)	!L_(43) O L_(1945)
        SPW_0(162667)=SPW_0(162667).or.SPW_0(173970)	!L_(43) O L_(1959)
        SPW_0(162667)=SPW_0(162667).or.SPW_0(173984)	!L_(43) O L_(1973)
!end ������:20FDA64AE500
!beg �����:20FDA60CONTS01VZ1
        SPW_0(163002)=.false.	!L_(378) O
!end �����:20FDA60CONTS01VZ1
!beg �����:20FDA60CONTS01VZ2
        SPW_0(162996)=.false.	!L_(372) O
!end �����:20FDA60CONTS01VZ2
!beg ����:20FDA63AE500
        SPW_0(162669)=.false.	!L_(45) O
!end ����:20FDA63AE500
!beg �������:20FDA66AB800
        SPW_0(162753)=SPW_0(172019)	!L_(129) O L_(8)
        SPW_0(162753)=SPW_0(162753).or.SPW_0(173740)	!L_(129) O L_(1729)
!end �������:20FDA66AB800
!beg �������:20FDA66AB801
        SPW_0(162751)=SPW_0(172019)	!L_(127) O L_(8)
        SPW_0(162751)=SPW_0(162751).or.SPW_0(173355)	!L_(127) O L_(1344)
!end �������:20FDA66AB801
!beg �������:20FDA33AE002
        SPW_0(162720)=.false.	!L_(96) O
!end �������:20FDA33AE002
!beg �����:20FDA64AE500
        SPW_0(162668)=SPW_0(173921)	!L_(44) O L_(1910)
        SPW_0(162668)=SPW_0(162668).or.SPW_0(173937)	!L_(44) O L_(1926)
        SPW_0(162668)=SPW_0(162668).or.SPW_0(173952)	!L_(44) O L_(1941)
        SPW_0(162668)=SPW_0(162668).or.SPW_0(173966)	!L_(44) O L_(1955)
        SPW_0(162668)=SPW_0(162668).or.SPW_0(173979)	!L_(44) O L_(1968)
!end �����:20FDA64AE500
!beg �������:20FDA66AB806
        SPW_0(162739)=SPW_0(172828)	!L_(115) O L_(817)
!end �������:20FDA66AB806
!beg ������:20FDA66AE401
        SPW_0(162679)=SPW_0(172823)	!L_(55) O L_(812)
        SPW_0(162679)=SPW_0(162679).or.SPW_0(172838)	!L_(55) O L_(827)
        SPW_0(162679)=SPW_0(162679).or.SPW_0(172847)	!L_(55) O L_(836)
        SPW_0(162679)=SPW_0(162679).or.SPW_0(172856)	!L_(55) O L_(845)
        SPW_0(162679)=SPW_0(162679).or.SPW_0(172871)	!L_(55) O L_(860)
!end ������:20FDA66AE401
!beg ������:20FDA66AE406
        SPW_0(162722)=SPW_0(173317)	!L_(98) O L_(1306)
        SPW_0(162722)=SPW_0(162722).or.SPW_0(173328)	!L_(98) O L_(1317)
        SPW_0(162722)=SPW_0(162722).or.SPW_0(173338)	!L_(98) O L_(1327)
        SPW_0(162722)=SPW_0(162722).or.SPW_0(173347)	!L_(98) O L_(1336)
        SPW_0(162722)=SPW_0(162722).or.SPW_0(173358)	!L_(98) O L_(1347)
!end ������:20FDA66AE406
!beg ����������:20FDA60AE403
        SPW_0(162631)=SPW_0(172720)	!L_(7) O L_(709)
!end ����������:20FDA60AE403
!beg ����:20FDA65AE401
        SPW_0(162681)=.false.	!L_(57) O
!end ����:20FDA65AE401
!beg ���������:20FDA60AE403
        SPW_0(162637)=SPW_0(173686)	!L_(13) O L_(1675)
!end ���������:20FDA60AE403
!beg ���������8:20FDA60AE403
        SPW_0(162630)=.false.	!L_(6) O
!end ���������8:20FDA60AE403
!beg �����:20FDA66AE401
        SPW_0(162680)=SPW_0(172834)	!L_(56) O L_(823)
        SPW_0(162680)=SPW_0(162680).or.SPW_0(172842)	!L_(56) O L_(831)
        SPW_0(162680)=SPW_0(162680).or.SPW_0(172851)	!L_(56) O L_(840)
        SPW_0(162680)=SPW_0(162680).or.SPW_0(172860)	!L_(56) O L_(849)
        SPW_0(162680)=SPW_0(162680).or.SPW_0(172866)	!L_(56) O L_(855)
!end �����:20FDA66AE401
!beg ����:20FDA65AE406
        SPW_0(162724)=.false.	!L_(100) O
!end ����:20FDA65AE406
!beg �����:20FDA66AE406
        SPW_0(162723)=SPW_0(173313)	!L_(99) O L_(1302)
        SPW_0(162723)=SPW_0(162723).or.SPW_0(173324)	!L_(99) O L_(1313)
        SPW_0(162723)=SPW_0(162723).or.SPW_0(173334)	!L_(99) O L_(1323)
        SPW_0(162723)=SPW_0(162723).or.SPW_0(173343)	!L_(99) O L_(1332)
        SPW_0(162723)=SPW_0(162723).or.SPW_0(173351)	!L_(99) O L_(1340)
!end �����:20FDA66AE406
!beg ������:20FDA61AE500
        SPW_0(162676)=SPW_0(174186)	!L_(52) O L_(2175)
        SPW_0(162676)=SPW_0(162676).or.SPW_0(174202)	!L_(52) O L_(2191)
        SPW_0(162676)=SPW_0(162676).or.SPW_0(174217)	!L_(52) O L_(2206)
        SPW_0(162676)=SPW_0(162676).or.SPW_0(174231)	!L_(52) O L_(2220)
        SPW_0(162676)=SPW_0(162676).or.SPW_0(174245)	!L_(52) O L_(2234)
!end ������:20FDA61AE500
!beg ����:20FDA60AE500
        SPW_0(162716)=.false.	!L_(92) O
!end ����:20FDA60AE500
!beg �������:20FDA63AB800
        SPW_0(162765)=SPW_0(172097)	!L_(141) O L_(86)
        SPW_0(162765)=SPW_0(162765).or.SPW_0(174001)	!L_(141) O L_(1990)
!end �������:20FDA63AB800
!beg ����:20FDA60AE501
        SPW_0(162713)=.false.	!L_(89) O
!end ����:20FDA60AE501
!beg ����:20FDA60AE502
        SPW_0(162781)=.false.	!L_(157) O
!end ����:20FDA60AE502
!beg �������:20FDA63AB801
        SPW_0(162763)=SPW_0(172097)	!L_(139) O L_(86)
        SPW_0(162763)=SPW_0(162763).or.SPW_0(173526)	!L_(139) O L_(1515)
!end �������:20FDA63AB801
!beg �����:20FDA61AE500
        SPW_0(162677)=SPW_0(174066)	!L_(53) O L_(2055)
        SPW_0(162677)=SPW_0(162677).or.SPW_0(174182)	!L_(53) O L_(2171)
        SPW_0(162677)=SPW_0(162677).or.SPW_0(174198)	!L_(53) O L_(2187)
        SPW_0(162677)=SPW_0(162677).or.SPW_0(174213)	!L_(53) O L_(2202)
        SPW_0(162677)=SPW_0(162677).or.SPW_0(174227)	!L_(53) O L_(2216)
        SPW_0(162677)=SPW_0(162677).or.SPW_0(174240)	!L_(53) O L_(2229)
!end �����:20FDA61AE500
!beg ����:20FDA60AE503
        SPW_0(162645)=.false.	!L_(21) O
!end ����:20FDA60AE503
!beg ����:20FDA60AE504
        SPW_0(162648)=.false.	!L_(24) O
!end ����:20FDA60AE504
!beg �����:20FDA20KANT01_C3
        SPW_0(163017)=.false.	!L_(393) O
!end �����:20FDA20KANT01_C3
!beg �����:20FDA20KANT01_C4
        SPW_0(163014)=.false.	!L_(390) O
!end �����:20FDA20KANT01_C4
!beg �������:20FDA63AB806
        SPW_0(162745)=SPW_0(173062)	!L_(121) O L_(1051)
!end �������:20FDA63AB806
!beg �����:20FDA20KANT01_C6
        SPW_0(163011)=.false.	!L_(387) O
!end �����:20FDA20KANT01_C6
!beg ����:20FDA60AE509
        SPW_0(162651)=.false.	!L_(27) O
!end ����:20FDA60AE509
!beg �����:20FDA20KANT01_C7
        SPW_0(163008)=.false.	!L_(384) O
!end �����:20FDA20KANT01_C7
!beg ������:20FDA20TRAN04_C3
        SPW_0(163130)=.false.	!L_(506) O
!end ������:20FDA20TRAN04_C3
!beg ������:20FDA20TRAN04_C4
        SPW_0(163127)=.false.	!L_(503) O
!end ������:20FDA20TRAN04_C4
!beg ������:20FDA20TRAN04_C5
        SPW_0(163124)=.false.	!L_(500) O
!end ������:20FDA20TRAN04_C5
!beg ������:20FDA20TRAN04_C6
        SPW_0(163121)=.false.	!L_(497) O
!end ������:20FDA20TRAN04_C6
!beg ������:20FDA20TRAN04_C7
        SPW_0(163118)=.false.	!L_(494) O
!end ������:20FDA20TRAN04_C7
!beg ������:20FDA63AE401
        SPW_0(162688)=SPW_0(173057)	!L_(64) O L_(1046)
        SPW_0(162688)=SPW_0(162688).or.SPW_0(173072)	!L_(64) O L_(1061)
        SPW_0(162688)=SPW_0(162688).or.SPW_0(173081)	!L_(64) O L_(1070)
        SPW_0(162688)=SPW_0(162688).or.SPW_0(173090)	!L_(64) O L_(1079)
        SPW_0(162688)=SPW_0(162688).or.SPW_0(173105)	!L_(64) O L_(1094)
!end ������:20FDA63AE401
!beg ����:20FDA60AE514
        SPW_0(162657)=.false.	!L_(33) O
!end ����:20FDA60AE514
!beg ������:20FDA63AE406
        SPW_0(162731)=SPW_0(173488)	!L_(107) O L_(1477)
        SPW_0(162731)=SPW_0(162731).or.SPW_0(173499)	!L_(107) O L_(1488)
        SPW_0(162731)=SPW_0(162731).or.SPW_0(173509)	!L_(107) O L_(1498)
        SPW_0(162731)=SPW_0(162731).or.SPW_0(173518)	!L_(107) O L_(1507)
        SPW_0(162731)=SPW_0(162731).or.SPW_0(173529)	!L_(107) O L_(1518)
!end ������:20FDA63AE406
!beg ����:20FDA60AE515
        SPW_0(162654)=.false.	!L_(30) O
!end ����:20FDA60AE515
!beg ����:20FDA62AE401
        SPW_0(162690)=.false.	!L_(66) O
!end ����:20FDA62AE401
!beg �����:20FDA63AE401
        SPW_0(162689)=SPW_0(173068)	!L_(65) O L_(1057)
        SPW_0(162689)=SPW_0(162689).or.SPW_0(173076)	!L_(65) O L_(1065)
        SPW_0(162689)=SPW_0(162689).or.SPW_0(173085)	!L_(65) O L_(1074)
        SPW_0(162689)=SPW_0(162689).or.SPW_0(173094)	!L_(65) O L_(1083)
        SPW_0(162689)=SPW_0(162689).or.SPW_0(173100)	!L_(65) O L_(1089)
!end �����:20FDA63AE401
!beg ����:20FDA62AE406
        SPW_0(162733)=.false.	!L_(109) O
!end ����:20FDA62AE406
!beg �����:20FDA63AE406
        SPW_0(162732)=SPW_0(173484)	!L_(108) O L_(1473)
        SPW_0(162732)=SPW_0(162732).or.SPW_0(173495)	!L_(108) O L_(1484)
        SPW_0(162732)=SPW_0(162732).or.SPW_0(173505)	!L_(108) O L_(1494)
        SPW_0(162732)=SPW_0(162732).or.SPW_0(173514)	!L_(108) O L_(1503)
        SPW_0(162732)=SPW_0(162732).or.SPW_0(173522)	!L_(108) O L_(1511)
!end �����:20FDA63AE406
!beg �����:20FDA20TRAN03_C3
        SPW_0(163116)=.false.	!L_(492) O
!end �����:20FDA20TRAN03_C3
!beg �����:20FDA20TRAN03_C4
        SPW_0(163113)=.false.	!L_(489) O
!end �����:20FDA20TRAN03_C4
!beg �����:20FDA20TRAN03_C5
        SPW_0(163110)=.false.	!L_(486) O
!end �����:20FDA20TRAN03_C5
!beg �����:20FDA20TRAN03_C6
        SPW_0(163107)=.false.	!L_(483) O
!end �����:20FDA20TRAN03_C6
!beg �����:20FDA20TRAN03_C7
        SPW_0(163104)=.false.	!L_(480) O
!end �����:20FDA20TRAN03_C7
!beg �������:20FDA60AB800
        SPW_0(162779)=SPW_0(173662)	!L_(155) O L_(1651)
!end �������:20FDA60AB800
!beg �������:20FDA60AB801
        SPW_0(162777)=SPW_0(173643)	!L_(153) O L_(1632)
!end �������:20FDA60AB801
!beg �������:20FDA60AB806
        SPW_0(162775)=SPW_0(172756)	!L_(151) O L_(745)
!end �������:20FDA60AB806
!beg �������:20FDA60AB807
        SPW_0(162773)=SPW_0(172723)	!L_(149) O L_(712)
!end �������:20FDA60AB807
!beg ������:20FDA60AE400
        SPW_0(162789)=.false.	!L_(165) O
!end ������:20FDA60AE400
!beg ������:20FDA60AE401
        SPW_0(162787)=.false.	!L_(163) O
!end ������:20FDA60AE401
!beg ������:20FDA60AE402
        SPW_0(162699)=SPW_0(172689)	!L_(75) O L_(678)
        SPW_0(162699)=SPW_0(162699).or.SPW_0(173672)	!L_(75) O L_(1661)
!end ������:20FDA60AE402
!beg ������:20FDA60AE403
        SPW_0(162627)=.false.	!L_(3) O
!end ������:20FDA60AE403
!beg �������:20FDA66AB800
        SPW_0(162754)=SPW_0(173736)	!L_(130) O L_(1725)
!end �������:20FDA66AB800
!beg �����:20FDA60AE400
        SPW_0(162790)=.false.	!L_(166) O
!end �����:20FDA60AE400
!beg �������:20FDA66AB801
        SPW_0(162752)=SPW_0(172781)	!L_(128) O L_(770)
        SPW_0(162752)=SPW_0(162752).or.SPW_0(173736)	!L_(128) O L_(1725)
!end �������:20FDA66AB801
!beg �����:20FDA60AE401
        SPW_0(162788)=.false.	!L_(164) O
!end �����:20FDA60AE401
!beg �����:20FDA60AE402
        SPW_0(162700)=SPW_0(172679)	!L_(76) O L_(668)
        SPW_0(162700)=SPW_0(162700).or.SPW_0(173669)	!L_(76) O L_(1658)
        SPW_0(162700)=SPW_0(162700).or.SPW_0(172701)	!L_(76) O L_(690)
!end �����:20FDA60AE402
!beg �����:20FDA60AE403
        SPW_0(162639)=.false.	!L_(15) O
!end �����:20FDA60AE403
!beg �������:20FDA66AB806
        SPW_0(162740)=SPW_0(172799)	!L_(116) O L_(788)
!end �������:20FDA66AB806
!beg ������:20FDA20TRAN09_C3
        SPW_0(163040)=.false.	!L_(416) O
!end ������:20FDA20TRAN09_C3
!beg ������:20FDA20TRAN09_C4
        SPW_0(163037)=.false.	!L_(413) O
!end ������:20FDA20TRAN09_C4
!beg ������:20FDA20TRAN09_C5
        SPW_0(163034)=.false.	!L_(410) O
!end ������:20FDA20TRAN09_C5
!beg ������:20FDA20TRAN09_C6
        SPW_0(163031)=.false.	!L_(407) O
!end ������:20FDA20TRAN09_C6
!beg �����:20FDA20TRAN08_C3
        SPW_0(163053)=.false.	!L_(429) O
!end �����:20FDA20TRAN08_C3
!beg �����:20FDA20TRAN08_C4
        SPW_0(163050)=.false.	!L_(426) O
!end �����:20FDA20TRAN08_C4
!beg �����:20FDA20TRAN08_C5
        SPW_0(163047)=.false.	!L_(423) O
!end �����:20FDA20TRAN08_C5
!beg �����:20FDA20TRAN08_C6
        SPW_0(163044)=.false.	!L_(420) O
!end �����:20FDA20TRAN08_C6
!beg �������:20FDA63AB800
        SPW_0(162766)=SPW_0(173997)	!L_(142) O L_(1986)
!end �������:20FDA63AB800
!beg �������:20FDA63AB801
        SPW_0(162764)=SPW_0(172790)	!L_(140) O L_(779)
        SPW_0(162764)=SPW_0(162764).or.SPW_0(173997)	!L_(140) O L_(1986)
!end �������:20FDA63AB801
!beg ����:20FDA60AE403
        SPW_0(162634)=SPW_0(173725)	!L_(10) O L_(1714)
!end ����:20FDA60AE403
!beg �������:20FDA63AB806
        SPW_0(162746)=SPW_0(173033)	!L_(122) O L_(1022)
!end �������:20FDA63AB806
!beg ����:20FDA20TRAN04_C3
        SPW_0(163129)=.false.	!L_(505) O
!end ����:20FDA20TRAN04_C3
!beg ����:20FDA20TRAN04_C4
        SPW_0(163126)=.false.	!L_(502) O
!end ����:20FDA20TRAN04_C4
!beg ����:20FDA20TRAN04_C5
        SPW_0(163123)=.false.	!L_(499) O
!end ����:20FDA20TRAN04_C5
!beg ����:20FDA20TRAN04_C6
        SPW_0(163120)=.false.	!L_(496) O
!end ����:20FDA20TRAN04_C6
!beg ����:20FDA20TRAN04_C7
        SPW_0(163117)=.false.	!L_(493) O
!end ����:20FDA20TRAN04_C7
!beg ������:20FDA21AE701
        SPW_0(163217)=.false.	!L_(593) O
!end ������:20FDA21AE701
!beg ���������:20FDA60AE402
        SPW_0(162696)=.false.	!L_(72) O
!end ���������:20FDA60AE402
!beg ������:20FDA21AE702
        SPW_0(163236)=.false.	!L_(612) O
!end ������:20FDA21AE702
!beg ���������:20FDA60AE403
        SPW_0(162625)=.false.	!L_(1) O
!end ���������:20FDA60AE403
!beg �������:20FDA60AB800
        SPW_0(162780)=SPW_0(173658)	!L_(156) O L_(1647)
!end �������:20FDA60AB800
!beg �����:20FDA21AE701
        SPW_0(163218)=.false.	!L_(594) O
!end �����:20FDA21AE701
!beg �������:20FDA60AB801
        SPW_0(162778)=SPW_0(173628)	!L_(154) O L_(1617)
!end �������:20FDA60AB801
!beg �����:20FDA21AE702
        SPW_0(163237)=.false.	!L_(613) O
!end �����:20FDA21AE702
!beg �������:20FDA60AB806
        SPW_0(162776)=SPW_0(172736)	!L_(152) O L_(725)
!end �������:20FDA60AB806
!beg ����:20FDA60AE201
        SPW_0(162640)=SPW_0(173669)	!L_(16) O L_(1658)
!end ����:20FDA60AE201
!beg �������:20FDA60AB807
        SPW_0(162774)=SPW_0(172706)	!L_(150) O L_(695)
!end �������:20FDA60AB807
!beg ����:20FDA60TELEZ
        SPW_0(163178)=.false.	!L_(554) O
!end ����:20FDA60TELEZ
!beg ������:20FDA20KANT01_C3
        SPW_0(163016)=.false.	!L_(392) O
!end ������:20FDA20KANT01_C3
!beg ������:20FDA20KANT01_C4
        SPW_0(163013)=.false.	!L_(389) O
!end ������:20FDA20KANT01_C4
!beg �������:20FDA60AE403
        SPW_0(162635)=SPW_0(173703)	!L_(11) O L_(1692)
!end �������:20FDA60AE403
!beg ������:20FDA20KANT01_C6
        SPW_0(163010)=.false.	!L_(386) O
!end ������:20FDA20KANT01_C6
!beg ������:20FDA20KANT01_C7
        SPW_0(163007)=.false.	!L_(383) O
!end ������:20FDA20KANT01_C7
!beg ����:20FDA20TRAN09_C3
        SPW_0(163039)=.false.	!L_(415) O
!end ����:20FDA20TRAN09_C3
!beg ����:20FDA20TRAN09_C4
        SPW_0(163036)=.false.	!L_(412) O
!end ����:20FDA20TRAN09_C4
!beg ����:20FDA20TRAN09_C5
        SPW_0(163033)=.false.	!L_(409) O
!end ����:20FDA20TRAN09_C5
!beg ����:20FDA20TRAN09_C6
        SPW_0(163030)=.false.	!L_(406) O
!end ����:20FDA20TRAN09_C6
!beg ���������9:20FDA60AE403
        SPW_0(162629)=.false.	!L_(5) O
!end ���������9:20FDA60AE403
!beg ������:20FDA20TRAN03_C3
        SPW_0(163115)=.false.	!L_(491) O
!end ������:20FDA20TRAN03_C3
!beg ������:20FDA20TRAN03_C4
        SPW_0(163112)=.false.	!L_(488) O
!end ������:20FDA20TRAN03_C4
!beg ������:20FDA20TRAN03_C5
        SPW_0(163109)=.false.	!L_(485) O
!end ������:20FDA20TRAN03_C5
!beg ������:20FDA20TRAN03_C6
        SPW_0(163106)=.false.	!L_(482) O
!end ������:20FDA20TRAN03_C6
!beg ������:20FDA20TRAN03_C7
        SPW_0(163103)=.false.	!L_(479) O
!end ������:20FDA20TRAN03_C7
!beg ������:20FDA60AE402
        SPW_0(162697)=SPW_0(155250)	!L_(73) O L_(1159)
!end ������:20FDA60AE402
!beg ������:20FDA60AE403
        SPW_0(162626)=.false.	!L_(2) O
!end ������:20FDA60AE403
!beg �����:20FDA20TRAN02_C3
        SPW_0(163146)=.false.	!L_(522) O
!end �����:20FDA20TRAN02_C3
!beg �����:20FDA20TRAN02_C4
        SPW_0(163143)=.false.	!L_(519) O
!end �����:20FDA20TRAN02_C4
!beg �����:20FDA20TRAN02_C5
        SPW_0(163140)=.false.	!L_(516) O
!end �����:20FDA20TRAN02_C5
!beg �����:20FDA20TRAN02_C6
        SPW_0(163137)=.false.	!L_(513) O
!end �����:20FDA20TRAN02_C6
!beg �����:20FDA20TRAN02_C7
        SPW_0(163134)=.false.	!L_(510) O
!end �����:20FDA20TRAN02_C7
!beg ������:20FDA66AE500
        SPW_0(162661)=SPW_0(173751)	!L_(37) O L_(1740)
        SPW_0(162661)=SPW_0(162661).or.SPW_0(173767)	!L_(37) O L_(1756)
        SPW_0(162661)=SPW_0(162661).or.SPW_0(173782)	!L_(37) O L_(1771)
        SPW_0(162661)=SPW_0(162661).or.SPW_0(173796)	!L_(37) O L_(1785)
        SPW_0(162661)=SPW_0(162661).or.SPW_0(173810)	!L_(37) O L_(1799)
!end ������:20FDA66AE500
!beg ������:20FDA20TRAN08_C3
        SPW_0(163052)=.false.	!L_(428) O
!end ������:20FDA20TRAN08_C3
!beg ������:20FDA20TRAN08_C4
        SPW_0(163049)=.false.	!L_(425) O
!end ������:20FDA20TRAN08_C4
!beg ������:20FDA20TRAN08_C5
        SPW_0(163046)=.false.	!L_(422) O
!end ������:20FDA20TRAN08_C5
!beg ������:20FDA20TRAN08_C6
        SPW_0(163043)=.false.	!L_(419) O
!end ������:20FDA20TRAN08_C6
!beg ����:20FDA65AE500
        SPW_0(162663)=.false.	!L_(39) O
!end ����:20FDA65AE500
!beg �����:20FDA66AE500
        SPW_0(162662)=SPW_0(173747)	!L_(38) O L_(1736)
        SPW_0(162662)=SPW_0(162662).or.SPW_0(173763)	!L_(38) O L_(1752)
        SPW_0(162662)=SPW_0(162662).or.SPW_0(173778)	!L_(38) O L_(1767)
        SPW_0(162662)=SPW_0(162662).or.SPW_0(173792)	!L_(38) O L_(1781)
        SPW_0(162662)=SPW_0(162662).or.SPW_0(173805)	!L_(38) O L_(1794)
!end �����:20FDA66AE500
!beg �����:20FDA20TRAN07_C3
        SPW_0(163065)=.false.	!L_(441) O
!end �����:20FDA20TRAN07_C3
!beg �����:20FDA20TRAN07_C4
        SPW_0(163062)=.false.	!L_(438) O
!end �����:20FDA20TRAN07_C4
!beg �����:20FDA20TRAN07_C5
        SPW_0(163059)=.false.	!L_(435) O
!end �����:20FDA20TRAN07_C5
!beg �����:20FDA20TRAN07_C6
        SPW_0(163056)=.false.	!L_(432) O
!end �����:20FDA20TRAN07_C6
!beg ����:20FDA20KANT01_C3
        SPW_0(163015)=.false.	!L_(391) O
!end ����:20FDA20KANT01_C3
!beg ����:20FDA20KANT01_C4
        SPW_0(163012)=.false.	!L_(388) O
!end ����:20FDA20KANT01_C4
!beg ����:20FDA20KANT01_C6
        SPW_0(163009)=.false.	!L_(385) O
!end ����:20FDA20KANT01_C6
!beg ����:20FDA20KANT01_C10
        SPW_0(163003)=.false.	!L_(379) O
!end ����:20FDA20KANT01_C10
!beg ����:20FDA20KANT01_C7
        SPW_0(163006)=.false.	!L_(382) O
!end ����:20FDA20KANT01_C7
!beg ������:20FDA63AE500
        SPW_0(162670)=SPW_0(174012)	!L_(46) O L_(2001)
        SPW_0(162670)=SPW_0(162670).or.SPW_0(174028)	!L_(46) O L_(2017)
        SPW_0(162670)=SPW_0(162670).or.SPW_0(174043)	!L_(46) O L_(2032)
        SPW_0(162670)=SPW_0(162670).or.SPW_0(174057)	!L_(46) O L_(2046)
        SPW_0(162670)=SPW_0(162670).or.SPW_0(174071)	!L_(46) O L_(2060)
!end ������:20FDA63AE500
!beg ����:20FDA62AE500
        SPW_0(162672)=.false.	!L_(48) O
!end ����:20FDA62AE500
!beg �������:20FDA65AB800
        SPW_0(162757)=SPW_0(172045)	!L_(133) O L_(34)
        SPW_0(162757)=SPW_0(162757).or.SPW_0(173827)	!L_(133) O L_(1816)
!end �������:20FDA65AB800
!beg �������:20FDA65AB801
        SPW_0(162755)=SPW_0(172045)	!L_(131) O L_(34)
        SPW_0(162755)=SPW_0(162755).or.SPW_0(173412)	!L_(131) O L_(1401)
!end �������:20FDA65AB801
!beg �����:20FDA63AE500
        SPW_0(162671)=SPW_0(174008)	!L_(47) O L_(1997)
        SPW_0(162671)=SPW_0(162671).or.SPW_0(174024)	!L_(47) O L_(2013)
        SPW_0(162671)=SPW_0(162671).or.SPW_0(174039)	!L_(47) O L_(2028)
        SPW_0(162671)=SPW_0(162671).or.SPW_0(174053)	!L_(47) O L_(2042)
!end �����:20FDA63AE500
!beg �������:20FDA65AB806
        SPW_0(162741)=SPW_0(172906)	!L_(117) O L_(895)
!end �������:20FDA65AB806
!beg ����:20FDA20TRAN03_C3
        SPW_0(163114)=.false.	!L_(490) O
!end ����:20FDA20TRAN03_C3
!beg ����:20FDA20TRAN03_C4
        SPW_0(163111)=.false.	!L_(487) O
!end ����:20FDA20TRAN03_C4
!beg ����:20FDA20TRAN03_C5
        SPW_0(163108)=.false.	!L_(484) O
!end ����:20FDA20TRAN03_C5
!beg ����:20FDA20TRAN03_C6
        SPW_0(163105)=.false.	!L_(481) O
!end ����:20FDA20TRAN03_C6
!beg ����:20FDA20TRAN03_C7
        SPW_0(163102)=.false.	!L_(478) O
!end ����:20FDA20TRAN03_C7
!beg ������:20FDA65AE401
        SPW_0(162682)=SPW_0(172901)	!L_(58) O L_(890)
        SPW_0(162682)=SPW_0(162682).or.SPW_0(172916)	!L_(58) O L_(905)
        SPW_0(162682)=SPW_0(162682).or.SPW_0(172925)	!L_(58) O L_(914)
        SPW_0(162682)=SPW_0(162682).or.SPW_0(172934)	!L_(58) O L_(923)
        SPW_0(162682)=SPW_0(162682).or.SPW_0(172949)	!L_(58) O L_(938)
!end ������:20FDA65AE401
!beg ������:20FDA20KANT01_C10
        SPW_0(163004)=.false.	!L_(380) O
!end ������:20FDA20KANT01_C10
!beg ������:20FDA65AE406
        SPW_0(162725)=SPW_0(173374)	!L_(101) O L_(1363)
        SPW_0(162725)=SPW_0(162725).or.SPW_0(173385)	!L_(101) O L_(1374)
        SPW_0(162725)=SPW_0(162725).or.SPW_0(173395)	!L_(101) O L_(1384)
        SPW_0(162725)=SPW_0(162725).or.SPW_0(173404)	!L_(101) O L_(1393)
        SPW_0(162725)=SPW_0(162725).or.SPW_0(173415)	!L_(101) O L_(1404)
!end ������:20FDA65AE406
!beg ����:20FDA64AE401
        SPW_0(162684)=.false.	!L_(60) O
!end ����:20FDA64AE401
!beg �����:20FDA65AE401
        SPW_0(162683)=SPW_0(172912)	!L_(59) O L_(901)
        SPW_0(162683)=SPW_0(162683).or.SPW_0(172920)	!L_(59) O L_(909)
        SPW_0(162683)=SPW_0(162683).or.SPW_0(172929)	!L_(59) O L_(918)
        SPW_0(162683)=SPW_0(162683).or.SPW_0(172938)	!L_(59) O L_(927)
        SPW_0(162683)=SPW_0(162683).or.SPW_0(172944)	!L_(59) O L_(933)
!end �����:20FDA65AE401
!beg ����:20FDA64AE406
        SPW_0(162727)=.false.	!L_(103) O
!end ����:20FDA64AE406
!beg �����:20FDA65AE406
        SPW_0(162726)=SPW_0(173370)	!L_(102) O L_(1359)
        SPW_0(162726)=SPW_0(162726).or.SPW_0(173381)	!L_(102) O L_(1370)
        SPW_0(162726)=SPW_0(162726).or.SPW_0(173391)	!L_(102) O L_(1380)
        SPW_0(162726)=SPW_0(162726).or.SPW_0(173400)	!L_(102) O L_(1389)
        SPW_0(162726)=SPW_0(162726).or.SPW_0(173408)	!L_(102) O L_(1397)
!end �����:20FDA65AE406
!beg ������:20FDA60AE500
        SPW_0(162717)=SPW_0(172696)	!L_(93) O L_(685)
        SPW_0(162717)=SPW_0(162717).or.SPW_0(173712)	!L_(93) O L_(1701)
        SPW_0(162717)=SPW_0(162717).or.SPW_0(173694)	!L_(93) O L_(1683)
!end ������:20FDA60AE500
!beg ������:20FDA60AE501
        SPW_0(162714)=.false.	!L_(90) O
!end ������:20FDA60AE501
!beg ������:20FDA60AE502
        SPW_0(162782)=SPW_0(173699)	!L_(158) O L_(1688)
!end ������:20FDA60AE502
!beg ������:20FDA60AE503
        SPW_0(162646)=.false.	!L_(22) O
!end ������:20FDA60AE503
!beg ������:20FDA60AE504
        SPW_0(162649)=SPW_0(173680)	!L_(25) O L_(1669)
!end ������:20FDA60AE504
!beg �������:20FDA62AB800
        SPW_0(162767)=SPW_0(172123)	!L_(143) O L_(112)
        SPW_0(162767)=SPW_0(162767).or.SPW_0(174088)	!L_(143) O L_(2077)
!end �������:20FDA62AB800
!beg ������:20FDA60AE509
        SPW_0(162652)=SPW_0(173649)	!L_(28) O L_(1638)
!end ������:20FDA60AE509
!beg �������:20FDA62AB801
        SPW_0(162643)=SPW_0(172123)	!L_(19) O L_(112)
        SPW_0(162643)=SPW_0(162643).or.SPW_0(173298)	!L_(19) O L_(1287)
!end �������:20FDA62AB801
!beg �����:20FDA60AE500
        SPW_0(162718)=SPW_0(172679)	!L_(94) O L_(668)
        SPW_0(162718)=SPW_0(162718).or.SPW_0(173715)	!L_(94) O L_(1704)
        SPW_0(162718)=SPW_0(162718).or.SPW_0(173708)	!L_(94) O L_(1697)
!end �����:20FDA60AE500
!beg �����:20FDA60AE501
        SPW_0(162715)=.false.	!L_(91) O
!end �����:20FDA60AE501
!beg �����:20FDA60AE502
        SPW_0(162783)=SPW_0(173694)	!L_(159) O L_(1683)
!end �����:20FDA60AE502
!beg �����:20FDA60AE503
        SPW_0(162647)=.false.	!L_(23) O
!end �����:20FDA60AE503
!beg �������:20FDA62AB806
        SPW_0(162747)=SPW_0(173140)	!L_(123) O L_(1129)
!end �������:20FDA62AB806
!beg �����:20FDA60AE504
        SPW_0(162650)=SPW_0(173675)	!L_(26) O L_(1664)
!end �����:20FDA60AE504
!beg �����:20FDA60AE509
        SPW_0(162653)=SPW_0(173635)	!L_(29) O L_(1624)
!end �����:20FDA60AE509
!beg ������:20FDA60AE514
        SPW_0(162658)=SPW_0(172744)	!L_(34) O L_(733)
!end ������:20FDA60AE514
!beg ������:20FDA60AE515
        SPW_0(162655)=SPW_0(172716)	!L_(31) O L_(705)
!end ������:20FDA60AE515
!beg ������:20FDA62AE401
        SPW_0(162691)=SPW_0(173135)	!L_(67) O L_(1124)
        SPW_0(162691)=SPW_0(162691).or.SPW_0(173150)	!L_(67) O L_(1139)
        SPW_0(162691)=SPW_0(162691).or.SPW_0(173159)	!L_(67) O L_(1148)
        SPW_0(162691)=SPW_0(162691).or.SPW_0(173168)	!L_(67) O L_(1157)
        SPW_0(162691)=SPW_0(162691).or.SPW_0(173183)	!L_(67) O L_(1172)
!end ������:20FDA62AE401
!beg ������:20FDA62AE406
        SPW_0(162734)=SPW_0(173260)	!L_(110) O L_(1249)
        SPW_0(162734)=SPW_0(162734).or.SPW_0(173271)	!L_(110) O L_(1260)
        SPW_0(162734)=SPW_0(162734).or.SPW_0(173281)	!L_(110) O L_(1270)
        SPW_0(162734)=SPW_0(162734).or.SPW_0(173290)	!L_(110) O L_(1279)
        SPW_0(162734)=SPW_0(162734).or.SPW_0(173301)	!L_(110) O L_(1290)
!end ������:20FDA62AE406
!beg �����:20FDA60AE514
        SPW_0(162659)=SPW_0(172740)	!L_(35) O L_(729)
!end �����:20FDA60AE514
!beg ����:20FDA61AE401
        SPW_0(162693)=.false.	!L_(69) O
!end ����:20FDA61AE401
!beg �����:20FDA60AE515
        SPW_0(162656)=SPW_0(172710)	!L_(32) O L_(699)
!end �����:20FDA60AE515
!beg �����:20FDA62AE401
        SPW_0(162692)=SPW_0(173146)	!L_(68) O L_(1135)
        SPW_0(162692)=SPW_0(162692).or.SPW_0(173154)	!L_(68) O L_(1143)
        SPW_0(162692)=SPW_0(162692).or.SPW_0(173163)	!L_(68) O L_(1152)
        SPW_0(162692)=SPW_0(162692).or.SPW_0(173172)	!L_(68) O L_(1161)
        SPW_0(162692)=SPW_0(162692).or.SPW_0(173178)	!L_(68) O L_(1167)
!end �����:20FDA62AE401
!beg ����:20FDA20TRAN08_C3
        SPW_0(163051)=.false.	!L_(427) O
!end ����:20FDA20TRAN08_C3
!beg ����:20FDA61AE406
        SPW_0(162736)=.false.	!L_(112) O
!end ����:20FDA61AE406
!beg ����:20FDA20TRAN08_C4
        SPW_0(163048)=.false.	!L_(424) O
!end ����:20FDA20TRAN08_C4
!beg ����:20FDA20TRAN08_C5
        SPW_0(163045)=.false.	!L_(421) O
!end ����:20FDA20TRAN08_C5
!beg ����:20FDA20TRAN08_C6
        SPW_0(163042)=.false.	!L_(418) O
!end ����:20FDA20TRAN08_C6
!beg �����:20FDA62AE406
        SPW_0(162735)=SPW_0(173256)	!L_(111) O L_(1245)
        SPW_0(162735)=SPW_0(162735).or.SPW_0(173267)	!L_(111) O L_(1256)
        SPW_0(162735)=SPW_0(162735).or.SPW_0(173277)	!L_(111) O L_(1266)
        SPW_0(162735)=SPW_0(162735).or.SPW_0(173286)	!L_(111) O L_(1275)
        SPW_0(162735)=SPW_0(162735).or.SPW_0(173294)	!L_(111) O L_(1283)
!end �����:20FDA62AE406
!beg ����:20FDA21AB002
        SPW_0(163197)=.false.	!L_(573) O
!end ����:20FDA21AB002
!beg ������:20FDA20TRAN02_C3
        SPW_0(163145)=.false.	!L_(521) O
!end ������:20FDA20TRAN02_C3
!beg ������:20FDA20TRAN02_C4
        SPW_0(163142)=.false.	!L_(518) O
!end ������:20FDA20TRAN02_C4
!beg ������:20FDA20TRAN02_C5
        SPW_0(163139)=.false.	!L_(515) O
!end ������:20FDA20TRAN02_C5
!beg ������:20FDA20TRAN02_C6
        SPW_0(163136)=.false.	!L_(512) O
!end ������:20FDA20TRAN02_C6
!beg ������:20FDA20TRAN02_C7
        SPW_0(163133)=.false.	!L_(509) O
!end ������:20FDA20TRAN02_C7
!beg �����:20FDA20TRAN10_C3
        SPW_0(163029)=.false.	!L_(405) O
!end �����:20FDA20TRAN10_C3
!beg �����:20FDA20TRAN10_C4
        SPW_0(163026)=.false.	!L_(402) O
!end �����:20FDA20TRAN10_C4
!beg �����:20FDA20TRAN10_C5
        SPW_0(163023)=.false.	!L_(399) O
!end �����:20FDA20TRAN10_C5
!beg �����:20FDA20TRAN10_C6
        SPW_0(163020)=.false.	!L_(396) O
!end �����:20FDA20TRAN10_C6
!beg �����:20FDA20TRAN01_C3
        SPW_0(163161)=.false.	!L_(537) O
!end �����:20FDA20TRAN01_C3
!beg �����:20FDA20TRAN01_C4
        SPW_0(163158)=.false.	!L_(534) O
!end �����:20FDA20TRAN01_C4
!beg �������:20FDA65AB800
        SPW_0(162758)=SPW_0(173823)	!L_(134) O L_(1812)
!end �������:20FDA65AB800
!beg �����:20FDA20TRAN01_C5
        SPW_0(163155)=.false.	!L_(531) O
!end �����:20FDA20TRAN01_C5
!beg �������:20FDA65AB801
        SPW_0(162756)=SPW_0(172784)	!L_(132) O L_(773)
        SPW_0(162756)=SPW_0(162756).or.SPW_0(173823)	!L_(132) O L_(1812)
!end �������:20FDA65AB801
!beg �����:20FDA20TRAN01_C6
        SPW_0(163152)=.false.	!L_(528) O
!end �����:20FDA20TRAN01_C6
!beg ������:20FDA60AE403
        SPW_0(162633)=.false.	!L_(9) O
!end ������:20FDA60AE403
!beg �����:20FDA20TRAN01_C7
        SPW_0(163149)=.false.	!L_(525) O
!end �����:20FDA20TRAN01_C7
!beg �������:20FDA65AB806
        SPW_0(162742)=SPW_0(172877)	!L_(118) O L_(866)
!end �������:20FDA65AB806
!beg ������:20FDA21AB001VS
        SPW_0(162965)=.false.	!L_(341) O
!end ������:20FDA21AB001VS
!beg ������:20FDA21AB001VZ
        SPW_0(162968)=.false.	!L_(344) O
!end ������:20FDA21AB001VZ
!beg ����:20FDA60AE403
        SPW_0(162632)=SPW_0(172701)	!L_(8) O L_(690)
        SPW_0(162632)=SPW_0(162632).or.SPW_0(173691)	!L_(8) O L_(1680)
!end ����:20FDA60AE403
!beg ������:20FDA20TRAN07_C3
        SPW_0(163064)=.false.	!L_(440) O
!end ������:20FDA20TRAN07_C3
!beg ������:20FDA20TRAN07_C4
        SPW_0(163061)=.false.	!L_(437) O
!end ������:20FDA20TRAN07_C4
!beg ������:20FDA20TRAN07_C5
        SPW_0(163058)=.false.	!L_(434) O
!end ������:20FDA20TRAN07_C5
!beg ������:20FDA20TRAN07_C6
        SPW_0(163055)=.false.	!L_(431) O
!end ������:20FDA20TRAN07_C6
!beg �������:20FDA62AB800
        SPW_0(162768)=SPW_0(174084)	!L_(144) O L_(2073)
!end �������:20FDA62AB800
!beg �������:20FDA62AB801
        SPW_0(162644)=SPW_0(172793)	!L_(20) O L_(782)
        SPW_0(162644)=SPW_0(162644).or.SPW_0(174084)	!L_(20) O L_(2073)
!end �������:20FDA62AB801
!beg �������:20FDA62AB806
        SPW_0(162748)=SPW_0(173111)	!L_(124) O L_(1100)
!end �������:20FDA62AB806
!beg �����:20FDA20TRAN06_C3
        SPW_0(163089)=.false.	!L_(465) O
!end �����:20FDA20TRAN06_C3
!beg �����:20FDA20TRAN06_C4
        SPW_0(163086)=.false.	!L_(462) O
!end �����:20FDA20TRAN06_C4
!beg �����:20FDA20TRAN06_C5
        SPW_0(163083)=.false.	!L_(459) O
!end �����:20FDA20TRAN06_C5
!beg �����:20FDA20TRAN06_C6
        SPW_0(163080)=.false.	!L_(456) O
!end �����:20FDA20TRAN06_C6
!beg �����:20FDA20TRAN06_C7
        SPW_0(163071)=.false.	!L_(447) O
!end �����:20FDA20TRAN06_C7
!beg �����:20FDA20TRAN06_C8
        SPW_0(163068)=.false.	!L_(444) O
!end �����:20FDA20TRAN06_C8
!beg ���������10:20FDA60AE403
        SPW_0(162638)=.false.	!L_(14) O
!end ���������10:20FDA60AE403
!beg ������:20FDA60AE201
        SPW_0(162641)=SPW_0(172685)	!L_(17) O L_(674)
        SPW_0(162641)=SPW_0(162641).or.SPW_0(173729)	!L_(17) O L_(1718)
!end ������:20FDA60AE201
!beg ����:20FDA21AB001VS
        SPW_0(162964)=.false.	!L_(340) O
!end ����:20FDA21AB001VS
!beg ������:20FDA60TELEZ
        SPW_0(163179)=.false.	!L_(555) O
!end ������:20FDA60TELEZ
!beg �����:20FDA60AE201
        SPW_0(162642)=SPW_0(172674)	!L_(18) O L_(663)
        SPW_0(162642)=SPW_0(162642).or.SPW_0(172701)	!L_(18) O L_(690)
        SPW_0(162642)=SPW_0(162642).or.SPW_0(172750)	!L_(18) O L_(739)
        SPW_0(162642)=SPW_0(162642).or.SPW_0(173666)	!L_(18) O L_(1655)
!end �����:20FDA60AE201
!beg ����:20FDA21AB001VZ
        SPW_0(162967)=.false.	!L_(343) O
!end ����:20FDA21AB001VZ
!beg ����:20FDA20TRAN02_C3
        SPW_0(163144)=.false.	!L_(520) O
!end ����:20FDA20TRAN02_C3
!beg ����:20FDA20TRAN02_C4
        SPW_0(163141)=.false.	!L_(517) O
!end ����:20FDA20TRAN02_C4
!beg ����:20FDA20TRAN02_C5
        SPW_0(163138)=.false.	!L_(514) O
!end ����:20FDA20TRAN02_C5
!beg ����:20FDA20TRAN02_C6
        SPW_0(163135)=.false.	!L_(511) O
!end ����:20FDA20TRAN02_C6
!beg ����:20FDA20TRAN02_C7
        SPW_0(163132)=.false.	!L_(508) O
!end ����:20FDA20TRAN02_C7
!beg �����:20FDA60TELEZ
        SPW_0(163180)=.false.	!L_(556) O
!end �����:20FDA60TELEZ
!beg �����:20FDA20KANT01_C10
        SPW_0(163005)=.false.	!L_(381) O
!end �����:20FDA20KANT01_C10
!beg �����:20FDA21AB001VS
        SPW_0(162966)=.false.	!L_(342) O
!end �����:20FDA21AB001VS
!beg �����:20FDA21AB001VZ
        SPW_0(162969)=.false.	!L_(345) O
!end �����:20FDA21AB001VZ
!beg ����:20FDA20TRAN07_C3
        SPW_0(163063)=.false.	!L_(439) O
!end ����:20FDA20TRAN07_C3
!beg ����:20FDA20TRAN07_C4
        SPW_0(163060)=.false.	!L_(436) O
!end ����:20FDA20TRAN07_C4
!beg ����:20FDA20TRAN07_C5
        SPW_0(163057)=.false.	!L_(433) O
!end ����:20FDA20TRAN07_C5
!beg ����:20FDA20TRAN07_C6
        SPW_0(163054)=.false.	!L_(430) O
!end ����:20FDA20TRAN07_C6
!beg ������:20FDA20TRAN10_C3
        SPW_0(163028)=.false.	!L_(404) O
!end ������:20FDA20TRAN10_C3
!beg ������:20FDA20TRAN10_C4
        SPW_0(163025)=.false.	!L_(401) O
!end ������:20FDA20TRAN10_C4
!beg ������:20FDA20TRAN10_C5
        SPW_0(163022)=.false.	!L_(398) O
!end ������:20FDA20TRAN10_C5
!beg ������:20FDA20TRAN10_C6
        SPW_0(163019)=.false.	!L_(395) O
!end ������:20FDA20TRAN10_C6
!beg ������:20FDA20TRAN01_C3
        SPW_0(163160)=.false.	!L_(536) O
!end ������:20FDA20TRAN01_C3
!beg ������:20FDA20TRAN01_C4
        SPW_0(163157)=.false.	!L_(533) O
!end ������:20FDA20TRAN01_C4
!beg ������:20FDA20TRAN01_C5
        SPW_0(163154)=.false.	!L_(530) O
!end ������:20FDA20TRAN01_C5
!beg ������:20FDA20TRAN01_C6
        SPW_0(163151)=.false.	!L_(527) O
!end ������:20FDA20TRAN01_C6
!beg ������:20FDA20TRAN01_C7
        SPW_0(163148)=.false.	!L_(524) O
!end ������:20FDA20TRAN01_C7
!beg ������:20FDA65AE500
        SPW_0(162664)=SPW_0(173838)	!L_(40) O L_(1827)
        SPW_0(162664)=SPW_0(162664).or.SPW_0(173854)	!L_(40) O L_(1843)
        SPW_0(162664)=SPW_0(162664).or.SPW_0(173869)	!L_(40) O L_(1858)
        SPW_0(162664)=SPW_0(162664).or.SPW_0(173883)	!L_(40) O L_(1872)
        SPW_0(162664)=SPW_0(162664).or.SPW_0(173897)	!L_(40) O L_(1886)
!end ������:20FDA65AE500
!beg ����:20FDA64AE500
        SPW_0(162666)=.false.	!L_(42) O
!end ����:20FDA64AE500
!beg �����:20FDA65AE500
        SPW_0(162665)=SPW_0(173834)	!L_(41) O L_(1823)
        SPW_0(162665)=SPW_0(162665).or.SPW_0(173850)	!L_(41) O L_(1839)
        SPW_0(162665)=SPW_0(162665).or.SPW_0(173865)	!L_(41) O L_(1854)
        SPW_0(162665)=SPW_0(162665).or.SPW_0(173879)	!L_(41) O L_(1868)
        SPW_0(162665)=SPW_0(162665).or.SPW_0(173892)	!L_(41) O L_(1881)
!end �����:20FDA65AE500
!beg ����:20FDA60CONTS01VS1
        SPW_0(162997)=.false.	!L_(373) O
!end ����:20FDA60CONTS01VS1
!beg ����:20FDA60CONTS01VS2
        SPW_0(162991)=.false.	!L_(367) O
!end ����:20FDA60CONTS01VS2
!beg ������:20FDA20TRAN06_C3
        SPW_0(163088)=.false.	!L_(464) O
!end ������:20FDA20TRAN06_C3
!beg ������:20FDA20TRAN06_C4
        SPW_0(163085)=.false.	!L_(461) O
!end ������:20FDA20TRAN06_C4
!beg ����:20FDA66AE401
        SPW_0(162678)=.false.	!L_(54) O
!end ����:20FDA66AE401
!beg ������:20FDA20TRAN06_C5
        SPW_0(163082)=.false.	!L_(458) O
!end ������:20FDA20TRAN06_C5
!beg ������:20FDA20TRAN06_C6
        SPW_0(163079)=.false.	!L_(455) O
!end ������:20FDA20TRAN06_C6
!beg ������:20FDA20TRAN06_C7
        SPW_0(163070)=.false.	!L_(446) O
!end ������:20FDA20TRAN06_C7
!beg ������:20FDA20TRAN06_C8
        SPW_0(163067)=.false.	!L_(443) O
!end ������:20FDA20TRAN06_C8
!beg ����:20FDA66AE406
        SPW_0(162721)=.false.	!L_(97) O
!end ����:20FDA66AE406
!beg ���������:20FDA60AE402
        SPW_0(162702)=.true.	!L_(78) A
!end ���������:20FDA60AE402
!beg ������:20FDA62AE500
        SPW_0(162673)=SPW_0(174099)	!L_(49) O L_(2088)
        SPW_0(162673)=SPW_0(162673).or.SPW_0(174115)	!L_(49) O L_(2104)
        SPW_0(162673)=SPW_0(162673).or.SPW_0(174130)	!L_(49) O L_(2119)
        SPW_0(162673)=SPW_0(162673).or.SPW_0(174144)	!L_(49) O L_(2133)
        SPW_0(162673)=SPW_0(162673).or.SPW_0(174158)	!L_(49) O L_(2147)
!end ������:20FDA62AE500
!beg ����:20FDA61AE500
        SPW_0(162675)=.false.	!L_(51) O
!end ����:20FDA61AE500
!beg �������:20FDA64AB800
        SPW_0(162761)=SPW_0(172071)	!L_(137) O L_(60)
        SPW_0(162761)=SPW_0(162761).or.SPW_0(173914)	!L_(137) O L_(1903)
!end �������:20FDA64AB800
!beg �������:20FDA64AB801
        SPW_0(162759)=SPW_0(172071)	!L_(135) O L_(60)
        SPW_0(162759)=SPW_0(162759).or.SPW_0(173469)	!L_(135) O L_(1458)
!end �������:20FDA64AB801
!beg ������:20FDA60CONTS01VS1
        SPW_0(162998)=.false.	!L_(374) O
!end ������:20FDA60CONTS01VS1
!beg �����:20FDA62AE500
        SPW_0(162674)=SPW_0(174095)	!L_(50) O L_(2084)
        SPW_0(162674)=SPW_0(162674).or.SPW_0(174111)	!L_(50) O L_(2100)
        SPW_0(162674)=SPW_0(162674).or.SPW_0(174126)	!L_(50) O L_(2115)
        SPW_0(162674)=SPW_0(162674).or.SPW_0(174140)	!L_(50) O L_(2129)
        SPW_0(162674)=SPW_0(162674).or.SPW_0(174153)	!L_(50) O L_(2142)
!end �����:20FDA62AE500
!beg ������:20FDA60CONTS01VS2
        SPW_0(162992)=.false.	!L_(368) O
!end ������:20FDA60CONTS01VS2
!beg �����:20FDA20TRAN05_C3
        SPW_0(163101)=.false.	!L_(477) O
!end �����:20FDA20TRAN05_C3
!beg �����:20FDA20TRAN05_C4
        SPW_0(163098)=.false.	!L_(474) O
!end �����:20FDA20TRAN05_C4
!beg �������:20FDA64AB806
        SPW_0(162743)=SPW_0(172984)	!L_(119) O L_(973)
!end �������:20FDA64AB806
!beg �����:20FDA20TRAN05_C5
        SPW_0(163095)=.false.	!L_(471) O
!end �����:20FDA20TRAN05_C5
!beg �����:20FDA20TRAN05_C6
        SPW_0(163092)=.false.	!L_(468) O
!end �����:20FDA20TRAN05_C6
!beg �����:20FDA20TRAN05_C7
        SPW_0(163077)=.false.	!L_(453) O
!end �����:20FDA20TRAN05_C7
!beg �����:20FDA20TRAN05_C8
        SPW_0(163074)=.false.	!L_(450) O
!end �����:20FDA20TRAN05_C8
!beg ������:20FDA64AE401
        SPW_0(162685)=SPW_0(172979)	!L_(61) O L_(968)
        SPW_0(162685)=SPW_0(162685).or.SPW_0(172994)	!L_(61) O L_(983)
        SPW_0(162685)=SPW_0(162685).or.SPW_0(173003)	!L_(61) O L_(992)
        SPW_0(162685)=SPW_0(162685).or.SPW_0(173012)	!L_(61) O L_(1001)
        SPW_0(162685)=SPW_0(162685).or.SPW_0(173027)	!L_(61) O L_(1016)
!end ������:20FDA64AE401
!beg ������:20FDA64AE406
        SPW_0(162728)=SPW_0(173431)	!L_(104) O L_(1420)
        SPW_0(162728)=SPW_0(162728).or.SPW_0(173442)	!L_(104) O L_(1431)
        SPW_0(162728)=SPW_0(162728).or.SPW_0(173452)	!L_(104) O L_(1441)
        SPW_0(162728)=SPW_0(162728).or.SPW_0(173461)	!L_(104) O L_(1450)
        SPW_0(162728)=SPW_0(162728).or.SPW_0(173472)	!L_(104) O L_(1461)
!end ������:20FDA64AE406
!beg ����:20FDA63AE401
        SPW_0(162687)=.false.	!L_(63) O
!end ����:20FDA63AE401
!beg �����:20FDA64AE401
        SPW_0(162686)=SPW_0(172990)	!L_(62) O L_(979)
        SPW_0(162686)=SPW_0(162686).or.SPW_0(172998)	!L_(62) O L_(987)
        SPW_0(162686)=SPW_0(162686).or.SPW_0(173007)	!L_(62) O L_(996)
        SPW_0(162686)=SPW_0(162686).or.SPW_0(173016)	!L_(62) O L_(1005)
        SPW_0(162686)=SPW_0(162686).or.SPW_0(173022)	!L_(62) O L_(1011)
!end �����:20FDA64AE401
!beg ����:20FDA63AE406
        SPW_0(162730)=.false.	!L_(106) O
!end ����:20FDA63AE406
!beg ������:20FDA21AE704M3
        SPW_0(163263)=.false.	!L_(639) O
!end ������:20FDA21AE704M3
!beg ����:20FDA20TRAN10_C3
        SPW_0(163027)=.false.	!L_(403) O
!end ����:20FDA20TRAN10_C3
!beg ������:20FDA21AE704M4
        SPW_0(163260)=.false.	!L_(636) O
!end ������:20FDA21AE704M4
!beg ����:20FDA20TRAN10_C4
        SPW_0(163024)=.false.	!L_(400) O
!end ����:20FDA20TRAN10_C4
!beg ������:20FDA21AE704M5
        SPW_0(163242)=.false.	!L_(618) O
!end ������:20FDA21AE704M5
!beg ����:20FDA20TRAN10_C5
        SPW_0(163021)=.false.	!L_(397) O
!end ����:20FDA20TRAN10_C5
!beg �����:20FDA64AE406
        SPW_0(162729)=SPW_0(173427)	!L_(105) O L_(1416)
        SPW_0(162729)=SPW_0(162729).or.SPW_0(173438)	!L_(105) O L_(1427)
        SPW_0(162729)=SPW_0(162729).or.SPW_0(173448)	!L_(105) O L_(1437)
        SPW_0(162729)=SPW_0(162729).or.SPW_0(173457)	!L_(105) O L_(1446)
        SPW_0(162729)=SPW_0(162729).or.SPW_0(173465)	!L_(105) O L_(1454)
!end �����:20FDA64AE406
!beg ������:20FDA21AE704M6
        SPW_0(163239)=.false.	!L_(615) O
!end ������:20FDA21AE704M6
!beg ����:20FDA20TRAN10_C6
        SPW_0(163018)=.false.	!L_(394) O
!end ����:20FDA20TRAN10_C6
!beg ������:20FDA21AE704M7
        SPW_0(163248)=.false.	!L_(624) O
!end ������:20FDA21AE704M7
!beg ����:U
        SPW_0(162784)=.false.	!L_(160) O
!end ����:U
!beg ������:20FDA21AE704M8
        SPW_0(163245)=.false.	!L_(621) O
!end ������:20FDA21AE704M8
!beg ����:20FDA20TRAN01_C3
        SPW_0(163159)=.false.	!L_(535) O
!end ����:20FDA20TRAN01_C3
!beg ����:20FDA20TRAN01_C4
        SPW_0(163156)=.false.	!L_(532) O
!end ����:20FDA20TRAN01_C4
!beg ����:20FDA20TRAN01_C5
        SPW_0(163153)=.false.	!L_(529) O
!end ����:20FDA20TRAN01_C5
!beg �������:20FDA61AB800
        SPW_0(162771)=SPW_0(172149)	!L_(147) O L_(138)
        SPW_0(162771)=SPW_0(162771).or.SPW_0(174175)	!L_(147) O L_(2164)
!end �������:20FDA61AB800
!beg ����:20FDA20TRAN01_C6
        SPW_0(163150)=.false.	!L_(526) O
!end ����:20FDA20TRAN01_C6
!beg �������:20FDA61AB801
        SPW_0(162769)=SPW_0(172149)	!L_(145) O L_(138)
        SPW_0(162769)=SPW_0(162769).or.SPW_0(173241)	!L_(145) O L_(1230)
!end �������:20FDA61AB801
!beg ����:20FDA20TRAN01_C7
        SPW_0(163147)=.false.	!L_(523) O
!end ����:20FDA20TRAN01_C7
!beg �������:20FDA61AB806
        SPW_0(162749)=SPW_0(173560)	!L_(125) O L_(1549)
!end �������:20FDA61AB806
!beg ������:20FDA61AE401
        SPW_0(162694)=SPW_0(173555)	!L_(70) O L_(1544)
        SPW_0(162694)=SPW_0(162694).or.SPW_0(173570)	!L_(70) O L_(1559)
        SPW_0(162694)=SPW_0(162694).or.SPW_0(173579)	!L_(70) O L_(1568)
        SPW_0(162694)=SPW_0(162694).or.SPW_0(173588)	!L_(70) O L_(1577)
        SPW_0(162694)=SPW_0(162694).or.SPW_0(173603)	!L_(70) O L_(1592)
!end ������:20FDA61AE401
!beg ������:20FDA61AE406
        SPW_0(162737)=SPW_0(173203)	!L_(113) O L_(1192)
        SPW_0(162737)=SPW_0(162737).or.SPW_0(173214)	!L_(113) O L_(1203)
        SPW_0(162737)=SPW_0(162737).or.SPW_0(173224)	!L_(113) O L_(1213)
        SPW_0(162737)=SPW_0(162737).or.SPW_0(173233)	!L_(113) O L_(1222)
        SPW_0(162737)=SPW_0(162737).or.SPW_0(173244)	!L_(113) O L_(1233)
!end ������:20FDA61AE406
!beg ����:20FDA60AE402
        SPW_0(162698)=.false.	!L_(74) O
!end ����:20FDA60AE402
!beg ������:20FDA21AB002
        SPW_0(163198)=.false.	!L_(574) O
!end ������:20FDA21AB002
!beg ����:20FDA60AE403
        SPW_0(162628)=.false.	!L_(4) O
!end ����:20FDA60AE403
!beg �����:20FDA61AE401
        SPW_0(162695)=SPW_0(173566)	!L_(71) O L_(1555)
        SPW_0(162695)=SPW_0(162695).or.SPW_0(173574)	!L_(71) O L_(1563)
        SPW_0(162695)=SPW_0(162695).or.SPW_0(173583)	!L_(71) O L_(1572)
        SPW_0(162695)=SPW_0(162695).or.SPW_0(173592)	!L_(71) O L_(1581)
        SPW_0(162695)=SPW_0(162695).or.SPW_0(173598)	!L_(71) O L_(1587)
!end �����:20FDA61AE401
!beg �����:20FDA61AE406
        SPW_0(162738)=SPW_0(173199)	!L_(114) O L_(1188)
        SPW_0(162738)=SPW_0(162738).or.SPW_0(173210)	!L_(114) O L_(1199)
        SPW_0(162738)=SPW_0(162738).or.SPW_0(173220)	!L_(114) O L_(1209)
        SPW_0(162738)=SPW_0(162738).or.SPW_0(173229)	!L_(114) O L_(1218)
        SPW_0(162738)=SPW_0(162738).or.SPW_0(173237)	!L_(114) O L_(1226)
!end �����:20FDA61AE406
!beg �����:20FDA21AB002
        SPW_0(163199)=.false.	!L_(575) O
!end �����:20FDA21AB002
!beg ����:20FDA60CONTS01VZ1
        SPW_0(163000)=.false.	!L_(376) O
!end ����:20FDA60CONTS01VZ1
!beg ����:20FDA60CONTS01VZ2
        SPW_0(162994)=.false.	!L_(370) O
!end ����:20FDA60CONTS01VZ2
!beg ����������:20FDA60AE403
        SPW_0(162636)=.false.	!L_(12) O
!end ����������:20FDA60AE403
!beg ����:20FDA21AE704M3
        SPW_0(163262)=.false.	!L_(638) O
!end ����:20FDA21AE704M3
!beg ����:20FDA21AE704M4
        SPW_0(163259)=.false.	!L_(635) O
!end ����:20FDA21AE704M4
!beg ����:20FDA21AE704M5
        SPW_0(163241)=.false.	!L_(617) O
!end ����:20FDA21AE704M5
!beg �������:20FDA91AB001
        SPW_0(162711)=SPW_0(154441)	!L_(87) O L_(350)
        SPW_0(162711)=SPW_0(162711).or.SPW_0(154673)	!L_(87) O L_(582)
        SPW_0(162711)=SPW_0(162711).or.SPW_0(154909)	!L_(87) O L_(818)
        SPW_0(162711)=SPW_0(162711).or.SPW_0(155047)	!L_(87) O L_(956)
        SPW_0(162711)=SPW_0(162711).or.SPW_0(155172)	!L_(87) O L_(1081)
        SPW_0(162711)=SPW_0(162711).or.SPW_0(155298)	!L_(87) O L_(1207)
!end �������:20FDA91AB001
!beg ����:20FDA21AE704M6
        SPW_0(163238)=.false.	!L_(614) O
!end ����:20FDA21AE704M6
!beg �������:20FDA91AB002
        SPW_0(162707)=SPW_0(155085)	!L_(83) O L_(994)
        SPW_0(162707)=SPW_0(162707).or.SPW_0(155313)	!L_(83) O L_(1222)
!end �������:20FDA91AB002
!beg ����:20FDA21AE704M7
        SPW_0(163247)=.false.	!L_(623) O
!end ����:20FDA21AE704M7
!beg �������:20FDA91AB003
        SPW_0(162703)=SPW_0(154720)	!L_(79) O L_(629)
        SPW_0(162703)=SPW_0(162703).or.SPW_0(154855)	!L_(79) O L_(764)
!end �������:20FDA91AB003
!beg ����:20FDA21AE704M8
        SPW_0(163244)=.false.	!L_(620) O
!end ����:20FDA21AE704M8
!beg �������:20FDA91AB004
        SPW_0(162705)=SPW_0(154580)	!L_(81) O L_(489)
        SPW_0(162705)=SPW_0(162705).or.SPW_0(154843)	!L_(81) O L_(752)
!end �������:20FDA91AB004
!beg ����:20FDA20TRAN06_C3
        SPW_0(163087)=.false.	!L_(463) O
!end ����:20FDA20TRAN06_C3
!beg �������:20FDA91AB005
        SPW_0(162709)=SPW_0(154473)	!L_(85) O L_(382)
        SPW_0(162709)=SPW_0(162709).or.SPW_0(155150)	!L_(85) O L_(1059)
!end �������:20FDA91AB005
!beg ����:20FDA20TRAN06_C4
        SPW_0(163084)=.false.	!L_(460) O
!end ����:20FDA20TRAN06_C4
!beg ����:20FDA20TRAN06_C5
        SPW_0(163081)=.false.	!L_(457) O
!end ����:20FDA20TRAN06_C5
!beg ����:20FDA20TRAN06_C6
        SPW_0(163078)=.false.	!L_(454) O
!end ����:20FDA20TRAN06_C6
!beg ����:20FDA20TRAN06_C7
        SPW_0(163069)=.false.	!L_(445) O
!end ����:20FDA20TRAN06_C7
!beg ����:20FDA20TRAN06_C8
        SPW_0(163066)=.false.	!L_(442) O
!end ����:20FDA20TRAN06_C8
!beg �������:20FDA64AB800
        SPW_0(162762)=SPW_0(173910)	!L_(138) O L_(1899)
!end �������:20FDA64AB800
!beg �������:20FDA64AB801
        SPW_0(162760)=SPW_0(172787)	!L_(136) O L_(776)
        SPW_0(162760)=SPW_0(162760).or.SPW_0(173910)	!L_(136) O L_(1899)
!end �������:20FDA64AB801
!beg �������:20FDA64AB806
        SPW_0(162744)=SPW_0(172955)	!L_(120) O L_(944)
!end �������:20FDA64AB806
!beg ������:20FDA60CONTS01VZ1
        SPW_0(163001)=.false.	!L_(377) O
!end ������:20FDA60CONTS01VZ1
!beg ������:20FDA60CONTS01VZ2
        SPW_0(162995)=.false.	!L_(371) O
!end ������:20FDA60CONTS01VZ2
!beg �����:20FDA21AE704M3
        SPW_0(163264)=.false.	!L_(640) O
!end �����:20FDA21AE704M3
!beg ����:20FDA21AE701
        SPW_0(163216)=.false.	!L_(592) O
!end ����:20FDA21AE701
!beg �����:20FDA21AE704M4
        SPW_0(163261)=.false.	!L_(637) O
!end �����:20FDA21AE704M4
!beg ����:20FDA21AE702
        SPW_0(163235)=.false.	!L_(611) O
!end ����:20FDA21AE702
!beg �����:20FDA21AE704M5
        SPW_0(163243)=.false.	!L_(619) O
!end �����:20FDA21AE704M5
!beg �������:20FDA61AB800
        SPW_0(162772)=SPW_0(174171)	!L_(148) O L_(2160)
!end �������:20FDA61AB800
!beg �����:20FDA21AE704M6
        SPW_0(163240)=.false.	!L_(616) O
!end �����:20FDA21AE704M6
!beg �������:20FDA61AB801
        SPW_0(162770)=SPW_0(172796)	!L_(146) O L_(785)
        SPW_0(162770)=SPW_0(162770).or.SPW_0(174171)	!L_(146) O L_(2160)
!end �������:20FDA61AB801
!beg �����:20FDA21AE704M7
        SPW_0(163249)=.false.	!L_(625) O
!end �����:20FDA21AE704M7
!beg �����:20FDA21AE704M8
        SPW_0(163246)=.false.	!L_(622) O
!end �����:20FDA21AE704M8
!beg �������:20FDA61AB806
        SPW_0(162750)=SPW_0(173189)	!L_(126) O L_(1178)
!end �������:20FDA61AB806
!beg ����������:20FDA60AE402
        SPW_0(162701)=.true.	!L_(77) A
!end ����������:20FDA60AE402
!beg ������:20FDA20TRAN05_C3
        SPW_0(163100)=.false.	!L_(476) O
!end ������:20FDA20TRAN05_C3
!beg ������:20FDA20TRAN05_C4
        SPW_0(163097)=.false.	!L_(473) O
!end ������:20FDA20TRAN05_C4
!beg ������:20FDA20TRAN05_C5
        SPW_0(163094)=.false.	!L_(470) O
!end ������:20FDA20TRAN05_C5
!beg ������:20FDA20TRAN05_C6
        SPW_0(163091)=.false.	!L_(467) O
!end ������:20FDA20TRAN05_C6
!beg ������:20FDA20TRAN05_C7
        SPW_0(163076)=.false.	!L_(452) O
!end ������:20FDA20TRAN05_C7
!beg ������:20FDA20TRAN05_C8
        SPW_0(163073)=.false.	!L_(449) O
!end ������:20FDA20TRAN05_C8
!beg �����:20FDA60CONTS01VS1
        SPW_0(162999)=.false.	!L_(375) O
!end �����:20FDA60CONTS01VS1
!beg �����:20FDA60CONTS01VS2
        SPW_0(162993)=.false.	!L_(369) O
!end �����:20FDA60CONTS01VS2
!beg �����:20FDA20TRAN04_C3
        SPW_0(163131)=.false.	!L_(507) O
!end �����:20FDA20TRAN04_C3
!beg �����:20FDA20TRAN04_C4
        SPW_0(163128)=.false.	!L_(504) O
!end �����:20FDA20TRAN04_C4
!beg �����:20FDA20TRAN04_C5
        SPW_0(163125)=.false.	!L_(501) O
!end �����:20FDA20TRAN04_C5
       end

       subroutine FDA10_ConInQ
       end

       subroutine FDA10_ConIn
       LOGICAL*1 SPW_0(0:196607)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:49151)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:65535)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:16383)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:28671)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:7167)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_6(0:28671)
       Common/SPW_6/ SPW_6 !
       integer*4 SPW_6_4(0:7167)
       equivalence(SPW_6,SPW_6_4)
        Call FDA10_ConInQ
!beg ���������:20FDA13CU001KN01
        SPW_2(15806)=.false.	!L_(15) O
!end ���������:20FDA13CU001KN01
!beg �������:20FDA13AA201
        SPW_2(15811)=.false.	!L_(20) O
!end �������:20FDA13AA201
!beg �������:20FDA13AA202
        SPW_2(15809)=.false.	!L_(18) O
!end �������:20FDA13AA202
!beg �������:20FDA13AA201
        SPW_2(15812)=.false.	!L_(21) O
!end �������:20FDA13AA201
!beg �������:20FDA13AA202
        SPW_2(15810)=.false.	!L_(19) O
!end �������:20FDA13AA202
!beg �������:20FDA13AA101
        SPW_2(15807)=.false.	!L_(16) O
!end �������:20FDA13AA101
!beg ��������:20FDA13CU001KN01
        SPW_2(15805)=.false.	!L_(14) O
!end ��������:20FDA13CU001KN01
!beg �������:20FDA13AA101
        SPW_2(15808)=.false.	!L_(17) O
!end �������:20FDA13AA101
       end

       subroutine FDA20_ConInQ
       end

       subroutine FDA20_ConIn
       LOGICAL*1 SPW_0(0:196607)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:49151)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:65535)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:16383)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:28671)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:7167)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_6(0:28671)
       Common/SPW_6/ SPW_6 !
       integer*4 SPW_6_4(0:7167)
       equivalence(SPW_6,SPW_6_4)
        Call FDA20_ConInQ
!beg �������:20FDA23BR006KA12
        SPW_2(18898)=.false.	!L_(546) O
!end �������:20FDA23BR006KA12
!beg ��������:20FDA22AX001KN01
        SPW_2(18886)=.false.	!L_(534) O
!end ��������:20FDA22AX001KN01
!beg �������:20FDA23BR006KA12
        SPW_2(18899)=.false.	!L_(547) O
!end �������:20FDA23BR006KA12
!beg �������:20FDA21BR109KA03
        SPW_2(18892)=.false.	!L_(540) O
!end �������:20FDA21BR109KA03
!beg ���������:20FDA22AX001KN01
        SPW_2(18887)=.false.	!L_(535) O
!end ���������:20FDA22AX001KN01
!beg �������:20FDA21BR109KA03
        SPW_2(18893)=.false.	!L_(541) O
!end �������:20FDA21BR109KA03
!beg �������:20FDA23BR005KA12
        SPW_2(18900)=.false.	!L_(548) O
!end �������:20FDA23BR005KA12
!beg �������:20FDA23BR005KA12
        SPW_2(18901)=.false.	!L_(549) O
!end �������:20FDA23BR005KA12
!beg �������:20FDA21BR108KA03
        SPW_2(18894)=.false.	!L_(542) O
!end �������:20FDA21BR108KA03
!beg ��������:20FDA21AX001KN01
        SPW_2(18888)=.false.	!L_(536) O
!end ��������:20FDA21AX001KN01
!beg �������:20FDA21BR108KA03
        SPW_2(18895)=.false.	!L_(543) O
!end �������:20FDA21BR108KA03
!beg �������:20FDA23BR004KA12
        SPW_2(18902)=.false.	!L_(550) O
!end �������:20FDA23BR004KA12
!beg �������:20FDA23BR004KA12
        SPW_2(18903)=.false.	!L_(551) O
!end �������:20FDA23BR004KA12
!beg ��������:20FDA23CU001KN01
        SPW_2(18906)=.false.	!L_(554) O
!end ��������:20FDA23CU001KN01
!beg ���������:20FDA21AX001KN01
        SPW_2(18889)=.false.	!L_(537) O
!end ���������:20FDA21AX001KN01
!beg �������:20FDA21BR107KA03
        SPW_2(18896)=.false.	!L_(544) O
!end �������:20FDA21BR107KA03
!beg �������:20FDA21BR110KA03
        SPW_2(18890)=.false.	!L_(538) O
!end �������:20FDA21BR110KA03
!beg �������:20FDA21BR107KA03
        SPW_2(18897)=.false.	!L_(545) O
!end �������:20FDA21BR107KA03
!beg �������:20FDA23BR003KA12
        SPW_2(18904)=.false.	!L_(552) O
!end �������:20FDA23BR003KA12
!beg �������:20FDA21BR110KA03
        SPW_2(18891)=.false.	!L_(539) O
!end �������:20FDA21BR110KA03
!beg ���������:20FDA23CU001KN01
        SPW_2(18907)=.false.	!L_(555) O
!end ���������:20FDA23CU001KN01
!beg �������:20FDA23BR003KA12
        SPW_2(18905)=.false.	!L_(553) O
!end �������:20FDA23BR003KA12
       end

       subroutine FDA30_ConInQ
       end

       subroutine FDA30_ConIn
       LOGICAL*1 SPW_0(0:196607)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:49151)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:65535)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:16383)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:28671)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:7167)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_6(0:28671)
       Common/SPW_6/ SPW_6 !
       integer*4 SPW_6_4(0:7167)
       equivalence(SPW_6,SPW_6_4)
        Call FDA30_ConInQ
!beg ��������:20FDA34CU001KN01
        SPW_6(25116)=.false.	!L_(176) O
!end ��������:20FDA34CU001KN01
!beg ���������:20FDA31CU001KN01
        SPW_6(25123)=.false.	!L_(183) O
!end ���������:20FDA31CU001KN01
!beg ���������:20FDA34CU001KN01
        SPW_6(25117)=.false.	!L_(177) O
!end ���������:20FDA34CU001KN01
!beg ��������:20FDA33CU001KN01
        SPW_6(25118)=.false.	!L_(178) O
!end ��������:20FDA33CU001KN01
!beg ���������:20FDA33CU001KN01
        SPW_6(25119)=.false.	!L_(179) O
!end ���������:20FDA33CU001KN01
!beg ��������:20FDA32CU001KN01
        SPW_6(25120)=.false.	!L_(180) O
!end ��������:20FDA32CU001KN01
!beg ��������:20FDA35CU001KN01
        SPW_6(25114)=.false.	!L_(174) O
!end ��������:20FDA35CU001KN01
!beg ���������:20FDA32CU001KN01
        SPW_6(25121)=.false.	!L_(181) O
!end ���������:20FDA32CU001KN01
!beg ���������:20FDA35CU001KN01
        SPW_6(25115)=.false.	!L_(175) O
!end ���������:20FDA35CU001KN01
!beg ��������:20FDA31CU001KN01
        SPW_6(25122)=.false.	!L_(182) O
!end ��������:20FDA31CU001KN01
       end

       subroutine FDA40_ConInQ
       end

       subroutine FDA40_ConIn
       LOGICAL*1 SPW_0(0:196607)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:49151)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:65535)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:16383)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:28671)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:7167)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_6(0:28671)
       Common/SPW_6/ SPW_6 !
       integer*4 SPW_6_4(0:7167)
       equivalence(SPW_6,SPW_6_4)
        Call FDA40_ConInQ
!beg ������:20FDA40_PUNCH
        SPW_0(156633)=.false.	!L_(716) O
!end ������:20FDA40_PUNCH
!beg ������:20FDA40_STICK_LOADER
        SPW_0(156626)=.false.	!L_(709) O
!end ������:20FDA40_STICK_LOADER
!beg ������:20FDA91AE018
        SPW_0(156572)=.false.	!L_(655) O
!end ������:20FDA91AE018
!beg �����:20FDA91AE018
        SPW_0(156584)=.false.	!L_(667) O
!end �����:20FDA91AE018
!beg ����������:20FDA40_FEEDER
        SPW_0(156607)=.true.	!L_(690) A
!end ����������:20FDA40_FEEDER
!beg ���������:20FDA91AE013
        SPW_0(156555)=.false.	!L_(638) O
!end ���������:20FDA91AE013
!beg ���������:20FDA91AE018
        SPW_0(156570)=.false.	!L_(653) O
!end ���������:20FDA91AE018
!beg ���������4:20FDA91AE013
        SPW_0(156564)=.false.	!L_(647) O
!end ���������4:20FDA91AE013
!beg ���������4:20FDA91AE018
        SPW_0(156579)=.false.	!L_(662) O
!end ���������4:20FDA91AE018
!beg ������:20FDA40_FEEDER
        SPW_0(156603)=.false.	!L_(686) O
!end ������:20FDA40_FEEDER
!beg �����:20FDA40_PUNCH
        SPW_0(156634)=.false.	!L_(717) O
!end �����:20FDA40_PUNCH
!beg ���������9:20FDA91AE013
        SPW_0(156559)=.false.	!L_(642) O
!end ���������9:20FDA91AE013
!beg ���������9:20FDA91AE018
        SPW_0(156574)=.false.	!L_(657) O
!end ���������9:20FDA91AE018
!beg ������:20FDA91AE013
        SPW_0(156556)=.false.	!L_(639) O
!end ������:20FDA91AE013
!beg ������:20FDA91AE018
        SPW_0(156571)=SPW_0(154543)	!L_(654) O L_(452)
!end ������:20FDA91AE018
!beg ����:20FDA40_PUNCH
        SPW_0(156632)=.false.	!L_(715) O
!end ����:20FDA40_PUNCH
!beg ���������:20FDA40_EJECTOR
        SPW_0(156601)=.true.	!L_(684) A
!end ���������:20FDA40_EJECTOR
!beg ���������:20FDA40_FEEDER
        SPW_0(156608)=.true.	!L_(691) A
!end ���������:20FDA40_FEEDER
!beg ������:20FDA40_MATRIX
        SPW_0(156619)=.false.	!L_(702) O
!end ������:20FDA40_MATRIX
!beg �����:20FDA40_STICK_LOADER
        SPW_0(156627)=.false.	!L_(710) O
!end �����:20FDA40_STICK_LOADER
!beg ���������:20FDA40_PUNCH
        SPW_0(156630)=.false.	!L_(713) O
!end ���������:20FDA40_PUNCH
!beg ����:20FDA40_MATRIX
        SPW_0(156618)=.false.	!L_(701) O
!end ����:20FDA40_MATRIX
!beg ���������5:20FDA91AE013
        SPW_0(156563)=.false.	!L_(646) O
!end ���������5:20FDA91AE013
!beg ���������5:20FDA91AE018
        SPW_0(156578)=.false.	!L_(661) O
!end ���������5:20FDA91AE018
!beg ���������:20FDA40_FEEDER
        SPW_0(156602)=.false.	!L_(685) O
!end ���������:20FDA40_FEEDER
!beg �����:20FDA40_MATRIX
        SPW_0(156620)=.false.	!L_(703) O
!end �����:20FDA40_MATRIX
!beg ���������:20FDA40_BOOT
        SPW_0(156615)=.true.	!L_(698) A
!end ���������:20FDA40_BOOT
!beg �����:20FDA91AE013
        SPW_0(156557)=.false.	!L_(640) O
!end �����:20FDA91AE013
!beg ����:20FDA91AE013
        SPW_0(156569)=.false.	!L_(652) O
!end ����:20FDA91AE013
!beg ����������:20FDA40_EJECTOR
        SPW_0(156600)=.true.	!L_(683) A
!end ����������:20FDA40_EJECTOR
!beg ���������10:20FDA91AE013
        SPW_0(156568)=.false.	!L_(651) O
!end ���������10:20FDA91AE013
!beg ���������10:20FDA91AE018
        SPW_0(156583)=.false.	!L_(666) O
!end ���������10:20FDA91AE018
!beg ������:20FDA40_PUNCH
        SPW_0(156631)=.false.	!L_(714) O
!end ������:20FDA40_PUNCH
!beg ���������:20FDA40_STICK_LOADER
        SPW_0(156629)=.true.	!L_(712) A
!end ���������:20FDA40_STICK_LOADER
!beg ����:20FDA40_BOOT
        SPW_0(156611)=.false.	!L_(694) O
!end ����:20FDA40_BOOT
!beg ���������1:20FDA91AE013
        SPW_0(156567)=.false.	!L_(650) O
!end ���������1:20FDA91AE013
!beg ���������1:20FDA91AE018
        SPW_0(156582)=.false.	!L_(665) O
!end ���������1:20FDA91AE018
!beg ������:20FDA40_STICK_LOADER
        SPW_0(156624)=.false.	!L_(707) O
!end ������:20FDA40_STICK_LOADER
!beg ���������6:20FDA91AE013
        SPW_0(156562)=.false.	!L_(645) O
!end ���������6:20FDA91AE013
!beg ���������6:20FDA91AE018
        SPW_0(156577)=.false.	!L_(660) O
!end ���������6:20FDA91AE018
!beg ����������:20FDA40_BOOT
        SPW_0(156614)=.true.	!L_(697) A
!end ����������:20FDA40_BOOT
!beg ���������:20FDA40_STICK_LOADER
        SPW_0(156623)=.false.	!L_(706) O
!end ���������:20FDA40_STICK_LOADER
!beg ����������:20FDA40_MATRIX
        SPW_0(156621)=.true.	!L_(704) A
!end ����������:20FDA40_MATRIX
!beg ������:20FDA40_EJECTOR
        SPW_0(156596)=.false.	!L_(679) O
!end ������:20FDA40_EJECTOR
!beg ��������:20FDA41CU001KN01
        SPW_0(156637)=.false.	!L_(720) O
!end ��������:20FDA41CU001KN01
!beg ������:20FDA40_FEEDER
        SPW_0(156605)=.false.	!L_(688) O
!end ������:20FDA40_FEEDER
!beg ����:20FDA91AE013
        SPW_0(156558)=.false.	!L_(641) O
!end ����:20FDA91AE013
!beg ����:20FDA91AE018
        SPW_0(156573)=.false.	!L_(656) O
!end ����:20FDA91AE018
!beg ���������2:20FDA91AE013
        SPW_0(156566)=.false.	!L_(649) O
!end ���������2:20FDA91AE013
!beg ���������2:20FDA91AE018
        SPW_0(156581)=.false.	!L_(664) O
!end ���������2:20FDA91AE018
!beg ������:20FDA40_MATRIX
        SPW_0(156617)=.false.	!L_(700) O
!end ������:20FDA40_MATRIX
!beg ���������7:20FDA91AE013
        SPW_0(156561)=.false.	!L_(644) O
!end ���������7:20FDA91AE013
!beg ����:20FDA40_FEEDER
        SPW_0(156604)=.false.	!L_(687) O
!end ����:20FDA40_FEEDER
!beg ���������7:20FDA91AE018
        SPW_0(156576)=.false.	!L_(659) O
!end ���������7:20FDA91AE018
!beg ����������:20FDA40_PUNCH
        SPW_0(156635)=.true.	!L_(718) A
!end ����������:20FDA40_PUNCH
!beg ����:20FDA40_STICK_LOADER
        SPW_0(156625)=.false.	!L_(708) O
!end ����:20FDA40_STICK_LOADER
!beg �����:20FDA40_FEEDER
        SPW_0(156606)=.false.	!L_(689) O
!end �����:20FDA40_FEEDER
!beg ����������:20FDA40_STICK_LOADER
        SPW_0(156628)=.true.	!L_(711) A
!end ����������:20FDA40_STICK_LOADER
!beg �����:20FDA40_EJECTOR
        SPW_0(156599)=.false.	!L_(682) O
!end �����:20FDA40_EJECTOR
!beg ������:20FDA40_BOOT
        SPW_0(156612)=.false.	!L_(695) O
!end ������:20FDA40_BOOT
!beg ���������:20FDA40_MATRIX
        SPW_0(156622)=.true.	!L_(705) A
!end ���������:20FDA40_MATRIX
!beg ���������:20FDA41CU001KN01
        SPW_0(156638)=.false.	!L_(721) O
!end ���������:20FDA41CU001KN01
!beg �����:20FDA40_BOOT
        SPW_0(156613)=.false.	!L_(696) O
!end �����:20FDA40_BOOT
!beg ���������:20FDA40_EJECTOR
        SPW_0(156595)=.false.	!L_(678) O
!end ���������:20FDA40_EJECTOR
!beg ������:20FDA40_EJECTOR
        SPW_0(156598)=.false.	!L_(681) O
!end ������:20FDA40_EJECTOR
!beg ���������:20FDA40_BOOT
        SPW_0(156609)=.false.	!L_(692) O
!end ���������:20FDA40_BOOT
!beg ���������:20FDA40_MATRIX
        SPW_0(156616)=.false.	!L_(699) O
!end ���������:20FDA40_MATRIX
!beg ���������3:20FDA91AE013
        SPW_0(156565)=.false.	!L_(648) O
!end ���������3:20FDA91AE013
!beg ���������3:20FDA91AE018
        SPW_0(156580)=.false.	!L_(663) O
!end ���������3:20FDA91AE018
!beg ����:20FDA40_EJECTOR
        SPW_0(156597)=.false.	!L_(680) O
!end ����:20FDA40_EJECTOR
!beg ���������:20FDA40_PUNCH
        SPW_0(156636)=.true.	!L_(719) A
!end ���������:20FDA40_PUNCH
!beg ���������8:20FDA91AE013
        SPW_0(156560)=.false.	!L_(643) O
!end ���������8:20FDA91AE013
!beg ������:20FDA40_BOOT
        SPW_0(156610)=.false.	!L_(693) O
!end ������:20FDA40_BOOT
!beg ���������8:20FDA91AE018
        SPW_0(156575)=.false.	!L_(658) O
!end ���������8:20FDA91AE018
       end

       subroutine FDA50_ConInQ
       end

       subroutine FDA50_ConIn
       LOGICAL*1 SPW_0(0:196607)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:49151)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:65535)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:16383)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:28671)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:7167)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_6(0:28671)
       Common/SPW_6/ SPW_6 !
       integer*4 SPW_6_4(0:7167)
       equivalence(SPW_6,SPW_6_4)
        Call FDA50_ConInQ
!beg ���������2:20FDA52AE501KE01
        SPW_2(16293)=.false.	!L_(380) O
!end ���������2:20FDA52AE501KE01
!beg ���������2:20FDA52AE501KE02
        SPW_2(15928)=.false.	!L_(15) O
!end ���������2:20FDA52AE501KE02
!beg ���������9:20FDA52AE501KE01
        SPW_2(16286)=.false.	!L_(373) O
!end ���������9:20FDA52AE501KE01
!beg ���������9:20FDA52AE501KE02
        SPW_2(15921)=.false.	!L_(8) O
!end ���������9:20FDA52AE501KE02
!beg ���������1:20FDA52AE501KE01
        SPW_2(16294)=.false.	!L_(381) O
!end ���������1:20FDA52AE501KE01
!beg ���������1:20FDA52AE501KE02
        SPW_2(15929)=.false.	!L_(16) O
!end ���������1:20FDA52AE501KE02
!beg ���������8:20FDA52AE501KE01
        SPW_2(16287)=.false.	!L_(374) O
!end ���������8:20FDA52AE501KE01
!beg ���������8:20FDA52AE501KE02
        SPW_2(15922)=.false.	!L_(9) O
!end ���������8:20FDA52AE501KE02
!beg ���������10:20FDA52AE501KE01
        SPW_2(16295)=.false.	!L_(382) O
!end ���������10:20FDA52AE501KE01
!beg ���������10:20FDA52AE501KE02
        SPW_2(15930)=.false.	!L_(17) O
!end ���������10:20FDA52AE501KE02
!beg �����:20FDA52AE501KE01
        SPW_2(16296)=.false.	!L_(383) O
!end �����:20FDA52AE501KE01
!beg �����:20FDA52AE501KE02
        SPW_2(15931)=.false.	!L_(18) O
!end �����:20FDA52AE501KE02
!beg ���������7:20FDA52AE501KE01
        SPW_2(16288)=.false.	!L_(375) O
!end ���������7:20FDA52AE501KE01
!beg ���������7:20FDA52AE501KE02
        SPW_2(15923)=.false.	!L_(10) O
!end ���������7:20FDA52AE501KE02
!beg ��������:20FDA51CU001KN01
        SPW_2(16303)=.false.	!L_(390) O
!end ��������:20FDA51CU001KN01
!beg ������:20FDA52AE501KE01
        SPW_2(16284)=.false.	!L_(371) O
!end ������:20FDA52AE501KE01
!beg ������:20FDA52AE501KE02
        SPW_2(15919)=.false.	!L_(6) O
!end ������:20FDA52AE501KE02
!beg ���������6:20FDA52AE501KE01
        SPW_2(16289)=.false.	!L_(376) O
!end ���������6:20FDA52AE501KE01
!beg ���������6:20FDA52AE501KE02
        SPW_2(15924)=.false.	!L_(11) O
!end ���������6:20FDA52AE501KE02
!beg ���������:20FDA51CU001KN01
        SPW_2(16304)=.false.	!L_(391) O
!end ���������:20FDA51CU001KN01
!beg ����:20FDA52AE501KE01
        SPW_2(16285)=.false.	!L_(372) O
!end ����:20FDA52AE501KE01
!beg ����:20FDA52AE501KE02
        SPW_2(15920)=.false.	!L_(7) O
!end ����:20FDA52AE501KE02
!beg ���������5:20FDA52AE501KE01
        SPW_2(16290)=.false.	!L_(377) O
!end ���������5:20FDA52AE501KE01
!beg ���������5:20FDA52AE501KE02
        SPW_2(15925)=.false.	!L_(12) O
!end ���������5:20FDA52AE501KE02
!beg ���������:20FDA52AE501KE01
        SPW_2(16282)=.false.	!L_(369) O
!end ���������:20FDA52AE501KE01
!beg ���������:20FDA52AE501KE02
        SPW_2(15917)=.false.	!L_(4) O
!end ���������:20FDA52AE501KE02
!beg ���������4:20FDA52AE501KE01
        SPW_2(16291)=.false.	!L_(378) O
!end ���������4:20FDA52AE501KE01
!beg ���������4:20FDA52AE501KE02
        SPW_2(15926)=.false.	!L_(13) O
!end ���������4:20FDA52AE501KE02
!beg ���������3:20FDA52AE501KE01
        SPW_2(16292)=.false.	!L_(379) O
!end ���������3:20FDA52AE501KE01
!beg ���������3:20FDA52AE501KE02
        SPW_2(15927)=.false.	!L_(14) O
!end ���������3:20FDA52AE501KE02
!beg ������:20FDA52AE501KE01
        SPW_2(16283)=.false.	!L_(370) O
!end ������:20FDA52AE501KE01
!beg ������:20FDA52AE501KE02
        SPW_2(15918)=.false.	!L_(5) O
!end ������:20FDA52AE501KE02
       end

       subroutine FDA60_ConInQ
       end

       subroutine FDA60_ConIn
       LOGICAL*1 SPW_0(0:196607)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:49151)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:65535)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:16383)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:28671)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:7167)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_6(0:28671)
       Common/SPW_6/ SPW_6 !
       integer*4 SPW_6_4(0:7167)
       equivalence(SPW_6,SPW_6_4)
        Call FDA60_ConInQ
!beg �������:20FDA65AA206
        SPW_0(172309)=.false.	!L_(298) O
!end �������:20FDA65AA206
!beg �������:20FDA65AA207
        SPW_0(172307)=.false.	!L_(296) O
!end �������:20FDA65AA207
!beg �������:20FDA65AA208
        SPW_0(172305)=.false.	!L_(294) O
!end �������:20FDA65AA208
!beg �������:20FDA65AA209
        SPW_0(172303)=.false.	!L_(292) O
!end �������:20FDA65AA209
!beg �������:20FDA64AA133
        SPW_0(172598)=.false.	!L_(587) O
!end �������:20FDA64AA133
!beg �������:20FDA64AA134
        SPW_0(172596)=.false.	!L_(585) O
!end �������:20FDA64AA134
!beg �������:20FDA64AA135
        SPW_0(172606)=.false.	!L_(595) O
!end �������:20FDA64AA135
!beg �������:20FDA64AA136
        SPW_0(172604)=.false.	!L_(593) O
!end �������:20FDA64AA136
!beg �������:20FDA64AA137
        SPW_0(172602)=.false.	!L_(591) O
!end �������:20FDA64AA137
!beg �������:20FDA64AA138
        SPW_0(172600)=.false.	!L_(589) O
!end �������:20FDA64AA138
!beg �������:20FDA64AA143
        SPW_0(172546)=.false.	!L_(535) O
!end �������:20FDA64AA143
!beg �������:20FDA64AA144
        SPW_0(172528)=.false.	!L_(517) O
!end �������:20FDA64AA144
!beg �������:20FDA64AA145
        SPW_0(172544)=.false.	!L_(533) O
!end �������:20FDA64AA145
!beg �������:20FDA64AA150
        SPW_0(172492)=.false.	!L_(481) O
!end �������:20FDA64AA150
!beg �������:20FDA64AA151
        SPW_0(172490)=.false.	!L_(479) O
!end �������:20FDA64AA151
!beg �������:20FDA64AA152
        SPW_0(172488)=.false.	!L_(477) O
!end �������:20FDA64AA152
!beg �������:20FDA64AA153
        SPW_0(172354)=.false.	!L_(343) O
!end �������:20FDA64AA153
!beg �������:20FDA64AA154
        SPW_0(172352)=.false.	!L_(341) O
!end �������:20FDA64AA154
!beg �������:20FDA62AA206
        SPW_0(172333)=.false.	!L_(322) O
!end �������:20FDA62AA206
!beg �������:20FDA64AA155
        SPW_0(172486)=.false.	!L_(475) O
!end �������:20FDA64AA155
!beg �������:20FDA62AA207
        SPW_0(172331)=.false.	!L_(320) O
!end �������:20FDA62AA207
!beg �������:20FDA64AA156
        SPW_0(172484)=.false.	!L_(473) O
!end �������:20FDA64AA156
!beg �������:20FDA62AA208
        SPW_0(172329)=.false.	!L_(318) O
!end �������:20FDA62AA208
!beg �������:20FDA64AA157
        SPW_0(172410)=.false.	!L_(399) O
!end �������:20FDA64AA157
!beg �������:20FDA62AA209
        SPW_0(172327)=.false.	!L_(316) O
!end �������:20FDA62AA209
!beg �������:20FDA64AA158
        SPW_0(172408)=.false.	!L_(397) O
!end �������:20FDA64AA158
!beg �������:20FDA64AA159
        SPW_0(172412)=.false.	!L_(401) O
!end �������:20FDA64AA159
!beg �������:20FDA61AA133
        SPW_0(172622)=.false.	!L_(611) O
!end �������:20FDA61AA133
!beg �������:20FDA61AA134
        SPW_0(172620)=.false.	!L_(609) O
!end �������:20FDA61AA134
!beg �������:20FDA61AA135
        SPW_0(172630)=.false.	!L_(619) O
!end �������:20FDA61AA135
!beg �������:20FDA61AA136
        SPW_0(172628)=.false.	!L_(617) O
!end �������:20FDA61AA136
!beg �������:20FDA61AA137
        SPW_0(172626)=.false.	!L_(615) O
!end �������:20FDA61AA137
!beg �������:20FDA61AA138
        SPW_0(172624)=.false.	!L_(613) O
!end �������:20FDA61AA138
!beg �������:20FDA64AA160
        SPW_0(172414)=.false.	!L_(403) O
!end �������:20FDA64AA160
!beg �������:20FDA64AA161
        SPW_0(172420)=.false.	!L_(409) O
!end �������:20FDA64AA161
!beg �������:20FDA64AA162
        SPW_0(172418)=.false.	!L_(407) O
!end �������:20FDA64AA162
!beg �������:20FDA64AA163
        SPW_0(172416)=.false.	!L_(405) O
!end �������:20FDA64AA163
!beg �������:20FDA64AA164
        SPW_0(172372)=.false.	!L_(361) O
!end �������:20FDA64AA164
!beg �������:20FDA61AA143
        SPW_0(172558)=.false.	!L_(547) O
!end �������:20FDA61AA143
!beg �������:20FDA61AA144
        SPW_0(172534)=.false.	!L_(523) O
!end �������:20FDA61AA144
!beg �������:20FDA61AA145
        SPW_0(172556)=.false.	!L_(545) O
!end �������:20FDA61AA145
!beg �������:20FDA61AA150
        SPW_0(172522)=.false.	!L_(511) O
!end �������:20FDA61AA150
!beg �������:20FDA61AA151
        SPW_0(172520)=.false.	!L_(509) O
!end �������:20FDA61AA151
!beg �������:20FDA61AA152
        SPW_0(172518)=.false.	!L_(507) O
!end �������:20FDA61AA152
!beg �������:20FDA61AA153
        SPW_0(172366)=.false.	!L_(355) O
!end �������:20FDA61AA153
!beg �������:20FDA61AA154
        SPW_0(172364)=.false.	!L_(353) O
!end �������:20FDA61AA154
!beg �������:20FDA61AA155
        SPW_0(172516)=.false.	!L_(505) O
!end �������:20FDA61AA155
!beg �������:20FDA61AA156
        SPW_0(172514)=.false.	!L_(503) O
!end �������:20FDA61AA156
!beg �������:20FDA61AA157
        SPW_0(172452)=.false.	!L_(441) O
!end �������:20FDA61AA157
!beg �������:20FDA61AA158
        SPW_0(172450)=.false.	!L_(439) O
!end �������:20FDA61AA158
!beg �������:20FDA61AA159
        SPW_0(172454)=.false.	!L_(443) O
!end �������:20FDA61AA159
!beg �������:20FDA61AA160
        SPW_0(172456)=.false.	!L_(445) O
!end �������:20FDA61AA160
!beg �������:20FDA61AA161
        SPW_0(172462)=.false.	!L_(451) O
!end �������:20FDA61AA161
!beg �������:20FDA61AA162
        SPW_0(172460)=.false.	!L_(449) O
!end �������:20FDA61AA162
!beg �������:20FDA61AA163
        SPW_0(172458)=.false.	!L_(447) O
!end �������:20FDA61AA163
!beg �������:20FDA61AA164
        SPW_0(172378)=.false.	!L_(367) O
!end �������:20FDA61AA164
!beg �������:20FDA64AA133
        SPW_0(172597)=.false.	!L_(586) O
!end �������:20FDA64AA133
!beg �������:20FDA64AA134
        SPW_0(172595)=.false.	!L_(584) O
!end �������:20FDA64AA134
!beg �������:20FDA64AA135
        SPW_0(172605)=.false.	!L_(594) O
!end �������:20FDA64AA135
!beg �������:20FDA64AA136
        SPW_0(172603)=.false.	!L_(592) O
!end �������:20FDA64AA136
!beg �������:20FDA64AA137
        SPW_0(172601)=.false.	!L_(590) O
!end �������:20FDA64AA137
!beg �������:20FDA64AA138
        SPW_0(172599)=.false.	!L_(588) O
!end �������:20FDA64AA138
!beg �������:20FDA64AA143
        SPW_0(172545)=.false.	!L_(534) O
!end �������:20FDA64AA143
!beg �������:20FDA64AA144
        SPW_0(172527)=.false.	!L_(516) O
!end �������:20FDA64AA144
!beg �������:20FDA64AA145
        SPW_0(172543)=.false.	!L_(532) O
!end �������:20FDA64AA145
!beg �������:20FDA64AA150
        SPW_0(172491)=.false.	!L_(480) O
!end �������:20FDA64AA150
!beg �������:20FDA64AA151
        SPW_0(172489)=.false.	!L_(478) O
!end �������:20FDA64AA151
!beg �������:20FDA64AA152
        SPW_0(172487)=.false.	!L_(476) O
!end �������:20FDA64AA152
!beg �������:20FDA64AA153
        SPW_0(172353)=.false.	!L_(342) O
!end �������:20FDA64AA153
!beg �������:20FDA64AA154
        SPW_0(172351)=.false.	!L_(340) O
!end �������:20FDA64AA154
!beg �������:20FDA64AA155
        SPW_0(172485)=.false.	!L_(474) O
!end �������:20FDA64AA155
!beg �������:20FDA64AA156
        SPW_0(172483)=.false.	!L_(472) O
!end �������:20FDA64AA156
!beg �������:20FDA64AA157
        SPW_0(172409)=.false.	!L_(398) O
!end �������:20FDA64AA157
!beg �������:20FDA64AA158
        SPW_0(172407)=.false.	!L_(396) O
!end �������:20FDA64AA158
!beg �������:20FDA64AA159
        SPW_0(172411)=.false.	!L_(400) O
!end �������:20FDA64AA159
!beg �������:20FDA61AA133
        SPW_0(172621)=.false.	!L_(610) O
!end �������:20FDA61AA133
!beg �������:20FDA61AA134
        SPW_0(172619)=.false.	!L_(608) O
!end �������:20FDA61AA134
!beg �������:20FDA61AA135
        SPW_0(172629)=.false.	!L_(618) O
!end �������:20FDA61AA135
!beg �������:20FDA61AA136
        SPW_0(172627)=.false.	!L_(616) O
!end �������:20FDA61AA136
!beg �������:20FDA61AA137
        SPW_0(172625)=.false.	!L_(614) O
!end �������:20FDA61AA137
!beg ������:20FDA60AE408
        SPW_0(174262)=.false.	!L_(2251) O
!end ������:20FDA60AE408
!beg �������:20FDA61AA138
        SPW_0(172623)=.false.	!L_(612) O
!end �������:20FDA61AA138
!beg �������:20FDA64AA160
        SPW_0(172413)=.false.	!L_(402) O
!end �������:20FDA64AA160
!beg �������:20FDA64AA161
        SPW_0(172419)=.false.	!L_(408) O
!end �������:20FDA64AA161
!beg �������:20FDA64AA162
        SPW_0(172417)=.false.	!L_(406) O
!end �������:20FDA64AA162
!beg �������:20FDA64AA163
        SPW_0(172415)=.false.	!L_(404) O
!end �������:20FDA64AA163
!beg �������:20FDA64AA164
        SPW_0(172371)=.false.	!L_(360) O
!end �������:20FDA64AA164
!beg �����:20FDA60AE408
        SPW_0(174263)=.false.	!L_(2252) O
!end �����:20FDA60AE408
!beg ������:20FDA60AE413
        SPW_0(174258)=.false.	!L_(2247) O
!end ������:20FDA60AE413
!beg �������:20FDA61AA143
        SPW_0(172557)=.false.	!L_(546) O
!end �������:20FDA61AA143
!beg �������:20FDA61AA144
        SPW_0(172533)=.false.	!L_(522) O
!end �������:20FDA61AA144
!beg �������:20FDA61AA145
        SPW_0(172555)=.false.	!L_(544) O
!end �������:20FDA61AA145
!beg �����:20FDA60AE413
        SPW_0(174259)=.false.	!L_(2248) O
!end �����:20FDA60AE413
!beg �������:20FDA61AA150
        SPW_0(172521)=.false.	!L_(510) O
!end �������:20FDA61AA150
!beg �������:20FDA61AA151
        SPW_0(172519)=.false.	!L_(508) O
!end �������:20FDA61AA151
!beg �������:20FDA61AA152
        SPW_0(172517)=.false.	!L_(506) O
!end �������:20FDA61AA152
!beg �������:20FDA61AA153
        SPW_0(172365)=.false.	!L_(354) O
!end �������:20FDA61AA153
!beg �������:20FDA61AA154
        SPW_0(172363)=.false.	!L_(352) O
!end �������:20FDA61AA154
!beg �������:20FDA61AA155
        SPW_0(172515)=.false.	!L_(504) O
!end �������:20FDA61AA155
!beg �������:20FDA61AA156
        SPW_0(172513)=.false.	!L_(502) O
!end �������:20FDA61AA156
!beg �������:20FDA61AA157
        SPW_0(172451)=.false.	!L_(440) O
!end �������:20FDA61AA157
!beg �������:20FDA61AA158
        SPW_0(172449)=.false.	!L_(438) O
!end �������:20FDA61AA158
!beg �������:20FDA61AA159
        SPW_0(172453)=.false.	!L_(442) O
!end �������:20FDA61AA159
!beg �������:20FDA61AA160
        SPW_0(172455)=.false.	!L_(444) O
!end �������:20FDA61AA160
!beg �������:20FDA61AA161
        SPW_0(172461)=.false.	!L_(450) O
!end �������:20FDA61AA161
!beg �������:20FDA61AA162
        SPW_0(172459)=.false.	!L_(448) O
!end �������:20FDA61AA162
!beg �������:20FDA61AA163
        SPW_0(172457)=.false.	!L_(446) O
!end �������:20FDA61AA163
!beg �������:20FDA61AA164
        SPW_0(172377)=.false.	!L_(366) O
!end �������:20FDA61AA164
!beg ��������:20FDA60AE408
        SPW_0(174264)=.false.	!L_(2253) O
!end ��������:20FDA60AE408
!beg ��������:20FDA60AE413
        SPW_0(174260)=.false.	!L_(2249) O
!end ��������:20FDA60AE413
!beg �������:20FDA64AA206
        SPW_0(172318)=.false.	!L_(307) O
!end �������:20FDA64AA206
!beg �������:20FDA64AA207
        SPW_0(172316)=.false.	!L_(305) O
!end �������:20FDA64AA207
!beg �������:20FDA64AA208
        SPW_0(172314)=.false.	!L_(303) O
!end �������:20FDA64AA208
!beg �������:20FDA64AA209
        SPW_0(172312)=.false.	!L_(301) O
!end �������:20FDA64AA209
!beg �������:20FDA61AA206
        SPW_0(172342)=.false.	!L_(331) O
!end �������:20FDA61AA206
!beg �������:20FDA61AA207
        SPW_0(172340)=.false.	!L_(329) O
!end �������:20FDA61AA207
!beg �������:20FDA61AA208
        SPW_0(172338)=.false.	!L_(327) O
!end �������:20FDA61AA208
!beg �������:20FDA61AA209
        SPW_0(172336)=.false.	!L_(325) O
!end �������:20FDA61AA209
!beg �������:20FDA66AA133
        SPW_0(172562)=.false.	!L_(551) O
!end �������:20FDA66AA133
!beg �������:20FDA66AA134
        SPW_0(172560)=.false.	!L_(549) O
!end �������:20FDA66AA134
!beg �������:20FDA66AA135
        SPW_0(172570)=.false.	!L_(559) O
!end �������:20FDA66AA135
!beg �������:20FDA66AA136
        SPW_0(172568)=.false.	!L_(557) O
!end �������:20FDA66AA136
!beg �������:20FDA66AA137
        SPW_0(172566)=.false.	!L_(555) O
!end �������:20FDA66AA137
!beg �������:20FDA66AA138
        SPW_0(172564)=.false.	!L_(553) O
!end �������:20FDA66AA138
!beg �������:20FDA66AA143
        SPW_0(172538)=.false.	!L_(527) O
!end �������:20FDA66AA143
!beg �������:20FDA66AA144
        SPW_0(172524)=.false.	!L_(513) O
!end �������:20FDA66AA144
!beg �������:20FDA66AA145
        SPW_0(172536)=.false.	!L_(525) O
!end �������:20FDA66AA145
!beg �������:20FDA60AA100
        SPW_0(172664)=.false.	!L_(653) O
!end �������:20FDA60AA100
!beg �������:20FDA60AA101
        SPW_0(172662)=.false.	!L_(651) O
!end �������:20FDA60AA101
!beg �������:20FDA60AA102
        SPW_0(172660)=.false.	!L_(649) O
!end �������:20FDA60AA102
!beg �������:20FDA60AA103
        SPW_0(172658)=.false.	!L_(647) O
!end �������:20FDA60AA103
!beg �������:20FDA66AA150
        SPW_0(172472)=.false.	!L_(461) O
!end �������:20FDA66AA150
!beg �������:20FDA66AA151
        SPW_0(172470)=.false.	!L_(459) O
!end �������:20FDA66AA151
!beg �������:20FDA66AA152
        SPW_0(172468)=.false.	!L_(457) O
!end �������:20FDA66AA152
!beg �������:20FDA66AA153
        SPW_0(172346)=.false.	!L_(335) O
!end �������:20FDA66AA153
!beg �������:20FDA66AA154
        SPW_0(172344)=.false.	!L_(333) O
!end �������:20FDA66AA154
!beg �������:20FDA64AA206
        SPW_0(172317)=.false.	!L_(306) O
!end �������:20FDA64AA206
!beg �������:20FDA60AA108
        SPW_0(172656)=.false.	!L_(645) O
!end �������:20FDA60AA108
!beg �������:20FDA66AA155
        SPW_0(172466)=.false.	!L_(455) O
!end �������:20FDA66AA155
!beg �������:20FDA64AA207
        SPW_0(172315)=.false.	!L_(304) O
!end �������:20FDA64AA207
!beg �������:20FDA60AA109
        SPW_0(172654)=.false.	!L_(643) O
!end �������:20FDA60AA109
!beg �������:20FDA66AA156
        SPW_0(172464)=.false.	!L_(453) O
!end �������:20FDA66AA156
!beg �������:20FDA64AA208
        SPW_0(172313)=.false.	!L_(302) O
!end �������:20FDA64AA208
!beg �������:20FDA66AA157
        SPW_0(172382)=.false.	!L_(371) O
!end �������:20FDA66AA157
!beg �������:20FDA64AA209
        SPW_0(172311)=.false.	!L_(300) O
!end �������:20FDA64AA209
!beg �������:20FDA66AA158
        SPW_0(172380)=.false.	!L_(369) O
!end �������:20FDA66AA158
!beg �������:20FDA66AA159
        SPW_0(172384)=.false.	!L_(373) O
!end �������:20FDA66AA159
!beg �������:20FDA63AA133
        SPW_0(172586)=.false.	!L_(575) O
!end �������:20FDA63AA133
!beg �������:20FDA63AA134
        SPW_0(172584)=.false.	!L_(573) O
!end �������:20FDA63AA134
!beg �������:20FDA63AA135
        SPW_0(172594)=.false.	!L_(583) O
!end �������:20FDA63AA135
!beg �������:20FDA63AA136
        SPW_0(172592)=.false.	!L_(581) O
!end �������:20FDA63AA136
!beg �������:20FDA60AA110
        SPW_0(172652)=.false.	!L_(641) O
!end �������:20FDA60AA110
!beg �������:20FDA63AA137
        SPW_0(172590)=.false.	!L_(579) O
!end �������:20FDA63AA137
!beg �������:20FDA60AA111
        SPW_0(172650)=.false.	!L_(639) O
!end �������:20FDA60AA111
!beg �������:20FDA63AA138
        SPW_0(172588)=.false.	!L_(577) O
!end �������:20FDA63AA138
!beg �������:20FDA66AA160
        SPW_0(172386)=.false.	!L_(375) O
!end �������:20FDA66AA160
!beg �������:20FDA66AA161
        SPW_0(172392)=.false.	!L_(381) O
!end �������:20FDA66AA161
!beg �������:20FDA66AA162
        SPW_0(172390)=.false.	!L_(379) O
!end �������:20FDA66AA162
!beg �������:20FDA60AA116
        SPW_0(172638)=.false.	!L_(627) O
!end �������:20FDA60AA116
!beg �������:20FDA66AA163
        SPW_0(172388)=.false.	!L_(377) O
!end �������:20FDA66AA163
!beg �������:20FDA66AA164
        SPW_0(172368)=.false.	!L_(357) O
!end �������:20FDA66AA164
!beg �������:20FDA60AA117
        SPW_0(172294)=.false.	!L_(283) O
!end �������:20FDA60AA117
!beg �������:20FDA60AA118
        SPW_0(172636)=.false.	!L_(625) O
!end �������:20FDA60AA118
!beg �������:20FDA63AA143
        SPW_0(172550)=.false.	!L_(539) O
!end �������:20FDA63AA143
!beg �������:20FDA63AA144
        SPW_0(172530)=.false.	!L_(519) O
!end �������:20FDA63AA144
!beg �������:20FDA63AA145
        SPW_0(172548)=.false.	!L_(537) O
!end �������:20FDA63AA145
!beg �������:20FDA60AA124
        SPW_0(172634)=.false.	!L_(623) O
!end �������:20FDA60AA124
!beg �������:20FDA60AA126
        SPW_0(172632)=.false.	!L_(621) O
!end �������:20FDA60AA126
!beg �������:20FDA63AA150
        SPW_0(172502)=.false.	!L_(491) O
!end �������:20FDA63AA150
!beg �������:20FDA63AA151
        SPW_0(172500)=.false.	!L_(489) O
!end �������:20FDA63AA151
!beg �������:20FDA63AA152
        SPW_0(172498)=.false.	!L_(487) O
!end �������:20FDA63AA152
!beg �������:20FDA63AA153
        SPW_0(172358)=.false.	!L_(347) O
!end �������:20FDA63AA153
!beg �������:20FDA63AA154
        SPW_0(172356)=.false.	!L_(345) O
!end �������:20FDA63AA154
!beg �������:20FDA61AA206
        SPW_0(172341)=.false.	!L_(330) O
!end �������:20FDA61AA206
!beg �������:20FDA63AA155
        SPW_0(172496)=.false.	!L_(485) O
!end �������:20FDA63AA155
!beg �������:20FDA61AA207
        SPW_0(172339)=.false.	!L_(328) O
!end �������:20FDA61AA207
!beg �������:20FDA63AA156
        SPW_0(172494)=.false.	!L_(483) O
!end �������:20FDA63AA156
!beg �������:20FDA61AA208
        SPW_0(172337)=.false.	!L_(326) O
!end �������:20FDA61AA208
!beg �������:20FDA63AA157
        SPW_0(172424)=.false.	!L_(413) O
!end �������:20FDA63AA157
!beg �������:20FDA61AA209
        SPW_0(172335)=.false.	!L_(324) O
!end �������:20FDA61AA209
!beg �������:20FDA63AA158
        SPW_0(172422)=.false.	!L_(411) O
!end �������:20FDA63AA158
!beg �������:20FDA60AA132
        SPW_0(172644)=.false.	!L_(633) O
!end �������:20FDA60AA132
!beg �������:20FDA63AA159
        SPW_0(172426)=.false.	!L_(415) O
!end �������:20FDA63AA159
!beg �������:20FDA60AA133
        SPW_0(172642)=.false.	!L_(631) O
!end �������:20FDA60AA133
!beg �������:20FDA60AA134
        SPW_0(172648)=.false.	!L_(637) O
!end �������:20FDA60AA134
!beg �������:20FDA60AA135
        SPW_0(172646)=.false.	!L_(635) O
!end �������:20FDA60AA135
!beg �������:20FDA60AA136
        SPW_0(172640)=.false.	!L_(629) O
!end �������:20FDA60AA136
!beg �������:20FDA63AA160
        SPW_0(172428)=.false.	!L_(417) O
!end �������:20FDA63AA160
!beg �������:20FDA63AA161
        SPW_0(172434)=.false.	!L_(423) O
!end �������:20FDA63AA161
!beg �������:20FDA63AA162
        SPW_0(172432)=.false.	!L_(421) O
!end �������:20FDA63AA162
!beg �������:20FDA63AA163
        SPW_0(172430)=.false.	!L_(419) O
!end �������:20FDA63AA163
!beg �������:20FDA63AA164
        SPW_0(172374)=.false.	!L_(363) O
!end �������:20FDA63AA164
!beg �������:20FDA66AA133
        SPW_0(172561)=.false.	!L_(550) O
!end �������:20FDA66AA133
!beg �������:20FDA66AA134
        SPW_0(172559)=.false.	!L_(548) O
!end �������:20FDA66AA134
!beg �������:20FDA66AA135
        SPW_0(172569)=.false.	!L_(558) O
!end �������:20FDA66AA135
!beg �������:20FDA66AA136
        SPW_0(172567)=.false.	!L_(556) O
!end �������:20FDA66AA136
!beg �������:20FDA66AA137
        SPW_0(172565)=.false.	!L_(554) O
!end �������:20FDA66AA137
!beg �������:20FDA66AA138
        SPW_0(172563)=.false.	!L_(552) O
!end �������:20FDA66AA138
!beg �������:20FDA66AA143
        SPW_0(172537)=.false.	!L_(526) O
!end �������:20FDA66AA143
!beg �������:20FDA66AA144
        SPW_0(172523)=.false.	!L_(512) O
!end �������:20FDA66AA144
!beg �������:20FDA66AA145
        SPW_0(172535)=.false.	!L_(524) O
!end �������:20FDA66AA145
!beg �������:20FDA60AA100
        SPW_0(172663)=.false.	!L_(652) O
!end �������:20FDA60AA100
!beg �������:20FDA60AA101
        SPW_0(172661)=.false.	!L_(650) O
!end �������:20FDA60AA101
!beg �������:20FDA60AA102
        SPW_0(172659)=.false.	!L_(648) O
!end �������:20FDA60AA102
!beg �������:20FDA60AA103
        SPW_0(172657)=.false.	!L_(646) O
!end �������:20FDA60AA103
!beg �������:20FDA66AA150
        SPW_0(172471)=.false.	!L_(460) O
!end �������:20FDA66AA150
!beg �������:20FDA66AA151
        SPW_0(172469)=.false.	!L_(458) O
!end �������:20FDA66AA151
!beg �������:20FDA66AA152
        SPW_0(172467)=.false.	!L_(456) O
!end �������:20FDA66AA152
!beg �������:20FDA66AA153
        SPW_0(172345)=.false.	!L_(334) O
!end �������:20FDA66AA153
!beg �������:20FDA66AA154
        SPW_0(172343)=.false.	!L_(332) O
!end �������:20FDA66AA154
!beg �������:20FDA60AA108
        SPW_0(172655)=.false.	!L_(644) O
!end �������:20FDA60AA108
!beg �������:20FDA66AA155
        SPW_0(172465)=.false.	!L_(454) O
!end �������:20FDA66AA155
!beg �������:20FDA60AA109
        SPW_0(172653)=.false.	!L_(642) O
!end �������:20FDA60AA109
!beg �������:20FDA66AA156
        SPW_0(172463)=.false.	!L_(452) O
!end �������:20FDA66AA156
!beg �������:20FDA66AA157
        SPW_0(172381)=.false.	!L_(370) O
!end �������:20FDA66AA157
!beg �������:20FDA66AA158
        SPW_0(172379)=.false.	!L_(368) O
!end �������:20FDA66AA158
!beg �������:20FDA66AA159
        SPW_0(172383)=.false.	!L_(372) O
!end �������:20FDA66AA159
!beg �������:20FDA63AA133
        SPW_0(172585)=.false.	!L_(574) O
!end �������:20FDA63AA133
!beg �������:20FDA63AA134
        SPW_0(172583)=.false.	!L_(572) O
!end �������:20FDA63AA134
!beg �������:20FDA63AA135
        SPW_0(172593)=.false.	!L_(582) O
!end �������:20FDA63AA135
!beg �������:20FDA63AA136
        SPW_0(172591)=.false.	!L_(580) O
!end �������:20FDA63AA136
!beg �������:20FDA60AA110
        SPW_0(172651)=.false.	!L_(640) O
!end �������:20FDA60AA110
!beg �������:20FDA63AA137
        SPW_0(172589)=.false.	!L_(578) O
!end �������:20FDA63AA137
!beg �������:20FDA60AA111
        SPW_0(172649)=.false.	!L_(638) O
!end �������:20FDA60AA111
!beg �������:20FDA63AA138
        SPW_0(172587)=.false.	!L_(576) O
!end �������:20FDA63AA138
!beg �������:20FDA66AA160
        SPW_0(172385)=.false.	!L_(374) O
!end �������:20FDA66AA160
!beg �������:20FDA66AA161
        SPW_0(172391)=.false.	!L_(380) O
!end �������:20FDA66AA161
!beg �������:20FDA66AA162
        SPW_0(172389)=.false.	!L_(378) O
!end �������:20FDA66AA162
!beg �������:20FDA60AA116
        SPW_0(172637)=.false.	!L_(626) O
!end �������:20FDA60AA116
!beg �������:20FDA66AA163
        SPW_0(172387)=.false.	!L_(376) O
!end �������:20FDA66AA163
!beg �������:20FDA66AA164
        SPW_0(172367)=.false.	!L_(356) O
!end �������:20FDA66AA164
!beg �������:20FDA60AA117
        SPW_0(172293)=.false.	!L_(282) O
!end �������:20FDA60AA117
!beg �������:20FDA60AA118
        SPW_0(172635)=.false.	!L_(624) O
!end �������:20FDA60AA118
!beg �������:20FDA63AA143
        SPW_0(172549)=.false.	!L_(538) O
!end �������:20FDA63AA143
!beg �������:20FDA63AA144
        SPW_0(172529)=.false.	!L_(518) O
!end �������:20FDA63AA144
!beg �������:20FDA63AA145
        SPW_0(172547)=.false.	!L_(536) O
!end �������:20FDA63AA145
!beg �������:20FDA60AA124
        SPW_0(172633)=.false.	!L_(622) O
!end �������:20FDA60AA124
!beg �������:20FDA60AA126
        SPW_0(172631)=.false.	!L_(620) O
!end �������:20FDA60AA126
!beg �������:20FDA63AA150
        SPW_0(172501)=.false.	!L_(490) O
!end �������:20FDA63AA150
!beg �������:20FDA63AA151
        SPW_0(172499)=.false.	!L_(488) O
!end �������:20FDA63AA151
!beg �������:20FDA63AA152
        SPW_0(172497)=.false.	!L_(486) O
!end �������:20FDA63AA152
!beg �������:20FDA63AA153
        SPW_0(172357)=.false.	!L_(346) O
!end �������:20FDA63AA153
!beg �������:20FDA63AA154
        SPW_0(172355)=.false.	!L_(344) O
!end �������:20FDA63AA154
!beg �������:20FDA63AA155
        SPW_0(172495)=.false.	!L_(484) O
!end �������:20FDA63AA155
!beg �������:20FDA63AA156
        SPW_0(172493)=.false.	!L_(482) O
!end �������:20FDA63AA156
!beg �������:20FDA63AA157
        SPW_0(172423)=.false.	!L_(412) O
!end �������:20FDA63AA157
!beg �������:20FDA63AA158
        SPW_0(172421)=.false.	!L_(410) O
!end �������:20FDA63AA158
!beg �������:20FDA60AA132
        SPW_0(172643)=.false.	!L_(632) O
!end �������:20FDA60AA132
!beg �������:20FDA63AA159
        SPW_0(172425)=.false.	!L_(414) O
!end �������:20FDA63AA159
!beg �������:20FDA60AA133
        SPW_0(172641)=.false.	!L_(630) O
!end �������:20FDA60AA133
!beg �������:20FDA60AA134
        SPW_0(172647)=.false.	!L_(636) O
!end �������:20FDA60AA134
!beg �������:20FDA60AA135
        SPW_0(172645)=.false.	!L_(634) O
!end �������:20FDA60AA135
!beg �������:20FDA60AA136
        SPW_0(172639)=.false.	!L_(628) O
!end �������:20FDA60AA136
!beg �������:20FDA63AA160
        SPW_0(172427)=.false.	!L_(416) O
!end �������:20FDA63AA160
!beg �������:20FDA63AA161
        SPW_0(172433)=.false.	!L_(422) O
!end �������:20FDA63AA161
!beg �������:20FDA63AA162
        SPW_0(172431)=.false.	!L_(420) O
!end �������:20FDA63AA162
!beg �������:20FDA63AA163
        SPW_0(172429)=.false.	!L_(418) O
!end �������:20FDA63AA163
!beg �������:20FDA63AA164
        SPW_0(172373)=.false.	!L_(362) O
!end �������:20FDA63AA164
!beg �������:20FDA66AA206
        SPW_0(172302)=.false.	!L_(291) O
!end �������:20FDA66AA206
!beg �������:20FDA66AA207
        SPW_0(172300)=.false.	!L_(289) O
!end �������:20FDA66AA207
!beg �������:20FDA66AA208
        SPW_0(172298)=.false.	!L_(287) O
!end �������:20FDA66AA208
!beg �������:20FDA66AA209
        SPW_0(172296)=.false.	!L_(285) O
!end �������:20FDA66AA209
!beg �������:20FDA63AA206
        SPW_0(172326)=.false.	!L_(315) O
!end �������:20FDA63AA206
!beg �������:20FDA63AA207
        SPW_0(172324)=.false.	!L_(313) O
!end �������:20FDA63AA207
!beg �������:20FDA63AA208
        SPW_0(172322)=.false.	!L_(311) O
!end �������:20FDA63AA208
!beg �������:20FDA63AA209
        SPW_0(172320)=.false.	!L_(309) O
!end �������:20FDA63AA209
!beg �������:20FDA66AA206
        SPW_0(172301)=.false.	!L_(290) O
!end �������:20FDA66AA206
!beg �������:20FDA66AA207
        SPW_0(172299)=.false.	!L_(288) O
!end �������:20FDA66AA207
!beg �������:20FDA66AA208
        SPW_0(172297)=.false.	!L_(286) O
!end �������:20FDA66AA208
!beg �������:20FDA66AA209
        SPW_0(172295)=.false.	!L_(284) O
!end �������:20FDA66AA209
!beg �������:20FDA65AA133
        SPW_0(172574)=.false.	!L_(563) O
!end �������:20FDA65AA133
!beg �������:20FDA65AA134
        SPW_0(172572)=.false.	!L_(561) O
!end �������:20FDA65AA134
!beg �������:20FDA65AA135
        SPW_0(172582)=.false.	!L_(571) O
!end �������:20FDA65AA135
!beg �������:20FDA65AA136
        SPW_0(172580)=.false.	!L_(569) O
!end �������:20FDA65AA136
!beg �������:20FDA65AA137
        SPW_0(172578)=.false.	!L_(567) O
!end �������:20FDA65AA137
!beg �������:20FDA65AA138
        SPW_0(172576)=.false.	!L_(565) O
!end �������:20FDA65AA138
!beg �������:20FDA65AA143
        SPW_0(172542)=.false.	!L_(531) O
!end �������:20FDA65AA143
!beg �������:20FDA65AA144
        SPW_0(172526)=.false.	!L_(515) O
!end �������:20FDA65AA144
!beg �������:20FDA65AA145
        SPW_0(172540)=.false.	!L_(529) O
!end �������:20FDA65AA145
!beg �������:20FDA65AA150
        SPW_0(172482)=.false.	!L_(471) O
!end �������:20FDA65AA150
!beg �������:20FDA65AA151
        SPW_0(172480)=.false.	!L_(469) O
!end �������:20FDA65AA151
!beg �������:20FDA65AA152
        SPW_0(172478)=.false.	!L_(467) O
!end �������:20FDA65AA152
!beg �������:20FDA65AA153
        SPW_0(172350)=.false.	!L_(339) O
!end �������:20FDA65AA153
!beg �������:20FDA65AA154
        SPW_0(172348)=.false.	!L_(337) O
!end �������:20FDA65AA154
!beg �������:20FDA63AA206
        SPW_0(172325)=.false.	!L_(314) O
!end �������:20FDA63AA206
!beg �������:20FDA65AA155
        SPW_0(172476)=.false.	!L_(465) O
!end �������:20FDA65AA155
!beg �������:20FDA63AA207
        SPW_0(172323)=.false.	!L_(312) O
!end �������:20FDA63AA207
!beg �������:20FDA65AA156
        SPW_0(172474)=.false.	!L_(463) O
!end �������:20FDA65AA156
!beg �������:20FDA63AA208
        SPW_0(172321)=.false.	!L_(310) O
!end �������:20FDA63AA208
!beg �������:20FDA65AA157
        SPW_0(172396)=.false.	!L_(385) O
!end �������:20FDA65AA157
!beg �������:20FDA63AA209
        SPW_0(172319)=.false.	!L_(308) O
!end �������:20FDA63AA209
!beg �������:20FDA65AA158
        SPW_0(172394)=.false.	!L_(383) O
!end �������:20FDA65AA158
!beg �������:20FDA65AA159
        SPW_0(172398)=.false.	!L_(387) O
!end �������:20FDA65AA159
!beg �������:20FDA62AA133
        SPW_0(172610)=.false.	!L_(599) O
!end �������:20FDA62AA133
!beg �������:20FDA62AA134
        SPW_0(172608)=.false.	!L_(597) O
!end �������:20FDA62AA134
!beg �������:20FDA62AA135
        SPW_0(172618)=.false.	!L_(607) O
!end �������:20FDA62AA135
!beg �������:20FDA62AA136
        SPW_0(172616)=.false.	!L_(605) O
!end �������:20FDA62AA136
!beg �������:20FDA62AA137
        SPW_0(172614)=.false.	!L_(603) O
!end �������:20FDA62AA137
!beg �������:20FDA62AA138
        SPW_0(172612)=.false.	!L_(601) O
!end �������:20FDA62AA138
!beg �������:20FDA65AA160
        SPW_0(172400)=.false.	!L_(389) O
!end �������:20FDA65AA160
!beg �������:20FDA65AA161
        SPW_0(172406)=.false.	!L_(395) O
!end �������:20FDA65AA161
!beg �������:20FDA65AA162
        SPW_0(172404)=.false.	!L_(393) O
!end �������:20FDA65AA162
!beg �������:20FDA65AA163
        SPW_0(172402)=.false.	!L_(391) O
!end �������:20FDA65AA163
!beg �������:20FDA65AA164
        SPW_0(172370)=.false.	!L_(359) O
!end �������:20FDA65AA164
!beg �������:20FDA62AA143
        SPW_0(172554)=.false.	!L_(543) O
!end �������:20FDA62AA143
!beg �������:20FDA62AA144
        SPW_0(172532)=.false.	!L_(521) O
!end �������:20FDA62AA144
!beg �������:20FDA62AA145
        SPW_0(172552)=.false.	!L_(541) O
!end �������:20FDA62AA145
!beg �������:20FDA62AA150
        SPW_0(172512)=.false.	!L_(501) O
!end �������:20FDA62AA150
!beg �������:20FDA62AA151
        SPW_0(172510)=.false.	!L_(499) O
!end �������:20FDA62AA151
!beg �������:20FDA62AA152
        SPW_0(172508)=.false.	!L_(497) O
!end �������:20FDA62AA152
!beg �������:20FDA62AA153
        SPW_0(172362)=.false.	!L_(351) O
!end �������:20FDA62AA153
!beg �������:20FDA62AA154
        SPW_0(172360)=.false.	!L_(349) O
!end �������:20FDA62AA154
!beg �������:20FDA62AA155
        SPW_0(172506)=.false.	!L_(495) O
!end �������:20FDA62AA155
!beg �������:20FDA62AA156
        SPW_0(172504)=.false.	!L_(493) O
!end �������:20FDA62AA156
!beg �������:20FDA62AA157
        SPW_0(172438)=.false.	!L_(427) O
!end �������:20FDA62AA157
!beg �������:20FDA62AA158
        SPW_0(172436)=.false.	!L_(425) O
!end �������:20FDA62AA158
!beg �������:20FDA62AA159
        SPW_0(172440)=.false.	!L_(429) O
!end �������:20FDA62AA159
!beg �������:20FDA62AA160
        SPW_0(172442)=.false.	!L_(431) O
!end �������:20FDA62AA160
!beg �������:20FDA62AA161
        SPW_0(172448)=.false.	!L_(437) O
!end �������:20FDA62AA161
!beg �������:20FDA62AA162
        SPW_0(172446)=.false.	!L_(435) O
!end �������:20FDA62AA162
!beg �������:20FDA62AA163
        SPW_0(172444)=.false.	!L_(433) O
!end �������:20FDA62AA163
!beg �������:20FDA62AA164
        SPW_0(172376)=.false.	!L_(365) O
!end �������:20FDA62AA164
!beg �������:20FDA65AA133
        SPW_0(172573)=.false.	!L_(562) O
!end �������:20FDA65AA133
!beg �������:20FDA65AA134
        SPW_0(172571)=.false.	!L_(560) O
!end �������:20FDA65AA134
!beg �������:20FDA65AA135
        SPW_0(172581)=.false.	!L_(570) O
!end �������:20FDA65AA135
!beg �������:20FDA65AA136
        SPW_0(172579)=.false.	!L_(568) O
!end �������:20FDA65AA136
!beg �������:20FDA65AA137
        SPW_0(172577)=.false.	!L_(566) O
!end �������:20FDA65AA137
!beg �������:20FDA65AA138
        SPW_0(172575)=.false.	!L_(564) O
!end �������:20FDA65AA138
!beg �������:20FDA65AA143
        SPW_0(172541)=.false.	!L_(530) O
!end �������:20FDA65AA143
!beg �������:20FDA65AA144
        SPW_0(172525)=.false.	!L_(514) O
!end �������:20FDA65AA144
!beg �������:20FDA65AA145
        SPW_0(172539)=.false.	!L_(528) O
!end �������:20FDA65AA145
!beg �������:20FDA65AA150
        SPW_0(172481)=.false.	!L_(470) O
!end �������:20FDA65AA150
!beg �������:20FDA65AA151
        SPW_0(172479)=.false.	!L_(468) O
!end �������:20FDA65AA151
!beg �������:20FDA65AA152
        SPW_0(172477)=.false.	!L_(466) O
!end �������:20FDA65AA152
!beg �������:20FDA65AA153
        SPW_0(172349)=.false.	!L_(338) O
!end �������:20FDA65AA153
!beg �������:20FDA65AA154
        SPW_0(172347)=.false.	!L_(336) O
!end �������:20FDA65AA154
!beg �������:20FDA65AA155
        SPW_0(172475)=.false.	!L_(464) O
!end �������:20FDA65AA155
!beg �������:20FDA65AA156
        SPW_0(172473)=.false.	!L_(462) O
!end �������:20FDA65AA156
!beg �������:20FDA65AA157
        SPW_0(172395)=.false.	!L_(384) O
!end �������:20FDA65AA157
!beg �������:20FDA65AA158
        SPW_0(172393)=.false.	!L_(382) O
!end �������:20FDA65AA158
!beg �������:20FDA65AA159
        SPW_0(172397)=.false.	!L_(386) O
!end �������:20FDA65AA159
!beg �������:20FDA62AA133
        SPW_0(172609)=.false.	!L_(598) O
!end �������:20FDA62AA133
!beg �������:20FDA62AA134
        SPW_0(172607)=.false.	!L_(596) O
!end �������:20FDA62AA134
!beg �������:20FDA62AA135
        SPW_0(172617)=.false.	!L_(606) O
!end �������:20FDA62AA135
!beg �������:20FDA62AA136
        SPW_0(172615)=.false.	!L_(604) O
!end �������:20FDA62AA136
!beg �������:20FDA62AA137
        SPW_0(172613)=.false.	!L_(602) O
!end �������:20FDA62AA137
!beg �������:20FDA62AA138
        SPW_0(172611)=.false.	!L_(600) O
!end �������:20FDA62AA138
!beg �������:20FDA65AA160
        SPW_0(172399)=.false.	!L_(388) O
!end �������:20FDA65AA160
!beg �������:20FDA65AA161
        SPW_0(172405)=.false.	!L_(394) O
!end �������:20FDA65AA161
!beg �������:20FDA65AA162
        SPW_0(172403)=.false.	!L_(392) O
!end �������:20FDA65AA162
!beg �������:20FDA65AA163
        SPW_0(172401)=.false.	!L_(390) O
!end �������:20FDA65AA163
!beg �������:20FDA65AA164
        SPW_0(172369)=.false.	!L_(358) O
!end �������:20FDA65AA164
!beg ����:20FDA60AE408
        SPW_0(174261)=.false.	!L_(2250) O
!end ����:20FDA60AE408
!beg �������:20FDA62AA143
        SPW_0(172553)=.false.	!L_(542) O
!end �������:20FDA62AA143
!beg �������:20FDA62AA144
        SPW_0(172531)=.false.	!L_(520) O
!end �������:20FDA62AA144
!beg �������:20FDA62AA145
        SPW_0(172551)=.false.	!L_(540) O
!end �������:20FDA62AA145
!beg ����:20FDA60AE413
        SPW_0(174257)=.false.	!L_(2246) O
!end ����:20FDA60AE413
!beg �������:20FDA62AA150
        SPW_0(172511)=.false.	!L_(500) O
!end �������:20FDA62AA150
!beg �������:20FDA62AA151
        SPW_0(172509)=.false.	!L_(498) O
!end �������:20FDA62AA151
!beg �������:20FDA62AA152
        SPW_0(172507)=.false.	!L_(496) O
!end �������:20FDA62AA152
!beg �������:20FDA62AA153
        SPW_0(172361)=.false.	!L_(350) O
!end �������:20FDA62AA153
!beg �������:20FDA62AA154
        SPW_0(172359)=.false.	!L_(348) O
!end �������:20FDA62AA154
!beg �������:20FDA62AA155
        SPW_0(172505)=.false.	!L_(494) O
!end �������:20FDA62AA155
!beg �������:20FDA62AA156
        SPW_0(172503)=.false.	!L_(492) O
!end �������:20FDA62AA156
!beg �������:20FDA62AA157
        SPW_0(172437)=.false.	!L_(426) O
!end �������:20FDA62AA157
!beg �������:20FDA62AA158
        SPW_0(172435)=.false.	!L_(424) O
!end �������:20FDA62AA158
!beg �������:20FDA62AA159
        SPW_0(172439)=.false.	!L_(428) O
!end �������:20FDA62AA159
!beg �������:20FDA62AA160
        SPW_0(172441)=.false.	!L_(430) O
!end �������:20FDA62AA160
!beg �������:20FDA62AA161
        SPW_0(172447)=.false.	!L_(436) O
!end �������:20FDA62AA161
!beg �������:20FDA62AA162
        SPW_0(172445)=.false.	!L_(434) O
!end �������:20FDA62AA162
!beg �������:20FDA62AA163
        SPW_0(172443)=.false.	!L_(432) O
!end �������:20FDA62AA163
!beg �������:20FDA62AA164
        SPW_0(172375)=.false.	!L_(364) O
!end �������:20FDA62AA164
!beg �������:20FDA65AA206
        SPW_0(172310)=.false.	!L_(299) O
!end �������:20FDA65AA206
!beg �������:20FDA65AA207
        SPW_0(172308)=.false.	!L_(297) O
!end �������:20FDA65AA207
!beg �������:20FDA65AA208
        SPW_0(172306)=.false.	!L_(295) O
!end �������:20FDA65AA208
!beg �������:20FDA65AA209
        SPW_0(172304)=.false.	!L_(293) O
!end �������:20FDA65AA209
!beg �������:20FDA62AA206
        SPW_0(172334)=.false.	!L_(323) O
!end �������:20FDA62AA206
!beg �������:20FDA62AA207
        SPW_0(172332)=.false.	!L_(321) O
!end �������:20FDA62AA207
!beg �������:20FDA62AA208
        SPW_0(172330)=.false.	!L_(319) O
!end �������:20FDA62AA208
!beg �������:20FDA62AA209
        SPW_0(172328)=.false.	!L_(317) O
!end �������:20FDA62AA209
       end

       subroutine FDA70_ConInQ
       end

       subroutine FDA70_ConIn
       LOGICAL*1 SPW_0(0:196607)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:49151)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:65535)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:16383)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:28671)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:7167)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_6(0:28671)
       Common/SPW_6/ SPW_6 !
       integer*4 SPW_6_4(0:7167)
       equivalence(SPW_6,SPW_6_4)
        Call FDA70_ConInQ
!beg ���������3:20FDA73AE003
        SPW_4(16062)=.false.	!L_(59) O
!end ���������3:20FDA73AE003
!beg ���������8:20FDA71AE801
        SPW_4(16042)=.false.	!L_(39) O
!end ���������8:20FDA71AE801
!beg ����:20FDA71AE202
        SPW_4(16025)=.false.	!L_(22) O
!end ����:20FDA71AE202
!beg ��1-����:20FDA71AE202
        SPW_4(16036)=.false.	!L_(33) O
!end ��1-����:20FDA71AE202
!beg ���������:20FDA71CU001KN01
        SPW_4(16089)=.false.	!L_(86) O
!end ���������:20FDA71CU001KN01
!beg ���������8:20FDA73AE003
        SPW_4(16057)=.false.	!L_(54) O
!end ���������8:20FDA73AE003
!beg ���������2:20FDA71AE202
        SPW_4(16033)=.false.	!L_(30) O
!end ���������2:20FDA71AE202
!beg ���������5:20FDA74AE001
        SPW_4(16075)=.false.	!L_(72) O
!end ���������5:20FDA74AE001
!beg ���������7:20FDA71AE202
        SPW_4(16028)=.false.	!L_(25) O
!end ���������7:20FDA71AE202
!beg ���������:20FDA71AE801
        SPW_4(16037)=.false.	!L_(34) O
!end ���������:20FDA71AE801
!beg ���������4:20FDA71AE801
        SPW_4(16046)=.false.	!L_(43) O
!end ���������4:20FDA71AE801
!beg ���������-�����:20FDA73AE003
        SPW_4(16054)=.false.	!L_(51) O
!end ���������-�����:20FDA73AE003
!beg ���������:20FDA73AE003
        SPW_4(16052)=.false.	!L_(49) O
!end ���������:20FDA73AE003
!beg ���������10:20FDA74AE001
        SPW_4(16080)=.false.	!L_(77) O
!end ���������10:20FDA74AE001
!beg ���������4:20FDA73AE003
        SPW_4(16061)=.false.	!L_(58) O
!end ���������4:20FDA73AE003
!beg ���������9:20FDA71AE801
        SPW_4(16041)=.false.	!L_(38) O
!end ���������9:20FDA71AE801
!beg ������:20FDA71AE801
        SPW_4(16038)=.false.	!L_(35) O
!end ������:20FDA71AE801
!beg ��������:20FDA71AM001
        SPW_4(16020)=.false.	!L_(17) O
!end ��������:20FDA71AM001
!beg ���������1:20FDA74AE001
        SPW_4(16079)=.false.	!L_(76) O
!end ���������1:20FDA74AE001
!beg ���������9:20FDA73AE003
        SPW_4(16056)=.false.	!L_(53) O
!end ���������9:20FDA73AE003
!beg ���������3:20FDA71AE202
        SPW_4(16032)=.false.	!L_(29) O
!end ���������3:20FDA71AE202
!beg ������:20FDA73AE003
        SPW_4(16053)=.false.	!L_(50) O
!end ������:20FDA73AE003
!beg ���������6:20FDA74AE001
        SPW_4(16074)=.false.	!L_(71) O
!end ���������6:20FDA74AE001
!beg ���������8:20FDA71AE202
        SPW_4(16027)=.false.	!L_(24) O
!end ���������8:20FDA71AE202
!beg ���� � ��������������:20FDA71AE801
        SPW_4(16048)=.false.	!L_(45) O
!end ���� � ��������������:20FDA71AE801
!beg �������:20FDA71AE701KE01
        SPW_4(16083)=.false.	!L_(80) O
!end �������:20FDA71AE701KE01
!beg �������:20FDA71AE701KE02
        SPW_4(16017)=.false.	!L_(14) O
!end �������:20FDA71AE701KE02
!beg ����-������:20FDA71AE801
        SPW_4(16051)=.false.	!L_(48) O
!end ����-������:20FDA71AE801
!beg ���������5:20FDA71AE801
        SPW_4(16045)=.false.	!L_(42) O
!end ���������5:20FDA71AE801
!beg �������:20FDA71AE701KE01
        SPW_4(16082)=.false.	!L_(79) O
!end �������:20FDA71AE701KE01
!beg �������:20FDA71AE701KE02
        SPW_4(16016)=.false.	!L_(13) O
!end �������:20FDA71AE701KE02
!beg ���������5:20FDA73AE003
        SPW_4(16060)=.false.	!L_(57) O
!end ���������5:20FDA73AE003
!beg ���������-�����������:20FDA73AE003
        SPW_4(16064)=.false.	!L_(61) O
!end ���������-�����������:20FDA73AE003
!beg ����:20FDA74AE001
        SPW_4(16070)=.false.	!L_(67) O
!end ����:20FDA74AE001
!beg ���������2:20FDA74AE001
        SPW_4(16078)=.false.	!L_(75) O
!end ���������2:20FDA74AE001
!beg ���������:20FDA71AJ002
        SPW_4(16019)=.false.	!L_(16) O
!end ���������:20FDA71AJ002
!beg ���������:20FDA71AE202
        SPW_4(16022)=.false.	!L_(19) O
!end ���������:20FDA71AE202
!beg ���������4:20FDA71AE202
        SPW_4(16031)=.false.	!L_(28) O
!end ���������4:20FDA71AE202
!beg ���������7:20FDA74AE001
        SPW_4(16073)=.false.	!L_(70) O
!end ���������7:20FDA74AE001
!beg ���������10:20FDA71AE801
        SPW_4(16050)=.false.	!L_(47) O
!end ���������10:20FDA71AE801
!beg ���������9:20FDA71AE202
        SPW_4(16026)=.false.	!L_(23) O
!end ���������9:20FDA71AE202
!beg ������:20FDA71AE202
        SPW_4(16023)=SPW_0(154505)	!L_(20) O L_(414)
!end ������:20FDA71AE202
!beg ���������10:20FDA73AE003
        SPW_4(16065)=.false.	!L_(62) O
!end ���������10:20FDA73AE003
!beg ��1-�����:20FDA71AE202
        SPW_4(16024)=.false.	!L_(21) O
!end ��1-�����:20FDA71AE202
!beg ���������6:20FDA71AE801
        SPW_4(16044)=.false.	!L_(41) O
!end ���������6:20FDA71AE801
!beg ����-�����:20FDA71AE801
        SPW_4(16039)=.false.	!L_(36) O
!end ����-�����:20FDA71AE801
!beg ��������:20FDA72CU001KN01
        SPW_4(16086)=.false.	!L_(83) O
!end ��������:20FDA72CU001KN01
!beg ���������6:20FDA73AE003
        SPW_4(16059)=.false.	!L_(56) O
!end ���������6:20FDA73AE003
!beg ���� ��� �����������:20FDA71AE801
        SPW_4(16049)=.false.	!L_(46) O
!end ���� ��� �����������:20FDA71AE801
!beg ���������3:20FDA74AE001
        SPW_4(16077)=.false.	!L_(74) O
!end ���������3:20FDA74AE001
!beg ���������5:20FDA71AE202
        SPW_4(16030)=.false.	!L_(27) O
!end ���������5:20FDA71AE202
!beg ��1-�����-���:20FDA71AE202
        SPW_4(16034)=.false.	!L_(31) O
!end ��1-�����-���:20FDA71AE202
!beg ���������8:20FDA74AE001
        SPW_4(16072)=.false.	!L_(69) O
!end ���������8:20FDA74AE001
!beg ���������:20FDA72CU001KN01
        SPW_4(16087)=.false.	!L_(84) O
!end ���������:20FDA72CU001KN01
!beg ����:20FDA71AE801
        SPW_4(16040)=.false.	!L_(37) O
!end ����:20FDA71AE801
!beg ������:20FDA74AE001
        SPW_4(16069)=.false.	!L_(66) O
!end ������:20FDA74AE001
!beg ����:20FDA73AE003
        SPW_4(16055)=.false.	!L_(52) O
!end ����:20FDA73AE003
!beg �����:20FDA74AE001
        SPW_4(16081)=.false.	!L_(78) O
!end �����:20FDA74AE001
!beg ���������2:20FDA73AE003
        SPW_4(16063)=.false.	!L_(60) O
!end ���������2:20FDA73AE003
!beg ��������:20FDA71AJ002
        SPW_4(16018)=.false.	!L_(15) O
!end ��������:20FDA71AJ002
!beg ���������7:20FDA71AE801
        SPW_4(16043)=.false.	!L_(40) O
!end ���������7:20FDA71AE801
!beg ���������10:20FDA71AE202
        SPW_4(16035)=.false.	!L_(32) O
!end ���������10:20FDA71AE202
!beg ���������-����:20FDA73AE003
        SPW_4(16066)=.false.	!L_(63) O
!end ���������-����:20FDA73AE003
!beg ���������7:20FDA73AE003
        SPW_4(16058)=.false.	!L_(55) O
!end ���������7:20FDA73AE003
!beg ���������:20FDA74AE001
        SPW_4(16067)=.false.	!L_(64) O
!end ���������:20FDA74AE001
!beg �������:20FDA71AA001
        SPW_4(16090)=.false.	!L_(87) O
!end �������:20FDA71AA001
!beg �������:20FDA73AE403KA01
        SPW_4(16085)=.false.	!L_(82) O
!end �������:20FDA73AE403KA01
!beg ���������4:20FDA74AE001
        SPW_4(16076)=.false.	!L_(73) O
!end ���������4:20FDA74AE001
!beg ���������6:20FDA71AE202
        SPW_4(16029)=.false.	!L_(26) O
!end ���������6:20FDA71AE202
!beg ���������:20FDA71AM001
        SPW_4(16021)=.false.	!L_(18) O
!end ���������:20FDA71AM001
!beg ���������9:20FDA74AE001
        SPW_4(16071)=.false.	!L_(68) O
!end ���������9:20FDA74AE001
!beg ��������:20FDA71CU001KN01
        SPW_4(16088)=.false.	!L_(85) O
!end ��������:20FDA71CU001KN01
!beg ������:20FDA74AE001
        SPW_4(16068)=.false.	!L_(65) O
!end ������:20FDA74AE001
!beg �������:20FDA73AE403KA01
        SPW_4(16084)=.false.	!L_(81) O
!end �������:20FDA73AE403KA01
!beg ���������3:20FDA71AE801
        SPW_4(16047)=.false.	!L_(44) O
!end ���������3:20FDA71AE801
!beg �������:20FDA71AA001
        SPW_4(16091)=.false.	!L_(88) O
!end �������:20FDA71AA001
       end

       subroutine FDA90_ConInQ
       end

       subroutine FDA90_ConIn
       LOGICAL*1 SPW_0(0:196607)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:49151)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:65535)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:16383)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:28671)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:7167)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_6(0:28671)
       Common/SPW_6/ SPW_6 !
       integer*4 SPW_6_4(0:7167)
       equivalence(SPW_6,SPW_6_4)
        Call FDA90_ConInQ
!beg ������:20FDA91AE002KE01
        SPW_0(154282)=.false.	!L_(191) O
!end ������:20FDA91AE002KE01
!beg ��������:20FDA91CM001KN01
        SPW_0(154341)=.false.	!L_(250) O
!end ��������:20FDA91CM001KN01
!beg ����������:20FDA91AE001KE01
        SPW_0(154291)=.true.	!L_(200) A
!end ����������:20FDA91AE001KE01
!beg �����:20FDA91AE001KE01
        SPW_0(154290)=.false.	!L_(199) O
!end �����:20FDA91AE001KE01
!beg ���������:20FDA91AE003KE01
        SPW_0(154293)=.false.	!L_(202) O
!end ���������:20FDA91AE003KE01
!beg ���������:20FDA91AE002KE01
        SPW_0(154285)=.true.	!L_(194) A
!end ���������:20FDA91AE002KE01
!beg ���������:20FDA91AE006
        SPW_0(154321)=.false.	!L_(230) O
!end ���������:20FDA91AE006
!beg �����:FDA70_LIFT
        SPW_0(154254)=.false.	!L_(163) O
!end �����:FDA70_LIFT
!beg ���������:20FDA91AE007
        SPW_0(154307)=.false.	!L_(216) O
!end ���������:20FDA91AE007
!beg ���������:20FDA91AE008
        SPW_0(154300)=.false.	!L_(209) O
!end ���������:20FDA91AE008
!beg ���������:20FDA91AE009
        SPW_0(154314)=.false.	!L_(223) O
!end ���������:20FDA91AE009
!beg ����:20FDA91AE002KE01
        SPW_0(154281)=.false.	!L_(190) O
!end ����:20FDA91AE002KE01
!beg ���������:20FDA91AE012
        SPW_0(154244)=.false.	!L_(153) O
!end ���������:20FDA91AE012
!beg ���������:20FDA91AE014
        SPW_0(154328)=.false.	!L_(237) O
!end ���������:20FDA91AE014
!beg ���������:20FDA91CM001KN01
        SPW_0(154342)=.false.	!L_(251) O
!end ���������:20FDA91CM001KN01
!beg ���������:20FDA90AE999
        SPW_0(154258)=.false.	!L_(167) O
!end ���������:20FDA90AE999
!beg ������:20FDA91AE006
        SPW_0(154322)=.false.	!L_(231) O
!end ������:20FDA91AE006
!beg ������:20FDA91AE007
        SPW_0(154308)=.false.	!L_(217) O
!end ������:20FDA91AE007
!beg ������:20FDA91AE008
        SPW_0(154301)=.false.	!L_(210) O
!end ������:20FDA91AE008
!beg ������:20FDA91AE009
        SPW_0(154315)=.false.	!L_(224) O
!end ������:20FDA91AE009
!beg ������:20FDA91AE012
        SPW_0(154245)=.false.	!L_(154) O
!end ������:20FDA91AE012
!beg ������:20FDA91AE014
        SPW_0(154329)=.false.	!L_(238) O
!end ������:20FDA91AE014
!beg ��������:20FDA91CU002KN01
        SPW_0(154335)=.false.	!L_(244) O
!end ��������:20FDA91CU002KN01
!beg ������:20FDA91AE001KE01
        SPW_0(154289)=.false.	!L_(198) O
!end ������:20FDA91AE001KE01
!beg ����������:20FDA91AE005KE01
        SPW_0(154277)=.true.	!L_(186) A
!end ����������:20FDA91AE005KE01
!beg ������:20FDA90AE999
        SPW_0(154259)=.false.	!L_(168) O
!end ������:20FDA90AE999
!beg �����:20FDA91AE005KE01
        SPW_0(154276)=.false.	!L_(185) O
!end �����:20FDA91AE005KE01
!beg ���������:20FDA91AE002KE01
        SPW_0(154279)=.false.	!L_(188) O
!end ���������:20FDA91AE002KE01
!beg ����:FDA70_LIFT
        SPW_0(154255)=.false.	!L_(164) O
!end ����:FDA70_LIFT
!beg ������:20FDA91AE003KE01
        SPW_0(154294)=.false.	!L_(203) O
!end ������:20FDA91AE003KE01
!beg ���������:20FDA91AE001KE01
        SPW_0(154292)=.true.	!L_(201) A
!end ���������:20FDA91AE001KE01
!beg ����������:20FDA91AE018KE01
        SPW_0(154270)=.true.	!L_(179) A
!end ����������:20FDA91AE018KE01
!beg �����:20FDA91AE018KE01
        SPW_0(154269)=.false.	!L_(178) O
!end �����:20FDA91AE018KE01
!beg ����:20FDA91AE001KE01
        SPW_0(154288)=.false.	!L_(197) O
!end ����:20FDA91AE001KE01
!beg ���������:20FDA91CU002KN01
        SPW_0(154336)=.false.	!L_(245) O
!end ���������:20FDA91CU002KN01
!beg ��������:20FDA91CU001KN01
        SPW_0(154339)=.false.	!L_(248) O
!end ��������:20FDA91CU001KN01
!beg �����:20FDA91AE006
        SPW_0(154324)=.false.	!L_(233) O
!end �����:20FDA91AE006
!beg �����:20FDA91AE007
        SPW_0(154310)=.false.	!L_(219) O
!end �����:20FDA91AE007
!beg �����:20FDA91AE008
        SPW_0(154303)=.false.	!L_(212) O
!end �����:20FDA91AE008
!beg �����:20FDA91AE009
        SPW_0(154317)=.false.	!L_(226) O
!end �����:20FDA91AE009
!beg ����:20FDA91AE006
        SPW_0(154325)=.false.	!L_(234) O
!end ����:20FDA91AE006
!beg ����:20FDA91AE007
        SPW_0(154311)=.false.	!L_(220) O
!end ����:20FDA91AE007
!beg ����:20FDA91AE008
        SPW_0(154304)=.false.	!L_(213) O
!end ����:20FDA91AE008
!beg ������:20FDA91AE005KE01
        SPW_0(154275)=.false.	!L_(184) O
!end ������:20FDA91AE005KE01
!beg ����:20FDA91AE009
        SPW_0(154318)=.false.	!L_(227) O
!end ����:20FDA91AE009
!beg �����:20FDA91AE012
        SPW_0(154247)=.false.	!L_(156) O
!end �����:20FDA91AE012
!beg �����:20FDA91AE014
        SPW_0(154331)=.false.	!L_(240) O
!end �����:20FDA91AE014
!beg ����:20FDA91AE012
        SPW_0(154248)=.false.	!L_(157) O
!end ����:20FDA91AE012
!beg ����:20FDA91AE014
        SPW_0(154332)=.false.	!L_(241) O
!end ����:20FDA91AE014
!beg ���������:20FDA91AE001KE01
        SPW_0(154286)=.false.	!L_(195) O
!end ���������:20FDA91AE001KE01
!beg �����:20FDA90AE999
        SPW_0(154261)=.false.	!L_(170) O
!end �����:20FDA90AE999
!beg ����:20FDA90AE999
        SPW_0(154262)=.false.	!L_(171) O
!end ����:20FDA90AE999
!beg ������:20FDA91AE002KE01
        SPW_0(154280)=.false.	!L_(189) O
!end ������:20FDA91AE002KE01
!beg ������:20FDA91AE018KE01
        SPW_0(154268)=.false.	!L_(177) O
!end ������:20FDA91AE018KE01
!beg ���������:20FDA91AE005KE01
        SPW_0(154278)=.true.	!L_(187) A
!end ���������:20FDA91AE005KE01
!beg ���������:20FDA91AE006
        SPW_0(154327)=.true.	!L_(236) A
!end ���������:20FDA91AE006
!beg ���������:20FDA91AE007
        SPW_0(154313)=.true.	!L_(222) A
!end ���������:20FDA91AE007
!beg ���������:20FDA91AE008
        SPW_0(154306)=.true.	!L_(215) A
!end ���������:20FDA91AE008
!beg ���������:20FDA91AE009
        SPW_0(154320)=.true.	!L_(229) A
!end ���������:20FDA91AE009
!beg ���������:FDA70_LIFT
        SPW_0(154256)=.true.	!L_(165) A
!end ���������:FDA70_LIFT
!beg ���������:20FDA91AE012
        SPW_0(154250)=.true.	!L_(159) A
!end ���������:20FDA91AE012
!beg ���������:20FDA91AE014
        SPW_0(154334)=.true.	!L_(243) A
!end ���������:20FDA91AE014
!beg ���������:20FDA91CU001KN01
        SPW_0(154340)=.false.	!L_(249) O
!end ���������:20FDA91CU001KN01
!beg ����:20FDA91AE005KE01
        SPW_0(154274)=.false.	!L_(183) O
!end ����:20FDA91AE005KE01
!beg ���������:20FDA90AE999
        SPW_0(154264)=.true.	!L_(173) A
!end ���������:20FDA90AE999
!beg ���������:20FDA91AE018KE01
        SPW_0(154271)=.true.	!L_(180) A
!end ���������:20FDA91AE018KE01
!beg ����:20FDA91AE018KE01
        SPW_0(154267)=.false.	!L_(176) O
!end ����:20FDA91AE018KE01
!beg ����������:20FDA91AE003KE01
        SPW_0(154298)=.true.	!L_(207) A
!end ����������:20FDA91AE003KE01
!beg ������:FDA70_LIFT
        SPW_0(154252)=.false.	!L_(161) O
!end ������:FDA70_LIFT
!beg �����:20FDA91AE003KE01
        SPW_0(154297)=.false.	!L_(206) O
!end �����:20FDA91AE003KE01
!beg ���������:20FDA91AE005KE01
        SPW_0(154272)=.false.	!L_(181) O
!end ���������:20FDA91AE005KE01
!beg ������:20FDA91AE001KE01
        SPW_0(154287)=.false.	!L_(196) O
!end ������:20FDA91AE001KE01
!beg ����:20FDA91AE006
        SPW_0(154323)=.false.	!L_(232) O
!end ����:20FDA91AE006
!beg ����:20FDA91AE007
        SPW_0(154309)=.false.	!L_(218) O
!end ����:20FDA91AE007
!beg ����:20FDA91AE008
        SPW_0(154302)=.false.	!L_(211) O
!end ����:20FDA91AE008
!beg ����:20FDA91AE009
        SPW_0(154316)=.false.	!L_(225) O
!end ����:20FDA91AE009
!beg ����:20FDA91AE012
        SPW_0(154246)=.false.	!L_(155) O
!end ����:20FDA91AE012
!beg ����:20FDA91AE014
        SPW_0(154330)=.false.	!L_(239) O
!end ����:20FDA91AE014
!beg ���������:20FDA91AE018KE01
        SPW_0(154265)=.false.	!L_(174) O
!end ���������:20FDA91AE018KE01
!beg ����:20FDA90AE999
        SPW_0(154260)=.false.	!L_(169) O
!end ����:20FDA90AE999
!beg ����:FDA70_LIFT
        SPW_0(154253)=.false.	!L_(162) O
!end ����:FDA70_LIFT
!beg ���������:20FDA91AE006
        SPW_0(154326)=.true.	!L_(235) A
!end ���������:20FDA91AE006
!beg ���������:20FDA91AE007
        SPW_0(154312)=.true.	!L_(221) A
!end ���������:20FDA91AE007
!beg ���������:20FDA91AE008
        SPW_0(154305)=.true.	!L_(214) A
!end ���������:20FDA91AE008
!beg ���������:20FDA91AE009
        SPW_0(154319)=.true.	!L_(228) A
!end ���������:20FDA91AE009
!beg ���������:20FDA91AE012
        SPW_0(154249)=.true.	!L_(158) A
!end ���������:20FDA91AE012
!beg ���������:20FDA91AE014
        SPW_0(154333)=.true.	!L_(242) A
!end ���������:20FDA91AE014
!beg ���������:FDA70_LIFT
        SPW_0(154251)=.false.	!L_(160) O
!end ���������:FDA70_LIFT
!beg ������:20FDA91AE003KE01
        SPW_0(154296)=.false.	!L_(205) O
!end ������:20FDA91AE003KE01
!beg ��������:20FDA91CM002KN01
        SPW_0(154337)=.false.	!L_(246) O
!end ��������:20FDA91CM002KN01
!beg ����������:20FDA91AE002KE01
        SPW_0(154284)=.true.	!L_(193) A
!end ����������:20FDA91AE002KE01
!beg ���������:20FDA90AE999
        SPW_0(154263)=.true.	!L_(172) A
!end ���������:20FDA90AE999
!beg �����:20FDA91AE002KE01
        SPW_0(154283)=.false.	!L_(192) O
!end �����:20FDA91AE002KE01
!beg ������:20FDA91AE005KE01
        SPW_0(154273)=.false.	!L_(182) O
!end ������:20FDA91AE005KE01
!beg ���������:20FDA91AE003KE01
        SPW_0(154299)=.true.	!L_(208) A
!end ���������:20FDA91AE003KE01
!beg ����:20FDA91AE003KE01
        SPW_0(154295)=.false.	!L_(204) O
!end ����:20FDA91AE003KE01
!beg ���������:20FDA91CM002KN01
        SPW_0(154338)=.false.	!L_(247) O
!end ���������:20FDA91CM002KN01
!beg ������:20FDA91AE018KE01
        SPW_0(154266)=.false.	!L_(175) O
!end ������:20FDA91AE018KE01
!beg ���������:FDA70_LIFT
        SPW_0(154257)=.true.	!L_(166) A
!end ���������:FDA70_LIFT
       end
