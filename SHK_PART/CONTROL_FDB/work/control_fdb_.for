       SUBROUTINE SPW_Init_tab(iTabVer)
       integer*4 iTabVer
       TYPE TSubr
         sequence
         integer*4 addr
         integer*4 period
         integer*4 phase
         integer*4 object
         integer*4 allowed
         integer*4 ncall
         real*8    time
         integer*4 individualScale
         integer*4 rezerv(2),flags,CPUTime(2)
         character(LEN=32) name
       END TYPE TSubr

       TYPE(TSubr) SPW_Table_Sub(9)
       Common/SPW_Table_Sub/ SPW_Table_Sub
       external FDB
       Data SPW_Table_Sub(1)/TSubr(0,1,0,2,0,0,0.,0,0,8,0,
     &   'FDB')/
       external FDB10
       Data SPW_Table_Sub(2)/TSubr(0,1,0,3,0,0,0.,0,0,8,0,
     &   'FDB10')/
       external FDB20
       Data SPW_Table_Sub(3)/TSubr(0,1,0,4,0,0,0.,0,0,8,0,
     &   'FDB20')/
       external FDB30
       Data SPW_Table_Sub(4)/TSubr(0,1,0,5,0,0,0.,0,0,8,0,
     &   'FDB30')/
       external FDB40
       Data SPW_Table_Sub(5)/TSubr(0,1,0,6,0,0,0.,0,0,8,0,
     &   'FDB40')/
       external FDB50
       Data SPW_Table_Sub(6)/TSubr(0,1,0,7,0,0,0.,0,0,8,0,
     &   'FDB50')/
       external FDB60
       Data SPW_Table_Sub(7)/TSubr(0,1,0,8,0,0,0.,0,0,8,0,
     &   'FDB60')/
       external FDB70
       Data SPW_Table_Sub(8)/TSubr(0,1,0,9,0,0,0.,0,0,8,0,
     &   'FDB70')/
       external FDB90
       Data SPW_Table_Sub(9)/TSubr(0,1,0,10,0,0,0.,0,0,8,0,
     &   'FDB90')/

       LOGICAL*1 SPW_0(0:20479)
       Common/SPW_0/ SPW_0 !
       CHARACTER*1 SPW_1(0:2047)
       Common/SPW_1/ SPW_1 !
       LOGICAL*1 SPW_2(0:4095)
       Common/SPW_2/ SPW_2 !
       CHARACTER*1 SPW_3(0:1023)
       Common/SPW_3/ SPW_3 !
       LOGICAL*1 SPW_4(0:20479)
       Common/SPW_4/ SPW_4 !
       CHARACTER*1 SPW_5(0:1279)
       Common/SPW_5/ SPW_5 !
       LOGICAL*1 SPW_6(0:3071)
       Common/SPW_6/ SPW_6 !
       CHARACTER*1 SPW_7(0:319)
       Common/SPW_7/ SPW_7 !
       LOGICAL*1 SPW_8(0:3071)
       Common/SPW_8/ SPW_8 !
       CHARACTER*1 SPW_9(0:1023)
       Common/SPW_9/ SPW_9 !
       LOGICAL*1 SPW_10(0:20479)
       Common/SPW_10/ SPW_10 !
       CHARACTER*1 SPW_11(0:639)
       Common/SPW_11/ SPW_11 !
       LOGICAL*1 SPW_12(0:1535)
       Common/SPW_12/ SPW_12 !
       CHARACTER*1 SPW_13(0:319)
       Common/SPW_13/ SPW_13 !
       LOGICAL*1 SPW_14(0:5119)
       Common/SPW_14/ SPW_14 !
       CHARACTER*1 SPW_15(0:191)
       Common/SPW_15/ SPW_15 !
       LOGICAL*1 SPW_16(0:14335)
       Common/SPW_16/ SPW_16 !
       CHARACTER*1 SPW_17(0:1279)
       Common/SPW_17/ SPW_17 !
       LOGICAL*1 SPW_18(0:15)
       Common/SPW_18/ SPW_18 !
       LOGICAL*1 SPW_19(0:15)
       Common/SPW_19/ SPW_19 !
       LOGICAL*1 SPW_20(0:15)
       Common/SPW_20/ SPW_20 !
       LOGICAL*1 SPW_21(0:15)
       Common/SPW_21/ SPW_21 !
       LOGICAL*1 SPW_22(0:15)
       Common/SPW_22/ SPW_22 !
       LOGICAL*1 SPW_23(0:31)
       Common/SPW_23/ SPW_23 !

       real*4 info_kwant
       integer*4 info_siz
       common/spw_info/ info_siz,info_kwant
       !MS$ATTRIBUTES DLLEXPORT::spw_info
       Data info_siz/8/,info_kwant/0.125/
       integer*4 dispStateHdr
       integer*4 stepCounter
       real*8    subInfo(36)
       common/spw_dispState/ dispStateHdr,stepCounter,subInfo

       character*(32) ver(10)
       integer*4 nver,versiz
       common/spw_versions/ nver,versiz,ver
       !MS$ATTRIBUTES DLLEXPORT::spw_versions
       Data nver/10/,versiz/32/
       Data ver(1)/''/
       Data ver(2)/'v_16A5ED16_FDB'/
       Data ver(3)/'v_83BECB3D_FDB10'/
       Data ver(4)/'v_9B42E865_FDB20'/
       Data ver(5)/'v_CCA77F0A_FDB30'/
       Data ver(6)/'v_A5F661D1_FDB40'/
       Data ver(7)/'v_6D0FE6BB_FDB50'/
       Data ver(8)/'v_958C6E44_FDB60'/
       Data ver(9)/'v_DC7FB7D8_FDB70'/
       Data ver(10)/'v_3D24EABA_FDB90'/

       CALL SPW_SET_SIGN(787968374,0,-1457478088)
       iTabVer=iTabVer*1000+1088

       CALL SPW_SET_UIDTR(-580556338,-2015897803)
       CALL SPW_SET_KWANT(REAL(0.125))
       CALL SPW_SET_NCMN(24)
       CALL SPW_ADD_CMN(LOC(SPW_0),18227)
       CALL SPW_ADD_CMN(LOC(SPW_1),2010)
       CALL SPW_ADD_CMN(LOC(SPW_2),3969)
       CALL SPW_ADD_CMN(LOC(SPW_3),1002)
       CALL SPW_ADD_CMN(LOC(SPW_4),20281)
       CALL SPW_ADD_CMN(LOC(SPW_5),1148)
       CALL SPW_ADD_CMN(LOC(SPW_6),2749)
       CALL SPW_ADD_CMN(LOC(SPW_7),310)
       CALL SPW_ADD_CMN(LOC(SPW_8),2563)
       CALL SPW_ADD_CMN(LOC(SPW_9),988)
       CALL SPW_ADD_CMN(LOC(SPW_10),20011)
       CALL SPW_ADD_CMN(LOC(SPW_11),622)
       CALL SPW_ADD_CMN(LOC(SPW_12),1502)
       CALL SPW_ADD_CMN(LOC(SPW_13),300)
       CALL SPW_ADD_CMN(LOC(SPW_14),4936)
       CALL SPW_ADD_CMN(LOC(SPW_15),188)
       CALL SPW_ADD_CMN(LOC(SPW_16),13003)
       CALL SPW_ADD_CMN(LOC(SPW_17),1050)
       CALL SPW_ADD_CMN(LOC(SPW_18),8)
       CALL SPW_ADD_CMN(LOC(SPW_19),2)
       CALL SPW_ADD_CMN(LOC(SPW_20),6)
       CALL SPW_ADD_CMN(LOC(SPW_21),7)
       CALL SPW_ADD_CMN(LOC(SPW_22),8)
       CALL SPW_ADD_CMN(LOC(SPW_23),17)
       CALL SPW_SET_NAMES(48648,
     +    'control_fdb_.vpt',
     +    'control_fdb_.87D7D735.vpt')
       SPW_Table_Sub(1)%addr=LOC(FDB)
       SPW_Table_Sub(1)%allowed=LOC(SPW_0)+7688
       SPW_Table_Sub(1)%individualScale=LOC(SPW_0)+408
       SPW_Table_Sub(2)%addr=LOC(FDB10)
       SPW_Table_Sub(2)%allowed=LOC(SPW_2)+2609
       SPW_Table_Sub(2)%individualScale=LOC(SPW_2)+384
       SPW_Table_Sub(3)%addr=LOC(FDB20)
       SPW_Table_Sub(3)%allowed=LOC(SPW_4)+19909
       SPW_Table_Sub(3)%individualScale=LOC(SPW_4)+1192
       SPW_Table_Sub(4)%addr=LOC(FDB30)
       SPW_Table_Sub(4)%allowed=LOC(SPW_6)+2747
       SPW_Table_Sub(4)%individualScale=LOC(SPW_6)+304
       SPW_Table_Sub(5)%addr=LOC(FDB40)
       SPW_Table_Sub(5)%allowed=LOC(SPW_8)+2224
       SPW_Table_Sub(5)%individualScale=LOC(SPW_8)+168
       SPW_Table_Sub(6)%addr=LOC(FDB50)
       SPW_Table_Sub(6)%allowed=LOC(SPW_10)+20010
       SPW_Table_Sub(6)%individualScale=LOC(SPW_10)+2776
       SPW_Table_Sub(7)%addr=LOC(FDB60)
       SPW_Table_Sub(7)%allowed=LOC(SPW_12)+1501
       SPW_Table_Sub(7)%individualScale=LOC(SPW_12)+128
       SPW_Table_Sub(8)%addr=LOC(FDB70)
       SPW_Table_Sub(8)%allowed=LOC(SPW_14)+4934
       SPW_Table_Sub(8)%individualScale=LOC(SPW_14)+496
       SPW_Table_Sub(9)%addr=LOC(FDB90)
       SPW_Table_Sub(9)%allowed=LOC(SPW_16)+13002
       SPW_Table_Sub(9)%individualScale=LOC(SPW_16)+1280
       CALL SPW_SET_SUB_LST(9,SPW_Table_Sub,stepCounter)
       end

       subroutine spw_initDisp
       integer*4 dispStateHdr
       integer*4 stepCounter
       real*8    subInfo(36)
       common/spw_dispState/ dispStateHdr,stepCounter,subInfo
       stepCounter=0
       subInfo=0
       end

       subroutine FDB_ConInQ
       end

       subroutine FDB_ConIn
       LOGICAL*1 SPW_0(0:20479)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:5119)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:4095)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:1023)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:20479)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:5119)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_8(0:3071)
       Common/SPW_8/ SPW_8 !
       integer*4 SPW_8_4(0:767)
       equivalence(SPW_8,SPW_8_4)
        Call FDB_ConInQ
!beg ����:20FDB50AW001
        SPW_0(16237)=.false.	!L_(17) O
!end ����:20FDB50AW001
!beg ����:20FDB50AW002
        SPW_0(16240)=.false.	!L_(20) O
!end ����:20FDB50AW002
!beg ������:20FDB50AE500
        SPW_0(16244)=SPW_4(20210)	!L_(24) O L_(284)
!end ������:20FDB50AE500
!beg ������:20FDB50AE501
        SPW_0(16262)=SPW_4(20021)	!L_(42) O L_(95)
!end ������:20FDB50AE501
!beg ������:20FDB50AE502
        SPW_0(16259)=SPW_4(20006)	!L_(39) O L_(80)
!end ������:20FDB50AE502
!beg ������:20FDB50AE503
        SPW_0(16256)=SPW_4(19991)	!L_(36) O L_(65)
!end ������:20FDB50AE503
!beg ������:20FDB50AE504
        SPW_0(16253)=SPW_4(19978)	!L_(33) O L_(52)
!end ������:20FDB50AE504
!beg ������:20FDB50AE505
        SPW_0(16250)=SPW_4(19960)	!L_(30) O L_(34)
!end ������:20FDB50AE505
!beg ������:20FDB50AE506
        SPW_0(16247)=SPW_4(20195)	!L_(27) O L_(269)
!end ������:20FDB50AE506
!beg �����:20FDB50AE500
        SPW_0(16245)=SPW_4(20207)	!L_(25) O L_(281)
!end �����:20FDB50AE500
!beg �����:20FDB50AE501
        SPW_0(16263)=SPW_4(20003)	!L_(43) O L_(77)
!end �����:20FDB50AE501
!beg �����:20FDB50AE502
        SPW_0(16260)=SPW_4(19999)	!L_(40) O L_(73)
!end �����:20FDB50AE502
!beg �����:20FDB50AE503
        SPW_0(16257)=SPW_4(19988)	!L_(37) O L_(62)
!end �����:20FDB50AE503
!beg �����:20FDB50AE504
        SPW_0(16254)=SPW_4(19970)	!L_(34) O L_(44)
!end �����:20FDB50AE504
!beg �����:20FDB50AE505
        SPW_0(16251)=SPW_4(19953)	!L_(31) O L_(27)
!end �����:20FDB50AE505
!beg �����:20FDB50AE506
        SPW_0(16248)=SPW_4(20187)	!L_(28) O L_(261)
!end �����:20FDB50AE506
!beg ������:20FDB50AW001
        SPW_0(16238)=.false.	!L_(18) O
!end ������:20FDB50AW001
!beg ������:20FDB50AW002
        SPW_0(16241)=.false.	!L_(21) O
!end ������:20FDB50AW002
!beg �����:20FDB50AW001
        SPW_0(16239)=.false.	!L_(19) O
!end �����:20FDB50AW001
!beg �����:20FDB50AW002
        SPW_0(16242)=.false.	!L_(22) O
!end �����:20FDB50AW002
!beg ���������:20FDB50AE401
        SPW_0(16234)=.true.	!L_(14) A
!end ���������:20FDB50AE401
!beg ���������:20FDB50AE403
        SPW_0(16229)=.true.	!L_(9) A
!end ���������:20FDB50AE403
!beg ����:20FDB50AE401
        SPW_0(16230)=SPW_4(20032)	!L_(10) O L_(106)
        SPW_0(16230)=SPW_0(16230).or.SPW_4(20056)	!L_(10) O L_(130)
        SPW_0(16230)=SPW_0(16230).or.SPW_4(20059)	!L_(10) O L_(133)
        SPW_0(16230)=SPW_0(16230).or.SPW_4(20062)	!L_(10) O L_(136)
        SPW_0(16230)=SPW_0(16230).or.SPW_4(20065)	!L_(10) O L_(139)
        SPW_0(16230)=SPW_0(16230).or.SPW_4(20068)	!L_(10) O L_(142)
        SPW_0(16230)=SPW_0(16230).or.SPW_4(20071)	!L_(10) O L_(145)
        SPW_0(16230)=SPW_0(16230).or.SPW_4(20074)	!L_(10) O L_(148)
        SPW_0(16230)=SPW_0(16230).or.SPW_4(20258)	!L_(10) O L_(332)
!end ����:20FDB50AE401
!beg ����:20FDB50AE403
        SPW_0(16225)=SPW_4(20276)	!L_(5) O L_(350)
!end ����:20FDB50AE403
!beg �������:20FDB50AB002
        SPW_0(16270)=SPW_4(20214)	!L_(50) O L_(288)
!end �������:20FDB50AB002
!beg �������:20FDB50AB003
        SPW_0(16268)=SPW_4(20200)	!L_(48) O L_(274)
!end �������:20FDB50AB003
!beg �������:20FDB50AB004
        SPW_0(16266)=SPW_4(20028)	!L_(46) O L_(102)
!end �������:20FDB50AB004
!beg �������:20FDB50AB005
        SPW_0(16264)=SPW_4(20013)	!L_(44) O L_(87)
!end �������:20FDB50AB005
!beg �������:20FDB50AB006
        SPW_0(16274)=SPW_4(19985)	!L_(54) O L_(59)
!end �������:20FDB50AB006
!beg �������:20FDB50AB007
        SPW_0(16272)=SPW_4(19967)	!L_(52) O L_(41)
!end �������:20FDB50AB007
!beg ����������:20FDB50AE401
        SPW_0(16233)=.true.	!L_(13) A
!end ����������:20FDB50AE401
!beg ����������:20FDB50AE403
        SPW_0(16228)=.true.	!L_(8) A
!end ����������:20FDB50AE403
!beg �������:20FDB23AE002
        SPW_0(16235)=.false.	!L_(15) O
!end �������:20FDB23AE002
!beg �������:20FDB50AB002
        SPW_0(16271)=SPW_4(20203)	!L_(51) O L_(277)
!end �������:20FDB50AB002
!beg �������:20FDB50AB003
        SPW_0(16269)=SPW_4(20192)	!L_(49) O L_(266)
!end �������:20FDB50AB003
!beg �������:20FDB50AB004
        SPW_0(16267)=SPW_4(20018)	!L_(47) O L_(92)
!end �������:20FDB50AB004
!beg �������:20FDB50AB005
        SPW_0(16265)=SPW_4(19996)	!L_(45) O L_(70)
!end �������:20FDB50AB005
!beg �������:20FDB50AB006
        SPW_0(16275)=SPW_4(19975)	!L_(55) O L_(49)
!end �������:20FDB50AB006
!beg �������:20FDB50AB007
        SPW_0(16273)=SPW_4(19957)	!L_(53) O L_(31)
!end �������:20FDB50AB007
!beg �������:20FDB23AE002
        SPW_0(16236)=.false.	!L_(16) O
!end �������:20FDB23AE002
!beg ����:20FDB50AE500
        SPW_0(16243)=.false.	!L_(23) O
!end ����:20FDB50AE500
!beg ����:20FDB50AE501
        SPW_0(16261)=.false.	!L_(41) O
!end ����:20FDB50AE501
!beg ����:20FDB50AE502
        SPW_0(16258)=.false.	!L_(38) O
!end ����:20FDB50AE502
!beg ����:20FDB50AE503
        SPW_0(16255)=.false.	!L_(35) O
!end ����:20FDB50AE503
!beg ����:20FDB50AE504
        SPW_0(16252)=.false.	!L_(32) O
!end ����:20FDB50AE504
!beg ����:20FDB50AE505
        SPW_0(16249)=.false.	!L_(29) O
!end ����:20FDB50AE505
!beg ����:20FDB50AE506
        SPW_0(16246)=.false.	!L_(26) O
!end ����:20FDB50AE506
!beg ������:20FDB50AE400
        SPW_0(16223)=SPW_4(20124)	!L_(3) O L_(198)
        SPW_0(16223)=SPW_0(16223).or.SPW_4(20276)	!L_(3) O L_(350)
!end ������:20FDB50AE400
!beg ������:20FDB50AE401
        SPW_0(16231)=SPW_4(20040)	!L_(11) O L_(114)
        SPW_0(16231)=SPW_0(16231).or.SPW_4(20103)	!L_(11) O L_(177)
        SPW_0(16231)=SPW_0(16231).or.SPW_4(20177)	!L_(11) O L_(251)
        SPW_0(16231)=SPW_0(16231).or.SPW_4(20234)	!L_(11) O L_(308)
        SPW_0(16231)=SPW_0(16231).or.SPW_4(20248)	!L_(11) O L_(322)
!end ������:20FDB50AE401
!beg ������:20FDB50AE402
        SPW_0(16221)=SPW_4(20159)	!L_(1) O L_(233)
        SPW_0(16221)=SPW_0(16221).or.SPW_4(20182)	!L_(1) O L_(256)
!end ������:20FDB50AE402
!beg ������:20FDB50AE403
        SPW_0(16226)=SPW_4(20045)	!L_(6) O L_(119)
        SPW_0(16226)=SPW_0(16226).or.SPW_4(20051)	!L_(6) O L_(125)
        SPW_0(16226)=SPW_0(16226).or.SPW_4(20094)	!L_(6) O L_(168)
        SPW_0(16226)=SPW_0(16226).or.SPW_4(20108)	!L_(6) O L_(182)
        SPW_0(16226)=SPW_0(16226).or.SPW_4(20127)	!L_(6) O L_(201)
        SPW_0(16226)=SPW_0(16226).or.SPW_4(20142)	!L_(6) O L_(216)
        SPW_0(16226)=SPW_0(16226).or.SPW_4(20164)	!L_(6) O L_(238)
        SPW_0(16226)=SPW_0(16226).or.SPW_4(20225)	!L_(6) O L_(299)
        SPW_0(16226)=SPW_0(16226).or.SPW_4(20239)	!L_(6) O L_(313)
        SPW_0(16226)=SPW_0(16226).or.SPW_4(20253)	!L_(6) O L_(327)
!end ������:20FDB50AE403
!beg �����:20FDB50AE400
        SPW_0(16224)=.false.	!L_(4) O
!end �����:20FDB50AE400
!beg �����:20FDB50AE401
        SPW_0(16232)=SPW_4(20080)	!L_(12) O L_(154)
        SPW_0(16232)=SPW_0(16232).or.SPW_4(20089)	!L_(12) O L_(163)
        SPW_0(16232)=SPW_0(16232).or.SPW_4(20113)	!L_(12) O L_(187)
        SPW_0(16232)=SPW_0(16232).or.SPW_4(20137)	!L_(12) O L_(211)
        SPW_0(16232)=SPW_0(16232).or.SPW_4(20152)	!L_(12) O L_(226)
        SPW_0(16232)=SPW_0(16232).or.SPW_4(20220)	!L_(12) O L_(294)
!end �����:20FDB50AE401
!beg �����:20FDB50AE402
        SPW_0(16222)=SPW_4(20168)	!L_(2) O L_(242)
!end �����:20FDB50AE402
!beg �����:20FDB50AE403
        SPW_0(16227)=SPW_4(20035)	!L_(7) O L_(109)
        SPW_0(16227)=SPW_0(16227).or.SPW_4(20084)	!L_(7) O L_(158)
        SPW_0(16227)=SPW_0(16227).or.SPW_4(20098)	!L_(7) O L_(172)
        SPW_0(16227)=SPW_0(16227).or.SPW_4(20118)	!L_(7) O L_(192)
        SPW_0(16227)=SPW_0(16227).or.SPW_4(20131)	!L_(7) O L_(205)
        SPW_0(16227)=SPW_0(16227).or.SPW_4(20146)	!L_(7) O L_(220)
        SPW_0(16227)=SPW_0(16227).or.SPW_4(20174)	!L_(7) O L_(248)
        SPW_0(16227)=SPW_0(16227).or.SPW_4(20229)	!L_(7) O L_(303)
        SPW_0(16227)=SPW_0(16227).or.SPW_4(20243)	!L_(7) O L_(317)
        SPW_0(16227)=SPW_0(16227).or.SPW_4(20258)	!L_(7) O L_(332)
!end �����:20FDB50AE403
       end

       subroutine FDB10_ConInQ
       end

       subroutine FDB10_ConIn
       LOGICAL*1 SPW_0(0:20479)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:5119)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:4095)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:1023)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:20479)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:5119)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_8(0:3071)
       Common/SPW_8/ SPW_8 !
       integer*4 SPW_8_4(0:767)
       equivalence(SPW_8,SPW_8_4)
        Call FDB10_ConInQ
!beg �������:20FDB13BR006KA12
        SPW_0(18215)=.false.	!L_(16) O
!end �������:20FDB13BR006KA12
!beg �������:20FDB11BR109KA03
        SPW_0(18208)=.false.	!L_(9) O
!end �������:20FDB11BR109KA03
!beg �������:20FDB11BR109KA03
        SPW_0(18209)=.false.	!L_(10) O
!end �������:20FDB11BR109KA03
!beg �������:20FDB13BR005KA12
        SPW_0(18216)=.false.	!L_(17) O
!end �������:20FDB13BR005KA12
!beg �������:20FDB13BR005KA12
        SPW_0(18217)=.false.	!L_(18) O
!end �������:20FDB13BR005KA12
!beg �������:20FDB11BR108KA03
        SPW_0(18210)=.false.	!L_(11) O
!end �������:20FDB11BR108KA03
!beg �������:20FDB11BR108KA03
        SPW_0(18211)=.false.	!L_(12) O
!end �������:20FDB11BR108KA03
!beg �������:20FDB13BR004KA12
        SPW_0(18218)=.false.	!L_(19) O
!end �������:20FDB13BR004KA12
!beg �������:20FDB13BR004KA12
        SPW_0(18219)=.false.	!L_(20) O
!end �������:20FDB13BR004KA12
!beg �������:20FDB11BR107KA03
        SPW_0(18212)=.false.	!L_(13) O
!end �������:20FDB11BR107KA03
!beg �������:20FDB11BR110KA03
        SPW_0(18206)=.false.	!L_(7) O
!end �������:20FDB11BR110KA03
!beg �������:20FDB11BR107KA03
        SPW_0(18213)=.false.	!L_(14) O
!end �������:20FDB11BR107KA03
!beg �������:20FDB13BR003KA12
        SPW_0(18220)=.false.	!L_(21) O
!end �������:20FDB13BR003KA12
!beg �������:20FDB11BR110KA03
        SPW_0(18207)=.false.	!L_(8) O
!end �������:20FDB11BR110KA03
!beg �������:20FDB13BR003KA12
        SPW_0(18221)=.false.	!L_(22) O
!end �������:20FDB13BR003KA12
!beg �������:20FDB13BR006KA12
        SPW_0(18214)=.false.	!L_(15) O
!end �������:20FDB13BR006KA12
       end

       subroutine FDB20_ConInQ
       end

       subroutine FDB20_ConIn
       LOGICAL*1 SPW_0(0:20479)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:5119)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:4095)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:1023)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:20479)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:5119)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_8(0:3071)
       Common/SPW_8/ SPW_8 !
       integer*4 SPW_8_4(0:767)
       equivalence(SPW_8,SPW_8_4)
        Call FDB20_ConInQ
!beg ��������:20FDB23CU001KN01
        SPW_0(16441)=.false.	!L_(37) O
!end ��������:20FDB23CU001KN01
!beg ���������:20FDB23CU001KN01
        SPW_0(16442)=.false.	!L_(38) O
!end ���������:20FDB23CU001KN01
!beg ��������:20FDB22CU001KN01
        SPW_0(16443)=.false.	!L_(39) O
!end ��������:20FDB22CU001KN01
!beg ��������:20FDB25CU001KN01
        SPW_0(16437)=.false.	!L_(33) O
!end ��������:20FDB25CU001KN01
!beg ���������:20FDB22CU001KN01
        SPW_0(16444)=.false.	!L_(40) O
!end ���������:20FDB22CU001KN01
!beg ���������:20FDB25CU001KN01
        SPW_0(16438)=.false.	!L_(34) O
!end ���������:20FDB25CU001KN01
!beg ��������:20FDB21CU001KN01
        SPW_0(16445)=.false.	!L_(41) O
!end ��������:20FDB21CU001KN01
!beg ��������:20FDB24CU001KN01
        SPW_0(16439)=.false.	!L_(35) O
!end ��������:20FDB24CU001KN01
!beg ���������:20FDB21CU001KN01
        SPW_0(16446)=.false.	!L_(42) O
!end ���������:20FDB21CU001KN01
!beg ���������:20FDB24CU001KN01
        SPW_0(16440)=.false.	!L_(36) O
!end ���������:20FDB24CU001KN01
       end

       subroutine FDB40_ConInQ
       end

       subroutine FDB40_ConIn
       LOGICAL*1 SPW_0(0:20479)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:5119)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:4095)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:1023)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:20479)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:5119)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_8(0:3071)
       Common/SPW_8/ SPW_8 !
       integer*4 SPW_8_4(0:767)
       equivalence(SPW_8,SPW_8_4)
        Call FDB40_ConInQ
!beg ��������:20FDB41CU001KN01
        SPW_2(3813)=.false.	!L_(10) O
!end ��������:20FDB41CU001KN01
!beg ���������:20FDB41CU001KN01
        SPW_2(3814)=.false.	!L_(11) O
!end ���������:20FDB41CU001KN01
       end

       subroutine FDB60_ConInQ
       end

       subroutine FDB60_ConIn
       LOGICAL*1 SPW_0(0:20479)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:5119)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:4095)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:1023)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:20479)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:5119)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_8(0:3071)
       Common/SPW_8/ SPW_8 !
       integer*4 SPW_8_4(0:767)
       equivalence(SPW_8,SPW_8_4)
        Call FDB60_ConInQ
!beg ��������:20FDB60KN001
        SPW_4(19930)=.false.	!L_(4) O
!end ��������:20FDB60KN001
!beg ���������:20FDB60KN001
        SPW_4(19931)=.false.	!L_(5) O
!end ���������:20FDB60KN001
       end

       subroutine FDB70_ConInQ
       end

       subroutine FDB70_ConIn
       LOGICAL*1 SPW_0(0:20479)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:5119)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:4095)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:1023)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:20479)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:5119)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_8(0:3071)
       Common/SPW_8/ SPW_8 !
       integer*4 SPW_8_4(0:767)
       equivalence(SPW_8,SPW_8_4)
        Call FDB70_ConInQ
!beg ���������:20FDB73CU001KN01
        SPW_0(18144)=.false.	!L_(20) O
!end ���������:20FDB73CU001KN01
!beg ��������:20FDB72CU001KN01
        SPW_0(18145)=.false.	!L_(21) O
!end ��������:20FDB72CU001KN01
!beg ���������:20FDB72CU001KN01
        SPW_0(18146)=.false.	!L_(22) O
!end ���������:20FDB72CU001KN01
!beg ��������:20FDB71CU001KN01
        SPW_0(18147)=.false.	!L_(23) O
!end ��������:20FDB71CU001KN01
!beg ���������:20FDB71CU001KN01
        SPW_0(18148)=.false.	!L_(24) O
!end ���������:20FDB71CU001KN01
!beg �������:20FDB71AA001
        SPW_0(18149)=.false.	!L_(25) O
!end �������:20FDB71AA001
!beg �������:20FDB71AA001
        SPW_0(18150)=.false.	!L_(26) O
!end �������:20FDB71AA001
!beg ��������:20FDB73CU001KN01
        SPW_0(18143)=.false.	!L_(19) O
!end ��������:20FDB73CU001KN01
       end

       subroutine FDB90_ConInQ
       end

       subroutine FDB90_ConIn
       LOGICAL*1 SPW_0(0:20479)
       Common/SPW_0/ SPW_0 !
       integer*4 SPW_0_4(0:5119)
       equivalence(SPW_0,SPW_0_4)
       LOGICAL*1 SPW_2(0:4095)
       Common/SPW_2/ SPW_2 !
       integer*4 SPW_2_4(0:1023)
       equivalence(SPW_2,SPW_2_4)
       LOGICAL*1 SPW_4(0:20479)
       Common/SPW_4/ SPW_4 !
       integer*4 SPW_4_4(0:5119)
       equivalence(SPW_4,SPW_4_4)
       LOGICAL*1 SPW_8(0:3071)
       Common/SPW_8/ SPW_8 !
       integer*4 SPW_8_4(0:767)
       equivalence(SPW_8,SPW_8_4)
        Call FDB90_ConInQ
!beg ���������:20FDB91CU001KN01
        SPW_8(2238)=.false.	!L_(13) O
!end ���������:20FDB91CU001KN01
!beg ��������:20FDB91CM003KN01
        SPW_8(2231)=.false.	!L_(6) O
!end ��������:20FDB91CM003KN01
!beg ��������:20FDB92CM001KN01
        SPW_8(2243)=.false.	!L_(18) O
!end ��������:20FDB92CM001KN01
!beg ���������:20FDB91CM003KN01
        SPW_8(2232)=.false.	!L_(7) O
!end ���������:20FDB91CM003KN01
!beg ��������:20FDB92CM101KN01
        SPW_8(2241)=.false.	!L_(16) O
!end ��������:20FDB92CM101KN01
!beg ��������:20FDB91CM002KN01
        SPW_8(2235)=.false.	!L_(10) O
!end ��������:20FDB91CM002KN01
!beg ���������:20FDB92CM001KN01
        SPW_8(2244)=.false.	!L_(19) O
!end ���������:20FDB92CM001KN01
!beg ���������:20FDB92CM101KN01
        SPW_8(2242)=.false.	!L_(17) O
!end ���������:20FDB92CM101KN01
!beg ���������:20FDB91CM002KN01
        SPW_8(2236)=.false.	!L_(11) O
!end ���������:20FDB91CM002KN01
!beg ��������:20FDB91CU003KN01
        SPW_8(2229)=.false.	!L_(4) O
!end ��������:20FDB91CU003KN01
!beg ��������:20FDB91CM001KN01
        SPW_8(2239)=.false.	!L_(14) O
!end ��������:20FDB91CM001KN01
!beg ���������:20FDB91CU003KN01
        SPW_8(2230)=.false.	!L_(5) O
!end ���������:20FDB91CU003KN01
!beg ���������:20FDB91CM001KN01
        SPW_8(2240)=.false.	!L_(15) O
!end ���������:20FDB91CM001KN01
!beg ��������:20FDB91CU002KN01
        SPW_8(2233)=.false.	!L_(8) O
!end ��������:20FDB91CU002KN01
!beg ���������:20FDB91CU002KN01
        SPW_8(2234)=.false.	!L_(9) O
!end ���������:20FDB91CU002KN01
!beg ��������:20FDB91CU001KN01
        SPW_8(2237)=.false.	!L_(12) O
!end ��������:20FDB91CU001KN01
       end
