      Subroutine FDB90(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDB90.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      Call FDB90_ConIn
      R_(62)=R8_ipe
C FDB90_vent_log.fgi( 337, 476):pre: �������������� �����  
      R_(66)=R8_ire
C FDB90_vent_log.fgi( 277, 475):pre: �������������� �����  
      R_(82)=R8_ixe
C FDB90_vent_log.fgi( 403, 476):pre: �������������� �����  
      R_(78)=R8_ive
C FDB90_vent_log.fgi( 466, 476):pre: �������������� �����  
      R_(70)=R8_ise
C FDB90_vent_log.fgi( 215, 476):pre: �������������� �����  
      R_(74)=R8_ite
C FDB90_vent_log.fgi( 155, 475):pre: �������������� �����  
      R_(86)=R8_ibi
C FDB90_vent_log.fgi(  97, 476):pre: �������������� �����  
      R_(90)=R8_idi
C FDB90_vent_log.fgi(  37, 475):pre: �������������� �����  
      R_abif = R8_e + (-R8_ol)
C FDB90_vent_log.fgi( 332, 378):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_uvef,R_odif,REAL(1,4)
     &,
     & REAL(R_ixef,4),REAL(R_oxef,4),
     & REAL(R_ovef,4),REAL(R_ivef,4),I_idif,
     & REAL(R_ebif,4),L_ibif,REAL(R_obif,4),L_ubif,L_adif
     &,R_uxef,
     & REAL(R_exef,4),REAL(R_axef,4),L_edif,REAL(R_abif,4
     &))
      !}
C FDB90_vlv.fgi( 192, 255):���������� �������,20FDB91CP181XQ01
      R_osef = R8_i
C FDB90_vent_log.fgi(  32, 398):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_iref,R_evef,REAL(1,4)
     &,
     & REAL(R_asef,4),REAL(R_esef,4),
     & REAL(R_eref,4),REAL(R_aref,4),I_avef,
     & REAL(R_usef,4),L_atef,REAL(R_etef,4),L_itef,L_otef
     &,R_isef,
     & REAL(R_uref,4),REAL(R_oref,4),L_utef,REAL(R_osef,4
     &))
      !}
C FDB90_vlv.fgi( 192, 241):���������� �������,20FDB91CT181XQ01
      R_(7) = R8_ul + (-R8_ol)
C FDB90_vent_log.fgi( 271, 314):��������
      R_(9) = R8_am + (-R8_ol)
C FDB90_vent_log.fgi( 271, 322):��������
      R_(11) = R8_em + (-R8_ol)
C FDB90_vent_log.fgi( 271, 330):��������
      R_(13) = R8_im + (-R8_ol)
C FDB90_vent_log.fgi( 271, 338):��������
      R_(15) = R8_om + (-R8_ol)
C FDB90_vent_log.fgi( 271, 346):��������
      R_(17) = R8_um + (-R8_ol)
C FDB90_vent_log.fgi( 271, 354):��������
      R_(19) = R8_ap + (-R8_ol)
C FDB90_vent_log.fgi( 271, 362):��������
      R_(21) = R8_ep + (-R8_ol)
C FDB90_vent_log.fgi( 271, 370):��������
      R_(23) = R8_ip + (-R8_ol)
C FDB90_vent_log.fgi( 271, 378):��������
      R_(25) = R8_op + (-R8_ol)
C FDB90_vent_log.fgi( 271, 386):��������
      R_(27) = R8_up + (-R8_ol)
C FDB90_vent_log.fgi( 271, 394):��������
      R_(29) = R8_ar + (-R8_ol)
C FDB90_vent_log.fgi( 271, 402):��������
      R_(31) = R8_er + (-R8_ol)
C FDB90_vent_log.fgi( 271, 410):��������
      R_(33) = R8_ir + (-R8_ol)
C FDB90_vent_log.fgi( 271, 418):��������
      R_(1) = 100
C FDB90_vent_log.fgi( 398, 356):��������� (RE4) (�������)
      R_(2) = 100
C FDB90_vent_log.fgi( 398, 361):��������� (RE4) (�������)
      R_(3) = 100
C FDB90_vent_log.fgi( 398, 367):��������� (RE4) (�������)
      R_(4) = 100
C FDB90_vent_log.fgi( 398, 373):��������� (RE4) (�������)
      R_(5) = 100
C FDB90_vent_log.fgi( 398, 379):��������� (RE4) (�������)
      R_ulav = R8_o
C FDB90_vent_log.fgi( 444, 405):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_okav,R_ipav,REAL(1,4)
     &,
     & REAL(R_elav,4),REAL(R_ilav,4),
     & REAL(R_ikav,4),REAL(R_ekav,4),I_epav,
     & REAL(R_amav,4),L_emav,REAL(R_imav,4),L_omav,L_umav
     &,R_olav,
     & REAL(R_alav,4),REAL(R_ukav,4),L_apav,REAL(R_ulav,4
     &))
      !}
C FDB90_vlv.fgi( 104, 155):���������� �������,20FDB92CM101XQ01
      R_akat = R8_u
C FDB90_vent_log.fgi( 444, 409):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_udat,R_olat,REAL(1,4)
     &,
     & REAL(R_ifat,4),REAL(R_ofat,4),
     & REAL(R_odat,4),REAL(R_idat,4),I_ilat,
     & REAL(R_ekat,4),L_ikat,REAL(R_okat,4),L_ukat,L_alat
     &,R_ufat,
     & REAL(R_efat,4),REAL(R_afat,4),L_elat,REAL(R_akat,4
     &))
      !}
C FDB90_vlv.fgi(  47, 241):���������� �������,20FDB92CM001XQ01
      R_usud = R8_ad
C FDB90_vent_log.fgi( 444, 413):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_orud,R_ivud,REAL(1,4)
     &,
     & REAL(R_esud,4),REAL(R_isud,4),
     & REAL(R_irud,4),REAL(R_erud,4),I_evud,
     & REAL(R_atud,4),L_etud,REAL(R_itud,4),L_otud,L_utud
     &,R_osud,
     & REAL(R_asud,4),REAL(R_urud,4),L_avud,REAL(R_usud,4
     &))
      !}
C FDB90_vlv.fgi( 192, 212):���������� �������,20FDB91CM003XQ01
      R_upal = R8_ed
C FDB90_vent_log.fgi( 444, 417):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_omal,R_isal,REAL(1,4)
     &,
     & REAL(R_epal,4),REAL(R_ipal,4),
     & REAL(R_imal,4),REAL(R_emal,4),I_esal,
     & REAL(R_aral,4),L_eral,REAL(R_iral,4),L_oral,L_ural
     &,R_opal,
     & REAL(R_apal,4),REAL(R_umal,4),L_asal,REAL(R_upal,4
     &))
      !}
C FDB90_vlv.fgi( 162, 226):���������� �������,20FDB91CM002XQ01
      R_akam = R8_id
C FDB90_vent_log.fgi( 444, 421):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_udam,R_olam,REAL(1,4)
     &,
     & REAL(R_ifam,4),REAL(R_ofam,4),
     & REAL(R_odam,4),REAL(R_idam,4),I_ilam,
     & REAL(R_ekam,4),L_ikam,REAL(R_okam,4),L_ukam,L_alam
     &,R_ufam,
     & REAL(R_efam,4),REAL(R_afam,4),L_elam,REAL(R_akam,4
     &))
      !}
C FDB90_vlv.fgi( 133, 198):���������� �������,20FDB91CM001XQ01
      R_idav = R8_od
C FDB90_vent_log.fgi( 388, 405):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ebav,R_akav,REAL(1,4)
     &,
     & REAL(R_ubav,4),REAL(R_adav,4),
     & REAL(R_abav,4),REAL(R_uxut,4),I_ufav,
     & REAL(R_odav,4),L_udav,REAL(R_afav,4),L_efav,L_ifav
     &,R_edav,
     & REAL(R_obav,4),REAL(R_ibav,4),L_ofav,REAL(R_idav,4
     &))
      !}
C FDB90_vlv.fgi( 104, 141):���������� �������,20FDB92CU101XQ01
      R_oxus = R8_ud
C FDB90_vent_log.fgi( 388, 409):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ivus,R_edat,REAL(1,4)
     &,
     & REAL(R_axus,4),REAL(R_exus,4),
     & REAL(R_evus,4),REAL(R_avus,4),I_adat,
     & REAL(R_uxus,4),L_abat,REAL(R_ebat,4),L_ibat,L_obat
     &,R_ixus,
     & REAL(R_uvus,4),REAL(R_ovus,4),L_ubat,REAL(R_oxus,4
     &))
      !}
C FDB90_vlv.fgi(  47, 227):���������� �������,20FDB92CU001XQ01
      R_ebaf = R8_af
C FDB90_vent_log.fgi( 388, 413):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_axud,R_udaf,REAL(1,4)
     &,
     & REAL(R_oxud,4),REAL(R_uxud,4),
     & REAL(R_uvud,4),REAL(R_ovud,4),I_odaf,
     & REAL(R_ibaf,4),L_obaf,REAL(R_ubaf,4),L_adaf,L_edaf
     &,R_abaf,
     & REAL(R_ixud,4),REAL(R_exud,4),L_idaf,REAL(R_ebaf,4
     &))
      !}
C FDB90_vlv.fgi( 133, 155):���������� �������,20FDB91CU003XQ01
      R_ikal = R8_ef
C FDB90_vent_log.fgi( 388, 417):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_efal,R_amal,REAL(1,4)
     &,
     & REAL(R_ufal,4),REAL(R_akal,4),
     & REAL(R_afal,4),REAL(R_udal,4),I_ulal,
     & REAL(R_okal,4),L_ukal,REAL(R_alal,4),L_elal,L_ilal
     &,R_ekal,
     & REAL(R_ofal,4),REAL(R_ifal,4),L_olal,REAL(R_ikal,4
     &))
      !}
C FDB90_vlv.fgi( 162, 212):���������� �������,20FDB91CU002XQ01
      R_oxul = R8_if
C FDB90_vent_log.fgi( 388, 421):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ivul,R_edam,REAL(1,4)
     &,
     & REAL(R_axul,4),REAL(R_exul,4),
     & REAL(R_evul,4),REAL(R_avul,4),I_adam,
     & REAL(R_uxul,4),L_abam,REAL(R_ebam,4),L_ibam,L_obam
     &,R_ixul,
     & REAL(R_uvul,4),REAL(R_ovul,4),L_ubam,REAL(R_oxul,4
     &))
      !}
C FDB90_vlv.fgi( 133, 184):���������� �������,20FDB91CU001XQ01
      R_oput = R8_of + (-R8_ol)
C FDB90_vent_log.fgi( 332, 353):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_imut,R_esut,REAL(1,4)
     &,
     & REAL(R_aput,4),REAL(R_eput,4),
     & REAL(R_emut,4),REAL(R_amut,4),I_asut,
     & REAL(R_uput,4),L_arut,REAL(R_erut,4),L_irut,L_orut
     &,R_iput,
     & REAL(R_umut,4),REAL(R_omut,4),L_urut,REAL(R_oput,4
     &))
      !}
C FDB90_vlv.fgi(  74, 127):���������� �������,20FDB92CP002XQ01
      R_idus = R8_uf + (-R8_ol)
C FDB90_vent_log.fgi( 332, 362):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ebus,R_akus,REAL(1,4)
     &,
     & REAL(R_ubus,4),REAL(R_adus,4),
     & REAL(R_abus,4),REAL(R_uxos,4),I_ufus,
     & REAL(R_odus,4),L_udus,REAL(R_afus,4),L_efus,L_ifus
     &,R_edus,
     & REAL(R_obus,4),REAL(R_ibus,4),L_ofus,REAL(R_idus,4
     &))
      !}
C FDB90_vlv.fgi(  47, 183):���������� �������,20FDB92CP001XQ01
      R_ek = R8_ak + (-R8_ol)
C FDB90_vent_log.fgi( 332, 371):��������
      R_emef = R8_ik + (-R8_ol)
C FDB90_vent_log.fgi( 332, 385):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_alef,R_upef,REAL(1,4)
     &,
     & REAL(R_olef,4),REAL(R_ulef,4),
     & REAL(R_ukef,4),REAL(R_okef,4),I_opef,
     & REAL(R_imef,4),L_omef,REAL(R_umef,4),L_apef,L_epef
     &,R_amef,
     & REAL(R_ilef,4),REAL(R_elef,4),L_ipef,REAL(R_emef,4
     &))
      !}
C FDB90_vlv.fgi( 192, 184):���������� �������,20FDB91CP191XQ01
      R_upif = R8_ok + (-R8_ol)
C FDB90_vent_log.fgi( 332, 394):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_omif,R_isif,REAL(1,4)
     &,
     & REAL(R_epif,4),REAL(R_ipif,4),
     & REAL(R_imif,4),REAL(R_emif,4),I_esif,
     & REAL(R_arif,4),L_erif,REAL(R_irif,4),L_orif,L_urif
     &,R_opif,
     & REAL(R_apif,4),REAL(R_umif,4),L_asif,REAL(R_upif,4
     &))
      !}
C FDB90_vlv.fgi( 162, 184):���������� �������,20FDB91CP051XQ01
      R_utam = R8_uk + (-R8_ol)
C FDB90_vent_log.fgi( 332, 403):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_osam,R_ixam,REAL(1,4)
     &,
     & REAL(R_etam,4),REAL(R_itam,4),
     & REAL(R_isam,4),REAL(R_esam,4),I_exam,
     & REAL(R_avam,4),L_evam,REAL(R_ivam,4),L_ovam,L_uvam
     &,R_otam,
     & REAL(R_atam,4),REAL(R_usam,4),L_axam,REAL(R_utam,4
     &))
      !}
C FDB90_vlv.fgi( 133, 226):���������� �������,20FDB91CP031XQ01
      R_odel = R8_al + (-R8_ol)
C FDB90_vent_log.fgi( 332, 412):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ibel,R_ekel,REAL(1,4)
     &,
     & REAL(R_adel,4),REAL(R_edel,4),
     & REAL(R_ebel,4),REAL(R_abel,4),I_akel,
     & REAL(R_udel,4),L_afel,REAL(R_efel,4),L_ifel,L_ofel
     &,R_idel,
     & REAL(R_ubel,4),REAL(R_obel,4),L_ufel,REAL(R_odel,4
     &))
      !}
C FDB90_vlv.fgi( 162, 255):���������� �������,20FDB91CP021XQ01
      R_il = R8_el + (-R8_ol)
C FDB90_vent_log.fgi( 332, 421):��������
      R_(6) = 1e-3
C FDB90_vent_log.fgi( 277, 311):��������� (RE4) (�������)
      R_oxep = R_(7) * R_(6)
C FDB90_vent_log.fgi( 280, 313):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ivep,R_edip,REAL(1,4)
     &,
     & REAL(R_axep,4),REAL(R_exep,4),
     & REAL(R_evep,4),REAL(R_avep,4),I_adip,
     & REAL(R_uxep,4),L_abip,REAL(R_ebip,4),L_ibip,L_obip
     &,R_ixep,
     & REAL(R_uvep,4),REAL(R_ovep,4),L_ubip,REAL(R_oxep,4
     &))
      !}
C FDB90_vlv.fgi( 104,  48):���������� �������,20FDB93CP004XQ01
      R_(8) = 1e-3
C FDB90_vent_log.fgi( 277, 319):��������� (RE4) (�������)
      R_akip = R_(9) * R_(8)
C FDB90_vent_log.fgi( 280, 321):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_udip,R_olip,REAL(1,4)
     &,
     & REAL(R_ifip,4),REAL(R_ofip,4),
     & REAL(R_odip,4),REAL(R_idip,4),I_ilip,
     & REAL(R_ekip,4),L_ikip,REAL(R_okip,4),L_ukip,L_alip
     &,R_ufip,
     & REAL(R_efip,4),REAL(R_afip,4),L_elip,REAL(R_akip,4
     &))
      !}
C FDB90_vlv.fgi(  74,  48):���������� �������,20FDB93CP003XQ01
      R_(10) = 1e-3
C FDB90_vent_log.fgi( 277, 327):��������� (RE4) (�������)
      R_ixup = R_(11) * R_(10)
C FDB90_vent_log.fgi( 280, 329):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_evup,R_adar,REAL(1,4)
     &,
     & REAL(R_uvup,4),REAL(R_axup,4),
     & REAL(R_avup,4),REAL(R_utup,4),I_ubar,
     & REAL(R_oxup,4),L_uxup,REAL(R_abar,4),L_ebar,L_ibar
     &,R_exup,
     & REAL(R_ovup,4),REAL(R_ivup,4),L_obar,REAL(R_ixup,4
     &))
      !}
C FDB90_vlv.fgi( 104,  95):���������� �������,20FDB93CP002XQ01
      R_(12) = 1e-3
C FDB90_vent_log.fgi( 277, 335):��������� (RE4) (�������)
      R_epar = R_(13) * R_(12)
C FDB90_vent_log.fgi( 280, 337):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_amar,R_urar,REAL(1,4)
     &,
     & REAL(R_omar,4),REAL(R_umar,4),
     & REAL(R_ular,4),REAL(R_olar,4),I_orar,
     & REAL(R_ipar,4),L_opar,REAL(R_upar,4),L_arar,L_erar
     &,R_apar,
     & REAL(R_imar,4),REAL(R_emar,4),L_irar,REAL(R_epar,4
     &))
      !}
C FDB90_vlv.fgi( 104,  79):���������� �������,20FDB93CP001XQ01
      R_(14) = 1e-3
C FDB90_vent_log.fgi( 277, 343):��������� (RE4) (�������)
      R_idox = R_(15) * R_(14)
C FDB90_vent_log.fgi( 280, 345):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ebox,R_akox,REAL(1,4)
     &,
     & REAL(R_ubox,4),REAL(R_adox,4),
     & REAL(R_abox,4),REAL(R_uxix,4),I_ufox,
     & REAL(R_odox,4),L_udox,REAL(R_afox,4),L_efox,L_ifox
     &,R_edox,
     & REAL(R_obox,4),REAL(R_ibox,4),L_ofox,REAL(R_idox,4
     &))
      !}
C FDB90_vlv.fgi( 104, 198):���������� �������,20FDB92CP106XQ01
      R_(16) = 1e-3
C FDB90_vent_log.fgi( 277, 351):��������� (RE4) (�������)
      R_avix = R_(17) * R_(16)
C FDB90_vent_log.fgi( 280, 353):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_usix,R_oxix,REAL(1,4)
     &,
     & REAL(R_itix,4),REAL(R_otix,4),
     & REAL(R_osix,4),REAL(R_isix,4),I_ixix,
     & REAL(R_evix,4),L_ivix,REAL(R_ovix,4),L_uvix,L_axix
     &,R_utix,
     & REAL(R_etix,4),REAL(R_atix,4),L_exix,REAL(R_avix,4
     &))
      !}
C FDB90_vlv.fgi( 104, 184):���������� �������,20FDB92CP105XQ01
      R_(18) = 1e-3
C FDB90_vent_log.fgi( 277, 359):��������� (RE4) (�������)
      R_opix = R_(19) * R_(18)
C FDB90_vent_log.fgi( 280, 361):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_imix,R_esix,REAL(1,4)
     &,
     & REAL(R_apix,4),REAL(R_epix,4),
     & REAL(R_emix,4),REAL(R_amix,4),I_asix,
     & REAL(R_upix,4),L_arix,REAL(R_erix,4),L_irix,L_orix
     &,R_ipix,
     & REAL(R_umix,4),REAL(R_omix,4),L_urix,REAL(R_opix,4
     &))
      !}
C FDB90_vlv.fgi( 104, 169):���������� �������,20FDB92CP104XQ01
      R_(20) = 1e-3
C FDB90_vent_log.fgi( 277, 367):��������� (RE4) (�������)
      R_opobe = R_(21) * R_(20)
C FDB90_vent_log.fgi( 280, 369):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_imobe,R_esobe,REAL(1,4
     &),
     & REAL(R_apobe,4),REAL(R_epobe,4),
     & REAL(R_emobe,4),REAL(R_amobe,4),I_asobe,
     & REAL(R_upobe,4),L_arobe,REAL(R_erobe,4),L_irobe,L_orobe
     &,R_ipobe,
     & REAL(R_umobe,4),REAL(R_omobe,4),L_urobe,REAL(R_opobe
     &,4))
      !}
C FDB90_vlv.fgi( 104, 269):���������� �������,20FDB92CP007XQ01
      R_(22) = 1e-3
C FDB90_vent_log.fgi( 277, 375):��������� (RE4) (�������)
      R_ekobe = R_(23) * R_(22)
C FDB90_vent_log.fgi( 280, 377):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_afobe,R_ulobe,REAL(1,4
     &),
     & REAL(R_ofobe,4),REAL(R_ufobe,4),
     & REAL(R_udobe,4),REAL(R_odobe,4),I_olobe,
     & REAL(R_ikobe,4),L_okobe,REAL(R_ukobe,4),L_alobe,L_elobe
     &,R_akobe,
     & REAL(R_ifobe,4),REAL(R_efobe,4),L_ilobe,REAL(R_ekobe
     &,4))
      !}
C FDB90_vlv.fgi( 104, 255):���������� �������,20FDB92CP006XQ01
      R_(24) = 1e-3
C FDB90_vent_log.fgi( 277, 383):��������� (RE4) (�������)
      R_uxibe = R_(25) * R_(24)
C FDB90_vent_log.fgi( 280, 385):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ovibe,R_idobe,REAL(1,4
     &),
     & REAL(R_exibe,4),REAL(R_ixibe,4),
     & REAL(R_ivibe,4),REAL(R_evibe,4),I_edobe,
     & REAL(R_abobe,4),L_ebobe,REAL(R_ibobe,4),L_obobe,L_ubobe
     &,R_oxibe,
     & REAL(R_axibe,4),REAL(R_uvibe,4),L_adobe,REAL(R_uxibe
     &,4))
      !}
C FDB90_vlv.fgi( 104, 241):���������� �������,20FDB92CP005XQ01
      R_(26) = 1e-3
C FDB90_vent_log.fgi( 277, 391):��������� (RE4) (�������)
      R_isibe = R_(27) * R_(26)
C FDB90_vent_log.fgi( 280, 393):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_eribe,R_avibe,REAL(1,4
     &),
     & REAL(R_uribe,4),REAL(R_asibe,4),
     & REAL(R_aribe,4),REAL(R_upibe,4),I_utibe,
     & REAL(R_osibe,4),L_usibe,REAL(R_atibe,4),L_etibe,L_itibe
     &,R_esibe,
     & REAL(R_oribe,4),REAL(R_iribe,4),L_otibe,REAL(R_isibe
     &,4))
      !}
C FDB90_vlv.fgi( 104, 226):���������� �������,20FDB92CP004XQ01
      R_(28) = 1e-3
C FDB90_vent_log.fgi( 277, 399):��������� (RE4) (�������)
      R_axir = R_(29) * R_(28)
C FDB90_vent_log.fgi( 280, 401):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_avir,R_obor,REAL(1,4)
     &,
     & REAL(R_ovir,4),REAL(R_uvir,4),
     & REAL(R_utir,4),REAL(R_otir,4),I_ibor,
     & REAL(R_exir,4),L_ixir,REAL(R_oxir,4),L_uxir,L_abor
     &,R_ivube,
     & REAL(R_ivir,4),REAL(R_evir,4),L_ebor,REAL(R_axir,4
     &))
      !}
C FDB90_vlv.fgi(  47,  79):���������� �������,20FDB91CP011XQ01
      L_(20)=R_ivube.lt.R0_evube
C FDB90_logic.fgi( 533,  95):���������� <
      L_ovube=L_(20).and..not.L0_avube
      L0_avube=L_(20)
C FDB90_logic.fgi( 541,  95):������������  �� 1 ���
      R_(30) = 1e-3
C FDB90_vent_log.fgi( 277, 407):��������� (RE4) (�������)
      R_efor = R_(31) * R_(30)
C FDB90_vent_log.fgi( 280, 409):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_edor,R_ukor,REAL(1,4)
     &,
     & REAL(R_udor,4),REAL(R_afor,4),
     & REAL(R_ador,4),REAL(R_ubor,4),I_okor,
     & REAL(R_ifor,4),L_ofor,REAL(R_ufor,4),L_akor,L_ekor
     &,R_oxube,
     & REAL(R_odor,4),REAL(R_idor,4),L_ikor,REAL(R_efor,4
     &))
      !}
C FDB90_vlv.fgi( 104, 111):���������� �������,20FDB91CP006XQ01
      L_(21)=R_oxube.lt.R0_exube
C FDB90_logic.fgi( 533, 103):���������� <
      L_uxube=L_(21).and..not.L0_uvube
      L0_uvube=L_(21)
C FDB90_logic.fgi( 541, 103):������������  �� 1 ���
      R_(32) = 1e-3
C FDB90_vent_log.fgi( 277, 415):��������� (RE4) (�������)
      R_imor = R_(33) * R_(32)
C FDB90_vent_log.fgi( 280, 417):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ilor,R_aror,REAL(1,4)
     &,
     & REAL(R_amor,4),REAL(R_emor,4),
     & REAL(R_elor,4),REAL(R_alor,4),I_upor,
     & REAL(R_omor,4),L_umor,REAL(R_apor,4),L_epor,L_ipor
     &,R_ebade,
     & REAL(R_ulor,4),REAL(R_olor,4),L_opor,REAL(R_imor,4
     &))
      !}
C FDB90_vlv.fgi(  74, 111):���������� �������,20FDB91CP005XQ01
      L_(22)=R_ebade.lt.R0_ixube
C FDB90_logic.fgi( 533, 111):���������� <
      L_abade=L_(22).and..not.L0_axube
      L0_axube=L_(22)
C FDB90_logic.fgi( 541, 111):������������  �� 1 ���
      R_(34) = 60000
C FDB90_vent_log.fgi( 208, 313):��������� (RE4) (�������)
      if(R8_or.ge.0.0) then
         R_(35)=R8_ur/max(R8_or,1.0e-10)
      else
         R_(35)=R8_ur/min(R8_or,-1.0e-10)
      endif
C FDB90_vent_log.fgi( 198, 316):�������� ����������
      R_ipip = R_(35) * R_(34)
C FDB90_vent_log.fgi( 211, 315):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_emip,R_asip,REAL(1,4)
     &,
     & REAL(R_umip,4),REAL(R_apip,4),
     & REAL(R_amip,4),REAL(R_ulip,4),I_urip,
     & REAL(R_opip,4),L_upip,REAL(R_arip,4),L_erip,L_irip
     &,R_epip,
     & REAL(R_omip,4),REAL(R_imip,4),L_orip,REAL(R_ipip,4
     &))
      !}
C FDB90_vlv.fgi( 104,  63):���������� �������,20FDB93CF004XQ01
      R_(36) = 60000
C FDB90_vent_log.fgi( 208, 321):��������� (RE4) (�������)
      if(R8_as.ge.0.0) then
         R_(37)=R8_es/max(R8_as,1.0e-10)
      else
         R_(37)=R8_es/min(R8_as,-1.0e-10)
      endif
C FDB90_vent_log.fgi( 198, 324):�������� ����������
      R_utip = R_(37) * R_(36)
C FDB90_vent_log.fgi( 211, 323):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_osip,R_ixip,REAL(1,4)
     &,
     & REAL(R_etip,4),REAL(R_itip,4),
     & REAL(R_isip,4),REAL(R_esip,4),I_exip,
     & REAL(R_avip,4),L_evip,REAL(R_ivip,4),L_ovip,L_uvip
     &,R_otip,
     & REAL(R_atip,4),REAL(R_usip,4),L_axip,REAL(R_utip,4
     &))
      !}
C FDB90_vlv.fgi(  74,  63):���������� �������,20FDB93CF003XQ01
      R_(38) = 60000
C FDB90_vent_log.fgi( 208, 329):��������� (RE4) (�������)
      if(R8_is.ge.0.0) then
         R_(39)=R8_os/max(R8_is,1.0e-10)
      else
         R_(39)=R8_os/min(R8_is,-1.0e-10)
      endif
C FDB90_vent_log.fgi( 198, 332):�������� ����������
      R_ufar = R_(39) * R_(38)
C FDB90_vent_log.fgi( 211, 331):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_odar,R_ilar,REAL(1,4)
     &,
     & REAL(R_efar,4),REAL(R_ifar,4),
     & REAL(R_idar,4),REAL(R_edar,4),I_elar,
     & REAL(R_akar,4),L_ekar,REAL(R_ikar,4),L_okar,L_ukar
     &,R_ofar,
     & REAL(R_afar,4),REAL(R_udar,4),L_alar,REAL(R_ufar,4
     &))
      !}
C FDB90_vlv.fgi(  47,  63):���������� �������,20FDB93CF002XQ01
      R_(40) = 60000
C FDB90_vent_log.fgi( 208, 337):��������� (RE4) (�������)
      if(R8_us.ge.0.0) then
         R_(41)=R8_at/max(R8_us,1.0e-10)
      else
         R_(41)=R8_at/min(R8_us,-1.0e-10)
      endif
C FDB90_vent_log.fgi( 198, 340):�������� ����������
      R_otar = R_(41) * R_(40)
C FDB90_vent_log.fgi( 211, 339):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_isar,R_exar,REAL(1,4)
     &,
     & REAL(R_atar,4),REAL(R_etar,4),
     & REAL(R_esar,4),REAL(R_asar,4),I_axar,
     & REAL(R_utar,4),L_avar,REAL(R_evar,4),L_ivar,L_ovar
     &,R_itar,
     & REAL(R_usar,4),REAL(R_osar,4),L_uvar,REAL(R_otar,4
     &))
      !}
C FDB90_vlv.fgi(  74,  79):���������� �������,20FDB93CF001XQ01
      R_(42) = 60000
C FDB90_vent_log.fgi( 208, 345):��������� (RE4) (�������)
      if(R8_et.ge.0.0) then
         R_(43)=R8_it/max(R8_et,1.0e-10)
      else
         R_(43)=R8_it/min(R8_et,-1.0e-10)
      endif
C FDB90_vent_log.fgi( 198, 348):�������� ����������
      R_akux = R_(43) * R_(42)
C FDB90_vent_log.fgi( 211, 347):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_udux,R_olux,REAL(1,4)
     &,
     & REAL(R_ifux,4),REAL(R_ofux,4),
     & REAL(R_odux,4),REAL(R_idux,4),I_ilux,
     & REAL(R_ekux,4),L_ikux,REAL(R_okux,4),L_ukux,L_alux
     &,R_ufux,
     & REAL(R_efux,4),REAL(R_afux,4),L_elux,REAL(R_akux,4
     &))
      !}
C FDB90_vlv.fgi(  74, 212):���������� �������,20FDB92CF106XQ01
      R_(44) = 60000
C FDB90_vent_log.fgi( 208, 353):��������� (RE4) (�������)
      if(R8_ot.ge.0.0) then
         R_(45)=R8_ut/max(R8_ot,1.0e-10)
      else
         R_(45)=R8_ut/min(R8_ot,-1.0e-10)
      endif
C FDB90_vent_log.fgi( 198, 356):�������� ����������
      R_oxox = R_(45) * R_(44)
C FDB90_vent_log.fgi( 211, 355):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ivox,R_edux,REAL(1,4)
     &,
     & REAL(R_axox,4),REAL(R_exox,4),
     & REAL(R_evox,4),REAL(R_avox,4),I_adux,
     & REAL(R_uxox,4),L_abux,REAL(R_ebux,4),L_ibux,L_obux
     &,R_ixox,
     & REAL(R_uvox,4),REAL(R_ovox,4),L_ubux,REAL(R_oxox,4
     &))
      !}
C FDB90_vlv.fgi(  74, 198):���������� �������,20FDB92CF105XQ01
      R_(46) = 60000
C FDB90_vent_log.fgi( 208, 361):��������� (RE4) (�������)
      if(R8_av.ge.0.0) then
         R_(47)=R8_ev/max(R8_av,1.0e-10)
      else
         R_(47)=R8_ev/min(R8_av,-1.0e-10)
      endif
C FDB90_vent_log.fgi( 198, 364):�������� ����������
      R_esox = R_(47) * R_(46)
C FDB90_vent_log.fgi( 211, 363):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_arox,R_utox,REAL(1,4)
     &,
     & REAL(R_orox,4),REAL(R_urox,4),
     & REAL(R_upox,4),REAL(R_opox,4),I_otox,
     & REAL(R_isox,4),L_osox,REAL(R_usox,4),L_atox,L_etox
     &,R_asox,
     & REAL(R_irox,4),REAL(R_erox,4),L_itox,REAL(R_esox,4
     &))
      !}
C FDB90_vlv.fgi(  74, 183):���������� �������,20FDB92CF104XQ01
      R_(48) = 60000
C FDB90_vent_log.fgi( 208, 369):��������� (RE4) (�������)
      if(R8_iv.ge.0.0) then
         R_(49)=R8_ov/max(R8_iv,1.0e-10)
      else
         R_(49)=R8_ov/min(R8_iv,-1.0e-10)
      endif
C FDB90_vent_log.fgi( 198, 372):�������� ����������
      R_esube = R_(49) * R_(48)
C FDB90_vent_log.fgi( 211, 371):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_arube,R_utube,REAL(1,4
     &),
     & REAL(R_orube,4),REAL(R_urube,4),
     & REAL(R_upube,4),REAL(R_opube,4),I_otube,
     & REAL(R_isube,4),L_osube,REAL(R_usube,4),L_atube,L_etube
     &,R_asube,
     & REAL(R_irube,4),REAL(R_erube,4),L_itube,REAL(R_esube
     &,4))
      !}
C FDB90_vlv.fgi(  74, 269):���������� �������,20FDB92CF007XQ01
      R_(50) = 60000
C FDB90_vent_log.fgi( 208, 377):��������� (RE4) (�������)
      if(R8_uv.ge.0.0) then
         R_(51)=R8_ax/max(R8_uv,1.0e-10)
      else
         R_(51)=R8_ax/min(R8_uv,-1.0e-10)
      endif
C FDB90_vent_log.fgi( 198, 380):�������� ����������
      R_ulube = R_(51) * R_(50)
C FDB90_vent_log.fgi( 211, 379):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_okube,R_ipube,REAL(1,4
     &),
     & REAL(R_elube,4),REAL(R_ilube,4),
     & REAL(R_ikube,4),REAL(R_ekube,4),I_epube,
     & REAL(R_amube,4),L_emube,REAL(R_imube,4),L_omube,L_umube
     &,R_olube,
     & REAL(R_alube,4),REAL(R_ukube,4),L_apube,REAL(R_ulube
     &,4))
      !}
C FDB90_vlv.fgi(  74, 255):���������� �������,20FDB92CF006XQ01
      R_(52) = 60000
C FDB90_vent_log.fgi( 208, 385):��������� (RE4) (�������)
      if(R8_ex.ge.0.0) then
         R_(53)=R8_ix/max(R8_ex,1.0e-10)
      else
         R_(53)=R8_ix/min(R8_ex,-1.0e-10)
      endif
C FDB90_vent_log.fgi( 198, 388):�������� ����������
      R_idube = R_(53) * R_(52)
C FDB90_vent_log.fgi( 211, 387):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ebube,R_akube,REAL(1,4
     &),
     & REAL(R_ubube,4),REAL(R_adube,4),
     & REAL(R_abube,4),REAL(R_uxobe,4),I_ufube,
     & REAL(R_odube,4),L_udube,REAL(R_afube,4),L_efube,L_ifube
     &,R_edube,
     & REAL(R_obube,4),REAL(R_ibube,4),L_ofube,REAL(R_idube
     &,4))
      !}
C FDB90_vlv.fgi(  74, 241):���������� �������,20FDB92CF005XQ01
      R_(54) = 60000
C FDB90_vent_log.fgi( 208, 393):��������� (RE4) (�������)
      if(R8_ox.ge.0.0) then
         R_(55)=R8_ux/max(R8_ox,1.0e-10)
      else
         R_(55)=R8_ux/min(R8_ox,-1.0e-10)
      endif
C FDB90_vent_log.fgi( 198, 396):�������� ����������
      R_avobe = R_(55) * R_(54)
C FDB90_vent_log.fgi( 211, 395):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_usobe,R_oxobe,REAL(1,4
     &),
     & REAL(R_itobe,4),REAL(R_otobe,4),
     & REAL(R_osobe,4),REAL(R_isobe,4),I_ixobe,
     & REAL(R_evobe,4),L_ivobe,REAL(R_ovobe,4),L_uvobe,L_axobe
     &,R_utobe,
     & REAL(R_etobe,4),REAL(R_atobe,4),L_exobe,REAL(R_avobe
     &,4))
      !}
C FDB90_vlv.fgi(  74, 226):���������� �������,20FDB92CF004XQ01
      R_(56) = 60000
C FDB90_vent_log.fgi( 208, 401):��������� (RE4) (�������)
      if(R8_abe.ge.0.0) then
         R_(57)=R8_ebe/max(R8_abe,1.0e-10)
      else
         R_(57)=R8_ebe/min(R8_abe,-1.0e-10)
      endif
C FDB90_vent_log.fgi( 198, 404):�������� ����������
      R_usor = R_(57) * R_(56)
C FDB90_vent_log.fgi( 211, 403):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_oror,R_ivor,REAL(1,4)
     &,
     & REAL(R_esor,4),REAL(R_isor,4),
     & REAL(R_iror,4),REAL(R_eror,4),I_evor,
     & REAL(R_ator,4),L_etor,REAL(R_itor,4),L_otor,L_utor
     &,R_osor,
     & REAL(R_asor,4),REAL(R_uror,4),L_avor,REAL(R_usor,4
     &))
      !}
C FDB90_vlv.fgi(  47,  95):���������� �������,20FDB91CF011XQ01
      R_(58) = 60000
C FDB90_vent_log.fgi( 208, 409):��������� (RE4) (�������)
      if(R8_ibe.ge.0.0) then
         R_(59)=R8_obe/max(R8_ibe,1.0e-10)
      else
         R_(59)=R8_obe/min(R8_ibe,-1.0e-10)
      endif
C FDB90_vent_log.fgi( 198, 412):�������� ����������
      R_ebur = R_(59) * R_(58)
C FDB90_vent_log.fgi( 211, 411):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_axor,R_udur,REAL(1,4)
     &,
     & REAL(R_oxor,4),REAL(R_uxor,4),
     & REAL(R_uvor,4),REAL(R_ovor,4),I_odur,
     & REAL(R_ibur,4),L_obur,REAL(R_ubur,4),L_adur,L_edur
     &,R_abur,
     & REAL(R_ixor,4),REAL(R_exor,4),L_idur,REAL(R_ebur,4
     &))
      !}
C FDB90_vlv.fgi(  47, 111):���������� �������,20FDB91CF006XQ01
      R_(60) = 60000
C FDB90_vent_log.fgi( 208, 417):��������� (RE4) (�������)
      if(R8_ube.ge.0.0) then
         R_(61)=R8_ade/max(R8_ube,1.0e-10)
      else
         R_(61)=R8_ade/min(R8_ube,-1.0e-10)
      endif
C FDB90_vent_log.fgi( 198, 420):�������� ����������
      R_okur = R_(61) * R_(60)
C FDB90_vent_log.fgi( 211, 419):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ifur,R_emur,REAL(1,4)
     &,
     & REAL(R_akur,4),REAL(R_ekur,4),
     & REAL(R_efur,4),REAL(R_afur,4),I_amur,
     & REAL(R_ukur,4),L_alur,REAL(R_elur,4),L_ilur,L_olur
     &,R_ikur,
     & REAL(R_ufur,4),REAL(R_ofur,4),L_ulur,REAL(R_okur,4
     &))
      !}
C FDB90_vlv.fgi(  47, 127):���������� �������,20FDB91CF005XQ01
      R_oxav = R8_ede
C FDB90_vent_log.fgi( 143, 401):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ivav,R_edev,REAL(1,4)
     &,
     & REAL(R_axav,4),REAL(R_exav,4),
     & REAL(R_evav,4),REAL(R_avav,4),I_adev,
     & REAL(R_uxav,4),L_abev,REAL(R_ebev,4),L_ibev,L_obev
     &,R_ixav,
     & REAL(R_uvav,4),REAL(R_ovav,4),L_ubev,REAL(R_oxav,4
     &))
      !}
C FDB90_vlv.fgi(  74, 169):���������� �������,20FDB92CF101XQ01
      R_utat = R8_ide
C FDB90_vent_log.fgi( 143, 404):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_osat,R_ixat,REAL(1,4)
     &,
     & REAL(R_etat,4),REAL(R_itat,4),
     & REAL(R_isat,4),REAL(R_esat,4),I_exat,
     & REAL(R_avat,4),L_evat,REAL(R_ivat,4),L_ovat,L_uvat
     &,R_otat,
     & REAL(R_atat,4),REAL(R_usat,4),L_axat,REAL(R_utat,4
     &))
      !}
C FDB90_vlv.fgi(  47, 269):���������� �������,20FDB92CF001XQ01
      R_araf = R8_ode
C FDB90_vent_log.fgi( 143, 407):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_umaf,R_osaf,REAL(1,4)
     &,
     & REAL(R_ipaf,4),REAL(R_opaf,4),
     & REAL(R_omaf,4),REAL(R_imaf,4),I_isaf,
     & REAL(R_eraf,4),L_iraf,REAL(R_oraf,4),L_uraf,L_asaf
     &,R_upaf,
     & REAL(R_epaf,4),REAL(R_apaf,4),L_esaf,REAL(R_araf,4
     &))
      !}
C FDB90_vlv.fgi( 192, 198):���������� �������,20FDB91CF061XQ01
      R_ivaf = R8_ude
C FDB90_vent_log.fgi( 143, 410):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_etaf,R_abef,REAL(1,4)
     &,
     & REAL(R_utaf,4),REAL(R_avaf,4),
     & REAL(R_ataf,4),REAL(R_usaf,4),I_uxaf,
     & REAL(R_ovaf,4),L_uvaf,REAL(R_axaf,4),L_exaf,L_ixaf
     &,R_evaf,
     & REAL(R_otaf,4),REAL(R_itaf,4),L_oxaf,REAL(R_ivaf,4
     &))
      !}
C FDB90_vlv.fgi( 192, 269):���������� �������,20FDB91CF051XQ01
      R_isel = R8_afe
C FDB90_vent_log.fgi( 143, 413):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_erel,R_avel,REAL(1,4)
     &,
     & REAL(R_urel,4),REAL(R_asel,4),
     & REAL(R_arel,4),REAL(R_upel,4),I_utel,
     & REAL(R_osel,4),L_usel,REAL(R_atel,4),L_etel,L_itel
     &,R_esel,
     & REAL(R_orel,4),REAL(R_irel,4),L_otel,REAL(R_isel,4
     &))
      !}
C FDB90_vlv.fgi( 133, 269):���������� �������,20FDB91CF041XQ01
      R_uxel = R8_efe
C FDB90_vent_log.fgi( 143, 416):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ovel,R_idil,REAL(1,4)
     &,
     & REAL(R_exel,4),REAL(R_ixel,4),
     & REAL(R_ivel,4),REAL(R_evel,4),I_edil,
     & REAL(R_abil,4),L_ebil,REAL(R_ibil,4),L_obil,L_ubil
     &,R_oxel,
     & REAL(R_axel,4),REAL(R_uvel,4),L_adil,REAL(R_uxel,4
     &))
      !}
C FDB90_vlv.fgi( 104,  33):���������� �������,20FDB91CF031XQ01
      R_irap = R8_ife
C FDB90_vent_log.fgi( 143, 419):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_epap,R_atap,REAL(1,4)
     &,
     & REAL(R_upap,4),REAL(R_arap,4),
     & REAL(R_apap,4),REAL(R_umap,4),I_usap,
     & REAL(R_orap,4),L_urap,REAL(R_asap,4),L_esap,L_isap
     &,R_erap,
     & REAL(R_opap,4),REAL(R_ipap,4),L_osap,REAL(R_irap,4
     &))
      !}
C FDB90_vlv.fgi(  74,  33):���������� �������,20FDB91CF021XQ01
      R_ofe = R8_ufe
C FDB90_vent_log.fgi( 143, 422):��������
      R_avut = R8_ake
C FDB90_vent_log.fgi(  86, 410):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_usut,R_oxut,REAL(1,4)
     &,
     & REAL(R_itut,4),REAL(R_otut,4),
     & REAL(R_osut,4),REAL(R_isut,4),I_ixut,
     & REAL(R_evut,4),L_ivut,REAL(R_ovut,4),L_uvut,L_axut
     &,R_utut,
     & REAL(R_etut,4),REAL(R_atut,4),L_exut,REAL(R_avut,4
     &))
      !}
C FDB90_vlv.fgi(  74, 141):���������� �������,20FDB92CP103XQ01
      R_esus = R8_eke
C FDB90_vent_log.fgi(  86, 413):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_arus,R_utus,REAL(1,4)
     &,
     & REAL(R_orus,4),REAL(R_urus,4),
     & REAL(R_upus,4),REAL(R_opus,4),I_otus,
     & REAL(R_isus,4),L_osus,REAL(R_usus,4),L_atus,L_etus
     &,R_asus,
     & REAL(R_irus,4),REAL(R_erus,4),L_itus,REAL(R_esus,4
     &))
      !}
C FDB90_vlv.fgi(  47, 212):���������� �������,20FDB92CP003XQ01
      R_okaf = R8_ike
C FDB90_vent_log.fgi(  86, 416):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ifaf,R_emaf,REAL(1,4)
     &,
     & REAL(R_akaf,4),REAL(R_ekaf,4),
     & REAL(R_efaf,4),REAL(R_afaf,4),I_amaf,
     & REAL(R_ukaf,4),L_alaf,REAL(R_elaf,4),L_ilaf,L_olaf
     &,R_ikaf,
     & REAL(R_ufaf,4),REAL(R_ofaf,4),L_ulaf,REAL(R_okaf,4
     &))
      !}
C FDB90_vlv.fgi( 192, 226):���������� �������,20FDB91CP003XQ01
      R_eval = R8_oke
C FDB90_vent_log.fgi(  86, 419):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_atal,R_uxal,REAL(1,4)
     &,
     & REAL(R_otal,4),REAL(R_utal,4),
     & REAL(R_usal,4),REAL(R_osal,4),I_oxal,
     & REAL(R_ival,4),L_oval,REAL(R_uval,4),L_axal,L_exal
     &,R_aval,
     & REAL(R_ital,4),REAL(R_etal,4),L_ixal,REAL(R_eval,4
     &))
      !}
C FDB90_vlv.fgi( 162, 241):���������� �������,20FDB91CP002XQ01
      R_ipam = R8_uke
C FDB90_vent_log.fgi(  86, 422):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_emam,R_asam,REAL(1,4)
     &,
     & REAL(R_umam,4),REAL(R_apam,4),
     & REAL(R_amam,4),REAL(R_ulam,4),I_uram,
     & REAL(R_opam,4),L_upam,REAL(R_aram,4),L_eram,L_iram
     &,R_epam,
     & REAL(R_omam,4),REAL(R_imam,4),L_oram,REAL(R_ipam,4
     &))
      !}
C FDB90_vlv.fgi( 133, 212):���������� �������,20FDB91CP001XQ01
      R_esep = R8_ale
C FDB90_vent_log.fgi(  32, 395):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_arep,R_utep,REAL(1,4)
     &,
     & REAL(R_orep,4),REAL(R_urep,4),
     & REAL(R_upep,4),REAL(R_opep,4),I_otep,
     & REAL(R_isep,4),L_osep,REAL(R_usep,4),L_atep,L_etep
     &,R_asep,
     & REAL(R_irep,4),REAL(R_erep,4),L_itep,REAL(R_esep,4
     &))
      !}
C FDB90_vlv.fgi(  47,  48):���������� �������,20FDB93CT001XQ01
      R_udef = R8_ele
C FDB90_vent_log.fgi(  32, 401):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_obef,R_ikef,REAL(1,4)
     &,
     & REAL(R_edef,4),REAL(R_idef,4),
     & REAL(R_ibef,4),REAL(R_ebef,4),I_ekef,
     & REAL(R_afef,4),L_efef,REAL(R_ifef,4),L_ofef,L_ufef
     &,R_odef,
     & REAL(R_adef,4),REAL(R_ubef,4),L_akef,REAL(R_udef,4
     &))
      !}
C FDB90_vlv.fgi( 192, 169):���������� �������,20FDB91CT191XQ01
      R_ekut = R8_ile
C FDB90_vent_log.fgi(  32, 404):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_afut,R_ulut,REAL(1,4)
     &,
     & REAL(R_ofut,4),REAL(R_ufut,4),
     & REAL(R_udut,4),REAL(R_odut,4),I_olut,
     & REAL(R_ikut,4),L_okut,REAL(R_ukut,4),L_alut,L_elut
     &,R_akut,
     & REAL(R_ifut,4),REAL(R_efut,4),L_ilut,REAL(R_ekut,4
     &))
      !}
C FDB90_vlv.fgi( 104, 127):���������� �������,20FDB92CT002XQ01
      R_avos = R8_ole
C FDB90_vent_log.fgi(  32, 407):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_usos,R_oxos,REAL(1,4)
     &,
     & REAL(R_itos,4),REAL(R_otos,4),
     & REAL(R_osos,4),REAL(R_isos,4),I_ixos,
     & REAL(R_evos,4),L_ivos,REAL(R_ovos,4),L_uvos,L_axos
     &,R_utos,
     & REAL(R_etos,4),REAL(R_atos,4),L_exos,REAL(R_avos,4
     &))
      !}
C FDB90_vlv.fgi(  47, 169):���������� �������,20FDB92CT001XQ01
      R_ule = R8_ame
C FDB90_vent_log.fgi(  32, 410):��������
      R_ikif = R8_eme
C FDB90_vent_log.fgi(  32, 413):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_efif,R_amif,REAL(1,4)
     &,
     & REAL(R_ufif,4),REAL(R_akif,4),
     & REAL(R_afif,4),REAL(R_udif,4),I_ulif,
     & REAL(R_okif,4),L_ukif,REAL(R_alif,4),L_elif,L_ilif
     &,R_ekif,
     & REAL(R_ofif,4),REAL(R_ifif,4),L_olif,REAL(R_ikif,4
     &))
      !}
C FDB90_vlv.fgi( 162, 169):���������� �������,20FDB91CT051XQ01
      R_edem = R8_ime
C FDB90_vent_log.fgi(  32, 416):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_abem,R_ufem,REAL(1,4)
     &,
     & REAL(R_obem,4),REAL(R_ubem,4),
     & REAL(R_uxam,4),REAL(R_oxam,4),I_ofem,
     & REAL(R_idem,4),L_odem,REAL(R_udem,4),L_afem,L_efem
     &,R_adem,
     & REAL(R_ibem,4),REAL(R_ebem,4),L_ifem,REAL(R_edem,4
     &))
      !}
C FDB90_vlv.fgi( 133, 241):���������� �������,20FDB91CT031XQ01
      R_amel = R8_ome
C FDB90_vent_log.fgi(  32, 419):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ukel,R_opel,REAL(1,4)
     &,
     & REAL(R_ilel,4),REAL(R_olel,4),
     & REAL(R_okel,4),REAL(R_ikel,4),I_ipel,
     & REAL(R_emel,4),L_imel,REAL(R_omel,4),L_umel,L_apel
     &,R_ulel,
     & REAL(R_elel,4),REAL(R_alel,4),L_epel,REAL(R_amel,4
     &))
      !}
C FDB90_vlv.fgi( 162, 269):���������� �������,20FDB91CT021XQ01
      R_alap = R8_ume
C FDB90_vent_log.fgi(  32, 422):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ufap,R_omap,REAL(1,4)
     &,
     & REAL(R_ikap,4),REAL(R_okap,4),
     & REAL(R_ofap,4),REAL(R_ifap,4),I_imap,
     & REAL(R_elap,4),L_ilap,REAL(R_olap,4),L_ulap,L_amap
     &,R_ukap,
     & REAL(R_ekap,4),REAL(R_akap,4),L_emap,REAL(R_alap,4
     &))
      !}
C FDB90_vlv.fgi( 133, 255):���������� �������,20FDB91CT011XQ01
      !��������� R_(65) = FDB90_vent_logC?? /0.0/
      R_(65)=R0_ope
C FDB90_vent_log.fgi( 325, 477):���������
      !��������� R_(64) = FDB90_vent_logC?? /0.001/
      R_(64)=R0_upe
C FDB90_vent_log.fgi( 325, 475):���������
      !��������� R_(69) = FDB90_vent_logC?? /0.0/
      R_(69)=R0_ore
C FDB90_vent_log.fgi( 265, 476):���������
      !��������� R_(68) = FDB90_vent_logC?? /0.001/
      R_(68)=R0_ure
C FDB90_vent_log.fgi( 265, 474):���������
      !��������� R_(73) = FDB90_vent_logC?? /0.0/
      R_(73)=R0_ose
C FDB90_vent_log.fgi( 203, 477):���������
      !��������� R_(72) = FDB90_vent_logC?? /0.001/
      R_(72)=R0_use
C FDB90_vent_log.fgi( 203, 475):���������
      !��������� R_(77) = FDB90_vent_logC?? /0.0/
      R_(77)=R0_ote
C FDB90_vent_log.fgi( 143, 476):���������
      !��������� R_(76) = FDB90_vent_logC?? /0.001/
      R_(76)=R0_ute
C FDB90_vent_log.fgi( 143, 474):���������
      !��������� R_(81) = FDB90_vent_logC?? /0.0/
      R_(81)=R0_ove
C FDB90_vent_log.fgi( 454, 477):���������
      !��������� R_(80) = FDB90_vent_logC?? /0.001/
      R_(80)=R0_uve
C FDB90_vent_log.fgi( 454, 475):���������
      !��������� R_(85) = FDB90_vent_logC?? /0.0/
      R_(85)=R0_oxe
C FDB90_vent_log.fgi( 391, 477):���������
      !��������� R_(84) = FDB90_vent_logC?? /0.001/
      R_(84)=R0_uxe
C FDB90_vent_log.fgi( 391, 475):���������
      !��������� R_(89) = FDB90_vent_logC?? /0.0/
      R_(89)=R0_obi
C FDB90_vent_log.fgi(  85, 477):���������
      !��������� R_(88) = FDB90_vent_logC?? /0.001/
      R_(88)=R0_ubi
C FDB90_vent_log.fgi(  85, 475):���������
      !��������� R_(93) = FDB90_vent_logC?? /0.0/
      R_(93)=R0_odi
C FDB90_vent_log.fgi(  25, 476):���������
      !��������� R_(92) = FDB90_vent_logC?? /0.001/
      R_(92)=R0_udi
C FDB90_vent_log.fgi(  25, 474):���������
      !{
      Call DAT_ANA_HANDLER(deltat,R_upup,R_otup,REAL(1,4)
     &,
     & REAL(R_irup,4),REAL(R_orup,4),
     & REAL(R_opup,4),REAL(R_ipup,4),I_itup,
     & REAL(R_esup,4),L_isup,REAL(R_osup,4),L_usup,L_atup
     &,R_urup,
     & REAL(R_erup,4),REAL(R_arup,4),L_etup,REAL(R_asup,4
     &))
      !}
C FDB90_vlv.fgi(  74,  95):���������� �������,20FDB93CW002XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_uxar,R_ofer,REAL(1,4)
     &,
     & REAL(R_iber,4),REAL(R_ober,4),
     & REAL(R_oxar,4),REAL(R_ixar,4),I_ifer,
     & REAL(R_eder,4),L_ider,REAL(R_oder,4),L_uder,L_afer
     &,R_uber,
     & REAL(R_eber,4),REAL(R_aber,4),L_efer,REAL(R_ader,4
     &))
      !}
C FDB90_vlv.fgi(  47, 141):���������� �������,20FDB93CW001XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_imos,R_esos,REAL(1,4)
     &,
     & REAL(R_apos,4),REAL(R_epos,4),
     & REAL(R_emos,4),REAL(R_amos,4),I_asos,
     & REAL(R_upos,4),L_aros,REAL(R_eros,4),L_iros,L_oros
     &,R_ipos,
     & REAL(R_umos,4),REAL(R_omos,4),L_uros,REAL(R_opos,4
     &))
      !}
C FDB90_vlv.fgi(  47, 155):���������� �������,20FDB92CW002XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_okus,R_ipus,REAL(1,4)
     &,
     & REAL(R_elus,4),REAL(R_ilus,4),
     & REAL(R_ikus,4),REAL(R_ekus,4),I_epus,
     & REAL(R_amus,4),L_emus,REAL(R_imus,4),L_omus,L_umus
     &,R_olus,
     & REAL(R_alus,4),REAL(R_ukus,4),L_apus,REAL(R_ulus,4
     &))
      !}
C FDB90_vlv.fgi(  47, 198):���������� �������,20FDB92CW001XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_okox,R_ipox,REAL(1,4)
     &,
     & REAL(R_elox,4),REAL(R_ilox,4),
     & REAL(R_ikox,4),REAL(R_ekox,4),I_epox,
     & REAL(R_amox,4),L_emox,REAL(R_imox,4),L_omox,L_umox
     &,R_olox,
     & REAL(R_alox,4),REAL(R_ukox,4),L_apox,REAL(R_ulox,4
     &))
      !}
C FDB90_vlv.fgi( 104, 212):���������� �������,20FDB92CP107XQ01
      L_(27)=I_edade.ne.0
C FDB90_logic.fgi( 500, 157):��������� 1->LO
      L_(28)=I_edade.ne.0
C FDB90_logic.fgi( 502, 191):��������� 1->LO
      L_(31)=I_udade.ne.0
C FDB90_logic.fgi( 500, 167):��������� 1->LO
      L_(40)=I_akade.ne.0
C FDB90_logic.fgi( 502, 207):��������� 1->LO
      L_(43)=I_ekade.ne.0
C FDB90_logic.fgi( 500, 140):��������� 1->LO
      L_(50)=I_ikade.ne.0
C FDB90_logic.fgi( 500, 163):��������� 1->LO
      L_(51)=I_okade.ne.0
C FDB90_logic.fgi( 502, 196):��������� 1->LO
      R_(94) = 60
C FDB90_logic.fgi( 527, 202):��������� (RE4) (�������)
      R_(97) = 0.0
C FDB90_logic.fgi( 533, 196):��������� (RE4) (�������)
      L_(56)=I_apade.ne.0
C FDB90_logic.fgi( 500, 237):��������� 1->LO
      L_(57)=I_apade.ne.0
C FDB90_logic.fgi( 502, 271):��������� 1->LO
      L_(60)=I_opade.ne.0
C FDB90_logic.fgi( 500, 247):��������� 1->LO
      L_(69)=I_urade.ne.0
C FDB90_logic.fgi( 502, 287):��������� 1->LO
      L_(72)=I_asade.ne.0
C FDB90_logic.fgi( 500, 220):��������� 1->LO
      L_(79)=I_esade.ne.0
C FDB90_logic.fgi( 500, 243):��������� 1->LO
      L_(80)=I_isade.ne.0
C FDB90_logic.fgi( 502, 276):��������� 1->LO
      R_(98) = 60
C FDB90_logic.fgi( 527, 282):��������� (RE4) (�������)
      R_(101) = 0.0
C FDB90_logic.fgi( 533, 276):��������� (RE4) (�������)
      L_(85)=I_uvade.ne.0
C FDB90_logic.fgi( 500, 319):��������� 1->LO
      L_(86)=I_uvade.ne.0
C FDB90_logic.fgi( 502, 353):��������� 1->LO
      L_(89)=I_ixade.ne.0
C FDB90_logic.fgi( 500, 329):��������� 1->LO
      L_(98)=I_obede.ne.0
C FDB90_logic.fgi( 502, 369):��������� 1->LO
      L_(101)=I_ubede.ne.0
C FDB90_logic.fgi( 500, 302):��������� 1->LO
      L_(108)=I_adede.ne.0
C FDB90_logic.fgi( 500, 325):��������� 1->LO
      L_(109)=I_edede.ne.0
C FDB90_logic.fgi( 502, 358):��������� 1->LO
      R_(102) = 60
C FDB90_logic.fgi( 527, 364):��������� (RE4) (�������)
      R_(105) = 0.0
C FDB90_logic.fgi( 533, 358):��������� (RE4) (�������)
      L_(110)=I_ukede.ne.0
C FDB90_logic.fgi( 364,  49):��������� 1->LO
      L_(115)=I_alede.ne.0
C FDB90_logic.fgi( 364,  55):��������� 1->LO
      L_(113) = L_(110).OR.L_(115)
C FDB90_logic.fgi( 372,  61):���
      L_(116)=I_ilede.ne.0
C FDB90_logic.fgi( 364,  65):��������� 1->LO
      L0_okede=(L_(116).or.L0_okede).and..not.(L_(113))
      L_(114)=.not.L0_okede
C FDB90_logic.fgi( 378,  63):RS �������
      L_(111) = L_(116).OR.L_(110)
C FDB90_logic.fgi( 372,  51):���
      L0_ikede=(L_(115).or.L0_ikede).and..not.(L_(111))
      L_(112)=.not.L0_ikede
C FDB90_logic.fgi( 378,  53):RS �������
      R_(106) = 60
C FDB90_logic.fgi( 388,  76):��������� (RE4) (�������)
      R_(109) = 0.0
C FDB90_logic.fgi( 396,  67):��������� (RE4) (�������)
      if(L0_ikede) then
         R_(107)=R_(106)
      else
         R_(107)=R_(109)
      endif
C FDB90_logic.fgi( 400,  59):���� RE IN LO CH7
      if(L0_okede) then
         R_(108)=R_(106)
      else
         R_(108)=R_(109)
      endif
C FDB90_logic.fgi( 400,  77):���� RE IN LO CH7
      R_elede = R_(108) + (-R_(107))
C FDB90_logic.fgi( 406,  70):��������
      R_ulede=R_ulede+deltat/R0_akede*R_elede
      if(R_ulede.gt.R0_ufede) then
         R_ulede=R0_ufede
      elseif(R_ulede.lt.R0_ekede) then
         R_ulede=R0_ekede
      endif
C FDB90_logic.fgi( 422,  54):����������
      R_olede=R_ulede
C FDB90_logic.fgi( 446,  56):������,20FDB91AE019VX01
      L_(117)=I_apede.ne.0
C FDB90_logic.fgi( 231,  49):��������� 1->LO
      L_(122)=I_epede.ne.0
C FDB90_logic.fgi( 231,  55):��������� 1->LO
      L_(120) = L_(117).OR.L_(122)
C FDB90_logic.fgi( 239,  61):���
      L_(123)=I_opede.ne.0
C FDB90_logic.fgi( 231,  65):��������� 1->LO
      L0_umede=(L_(123).or.L0_umede).and..not.(L_(120))
      L_(121)=.not.L0_umede
C FDB90_logic.fgi( 245,  63):RS �������
      L_(118) = L_(123).OR.L_(117)
C FDB90_logic.fgi( 239,  51):���
      L0_omede=(L_(122).or.L0_omede).and..not.(L_(118))
      L_(119)=.not.L0_omede
C FDB90_logic.fgi( 245,  53):RS �������
      R_(110) = 60
C FDB90_logic.fgi( 255,  76):��������� (RE4) (�������)
      R_(113) = 0.0
C FDB90_logic.fgi( 263,  67):��������� (RE4) (�������)
      if(L0_omede) then
         R_(111)=R_(110)
      else
         R_(111)=R_(113)
      endif
C FDB90_logic.fgi( 267,  59):���� RE IN LO CH7
      if(L0_umede) then
         R_(112)=R_(110)
      else
         R_(112)=R_(113)
      endif
C FDB90_logic.fgi( 267,  77):���� RE IN LO CH7
      R_ipede = R_(112) + (-R_(111))
C FDB90_logic.fgi( 273,  70):��������
      R_arede=R_arede+deltat/R0_emede*R_ipede
      if(R_arede.gt.R0_amede) then
         R_arede=R0_amede
      elseif(R_arede.lt.R0_imede) then
         R_arede=R0_imede
      endif
C FDB90_logic.fgi( 289,  54):����������
      R_upede=R_arede
C FDB90_logic.fgi( 314,  56):������,20FDB91AE018VX01
      L_(128)=I_asede.ne.0
C FDB90_logic.fgi( 358, 167):��������� 1->LO
      L_(129)=I_asede.ne.0
C FDB90_logic.fgi( 360, 201):��������� 1->LO
      L_(132)=I_osede.ne.0
C FDB90_logic.fgi( 358, 177):��������� 1->LO
      L_(141)=I_utede.ne.0
C FDB90_logic.fgi( 360, 217):��������� 1->LO
      L_(144)=I_avede.ne.0
C FDB90_logic.fgi( 358, 150):��������� 1->LO
      L_(151)=I_evede.ne.0
C FDB90_logic.fgi( 358, 173):��������� 1->LO
      L_(152)=I_ivede.ne.0
C FDB90_logic.fgi( 360, 206):��������� 1->LO
      R_(114) = 60
C FDB90_logic.fgi( 385, 212):��������� (RE4) (�������)
      R_(117) = 0.0
C FDB90_logic.fgi( 391, 206):��������� (RE4) (�������)
      L_(157)=I_ubide.ne.0
C FDB90_logic.fgi( 358, 242):��������� 1->LO
      L_(158)=I_ubide.ne.0
C FDB90_logic.fgi( 360, 276):��������� 1->LO
      L_(161)=I_idide.ne.0
C FDB90_logic.fgi( 358, 252):��������� 1->LO
      L_(170)=I_ofide.ne.0
C FDB90_logic.fgi( 360, 292):��������� 1->LO
      L_(173)=I_ufide.ne.0
C FDB90_logic.fgi( 358, 225):��������� 1->LO
      L_(180)=I_akide.ne.0
C FDB90_logic.fgi( 358, 248):��������� 1->LO
      L_(181)=I_ekide.ne.0
C FDB90_logic.fgi( 360, 281):��������� 1->LO
      R_(118) = 60
C FDB90_logic.fgi( 385, 287):��������� (RE4) (�������)
      R_(121) = 0.0
C FDB90_logic.fgi( 391, 281):��������� (RE4) (�������)
      L_(186)=I_omide.ne.0
C FDB90_logic.fgi( 358, 319):��������� 1->LO
      L_(187)=I_omide.ne.0
C FDB90_logic.fgi( 360, 353):��������� 1->LO
      L_(190)=I_epide.ne.0
C FDB90_logic.fgi( 358, 329):��������� 1->LO
      L_(199)=I_iride.ne.0
C FDB90_logic.fgi( 360, 369):��������� 1->LO
      L_(202)=I_oride.ne.0
C FDB90_logic.fgi( 358, 302):��������� 1->LO
      L_(209)=I_uride.ne.0
C FDB90_logic.fgi( 358, 325):��������� 1->LO
      L_(210)=I_aside.ne.0
C FDB90_logic.fgi( 360, 358):��������� 1->LO
      R_(122) = 60
C FDB90_logic.fgi( 385, 364):��������� (RE4) (�������)
      R_(125) = 0.0
C FDB90_logic.fgi( 391, 358):��������� (RE4) (�������)
      L_(215)=I_ivide.ne.0
C FDB90_logic.fgi( 229, 165):��������� 1->LO
      L_(216)=I_ivide.ne.0
C FDB90_logic.fgi( 231, 199):��������� 1->LO
      L_(219)=I_axide.ne.0
C FDB90_logic.fgi( 229, 175):��������� 1->LO
      L_(228)=I_ebode.ne.0
C FDB90_logic.fgi( 231, 215):��������� 1->LO
      L_(231)=I_ibode.ne.0
C FDB90_logic.fgi( 229, 148):��������� 1->LO
      L_(238)=I_obode.ne.0
C FDB90_logic.fgi( 229, 171):��������� 1->LO
      L_(239)=I_ubode.ne.0
C FDB90_logic.fgi( 231, 204):��������� 1->LO
      R_(126) = 60
C FDB90_logic.fgi( 256, 210):��������� (RE4) (�������)
      R_(129) = 0.0
C FDB90_logic.fgi( 262, 204):��������� (RE4) (�������)
      L_(244)=I_ekode.ne.0
C FDB90_logic.fgi( 229, 243):��������� 1->LO
      L_(245)=I_ekode.ne.0
C FDB90_logic.fgi( 231, 277):��������� 1->LO
      L_(248)=I_ukode.ne.0
C FDB90_logic.fgi( 229, 253):��������� 1->LO
      L_(257)=I_ulode.ne.0
C FDB90_logic.fgi( 231, 293):��������� 1->LO
      L_(260)=I_ipude.ne.0
C FDB90_logic.fgi( 229, 226):��������� 1->LO
      L_(267)=I_opude.ne.0
C FDB90_logic.fgi( 229, 249):��������� 1->LO
      L_(268)=I_arude.ne.0
C FDB90_logic.fgi( 231, 282):��������� 1->LO
      R_(130) = 60
C FDB90_logic.fgi( 256, 288):��������� (RE4) (�������)
      R_(133) = 0.0
C FDB90_logic.fgi( 262, 282):��������� (RE4) (�������)
      L_(273)=I_arode.ne.0
C FDB90_logic.fgi( 229, 321):��������� 1->LO
      L_(274)=I_arode.ne.0
C FDB90_logic.fgi( 231, 355):��������� 1->LO
      L_(277)=I_isode.ne.0
C FDB90_logic.fgi( 229, 331):��������� 1->LO
      L_(286)=I_esode.ne.0
C FDB90_logic.fgi( 231, 371):��������� 1->LO
      L_(287)=I_otode.ne.0
C FDB90_logic.fgi( 231,  84):��������� 1->LO
      L_(292)=I_utode.ne.0
C FDB90_logic.fgi( 231,  90):��������� 1->LO
      L_(290) = L_(287).OR.L_(292)
C FDB90_logic.fgi( 239,  96):���
      L_(293)=I_evode.ne.0
C FDB90_logic.fgi( 231, 100):��������� 1->LO
      L0_itode=(L_(293).or.L0_itode).and..not.(L_(290))
      L_(291)=.not.L0_itode
C FDB90_logic.fgi( 245,  98):RS �������
      L_(288) = L_(293).OR.L_(287)
C FDB90_logic.fgi( 239,  86):���
      L0_etode=(L_(292).or.L0_etode).and..not.(L_(288))
      L_(289)=.not.L0_etode
C FDB90_logic.fgi( 245,  88):RS �������
      R_(134) = 60
C FDB90_logic.fgi( 255, 111):��������� (RE4) (�������)
      R_(137) = 0.0
C FDB90_logic.fgi( 263, 102):��������� (RE4) (�������)
      if(L0_etode) then
         R_(135)=R_(134)
      else
         R_(135)=R_(137)
      endif
C FDB90_logic.fgi( 267,  94):���� RE IN LO CH7
      if(L0_itode) then
         R_(136)=R_(134)
      else
         R_(136)=R_(137)
      endif
C FDB90_logic.fgi( 267, 112):���� RE IN LO CH7
      R_avode = R_(136) + (-R_(135))
C FDB90_logic.fgi( 273, 105):��������
      R_ovode=R_ovode+deltat/R0_usode*R_avode
      if(R_ovode.gt.R0_osode) then
         R_ovode=R0_osode
      elseif(R_ovode.lt.R0_atode) then
         R_ovode=R0_atode
      endif
C FDB90_logic.fgi( 289,  89):����������
      R_ivode=R_ovode
C FDB90_logic.fgi( 314,  91):������,20FDB91AE005VX01
      L_(294)=I_uxode.ne.0
C FDB90_logic.fgi( 366,  89):��������� 1->LO
      L_(299)=I_abude.ne.0
C FDB90_logic.fgi( 366,  95):��������� 1->LO
      L_(297) = L_(294).OR.L_(299)
C FDB90_logic.fgi( 374, 101):���
      L_(300)=I_ibude.ne.0
C FDB90_logic.fgi( 366, 105):��������� 1->LO
      L0_oxode=(L_(300).or.L0_oxode).and..not.(L_(297))
      L_(298)=.not.L0_oxode
C FDB90_logic.fgi( 380, 103):RS �������
      L_(295) = L_(300).OR.L_(294)
C FDB90_logic.fgi( 374,  91):���
      L0_ixode=(L_(299).or.L0_ixode).and..not.(L_(295))
      L_(296)=.not.L0_ixode
C FDB90_logic.fgi( 380,  93):RS �������
      R_(138) = 60
C FDB90_logic.fgi( 390, 116):��������� (RE4) (�������)
      R_(141) = 0.0
C FDB90_logic.fgi( 398, 107):��������� (RE4) (�������)
      if(L0_ixode) then
         R_(139)=R_(138)
      else
         R_(139)=R_(141)
      endif
C FDB90_logic.fgi( 402,  99):���� RE IN LO CH7
      if(L0_oxode) then
         R_(140)=R_(138)
      else
         R_(140)=R_(141)
      endif
C FDB90_logic.fgi( 402, 117):���� RE IN LO CH7
      R_ebude = R_(140) + (-R_(139))
C FDB90_logic.fgi( 408, 110):��������
      R_ubude=R_ubude+deltat/R0_axode*R_ebude
      if(R_ubude.gt.R0_uvode) then
         R_ubude=R0_uvode
      elseif(R_ubude.lt.R0_exode) then
         R_ubude=R0_exode
      endif
C FDB90_logic.fgi( 424,  94):����������
      R_obude=R_ubude
C FDB90_logic.fgi( 448,  96):������,20FDB91AE003VX01
      L_(301)=I_afude.ne.0
C FDB90_logic.fgi( 366, 122):��������� 1->LO
      L_(306)=I_efude.ne.0
C FDB90_logic.fgi( 366, 128):��������� 1->LO
      L_(304) = L_(301).OR.L_(306)
C FDB90_logic.fgi( 374, 134):���
      L_(307)=I_ofude.ne.0
C FDB90_logic.fgi( 366, 138):��������� 1->LO
      L0_udude=(L_(307).or.L0_udude).and..not.(L_(304))
      L_(305)=.not.L0_udude
C FDB90_logic.fgi( 380, 136):RS �������
      L_(302) = L_(307).OR.L_(301)
C FDB90_logic.fgi( 374, 124):���
      L0_odude=(L_(306).or.L0_odude).and..not.(L_(302))
      L_(303)=.not.L0_odude
C FDB90_logic.fgi( 380, 126):RS �������
      R_(142) = 60
C FDB90_logic.fgi( 390, 149):��������� (RE4) (�������)
      R_(145) = 0.0
C FDB90_logic.fgi( 398, 140):��������� (RE4) (�������)
      if(L0_odude) then
         R_(143)=R_(142)
      else
         R_(143)=R_(145)
      endif
C FDB90_logic.fgi( 402, 132):���� RE IN LO CH7
      if(L0_udude) then
         R_(144)=R_(142)
      else
         R_(144)=R_(145)
      endif
C FDB90_logic.fgi( 402, 150):���� RE IN LO CH7
      R_ifude = R_(144) + (-R_(143))
C FDB90_logic.fgi( 408, 143):��������
      R_akude=R_akude+deltat/R0_edude*R_ifude
      if(R_akude.gt.R0_adude) then
         R_akude=R0_adude
      elseif(R_akude.lt.R0_idude) then
         R_akude=R0_idude
      endif
C FDB90_logic.fgi( 424, 127):����������
      R_ufude=R_akude
C FDB90_logic.fgi( 448, 129):������,20FDB91AE002VX01
      L_(308)=I_elude.ne.0
C FDB90_logic.fgi( 231, 115):��������� 1->LO
      L_(313)=I_ilude.ne.0
C FDB90_logic.fgi( 231, 121):��������� 1->LO
      L_(311) = L_(308).OR.L_(313)
C FDB90_logic.fgi( 239, 127):���
      L_(314)=I_ulude.ne.0
C FDB90_logic.fgi( 231, 131):��������� 1->LO
      L0_alude=(L_(314).or.L0_alude).and..not.(L_(311))
      L_(312)=.not.L0_alude
C FDB90_logic.fgi( 245, 129):RS �������
      L_(309) = L_(314).OR.L_(308)
C FDB90_logic.fgi( 239, 117):���
      L0_ukude=(L_(313).or.L0_ukude).and..not.(L_(309))
      L_(310)=.not.L0_ukude
C FDB90_logic.fgi( 245, 119):RS �������
      R_(146) = 60
C FDB90_logic.fgi( 255, 142):��������� (RE4) (�������)
      R_(149) = 0.0
C FDB90_logic.fgi( 263, 133):��������� (RE4) (�������)
      if(L0_ukude) then
         R_(147)=R_(146)
      else
         R_(147)=R_(149)
      endif
C FDB90_logic.fgi( 267, 125):���� RE IN LO CH7
      if(L0_alude) then
         R_(148)=R_(146)
      else
         R_(148)=R_(149)
      endif
C FDB90_logic.fgi( 267, 143):���� RE IN LO CH7
      R_olude = R_(148) + (-R_(147))
C FDB90_logic.fgi( 273, 136):��������
      R_emude=R_emude+deltat/R0_ikude*R_olude
      if(R_emude.gt.R0_ekude) then
         R_emude=R0_ekude
      elseif(R_emude.lt.R0_okude) then
         R_emude=R0_okude
      endif
C FDB90_logic.fgi( 289, 120):����������
      R_amude=R_emude
C FDB90_logic.fgi( 314, 122):������,20FDB91AE001VX01
      L_(317)=I_orude.ne.0
C FDB90_logic.fgi( 229, 304):��������� 1->LO
      L_(324)=I_urude.ne.0
C FDB90_logic.fgi( 229, 327):��������� 1->LO
      L_(325)=I_esude.ne.0
C FDB90_logic.fgi( 231, 360):��������� 1->LO
      R_(150) = 60
C FDB90_logic.fgi( 256, 366):��������� (RE4) (�������)
      R_(153) = 0.0
C FDB90_logic.fgi( 262, 360):��������� (RE4) (�������)
      I_(2) = z'01000007'
C FDB90_logic.fgi( 147, 337):��������� ������������� IN (�������)
      I_(1) = z'0100000A'
C FDB90_logic.fgi( 147, 335):��������� ������������� IN (�������)
      I_(4) = z'01000007'
C FDB90_logic.fgi( 147, 347):��������� ������������� IN (�������)
      I_(3) = z'0100000A'
C FDB90_logic.fgi( 147, 345):��������� ������������� IN (�������)
      I_(6) = z'01000007'
C FDB90_logic.fgi( 147, 357):��������� ������������� IN (�������)
      I_(5) = z'0100000A'
C FDB90_logic.fgi( 147, 355):��������� ������������� IN (�������)
      C30_itude = '������������������'
C FDB90_logic.fgi( 108, 367):��������� ���������� CH20 (CH30) (�������)
      C30_otude = '������������'
C FDB90_logic.fgi( 129, 371):��������� ���������� CH20 (CH30) (�������)
      C30_utude = '������'
C FDB90_logic.fgi( 123, 366):��������� ���������� CH20 (CH30) (�������)
      C30_ivude = '�� ������'
C FDB90_logic.fgi( 108, 369):��������� ���������� CH20 (CH30) (�������)
      L_(335)=.true.
C FDB90_logic.fgi(  75, 343):��������� ���������� (�������)
      L0_exude=R0_oxude.ne.R0_ixude
      R0_ixude=R0_oxude
C FDB90_logic.fgi(  62, 336):���������� ������������� ������
      if(L0_exude) then
         L_(332)=L_(335)
      else
         L_(332)=.false.
      endif
C FDB90_logic.fgi(  79, 342):���� � ������������� �������
      L_(3)=L_(332)
C FDB90_logic.fgi(  79, 342):������-�������: ���������� ��� �������������� ������
      L_(336)=.true.
C FDB90_logic.fgi(  75, 360):��������� ���������� (�������)
      L0_ebafe=R0_obafe.ne.R0_ibafe
      R0_ibafe=R0_obafe
C FDB90_logic.fgi(  62, 353):���������� ������������� ������
      if(L0_ebafe) then
         L_(333)=L_(336)
      else
         L_(333)=.false.
      endif
C FDB90_logic.fgi(  79, 359):���� � ������������� �������
      L_(2)=L_(333)
C FDB90_logic.fgi(  79, 359):������-�������: ���������� ��� �������������� ������
      L_(330) = L_(2).OR.L_(3)
C FDB90_logic.fgi(  89, 372):���
      L_(337)=.true.
C FDB90_logic.fgi(  75, 376):��������� ���������� (�������)
      L0_edafe=R0_odafe.ne.R0_idafe
      R0_idafe=R0_odafe
C FDB90_logic.fgi(  62, 369):���������� ������������� ������
      if(L0_edafe) then
         L_(334)=L_(337)
      else
         L_(334)=.false.
      endif
C FDB90_logic.fgi(  79, 375):���� � ������������� �������
      L_(1)=L_(334)
C FDB90_logic.fgi(  79, 375):������-�������: ���������� ��� �������������� ������
      L_(326) = L_(1).OR.L_(2)
C FDB90_logic.fgi(  89, 339):���
      L_axude=(L_(3).or.L_axude).and..not.(L_(326))
      L_(327)=.not.L_axude
C FDB90_logic.fgi(  97, 341):RS �������
      L_uvude=L_axude
C FDB90_logic.fgi( 113, 343):������,FDB9_tech_mode
      if(L_axude) then
         I_usude=I_(1)
      else
         I_usude=I_(2)
      endif
C FDB90_logic.fgi( 150, 335):���� RE IN LO CH7
      L_(328) = L_(1).OR.L_(3)
C FDB90_logic.fgi(  89, 356):���
      L_abafe=(L_(2).or.L_abafe).and..not.(L_(328))
      L_(329)=.not.L_abafe
C FDB90_logic.fgi(  97, 358):RS �������
      L_uxude=L_abafe
C FDB90_logic.fgi( 113, 360):������,FDB9_ruch_mode
      if(L_abafe) then
         I_atude=I_(3)
      else
         I_atude=I_(4)
      endif
C FDB90_logic.fgi( 150, 345):���� RE IN LO CH7
      L_adafe=(L_(1).or.L_adafe).and..not.(L_(330))
      L_(331)=.not.L_adafe
C FDB90_logic.fgi(  97, 374):RS �������
      L_ubafe=L_adafe
C FDB90_logic.fgi( 113, 376):������,FDB9_avt_mode
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_adof,4),L_ilof,L_olof
     &,R8_ovif,C30_abof,
     & L_uvude,L_uxude,L_ubafe,I_ikof,I_okof,R_ibof,R_ebof
     &,
     & R_osif,REAL(R_atif,4),R_utif,
     & REAL(R_evif,4),R_etif,REAL(R_otif,4),I_ofof,
     & I_ukof,I_ekof,I_akof,L_ivif,L_alof,L_imof,L_avif,
     & L_itif,L_ixif,L_exif,L_ulof,L_usif,L_uxif,
     & L_apof,L_elof,L_obof,L_ubof,REAL(R8_atebe,8),REAL(1.0
     &,4),R8_ubibe,
     & L_axif,L_uvif,L_omof,R_ifof,REAL(R_oxif,4),L_umof,L_epof
     &,
     & L_edof,L_idof,L_odof,L_afof,L_efof,L_udof)
      !}

      if(L_efof.or.L_afof.or.L_udof.or.L_odof.or.L_idof.or.L_edof
     &) then      
                  I_ufof = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ufof = z'40000000'
      endif
C FDB90_vlv.fgi( 337, 209):���� ���������� �������� ��������,20FDB91AA221
      R_abal = R_ifof * R_(4)
C FDB90_vent_log.fgi( 401, 375):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_uvuk,R_odal,REAL(1,4)
     &,
     & REAL(R_ixuk,4),REAL(R_oxuk,4),
     & REAL(R_ovuk,4),REAL(R_ivuk,4),I_idal,
     & REAL(R_ebal,4),L_ibal,REAL(R_obal,4),L_ubal,L_adal
     &,R_uxuk,
     & REAL(R_exuk,4),REAL(R_axuk,4),L_edal,REAL(R_abal,4
     &))
      !}
C FDB90_vlv.fgi( 162, 198):���������� �������,20FDB91AA221XQ01
      !{
      Call BVALVE_MAN(deltat,REAL(R_ivof,4),L_uduf,L_afuf
     &,R8_isof,L_uvude,
     & L_uxude,L_ubafe,I_ubuf,I_aduf,R_ipof,
     & REAL(R_upof,4),R_orof,REAL(R_asof,4),
     & R_arof,REAL(R_irof,4),I_abuf,I_eduf,I_obuf,I_ibuf,L_esof
     &,
     & L_iduf,L_ufuf,L_urof,L_erof,
     & L_etof,L_atof,L_efuf,L_opof,L_otof,L_ikuf,L_oduf,
     & L_avof,L_evof,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_usof,
     & L_osof,L_akuf,R_uxof,REAL(R_itof,4),L_ekuf,L_okuf,L_ovof
     &,
     & L_uvof,L_axof,L_ixof,L_oxof,L_exof)
      !}

      if(L_oxof.or.L_ixof.or.L_exof.or.L_axof.or.L_uvof.or.L_ovof
     &) then      
                  I_ebuf = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ebuf = z'40000000'
      endif
C FDB90_vlv.fgi( 402, 209):���� ���������� ������ �������,20FDB91AA125
      !{
      Call BVALVE_MAN(deltat,REAL(R_uruf,4),L_exuf,L_ixuf
     &,R8_umuf,L_uvude,
     & L_uxude,L_ubafe,I_evuf,I_ivuf,R_ukuf,
     & REAL(R_eluf,4),R_amuf,REAL(R_imuf,4),
     & R_iluf,REAL(R_uluf,4),I_ituf,I_ovuf,I_avuf,I_utuf,L_omuf
     &,
     & L_uvuf,L_ebak,L_emuf,L_oluf,
     & L_opuf,L_ipuf,L_oxuf,L_aluf,L_aruf,L_ubak,L_axuf,
     & L_iruf,L_oruf,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_epuf,
     & L_apuf,L_ibak,R_etuf,REAL(R_upuf,4),L_obak,L_adak,L_asuf
     &,
     & L_esuf,L_isuf,L_usuf,L_atuf,L_osuf)
      !}

      if(L_atuf.or.L_usuf.or.L_osuf.or.L_isuf.or.L_esuf.or.L_asuf
     &) then      
                  I_otuf = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_otuf = z'40000000'
      endif
C FDB90_vlv.fgi( 386, 183):���� ���������� ������ �������,20FDB91AA124
      !{
      Call BVALVE_MAN(deltat,REAL(R_emak,4),L_osak,L_usak
     &,R8_ekak,L_uvude,
     & L_uxude,L_ubafe,I_orak,I_urak,R_edak,
     & REAL(R_odak,4),R_ifak,REAL(R_ufak,4),
     & R_udak,REAL(R_efak,4),I_upak,I_asak,I_irak,I_erak,L_akak
     &,
     & L_esak,L_otak,L_ofak,L_afak,
     & L_alak,L_ukak,L_atak,L_idak,L_ilak,L_evak,L_isak,
     & L_ulak,L_amak,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_okak,
     & L_ikak,L_utak,R_opak,REAL(R_elak,4),L_avak,L_ivak,L_imak
     &,
     & L_omak,L_umak,L_epak,L_ipak,L_apak)
      !}

      if(L_ipak.or.L_epak.or.L_apak.or.L_umak.or.L_omak.or.L_imak
     &) then      
                  I_arak = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_arak = z'40000000'
      endif
C FDB90_vlv.fgi( 419, 183):���� ���������� ������ �������,20FDB91AA123
      !{
      Call BVALVE_MAN(deltat,REAL(R_ofek,4),L_apek,L_epek
     &,R8_obek,L_uvude,
     & L_uxude,L_ubafe,I_amek,I_emek,R_ovak,
     & REAL(R_axak,4),R_uxak,REAL(R_ebek,4),
     & R_exak,REAL(R_oxak,4),I_elek,I_imek,I_ulek,I_olek,L_ibek
     &,
     & L_omek,L_arek,L_abek,L_ixak,
     & L_idek,L_edek,L_ipek,L_uvak,L_udek,L_orek,L_umek,
     & L_efek,L_ifek,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_adek,
     & L_ubek,L_erek,R_alek,REAL(R_odek,4),L_irek,L_urek,L_ufek
     &,
     & L_akek,L_ekek,L_okek,L_ukek,L_ikek)
      !}

      if(L_ukek.or.L_okek.or.L_ikek.or.L_ekek.or.L_akek.or.L_ufek
     &) then      
                  I_ilek = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ilek = z'40000000'
      endif
C FDB90_vlv.fgi( 371, 183):���� ���������� ������ �������,20FDB91AA122
      !{
      Call BVALVE_MAN(deltat,REAL(R_abik,4),L_ikik,L_okik
     &,R8_avek,L_uvude,
     & L_uxude,L_ubafe,I_ifik,I_ofik,R_asek,
     & REAL(R_isek,4),R_etek,REAL(R_otek,4),
     & R_osek,REAL(R_atek,4),I_odik,I_ufik,I_efik,I_afik,L_utek
     &,
     & L_akik,L_ilik,L_itek,L_usek,
     & L_uvek,L_ovek,L_ukik,L_esek,L_exek,L_amik,L_ekik,
     & L_oxek,L_uxek,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_ivek,
     & L_evek,L_olik,R_idik,REAL(R_axek,4),L_ulik,L_emik,L_ebik
     &,
     & L_ibik,L_obik,L_adik,L_edik,L_ubik)
      !}

      if(L_edik.or.L_adik.or.L_ubik.or.L_obik.or.L_ibik.or.L_ebik
     &) then      
                  I_udik = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_udik = z'40000000'
      endif
C FDB90_vlv.fgi( 402, 183):���� ���������� ������ �������,20FDB91AA121
      !{
      Call BVALVE_MAN(deltat,REAL(R_itik,4),L_ubok,L_adok
     &,R8_irik,L_uvude,
     & L_uxude,L_ubafe,I_uxik,I_abok,R_imik,
     & REAL(R_umik,4),R_opik,REAL(R_arik,4),
     & R_apik,REAL(R_ipik,4),I_axik,I_ebok,I_oxik,I_ixik,L_erik
     &,
     & L_ibok,L_udok,L_upik,L_epik,
     & L_esik,L_asik,L_edok,L_omik,L_osik,L_ifok,L_obok,
     & L_atik,L_etik,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_urik,
     & L_orik,L_afok,R_uvik,REAL(R_isik,4),L_efok,L_ofok,L_otik
     &,
     & L_utik,L_avik,L_ivik,L_ovik,L_evik)
      !}

      if(L_ovik.or.L_ivik.or.L_evik.or.L_avik.or.L_utik.or.L_otik
     &) then      
                  I_exik = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_exik = z'40000000'
      endif
C FDB90_vlv.fgi( 419, 209):���� ���������� ������ �������,20FDB91AA126
      !{
      Call BVALVE_MAN(deltat,REAL(R_edul,4),L_olul,L_ulul
     &,R8_exol,L_uvude,
     & L_uxude,L_ubafe,I_okul,I_ukul,R_etol,
     & REAL(R_otol,4),R_ivol,REAL(R_uvol,4),
     & R_utol,REAL(R_evol,4),I_uful,I_alul,I_ikul,I_ekul,L_axol
     &,
     & L_elul,L_omul,L_ovol,L_avol,
     & L_abul,L_uxol,L_amul,L_itol,L_ibul,L_epul,L_ilul,
     & L_ubul,L_adul,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_oxol,
     & L_ixol,L_umul,R_oful,REAL(R_ebul,4),L_apul,L_ipul,L_idul
     &,
     & L_odul,L_udul,L_eful,L_iful,L_aful)
      !}

      if(L_iful.or.L_eful.or.L_aful.or.L_udul.or.L_odul.or.L_idul
     &) then      
                  I_akul = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_akul = z'40000000'
      endif
C FDB90_vlv.fgi( 354, 183):���� ���������� ������ �������,20FDB91AA113
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_irem,4),L_uvem,L_axem
     &,R8_amem,C30_ipem,
     & L_uvude,L_uxude,L_ubafe,I_utem,I_avem,R_upem,R_opem
     &,
     & R_akem,REAL(R_ikem,4),R_elem,
     & REAL(R_olem,4),R_okem,REAL(R_alem,4),I_atem,
     & I_evem,I_otem,I_item,L_ulem,L_ivem,L_uxem,L_ilem,
     & L_ukem,L_umem,L_omem,L_exem,L_ekem,L_epem,
     & L_ibim,L_ovem,L_arem,L_erem,REAL(R8_atebe,8),REAL(1.0
     &,4),R8_ubibe,
     & L_imem,L_emem,L_abim,R_usem,REAL(R_apem,4),L_ebim,L_obim
     &,
     & L_orem,L_urem,L_asem,L_isem,L_osem,L_esem)
      !}

      if(L_osem.or.L_isem.or.L_esem.or.L_asem.or.L_urem.or.L_orem
     &) then      
                  I_etem = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_etem = z'40000000'
      endif
C FDB90_vlv.fgi( 320, 209):���� ���������� �������� ��������,20FDB91AA211
      R_esul = R_usem * R_(3)
C FDB90_vent_log.fgi( 401, 369):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_arul,R_utul,REAL(1,4)
     &,
     & REAL(R_orul,4),REAL(R_urul,4),
     & REAL(R_upul,4),REAL(R_opul,4),I_otul,
     & REAL(R_isul,4),L_osul,REAL(R_usul,4),L_atul,L_etul
     &,R_asul,
     & REAL(R_irul,4),REAL(R_erul,4),L_itul,REAL(R_esul,4
     &))
      !}
C FDB90_vlv.fgi( 133, 169):���������� �������,20FDB91AA211XQ01
      !{
      Call BVALVE_MAN(deltat,REAL(R_ulim,4),L_esim,L_isim
     &,R8_ufim,L_uvude,
     & L_uxude,L_ubafe,I_erim,I_irim,R_ubim,
     & REAL(R_edim,4),R_afim,REAL(R_ifim,4),
     & R_idim,REAL(R_udim,4),I_ipim,I_orim,I_arim,I_upim,L_ofim
     &,
     & L_urim,L_etim,L_efim,L_odim,
     & L_okim,L_ikim,L_osim,L_adim,L_alim,L_utim,L_asim,
     & L_ilim,L_olim,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_ekim,
     & L_akim,L_itim,R_epim,REAL(R_ukim,4),L_otim,L_avim,L_amim
     &,
     & L_emim,L_imim,L_umim,L_apim,L_omim)
      !}

      if(L_apim.or.L_umim.or.L_omim.or.L_imim.or.L_emim.or.L_amim
     &) then      
                  I_opim = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_opim = z'40000000'
      endif
C FDB90_vlv.fgi( 337, 183):���� ���������� ������ �������,20FDB91AA115
      !{
      Call BVALVE_MAN(deltat,REAL(R_efom,4),L_omom,L_umom
     &,R8_ebom,L_uvude,
     & L_uxude,L_ubafe,I_olom,I_ulom,R_evim,
     & REAL(R_ovim,4),R_ixim,REAL(R_uxim,4),
     & R_uvim,REAL(R_exim,4),I_ukom,I_amom,I_ilom,I_elom,L_abom
     &,
     & L_emom,L_opom,L_oxim,L_axim,
     & L_adom,L_ubom,L_apom,L_ivim,L_idom,L_erom,L_imom,
     & L_udom,L_afom,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_obom,
     & L_ibom,L_upom,R_okom,REAL(R_edom,4),L_arom,L_irom,L_ifom
     &,
     & L_ofom,L_ufom,L_ekom,L_ikom,L_akom)
      !}

      if(L_ikom.or.L_ekom.or.L_akom.or.L_ufom.or.L_ofom.or.L_ifom
     &) then      
                  I_alom = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_alom = z'40000000'
      endif
C FDB90_vlv.fgi( 320, 183):���� ���������� ������ �������,20FDB91AA114
      !{
      Call BVALVE_MAN(deltat,REAL(R_oxom,4),L_akum,L_ekum
     &,R8_otom,L_uvude,
     & L_uxude,L_ubafe,I_afum,I_efum,R_orom,
     & REAL(R_asom,4),R_usom,REAL(R_etom,4),
     & R_esom,REAL(R_osom,4),I_edum,I_ifum,I_udum,I_odum,L_itom
     &,
     & L_ofum,L_alum,L_atom,L_isom,
     & L_ivom,L_evom,L_ikum,L_urom,L_uvom,L_olum,L_ufum,
     & L_exom,L_ixom,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_avom,
     & L_utom,L_elum,R_adum,REAL(R_ovom,4),L_ilum,L_ulum,L_uxom
     &,
     & L_abum,L_ebum,L_obum,L_ubum,L_ibum)
      !}

      if(L_ubum.or.L_obum.or.L_ibum.or.L_ebum.or.L_abum.or.L_uxom
     &) then      
                  I_idum = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_idum = z'40000000'
      endif
C FDB90_vlv.fgi( 303, 183):���� ���������� ������ �������,20FDB91AA111
      !{
      Call BVALVE_MAN(deltat,REAL(R_atum,4),L_ibap,L_obap
     &,R8_arum,L_uvude,
     & L_uxude,L_ubafe,I_ixum,I_oxum,R_amum,
     & REAL(R_imum,4),R_epum,REAL(R_opum,4),
     & R_omum,REAL(R_apum,4),I_ovum,I_uxum,I_exum,I_axum,L_upum
     &,
     & L_abap,L_idap,L_ipum,L_umum,
     & L_urum,L_orum,L_ubap,L_emum,L_esum,L_afap,L_ebap,
     & L_osum,L_usum,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_irum,
     & L_erum,L_odap,R_ivum,REAL(R_asum,4),L_udap,L_efap,L_etum
     &,
     & L_itum,L_otum,L_avum,L_evum,L_utum)
      !}

      if(L_evum.or.L_avum.or.L_utum.or.L_otum.or.L_itum.or.L_etum
     &) then      
                  I_uvum = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_uvum = z'40000000'
      endif
C FDB90_vlv.fgi( 285, 183):���� ���������� ������ �������,20FDB91AA116
      !{
      Call BVALVE_MAN(deltat,REAL(R_edep,4),L_olep,L_ulep
     &,R8_exap,L_uvude,
     & L_uxude,L_ubafe,I_okep,I_ukep,R_etap,
     & REAL(R_otap,4),R_ivap,REAL(R_uvap,4),
     & R_utap,REAL(R_evap,4),I_ufep,I_alep,I_ikep,I_ekep,L_axap
     &,
     & L_elep,L_omep,L_ovap,L_avap,
     & L_abep,L_uxap,L_amep,L_itap,L_ibep,L_epep,L_ilep,
     & L_ubep,L_adep,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_oxap,
     & L_ixap,L_umep,R_ofep,REAL(R_ebep,4),L_apep,L_ipep,L_idep
     &,
     & L_odep,L_udep,L_efep,L_ifep,L_afep)
      !}

      if(L_ifep.or.L_efep.or.L_afep.or.L_udep.or.L_odep.or.L_idep
     &) then      
                  I_akep = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_akep = z'40000000'
      endif
C FDB90_vlv.fgi( 386, 209):���� ���������� ������ �������,20FDB91AA112
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_udud,4),L_emud,L_imud
     &,R8_ixod,C30_ubud,
     & L_uvude,L_uxude,L_ubafe,I_elud,I_ilud,R_edud,R_adud
     &,
     & R_itod,REAL(R_utod,4),R_ovod,
     & REAL(R_axod,4),R_avod,REAL(R_ivod,4),I_ikud,
     & I_olud,I_alud,I_ukud,L_exod,L_ulud,L_epud,L_uvod,
     & L_evod,L_ebud,L_abud,L_omud,L_otod,L_obud,
     & L_upud,L_amud,L_idud,L_odud,REAL(R8_atebe,8),REAL(1.0
     &,4),R8_ubibe,
     & L_uxod,L_oxod,L_ipud,R_ekud,REAL(R_ibud,4),L_opud,L_arud
     &,
     & L_afud,L_efud,L_ifud,L_ufud,L_akud,L_ofud)
      !}

      if(L_akud.or.L_ufud.or.L_ofud.or.L_ifud.or.L_efud.or.L_afud
     &) then      
                  I_okud = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_okud = z'40000000'
      endif
C FDB90_vlv.fgi( 353, 238):���� ���������� �������� ��������,20FDB91AA231
      R_oki = R_ekud * R_(1)
C FDB90_vent_log.fgi( 401, 358):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ifi,R_emi,REAL(1,4),
     & REAL(R_aki,4),REAL(R_eki,4),
     & REAL(R_efi,4),REAL(R_afi,4),I_ami,
     & REAL(R_uki,4),L_ali,REAL(R_eli,4),L_ili,L_oli,R_iki
     &,
     & REAL(R_ufi,4),REAL(R_ofi,4),L_uli,REAL(R_oki,4))
      !}
C FDB90_vlv.fgi( 133, 141):���������� �������,20FDB91AA231XQ01
      !{
      Call BVALVE_MAN(deltat,REAL(R_alod,4),L_irod,L_orod
     &,R8_afod,L_uvude,
     & L_uxude,L_ubafe,I_ipod,I_opod,R_abod,
     & REAL(R_ibod,4),R_edod,REAL(R_odod,4),
     & R_obod,REAL(R_adod,4),I_omod,I_upod,I_epod,I_apod,L_udod
     &,
     & L_arod,L_isod,L_idod,L_ubod,
     & L_ufod,L_ofod,L_urod,L_ebod,L_ekod,L_atod,L_erod,
     & L_okod,L_ukod,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_ifod,
     & L_efod,L_osod,R_imod,REAL(R_akod,4),L_usod,L_etod,L_elod
     &,
     & L_ilod,L_olod,L_amod,L_emod,L_ulod)
      !}

      if(L_emod.or.L_amod.or.L_ulod.or.L_olod.or.L_ilod.or.L_elod
     &) then      
                  I_umod = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_umod = z'40000000'
      endif
C FDB90_vlv.fgi( 370, 238):���� ���������� ������ �������,20FDB91AA134
      !{
      Call BVALVE_MAN(deltat,REAL(R_opid,4),L_avid,L_evid
     &,R8_olid,L_uvude,
     & L_uxude,L_ubafe,I_atid,I_etid,R_ofid,
     & REAL(R_akid,4),R_ukid,REAL(R_elid,4),
     & R_ekid,REAL(R_okid,4),I_esid,I_itid,I_usid,I_osid,L_ilid
     &,
     & L_otid,L_axid,L_alid,L_ikid,
     & L_imid,L_emid,L_ivid,L_ufid,L_umid,L_oxid,L_utid,
     & L_epid,L_ipid,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_amid,
     & L_ulid,L_exid,R_asid,REAL(R_omid,4),L_ixid,L_uxid,L_upid
     &,
     & L_arid,L_erid,L_orid,L_urid,L_irid)
      !}

      if(L_urid.or.L_orid.or.L_irid.or.L_erid.or.L_arid.or.L_upid
     &) then      
                  I_isid = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_isid = z'40000000'
      endif
C FDB90_vlv.fgi( 386, 238):���� ���������� ������ �������,20FDB91AA133
      !{Call KN_VLVR(deltat,REAL(R_okop,4),L_arop,L_erop,R8_odop
C ,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_okop,4),L_arop
     &,L_erop,R8_odop,C30_akop,
     & L_uvude,L_uxude,L_ubafe,I_apop,I_epop,R_oxip,
     & REAL(R_abop,4),R_ubop,REAL(R_edop,4),
     & R_ebop,REAL(R_obop,4),I_emop,I_ipop,I_umop,I_omop,
     & L_adop,L_ibop,L_ifop,L_efop,L_irop,
     & L_uxip,L_ufop,L_osop,L_afop,L_udop,L_isop,L_usop,
     & L_idop,L_opop,L_asop,L_upop,L_ekop,L_ikop,
     & REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe,L_esop,R_amop
     &,
     & REAL(R_ofop,4),L_ukop,L_alop,L_elop,L_olop,L_ulop,L_ilop
     &)
      !}

      if(L_ulop.or.L_olop.or.L_ilop.or.L_elop.or.L_alop.or.L_ukop
     &) then      
                  I_imop = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_imop = z'40000000'
      endif
C FDB90_vlv.fgi( 352, 263):���� ���������� ���������������� ��������,20FDB93AX004Y01
      !{
      Call BVALVE_MAN(deltat,REAL(R_eted,4),L_obid,L_ubid
     &,R8_ered,L_uvude,
     & L_uxude,L_ubafe,I_oxed,I_uxed,R_emed,
     & REAL(R_omed,4),R_iped,REAL(R_uped,4),
     & R_umed,REAL(R_eped,4),I_uved,I_abid,I_ixed,I_exed,L_ared
     &,
     & L_ebid,L_odid,L_oped,L_aped,
     & L_ased,L_ured,L_adid,L_imed,L_ised,L_efid,L_ibid,
     & L_used,L_ated,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_ored,
     & L_ired,L_udid,R_oved,REAL(R_esed,4),L_afid,L_ifid,L_ited
     &,
     & L_oted,L_uted,L_eved,L_ived,L_aved)
      !}

      if(L_ived.or.L_eved.or.L_aved.or.L_uted.or.L_oted.or.L_ited
     &) then      
                  I_axed = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_axed = z'40000000'
      endif
C FDB90_vlv.fgi( 402, 238):���� ���������� ������ �������,20FDB91AA135
      !{Call KN_VLVR(deltat,REAL(R_adup,4),L_ilup,L_olup,R8_axop
C ,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_adup,4),L_ilup
     &,L_olup,R8_axop,C30_ibup,
     & L_uvude,L_uxude,L_ubafe,I_ikup,I_okup,R_atop,
     & REAL(R_itop,4),R_evop,REAL(R_ovop,4),
     & R_otop,REAL(R_avop,4),I_ofup,I_ukup,I_ekup,I_akup,
     & L_ivop,L_utop,L_uxop,L_oxop,L_ulup,
     & L_etop,L_ebup,L_apup,L_ixop,L_exop,L_umup,L_epup,
     & L_uvop,L_alup,L_imup,L_elup,L_obup,L_ubup,
     & REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe,L_omup,R_ifup
     &,
     & REAL(R_abup,4),L_edup,L_idup,L_odup,L_afup,L_efup,L_udup
     &)
      !}

      if(L_efup.or.L_afup.or.L_udup.or.L_odup.or.L_idup.or.L_edup
     &) then      
                  I_ufup = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ufup = z'40000000'
      endif
C FDB90_vlv.fgi( 335, 263):���� ���������� ���������������� ��������,20FDB93AX003Y01
      !{
      Call BVALVE_MAN(deltat,REAL(R_uxad,4),L_eked,L_iked
     &,R8_utad,L_uvude,
     & L_uxude,L_ubafe,I_efed,I_ifed,R_urad,
     & REAL(R_esad,4),R_atad,REAL(R_itad,4),
     & R_isad,REAL(R_usad,4),I_ided,I_ofed,I_afed,I_uded,L_otad
     &,
     & L_ufed,L_eled,L_etad,L_osad,
     & L_ovad,L_ivad,L_oked,L_asad,L_axad,L_uled,L_aked,
     & L_ixad,L_oxad,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_evad,
     & L_avad,L_iled,R_eded,REAL(R_uvad,4),L_oled,L_amed,L_abed
     &,
     & L_ebed,L_ibed,L_ubed,L_aded,L_obed)
      !}

      if(L_aded.or.L_ubed.or.L_obed.or.L_ibed.or.L_ebed.or.L_abed
     &) then      
                  I_oded = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_oded = z'40000000'
      endif
C FDB90_vlv.fgi( 419, 238):���� ���������� ������ �������,20FDB91AA132
      !{
      Call BVALVE_MAN(deltat,REAL(R_ifad,4),L_umad,L_apad
     &,R8_ibad,L_uvude,
     & L_uxude,L_ubafe,I_ulad,I_amad,R_ivu,
     & REAL(R_uvu,4),R_oxu,REAL(R_abad,4),
     & R_axu,REAL(R_ixu,4),I_alad,I_emad,I_olad,I_ilad,L_ebad
     &,
     & L_imad,L_upad,L_uxu,L_exu,
     & L_edad,L_adad,L_epad,L_ovu,L_odad,L_irad,L_omad,
     & L_afad,L_efad,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_ubad,
     & L_obad,L_arad,R_ukad,REAL(R_idad,4),L_erad,L_orad,L_ofad
     &,
     & L_ufad,L_akad,L_ikad,L_okad,L_ekad)
      !}

      if(L_okad.or.L_ikad.or.L_ekad.or.L_akad.or.L_ufad.or.L_ofad
     &) then      
                  I_elad = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_elad = z'40000000'
      endif
C FDB90_vlv.fgi( 354, 209):���� ���������� ������ �������,20FDB91AA131
      !{Call KN_VLVR(deltat,REAL(R_uper,4),L_ever,L_iver,R8_uler
C ,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_uper,4),L_ever
     &,L_iver,R8_uler,C30_eper,
     & L_uvude,L_uxude,L_ubafe,I_eter,I_iter,R_ufer,
     & REAL(R_eker,4),R_aler,REAL(R_iler,4),
     & R_iker,REAL(R_uker,4),I_iser,I_oter,I_ater,I_user,
     & L_eler,L_oker,L_omer,L_imer,L_over,
     & L_aker,L_aper,L_uxer,L_emer,L_amer,L_oxer,L_abir,
     & L_oler,L_uter,L_exer,L_aver,L_iper,L_oper,
     & REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe,L_ixer,R_eser
     &,
     & REAL(R_umer,4),L_arer,L_erer,L_irer,L_urer,L_aser,L_orer
     &)
      !}

      if(L_aser.or.L_urer.or.L_orer.or.L_irer.or.L_erer.or.L_arer
     &) then      
                  I_oser = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_oser = z'40000000'
      endif
C FDB90_vlv.fgi( 319, 263):���� ���������� ���������������� ��������,20FDB93AX002Y01
      !{Call KN_VLVR(deltat,REAL(R_itur,4),L_ubas,L_adas,R8_irur
C ,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_itur,4),L_ubas
     &,L_adas,R8_irur,C30_usur,
     & L_uvude,L_uxude,L_ubafe,I_uxur,I_abas,R_imur,
     & REAL(R_umur,4),R_opur,REAL(R_arur,4),
     & R_apur,REAL(R_ipur,4),I_axur,I_ebas,I_oxur,I_ixur,
     & L_upur,L_epur,L_esur,L_asur,L_edas,
     & L_omur,L_osur,L_ifas,L_urur,L_orur,L_efas,L_ofas,
     & L_erur,L_ibas,L_udas,L_obas,L_atur,L_etur,
     & REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe,L_afas,R_uvur
     &,
     & REAL(R_isur,4),L_otur,L_utur,L_avur,L_ivur,L_ovur,L_evur
     &)
      !}

      if(L_ovur.or.L_ivur.or.L_evur.or.L_avur.or.L_utur.or.L_otur
     &) then      
                  I_exur = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_exur = z'40000000'
      endif
C FDB90_vlv.fgi( 336, 238):���� ���������� ���������������� ��������,20FDB91AX011Y01
      !{Call KN_VLVR(deltat,REAL(R_opas,4),L_avas,L_evas,R8_ulas
C ,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_opas,4),L_avas
     &,L_evas,R8_ulas,C30_apas,
     & L_uvude,L_uxude,L_ubafe,I_atas,I_etas,R_ufas,
     & REAL(R_ekas,4),R_alas,REAL(R_ilas,4),
     & R_ikas,REAL(R_ukas,4),I_esas,I_itas,I_usas,I_osas,
     & L_elas,L_okas,L_imas,L_emas,L_ivas,
     & L_akas,L_umas,L_oxas,L_amas,L_uxube,L_ixas,L_uxas,
     & L_olas,L_otas,L_axas,L_utas,L_epas,L_ipas,
     & REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe,L_exas,R_asas
     &,
     & REAL(R_omas,4),L_upas,L_aras,L_eras,L_oras,L_uras,L_iras
     &)
      !}

      if(L_uras.or.L_oras.or.L_iras.or.L_eras.or.L_aras.or.L_upas
     &) then      
                  I_isas = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_isas = z'40000000'
      endif
C FDB90_vlv.fgi( 320, 238):���� ���������� ���������������� ��������,20FDB91AX006Y01
      !{
      Call BVALVE_MAN(deltat,REAL(R_amu,4),L_isu,L_osu,R8_aku
     &,L_uvude,
     & L_uxude,L_ubafe,I_iru,I_oru,R_adu,
     & REAL(R_idu,4),R_efu,REAL(R_ofu,4),
     & R_odu,REAL(R_afu,4),I_opu,I_uru,I_eru,I_aru,L_ufu,
     & L_asu,L_itu,L_ifu,L_udu,
     & L_uku,L_oku,L_usu,L_edu,L_elu,L_avu,L_esu,
     & L_olu,L_ulu,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe,L_iku
     &,
     & L_eku,L_otu,R_ipu,REAL(R_alu,4),L_utu,L_evu,L_emu,
     & L_imu,L_omu,L_apu,L_epu,L_umu)
      !}

      if(L_epu.or.L_apu.or.L_umu.or.L_omu.or.L_imu.or.L_emu
     &) then      
                  I_upu = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_upu = z'40000000'
      endif
C FDB90_vlv.fgi( 371, 209):���� ���������� ������ �������,20FDB91AA136
      !{Call KN_VLVR(deltat,REAL(R_ukes,4),L_eres,L_ires,R8_afes
C ,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_ukes,4),L_eres
     &,L_ires,R8_afes,C30_ekes,
     & L_uvude,L_uxude,L_ubafe,I_epes,I_ipes,R_abes,
     & REAL(R_ibes,4),R_edes,REAL(R_odes,4),
     & R_obes,REAL(R_ades,4),I_imes,I_opes,I_apes,I_umes,
     & L_ides,L_ubes,L_ofes,L_ifes,L_ores,
     & L_ebes,L_akes,L_uses,L_efes,L_abade,L_oses,L_ates,
     & L_udes,L_upes,L_eses,L_ares,L_ikes,L_okes,
     & REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe,L_ises,R_emes
     &,
     & REAL(R_ufes,4),L_ales,L_eles,L_iles,L_ules,L_ames,L_oles
     &)
      !}

      if(L_ames.or.L_ules.or.L_oles.or.L_iles.or.L_eles.or.L_ales
     &) then      
                  I_omes = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_omes = z'40000000'
      endif
C FDB90_vlv.fgi( 303, 238):���� ���������� ���������������� ��������,20FDB91AX005Y01
      !{
      Call BVALVE_MAN(deltat,REAL(R_oket,4),L_aret,L_eret
     &,R8_odet,L_uvude,
     & L_uxude,L_ubafe,I_apet,I_epet,R_oxat,
     & REAL(R_abet,4),R_ubet,REAL(R_edet,4),
     & R_ebet,REAL(R_obet,4),I_emet,I_ipet,I_umet,I_omet,L_idet
     &,
     & L_opet,L_aset,L_adet,L_ibet,
     & L_ifet,L_efet,L_iret,L_uxat,L_ufet,L_oset,L_upet,
     & L_eket,L_iket,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_afet,
     & L_udet,L_eset,R_amet,REAL(R_ofet,4),L_iset,L_uset,L_uket
     &,
     & L_alet,L_elet,L_olet,L_ulet,L_ilet)
      !}

      if(L_ulet.or.L_olet.or.L_ilet.or.L_elet.or.L_alet.or.L_uket
     &) then      
                  I_imet = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_imet = z'40000000'
      endif
C FDB90_vlv.fgi( 268, 183):���� ���������� ������ �������,20FDB92AA103
      !{
      Call BVALVE_MAN(deltat,REAL(R_adit,4),L_ilit,L_olit
     &,R8_axet,L_uvude,
     & L_uxude,L_ubafe,I_ikit,I_okit,R_atet,
     & REAL(R_itet,4),R_evet,REAL(R_ovet,4),
     & R_otet,REAL(R_avet,4),I_ofit,I_ukit,I_ekit,I_akit,L_uvet
     &,
     & L_alit,L_imit,L_ivet,L_utet,
     & L_uxet,L_oxet,L_ulit,L_etet,L_ebit,L_apit,L_elit,
     & L_obit,L_ubit,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_ixet,
     & L_exet,L_omit,R_ifit,REAL(R_abit,4),L_umit,L_epit,L_edit
     &,
     & L_idit,L_odit,L_afit,L_efit,L_udit)
      !}

      if(L_efit.or.L_afit.or.L_udit.or.L_odit.or.L_idit.or.L_edit
     &) then      
                  I_ufit = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ufit = z'40000000'
      endif
C FDB90_vlv.fgi( 250, 183):���� ���������� ������ �������,20FDB92AA102
      !{
      Call BVALVE_MAN(deltat,REAL(R_ivit,4),L_udot,L_afot
     &,R8_isit,L_uvude,
     & L_uxude,L_ubafe,I_ubot,I_adot,R_ipit,
     & REAL(R_upit,4),R_orit,REAL(R_asit,4),
     & R_arit,REAL(R_irit,4),I_abot,I_edot,I_obot,I_ibot,L_esit
     &,
     & L_idot,L_ufot,L_urit,L_erit,
     & L_etit,L_atit,L_efot,L_opit,L_otit,L_ikot,L_odot,
     & L_avit,L_evit,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_usit,
     & L_osit,L_akot,R_uxit,REAL(R_itit,4),L_ekot,L_okot,L_ovit
     &,
     & L_uvit,L_axit,L_ixit,L_oxit,L_exit)
      !}

      if(L_oxit.or.L_ixit.or.L_exit.or.L_axit.or.L_uvit.or.L_ovit
     &) then      
                  I_ebot = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ebot = z'40000000'
      endif
C FDB90_vlv.fgi( 232, 183):���� ���������� ������ �������,20FDB92AA101
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_esot,4),L_oxot,L_uxot
     &,R8_umot,C30_erot,
     & L_uvude,L_uxude,L_ubafe,I_ovot,I_uvot,R_orot,R_irot
     &,
     & R_ukot,REAL(R_elot,4),R_amot,
     & REAL(R_imot,4),R_ilot,REAL(R_ulot,4),I_utot,
     & I_axot,I_ivot,I_evot,L_omot,L_exot,L_obut,L_emot,
     & L_olot,L_opot,L_ipot,L_abut,L_alot,L_arot,
     & L_edut,L_ixot,L_urot,L_asot,REAL(R8_atebe,8),REAL(1.0
     &,4),R8_ubibe,
     & L_epot,L_apot,L_ubut,R_otot,REAL(R_upot,4),L_adut,L_idut
     &,
     & L_isot,L_osot,L_usot,L_etot,L_itot,L_atot)
      !}

      if(L_itot.or.L_etot.or.L_atot.or.L_usot.or.L_osot.or.L_isot
     &) then      
                  I_avot = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_avot = z'40000000'
      endif
C FDB90_vlv.fgi( 303, 209):���� ���������� �������� ��������,20FDB92AA201
      R_ipat = R_otot * R_(5)
C FDB90_vent_log.fgi( 401, 381):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_emat,R_asat,REAL(1,4)
     &,
     & REAL(R_umat,4),REAL(R_apat,4),
     & REAL(R_amat,4),REAL(R_ulat,4),I_urat,
     & REAL(R_opat,4),L_upat,REAL(R_arat,4),L_erat,L_irat
     &,R_epat,
     & REAL(R_omat,4),REAL(R_imat,4),L_orat,REAL(R_ipat,4
     &))
      !}
C FDB90_vlv.fgi(  47, 255):���������� �������,20FDB92AA201XQ01
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_umev,4),L_etev,L_itev
     &,R8_ikev,C30_ulev,
     & L_uvude,L_uxude,L_ubafe,I_esev,I_isev,R_emev,R_amev
     &,
     & R_idev,REAL(R_udev,4),R_ofev,
     & REAL(R_akev,4),R_afev,REAL(R_ifev,4),I_irev,
     & I_osev,I_asev,I_urev,L_ekev,L_usev,L_evev,L_ufev,
     & L_efev,L_elev,L_alev,L_otev,L_odev,L_olev,
     & L_uvev,L_atev,L_imev,L_omev,REAL(R8_atebe,8),REAL(1.0
     &,4),R8_ubibe,
     & L_ukev,L_okev,L_ivev,R_erev,REAL(R_ilev,4),L_ovev,L_axev
     &,
     & L_apev,L_epev,L_ipev,L_upev,L_arev,L_opev)
      !}

      if(L_arev.or.L_upev.or.L_opev.or.L_ipev.or.L_epev.or.L_apev
     &) then      
                  I_orev = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_orev = z'40000000'
      endif
C FDB90_vlv.fgi( 285, 209):���� ���������� �������� ��������,20FDB92AA251
      R_esav = R_erev * R_(2)
C FDB90_vent_log.fgi( 401, 363):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_arav,R_utav,REAL(1,4)
     &,
     & REAL(R_orav,4),REAL(R_urav,4),
     & REAL(R_upav,4),REAL(R_opav,4),I_otav,
     & REAL(R_isav,4),L_osav,REAL(R_usav,4),L_atav,L_etav
     &,R_asav,
     & REAL(R_irav,4),REAL(R_erav,4),L_itav,REAL(R_esav,4
     &))
      !}
C FDB90_vlv.fgi(  74, 155):���������� �������,20FDB92AA251XQ01
      !{
      Call BVALVE_MAN(deltat,REAL(R_ekiv,4),L_opiv,L_upiv
     &,R8_ediv,L_uvude,
     & L_uxude,L_ubafe,I_omiv,I_umiv,R_exev,
     & REAL(R_oxev,4),R_ibiv,REAL(R_ubiv,4),
     & R_uxev,REAL(R_ebiv,4),I_uliv,I_apiv,I_imiv,I_emiv,L_adiv
     &,
     & L_epiv,L_oriv,L_obiv,L_abiv,
     & L_afiv,L_udiv,L_ariv,L_ixev,L_ifiv,L_esiv,L_ipiv,
     & L_ufiv,L_akiv,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_odiv,
     & L_idiv,L_uriv,R_oliv,REAL(R_efiv,4),L_asiv,L_isiv,L_ikiv
     &,
     & L_okiv,L_ukiv,L_eliv,L_iliv,L_aliv)
      !}

      if(L_iliv.or.L_eliv.or.L_aliv.or.L_ukiv.or.L_okiv.or.L_ikiv
     &) then      
                  I_amiv = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_amiv = z'40000000'
      endif
C FDB90_vlv.fgi( 268, 209):���� ���������� ������ �������,20FDB92AA153
      !{
      Call BVALVE_MAN(deltat,REAL(R_obov,4),L_alov,L_elov
     &,R8_oviv,L_uvude,
     & L_uxude,L_ubafe,I_akov,I_ekov,R_osiv,
     & REAL(R_ativ,4),R_utiv,REAL(R_eviv,4),
     & R_etiv,REAL(R_otiv,4),I_efov,I_ikov,I_ufov,I_ofov,L_iviv
     &,
     & L_okov,L_amov,L_aviv,L_itiv,
     & L_ixiv,L_exiv,L_ilov,L_usiv,L_uxiv,L_omov,L_ukov,
     & L_ebov,L_ibov,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_axiv,
     & L_uviv,L_emov,R_afov,REAL(R_oxiv,4),L_imov,L_umov,L_ubov
     &,
     & L_adov,L_edov,L_odov,L_udov,L_idov)
      !}

      if(L_udov.or.L_odov.or.L_idov.or.L_edov.or.L_adov.or.L_ubov
     &) then      
                  I_ifov = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ifov = z'40000000'
      endif
C FDB90_vlv.fgi( 250, 209):���� ���������� ������ �������,20FDB92AA152
      !{
      Call BVALVE_MAN(deltat,REAL(R_avov,4),L_iduv,L_oduv
     &,R8_asov,L_uvude,
     & L_uxude,L_ubafe,I_ibuv,I_obuv,R_apov,
     & REAL(R_ipov,4),R_erov,REAL(R_orov,4),
     & R_opov,REAL(R_arov,4),I_oxov,I_ubuv,I_ebuv,I_abuv,L_urov
     &,
     & L_aduv,L_ifuv,L_irov,L_upov,
     & L_usov,L_osov,L_uduv,L_epov,L_etov,L_akuv,L_eduv,
     & L_otov,L_utov,REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe
     &,L_isov,
     & L_esov,L_ofuv,R_ixov,REAL(R_atov,4),L_ufuv,L_ekuv,L_evov
     &,
     & L_ivov,L_ovov,L_axov,L_exov,L_uvov)
      !}

      if(L_exov.or.L_axov.or.L_uvov.or.L_ovov.or.L_ivov.or.L_evov
     &) then      
                  I_uxov = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_uxov = z'40000000'
      endif
C FDB90_vlv.fgi( 232, 209):���� ���������� ������ �������,20FDB92AA151
      !{Call KN_VLVR(deltat,REAL(R_iruv,4),L_uvuv,L_axuv,R8_imuv
C ,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_iruv,4),L_uvuv
     &,L_axuv,R8_imuv,C30_upuv,
     & L_uvude,L_uxude,L_ubafe,I_utuv,I_avuv,R_ikuv,
     & REAL(R_ukuv,4),R_oluv,REAL(R_amuv,4),
     & R_aluv,REAL(R_iluv,4),I_atuv,I_evuv,I_otuv,I_ituv,
     & L_uluv,L_eluv,L_epuv,L_apuv,L_exuv,
     & L_okuv,L_opuv,L_ibax,L_umuv,L_omuv,L_ebax,L_obax,
     & L_emuv,L_ivuv,L_uxuv,L_ovuv,L_aruv,L_eruv,
     & REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe,L_abax,R_usuv
     &,
     & REAL(R_ipuv,4),L_oruv,L_uruv,L_asuv,L_isuv,L_osuv,L_esuv
     &)
      !}

      if(L_osuv.or.L_isuv.or.L_esuv.or.L_asuv.or.L_uruv.or.L_oruv
     &) then      
                  I_etuv = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_etuv = z'40000000'
      endif
C FDB90_vlv.fgi( 285, 238):���� ���������� ���������������� ��������,20FDB92AX104Y01
      !{Call KN_VLVR(deltat,REAL(R_ulax,4),L_esax,L_isax,R8_ufax
C ,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_ulax,4),L_esax
     &,L_isax,R8_ufax,C30_elax,
     & L_uvude,L_uxude,L_ubafe,I_erax,I_irax,R_ubax,
     & REAL(R_edax,4),R_afax,REAL(R_ifax,4),
     & R_idax,REAL(R_udax,4),I_ipax,I_orax,I_arax,I_upax,
     & L_efax,L_odax,L_okax,L_ikax,L_osax,
     & L_adax,L_alax,L_utax,L_ekax,L_akax,L_otax,L_avax,
     & L_ofax,L_urax,L_etax,L_asax,L_ilax,L_olax,
     & REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe,L_itax,R_epax
     &,
     & REAL(R_ukax,4),L_amax,L_emax,L_imax,L_umax,L_apax,L_omax
     &)
      !}

      if(L_apax.or.L_umax.or.L_omax.or.L_imax.or.L_emax.or.L_amax
     &) then      
                  I_opax = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_opax = z'40000000'
      endif
C FDB90_vlv.fgi( 268, 238):���� ���������� ���������������� ��������,20FDB92AX105Y01
      !{Call KN_VLVR(deltat,REAL(R_efex,4),L_omex,L_umex,R8_ebex
C ,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_efex,4),L_omex
     &,L_umex,R8_ebex,C30_odex,
     & L_uvude,L_uxude,L_ubafe,I_olex,I_ulex,R_evax,
     & REAL(R_ovax,4),R_ixax,REAL(R_uxax,4),
     & R_uvax,REAL(R_exax,4),I_ukex,I_amex,I_ilex,I_elex,
     & L_oxax,L_axax,L_adex,L_ubex,L_apex,
     & L_ivax,L_idex,L_erex,L_obex,L_ibex,L_arex,L_irex,
     & L_abex,L_emex,L_opex,L_imex,L_udex,L_afex,
     & REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe,L_upex,R_okex
     &,
     & REAL(R_edex,4),L_ifex,L_ofex,L_ufex,L_ekex,L_ikex,L_akex
     &)
      !}

      if(L_ikex.or.L_ekex.or.L_akex.or.L_ufex.or.L_ofex.or.L_ifex
     &) then      
                  I_alex = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_alex = z'40000000'
      endif
C FDB90_vlv.fgi( 250, 238):���� ���������� ���������������� ��������,20FDB92AX106Y01
      !{Call KN_VLVR(deltat,REAL(R_oxex,4),L_akix,L_ekix,R8_otex
C ,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_oxex,4),L_akix
     &,L_ekix,R8_otex,C30_axex,
     & L_uvude,L_uxude,L_ubafe,I_afix,I_efix,R_orex,
     & REAL(R_asex,4),R_usex,REAL(R_etex,4),
     & R_esex,REAL(R_osex,4),I_edix,I_ifix,I_udix,I_odix,
     & L_atex,L_isex,L_ivex,L_evex,L_ikix,
     & L_urex,L_uvex,L_olix,L_avex,L_utex,L_ilix,L_ulix,
     & L_itex,L_ofix,L_alix,L_ufix,L_exex,L_ixex,
     & REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe,L_elix,R_adix
     &,
     & REAL(R_ovex,4),L_uxex,L_abix,L_ebix,L_obix,L_ubix,L_ibix
     &)
      !}

      if(L_ubix.or.L_obix.or.L_ibix.or.L_ebix.or.L_abix.or.L_uxex
     &) then      
                  I_idix = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_idix = z'40000000'
      endif
C FDB90_vlv.fgi( 232, 238):���� ���������� ���������������� ��������,20FDB92AX107Y01
      !{Call KN_VLVR(deltat,REAL(R_usux,4),L_ebabe,L_ibabe
C ,R8_upux,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_usux,4),L_ebabe
     &,L_ibabe,R8_upux,C30_esux,
     & L_uvude,L_uxude,L_ubafe,I_exux,I_ixux,R_ulux,
     & REAL(R_emux,4),R_apux,REAL(R_ipux,4),
     & R_imux,REAL(R_umux,4),I_ivux,I_oxux,I_axux,I_uvux,
     & L_epux,L_omux,L_orux,L_irux,L_obabe,
     & L_amux,L_asux,L_udabe,L_erux,L_arux,L_odabe,L_afabe
     &,
     & L_opux,L_uxux,L_edabe,L_ababe,L_isux,L_osux,
     & REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe,L_idabe,R_evux
     &,
     & REAL(R_urux,4),L_atux,L_etux,L_itux,L_utux,L_avux,L_otux
     &)
      !}

      if(L_avux.or.L_utux.or.L_otux.or.L_itux.or.L_etux.or.L_atux
     &) then      
                  I_ovux = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ovux = z'40000000'
      endif
C FDB90_vlv.fgi( 285, 263):���� ���������� ���������������� ��������,20FDB92AX004Y01
      !{Call KN_VLVR(deltat,REAL(R_epabe,4),L_otabe,L_utabe
C ,R8_elabe,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_epabe,4),L_otabe
     &,L_utabe,R8_elabe,C30_omabe,
     & L_uvude,L_uxude,L_ubafe,I_osabe,I_usabe,R_efabe,
     & REAL(R_ofabe,4),R_ikabe,REAL(R_ukabe,4),
     & R_ufabe,REAL(R_ekabe,4),I_urabe,I_atabe,I_isabe,I_esabe
     &,
     & L_okabe,L_akabe,L_amabe,L_ulabe,L_avabe,
     & L_ifabe,L_imabe,L_exabe,L_olabe,L_ilabe,L_axabe,L_ixabe
     &,
     & L_alabe,L_etabe,L_ovabe,L_itabe,L_umabe,L_apabe,
     & REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe,L_uvabe,R_orabe
     &,
     & REAL(R_emabe,4),L_ipabe,L_opabe,L_upabe,L_erabe,L_irabe
     &,L_arabe)
      !}

      if(L_irabe.or.L_erabe.or.L_arabe.or.L_upabe.or.L_opabe.or.L_ipabe
     &) then      
                  I_asabe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_asabe = z'40000000'
      endif
C FDB90_vlv.fgi( 268, 263):���� ���������� ���������������� ��������,20FDB92AX005Y01
      !{Call KN_VLVR(deltat,REAL(R_okebe,4),L_arebe,L_erebe
C ,R8_odebe,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_okebe,4),L_arebe
     &,L_erebe,R8_odebe,C30_akebe,
     & L_uvude,L_uxude,L_ubafe,I_apebe,I_epebe,R_oxabe,
     & REAL(R_abebe,4),R_ubebe,REAL(R_edebe,4),
     & R_ebebe,REAL(R_obebe,4),I_emebe,I_ipebe,I_umebe,I_omebe
     &,
     & L_adebe,L_ibebe,L_ifebe,L_efebe,L_irebe,
     & L_uxabe,L_ufebe,L_osebe,L_afebe,L_udebe,L_isebe,L_usebe
     &,
     & L_idebe,L_opebe,L_asebe,L_upebe,L_ekebe,L_ikebe,
     & REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe,L_esebe,R_amebe
     &,
     & REAL(R_ofebe,4),L_ukebe,L_alebe,L_elebe,L_olebe,L_ulebe
     &,L_ilebe)
      !}

      if(L_ulebe.or.L_olebe.or.L_ilebe.or.L_elebe.or.L_alebe.or.L_ukebe
     &) then      
                  I_imebe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_imebe = z'40000000'
      endif
C FDB90_vlv.fgi( 250, 263):���� ���������� ���������������� ��������,20FDB92AX006Y01
      !{Call KN_VLVR(deltat,REAL(R_idibe,4),L_ulibe,L_amibe
C ,R8_exebe,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_idibe,4),L_ulibe
     &,L_amibe,R8_exebe,C30_obibe,
     & L_uvude,L_uxude,L_ubafe,I_ukibe,I_alibe,R_etebe,
     & REAL(R_otebe,4),R_ivebe,REAL(R_uvebe,4),
     & R_utebe,REAL(R_evebe,4),I_akibe,I_elibe,I_okibe,I_ikibe
     &,
     & L_ovebe,L_avebe,L_abibe,L_uxebe,L_emibe,
     & L_itebe,L_ibibe,L_ipibe,L_oxebe,L_ixebe,L_epibe,L_opibe
     &,
     & L_axebe,L_ilibe,L_umibe,L_olibe,L_adibe,L_edibe,
     & REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe,L_apibe,R_ufibe
     &,
     & REAL(R_ebibe,4),L_odibe,L_udibe,L_afibe,L_ifibe,L_ofibe
     &,L_efibe)
      !}

      if(L_ofibe.or.L_ifibe.or.L_efibe.or.L_afibe.or.L_udibe.or.L_odibe
     &) then      
                  I_ekibe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ekibe = z'40000000'
      endif
C FDB90_vlv.fgi( 232, 263):���� ���������� ���������������� ��������,20FDB92AX007Y01
      !{Call KN_VLVR(deltat,REAL(R_elir,4),L_orir,L_urir,R8_efir
C ,
      Call SOLENOIDVALVE_MAN(deltat,REAL(R_elir,4),L_orir
     &,L_urir,R8_efir,C30_okir,
     & L_uvude,L_uxude,L_ubafe,I_opir,I_upir,R_ebir,
     & REAL(R_obir,4),R_idir,REAL(R_udir,4),
     & R_ubir,REAL(R_edir,4),I_umir,I_arir,I_ipir,I_epir,
     & L_odir,L_adir,L_akir,L_ufir,L_asir,
     & L_ibir,L_ikir,L_etir,L_ofir,L_ifir,L_atir,L_itir,
     & L_afir,L_erir,L_osir,L_irir,L_ukir,L_alir,
     & REAL(R8_atebe,8),REAL(1.0,4),R8_ubibe,L_usir,R_omir
     &,
     & REAL(R_ekir,4),L_ilir,L_olir,L_ulir,L_emir,L_imir,L_amir
     &)
      !}

      if(L_imir.or.L_emir.or.L_amir.or.L_ulir.or.L_olir.or.L_ilir
     &) then      
                  I_apir = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_apir = z'40000000'
      endif
C FDB90_vlv.fgi( 303, 263):���� ���������� ���������������� ��������,20FDB93AX001Y01
      if(L_adafe) then
         C30_evude=C30_itude
      else
         C30_evude=C30_ivude
      endif
C FDB90_logic.fgi( 112, 367):���� RE IN LO CH20
      if(L_abafe) then
         C30_avude=C30_utude
      else
         C30_avude=C30_evude
      endif
C FDB90_logic.fgi( 127, 366):���� RE IN LO CH20
      if(L_axude) then
         C30_ovude=C30_otude
      else
         C30_ovude=C30_avude
      endif
C FDB90_logic.fgi( 136, 365):���� RE IN LO CH20
      if(L_adafe) then
         I_etude=I_(5)
      else
         I_etude=I_(6)
      endif
C FDB90_logic.fgi( 150, 355):���� RE IN LO CH7
      Call PUMP_HANDLER(deltat,C30_elil,I_umil,L_uvude,L_uxude
     &,L_ubafe,
     & I_apil,I_omil,R_akil,REAL(R_ikil,4),
     & R_ifil,REAL(R_ufil,4),I_epil,L_osil,L_uril,L_ivil,
     & L_ovil,L_amil,L_aril,L_iril,L_eril,L_oril,L_isil,L_efil
     &,
     & L_ekil,L_afil,L_ofil,L_util,L_(13),
     & L_avil,L_(12),L_udil,L_odil,L_emil,I_ipil,R_atil,R_etil
     &,
     & L_exil,L_afos,L_evil,REAL(R8_atebe,8),L_esil,
     & REAL(R8_asil,8),R_uvil,REAL(R_opil,4),R_upil,REAL(R8_ulil
     &,8),R_olil,
     & R8_ubibe,R_axil,R8_asil,REAL(R_okil,4),REAL(R_ukil
     &,4))
C FDB90_vlv.fgi( 266, 154):���������� ���������� �������,20FDB91CU001KN01
C label 999  try999=try999-1
C sav1=R_axil
C sav2=R_uvil
C sav3=L_osil
C sav4=L_isil
C sav5=L_uril
C sav6=R8_asil
C sav7=R_upil
C sav8=I_ipil
C sav9=I_epil
C sav10=I_apil
C sav11=I_umil
C sav12=I_omil
C sav13=I_imil
C sav14=L_emil
C sav15=L_amil
C sav16=R_olil
C sav17=C30_elil
C sav18=L_ekil
C sav19=L_ofil
      Call PUMP_HANDLER(deltat,C30_elil,I_umil,L_uvude,L_uxude
     &,L_ubafe,
     & I_apil,I_omil,R_akil,REAL(R_ikil,4),
     & R_ifil,REAL(R_ufil,4),I_epil,L_osil,L_uril,L_ivil,
     & L_ovil,L_amil,L_aril,L_iril,L_eril,L_oril,L_isil,L_efil
     &,
     & L_ekil,L_afil,L_ofil,L_util,L_(13),
     & L_avil,L_(12),L_udil,L_odil,L_emil,I_ipil,R_atil,R_etil
     &,
     & L_exil,L_afos,L_evil,REAL(R8_atebe,8),L_esil,
     & REAL(R8_asil,8),R_uvil,REAL(R_opil,4),R_upil,REAL(R8_ulil
     &,8),R_olil,
     & R8_ubibe,R_axil,R8_asil,REAL(R_okil,4),REAL(R_ukil
     &,4))
C FDB90_vlv.fgi( 266, 154):recalc:���������� ���������� �������,20FDB91CU001KN01
C if(sav1.ne.R_axil .and. try999.gt.0) goto 999
C if(sav2.ne.R_uvil .and. try999.gt.0) goto 999
C if(sav3.ne.L_osil .and. try999.gt.0) goto 999
C if(sav4.ne.L_isil .and. try999.gt.0) goto 999
C if(sav5.ne.L_uril .and. try999.gt.0) goto 999
C if(sav6.ne.R8_asil .and. try999.gt.0) goto 999
C if(sav7.ne.R_upil .and. try999.gt.0) goto 999
C if(sav8.ne.I_ipil .and. try999.gt.0) goto 999
C if(sav9.ne.I_epil .and. try999.gt.0) goto 999
C if(sav10.ne.I_apil .and. try999.gt.0) goto 999
C if(sav11.ne.I_umil .and. try999.gt.0) goto 999
C if(sav12.ne.I_omil .and. try999.gt.0) goto 999
C if(sav13.ne.I_imil .and. try999.gt.0) goto 999
C if(sav14.ne.L_emil .and. try999.gt.0) goto 999
C if(sav15.ne.L_amil .and. try999.gt.0) goto 999
C if(sav16.ne.R_olil .and. try999.gt.0) goto 999
C if(sav17.ne.C30_elil .and. try999.gt.0) goto 999
C if(sav18.ne.L_ekil .and. try999.gt.0) goto 999
C if(sav19.ne.L_ofil .and. try999.gt.0) goto 999
      if(L_isil) then
         R_(91)=R_(92)
      else
         R_(91)=R_(93)
      endif
C FDB90_vent_log.fgi(  28, 474):���� RE IN LO CH7
      R8_idi=(R0_adi*R_(90)+deltat*R_(91))/(R0_adi+deltat
     &)
C FDB90_vent_log.fgi(  37, 475):�������������� �����  
      R8_edi=R8_idi
C FDB90_vent_log.fgi(  53, 475):������,F_FDB91CU001KN01_G
      Call PUMP_HANDLER(deltat,C30_afol,I_okol,L_uvude,L_uxude
     &,L_ubafe,
     & I_ukol,I_ikol,R_ubol,REAL(R_edol,4),
     & R_ebol,REAL(R_obol,4),I_alol,L_ipol,L_omol,L_esol,
     & L_isol,L_ufol,L_ulol,L_emol,L_amol,L_imol,L_epol,L_abol
     &,
     & L_adol,L_uxil,L_ibol,L_orol,L_(15),
     & L_urol,L_(14),L_oxil,L_ixil,L_akol,I_elol,R_upol,R_arol
     &,
     & L_atol,L_afos,L_asol,REAL(R8_atebe,8),L_apol,
     & REAL(R8_umol,8),R_osol,REAL(R_ilol,4),R_olol,REAL(R8_ofol
     &,8),R_ifol,
     & R8_ubibe,R_usol,R8_umol,REAL(R_idol,4),REAL(R_odol
     &,4))
C FDB90_vlv.fgi( 249, 154):���������� ���������� �������,20FDB91CM001KN01
C label 1007  try1007=try1007-1
C sav1=R_usol
C sav2=R_osol
C sav3=L_ipol
C sav4=L_epol
C sav5=L_omol
C sav6=R8_umol
C sav7=R_olol
C sav8=I_elol
C sav9=I_alol
C sav10=I_ukol
C sav11=I_okol
C sav12=I_ikol
C sav13=I_ekol
C sav14=L_akol
C sav15=L_ufol
C sav16=R_ifol
C sav17=C30_afol
C sav18=L_adol
C sav19=L_ibol
      Call PUMP_HANDLER(deltat,C30_afol,I_okol,L_uvude,L_uxude
     &,L_ubafe,
     & I_ukol,I_ikol,R_ubol,REAL(R_edol,4),
     & R_ebol,REAL(R_obol,4),I_alol,L_ipol,L_omol,L_esol,
     & L_isol,L_ufol,L_ulol,L_emol,L_amol,L_imol,L_epol,L_abol
     &,
     & L_adol,L_uxil,L_ibol,L_orol,L_(15),
     & L_urol,L_(14),L_oxil,L_ixil,L_akol,I_elol,R_upol,R_arol
     &,
     & L_atol,L_afos,L_asol,REAL(R8_atebe,8),L_apol,
     & REAL(R8_umol,8),R_osol,REAL(R_ilol,4),R_olol,REAL(R8_ofol
     &,8),R_ifol,
     & R8_ubibe,R_usol,R8_umol,REAL(R_idol,4),REAL(R_odol
     &,4))
C FDB90_vlv.fgi( 249, 154):recalc:���������� ���������� �������,20FDB91CM001KN01
C if(sav1.ne.R_usol .and. try1007.gt.0) goto 1007
C if(sav2.ne.R_osol .and. try1007.gt.0) goto 1007
C if(sav3.ne.L_ipol .and. try1007.gt.0) goto 1007
C if(sav4.ne.L_epol .and. try1007.gt.0) goto 1007
C if(sav5.ne.L_omol .and. try1007.gt.0) goto 1007
C if(sav6.ne.R8_umol .and. try1007.gt.0) goto 1007
C if(sav7.ne.R_olol .and. try1007.gt.0) goto 1007
C if(sav8.ne.I_elol .and. try1007.gt.0) goto 1007
C if(sav9.ne.I_alol .and. try1007.gt.0) goto 1007
C if(sav10.ne.I_ukol .and. try1007.gt.0) goto 1007
C if(sav11.ne.I_okol .and. try1007.gt.0) goto 1007
C if(sav12.ne.I_ikol .and. try1007.gt.0) goto 1007
C if(sav13.ne.I_ekol .and. try1007.gt.0) goto 1007
C if(sav14.ne.L_akol .and. try1007.gt.0) goto 1007
C if(sav15.ne.L_ufol .and. try1007.gt.0) goto 1007
C if(sav16.ne.R_ifol .and. try1007.gt.0) goto 1007
C if(sav17.ne.C30_afol .and. try1007.gt.0) goto 1007
C if(sav18.ne.L_adol .and. try1007.gt.0) goto 1007
C if(sav19.ne.L_ibol .and. try1007.gt.0) goto 1007
      if(L_epol) then
         R_(87)=R_(88)
      else
         R_(87)=R_(89)
      endif
C FDB90_vent_log.fgi(  88, 475):���� RE IN LO CH7
      R8_ibi=(R0_abi*R_(86)+deltat*R_(87))/(R0_abi+deltat
     &)
C FDB90_vent_log.fgi(  97, 476):�������������� �����  
      R8_ebi=R8_ibi
C FDB90_vent_log.fgi( 113, 476):������,F_FDB91CM001KN01_G
      Call PUMP_HANDLER(deltat,C30_imok,I_arok,L_uvude,L_uxude
     &,L_ubafe,
     & I_erok,I_upok,R_elok,REAL(R_olok,4),
     & R_okok,REAL(R_alok,4),I_irok,L_utok,L_atok,L_oxok,
     & L_uxok,L_epok,L_esok,L_osok,L_isok,L_usok,L_otok,L_ikok
     &,
     & L_ilok,L_ekok,L_ukok,L_axok,L_(9),
     & L_exok,L_(8),L_akok,L_ufok,L_ipok,I_orok,R_evok,R_ivok
     &,
     & L_ibuk,L_afos,L_ixok,REAL(R8_atebe,8),L_itok,
     & REAL(R8_etok,8),R_abuk,REAL(R_urok,4),R_asok,REAL(R8_apok
     &,8),R_umok,
     & R8_ubibe,R_ebuk,R8_etok,REAL(R_ulok,4),REAL(R_amok
     &,4))
C FDB90_vlv.fgi( 267, 127):���������� ���������� �������,20FDB91CU002KN01
C label 1015  try1015=try1015-1
C sav1=R_ebuk
C sav2=R_abuk
C sav3=L_utok
C sav4=L_otok
C sav5=L_atok
C sav6=R8_etok
C sav7=R_asok
C sav8=I_orok
C sav9=I_irok
C sav10=I_erok
C sav11=I_arok
C sav12=I_upok
C sav13=I_opok
C sav14=L_ipok
C sav15=L_epok
C sav16=R_umok
C sav17=C30_imok
C sav18=L_ilok
C sav19=L_ukok
      Call PUMP_HANDLER(deltat,C30_imok,I_arok,L_uvude,L_uxude
     &,L_ubafe,
     & I_erok,I_upok,R_elok,REAL(R_olok,4),
     & R_okok,REAL(R_alok,4),I_irok,L_utok,L_atok,L_oxok,
     & L_uxok,L_epok,L_esok,L_osok,L_isok,L_usok,L_otok,L_ikok
     &,
     & L_ilok,L_ekok,L_ukok,L_axok,L_(9),
     & L_exok,L_(8),L_akok,L_ufok,L_ipok,I_orok,R_evok,R_ivok
     &,
     & L_ibuk,L_afos,L_ixok,REAL(R8_atebe,8),L_itok,
     & REAL(R8_etok,8),R_abuk,REAL(R_urok,4),R_asok,REAL(R8_apok
     &,8),R_umok,
     & R8_ubibe,R_ebuk,R8_etok,REAL(R_ulok,4),REAL(R_amok
     &,4))
C FDB90_vlv.fgi( 267, 127):recalc:���������� ���������� �������,20FDB91CU002KN01
C if(sav1.ne.R_ebuk .and. try1015.gt.0) goto 1015
C if(sav2.ne.R_abuk .and. try1015.gt.0) goto 1015
C if(sav3.ne.L_utok .and. try1015.gt.0) goto 1015
C if(sav4.ne.L_otok .and. try1015.gt.0) goto 1015
C if(sav5.ne.L_atok .and. try1015.gt.0) goto 1015
C if(sav6.ne.R8_etok .and. try1015.gt.0) goto 1015
C if(sav7.ne.R_asok .and. try1015.gt.0) goto 1015
C if(sav8.ne.I_orok .and. try1015.gt.0) goto 1015
C if(sav9.ne.I_irok .and. try1015.gt.0) goto 1015
C if(sav10.ne.I_erok .and. try1015.gt.0) goto 1015
C if(sav11.ne.I_arok .and. try1015.gt.0) goto 1015
C if(sav12.ne.I_upok .and. try1015.gt.0) goto 1015
C if(sav13.ne.I_opok .and. try1015.gt.0) goto 1015
C if(sav14.ne.L_ipok .and. try1015.gt.0) goto 1015
C if(sav15.ne.L_epok .and. try1015.gt.0) goto 1015
C if(sav16.ne.R_umok .and. try1015.gt.0) goto 1015
C if(sav17.ne.C30_imok .and. try1015.gt.0) goto 1015
C if(sav18.ne.L_ilok .and. try1015.gt.0) goto 1015
C if(sav19.ne.L_ukok .and. try1015.gt.0) goto 1015
      if(L_otok) then
         R_(75)=R_(76)
      else
         R_(75)=R_(77)
      endif
C FDB90_vent_log.fgi( 146, 474):���� RE IN LO CH7
      R8_ite=(R0_ate*R_(74)+deltat*R_(75))/(R0_ate+deltat
     &)
C FDB90_vent_log.fgi( 155, 475):�������������� �����  
      R8_ete=R8_ite
C FDB90_vent_log.fgi( 171, 475):������,F_FDB91CU002KN01_G
      L_(271)=R_isude.lt.R0_apode
C FDB90_logic.fgi( 217, 311):���������� <
C label 1023  try1023=try1023-1
      L_(280)=R_isude.gt.R0_asode
C FDB90_logic.fgi( 230, 335):���������� >
      L_(279) = L_(280).AND.L_(277)
C FDB90_logic.fgi( 238, 334):�
      L_(272)=R_isude.gt.R0_ipode
C FDB90_logic.fgi( 230, 315):���������� >
      L_(278) = L_(273).AND.L_(272)
C FDB90_logic.fgi( 237, 320):�
      L_(318) = L_(279).OR.L_(324).OR.L_(278)
C FDB90_logic.fgi( 238, 327):���
      L_(316)=R_isude.gt.R0_irode
C FDB90_logic.fgi( 231, 342):���������� >
      L_(276)=R_isude.gt.R0_opode
C FDB90_logic.fgi( 221, 339):���������� >
      L_(315) = L0_erode.AND.L_(276)
C FDB90_logic.fgi( 225, 340):�
      L_(322) = L_(317).OR.L_(318).OR.L_(316).OR.L_(315)
C FDB90_logic.fgi( 245, 343):���
      L_(284)=R_isude.lt.R0_orode
C FDB90_logic.fgi( 232, 365):���������� <
      L_(285) = L_(286).AND.L_(284)
C FDB90_logic.fgi( 239, 370):�
      L_(275)=R_isude.lt.R0_upode
C FDB90_logic.fgi( 232, 349):���������� <
      L_(283) = L_(274).AND.L_(275)
C FDB90_logic.fgi( 238, 351):�
      L_(321) = L_(285).OR.L_(325).OR.L_(283)
C FDB90_logic.fgi( 237, 360):���
      L0_epude=(L_(321).or.L0_epude).and..not.(L_(322))
      L_(323)=.not.L0_epude
C FDB90_logic.fgi( 252, 358):RS �������
      if(L0_epude) then
         R_(152)=R_(150)
      else
         R_(152)=R_(153)
      endif
C FDB90_logic.fgi( 266, 367):���� RE IN LO CH7
      L_(282) = L0_epode.AND.L_(271)
C FDB90_logic.fgi( 223, 312):�
      L_(281)=R_isude.lt.R0_urode
C FDB90_logic.fgi( 230, 308):���������� <
      L_(319) = L_(282).OR.L_(321).OR.L_(281).OR.L_(317)
C FDB90_logic.fgi( 245, 309):���
      L0_apude=(L_(318).or.L0_apude).and..not.(L_(319))
      L_(320)=.not.L0_apude
C FDB90_logic.fgi( 252, 325):RS �������
      if(L0_apude) then
         R_(151)=R_(150)
      else
         R_(151)=R_(153)
      endif
C FDB90_logic.fgi( 266, 356):���� RE IN LO CH7
      R_asude = R_(152) + (-R_(151))
C FDB90_logic.fgi( 272, 363):��������
      R_osude=R_osude+deltat/R0_omude*R_asude
      if(R_osude.gt.R0_imude) then
         R_osude=R0_imude
      elseif(R_osude.lt.R0_umude) then
         R_osude=R0_umude
      endif
C FDB90_logic.fgi( 283, 347):����������
      R_isude=R_osude
C FDB90_logic.fgi( 304, 349):������,20FDB91AE012-M01VY01
      L0_epode=(L_(273).or.L0_epode).and..not.(L_(319))
      L_(269)=.not.L0_epode
C FDB90_logic.fgi( 210, 314):RS �������
C sav1=L_(282)
      L_(282) = L0_epode.AND.L_(271)
C FDB90_logic.fgi( 223, 312):recalc:�
C if(sav1.ne.L_(282) .and. try1059.gt.0) goto 1059
      L0_erode=(L_(274).or.L0_erode).and..not.(L_(322))
      L_(270)=.not.L0_erode
C FDB90_logic.fgi( 219, 348):RS �������
C sav1=L_(315)
      L_(315) = L0_erode.AND.L_(276)
C FDB90_logic.fgi( 225, 340):recalc:�
C if(sav1.ne.L_(315) .and. try1039.gt.0) goto 1039
      L_(242)=R_erude.lt.R0_ifode
C FDB90_logic.fgi( 217, 233):���������� <
C label 1090  try1090=try1090-1
      L_(251)=R_erude.gt.R0_olode
C FDB90_logic.fgi( 230, 257):���������� >
      L_(250) = L_(251).AND.L_(248)
C FDB90_logic.fgi( 238, 256):�
      L_(243)=R_erude.gt.R0_ufode
C FDB90_logic.fgi( 230, 237):���������� >
      L_(249) = L_(244).AND.L_(243)
C FDB90_logic.fgi( 237, 242):�
      L_(261) = L_(250).OR.L_(267).OR.L_(249)
C FDB90_logic.fgi( 238, 249):���
      L_(259)=R_erude.gt.R0_alode
C FDB90_logic.fgi( 231, 264):���������� >
      L_(247)=R_erude.gt.R0_akode
C FDB90_logic.fgi( 221, 261):���������� >
      L_(258) = L0_okode.AND.L_(247)
C FDB90_logic.fgi( 225, 262):�
      L_(265) = L_(260).OR.L_(261).OR.L_(259).OR.L_(258)
C FDB90_logic.fgi( 245, 265):���
      L_(255)=R_erude.lt.R0_elode
C FDB90_logic.fgi( 232, 287):���������� <
      L_(256) = L_(257).AND.L_(255)
C FDB90_logic.fgi( 239, 292):�
      L_(246)=R_erude.lt.R0_ikode
C FDB90_logic.fgi( 232, 271):���������� <
      L_(254) = L_(245).AND.L_(246)
C FDB90_logic.fgi( 238, 273):�
      L_(264) = L_(256).OR.L_(268).OR.L_(254)
C FDB90_logic.fgi( 237, 282):���
      L0_umode=(L_(264).or.L0_umode).and..not.(L_(265))
      L_(266)=.not.L0_umode
C FDB90_logic.fgi( 252, 280):RS �������
      if(L0_umode) then
         R_(132)=R_(130)
      else
         R_(132)=R_(133)
      endif
C FDB90_logic.fgi( 266, 289):���� RE IN LO CH7
      L_(253) = L0_ofode.AND.L_(242)
C FDB90_logic.fgi( 223, 234):�
      L_(252)=R_erude.lt.R0_ilode
C FDB90_logic.fgi( 230, 230):���������� <
      L_(262) = L_(253).OR.L_(264).OR.L_(252).OR.L_(260)
C FDB90_logic.fgi( 245, 231):���
      L0_omode=(L_(261).or.L0_omode).and..not.(L_(262))
      L_(263)=.not.L0_omode
C FDB90_logic.fgi( 252, 247):RS �������
      if(L0_omode) then
         R_(131)=R_(130)
      else
         R_(131)=R_(133)
      endif
C FDB90_logic.fgi( 266, 278):���� RE IN LO CH7
      R_upude = R_(132) + (-R_(131))
C FDB90_logic.fgi( 272, 285):��������
      R_irude=R_irude+deltat/R0_emode*R_upude
      if(R_irude.gt.R0_amode) then
         R_irude=R0_amode
      elseif(R_irude.lt.R0_imode) then
         R_irude=R0_imode
      endif
C FDB90_logic.fgi( 283, 269):����������
      R_erude=R_irude
C FDB90_logic.fgi( 304, 271):������,20FDB91AE014-M01VY01
      L0_ofode=(L_(244).or.L0_ofode).and..not.(L_(262))
      L_(240)=.not.L0_ofode
C FDB90_logic.fgi( 210, 236):RS �������
C sav1=L_(253)
      L_(253) = L0_ofode.AND.L_(242)
C FDB90_logic.fgi( 223, 234):recalc:�
C if(sav1.ne.L_(253) .and. try1126.gt.0) goto 1126
      L0_okode=(L_(245).or.L0_okode).and..not.(L_(265))
      L_(241)=.not.L0_okode
C FDB90_logic.fgi( 219, 270):RS �������
C sav1=L_(258)
      L_(258) = L0_okode.AND.L_(247)
C FDB90_logic.fgi( 225, 262):recalc:�
C if(sav1.ne.L_(258) .and. try1106.gt.0) goto 1106
      L_(213)=R_abode.lt.R0_otide
C FDB90_logic.fgi( 217, 155):���������� <
C label 1156  try1156=try1156-1
      L_(222)=R_abode.gt.R0_uxide
C FDB90_logic.fgi( 230, 179):���������� >
      L_(221) = L_(222).AND.L_(219)
C FDB90_logic.fgi( 238, 178):�
      L_(214)=R_abode.gt.R0_avide
C FDB90_logic.fgi( 230, 159):���������� >
      L_(220) = L_(215).AND.L_(214)
C FDB90_logic.fgi( 237, 164):�
      L_(232) = L_(221).OR.L_(238).OR.L_(220)
C FDB90_logic.fgi( 238, 171):���
      L_(230)=R_abode.gt.R0_exide
C FDB90_logic.fgi( 231, 186):���������� >
      L_(218)=R_abode.gt.R0_evide
C FDB90_logic.fgi( 221, 183):���������� >
      L_(229) = L0_uvide.AND.L_(218)
C FDB90_logic.fgi( 225, 184):�
      L_(236) = L_(231).OR.L_(232).OR.L_(230).OR.L_(229)
C FDB90_logic.fgi( 245, 187):���
      L_(226)=R_abode.lt.R0_ixide
C FDB90_logic.fgi( 232, 209):���������� <
      L_(227) = L_(228).AND.L_(226)
C FDB90_logic.fgi( 239, 214):�
      L_(217)=R_abode.lt.R0_ovide
C FDB90_logic.fgi( 232, 193):���������� <
      L_(225) = L_(216).AND.L_(217)
C FDB90_logic.fgi( 238, 195):�
      L_(235) = L_(227).OR.L_(239).OR.L_(225)
C FDB90_logic.fgi( 237, 204):���
      L0_efode=(L_(235).or.L0_efode).and..not.(L_(236))
      L_(237)=.not.L0_efode
C FDB90_logic.fgi( 252, 202):RS �������
      if(L0_efode) then
         R_(128)=R_(126)
      else
         R_(128)=R_(129)
      endif
C FDB90_logic.fgi( 266, 211):���� RE IN LO CH7
      L_(224) = L0_utide.AND.L_(213)
C FDB90_logic.fgi( 223, 156):�
      L_(223)=R_abode.lt.R0_oxide
C FDB90_logic.fgi( 230, 152):���������� <
      L_(233) = L_(224).OR.L_(235).OR.L_(223).OR.L_(231)
C FDB90_logic.fgi( 245, 153):���
      L0_afode=(L_(232).or.L0_afode).and..not.(L_(233))
      L_(234)=.not.L0_afode
C FDB90_logic.fgi( 252, 169):RS �������
      if(L0_afode) then
         R_(127)=R_(126)
      else
         R_(127)=R_(129)
      endif
C FDB90_logic.fgi( 266, 200):���� RE IN LO CH7
      R_udode = R_(128) + (-R_(127))
C FDB90_logic.fgi( 272, 207):��������
      R_idode=R_idode+deltat/R0_edode*R_udode
      if(R_idode.gt.R0_adode) then
         R_idode=R0_adode
      elseif(R_idode.lt.R0_odode) then
         R_idode=R0_odode
      endif
C FDB90_logic.fgi( 283, 191):����������
      R_abode=R_idode
C FDB90_logic.fgi( 304, 193):������,20FDB91AE006-M01VY01
      L0_utide=(L_(215).or.L0_utide).and..not.(L_(233))
      L_(211)=.not.L0_utide
C FDB90_logic.fgi( 210, 158):RS �������
C sav1=L_(224)
      L_(224) = L0_utide.AND.L_(213)
C FDB90_logic.fgi( 223, 156):recalc:�
C if(sav1.ne.L_(224) .and. try1192.gt.0) goto 1192
      L0_uvide=(L_(216).or.L0_uvide).and..not.(L_(236))
      L_(212)=.not.L0_uvide
C FDB90_logic.fgi( 219, 192):RS �������
C sav1=L_(229)
      L_(229) = L0_uvide.AND.L_(218)
C FDB90_logic.fgi( 225, 184):recalc:�
C if(sav1.ne.L_(229) .and. try1172.gt.0) goto 1172
      Call PUMP_HANDLER(deltat,C30_ekuk,I_uluk,L_uvude,L_uxude
     &,L_ubafe,
     & I_amuk,I_oluk,R_afuk,REAL(R_ifuk,4),
     & R_iduk,REAL(R_uduk,4),I_emuk,L_oruk,L_upuk,L_ituk,
     & L_otuk,L_aluk,L_apuk,L_ipuk,L_epuk,L_opuk,L_iruk,L_eduk
     &,
     & L_efuk,L_aduk,L_oduk,L_usuk,L_(11),
     & L_atuk,L_(10),L_ubuk,L_obuk,L_eluk,I_imuk,R_asuk,R_esuk
     &,
     & L_evuk,L_afos,L_etuk,REAL(R8_atebe,8),L_eruk,
     & REAL(R8_aruk,8),R_utuk,REAL(R_omuk,4),R_umuk,REAL(R8_ukuk
     &,8),R_okuk,
     & R8_ubibe,R_avuk,R8_aruk,REAL(R_ofuk,4),REAL(R_ufuk
     &,4))
C FDB90_vlv.fgi( 249, 127):���������� ���������� �������,20FDB91CM002KN01
C label 1222  try1222=try1222-1
C sav1=R_avuk
C sav2=R_utuk
C sav3=L_oruk
C sav4=L_iruk
C sav5=L_upuk
C sav6=R8_aruk
C sav7=R_umuk
C sav8=I_imuk
C sav9=I_emuk
C sav10=I_amuk
C sav11=I_uluk
C sav12=I_oluk
C sav13=I_iluk
C sav14=L_eluk
C sav15=L_aluk
C sav16=R_okuk
C sav17=C30_ekuk
C sav18=L_efuk
C sav19=L_oduk
      Call PUMP_HANDLER(deltat,C30_ekuk,I_uluk,L_uvude,L_uxude
     &,L_ubafe,
     & I_amuk,I_oluk,R_afuk,REAL(R_ifuk,4),
     & R_iduk,REAL(R_uduk,4),I_emuk,L_oruk,L_upuk,L_ituk,
     & L_otuk,L_aluk,L_apuk,L_ipuk,L_epuk,L_opuk,L_iruk,L_eduk
     &,
     & L_efuk,L_aduk,L_oduk,L_usuk,L_(11),
     & L_atuk,L_(10),L_ubuk,L_obuk,L_eluk,I_imuk,R_asuk,R_esuk
     &,
     & L_evuk,L_afos,L_etuk,REAL(R8_atebe,8),L_eruk,
     & REAL(R8_aruk,8),R_utuk,REAL(R_omuk,4),R_umuk,REAL(R8_ukuk
     &,8),R_okuk,
     & R8_ubibe,R_avuk,R8_aruk,REAL(R_ofuk,4),REAL(R_ufuk
     &,4))
C FDB90_vlv.fgi( 249, 127):recalc:���������� ���������� �������,20FDB91CM002KN01
C if(sav1.ne.R_avuk .and. try1222.gt.0) goto 1222
C if(sav2.ne.R_utuk .and. try1222.gt.0) goto 1222
C if(sav3.ne.L_oruk .and. try1222.gt.0) goto 1222
C if(sav4.ne.L_iruk .and. try1222.gt.0) goto 1222
C if(sav5.ne.L_upuk .and. try1222.gt.0) goto 1222
C if(sav6.ne.R8_aruk .and. try1222.gt.0) goto 1222
C if(sav7.ne.R_umuk .and. try1222.gt.0) goto 1222
C if(sav8.ne.I_imuk .and. try1222.gt.0) goto 1222
C if(sav9.ne.I_emuk .and. try1222.gt.0) goto 1222
C if(sav10.ne.I_amuk .and. try1222.gt.0) goto 1222
C if(sav11.ne.I_uluk .and. try1222.gt.0) goto 1222
C if(sav12.ne.I_oluk .and. try1222.gt.0) goto 1222
C if(sav13.ne.I_iluk .and. try1222.gt.0) goto 1222
C if(sav14.ne.L_eluk .and. try1222.gt.0) goto 1222
C if(sav15.ne.L_aluk .and. try1222.gt.0) goto 1222
C if(sav16.ne.R_okuk .and. try1222.gt.0) goto 1222
C if(sav17.ne.C30_ekuk .and. try1222.gt.0) goto 1222
C if(sav18.ne.L_efuk .and. try1222.gt.0) goto 1222
C if(sav19.ne.L_oduk .and. try1222.gt.0) goto 1222
      if(L_iruk) then
         R_(71)=R_(72)
      else
         R_(71)=R_(73)
      endif
C FDB90_vent_log.fgi( 206, 475):���� RE IN LO CH7
      R8_ise=(R0_ase*R_(70)+deltat*R_(71))/(R0_ase+deltat
     &)
C FDB90_vent_log.fgi( 215, 476):�������������� �����  
      R8_ese=R8_ise
C FDB90_vent_log.fgi( 231, 476):������,F_FDB91CM002KN01_G
      Call PUMP_HANDLER(deltat,C30_uxes,I_idis,L_uvude,L_uxude
     &,L_ubafe,
     & I_odis,I_edis,R_oves,REAL(R_axes,4),
     & R_aves,REAL(R_ives,4),I_udis,L_elis,L_ikis,L_apis,
     & L_epis,L_obis,L_ofis,L_akis,L_ufis,L_ekis,L_alis,L_utes
     &,
     & L_uves,L_otes,L_eves,L_imis,L_(17),
     & L_omis,L_(16),L_ites,L_etes,L_ubis,I_afis,R_olis,R_ulis
     &,
     & L_upis,L_afos,L_umis,REAL(R8_atebe,8),L_ukis,
     & REAL(R8_okis,8),R_ipis,REAL(R_efis,4),R_ifis,REAL(R8_ibis
     &,8),R_ebis,
     & R8_ubibe,R_opis,R8_okis,REAL(R_exes,4),REAL(R_ixes
     &,4))
C FDB90_vlv.fgi( 232, 154):���������� ���������� �������,20FDB92CM101KN01
C label 1230  try1230=try1230-1
C sav1=R_opis
C sav2=R_ipis
C sav3=L_elis
C sav4=L_alis
C sav5=L_ikis
C sav6=R8_okis
C sav7=R_ifis
C sav8=I_afis
C sav9=I_udis
C sav10=I_odis
C sav11=I_idis
C sav12=I_edis
C sav13=I_adis
C sav14=L_ubis
C sav15=L_obis
C sav16=R_ebis
C sav17=C30_uxes
C sav18=L_uves
C sav19=L_eves
      Call PUMP_HANDLER(deltat,C30_uxes,I_idis,L_uvude,L_uxude
     &,L_ubafe,
     & I_odis,I_edis,R_oves,REAL(R_axes,4),
     & R_aves,REAL(R_ives,4),I_udis,L_elis,L_ikis,L_apis,
     & L_epis,L_obis,L_ofis,L_akis,L_ufis,L_ekis,L_alis,L_utes
     &,
     & L_uves,L_otes,L_eves,L_imis,L_(17),
     & L_omis,L_(16),L_ites,L_etes,L_ubis,I_afis,R_olis,R_ulis
     &,
     & L_upis,L_afos,L_umis,REAL(R8_atebe,8),L_ukis,
     & REAL(R8_okis,8),R_ipis,REAL(R_efis,4),R_ifis,REAL(R8_ibis
     &,8),R_ebis,
     & R8_ubibe,R_opis,R8_okis,REAL(R_exes,4),REAL(R_ixes
     &,4))
C FDB90_vlv.fgi( 232, 154):recalc:���������� ���������� �������,20FDB92CM101KN01
C if(sav1.ne.R_opis .and. try1230.gt.0) goto 1230
C if(sav2.ne.R_ipis .and. try1230.gt.0) goto 1230
C if(sav3.ne.L_elis .and. try1230.gt.0) goto 1230
C if(sav4.ne.L_alis .and. try1230.gt.0) goto 1230
C if(sav5.ne.L_ikis .and. try1230.gt.0) goto 1230
C if(sav6.ne.R8_okis .and. try1230.gt.0) goto 1230
C if(sav7.ne.R_ifis .and. try1230.gt.0) goto 1230
C if(sav8.ne.I_afis .and. try1230.gt.0) goto 1230
C if(sav9.ne.I_udis .and. try1230.gt.0) goto 1230
C if(sav10.ne.I_odis .and. try1230.gt.0) goto 1230
C if(sav11.ne.I_idis .and. try1230.gt.0) goto 1230
C if(sav12.ne.I_edis .and. try1230.gt.0) goto 1230
C if(sav13.ne.I_adis .and. try1230.gt.0) goto 1230
C if(sav14.ne.L_ubis .and. try1230.gt.0) goto 1230
C if(sav15.ne.L_obis .and. try1230.gt.0) goto 1230
C if(sav16.ne.R_ebis .and. try1230.gt.0) goto 1230
C if(sav17.ne.C30_uxes .and. try1230.gt.0) goto 1230
C if(sav18.ne.L_uves .and. try1230.gt.0) goto 1230
C if(sav19.ne.L_eves .and. try1230.gt.0) goto 1230
      if(L_alis) then
         R_(79)=R_(80)
      else
         R_(79)=R_(81)
      endif
C FDB90_vent_log.fgi( 457, 475):���� RE IN LO CH7
      R8_ive=(R0_ave*R_(78)+deltat*R_(79))/(R0_ave+deltat
     &)
C FDB90_vent_log.fgi( 466, 476):�������������� �����  
      R8_eve=R8_ive
C FDB90_vent_log.fgi( 482, 476):������,F_FDB92CM101KN01_G
      Call PUMP_HANDLER(deltat,C30_otis,I_exis,L_uvude,L_uxude
     &,L_ubafe,
     & I_ixis,I_axis,R_isis,REAL(R_usis,4),
     & R_uris,REAL(R_esis,4),I_oxis,L_efos,L_edos,L_alos,
     & L_elos,L_ivis,L_ibos,L_ubos,L_obos,L_ados,L_udos,L_oris
     &,
     & L_osis,L_iris,L_asis,L_ikos,L_(19),
     & L_okos,L_(18),L_eris,L_aris,L_ovis,I_uxis,R_ofos,R_ufos
     &,
     & L_ulos,L_afos,L_ukos,REAL(R8_atebe,8),L_odos,
     & REAL(R8_idos,8),R_ilos,REAL(R_abos,4),R_ebos,REAL(R8_evis
     &,8),R_avis,
     & R8_ubibe,R_olos,R8_idos,REAL(R_atis,4),REAL(R_etis
     &,4))
C FDB90_vlv.fgi( 232, 127):���������� ���������� �������,20FDB92CM001KN01
C label 1238  try1238=try1238-1
C sav1=R_olos
C sav2=R_ilos
C sav3=L_efos
C sav4=L_udos
C sav5=L_edos
C sav6=R8_idos
C sav7=R_ebos
C sav8=I_uxis
C sav9=I_oxis
C sav10=I_ixis
C sav11=I_exis
C sav12=I_axis
C sav13=I_uvis
C sav14=L_ovis
C sav15=L_ivis
C sav16=R_avis
C sav17=C30_otis
C sav18=L_osis
C sav19=L_asis
      Call PUMP_HANDLER(deltat,C30_otis,I_exis,L_uvude,L_uxude
     &,L_ubafe,
     & I_ixis,I_axis,R_isis,REAL(R_usis,4),
     & R_uris,REAL(R_esis,4),I_oxis,L_efos,L_edos,L_alos,
     & L_elos,L_ivis,L_ibos,L_ubos,L_obos,L_ados,L_udos,L_oris
     &,
     & L_osis,L_iris,L_asis,L_ikos,L_(19),
     & L_okos,L_(18),L_eris,L_aris,L_ovis,I_uxis,R_ofos,R_ufos
     &,
     & L_ulos,L_afos,L_ukos,REAL(R8_atebe,8),L_odos,
     & REAL(R8_idos,8),R_ilos,REAL(R_abos,4),R_ebos,REAL(R8_evis
     &,8),R_avis,
     & R8_ubibe,R_olos,R8_idos,REAL(R_atis,4),REAL(R_etis
     &,4))
C FDB90_vlv.fgi( 232, 127):recalc:���������� ���������� �������,20FDB92CM001KN01
C if(sav1.ne.R_olos .and. try1238.gt.0) goto 1238
C if(sav2.ne.R_ilos .and. try1238.gt.0) goto 1238
C if(sav3.ne.L_efos .and. try1238.gt.0) goto 1238
C if(sav4.ne.L_udos .and. try1238.gt.0) goto 1238
C if(sav5.ne.L_edos .and. try1238.gt.0) goto 1238
C if(sav6.ne.R8_idos .and. try1238.gt.0) goto 1238
C if(sav7.ne.R_ebos .and. try1238.gt.0) goto 1238
C if(sav8.ne.I_uxis .and. try1238.gt.0) goto 1238
C if(sav9.ne.I_oxis .and. try1238.gt.0) goto 1238
C if(sav10.ne.I_ixis .and. try1238.gt.0) goto 1238
C if(sav11.ne.I_exis .and. try1238.gt.0) goto 1238
C if(sav12.ne.I_axis .and. try1238.gt.0) goto 1238
C if(sav13.ne.I_uvis .and. try1238.gt.0) goto 1238
C if(sav14.ne.L_ovis .and. try1238.gt.0) goto 1238
C if(sav15.ne.L_ivis .and. try1238.gt.0) goto 1238
C if(sav16.ne.R_avis .and. try1238.gt.0) goto 1238
C if(sav17.ne.C30_otis .and. try1238.gt.0) goto 1238
C if(sav18.ne.L_osis .and. try1238.gt.0) goto 1238
C if(sav19.ne.L_asis .and. try1238.gt.0) goto 1238
      if(L_udos) then
         R_(83)=R_(84)
      else
         R_(83)=R_(85)
      endif
C FDB90_vent_log.fgi( 394, 475):���� RE IN LO CH7
      R8_ixe=(R0_axe*R_(82)+deltat*R_(83))/(R0_axe+deltat
     &)
C FDB90_vent_log.fgi( 403, 476):�������������� �����  
      R8_exe=R8_ixe
C FDB90_vent_log.fgi( 419, 476):������,F_FDB92CM001KN01_G
      Call PUMP_HANDLER(deltat,C30_asi,I_oti,L_uvude,L_uxude
     &,L_ubafe,
     & I_uti,I_iti,R_upi,REAL(R_eri,4),
     & R_epi,REAL(R_opi,4),I_avi,L_ibo,L_oxi,L_efo,
     & L_ifo,L_usi,L_uvi,L_exi,L_axi,L_ixi,L_ebo,L_api,
     & L_ari,L_umi,L_ipi,L_odo,L_(5),
     & L_udo,L_(4),L_omi,L_imi,L_ati,I_evi,R_ubo,R_ado,
     & L_ako,L_afos,L_afo,REAL(R8_atebe,8),L_abo,
     & REAL(R8_uxi,8),R_ofo,REAL(R_ivi,4),R_ovi,REAL(R8_osi
     &,8),R_isi,
     & R8_ubibe,R_ufo,R8_uxi,REAL(R_iri,4),REAL(R_ori,4))
C FDB90_vlv.fgi( 300, 154):���������� ���������� �������,20FDB91CU003KN01
C label 1246  try1246=try1246-1
C sav1=R_ufo
C sav2=R_ofo
C sav3=L_ibo
C sav4=L_ebo
C sav5=L_oxi
C sav6=R8_uxi
C sav7=R_ovi
C sav8=I_evi
C sav9=I_avi
C sav10=I_uti
C sav11=I_oti
C sav12=I_iti
C sav13=I_eti
C sav14=L_ati
C sav15=L_usi
C sav16=R_isi
C sav17=C30_asi
C sav18=L_ari
C sav19=L_ipi
      Call PUMP_HANDLER(deltat,C30_asi,I_oti,L_uvude,L_uxude
     &,L_ubafe,
     & I_uti,I_iti,R_upi,REAL(R_eri,4),
     & R_epi,REAL(R_opi,4),I_avi,L_ibo,L_oxi,L_efo,
     & L_ifo,L_usi,L_uvi,L_exi,L_axi,L_ixi,L_ebo,L_api,
     & L_ari,L_umi,L_ipi,L_odo,L_(5),
     & L_udo,L_(4),L_omi,L_imi,L_ati,I_evi,R_ubo,R_ado,
     & L_ako,L_afos,L_afo,REAL(R8_atebe,8),L_abo,
     & REAL(R8_uxi,8),R_ofo,REAL(R_ivi,4),R_ovi,REAL(R8_osi
     &,8),R_isi,
     & R8_ubibe,R_ufo,R8_uxi,REAL(R_iri,4),REAL(R_ori,4))
C FDB90_vlv.fgi( 300, 154):recalc:���������� ���������� �������,20FDB91CU003KN01
C if(sav1.ne.R_ufo .and. try1246.gt.0) goto 1246
C if(sav2.ne.R_ofo .and. try1246.gt.0) goto 1246
C if(sav3.ne.L_ibo .and. try1246.gt.0) goto 1246
C if(sav4.ne.L_ebo .and. try1246.gt.0) goto 1246
C if(sav5.ne.L_oxi .and. try1246.gt.0) goto 1246
C if(sav6.ne.R8_uxi .and. try1246.gt.0) goto 1246
C if(sav7.ne.R_ovi .and. try1246.gt.0) goto 1246
C if(sav8.ne.I_evi .and. try1246.gt.0) goto 1246
C if(sav9.ne.I_avi .and. try1246.gt.0) goto 1246
C if(sav10.ne.I_uti .and. try1246.gt.0) goto 1246
C if(sav11.ne.I_oti .and. try1246.gt.0) goto 1246
C if(sav12.ne.I_iti .and. try1246.gt.0) goto 1246
C if(sav13.ne.I_eti .and. try1246.gt.0) goto 1246
C if(sav14.ne.L_ati .and. try1246.gt.0) goto 1246
C if(sav15.ne.L_usi .and. try1246.gt.0) goto 1246
C if(sav16.ne.R_isi .and. try1246.gt.0) goto 1246
C if(sav17.ne.C30_asi .and. try1246.gt.0) goto 1246
C if(sav18.ne.L_ari .and. try1246.gt.0) goto 1246
C if(sav19.ne.L_ipi .and. try1246.gt.0) goto 1246
      if(L_ebo) then
         R_(67)=R_(68)
      else
         R_(67)=R_(69)
      endif
C FDB90_vent_log.fgi( 268, 474):���� RE IN LO CH7
      R8_ire=(R0_are*R_(66)+deltat*R_(67))/(R0_are+deltat
     &)
C FDB90_vent_log.fgi( 277, 475):�������������� �����  
      R8_ere=R8_ire
C FDB90_vent_log.fgi( 293, 475):������,F_FDB91CU003KN01_G
      Call PUMP_HANDLER(deltat,C30_umo,I_iro,L_uvude,L_uxude
     &,L_ubafe,
     & I_oro,I_ero,R_olo,REAL(R_amo,4),
     & R_alo,REAL(R_ilo,4),I_uro,L_evo,L_ito,L_abu,
     & L_ebu,L_opo,L_oso,L_ato,L_uso,L_eto,L_avo,L_uko,
     & L_ulo,L_oko,L_elo,L_ixo,L_(7),
     & L_oxo,L_(6),L_iko,L_eko,L_upo,I_aso,R_ovo,R_uvo,
     & L_ubu,L_afos,L_uxo,REAL(R8_atebe,8),L_uto,
     & REAL(R8_oto,8),R_ibu,REAL(R_eso,4),R_iso,REAL(R8_ipo
     &,8),R_epo,
     & R8_ubibe,R_obu,R8_oto,REAL(R_emo,4),REAL(R_imo,4))
C FDB90_vlv.fgi( 283, 154):���������� ���������� �������,20FDB91CM003KN01
C label 1254  try1254=try1254-1
C sav1=R_obu
C sav2=R_ibu
C sav3=L_evo
C sav4=L_avo
C sav5=L_ito
C sav6=R8_oto
C sav7=R_iso
C sav8=I_aso
C sav9=I_uro
C sav10=I_oro
C sav11=I_iro
C sav12=I_ero
C sav13=I_aro
C sav14=L_upo
C sav15=L_opo
C sav16=R_epo
C sav17=C30_umo
C sav18=L_ulo
C sav19=L_elo
      Call PUMP_HANDLER(deltat,C30_umo,I_iro,L_uvude,L_uxude
     &,L_ubafe,
     & I_oro,I_ero,R_olo,REAL(R_amo,4),
     & R_alo,REAL(R_ilo,4),I_uro,L_evo,L_ito,L_abu,
     & L_ebu,L_opo,L_oso,L_ato,L_uso,L_eto,L_avo,L_uko,
     & L_ulo,L_oko,L_elo,L_ixo,L_(7),
     & L_oxo,L_(6),L_iko,L_eko,L_upo,I_aso,R_ovo,R_uvo,
     & L_ubu,L_afos,L_uxo,REAL(R8_atebe,8),L_uto,
     & REAL(R8_oto,8),R_ibu,REAL(R_eso,4),R_iso,REAL(R8_ipo
     &,8),R_epo,
     & R8_ubibe,R_obu,R8_oto,REAL(R_emo,4),REAL(R_imo,4))
C FDB90_vlv.fgi( 283, 154):recalc:���������� ���������� �������,20FDB91CM003KN01
C if(sav1.ne.R_obu .and. try1254.gt.0) goto 1254
C if(sav2.ne.R_ibu .and. try1254.gt.0) goto 1254
C if(sav3.ne.L_evo .and. try1254.gt.0) goto 1254
C if(sav4.ne.L_avo .and. try1254.gt.0) goto 1254
C if(sav5.ne.L_ito .and. try1254.gt.0) goto 1254
C if(sav6.ne.R8_oto .and. try1254.gt.0) goto 1254
C if(sav7.ne.R_iso .and. try1254.gt.0) goto 1254
C if(sav8.ne.I_aso .and. try1254.gt.0) goto 1254
C if(sav9.ne.I_uro .and. try1254.gt.0) goto 1254
C if(sav10.ne.I_oro .and. try1254.gt.0) goto 1254
C if(sav11.ne.I_iro .and. try1254.gt.0) goto 1254
C if(sav12.ne.I_ero .and. try1254.gt.0) goto 1254
C if(sav13.ne.I_aro .and. try1254.gt.0) goto 1254
C if(sav14.ne.L_upo .and. try1254.gt.0) goto 1254
C if(sav15.ne.L_opo .and. try1254.gt.0) goto 1254
C if(sav16.ne.R_epo .and. try1254.gt.0) goto 1254
C if(sav17.ne.C30_umo .and. try1254.gt.0) goto 1254
C if(sav18.ne.L_ulo .and. try1254.gt.0) goto 1254
C if(sav19.ne.L_elo .and. try1254.gt.0) goto 1254
      if(L_avo) then
         R_(63)=R_(64)
      else
         R_(63)=R_(65)
      endif
C FDB90_vent_log.fgi( 328, 475):���� RE IN LO CH7
      R8_ipe=(R0_ape*R_(62)+deltat*R_(63))/(R0_ape+deltat
     &)
C FDB90_vent_log.fgi( 337, 476):�������������� �����  
      R8_epe=R8_ipe
C FDB90_vent_log.fgi( 353, 476):������,F_FDB91CM003KN01_G
      L_(184)=R_eride.lt.R0_ulide
C FDB90_logic.fgi( 346, 309):���������� <
C label 1262  try1262=try1262-1
      L_(193)=R_eride.gt.R0_aride
C FDB90_logic.fgi( 359, 333):���������� >
      L_(192) = L_(193).AND.L_(190)
C FDB90_logic.fgi( 367, 332):�
      L_(185)=R_eride.gt.R0_emide
C FDB90_logic.fgi( 359, 313):���������� >
      L_(191) = L_(186).AND.L_(185)
C FDB90_logic.fgi( 366, 318):�
      L_(203) = L_(192).OR.L_(209).OR.L_(191)
C FDB90_logic.fgi( 367, 325):���
      L_(201)=R_eride.gt.R0_ipide
C FDB90_logic.fgi( 360, 340):���������� >
      L_(189)=R_eride.gt.R0_imide
C FDB90_logic.fgi( 350, 337):���������� >
      L_(200) = L0_apide.AND.L_(189)
C FDB90_logic.fgi( 354, 338):�
      L_(207) = L_(202).OR.L_(203).OR.L_(201).OR.L_(200)
C FDB90_logic.fgi( 374, 341):���
      L_(197)=R_eride.lt.R0_opide
C FDB90_logic.fgi( 361, 363):���������� <
      L_(198) = L_(199).AND.L_(197)
C FDB90_logic.fgi( 368, 368):�
      L_(188)=R_eride.lt.R0_umide
C FDB90_logic.fgi( 361, 347):���������� <
      L_(196) = L_(187).AND.L_(188)
C FDB90_logic.fgi( 367, 349):�
      L_(206) = L_(198).OR.L_(210).OR.L_(196)
C FDB90_logic.fgi( 366, 358):���
      L0_itide=(L_(206).or.L0_itide).and..not.(L_(207))
      L_(208)=.not.L0_itide
C FDB90_logic.fgi( 381, 356):RS �������
      if(L0_itide) then
         R_(124)=R_(122)
      else
         R_(124)=R_(125)
      endif
C FDB90_logic.fgi( 395, 365):���� RE IN LO CH7
      L_(195) = L0_amide.AND.L_(184)
C FDB90_logic.fgi( 352, 310):�
      L_(194)=R_eride.lt.R0_upide
C FDB90_logic.fgi( 359, 306):���������� <
      L_(204) = L_(195).OR.L_(206).OR.L_(194).OR.L_(202)
C FDB90_logic.fgi( 374, 307):���
      L0_etide=(L_(203).or.L0_etide).and..not.(L_(204))
      L_(205)=.not.L0_etide
C FDB90_logic.fgi( 381, 323):RS �������
      if(L0_etide) then
         R_(123)=R_(122)
      else
         R_(123)=R_(125)
      endif
C FDB90_logic.fgi( 395, 354):���� RE IN LO CH7
      R_atide = R_(124) + (-R_(123))
C FDB90_logic.fgi( 401, 361):��������
      R_oside=R_oside+deltat/R0_iside*R_atide
      if(R_oside.gt.R0_eside) then
         R_oside=R0_eside
      elseif(R_oside.lt.R0_uside) then
         R_oside=R0_uside
      endif
C FDB90_logic.fgi( 412, 345):����������
      R_eride=R_oside
C FDB90_logic.fgi( 434, 347):������,20FDB91AE007-M01VY01
      L0_amide=(L_(186).or.L0_amide).and..not.(L_(204))
      L_(182)=.not.L0_amide
C FDB90_logic.fgi( 339, 312):RS �������
C sav1=L_(195)
      L_(195) = L0_amide.AND.L_(184)
C FDB90_logic.fgi( 352, 310):recalc:�
C if(sav1.ne.L_(195) .and. try1298.gt.0) goto 1298
      L0_apide=(L_(187).or.L0_apide).and..not.(L_(207))
      L_(183)=.not.L0_apide
C FDB90_logic.fgi( 348, 346):RS �������
C sav1=L_(200)
      L_(200) = L0_apide.AND.L_(189)
C FDB90_logic.fgi( 354, 338):recalc:�
C if(sav1.ne.L_(200) .and. try1278.gt.0) goto 1278
      L_(155)=R_ifide.lt.R0_abide
C FDB90_logic.fgi( 346, 232):���������� <
C label 1328  try1328=try1328-1
      L_(164)=R_ifide.gt.R0_efide
C FDB90_logic.fgi( 359, 256):���������� >
      L_(163) = L_(164).AND.L_(161)
C FDB90_logic.fgi( 367, 255):�
      L_(156)=R_ifide.gt.R0_ibide
C FDB90_logic.fgi( 359, 236):���������� >
      L_(162) = L_(157).AND.L_(156)
C FDB90_logic.fgi( 366, 241):�
      L_(174) = L_(163).OR.L_(180).OR.L_(162)
C FDB90_logic.fgi( 367, 248):���
      L_(172)=R_ifide.gt.R0_odide
C FDB90_logic.fgi( 360, 263):���������� >
      L_(160)=R_ifide.gt.R0_obide
C FDB90_logic.fgi( 350, 260):���������� >
      L_(171) = L0_edide.AND.L_(160)
C FDB90_logic.fgi( 354, 261):�
      L_(178) = L_(173).OR.L_(174).OR.L_(172).OR.L_(171)
C FDB90_logic.fgi( 374, 264):���
      L_(168)=R_ifide.lt.R0_udide
C FDB90_logic.fgi( 361, 286):���������� <
      L_(169) = L_(170).AND.L_(168)
C FDB90_logic.fgi( 368, 291):�
      L_(159)=R_ifide.lt.R0_adide
C FDB90_logic.fgi( 361, 270):���������� <
      L_(167) = L_(158).AND.L_(159)
C FDB90_logic.fgi( 367, 272):�
      L_(177) = L_(169).OR.L_(181).OR.L_(167)
C FDB90_logic.fgi( 366, 281):���
      L0_olide=(L_(177).or.L0_olide).and..not.(L_(178))
      L_(179)=.not.L0_olide
C FDB90_logic.fgi( 381, 279):RS �������
      if(L0_olide) then
         R_(120)=R_(118)
      else
         R_(120)=R_(121)
      endif
C FDB90_logic.fgi( 395, 288):���� RE IN LO CH7
      L_(166) = L0_ebide.AND.L_(155)
C FDB90_logic.fgi( 352, 233):�
      L_(165)=R_ifide.lt.R0_afide
C FDB90_logic.fgi( 359, 229):���������� <
      L_(175) = L_(166).OR.L_(177).OR.L_(165).OR.L_(173)
C FDB90_logic.fgi( 374, 230):���
      L0_ilide=(L_(174).or.L0_ilide).and..not.(L_(175))
      L_(176)=.not.L0_ilide
C FDB90_logic.fgi( 381, 246):RS �������
      if(L0_ilide) then
         R_(119)=R_(118)
      else
         R_(119)=R_(121)
      endif
C FDB90_logic.fgi( 395, 277):���� RE IN LO CH7
      R_elide = R_(120) + (-R_(119))
C FDB90_logic.fgi( 401, 284):��������
      R_ukide=R_ukide+deltat/R0_okide*R_elide
      if(R_ukide.gt.R0_ikide) then
         R_ukide=R0_ikide
      elseif(R_ukide.lt.R0_alide) then
         R_ukide=R0_alide
      endif
C FDB90_logic.fgi( 412, 268):����������
      R_ifide=R_ukide
C FDB90_logic.fgi( 434, 270):������,20FDB91AE008-M01VY01
      L0_ebide=(L_(157).or.L0_ebide).and..not.(L_(175))
      L_(153)=.not.L0_ebide
C FDB90_logic.fgi( 339, 235):RS �������
C sav1=L_(166)
      L_(166) = L0_ebide.AND.L_(155)
C FDB90_logic.fgi( 352, 233):recalc:�
C if(sav1.ne.L_(166) .and. try1364.gt.0) goto 1364
      L0_edide=(L_(158).or.L0_edide).and..not.(L_(178))
      L_(154)=.not.L0_edide
C FDB90_logic.fgi( 348, 269):RS �������
C sav1=L_(171)
      L_(171) = L0_edide.AND.L_(160)
C FDB90_logic.fgi( 354, 261):recalc:�
C if(sav1.ne.L_(171) .and. try1344.gt.0) goto 1344
      L_(126)=R_otede.lt.R0_erede
C FDB90_logic.fgi( 346, 157):���������� <
C label 1394  try1394=try1394-1
      L_(135)=R_otede.gt.R0_itede
C FDB90_logic.fgi( 359, 181):���������� >
      L_(134) = L_(135).AND.L_(132)
C FDB90_logic.fgi( 367, 180):�
      L_(127)=R_otede.gt.R0_orede
C FDB90_logic.fgi( 359, 161):���������� >
      L_(133) = L_(128).AND.L_(127)
C FDB90_logic.fgi( 366, 166):�
      L_(145) = L_(134).OR.L_(151).OR.L_(133)
C FDB90_logic.fgi( 367, 173):���
      L_(143)=R_otede.gt.R0_usede
C FDB90_logic.fgi( 360, 188):���������� >
      L_(131)=R_otede.gt.R0_urede
C FDB90_logic.fgi( 350, 185):���������� >
      L_(142) = L0_isede.AND.L_(131)
C FDB90_logic.fgi( 354, 186):�
      L_(149) = L_(144).OR.L_(145).OR.L_(143).OR.L_(142)
C FDB90_logic.fgi( 374, 189):���
      L_(139)=R_otede.lt.R0_atede
C FDB90_logic.fgi( 361, 211):���������� <
      L_(140) = L_(141).AND.L_(139)
C FDB90_logic.fgi( 368, 216):�
      L_(130)=R_otede.lt.R0_esede
C FDB90_logic.fgi( 361, 195):���������� <
      L_(138) = L_(129).AND.L_(130)
C FDB90_logic.fgi( 367, 197):�
      L_(148) = L_(140).OR.L_(152).OR.L_(138)
C FDB90_logic.fgi( 366, 206):���
      L0_uxede=(L_(148).or.L0_uxede).and..not.(L_(149))
      L_(150)=.not.L0_uxede
C FDB90_logic.fgi( 381, 204):RS �������
      if(L0_uxede) then
         R_(116)=R_(114)
      else
         R_(116)=R_(117)
      endif
C FDB90_logic.fgi( 395, 213):���� RE IN LO CH7
      L_(137) = L0_irede.AND.L_(126)
C FDB90_logic.fgi( 352, 158):�
      L_(136)=R_otede.lt.R0_etede
C FDB90_logic.fgi( 359, 154):���������� <
      L_(146) = L_(137).OR.L_(148).OR.L_(136).OR.L_(144)
C FDB90_logic.fgi( 374, 155):���
      L0_oxede=(L_(145).or.L0_oxede).and..not.(L_(146))
      L_(147)=.not.L0_oxede
C FDB90_logic.fgi( 381, 171):RS �������
      if(L0_oxede) then
         R_(115)=R_(114)
      else
         R_(115)=R_(117)
      endif
C FDB90_logic.fgi( 395, 202):���� RE IN LO CH7
      R_ixede = R_(116) + (-R_(115))
C FDB90_logic.fgi( 401, 209):��������
      R_axede=R_axede+deltat/R0_uvede*R_ixede
      if(R_axede.gt.R0_ovede) then
         R_axede=R0_ovede
      elseif(R_axede.lt.R0_exede) then
         R_axede=R0_exede
      endif
C FDB90_logic.fgi( 412, 193):����������
      R_otede=R_axede
C FDB90_logic.fgi( 434, 195):������,20FDB91AE009-M01VY01
      L0_irede=(L_(128).or.L0_irede).and..not.(L_(146))
      L_(124)=.not.L0_irede
C FDB90_logic.fgi( 339, 160):RS �������
C sav1=L_(137)
      L_(137) = L0_irede.AND.L_(126)
C FDB90_logic.fgi( 352, 158):recalc:�
C if(sav1.ne.L_(137) .and. try1430.gt.0) goto 1430
      L0_isede=(L_(129).or.L0_isede).and..not.(L_(149))
      L_(125)=.not.L0_isede
C FDB90_logic.fgi( 348, 194):RS �������
C sav1=L_(142)
      L_(142) = L0_isede.AND.L_(131)
C FDB90_logic.fgi( 354, 186):recalc:�
C if(sav1.ne.L_(142) .and. try1410.gt.0) goto 1410
      L_(83)=R_ibede.lt.R0_avade
C FDB90_logic.fgi( 488, 309):���������� <
C label 1460  try1460=try1460-1
      L_(92)=R_ibede.gt.R0_ebede
C FDB90_logic.fgi( 501, 333):���������� >
      L_(91) = L_(92).AND.L_(89)
C FDB90_logic.fgi( 509, 332):�
      L_(84)=R_ibede.gt.R0_ivade
C FDB90_logic.fgi( 501, 313):���������� >
      L_(90) = L_(85).AND.L_(84)
C FDB90_logic.fgi( 508, 318):�
      L_(102) = L_(91).OR.L_(108).OR.L_(90)
C FDB90_logic.fgi( 509, 325):���
      L_(100)=R_ibede.gt.R0_oxade
C FDB90_logic.fgi( 502, 340):���������� >
      L_(88)=R_ibede.gt.R0_ovade
C FDB90_logic.fgi( 492, 337):���������� >
      L_(99) = L0_exade.AND.L_(88)
C FDB90_logic.fgi( 496, 338):�
      L_(106) = L_(101).OR.L_(102).OR.L_(100).OR.L_(99)
C FDB90_logic.fgi( 516, 341):���
      L_(96)=R_ibede.lt.R0_uxade
C FDB90_logic.fgi( 503, 363):���������� <
      L_(97) = L_(98).AND.L_(96)
C FDB90_logic.fgi( 510, 368):�
      L_(87)=R_ibede.lt.R0_axade
C FDB90_logic.fgi( 503, 347):���������� <
      L_(95) = L_(86).AND.L_(87)
C FDB90_logic.fgi( 509, 349):�
      L_(105) = L_(97).OR.L_(109).OR.L_(95)
C FDB90_logic.fgi( 508, 358):���
      L0_ofede=(L_(105).or.L0_ofede).and..not.(L_(106))
      L_(107)=.not.L0_ofede
C FDB90_logic.fgi( 523, 356):RS �������
      if(L0_ofede) then
         R_(104)=R_(102)
      else
         R_(104)=R_(105)
      endif
C FDB90_logic.fgi( 537, 365):���� RE IN LO CH7
      L_(94) = L0_evade.AND.L_(83)
C FDB90_logic.fgi( 494, 310):�
      L_(93)=R_ibede.lt.R0_abede
C FDB90_logic.fgi( 501, 306):���������� <
      L_(103) = L_(94).OR.L_(105).OR.L_(93).OR.L_(101)
C FDB90_logic.fgi( 516, 307):���
      L0_ifede=(L_(102).or.L0_ifede).and..not.(L_(103))
      L_(104)=.not.L0_ifede
C FDB90_logic.fgi( 523, 323):RS �������
      if(L0_ifede) then
         R_(103)=R_(102)
      else
         R_(103)=R_(105)
      endif
C FDB90_logic.fgi( 537, 354):���� RE IN LO CH7
      R_efede = R_(104) + (-R_(103))
C FDB90_logic.fgi( 543, 361):��������
      R_udede=R_udede+deltat/R0_odede*R_efede
      if(R_udede.gt.R0_idede) then
         R_udede=R0_idede
      elseif(R_udede.lt.R0_afede) then
         R_udede=R0_afede
      endif
C FDB90_logic.fgi( 554, 345):����������
      R_ibede=R_udede
C FDB90_logic.fgi( 576, 347):������,20FDB91AE013-M01VY01
      L0_evade=(L_(85).or.L0_evade).and..not.(L_(103))
      L_(81)=.not.L0_evade
C FDB90_logic.fgi( 481, 312):RS �������
C sav1=L_(94)
      L_(94) = L0_evade.AND.L_(83)
C FDB90_logic.fgi( 494, 310):recalc:�
C if(sav1.ne.L_(94) .and. try1496.gt.0) goto 1496
      L0_exade=(L_(86).or.L0_exade).and..not.(L_(106))
      L_(82)=.not.L0_exade
C FDB90_logic.fgi( 490, 346):RS �������
C sav1=L_(99)
      L_(99) = L0_exade.AND.L_(88)
C FDB90_logic.fgi( 496, 338):recalc:�
C if(sav1.ne.L_(99) .and. try1476.gt.0) goto 1476
      L_(54)=R_orade.lt.R0_emade
C FDB90_logic.fgi( 488, 227):���������� <
C label 1526  try1526=try1526-1
      L_(63)=R_orade.gt.R0_irade
C FDB90_logic.fgi( 501, 251):���������� >
      L_(62) = L_(63).AND.L_(60)
C FDB90_logic.fgi( 509, 250):�
      L_(55)=R_orade.gt.R0_omade
C FDB90_logic.fgi( 501, 231):���������� >
      L_(61) = L_(56).AND.L_(55)
C FDB90_logic.fgi( 508, 236):�
      L_(73) = L_(62).OR.L_(79).OR.L_(61)
C FDB90_logic.fgi( 509, 243):���
      L_(71)=R_orade.gt.R0_upade
C FDB90_logic.fgi( 502, 258):���������� >
      L_(59)=R_orade.gt.R0_umade
C FDB90_logic.fgi( 492, 255):���������� >
      L_(70) = L0_ipade.AND.L_(59)
C FDB90_logic.fgi( 496, 256):�
      L_(77) = L_(72).OR.L_(73).OR.L_(71).OR.L_(70)
C FDB90_logic.fgi( 516, 259):���
      L_(67)=R_orade.lt.R0_arade
C FDB90_logic.fgi( 503, 281):���������� <
      L_(68) = L_(69).AND.L_(67)
C FDB90_logic.fgi( 510, 286):�
      L_(58)=R_orade.lt.R0_epade
C FDB90_logic.fgi( 503, 265):���������� <
      L_(66) = L_(57).AND.L_(58)
C FDB90_logic.fgi( 509, 267):�
      L_(76) = L_(68).OR.L_(80).OR.L_(66)
C FDB90_logic.fgi( 508, 276):���
      L0_utade=(L_(76).or.L0_utade).and..not.(L_(77))
      L_(78)=.not.L0_utade
C FDB90_logic.fgi( 523, 274):RS �������
      if(L0_utade) then
         R_(100)=R_(98)
      else
         R_(100)=R_(101)
      endif
C FDB90_logic.fgi( 537, 283):���� RE IN LO CH7
      L_(65) = L0_imade.AND.L_(54)
C FDB90_logic.fgi( 494, 228):�
      L_(64)=R_orade.lt.R0_erade
C FDB90_logic.fgi( 501, 224):���������� <
      L_(74) = L_(65).OR.L_(76).OR.L_(64).OR.L_(72)
C FDB90_logic.fgi( 516, 225):���
      L0_otade=(L_(73).or.L0_otade).and..not.(L_(74))
      L_(75)=.not.L0_otade
C FDB90_logic.fgi( 523, 241):RS �������
      if(L0_otade) then
         R_(99)=R_(98)
      else
         R_(99)=R_(101)
      endif
C FDB90_logic.fgi( 537, 272):���� RE IN LO CH7
      R_itade = R_(100) + (-R_(99))
C FDB90_logic.fgi( 543, 279):��������
      R_atade=R_atade+deltat/R0_usade*R_itade
      if(R_atade.gt.R0_osade) then
         R_atade=R0_osade
      elseif(R_atade.lt.R0_etade) then
         R_atade=R0_etade
      endif
C FDB90_logic.fgi( 554, 263):����������
      R_orade=R_atade
C FDB90_logic.fgi( 576, 265):������,20FDB91AE010-M01VY01
      L0_imade=(L_(56).or.L0_imade).and..not.(L_(74))
      L_(52)=.not.L0_imade
C FDB90_logic.fgi( 481, 230):RS �������
C sav1=L_(65)
      L_(65) = L0_imade.AND.L_(54)
C FDB90_logic.fgi( 494, 228):recalc:�
C if(sav1.ne.L_(65) .and. try1562.gt.0) goto 1562
      L0_ipade=(L_(57).or.L0_ipade).and..not.(L_(77))
      L_(53)=.not.L0_ipade
C FDB90_logic.fgi( 490, 264):RS �������
C sav1=L_(70)
      L_(70) = L0_ipade.AND.L_(59)
C FDB90_logic.fgi( 496, 256):recalc:�
C if(sav1.ne.L_(70) .and. try1542.gt.0) goto 1542
      L_(25)=R_ufade.lt.R0_ibade
C FDB90_logic.fgi( 488, 147):���������� <
C label 1592  try1592=try1592-1
      L_(34)=R_ufade.gt.R0_ofade
C FDB90_logic.fgi( 501, 171):���������� >
      L_(33) = L_(34).AND.L_(31)
C FDB90_logic.fgi( 509, 170):�
      L_(26)=R_ufade.gt.R0_ubade
C FDB90_logic.fgi( 501, 151):���������� >
      L_(32) = L_(27).AND.L_(26)
C FDB90_logic.fgi( 508, 156):�
      L_(44) = L_(33).OR.L_(50).OR.L_(32)
C FDB90_logic.fgi( 509, 163):���
      L_(42)=R_ufade.gt.R0_afade
C FDB90_logic.fgi( 502, 178):���������� >
      L_(30)=R_ufade.gt.R0_adade
C FDB90_logic.fgi( 492, 175):���������� >
      L_(41) = L0_odade.AND.L_(30)
C FDB90_logic.fgi( 496, 176):�
      L_(48) = L_(43).OR.L_(44).OR.L_(42).OR.L_(41)
C FDB90_logic.fgi( 516, 179):���
      L_(38)=R_ufade.lt.R0_efade
C FDB90_logic.fgi( 503, 201):���������� <
      L_(39) = L_(40).AND.L_(38)
C FDB90_logic.fgi( 510, 206):�
      L_(29)=R_ufade.lt.R0_idade
C FDB90_logic.fgi( 503, 185):���������� <
      L_(37) = L_(28).AND.L_(29)
C FDB90_logic.fgi( 509, 187):�
      L_(47) = L_(39).OR.L_(51).OR.L_(37)
C FDB90_logic.fgi( 508, 196):���
      L0_amade=(L_(47).or.L0_amade).and..not.(L_(48))
      L_(49)=.not.L0_amade
C FDB90_logic.fgi( 523, 194):RS �������
      if(L0_amade) then
         R_(96)=R_(94)
      else
         R_(96)=R_(97)
      endif
C FDB90_logic.fgi( 537, 203):���� RE IN LO CH7
      L_(36) = L0_obade.AND.L_(25)
C FDB90_logic.fgi( 494, 148):�
      L_(35)=R_ufade.lt.R0_ifade
C FDB90_logic.fgi( 501, 144):���������� <
      L_(45) = L_(36).OR.L_(47).OR.L_(35).OR.L_(43)
C FDB90_logic.fgi( 516, 145):���
      L0_ulade=(L_(44).or.L0_ulade).and..not.(L_(45))
      L_(46)=.not.L0_ulade
C FDB90_logic.fgi( 523, 161):RS �������
      if(L0_ulade) then
         R_(95)=R_(94)
      else
         R_(95)=R_(97)
      endif
C FDB90_logic.fgi( 537, 192):���� RE IN LO CH7
      R_olade = R_(96) + (-R_(95))
C FDB90_logic.fgi( 543, 199):��������
      R_elade=R_elade+deltat/R0_alade*R_olade
      if(R_elade.gt.R0_ukade) then
         R_elade=R0_ukade
      elseif(R_elade.lt.R0_ilade) then
         R_elade=R0_ilade
      endif
C FDB90_logic.fgi( 554, 183):����������
      R_ufade=R_elade
C FDB90_logic.fgi( 576, 185):������,20FDB91AE011-M01VY01
      L0_obade=(L_(27).or.L0_obade).and..not.(L_(45))
      L_(23)=.not.L0_obade
C FDB90_logic.fgi( 481, 150):RS �������
C sav1=L_(36)
      L_(36) = L0_obade.AND.L_(25)
C FDB90_logic.fgi( 494, 148):recalc:�
C if(sav1.ne.L_(36) .and. try1628.gt.0) goto 1628
      L0_odade=(L_(28).or.L0_odade).and..not.(L_(48))
      L_(24)=.not.L0_odade
C FDB90_logic.fgi( 490, 184):RS �������
C sav1=L_(41)
      L_(41) = L0_odade.AND.L_(30)
C FDB90_logic.fgi( 496, 176):recalc:�
C if(sav1.ne.L_(41) .and. try1608.gt.0) goto 1608
      End
