      Subroutine FDB40(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDB40.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      Call FDB40_ConIn
      R_(42)=R0_axaf
C FDB40_logic.fgi( 215,  99):pre: ������������  �� T
      R_(39)=R0_etaf
C FDB40_logic.fgi( 217,  92):pre: ������������  �� T
      R_(38)=R0_usaf
C FDB40_logic.fgi( 208,  92):pre: �������� ��������� ������
      R_(43)=R0_abef
C FDB40_logic.fgi( 217, 129):pre: ������������  �� T
      R_(41)=R0_ivaf
C FDB40_logic.fgi( 217, 122):pre: ������������  �� T
      R_(40)=R0_avaf
C FDB40_logic.fgi( 208, 122):pre: �������� ��������� ������
      R_(12)=R8_ar
C FDB40_vent_log.fgi(  83, 277):pre: �������������� �����  
      R_(31)=R0_abud
C FDB40_logic.fgi( 211, 392):pre: ������������  �� T
      R_(24)=R0_idod
C FDB40_logic.fgi( 209, 378):pre: ������������  �� T
      R_(25)=R0_afod
C FDB40_logic.fgi( 209, 361):pre: ������������  �� T
      R_(26)=R0_ofod
C FDB40_logic.fgi( 209, 344):pre: ������������  �� T
      R_(37)=R0_okud
C FDB40_logic.fgi( 209, 327):pre: ������������  �� T
      R_(36)=R0_akud
C FDB40_logic.fgi( 209, 310):pre: ������������  �� T
      R_(35)=R0_ifud
C FDB40_logic.fgi( 272, 389):pre: ������������  �� T
      R_(34)=R0_udud
C FDB40_logic.fgi( 272, 372):pre: ������������  �� T
      R_(33)=R0_edud
C FDB40_logic.fgi( 272, 355):pre: ������������  �� T
      R_(32)=R0_obud
C FDB40_logic.fgi( 272, 343):pre: ������������  �� T
      R_(4)=R8_if
C FDB40_vent_log.fgi( 153, 219):pre: �������������� �����  
      !��������� R_(1) = FDB40_vent_logC?? /0.0005/
      R_(1)=R0_e
C FDB40_vent_log.fgi( 228, 210):���������
      L_(4)=R8_i.gt.R_(1)
C FDB40_vent_log.fgi( 232, 211):���������� >
      !��������� R_(3) = FDB40_vent_logC?? /0.0/
      R_(3)=R0_o
C FDB40_vent_log.fgi( 238, 217):���������
      !��������� R_(2) = FDB40_vent_logC?? /500/
      R_(2)=R0_u
C FDB40_vent_log.fgi( 238, 215):���������
      if(L_(4)) then
         R_(11)=R_(2)
      else
         R_(11)=R_(3)
      endif
C FDB40_vent_log.fgi( 241, 215):���� RE IN LO CH7
      R_(7) = 1.0
C FDB40_vent_log.fgi( 140, 217):��������� (RE4) (�������)
      R_(8) = 0.0
C FDB40_vent_log.fgi( 140, 219):��������� (RE4) (�������)
      !��������� R_(5) = FDB40_vent_logC?? /1.3/
      R_(5)=R0_id
C FDB40_vent_log.fgi( 135, 238):���������
      !��������� R_(9) = FDB40_vent_logC?? /1/
      R_(9)=R0_of
C FDB40_vent_log.fgi( 237, 235):���������
      L_(8)=R8_uf.lt.R_(9)
C FDB40_vent_log.fgi( 241, 236):���������� <
      L_ek=L_ak.or.(L_ek.and..not.(L_(8)))
      L_(9)=.not.L_ek
C FDB40_vent_log.fgi( 271, 238):RS �������
      L_ok=L_ek
C FDB40_vent_log.fgi( 285, 240):������,20FDB42CW001_OUT
      !��������� R_(10) = FDB40_vent_logC?? /-50000/
      R_(10)=R0_ik
C FDB40_vent_log.fgi( 266, 214):���������
      if(L_ok) then
         R8_uk=R_(10)
      else
         R8_uk=R_(11)
      endif
C FDB40_vent_log.fgi( 269, 214):���� RE IN LO CH7
      R_ased = R8_al + (-R8_ep)
C FDB40_vent_log.fgi(  53, 101):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ared,R_oted,REAL(-1,4
     &),
     & REAL(R_ored,4),REAL(R_ured,4),
     & REAL(R_uped,4),REAL(R_oped,4),I_ited,
     & REAL(R_esed,4),L_ised,REAL(R_osed,4),L_used,L_ated
     &,R_esaf,
     & REAL(R_ired,4),REAL(R_ered,4),L_eted,REAL(R_ased,4
     &))
      !}
C FDB40_vlv.fgi(  32,  12):���������� �������,20FDB42CP001XQ01
      L_asaf=R_esaf.lt.R0_oraf
C FDB40_logic.fgi( 194,  62):���������� <
      L_uraf=L_asaf
C FDB40_logic.fgi( 230,  53):������,20FDB42AA005_uluop
      L_iraf=L_asaf
C FDB40_logic.fgi( 230,  62):������,20FDB41AA001_ulucl
      L_eraf = (.NOT.L_asaf)
C FDB40_logic.fgi( 203,  46):���
      L_araf=L_eraf
C FDB40_logic.fgi( 230,  37):������,20FDB42AA005_ulucl
      L_upaf=L_eraf
C FDB40_logic.fgi( 230,  46):������,20FDB41AA001_uluop
      R_ixed = R8_el
C FDB40_vent_log.fgi(  56, 110):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_eved,R_adid,REAL(1,4)
     &,
     & REAL(R_uved,4),REAL(R_axed,4),
     & REAL(R_aved,4),REAL(R_uted,4),I_ubid,
     & REAL(R_oxed,4),L_uxed,REAL(R_abid,4),L_ebid,L_ibid
     &,R_exed,
     & REAL(R_oved,4),REAL(R_ived,4),L_obid,REAL(R_ixed,4
     &))
      !}
C FDB40_vlv.fgi(  32,  28):���������� �������,20FDB42CT001XQ01
      R_ided = R8_il
C FDB40_vent_log.fgi(  56, 118):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ebed,R_aked,REAL(1,4)
     &,
     & REAL(R_ubed,4),REAL(R_aded,4),
     & REAL(R_abed,4),REAL(R_uxad,4),I_ufed,
     & REAL(R_oded,4),L_uded,REAL(R_afed,4),L_efed,L_ifed
     &,R_eded,
     & REAL(R_obed,4),REAL(R_ibed,4),L_ofed,REAL(R_ided,4
     &))
      !}
C FDB40_vlv.fgi(  32,  44):���������� �������,20FDB41CU001XQ01
      R_et = R8_ol
C FDB40_vent_log.fgi(  54, 127):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_as,R_uv,REAL(1,4),
     & REAL(R_os,4),REAL(R_us,4),
     & REAL(R_ur,4),REAL(R_or,4),I_ov,
     & REAL(R_it,4),L_ot,REAL(R_ut,4),L_av,L_ev,R_at,
     & REAL(R_is,4),REAL(R_es,4),L_iv,REAL(R_et,4))
      !}
C FDB40_vlv.fgi(  62,  59):���������� �������,20FDB42CF001XQ01
      if(R8_om.le.R0_im) then
         R_ekad=R0_em
      elseif(R8_om.gt.R0_am) then
         R_ekad=R0_ul
      else
         R_ekad=R0_em+(R8_om-(R0_im))*(R0_ul-(R0_em))/(R0_am
     &-(R0_im))
      endif
C FDB40_vent_log.fgi(  55, 139):��������������� ���������
      L_(5)=R_ekad.gt.R_(5)
C FDB40_vent_log.fgi( 139, 239):���������� >
      L_(6) = L_(5).OR.L_ad
C FDB40_vent_log.fgi( 164, 238):���
      L_ud=L_od.or.(L_ud.and..not.(L_(6)))
      L_(7)=.not.L_ud
C FDB40_vent_log.fgi( 170, 240):RS �������
      L_af=L_ud
C FDB40_vent_log.fgi( 184, 242):������,FDB42dust_start
      if(L_af) then
         R_(6)=R_(7)
      else
         R_(6)=R_(8)
      endif
C FDB40_vent_log.fgi( 143, 218):���� RE IN LO CH7
      R8_if=(R0_ed*R_(4)+deltat*R_(6))/(R0_ed+deltat)
C FDB40_vent_log.fgi( 153, 219):�������������� �����  
      R8_ef=R8_if
C FDB40_vent_log.fgi( 172, 219):������,20FDB42AN101Y01
      !{
      Call DAT_ANA_HANDLER(deltat,R_afad,R_ulad,REAL(1,4)
     &,
     & REAL(R_ofad,4),REAL(R_ufad,4),
     & REAL(R_udad,4),REAL(R_odad,4),I_olad,
     & REAL(R_ikad,4),L_okad,REAL(R_ukad,4),L_alad,L_elad
     &,R_akad,
     & REAL(R_ifad,4),REAL(R_efad,4),L_ilad,REAL(R_ekad,4
     &))
      !}
C FDB40_vlv.fgi(  32,  59):���������� �������,20FDB41CW001XQ01
      R_avad = R8_um
C FDB40_vent_log.fgi(  56, 147):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_usad,R_oxad,REAL(1,4)
     &,
     & REAL(R_itad,4),REAL(R_otad,4),
     & REAL(R_osad,4),REAL(R_isad,4),I_ixad,
     & REAL(R_evad,4),L_ivad,REAL(R_ovad,4),L_uvad,L_axad
     &,R_utad,
     & REAL(R_etad,4),REAL(R_atad,4),L_exad,REAL(R_avad,4
     &))
      !}
C FDB40_vlv.fgi(  62,  74):���������� �������,20FDB41CM001XQ01
      R_uxu = R8_ap + (-R8_ep)
C FDB40_vent_log.fgi(  53, 155):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ovu,R_idad,REAL(0.001
     &,4),
     & REAL(R_exu,4),REAL(R_ixu,4),
     & REAL(R_ivu,4),REAL(R_evu,4),I_edad,
     & REAL(R_abad,4),L_ebad,REAL(R_ibad,4),L_obad,L_ubad
     &,R_oxu,
     & REAL(R_axu,4),REAL(R_uvu,4),L_adad,REAL(R_uxu,4))
      !}
C FDB40_vlv.fgi(  32,  74):���������� �������,20FDB42CP003XQ01
      R_uled = R8_ip
C FDB40_vent_log.fgi(  58, 163):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_oked,R_iped,REAL(1,4)
     &,
     & REAL(R_eled,4),REAL(R_iled,4),
     & REAL(R_iked,4),REAL(R_eked,4),I_eped,
     & REAL(R_amed,4),L_emed,REAL(R_imed,4),L_omed,L_umed
     &,R_oled,
     & REAL(R_aled,4),REAL(R_uked,4),L_aped,REAL(R_uled,4
     &))
      !}
C FDB40_vlv.fgi(  62,  89):���������� �������,20FDB41CP002XQ01
      !��������� R_(15) = FDB40_vent_logC?? /0.0/
      R_(15)=R0_er
C FDB40_vent_log.fgi(  68, 278):���������
      !��������� R_(14) = FDB40_vent_logC?? /0.001/
      R_(14)=R0_ir
C FDB40_vent_log.fgi(  68, 276):���������
      !{
      Call DAT_ANA_HANDLER(deltat,R_imad,R_esad,REAL(1,4)
     &,
     & REAL(R_apad,4),REAL(R_epad,4),
     & REAL(R_emad,4),REAL(R_amad,4),I_asad,
     & REAL(R_upad,4),L_arad,REAL(R_erad,4),L_irad,L_orad
     &,R_ipad,
     & REAL(R_umad,4),REAL(R_omad,4),L_urad,REAL(R_opad,4
     &))
      !}
C FDB40_vlv.fgi(  32,  89):���������� �������,20FDB41CG001XQ01
      L_(12)=I_ekid.ne.0
C FDB40_logic.fgi( 368, 237):��������� 1->LO
      L_(17)=I_afid.ne.0
C FDB40_logic.fgi( 368, 243):��������� 1->LO
      L_(15) = L_(12).OR.L_(17)
C FDB40_logic.fgi( 385, 250):���
      L_(18)=I_efid.ne.0
C FDB40_logic.fgi( 368, 254):��������� 1->LO
      L0_ufid=(L_(18).or.L0_ufid).and..not.(L_(15))
      L_(16)=.not.L0_ufid
C FDB40_logic.fgi( 393, 252):RS �������
      L_(13) = L_(18).OR.L_(12)
C FDB40_logic.fgi( 385, 239):���
      L0_ofid=(L_(17).or.L0_ofid).and..not.(L_(13))
      L_(14)=.not.L0_ofid
C FDB40_logic.fgi( 393, 241):RS �������
      R_(16) = 60
C FDB40_logic.fgi( 396, 265):��������� (RE4) (�������)
      R_(19) = 0.0
C FDB40_logic.fgi( 404, 256):��������� (RE4) (�������)
      if(L0_ofid) then
         R_(17)=R_(16)
      else
         R_(17)=R_(19)
      endif
C FDB40_logic.fgi( 408, 248):���� RE IN LO CH7
      if(L0_ufid) then
         R_(18)=R_(16)
      else
         R_(18)=R_(19)
      endif
C FDB40_logic.fgi( 408, 266):���� RE IN LO CH7
      R_akid = R_(18) + (-R_(17))
C FDB40_logic.fgi( 414, 259):��������
      R_ifid=R_ifid+deltat/R0_idid*R_akid
      if(R_ifid.gt.R0_edid) then
         R_ifid=R0_edid
      elseif(R_ifid.lt.R0_odid) then
         R_ifid=R0_odid
      endif
C FDB40_logic.fgi( 429, 243):����������
      R_udid=R_ifid
C FDB40_logic.fgi( 456, 245):������,20FDB42AE501KE01VY01
      L_(26)=I_amid.ne.0
C FDB40_logic.fgi( 190, 201):��������� 1->LO
      L_(27)=I_amid.ne.0
C FDB40_logic.fgi( 191, 257):��������� 1->LO
      L_(31)=I_umid.ne.0
C FDB40_logic.fgi( 191, 215):��������� 1->LO
      L_(36)=I_epid.ne.0
C FDB40_logic.fgi( 191, 242):��������� 1->LO
      L_(41)=I_esid.ne.0
C FDB40_logic.fgi( 201, 167):��������� 1->LO
      L_(48)=I_ubef.ne.0
C FDB40_logic.fgi( 191, 221):��������� 1->LO
      L_(49)=I_erid.ne.0
C FDB40_logic.fgi( 201, 229):��������� 1->LO
      R_(20) = 60
C FDB40_logic.fgi( 239, 242):��������� (RE4) (�������)
      R_(23) = 0.0
C FDB40_logic.fgi( 247, 233):��������� (RE4) (�������)
      C30_isid = '������� "1"'
C FDB40_logic.fgi( 392, 451):��������� ���������� CH20 (CH30) (�������)
      C30_osid = '������� "2"'
C FDB40_logic.fgi( 408, 450):��������� ���������� CH20 (CH30) (�������)
      C30_atid = '������� "3"'
C FDB40_logic.fgi( 425, 449):��������� ���������� CH20 (CH30) (�������)
      C30_itid = '������� "4"'
C FDB40_logic.fgi( 442, 448):��������� ���������� CH20 (CH30) (�������)
      C30_utid = '������� "5"'
C FDB40_logic.fgi( 459, 447):��������� ���������� CH20 (CH30) (�������)
      C30_evid = '������� "6"'
C FDB40_logic.fgi( 475, 446):��������� ���������� CH20 (CH30) (�������)
      C30_ovid = '������� "7"'
C FDB40_logic.fgi( 492, 445):��������� ���������� CH20 (CH30) (�������)
      C30_axid = '������� "8"'
C FDB40_logic.fgi( 509, 444):��������� ���������� CH20 (CH30) (�������)
      C30_ixid = '���� ��������'
C FDB40_logic.fgi( 526, 443):��������� ���������� CH20 (CH30) (�������)
      C30_ibod = ''
C FDB40_logic.fgi( 376, 454):��������� ���������� CH20 (CH30) (�������)
      C30_ebod = '���.����'
C FDB40_logic.fgi( 376, 452):��������� ���������� CH20 (CH30) (�������)
      L_(100)=I_uxod.ne.0
C FDB40_logic.fgi( 190, 496):��������� 1->LO
      L_(107)=I_ovod.ne.0
C FDB40_logic.fgi( 190, 503):��������� 1->LO
      L_(108)=I_uvod.ne.0
C FDB40_logic.fgi( 190, 512):��������� 1->LO
      R_(27) = 60
C FDB40_logic.fgi( 218, 524):��������� (RE4) (�������)
      R_(30) = 0.0
C FDB40_logic.fgi( 226, 515):��������� (RE4) (�������)
      C8_olud = 'pos9'
C FDB40_logic.fgi( 260, 342):��������� ���������� CH8 (�������)
      call chcomp(C8_odaf,C8_olud,L_(113))
C FDB40_logic.fgi( 265, 343):���������� ���������
      if(L_(113).and..not.L0_adud) then
         R0_obud=R0_ubud
      else
         R0_obud=max(R_(32)-deltat,0.0)
      endif
      L_(109)=R0_obud.gt.0.0
      L0_adud=L_(113)
C FDB40_logic.fgi( 272, 343):������������  �� T
      C8_omud = 'pos8'
C FDB40_logic.fgi( 260, 354):��������� ���������� CH8 (�������)
      call chcomp(C8_odaf,C8_omud,L_(116))
C FDB40_logic.fgi( 265, 355):���������� ���������
      if(L_(116).and..not.L0_odud) then
         R0_edud=R0_idud
      else
         R0_edud=max(R_(33)-deltat,0.0)
      endif
      L_(110)=R0_edud.gt.0.0
      L0_odud=L_(116)
C FDB40_logic.fgi( 272, 355):������������  �� T
      C8_opud = 'pos7'
C FDB40_logic.fgi( 260, 371):��������� ���������� CH8 (�������)
      call chcomp(C8_odaf,C8_opud,L_(119))
C FDB40_logic.fgi( 265, 372):���������� ���������
      if(L_(119).and..not.L0_efud) then
         R0_udud=R0_afud
      else
         R0_udud=max(R_(34)-deltat,0.0)
      endif
      L_(111)=R0_udud.gt.0.0
      L0_efud=L_(119)
C FDB40_logic.fgi( 272, 372):������������  �� T
      C8_orud = 'pos6'
C FDB40_logic.fgi( 260, 388):��������� ���������� CH8 (�������)
      call chcomp(C8_odaf,C8_orud,L_(123))
C FDB40_logic.fgi( 265, 389):���������� ���������
      if(L_(123).and..not.L0_ufud) then
         R0_ifud=R0_ofud
      else
         R0_ifud=max(R_(35)-deltat,0.0)
      endif
      L_(121)=R0_ifud.gt.0.0
      L0_ufud=L_(123)
C FDB40_logic.fgi( 272, 389):������������  �� T
      C8_osud = 'pos5'
C FDB40_logic.fgi( 197, 309):��������� ���������� CH8 (�������)
      call chcomp(C8_odaf,C8_osud,L_(127))
C FDB40_logic.fgi( 202, 310):���������� ���������
      if(L_(127).and..not.L0_ikud) then
         R0_akud=R0_ekud
      else
         R0_akud=max(R_(36)-deltat,0.0)
      endif
      L_(125)=R0_akud.gt.0.0
      L0_ikud=L_(127)
C FDB40_logic.fgi( 209, 310):������������  �� T
      C8_otud = 'pos4'
C FDB40_logic.fgi( 197, 326):��������� ���������� CH8 (�������)
      call chcomp(C8_odaf,C8_otud,L_(131))
C FDB40_logic.fgi( 202, 327):���������� ���������
      if(L_(131).and..not.L0_alud) then
         R0_okud=R0_ukud
      else
         R0_okud=max(R_(37)-deltat,0.0)
      endif
      L_(129)=R0_okud.gt.0.0
      L0_alud=L_(131)
C FDB40_logic.fgi( 209, 327):������������  �� T
      C8_ovud = 'pos3'
C FDB40_logic.fgi( 197, 343):��������� ���������� CH8 (�������)
      call chcomp(C8_odaf,C8_ovud,L_(134))
C FDB40_logic.fgi( 202, 344):���������� ���������
      if(L_(134).and..not.L0_akod) then
         R0_ofod=R0_ufod
      else
         R0_ofod=max(R_(26)-deltat,0.0)
      endif
      L_(52)=R0_ofod.gt.0.0
      L0_akod=L_(134)
C FDB40_logic.fgi( 209, 344):������������  �� T
      C8_oxud = 'pos2'
C FDB40_logic.fgi( 197, 360):��������� ���������� CH8 (�������)
      call chcomp(C8_odaf,C8_oxud,L_(137))
C FDB40_logic.fgi( 202, 361):���������� ���������
      if(L_(137).and..not.L0_ifod) then
         R0_afod=R0_efod
      else
         R0_afod=max(R_(25)-deltat,0.0)
      endif
      L_(51)=R0_afod.gt.0.0
      L0_ifod=L_(137)
C FDB40_logic.fgi( 209, 361):������������  �� T
      C8_obaf = 'pos1'
C FDB40_logic.fgi( 197, 377):��������� ���������� CH8 (�������)
      call chcomp(C8_odaf,C8_obaf,L_(140))
C FDB40_logic.fgi( 202, 378):���������� ���������
      if(L_(140).and..not.L0_udod) then
         R0_idod=R0_odod
      else
         R0_idod=max(R_(24)-deltat,0.0)
      endif
      L_(50)=R0_idod.gt.0.0
      L0_udod=L_(140)
C FDB40_logic.fgi( 209, 378):������������  �� T
      C8_idaf = 'pos0'
C FDB40_logic.fgi( 197, 388):��������� ���������� CH8 (�������)
      call chcomp(C8_odaf,C8_idaf,L_(143))
C FDB40_logic.fgi( 202, 389):���������� ���������
      if(L_(143).and..not.L0_ibud) then
         R0_abud=R0_ebud
      else
         R0_abud=max(R_(31)-deltat,0.0)
      endif
      L_(141)=R0_abud.gt.0.0
      L0_ibud=L_(143)
C FDB40_logic.fgi( 211, 392):������������  �� T
      C30_udaf = '������������������'
C FDB40_logic.fgi(  79, 553):��������� ���������� CH20 (CH30) (�������)
      C30_afaf = '������������'
C FDB40_logic.fgi( 100, 557):��������� ���������� CH20 (CH30) (�������)
      C30_efaf = '������'
C FDB40_logic.fgi(  94, 552):��������� ���������� CH20 (CH30) (�������)
      C30_ufaf = '�� ������'
C FDB40_logic.fgi(  79, 555):��������� ���������� CH20 (CH30) (�������)
      I_(2) = z'01000007'
C FDB40_logic.fgi( 108, 522):��������� ������������� IN (�������)
      I_(1) = z'0100000A'
C FDB40_logic.fgi( 108, 520):��������� ������������� IN (�������)
      I_(4) = z'01000007'
C FDB40_logic.fgi( 108, 532):��������� ������������� IN (�������)
      I_(3) = z'0100000A'
C FDB40_logic.fgi( 108, 530):��������� ������������� IN (�������)
      I_(6) = z'01000007'
C FDB40_logic.fgi( 108, 542):��������� ������������� IN (�������)
      I_(5) = z'0100000A'
C FDB40_logic.fgi( 108, 540):��������� ������������� IN (�������)
      L_(153)=.true.
C FDB40_logic.fgi(  48, 528):��������� ���������� (�������)
      L0_elaf=R0_olaf.ne.R0_ilaf
      R0_ilaf=R0_olaf
C FDB40_logic.fgi(  35, 521):���������� ������������� ������
      if(L0_elaf) then
         L_(150)=L_(153)
      else
         L_(150)=.false.
      endif
C FDB40_logic.fgi(  52, 527):���� � ������������� �������
      L_(3)=L_(150)
C FDB40_logic.fgi(  52, 527):������-�������: ���������� ��� �������������� ������
      L_(154)=.true.
C FDB40_logic.fgi(  48, 545):��������� ���������� (�������)
      L0_emaf=R0_omaf.ne.R0_imaf
      R0_imaf=R0_omaf
C FDB40_logic.fgi(  35, 538):���������� ������������� ������
      if(L0_emaf) then
         L_(151)=L_(154)
      else
         L_(151)=.false.
      endif
C FDB40_logic.fgi(  52, 544):���� � ������������� �������
      L_(2)=L_(151)
C FDB40_logic.fgi(  52, 544):������-�������: ���������� ��� �������������� ������
      L_(148) = L_(2).OR.L_(3)
C FDB40_logic.fgi(  62, 557):���
      L_(155)=.true.
C FDB40_logic.fgi(  48, 561):��������� ���������� (�������)
      L0_epaf=R0_opaf.ne.R0_ipaf
      R0_ipaf=R0_opaf
C FDB40_logic.fgi(  35, 554):���������� ������������� ������
      if(L0_epaf) then
         L_(152)=L_(155)
      else
         L_(152)=.false.
      endif
C FDB40_logic.fgi(  52, 560):���� � ������������� �������
      L_(1)=L_(152)
C FDB40_logic.fgi(  52, 560):������-�������: ���������� ��� �������������� ������
      L_(144) = L_(1).OR.L_(2)
C FDB40_logic.fgi(  62, 524):���
      L_alaf=(L_(3).or.L_alaf).and..not.(L_(144))
      L_(145)=.not.L_alaf
C FDB40_logic.fgi(  70, 526):RS �������
      L_ukaf=L_alaf
C FDB40_logic.fgi(  86, 528):������,FDB4_tech_mode
      if(L_alaf) then
         I_ekaf=I_(1)
      else
         I_ekaf=I_(2)
      endif
C FDB40_logic.fgi( 111, 520):���� RE IN LO CH7
      L_(146) = L_(1).OR.L_(3)
C FDB40_logic.fgi(  62, 541):���
      L_amaf=(L_(2).or.L_amaf).and..not.(L_(146))
      L_(147)=.not.L_amaf
C FDB40_logic.fgi(  70, 543):RS �������
      L_ulaf=L_amaf
C FDB40_logic.fgi(  86, 545):������,FDB4_ruch_mode
      if(L_amaf) then
         I_ikaf=I_(3)
      else
         I_ikaf=I_(4)
      endif
C FDB40_logic.fgi( 111, 530):���� RE IN LO CH7
      L_apaf=(L_(1).or.L_apaf).and..not.(L_(148))
      L_(149)=.not.L_apaf
C FDB40_logic.fgi(  70, 559):RS �������
      L_umaf=L_apaf
C FDB40_logic.fgi(  86, 561):������,FDB4_avt_mode
      !{
      Call BVALVE_MAN(deltat,REAL(R_ulu,4),L_esu,L_isu,R8_aku
     &,L_ukaf,
     & L_ulaf,L_umaf,I_eru,I_iru,R_adu,
     & REAL(R_idu,4),R_efu,REAL(R_ofu,4),
     & R_odu,REAL(R_afu,4),I_ipu,I_oru,I_aru,I_upu,L_ufu,
     & L_uru,L_etu,L_ifu,L_udu,
     & L_iku,L_eku,L_osu,L_edu,L_uku,L_utu,L_asu,
     & L_ilu,L_olu,REAL(R8_ubu,8),REAL(1.0,4),R8_alu,L_asaf
     &,
     & L_eraf,L_itu,R_epu,REAL(R_oku,4),L_otu,L_avu,L_amu
     &,
     & L_emu,L_imu,L_umu,L_apu,L_omu)
      !}

      if(L_apu.or.L_umu.or.L_omu.or.L_imu.or.L_emu.or.L_amu
     &) then      
                  I_opu = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_opu = z'40000000'
      endif
C FDB40_vlv.fgi( 114,  84):���� ���������� ������ �������,20FDB42AA007
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ike,4),L_upe,L_are,R8_ade
     &,C30_ife,
     & L_ukaf,L_ulaf,L_umaf,I_ume,I_ape,R_ufe,R_ofe,
     & R_ax,REAL(R_ix,4),R_ebe,
     & REAL(R_obe,4),R_ox,REAL(R_abe,4),I_ame,
     & I_epe,I_ome,I_ime,L_ube,L_ipe,L_ure,L_ibe,
     & L_ux,L_ude,L_ode,L_ere,L_ex,L_efe,
     & L_ise,L_ope,L_ake,L_eke,REAL(R8_ubu,8),REAL(1.0,4)
     &,R8_alu,
     & L_ide,L_ede,L_ase,R_ule,REAL(R_afe,4),L_ese,L_ose,
     & L_oke,L_uke,L_ale,L_ile,L_ole,L_ele)
      !}

      if(L_ole.or.L_ile.or.L_ele.or.L_ale.or.L_uke.or.L_oke
     &) then      
                  I_eme = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_eme = z'40000000'
      endif
C FDB40_vlv.fgi( 148,  84):���� ���������� �������� ��������,20FDB41AA002
      !{
      Call BVALVE_MAN(deltat,REAL(R_iro,4),L_uvo,L_axo,R8_umo
     &,L_ukaf,
     & L_ulaf,L_umaf,I_uto,I_avo,R_uko,
     & REAL(R_elo,4),R_amo,REAL(R_imo,4),
     & R_ilo,REAL(R_ulo,4),I_ato,I_evo,I_oto,I_ito,L_omo,
     & L_ivo,L_uxo,L_emo,L_olo,
     & L_epo,L_apo,L_exo,L_alo,L_opo,L_ibu,L_ovo,
     & L_aro,L_ero,REAL(R8_ubu,8),REAL(1.0,4),R8_alu,L_araf
     &,
     & L_uraf,L_abu,R_uso,REAL(R_ipo,4),L_ebu,L_obu,L_oro
     &,
     & L_uro,L_aso,L_iso,L_oso,L_eso)
      !}

      if(L_oso.or.L_iso.or.L_eso.or.L_aso.or.L_uro.or.L_oro
     &) then      
                  I_eto = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_eto = z'40000000'
      endif
C FDB40_vlv.fgi(  97,  84):���� ���������� ������ �������,20FDB42AA005
      !{
      Call BVALVE_MAN(deltat,REAL(R_ivi,4),L_udo,L_afo,R8_usi
     &,L_ukaf,
     & L_ulaf,L_umaf,I_ubo,I_ado,R_upi,
     & REAL(R_eri,4),R_asi,REAL(R_isi,4),
     & R_iri,REAL(R_uri,4),I_abo,I_edo,I_obo,I_ibo,L_osi,
     & L_ido,L_ufo,L_esi,L_ori,
     & L_eti,L_ati,L_efo,L_ari,L_oti,L_iko,L_odo,
     & L_avi,L_evi,REAL(R8_ubu,8),REAL(1.0,4),R8_alu,L_iraf
     &,
     & L_upaf,L_ako,R_uxi,REAL(R_iti,4),L_eko,L_oko,L_ovi
     &,
     & L_uvi,L_axi,L_ixi,L_oxi,L_exi)
      !}

      if(L_oxi.or.L_ixi.or.L_exi.or.L_axi.or.L_uvi.or.L_ovi
     &) then      
                  I_ebo = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_ebo = z'40000000'
      endif
C FDB40_vlv.fgi( 131,  84):���� ���������� ������ �������,20FDB41AA001
      if(L_apaf) then
         I_okaf=I_(5)
      else
         I_okaf=I_(6)
      endif
C FDB40_logic.fgi( 111, 540):���� RE IN LO CH7
      if(L_apaf) then
         C30_ofaf=C30_udaf
      else
         C30_ofaf=C30_ufaf
      endif
C FDB40_logic.fgi(  83, 553):���� RE IN LO CH20
      if(L_amaf) then
         C30_ifaf=C30_efaf
      else
         C30_ifaf=C30_ofaf
      endif
C FDB40_logic.fgi(  98, 552):���� RE IN LO CH20
      if(L_alaf) then
         C30_akaf=C30_afaf
      else
         C30_akaf=C30_ifaf
      endif
C FDB40_logic.fgi( 107, 551):���� RE IN LO CH20
      L_(161)=I_ubef.ne.0
C FDB40_logic.fgi( 183,  99):��������� 1->LO
      L_(165)=I_ubef.ne.0
C FDB40_logic.fgi( 183, 129):��������� 1->LO
      Call PUMP_HANDLER(deltat,C30_ixe,I_adi,L_ukaf,L_ulaf
     &,L_umaf,
     & I_edi,I_ubi,R_eve,REAL(R_ove,4),
     & R_ote,REAL(R_ave,4),I_idi,L_ali,L_aki,L_umi,
     & L_api,L_ebi,L_efi,L_ofi,L_ifi,L_ufi,L_oki,L_ite,
     & L_ive,L_ete,L_ute,L_emi,L_(11),
     & L_imi,L_(10),L_ate,L_use,L_ibi,I_odi,R_ili,R_oli,
     & L_opi,L_uki,L_omi,REAL(R8_ubu,8),L_iki,
     & REAL(R8_eki,8),R_epi,REAL(R_udi,4),R_afi,REAL(R8_abi
     &,8),R_uxe,
     & R8_alu,R_ipi,R8_eki,REAL(R_uve,4),REAL(R_axe,4))
C FDB40_vlv.fgi(  97,  59):���������� ���������� �������,20FDB41CU001KN01
C label 311  try311=try311-1
C sav1=R_ipi
C sav2=R_epi
C sav3=L_ali
C sav4=L_oki
C sav5=L_aki
C sav6=R8_eki
C sav7=R_afi
C sav8=I_odi
C sav9=I_idi
C sav10=I_edi
C sav11=I_adi
C sav12=I_ubi
C sav13=I_obi
C sav14=L_ibi
C sav15=L_ebi
C sav16=R_uxe
C sav17=C30_ixe
C sav18=L_ive
C sav19=L_ute
      Call PUMP_HANDLER(deltat,C30_ixe,I_adi,L_ukaf,L_ulaf
     &,L_umaf,
     & I_edi,I_ubi,R_eve,REAL(R_ove,4),
     & R_ote,REAL(R_ave,4),I_idi,L_ali,L_aki,L_umi,
     & L_api,L_ebi,L_efi,L_ofi,L_ifi,L_ufi,L_oki,L_ite,
     & L_ive,L_ete,L_ute,L_emi,L_(11),
     & L_imi,L_(10),L_ate,L_use,L_ibi,I_odi,R_ili,R_oli,
     & L_opi,L_uki,L_omi,REAL(R8_ubu,8),L_iki,
     & REAL(R8_eki,8),R_epi,REAL(R_udi,4),R_afi,REAL(R8_abi
     &,8),R_uxe,
     & R8_alu,R_ipi,R8_eki,REAL(R_uve,4),REAL(R_axe,4))
C FDB40_vlv.fgi(  97,  59):recalc:���������� ���������� �������,20FDB41CU001KN01
C if(sav1.ne.R_ipi .and. try311.gt.0) goto 311
C if(sav2.ne.R_epi .and. try311.gt.0) goto 311
C if(sav3.ne.L_ali .and. try311.gt.0) goto 311
C if(sav4.ne.L_oki .and. try311.gt.0) goto 311
C if(sav5.ne.L_aki .and. try311.gt.0) goto 311
C if(sav6.ne.R8_eki .and. try311.gt.0) goto 311
C if(sav7.ne.R_afi .and. try311.gt.0) goto 311
C if(sav8.ne.I_odi .and. try311.gt.0) goto 311
C if(sav9.ne.I_idi .and. try311.gt.0) goto 311
C if(sav10.ne.I_edi .and. try311.gt.0) goto 311
C if(sav11.ne.I_adi .and. try311.gt.0) goto 311
C if(sav12.ne.I_ubi .and. try311.gt.0) goto 311
C if(sav13.ne.I_obi .and. try311.gt.0) goto 311
C if(sav14.ne.L_ibi .and. try311.gt.0) goto 311
C if(sav15.ne.L_ebi .and. try311.gt.0) goto 311
C if(sav16.ne.R_uxe .and. try311.gt.0) goto 311
C if(sav17.ne.C30_ixe .and. try311.gt.0) goto 311
C if(sav18.ne.L_ive .and. try311.gt.0) goto 311
C if(sav19.ne.L_ute .and. try311.gt.0) goto 311
      if(L_oki) then
         R_(13)=R_(14)
      else
         R_(13)=R_(15)
      endif
C FDB40_vent_log.fgi(  71, 276):���� RE IN LO CH7
      R8_ar=(R0_op*R_(12)+deltat*R_(13))/(R0_op+deltat)
C FDB40_vent_log.fgi(  83, 277):�������������� �����  
      R8_up=R8_ar
C FDB40_vent_log.fgi( 100, 277):������,F_FDB41CU001_G
      L_(25)=R_arid.gt.R0_olid
C FDB40_logic.fgi( 191, 194):���������� >
C label 319  try319=try319-1
      L_(30)=R_arid.gt.R0_imid
C FDB40_logic.fgi( 192, 209):���������� >
      L_(33) = L_(31).AND.L_(30)
C FDB40_logic.fgi( 203, 214):�
      L_(32) = L_(26).AND.L_(25)
C FDB40_logic.fgi( 203, 195):�
      L_(42) = L_(48).OR.L_(33).OR.L_(32)
C FDB40_logic.fgi( 216, 219):���
      L_(40)=R_arid.gt.R0_omid
C FDB40_logic.fgi( 211, 226):���������� >
      L_(21)=R_arid.gt.R0_ukid
C FDB40_logic.fgi( 196, 272):���������� >
      L_(20)=R_arid.lt.R0_okid
C FDB40_logic.fgi( 196, 266):���������� <
      L_(29)=R_arid.lt.R0_ulid
C FDB40_logic.fgi( 192, 250):���������� <
      L_(28) = L_(27).AND.L_(29)
C FDB40_logic.fgi( 202, 251):�
      L_(39) = L_(21).AND.L_(20).AND.L0_ikid
C FDB40_logic.fgi( 219, 270):�
      L_(46) = L_(41).OR.L_(42).OR.L_(40).OR.L_(39)
C FDB40_logic.fgi( 228, 227):���
      L_(34)=R_arid.lt.R0_apid
C FDB40_logic.fgi( 192, 235):���������� <
      L_(35) = L_(36).AND.L_(34)
C FDB40_logic.fgi( 204, 236):�
      L_(45) = L_(28).OR.L_(35).OR.L_(49)
C FDB40_logic.fgi( 216, 231):���
      L0_urid=(L_(45).or.L0_urid).and..not.(L_(46))
      L_(47)=.not.L0_urid
C FDB40_logic.fgi( 236, 229):RS �������
      if(L0_urid) then
         R_(22)=R_(20)
      else
         R_(22)=R_(23)
      endif
C FDB40_logic.fgi( 251, 243):���� RE IN LO CH7
      L_(38)=R_arid.lt.R0_emid
C FDB40_logic.fgi( 217, 208):���������� <
      L_(24)=R_arid.gt.R0_ilid
C FDB40_logic.fgi( 191, 188):���������� >
      L_(23)=R_arid.lt.R0_elid
C FDB40_logic.fgi( 191, 182):���������� <
      L_(37) = L_(24).AND.L_(23).AND.L0_alid
C FDB40_logic.fgi( 217, 186):�
      L_(43) = L_(45).OR.L_(41).OR.L_(38).OR.L_(37)
C FDB40_logic.fgi( 231, 215):���
      L0_orid=(L_(42).or.L0_orid).and..not.(L_(43))
      L_(44)=.not.L0_orid
C FDB40_logic.fgi( 245, 217):RS �������
      if(L0_orid) then
         R_(21)=R_(20)
      else
         R_(21)=R_(23)
      endif
C FDB40_logic.fgi( 251, 225):���� RE IN LO CH7
      R_asid = R_(22) + (-R_(21))
C FDB40_logic.fgi( 257, 236):��������
      R_irid=R_irid+deltat/R0_opid*R_asid
      if(R_irid.gt.R0_ipid) then
         R_irid=R0_ipid
      elseif(R_irid.lt.R0_upid) then
         R_irid=R0_upid
      endif
C FDB40_logic.fgi( 272, 223):����������
      R_arid=R_irid
C FDB40_logic.fgi( 300, 225):������,20FDB42AE401-M01VX01
      L0_alid=(L_(32).or.L0_alid).and..not.(L_(43))
      L_(22)=.not.L0_alid
C FDB40_logic.fgi( 208, 176):RS �������
C sav1=L_(37)
      L_(37) = L_(24).AND.L_(23).AND.L0_alid
C FDB40_logic.fgi( 217, 186):recalc:�
C if(sav1.ne.L_(37) .and. try367.gt.0) goto 367
      L0_ikid=(L_(28).or.L0_ikid).and..not.(L_(46))
      L_(19)=.not.L0_ikid
C FDB40_logic.fgi( 210, 260):RS �������
C sav1=L_(39)
      L_(39) = L_(21).AND.L_(20).AND.L0_ikid
C FDB40_logic.fgi( 219, 270):recalc:�
C if(sav1.ne.L_(39) .and. try344.gt.0) goto 344
      if(.not.L0_obef) then
         R0_avaf=0.0
      elseif(.not.L0_evaf) then
         R0_avaf=R0_utaf
      else
         R0_avaf=max(R_(40)-deltat,0.0)
      endif
      L_(157)=L0_obef.and.R0_avaf.le.0.0
      L0_evaf=L0_obef
C FDB40_logic.fgi( 208, 122):�������� ��������� ������
C label 406  try406=try406-1
      if(L_(157).and..not.L0_uvaf) then
         R0_ivaf=R0_ovaf
      else
         R0_ivaf=max(R_(41)-deltat,0.0)
      endif
      L_(163)=R0_ivaf.gt.0.0
      L0_uvaf=L_(157)
C FDB40_logic.fgi( 217, 122):������������  �� T
      L0_obef=(L_(165).or.L0_obef).and..not.(L_(163))
      L_(164)=.not.L0_obef
C FDB40_logic.fgi( 195, 127):RS �������
C sav1=L_(157)
      if(.not.L0_obef) then
         R0_avaf=0.0
      elseif(.not.L0_evaf) then
         R0_avaf=R0_utaf
      else
         R0_avaf=max(R_(40)-deltat,0.0)
      endif
      L_(157)=L0_obef.and.R0_avaf.le.0.0
      L0_evaf=L0_obef
C FDB40_logic.fgi( 208, 122):recalc:�������� ��������� ������
C if(sav1.ne.L_(157) .and. try406.gt.0) goto 406
      if(L0_obef.and..not.L0_ibef) then
         R0_abef=R0_ebef
      else
         R0_abef=max(R_(43)-deltat,0.0)
      endif
      L_(162)=R0_abef.gt.0.0
      L0_ibef=L0_obef
C FDB40_logic.fgi( 217, 129):������������  �� T
      if(.not.L0_oxaf) then
         R0_usaf=0.0
      elseif(.not.L0_ataf) then
         R0_usaf=R0_osaf
      else
         R0_usaf=max(R_(38)-deltat,0.0)
      endif
      L_(156)=L0_oxaf.and.R0_usaf.le.0.0
      L0_ataf=L0_oxaf
C FDB40_logic.fgi( 208,  92):�������� ��������� ������
C label 424  try424=try424-1
      if(L_(156).and..not.L0_otaf) then
         R0_etaf=R0_itaf
      else
         R0_etaf=max(R_(39)-deltat,0.0)
      endif
      L_(159)=R0_etaf.gt.0.0
      L0_otaf=L_(156)
C FDB40_logic.fgi( 217,  92):������������  �� T
      L0_oxaf=(L_(161).or.L0_oxaf).and..not.(L_(159))
      L_(160)=.not.L0_oxaf
C FDB40_logic.fgi( 195,  97):RS �������
C sav1=L_(156)
      if(.not.L0_oxaf) then
         R0_usaf=0.0
      elseif(.not.L0_ataf) then
         R0_usaf=R0_osaf
      else
         R0_usaf=max(R_(38)-deltat,0.0)
      endif
      L_(156)=L0_oxaf.and.R0_usaf.le.0.0
      L0_ataf=L0_oxaf
C FDB40_logic.fgi( 208,  92):recalc:�������� ��������� ������
C if(sav1.ne.L_(156) .and. try424.gt.0) goto 424
      if(L0_oxaf.and..not.L0_ixaf) then
         R0_axaf=R0_exaf
      else
         R0_axaf=max(R_(42)-deltat,0.0)
      endif
      L_(158)=R0_axaf.gt.0.0
      L0_ixaf=L0_oxaf
C FDB40_logic.fgi( 215,  99):������������  �� T
      L_uxaf = L_(162).OR.L_(158)
C FDB40_logic.fgi( 310, 126):���
      L_isaf = L_(163).OR.L_(159)
C FDB40_logic.fgi( 310, 111):���
      L_(71) = L_uxud.OR.L_uvud.OR.L_utud.OR.L_atud.OR.L_asud.OR.L_arud.
     &OR.L_umud.OR.L_ulud.OR.L_elud
C FDB40_logic.fgi( 189, 525):���
C label 448  try448=try448-1
      L_(142)=R_edaf.gt.R0_adaf
C FDB40_logic.fgi( 203, 394):���������� >
      L_ubaf = L_(142).AND.L_(141)
C FDB40_logic.fgi( 219, 393):�
      L_(139)=R_edaf.gt.R0_ibaf
C FDB40_logic.fgi( 203, 383):���������� >
      L_ebaf = L_(139).AND.L_(50)
C FDB40_logic.fgi( 215, 382):�
      L_(136)=R_edaf.gt.R0_ixud
C FDB40_logic.fgi( 203, 366):���������� >
      L_exud = L_(136).AND.L_(51)
C FDB40_logic.fgi( 215, 365):�
      L_(133)=R_edaf.gt.R0_ivud
C FDB40_logic.fgi( 203, 349):���������� >
      L_evud = L_(133).AND.L_(52)
C FDB40_logic.fgi( 215, 348):�
      L_(130)=R_edaf.gt.R0_itud
C FDB40_logic.fgi( 203, 332):���������� >
      L_etud = L_(130).AND.L_(129)
C FDB40_logic.fgi( 215, 331):�
      L_(126)=R_edaf.gt.R0_isud
C FDB40_logic.fgi( 203, 315):���������� >
      L_esud = L_(126).AND.L_(125)
C FDB40_logic.fgi( 215, 314):�
      L_(122)=R_edaf.gt.R0_irud
C FDB40_logic.fgi( 266, 394):���������� >
      L_erud = L_(122).AND.L_(121)
C FDB40_logic.fgi( 277, 393):�
      L_(118)=R_edaf.gt.R0_ipud
C FDB40_logic.fgi( 266, 377):���������� >
      L_epud = L_(118).AND.L_(111)
C FDB40_logic.fgi( 277, 376):�
      L_(115)=R_edaf.gt.R0_imud
C FDB40_logic.fgi( 266, 360):���������� >
      L_amud = L_(115).AND.L_(110)
C FDB40_logic.fgi( 277, 359):�
      L_(82) = L_ubaf.OR.L_ebaf.OR.L_exud.OR.L_evud.OR.L_etud.OR.L_esud.
     &OR.L_erud.OR.L_epud.OR.L_amud
C FDB40_logic.fgi( 207, 481):���
      L_(101) = L_(107).OR.L_(82)
C FDB40_logic.fgi( 196, 502):���
      L_evod=R_axod.lt.R0_ivod
C FDB40_logic.fgi( 265, 497):���������� <
      L_(81) = L0_omod.AND.L_evod
C FDB40_logic.fgi( 226, 458):�
      L_(69) = L_ebaf.OR.L_uxud
C FDB40_logic.fgi( 212, 451):���
      L_(98)=R_axod.lt.R0_atod
C FDB40_logic.fgi( 265, 491):���������� <
      L_(97)=R_axod.gt.R0_usod
C FDB40_logic.fgi( 265, 485):���������� >
      L_avod = L_(98).AND.L_(97)
C FDB40_logic.fgi( 270, 490):�
      L_(80) = L0_imod.AND.L_avod
C FDB40_logic.fgi( 226, 450):�
      L_(135)=R_edaf.lt.R0_axud
C FDB40_logic.fgi( 203, 355):���������� <
      L_uvud = L_(51).AND.L_(135)
C FDB40_logic.fgi( 215, 356):�
      L_(68) = L_exud.OR.L_uvud
C FDB40_logic.fgi( 212, 443):���
      L_(96)=R_axod.lt.R0_asod
C FDB40_logic.fgi( 265, 479):���������� <
      L_(95)=R_axod.gt.R0_urod
C FDB40_logic.fgi( 265, 473):���������� >
      L_utod = L_(96).AND.L_(95)
C FDB40_logic.fgi( 270, 478):�
      L_(79) = L0_emod.AND.L_utod
C FDB40_logic.fgi( 226, 442):�
      L_(132)=R_edaf.lt.R0_avud
C FDB40_logic.fgi( 203, 338):���������� <
      L_utud = L_(52).AND.L_(132)
C FDB40_logic.fgi( 215, 339):�
      L_(67) = L_evud.OR.L_utud
C FDB40_logic.fgi( 212, 435):���
      L_(94)=R_axod.lt.R0_orod
C FDB40_logic.fgi( 265, 467):���������� <
      L_(93)=R_axod.gt.R0_irod
C FDB40_logic.fgi( 265, 461):���������� >
      L_otod = L_(94).AND.L_(93)
C FDB40_logic.fgi( 270, 466):�
      L_(78) = L0_amod.AND.L_otod
C FDB40_logic.fgi( 226, 434):�
      L_(128)=R_edaf.lt.R0_usud
C FDB40_logic.fgi( 203, 321):���������� <
      L_atud = L_(129).AND.L_(128)
C FDB40_logic.fgi( 215, 322):�
      L_(66) = L_etud.OR.L_atud
C FDB40_logic.fgi( 212, 427):���
      L_(92)=R_axod.lt.R0_erod
C FDB40_logic.fgi( 310, 524):���������� <
      L_(91)=R_axod.gt.R0_arod
C FDB40_logic.fgi( 310, 518):���������� >
      L_itod = L_(92).AND.L_(91)
C FDB40_logic.fgi( 315, 523):�
      L_(77) = L0_ulod.AND.L_itod
C FDB40_logic.fgi( 226, 426):�
      L_(124)=R_edaf.lt.R0_urud
C FDB40_logic.fgi( 203, 304):���������� <
      L_asud = L_(125).AND.L_(124)
C FDB40_logic.fgi( 215, 305):�
      L_(65) = L_esud.OR.L_asud
C FDB40_logic.fgi( 212, 419):���
      L_(90)=R_axod.lt.R0_upod
C FDB40_logic.fgi( 310, 512):���������� <
      L_(89)=R_axod.gt.R0_opod
C FDB40_logic.fgi( 310, 506):���������� >
      L_etod = L_(90).AND.L_(89)
C FDB40_logic.fgi( 315, 511):�
      L_(76) = L0_olod.AND.L_etod
C FDB40_logic.fgi( 226, 418):�
      L_(120)=R_edaf.lt.R0_upud
C FDB40_logic.fgi( 266, 383):���������� <
      L_arud = L_(121).AND.L_(120)
C FDB40_logic.fgi( 277, 384):�
      L_(59) = L_erud.OR.L_arud
C FDB40_logic.fgi( 295, 442):���
      L_(88)=R_axod.lt.R0_edod
C FDB40_logic.fgi( 310, 500):���������� <
      L_(87)=R_axod.gt.R0_adod
C FDB40_logic.fgi( 310, 494):���������� >
      L_osod = L_(88).AND.L_(87)
C FDB40_logic.fgi( 315, 499):�
      L_(75) = L0_ilod.AND.L_osod
C FDB40_logic.fgi( 310, 441):�
      L_(117)=R_edaf.lt.R0_apud
C FDB40_logic.fgi( 266, 366):���������� <
      L_umud = L_(111).AND.L_(117)
C FDB40_logic.fgi( 277, 367):�
      L_(58) = L_epud.OR.L_umud
C FDB40_logic.fgi( 295, 434):���
      L_(86)=R_axod.lt.R0_ubod
C FDB40_logic.fgi( 310, 488):���������� <
      L_(85)=R_axod.gt.R0_obod
C FDB40_logic.fgi( 310, 482):���������� >
      L_isod = L_(86).AND.L_(85)
C FDB40_logic.fgi( 315, 487):�
      L_(74) = L0_elod.AND.L_isod
C FDB40_logic.fgi( 310, 433):�
      L_(114)=R_edaf.lt.R0_emud
C FDB40_logic.fgi( 266, 349):���������� <
      L_ulud = L_(110).AND.L_(114)
C FDB40_logic.fgi( 277, 350):�
      L_(57) = L_amud.OR.L_ulud
C FDB40_logic.fgi( 295, 426):���
      L_(84)=R_axod.lt.R0_ipod
C FDB40_logic.fgi( 310, 476):���������� <
      L_(83)=R_axod.gt.R0_epod
C FDB40_logic.fgi( 310, 470):���������� >
      L_esod = L_(84).AND.L_(83)
C FDB40_logic.fgi( 315, 475):�
      L_(73) = L0_alod.AND.L_esod
C FDB40_logic.fgi( 310, 425):�
      L_(112)=R_edaf.lt.R0_ilud
C FDB40_logic.fgi( 266, 337):���������� <
      L_elud = L_(109).AND.L_(112)
C FDB40_logic.fgi( 277, 338):�
      L_umod=R_axod.gt.R0_apod
C FDB40_logic.fgi( 310, 463):���������� >
      L_(72) = L0_ekod.AND.L_umod
C FDB40_logic.fgi( 310, 417):�
      L_(99) = L_(81).OR.L_(80).OR.L_(79).OR.L_(78).OR.L_
     &(77).OR.L_(76).OR.L_(75).OR.L_(74).OR.L_(73).OR.L_(72
     &)
C FDB40_logic.fgi( 252, 448):���
      L_(105) = L_(100).OR.L_(101).OR.L_(99)
C FDB40_logic.fgi( 208, 509):���
      L_(104) = L_(71).OR.L_(108)
C FDB40_logic.fgi( 196, 513):���
      L0_ixod=(L_(104).or.L0_ixod).and..not.(L_(105))
      L_(106)=.not.L0_ixod
C FDB40_logic.fgi( 215, 511):RS �������
      if(L0_ixod) then
         R_(29)=R_(27)
      else
         R_(29)=R_(30)
      endif
C FDB40_logic.fgi( 230, 525):���� RE IN LO CH7
      L_(102) = L_(104).OR.L_(100).OR.L_(99)
C FDB40_logic.fgi( 213, 498):���
      L0_exod=(L_(101).or.L0_exod).and..not.(L_(102))
      L_(103)=.not.L0_exod
C FDB40_logic.fgi( 224, 500):RS �������
      if(L0_exod) then
         R_(28)=R_(27)
      else
         R_(28)=R_(30)
      endif
C FDB40_logic.fgi( 230, 507):���� RE IN LO CH7
      R_oxod = R_(29) + (-R_(28))
C FDB40_logic.fgi( 236, 518):��������
      R_axod=R_axod+deltat/R0_okod*R_oxod
      if(R_axod.gt.R0_ikod) then
         R_axod=R0_ikod
      elseif(R_axod.lt.R0_ukod) then
         R_axod=R0_ukod
      endif
C FDB40_logic.fgi( 251, 502):����������
      R_edaf=R_axod
C FDB40_logic.fgi( 286, 504):������,20FDB42AE501KE02VX01
      L_(138)=R_edaf.lt.R0_abaf
C FDB40_logic.fgi( 203, 372):���������� <
      L_uxud = L_(50).AND.L_(138)
C FDB40_logic.fgi( 215, 373):�
C sav1=L_(69)
      L_(69) = L_ebaf.OR.L_uxud
C FDB40_logic.fgi( 212, 451):recalc:���
C if(sav1.ne.L_(69) .and. try548.gt.0) goto 548
C sav1=L_(71)
      L_(71) = L_uxud.OR.L_uvud.OR.L_utud.OR.L_atud.OR.L_asud.OR.L_arud.
     &OR.L_umud.OR.L_ulud.OR.L_elud
C FDB40_logic.fgi( 189, 525):recalc:���
C if(sav1.ne.L_(71) .and. try448.gt.0) goto 448
      L0_elod=(L_(58).or.L0_elod).and..not.(L_(99))
      L_(55)=.not.L0_elod
C FDB40_logic.fgi( 301, 432):RS �������
C sav1=L_(74)
      L_(74) = L0_elod.AND.L_isod
C FDB40_logic.fgi( 310, 433):recalc:�
C if(sav1.ne.L_(74) .and. try703.gt.0) goto 703
      L0_alod=(L_(57).or.L0_alod).and..not.(L_(99))
      L_(54)=.not.L0_alod
C FDB40_logic.fgi( 301, 424):RS �������
C sav1=L_(73)
      L_(73) = L0_alod.AND.L_esod
C FDB40_logic.fgi( 310, 425):recalc:�
C if(sav1.ne.L_(73) .and. try726.gt.0) goto 726
      L0_ekod=(L_elud.or.L0_ekod).and..not.(L_(99))
      L_(53)=.not.L0_ekod
C FDB40_logic.fgi( 304, 416):RS �������
C sav1=L_(72)
      L_(72) = L0_ekod.AND.L_umod
C FDB40_logic.fgi( 310, 417):recalc:�
C if(sav1.ne.L_(72) .and. try745.gt.0) goto 745
      L0_ilod=(L_(59).or.L0_ilod).and..not.(L_(99))
      L_(56)=.not.L0_ilod
C FDB40_logic.fgi( 301, 440):RS �������
C sav1=L_(75)
      L_(75) = L0_ilod.AND.L_osod
C FDB40_logic.fgi( 310, 441):recalc:�
C if(sav1.ne.L_(75) .and. try680.gt.0) goto 680
      L0_imod=(L_(69).or.L0_imod).and..not.(L_(99))
      L_(64)=.not.L0_imod
C FDB40_logic.fgi( 219, 449):RS �������
C sav1=L_(80)
      L_(80) = L0_imod.AND.L_avod
C FDB40_logic.fgi( 226, 450):recalc:�
C if(sav1.ne.L_(80) .and. try561.gt.0) goto 561
      L0_emod=(L_(68).or.L0_emod).and..not.(L_(99))
      L_(63)=.not.L0_emod
C FDB40_logic.fgi( 219, 441):RS �������
C sav1=L_(79)
      L_(79) = L0_emod.AND.L_utod
C FDB40_logic.fgi( 226, 442):recalc:�
C if(sav1.ne.L_(79) .and. try585.gt.0) goto 585
      L0_amod=(L_(67).or.L0_amod).and..not.(L_(99))
      L_(62)=.not.L0_amod
C FDB40_logic.fgi( 219, 433):RS �������
C sav1=L_(78)
      L_(78) = L0_amod.AND.L_otod
C FDB40_logic.fgi( 226, 434):recalc:�
C if(sav1.ne.L_(78) .and. try609.gt.0) goto 609
      L0_ulod=(L_(66).or.L0_ulod).and..not.(L_(99))
      L_(61)=.not.L0_ulod
C FDB40_logic.fgi( 219, 425):RS �������
C sav1=L_(77)
      L_(77) = L0_ulod.AND.L_itod
C FDB40_logic.fgi( 226, 426):recalc:�
C if(sav1.ne.L_(77) .and. try633.gt.0) goto 633
      L0_olod=(L_(65).or.L0_olod).and..not.(L_(99))
      L_(60)=.not.L0_olod
C FDB40_logic.fgi( 219, 417):RS �������
C sav1=L_(76)
      L_(76) = L0_olod.AND.L_etod
C FDB40_logic.fgi( 226, 418):recalc:�
C if(sav1.ne.L_(76) .and. try657.gt.0) goto 657
      L0_omod=(L_ubaf.or.L0_omod).and..not.(L_(99))
      L_(70)=.not.L0_omod
C FDB40_logic.fgi( 219, 457):RS �������
C sav1=L_(81)
      L_(81) = L0_omod.AND.L_evod
C FDB40_logic.fgi( 226, 458):recalc:�
C if(sav1.ne.L_(81) .and. try546.gt.0) goto 546
      if(L_evod) then
         C30_abod=C30_ebod
      else
         C30_abod=C30_ibod
      endif
C FDB40_logic.fgi( 380, 452):���� RE IN LO CH20
      if(L_avod) then
         C30_usid=C30_isid
      else
         C30_usid=C30_abod
      endif
C FDB40_logic.fgi( 396, 451):���� RE IN LO CH20
      if(L_utod) then
         C30_etid=C30_osid
      else
         C30_etid=C30_usid
      endif
C FDB40_logic.fgi( 412, 450):���� RE IN LO CH20
      if(L_otod) then
         C30_otid=C30_atid
      else
         C30_otid=C30_etid
      endif
C FDB40_logic.fgi( 429, 449):���� RE IN LO CH20
      if(L_itod) then
         C30_avid=C30_itid
      else
         C30_avid=C30_otid
      endif
C FDB40_logic.fgi( 446, 448):���� RE IN LO CH20
      if(L_etod) then
         C30_ivid=C30_utid
      else
         C30_ivid=C30_avid
      endif
C FDB40_logic.fgi( 463, 447):���� RE IN LO CH20
      if(L_osod) then
         C30_uvid=C30_evid
      else
         C30_uvid=C30_ivid
      endif
C FDB40_logic.fgi( 479, 446):���� RE IN LO CH20
      if(L_isod) then
         C30_exid=C30_ovid
      else
         C30_exid=C30_uvid
      endif
C FDB40_logic.fgi( 496, 445):���� RE IN LO CH20
      if(L_esod) then
         C30_oxid=C30_axid
      else
         C30_oxid=C30_exid
      endif
C FDB40_logic.fgi( 513, 444):���� RE IN LO CH20
      if(L_umod) then
         C30_uxid=C30_ixid
      else
         C30_uxid=C30_oxid
      endif
C FDB40_logic.fgi( 530, 443):���� RE IN LO CH20
      End
