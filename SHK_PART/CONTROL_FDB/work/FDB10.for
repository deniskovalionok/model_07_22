      Subroutine FDB10(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDB10.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      Call FDB10_ConIn
      R_(61)=R8_ofe
C FDB10_vent_log.fgi(  60, 283):pre: �������������� �����  
      R_(57)=R8_ide
C FDB10_vent_log.fgi( 136, 283):pre: �������������� �����  
      R_(12)=R8_ap
C FDB10_vent_log.fgi(  64, 224):pre: �������������� �����  
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_itid,4),R8_erid
     &,I_uxid,I_ibod,I_oxid,
     & C8_urid,I_ebod,R_usid,R_osid,R_emid,
     & REAL(R_omid,4),R_ipid,REAL(R_upid,4),
     & R_umid,REAL(R_epid,4),I_exid,I_obod,I_abod,I_axid,L_arid
     &,
     & L_adod,L_afod,L_opid,L_apid,
     & L_orid,L_irid,L_idod,L_imid,L_esid,L_ofod,L_atid,
     & L_etid,REAL(R8_amid,8),REAL(1.0,4),R8_isid,L_(21),L_elid
     &,L_(22),
     & L_ilid,L_edod,I_ubod,L_efod,R_uvid,REAL(R_asid,4),L_ifod
     &,L_olid,
     & L_ufod,L_ulid,L_otid,L_utid,L_avid,L_ivid,L_ovid,L_evid
     &)
      !}

      if(L_ovid.or.L_ivid.or.L_evid.or.L_avid.or.L_utid.or.L_otid
     &) then      
                  I_ixid = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ixid = z'40000000'
      endif
C FDB10_vlv.fgi( 122,  78):���� ���������� �������� � ���������������� ��������,20FDB13BR003KA12
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ese,4),R8_epe
     &,I_ove,I_exe,I_ive,
     & C8_upe,I_axe,R_ore,R_ire,R_ele,
     & REAL(R_ole,4),R_ime,REAL(R_ume,4),
     & R_ule,REAL(R_eme,4),I_ave,I_ixe,I_uve,I_ute,L_ape,
     & L_uxe,L_ubi,L_ome,L_ame,
     & L_ope,L_ipe,L_ebi,L_ile,L_ere,L_idi,L_ure,
     & L_ase,REAL(R8_amid,8),REAL(1.0,4),R8_isid,L_(7),L_ike
     &,L_(8),
     & L_oke,L_abi,I_oxe,L_adi,R_ote,REAL(R_are,4),L_edi,L_uke
     &,
     & L_odi,L_ale,L_ise,L_ose,L_use,L_ete,L_ite,L_ate)
      !}

      if(L_ite.or.L_ete.or.L_ate.or.L_use.or.L_ose.or.L_ise
     &) then      
                  I_eve = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_eve = z'40000000'
      endif
C FDB10_vlv.fgi( 178,  54):���� ���������� �������� � ���������������� ��������,20FDB11BR110KA03
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_iku,4),R8_idu
     &,I_umu,I_ipu,I_omu,
     & C8_afu,I_epu,R_ufu,R_ofu,R_ixo,
     & REAL(R_uxo,4),R_obu,REAL(R_adu,4),
     & R_abu,REAL(R_ibu,4),I_emu,I_opu,I_apu,I_amu,L_edu,
     & L_aru,L_asu,L_ubu,L_ebu,
     & L_udu,L_odu,L_iru,L_oxo,L_ifu,L_osu,L_aku,
     & L_eku,REAL(R8_amid,8),REAL(1.0,4),R8_isid,L_(13),L_ovo
     &,L_(14),
     & L_uvo,L_eru,I_upu,L_esu,R_ulu,REAL(R_efu,4),L_isu,L_axo
     &,
     & L_usu,L_exo,L_oku,L_uku,L_alu,L_ilu,L_olu,L_elu)
      !}

      if(L_olu.or.L_ilu.or.L_elu.or.L_alu.or.L_uku.or.L_oku
     &) then      
                  I_imu = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_imu = z'40000000'
      endif
C FDB10_vlv.fgi( 122,  54):���� ���������� �������� � ���������������� ��������,20FDB11BR107KA03
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_oved,4),R8_osed
     &,I_adid,I_odid,I_ubid,
     & C8_eted,I_idid,R_aved,R_uted,R_oped,
     & REAL(R_ared,4),R_ured,REAL(R_esed,4),
     & R_ered,REAL(R_ored,4),I_ibid,I_udid,I_edid,I_ebid,L_ised
     &,
     & L_efid,L_ekid,L_ased,L_ired,
     & L_ated,L_used,L_ofid,L_uped,L_oted,L_ukid,L_eved,
     & L_ived,REAL(R8_amid,8),REAL(1.0,4),R8_isid,L_(19),L_umed
     &,L_(20),
     & L_aped,L_ifid,I_afid,L_ikid,R_abid,REAL(R_ited,4),L_okid
     &,L_eped,
     & L_alid,L_iped,L_uved,L_axed,L_exed,L_oxed,L_uxed,L_ixed
     &)
      !}

      if(L_uxed.or.L_oxed.or.L_ixed.or.L_exed.or.L_axed.or.L_uved
     &) then      
                  I_obid = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_obid = z'40000000'
      endif
C FDB10_vlv.fgi( 140,  78):���� ���������� �������� � ���������������� ��������,20FDB13BR004KA12
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_amo,4),R8_ako
     &,I_iro,I_aso,I_ero,
     & C8_oko,I_uro,R_ilo,R_elo,R_ado,
     & REAL(R_ido,4),R_efo,REAL(R_ofo,4),
     & R_odo,REAL(R_afo,4),I_upo,I_eso,I_oro,I_opo,L_ufo,
     & L_oso,L_oto,L_ifo,L_udo,
     & L_iko,L_eko,L_ato,L_edo,L_alo,L_evo,L_olo,
     & L_ulo,REAL(R8_amid,8),REAL(1.0,4),R8_isid,L_(11),L_ebo
     &,L_(12),
     & L_ibo,L_uso,I_iso,L_uto,R_ipo,REAL(R_uko,4),L_avo,L_obo
     &,
     & L_ivo,L_ubo,L_emo,L_imo,L_omo,L_apo,L_epo,L_umo)
      !}

      if(L_epo.or.L_apo.or.L_umo.or.L_omo.or.L_imo.or.L_emo
     &) then      
                  I_aro = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_aro = z'40000000'
      endif
C FDB10_vlv.fgi( 140,  54):���� ���������� �������� � ���������������� ��������,20FDB11BR108KA03
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_ebed,4),R8_evad
     &,I_ofed,I_eked,I_ifed,
     & C8_uvad,I_aked,R_oxad,R_ixad,R_esad,
     & REAL(R_osad,4),R_itad,REAL(R_utad,4),
     & R_usad,REAL(R_etad,4),I_afed,I_iked,I_ufed,I_uded,L_avad
     &,
     & L_uked,L_uled,L_otad,L_atad,
     & L_ovad,L_ivad,L_eled,L_isad,L_exad,L_imed,L_uxad,
     & L_abed,REAL(R8_amid,8),REAL(1.0,4),R8_isid,L_(17),L_irad
     &,L_(18),
     & L_orad,L_aled,I_oked,L_amed,R_oded,REAL(R_axad,4),L_emed
     &,L_urad,
     & L_omed,L_asad,L_ibed,L_obed,L_ubed,L_eded,L_ided,L_aded
     &)
      !}

      if(L_ided.or.L_eded.or.L_aded.or.L_ubed.or.L_obed.or.L_ibed
     &) then      
                  I_efed = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_efed = z'40000000'
      endif
C FDB10_vlv.fgi( 160,  78):���� ���������� �������� � ���������������� ��������,20FDB13BR005KA12
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_opi,4),R8_oli
     &,I_ati,I_oti,I_usi,
     & C8_emi,I_iti,R_api,R_umi,R_ofi,
     & REAL(R_aki,4),R_uki,REAL(R_eli,4),
     & R_eki,REAL(R_oki,4),I_isi,I_uti,I_eti,I_esi,L_ili,
     & L_evi,L_exi,L_ali,L_iki,
     & L_ami,L_uli,L_ovi,L_ufi,L_omi,L_uxi,L_epi,
     & L_ipi,REAL(R8_amid,8),REAL(1.0,4),R8_isid,L_(9),L_udi
     &,L_(10),
     & L_afi,L_ivi,I_avi,L_ixi,R_asi,REAL(R_imi,4),L_oxi,L_efi
     &,
     & L_abo,L_ifi,L_upi,L_ari,L_eri,L_ori,L_uri,L_iri)
      !}

      if(L_uri.or.L_ori.or.L_iri.or.L_eri.or.L_ari.or.L_upi
     &) then      
                  I_osi = ior(z'000000FF',z'04000000')   
     &                         
                else
                   I_osi = z'40000000'
      endif
C FDB10_vlv.fgi( 160,  54):���� ���������� �������� � ���������������� ��������,20FDB11BR109KA03
      !{
      Call KLAPAN_EPRIVOD_MAN(deltat,REAL(R_udad,4),R8_uxu
     &,I_elad,I_ulad,I_alad,
     & C8_ibad,I_olad,R_edad,R_adad,R_utu,
     & REAL(R_evu,4),R_axu,REAL(R_ixu,4),
     & R_ivu,REAL(R_uvu,4),I_okad,I_amad,I_ilad,I_ikad,L_oxu
     &,
     & L_imad,L_ipad,L_exu,L_ovu,
     & L_ebad,L_abad,L_umad,L_avu,L_ubad,L_arad,L_idad,
     & L_odad,REAL(R8_amid,8),REAL(1.0,4),R8_isid,L_(15),L_atu
     &,L_(16),
     & L_etu,L_omad,I_emad,L_opad,R_ekad,REAL(R_obad,4),L_upad
     &,L_itu,
     & L_erad,L_otu,L_afad,L_efad,L_ifad,L_ufad,L_akad,L_ofad
     &)
      !}

      if(L_akad.or.L_ufad.or.L_ofad.or.L_ifad.or.L_efad.or.L_afad
     &) then      
                  I_ukad = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ukad = z'40000000'
      endif
C FDB10_vlv.fgi( 178,  78):���� ���������� �������� � ���������������� ��������,20FDB13BR006KA12
      R_e = R8_i
C FDB10_vent_log.fgi( 121,  33):��������
      R_o = R8_u
C FDB10_vent_log.fgi( 121,  38):��������
      R_ad = R8_ed
C FDB10_vent_log.fgi(  49,  33):��������
      R_id = R8_od
C FDB10_vent_log.fgi(  49,  38):��������
      R_(1) = 60000
C FDB10_vent_log.fgi( 128,  71):��������� (RE4) (�������)
      if(R8_ud.ge.0.0) then
         R_(2)=R8_ef/max(R8_ud,1.0e-10)
      else
         R_(2)=R8_ef/min(R8_ud,-1.0e-10)
      endif
C FDB10_vent_log.fgi( 118,  74):�������� ����������
      R8_af = R_(2) * R_(1)
C FDB10_vent_log.fgi( 131,  73):����������
      R_(3) = 60000
C FDB10_vent_log.fgi( 128,  87):��������� (RE4) (�������)
      if(R8_if.ge.0.0) then
         R_(4)=R8_uf/max(R8_if,1.0e-10)
      else
         R_(4)=R8_uf/min(R8_if,-1.0e-10)
      endif
C FDB10_vent_log.fgi( 118,  90):�������� ����������
      R8_of = R_(4) * R_(3)
C FDB10_vent_log.fgi( 131,  89):����������
      R_(5) = 60000
C FDB10_vent_log.fgi( 128, 102):��������� (RE4) (�������)
      if(R8_ak.ge.0.0) then
         R_(6)=R8_ek/max(R8_ak,1.0e-10)
      else
         R_(6)=R8_ek/min(R8_ak,-1.0e-10)
      endif
C FDB10_vent_log.fgi( 118, 105):�������� ����������
      R_ubif = R_(6) * R_(5)
C FDB10_vent_log.fgi( 131, 104):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_oxef,R_ifif,REAL(1,4)
     &,
     & REAL(R_ebif,4),REAL(R_ibif,4),
     & REAL(R_ixef,4),REAL(R_exef,4),I_efif,
     & REAL(R_adif,4),L_edif,REAL(R_idif,4),L_odif,L_udif
     &,R_obif,
     & REAL(R_abif,4),REAL(R_uxef,4),L_afif,REAL(R_ubif,4
     &))
      !}
C FDB10_vlv.fgi(  86, 110):���������� �������,20FDB13CF104XQ01
      R_(7) = 60000
C FDB10_vent_log.fgi( 128, 114):��������� (RE4) (�������)
      if(R8_ik.ge.0.0) then
         R_(8)=R8_ok/max(R8_ik,1.0e-10)
      else
         R_(8)=R8_ok/min(R8_ik,-1.0e-10)
      endif
C FDB10_vent_log.fgi( 118, 117):�������� ����������
      R_adaf = R_(8) * R_(7)
C FDB10_vent_log.fgi( 131, 116):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_uxud,R_ofaf,REAL(1,4)
     &,
     & REAL(R_ibaf,4),REAL(R_obaf,4),
     & REAL(R_oxud,4),REAL(R_ixud,4),I_ifaf,
     & REAL(R_edaf,4),L_idaf,REAL(R_odaf,4),L_udaf,L_afaf
     &,R_ubaf,
     & REAL(R_ebaf,4),REAL(R_abaf,4),L_efaf,REAL(R_adaf,4
     &))
      !}
C FDB10_vlv.fgi(  56,  79):���������� �������,20FDB13CF103XQ01
      !��������� R_(9) = FDB10_vent_logC?? /0.0005/
      R_(9)=R0_al
C FDB10_vent_log.fgi( 128, 221):���������
      L_(1)=R8_el.gt.R_(9)
C FDB10_vent_log.fgi( 132, 222):���������� >
      !��������� R_(11) = FDB10_vent_logC?? /0.0/
      R_(11)=R0_il
C FDB10_vent_log.fgi( 140, 228):���������
      !��������� R_(10) = FDB10_vent_logC?? /500/
      R_(10)=R0_ol
C FDB10_vent_log.fgi( 140, 226):���������
      if(L_(1)) then
         R_(19)=R_(10)
      else
         R_(19)=R_(11)
      endif
C FDB10_vent_log.fgi( 143, 226):���� RE IN LO CH7
      R_(15) = 1.0
C FDB10_vent_log.fgi(  51, 222):��������� (RE4) (�������)
      R_(16) = 0.0
C FDB10_vent_log.fgi(  51, 224):��������� (RE4) (�������)
      !��������� R_(13) = FDB10_vent_logC?? /2.6/
      R_(13)=R0_am
C FDB10_vent_log.fgi(  47, 245):���������
      !��������� R_(17) = FDB10_vent_logC?? /1/
      R_(17)=R0_ep
C FDB10_vent_log.fgi( 142, 244):���������
      L_(5)=R8_is.lt.R_(17)
C FDB10_vent_log.fgi( 146, 245):���������� <
      L_op=L_ip.or.(L_op.and..not.(L_(5)))
      L_(6)=.not.L_op
C FDB10_vent_log.fgi( 176, 247):RS �������
      L_ar=L_op
C FDB10_vent_log.fgi( 191, 249):������,20FDB13CW001_OUT
      !��������� R_(18) = FDB10_vent_logC?? /-50000/
      R_(18)=R0_up
C FDB10_vent_log.fgi( 171, 225):���������
      if(L_ar) then
         R8_er=R_(18)
      else
         R8_er=R_(19)
      endif
C FDB10_vent_log.fgi( 174, 225):���� RE IN LO CH7
      if(R8_is.le.R0_as) then
         R8_es=R0_ur
      elseif(R8_is.gt.R0_or) then
         R8_es=R0_ir
      else
         R8_es=R0_ur+(R8_is-(R0_as))*(R0_ir-(R0_ur))/(R0_or
     &-(R0_as))
      endif
C FDB10_vent_log.fgi( 127,  55):��������������� ���������
      L_(2)=R8_es.gt.R_(13)
C FDB10_vent_log.fgi(  51, 246):���������� >
      L_(3) = L_(2).OR.L_uk
C FDB10_vent_log.fgi(  65, 245):���
      L_im=L_em.or.(L_im.and..not.(L_(3)))
      L_(4)=.not.L_im
C FDB10_vent_log.fgi(  81, 248):RS �������
      L_om=L_im
C FDB10_vent_log.fgi(  95, 250):������,FDB13dust_start
      if(L_om) then
         R_(14)=R_(15)
      else
         R_(14)=R_(16)
      endif
C FDB10_vent_log.fgi(  54, 223):���� RE IN LO CH7
      R8_ap=(R0_ul*R_(12)+deltat*R_(14))/(R0_ul+deltat)
C FDB10_vent_log.fgi(  64, 224):�������������� �����  
      R8_um=R8_ap
C FDB10_vent_log.fgi(  82, 224):������,20FDB13AN002KA11
      R_(20) = 60
C FDB10_vent_log.fgi(  53, 111):��������� (RE4) (�������)
      R_(21) = 1000
C FDB10_vent_log.fgi(  46, 114):��������� (RE4) (�������)
      R_(22) = R8_os * R_(21)
C FDB10_vent_log.fgi(  48, 116):����������
      if(R_(20).ge.0.0) then
         R_asod=R_(22)/max(R_(20),1.0e-10)
      else
         R_asod=R_(22)/min(R_(20),-1.0e-10)
      endif
C FDB10_vent_log.fgi(  57, 114):�������� ����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_upod,R_otod,REAL(1,4)
     &,
     & REAL(R_irod,4),REAL(R_orod,4),
     & REAL(R_opod,4),REAL(R_ipod,4),I_itod,
     & REAL(R_esod,4),L_isod,REAL(R_osod,4),L_usod,L_atod
     &,R_urod,
     & REAL(R_erod,4),REAL(R_arod,4),L_etod,REAL(R_asod,4
     &))
      !}
C FDB10_vlv.fgi(  56,  47):���������� �������,20FDB11CF001XQ01
      R8_us = R8_at
C FDB10_vent_log.fgi( 128, 127):��������
      R_et = R8_it
C FDB10_vent_log.fgi(  55, 127):��������
      R_(23) = 1e-3
C FDB10_vent_log.fgi( 128, 174):��������� (RE4) (�������)
      R_(24) = R8_ut * R_(23)
C FDB10_vent_log.fgi( 131, 176):����������
      R_ot = R_(24)
C FDB10_vent_log.fgi( 135, 176):��������
      R_(25) = 1e-3
C FDB10_vent_log.fgi(  54, 174):��������� (RE4) (�������)
      R_(26) = R8_ev * R_(25)
C FDB10_vent_log.fgi(  57, 176):����������
      R_av = R_(26)
C FDB10_vent_log.fgi(  61, 176):��������
      R_(28) = R8_iv + (-R8_ube)
C FDB10_vent_log.fgi( 120, 142):��������
      R_(27) = 1e-3
C FDB10_vent_log.fgi( 128, 139):��������� (RE4) (�������)
      R_(29) = R_(28) * R_(27)
C FDB10_vent_log.fgi( 131, 141):����������
      R_ilaf = R_(29)
C FDB10_vent_log.fgi( 135, 141):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ekaf,R_apaf,REAL(1,4)
     &,
     & REAL(R_ukaf,4),REAL(R_alaf,4),
     & REAL(R_akaf,4),REAL(R_ufaf,4),I_umaf,
     & REAL(R_olaf,4),L_ulaf,REAL(R_amaf,4),L_emaf,L_imaf
     &,R_elaf,
     & REAL(R_okaf,4),REAL(R_ikaf,4),L_omaf,REAL(R_ilaf,4
     &))
      !}
C FDB10_vlv.fgi(  86, 154):���������� �������,20FDB13CP105XQ01
      R_(31) = R8_ov + (-R8_ube)
C FDB10_vent_log.fgi( 120, 152):��������
      R_(30) = 1e-3
C FDB10_vent_log.fgi( 128, 149):��������� (RE4) (�������)
      R_(32) = R_(31) * R_(30)
C FDB10_vent_log.fgi( 131, 151):����������
      R_uraf = R_(32)
C FDB10_vent_log.fgi( 135, 151):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_opaf,R_itaf,REAL(1,4)
     &,
     & REAL(R_eraf,4),REAL(R_iraf,4),
     & REAL(R_ipaf,4),REAL(R_epaf,4),I_etaf,
     & REAL(R_asaf,4),L_esaf,REAL(R_isaf,4),L_osaf,L_usaf
     &,R_oraf,
     & REAL(R_araf,4),REAL(R_upaf,4),L_ataf,REAL(R_uraf,4
     &))
      !}
C FDB10_vlv.fgi(  86, 125):���������� �������,20FDB13CP104XQ01
      R_(34) = R8_uv + (-R8_ube)
C FDB10_vent_log.fgi( 120, 160):��������
      R_(33) = 1e-3
C FDB10_vent_log.fgi( 128, 157):��������� (RE4) (�������)
      R_(35) = R_(34) * R_(33)
C FDB10_vent_log.fgi( 131, 159):����������
      R_exaf = R_(35)
C FDB10_vent_log.fgi( 135, 159):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_avaf,R_ubef,REAL(1,4)
     &,
     & REAL(R_ovaf,4),REAL(R_uvaf,4),
     & REAL(R_utaf,4),REAL(R_otaf,4),I_obef,
     & REAL(R_ixaf,4),L_oxaf,REAL(R_uxaf,4),L_abef,L_ebef
     &,R_axaf,
     & REAL(R_ivaf,4),REAL(R_evaf,4),L_ibef,REAL(R_exaf,4
     &))
      !}
C FDB10_vlv.fgi(  86, 140):���������� �������,20FDB13CP103XQ01
      R_(37) = R8_ax + (-R8_ube)
C FDB10_vent_log.fgi( 120, 169):��������
      R_(36) = 1e-3
C FDB10_vent_log.fgi( 128, 166):��������� (RE4) (�������)
      R_(38) = R_(37) * R_(36)
C FDB10_vent_log.fgi( 131, 168):����������
      R_ofef = R_(38)
C FDB10_vent_log.fgi( 135, 168):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_idef,R_elef,REAL(1,4)
     &,
     & REAL(R_afef,4),REAL(R_efef,4),
     & REAL(R_edef,4),REAL(R_adef,4),I_alef,
     & REAL(R_ufef,4),L_akef,REAL(R_ekef,4),L_ikef,L_okef
     &,R_ifef,
     & REAL(R_udef,4),REAL(R_odef,4),L_ukef,REAL(R_ofef,4
     &))
      !}
C FDB10_vlv.fgi(  86, 168):���������� �������,20FDB13CP102XQ01
      R_(40) = R8_ex + (-R8_ube)
C FDB10_vent_log.fgi(  46, 142):��������
      R_(39) = 1e-3
C FDB10_vent_log.fgi(  54, 139):��������� (RE4) (�������)
      R_(41) = R_(40) * R_(39)
C FDB10_vent_log.fgi(  57, 141):����������
      R_ixod = R_(41)
C FDB10_vent_log.fgi(  61, 141):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_evod,R_adud,REAL(1,4)
     &,
     & REAL(R_uvod,4),REAL(R_axod,4),
     & REAL(R_avod,4),REAL(R_utod,4),I_ubud,
     & REAL(R_oxod,4),L_uxod,REAL(R_abud,4),L_ebud,L_ibud
     &,R_exod,
     & REAL(R_ovod,4),REAL(R_ivod,4),L_obud,REAL(R_ixod,4
     &))
      !}
C FDB10_vlv.fgi(  86,  47):���������� �������,20FDB11CP110XQ01
      R_(43) = R8_ix + (-R8_ube)
C FDB10_vent_log.fgi(  46, 152):��������
      R_(42) = 1e-3
C FDB10_vent_log.fgi(  54, 149):��������� (RE4) (�������)
      R_(44) = R_(43) * R_(42)
C FDB10_vent_log.fgi(  57, 151):����������
      R_ufud = R_(44)
C FDB10_vent_log.fgi(  61, 151):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_odud,R_ilud,REAL(1,4)
     &,
     & REAL(R_efud,4),REAL(R_ifud,4),
     & REAL(R_idud,4),REAL(R_edud,4),I_elud,
     & REAL(R_akud,4),L_ekud,REAL(R_ikud,4),L_okud,L_ukud
     &,R_ofud,
     & REAL(R_afud,4),REAL(R_udud,4),L_alud,REAL(R_ufud,4
     &))
      !}
C FDB10_vlv.fgi(  86,  63):���������� �������,20FDB11CP109XQ01
      R_(46) = R8_ox + (-R8_ube)
C FDB10_vent_log.fgi(  46, 160):��������
      R_(45) = 1e-3
C FDB10_vent_log.fgi(  54, 157):��������� (RE4) (�������)
      R_(47) = R_(46) * R_(45)
C FDB10_vent_log.fgi(  57, 159):����������
      R_epud = R_(47)
C FDB10_vent_log.fgi(  61, 159):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_amud,R_urud,REAL(1,4)
     &,
     & REAL(R_omud,4),REAL(R_umud,4),
     & REAL(R_ulud,4),REAL(R_olud,4),I_orud,
     & REAL(R_ipud,4),L_opud,REAL(R_upud,4),L_arud,L_erud
     &,R_apud,
     & REAL(R_imud,4),REAL(R_emud,4),L_irud,REAL(R_epud,4
     &))
      !}
C FDB10_vlv.fgi(  86,  79):���������� �������,20FDB11CP108XQ01
      R_(49) = R8_ux + (-R8_ube)
C FDB10_vent_log.fgi(  46, 169):��������
      R_(48) = 1e-3
C FDB10_vent_log.fgi(  54, 166):��������� (RE4) (�������)
      R_(50) = R_(49) * R_(48)
C FDB10_vent_log.fgi(  57, 168):����������
      R_otud = R_(50)
C FDB10_vent_log.fgi(  61, 168):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_isud,R_exud,REAL(1,4)
     &,
     & REAL(R_atud,4),REAL(R_etud,4),
     & REAL(R_esud,4),REAL(R_asud,4),I_axud,
     & REAL(R_utud,4),L_avud,REAL(R_evud,4),L_ivud,L_ovud
     &,R_itud,
     & REAL(R_usud,4),REAL(R_osud,4),L_uvud,REAL(R_otud,4
     &))
      !}
C FDB10_vlv.fgi(  86,  95):���������� �������,20FDB11CP107XQ01
      R_(52) = R8_ube + (-R8_abe)
C FDB10_vent_log.fgi( 120, 193):��������
      R_(51) = 1e-3
C FDB10_vent_log.fgi( 128, 190):��������� (RE4) (�������)
      R_(53) = R_(52) * R_(51)
C FDB10_vent_log.fgi( 131, 192):����������
      R_ebe = R_(53)
C FDB10_vent_log.fgi( 135, 192):��������
      R_(55) = R8_ube + (-R8_ibe)
C FDB10_vent_log.fgi(  46, 193):��������
      R_(54) = 1e-3
C FDB10_vent_log.fgi(  54, 190):��������� (RE4) (�������)
      R_(56) = R_(55) * R_(54)
C FDB10_vent_log.fgi(  57, 192):����������
      R_obe = R_(56)
C FDB10_vent_log.fgi(  61, 192):��������
      !��������� R_(60) = FDB10_vent_logC?? /0.0/
      R_(60)=R0_ode
C FDB10_vent_log.fgi( 123, 284):���������
      !��������� R_(59) = FDB10_vent_logC?? /0.001/
      R_(59)=R0_ude
C FDB10_vent_log.fgi( 123, 282):���������
      if(L_afe) then
         R_(58)=R_(59)
      else
         R_(58)=R_(60)
      endif
C FDB10_vent_log.fgi( 126, 282):���� RE IN LO CH7
      R8_ide=(R0_ade*R_(57)+deltat*R_(58))/(R0_ade+deltat
     &)
C FDB10_vent_log.fgi( 136, 283):�������������� �����  
      R8_ede=R8_ide
C FDB10_vent_log.fgi( 155, 283):������,F_FDB13CU001_G
      !��������� R_(64) = FDB10_vent_logC?? /0.0/
      R_(64)=R0_ufe
C FDB10_vent_log.fgi(  45, 284):���������
      !��������� R_(63) = FDB10_vent_logC?? /0.001/
      R_(63)=R0_ake
C FDB10_vent_log.fgi(  45, 282):���������
      if(L_eke) then
         R_(62)=R_(63)
      else
         R_(62)=R_(64)
      endif
C FDB10_vent_log.fgi(  48, 282):���� RE IN LO CH7
      R8_ofe=(R0_efe*R_(61)+deltat*R_(62))/(R0_efe+deltat
     &)
C FDB10_vent_log.fgi(  60, 283):�������������� �����  
      R8_ife=R8_ofe
C FDB10_vent_log.fgi(  77, 283):������,F_FDB11AX001_G
      !{
      Call DAT_ANA_HANDLER(deltat,R_ikod,R_epod,REAL(1,4)
     &,
     & REAL(R_alod,4),REAL(R_elod,4),
     & REAL(R_ekod,4),REAL(R_akod,4),I_apod,
     & REAL(R_ulod,4),L_amod,REAL(R_emod,4),L_imod,L_omod
     &,R_ilod,
     & REAL(R_ukod,4),REAL(R_okod,4),L_umod,REAL(R_olod,4
     &))
      !}
C FDB10_vlv.fgi(  26,  47):���������� �������,20FDB13CW001XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_ulef,R_oref,REAL(1,4)
     &,
     & REAL(R_imef,4),REAL(R_omef,4),
     & REAL(R_olef,4),REAL(R_ilef,4),I_iref,
     & REAL(R_epef,4),L_ipef,REAL(R_opef,4),L_upef,L_aref
     &,R_umef,
     & REAL(R_emef,4),REAL(R_amef,4),L_eref,REAL(R_apef,4
     &))
      !}
C FDB10_vlv.fgi(  56,  63):���������� �������,20FDB13CF106XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_esef,R_axef,REAL(1,4)
     &,
     & REAL(R_usef,4),REAL(R_atef,4),
     & REAL(R_asef,4),REAL(R_uref,4),I_uvef,
     & REAL(R_otef,4),L_utef,REAL(R_avef,4),L_evef,L_ivef
     &,R_etef,
     & REAL(R_osef,4),REAL(R_isef,4),L_ovef,REAL(R_itef,4
     &))
      !}
C FDB10_vlv.fgi(  56,  95):���������� �������,20FDB13CF105XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_akif,R_umif,REAL(1,4)
     &,
     & REAL(R_okif,4),REAL(R_ukif,4),
     & REAL(R_ufif,4),REAL(R_ofif,4),I_omif,
     & REAL(R_ilif,4),L_olif,REAL(R_ulif,4),L_amif,L_emif
     &,R_alif,
     & REAL(R_ikif,4),REAL(R_ekif,4),L_imif,REAL(R_elif,4
     &))
      !}
C FDB10_vlv.fgi(  56, 125):���������� �������,20FDB11CW001XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_ipif,R_etif,REAL(1,4)
     &,
     & REAL(R_arif,4),REAL(R_erif,4),
     & REAL(R_epif,4),REAL(R_apif,4),I_atif,
     & REAL(R_urif,4),L_asif,REAL(R_esif,4),L_isif,L_osif
     &,R_irif,
     & REAL(R_upif,4),REAL(R_opif,4),L_usif,REAL(R_orif,4
     &))
      !}
C FDB10_vlv.fgi(  56, 110):���������� �������,20FDB11CW003XQ01
      C8_itif = 'regim5'
C FDB10_logic.fgi(  45,  26):��������� ���������� CH8 (�������)
      call chcomp(C8_obof,C8_itif,L_(23))
C FDB10_logic.fgi(  50,  27):���������� ���������
      C8_otif = 'regim4'
C FDB10_logic.fgi(  45,  32):��������� ���������� CH8 (�������)
      call chcomp(C8_obof,C8_otif,L_(24))
C FDB10_logic.fgi(  50,  33):���������� ���������
      C8_utif = 'regim3'
C FDB10_logic.fgi(  45,  38):��������� ���������� CH8 (�������)
      call chcomp(C8_obof,C8_utif,L_(25))
C FDB10_logic.fgi(  50,  39):���������� ���������
      C8_avif = 'regim2'
C FDB10_logic.fgi(  45,  44):��������� ���������� CH8 (�������)
      call chcomp(C8_obof,C8_avif,L_(26))
C FDB10_logic.fgi(  50,  45):���������� ���������
      C30_evif = '�������������� �����'
C FDB10_logic.fgi(  98,  35):��������� ���������� CH20 (CH30) (�������)
      C30_ovif = '������������������ �����'
C FDB10_logic.fgi(  70,  36):��������� ���������� CH20 (CH30) (�������)
      C30_axif = '������ �����'
C FDB10_logic.fgi( 110,  53):��������� ���������� CH20 (CH30) (�������)
      C30_ixif = '����� �������'
C FDB10_logic.fgi(  89,  54):��������� ���������� CH20 (CH30) (�������)
      C30_uxif = '��������� ���������'
C FDB10_logic.fgi(  70,  55):��������� ���������� CH20 (CH30) (�������)
      C30_abof = ''
C FDB10_logic.fgi(  70,  57):��������� ���������� CH20 (CH30) (�������)
      C8_ibof = 'regim1'
C FDB10_logic.fgi(  45,  50):��������� ���������� CH8 (�������)
      call chcomp(C8_obof,C8_ibof,L_(27))
C FDB10_logic.fgi(  50,  51):���������� ���������
      if(L_(27)) then
         C30_oxif=C30_uxif
      else
         C30_oxif=C30_abof
      endif
C FDB10_logic.fgi(  74,  55):���� RE IN LO CH20
      if(L_(26)) then
         C30_exif=C30_ixif
      else
         C30_exif=C30_oxif
      endif
C FDB10_logic.fgi(  94,  54):���� RE IN LO CH20
      if(L_(25)) then
         C30_uvif=C30_axif
      else
         C30_uvif=C30_exif
      endif
C FDB10_logic.fgi( 115,  53):���� RE IN LO CH20
      if(L_(24)) then
         C30_ivif=C30_ovif
      else
         C30_ivif=C30_uvif
      endif
C FDB10_logic.fgi(  75,  36):���� RE IN LO CH20
      if(L_(23)) then
         C30_ebof=C30_evif
      else
         C30_ebof=C30_ivif
      endif
C FDB10_logic.fgi( 103,  35):���� RE IN LO CH20
      End
