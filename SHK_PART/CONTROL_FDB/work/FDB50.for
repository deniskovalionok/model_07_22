      Subroutine FDB50(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDB50.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R_(65)=R8_efo
C FDB50_vent_log.fgi( 355, 476):pre: �������������� �����  
      R_(57)=R8_ebo
C FDB50_vent_log.fgi( 460, 476):pre: �������������� �����  
      R_(61)=R8_edo
C FDB50_vent_log.fgi( 409, 476):pre: �������������� �����  
      R_(69)=R8_eko
C FDB50_vent_log.fgi( 302, 476):pre: �������������� �����  
      R_(73)=R8_elo
C FDB50_vent_log.fgi( 251, 476):pre: �������������� �����  
      R_(77)=R8_emo
C FDB50_vent_log.fgi( 198, 476):pre: �������������� �����  
      R_(347)=R0_arure
C FDB50_logic.fgi( 196, 305):pre: �������� ��������� ������,wait01
      R_(343)=R0_elure
C FDB50_logic.fgi( 192, 293):pre: �������� ��������� ������,wait005
      R_(344)=R0_ulure
C FDB50_logic.fgi( 197, 395):pre: �������� ��������� ������,wait02
      R_(346)=R0_apure
C FDB50_logic.fgi( 230, 377):pre: �������� ��������� ������,wait001
      R_(345)=R0_imure
C FDB50_logic.fgi( 229, 311):pre: �������� ��������� ������,wait003
      R_(89)=R8_eto
C FDB50_vent_log.fgi(  34, 476):pre: �������������� �����  
      R_(85)=R8_uro
C FDB50_vent_log.fgi(  90, 476):pre: �������������� �����  
      R_(81)=R8_ipo
C FDB50_vent_log.fgi( 145, 476):pre: �������������� �����  
      !��������� R_(11) = FDB50_vent_logC?? /60/
      R_(11)=R0_e
C FDB50_vent_log.fgi( 148, 253):���������
      !��������� R_(13) = FDB50_vent_logC?? /60/
      R_(13)=R0_i
C FDB50_vent_log.fgi( 148, 261):���������
      !��������� R_(15) = FDB50_vent_logC?? /60/
      R_(15)=R0_o
C FDB50_vent_log.fgi( 148, 268):���������
      !��������� R_(17) = FDB50_vent_logC?? /60/
      R_(17)=R0_u
C FDB50_vent_log.fgi( 148, 276):���������
      !��������� R_(19) = FDB50_vent_logC?? /60/
      R_(19)=R0_ad
C FDB50_vent_log.fgi( 148, 284):���������
      !��������� R_(21) = FDB50_vent_logC?? /60/
      R_(21)=R0_ed
C FDB50_vent_log.fgi( 148, 291):���������
      !��������� R_(23) = FDB50_vent_logC?? /60/
      R_(23)=R0_id
C FDB50_vent_log.fgi( 148, 299):���������
      !��������� R_(25) = FDB50_vent_logC?? /60/
      R_(25)=R0_od
C FDB50_vent_log.fgi( 148, 306):���������
      !��������� R_(27) = FDB50_vent_logC?? /60/
      R_(27)=R0_ud
C FDB50_vent_log.fgi( 148, 313):���������
      !��������� R_(29) = FDB50_vent_logC?? /60/
      R_(29)=R0_af
C FDB50_vent_log.fgi( 148, 320):���������
      !��������� R_(31) = FDB50_vent_logC?? /60/
      R_(31)=R0_ef
C FDB50_vent_log.fgi( 148, 327):���������
      !��������� R_(33) = FDB50_vent_logC?? /60/
      R_(33)=R0_if
C FDB50_vent_log.fgi( 148, 335):���������
      !��������� R_(35) = FDB50_vent_logC?? /60/
      R_(35)=R0_of
C FDB50_vent_log.fgi( 148, 343):���������
      !��������� R_(37) = FDB50_vent_logC?? /60/
      R_(37)=R0_uf
C FDB50_vent_log.fgi( 148, 350):���������
      !��������� R_(39) = FDB50_vent_logC?? /60/
      R_(39)=R0_ak
C FDB50_vent_log.fgi( 148, 357):���������
      !��������� R_(41) = FDB50_vent_logC?? /60/
      R_(41)=R0_ek
C FDB50_vent_log.fgi( 148, 365):���������
      !��������� R_(1) = FDB50_vent_logC?? /13000/
      R_(1)=R0_ik
C FDB50_vent_log.fgi( 219, 378):���������
      if(R8_ok.ge.0.0) then
         R_(2)=R8_uk/max(R8_ok,1.0e-10)
      else
         R_(2)=R8_uk/min(R8_ok,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 209, 380):�������� ����������
      R_efid = R_(2) * R_(1)
C FDB50_vent_log.fgi( 222, 379):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_adid,R_alid,REAL(1,4
     &),
     & REAL(R_efid,4),I_ukid,REAL(R_odid,4),
     & REAL(R_udid,4),REAL(R_ubid,4),
     & REAL(R_obid,4),REAL(R_ofid,4),L_ufid,REAL(R_akid,4
     &),L_ekid,
     & L_ikid,R_afid,REAL(R_idid,4),REAL(R_edid,4),L_okid
     &)
      !}
C FDB50_vlv.fgi( 213, 138):���������� ������� ��� 2,20FDB50CF010XQ01
      !��������� R_(3) = FDB50_vent_logC?? /13000/
      R_(3)=R0_al
C FDB50_vent_log.fgi( 219, 388):���������
      if(R8_el.ge.0.0) then
         R_(4)=R8_il/max(R8_el,1.0e-10)
      else
         R_(4)=R8_il/min(R8_el,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 209, 390):�������� ����������
      R_umid = R_(4) * R_(3)
C FDB50_vent_log.fgi( 222, 389):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_olid,R_orid,REAL(1,4
     &),
     & REAL(R_umid,4),I_irid,REAL(R_emid,4),
     & REAL(R_imid,4),REAL(R_ilid,4),
     & REAL(R_elid,4),REAL(R_epid,4),L_ipid,REAL(R_opid,4
     &),L_upid,
     & L_arid,R_omid,REAL(R_amid,4),REAL(R_ulid,4),L_erid
     &)
      !}
C FDB50_vlv.fgi( 184, 138):���������� ������� ��� 2,20FDB50CF009XQ01
      !��������� R_(43) = FDB50_vent_logC?? /60/
      R_(43)=R0_ol
C FDB50_vent_log.fgi( 148, 373):���������
      !��������� R_(45) = FDB50_vent_logC?? /60/
      R_(45)=R0_ul
C FDB50_vent_log.fgi( 148, 381):���������
      !��������� R_(47) = FDB50_vent_logC?? /60/
      R_(47)=R0_am
C FDB50_vent_log.fgi( 148, 389):���������
      !��������� R_(49) = FDB50_vent_logC?? /60/
      R_(49)=R0_em
C FDB50_vent_log.fgi( 148, 396):���������
      !��������� R_(51) = FDB50_vent_logC?? /60/
      R_(51)=R0_im
C FDB50_vent_log.fgi( 148, 404):���������
      !��������� R_(53) = FDB50_vent_logC?? /60/
      R_(53)=R0_om
C FDB50_vent_log.fgi( 148, 412):���������
      !��������� R_(55) = FDB50_vent_logC?? /60/
      R_(55)=R0_um
C FDB50_vent_log.fgi( 148, 420):���������
      !��������� R_(5) = FDB50_vent_logC?? /13000/
      R_(5)=R0_ap
C FDB50_vent_log.fgi( 219, 400):���������
      !��������� R_(7) = FDB50_vent_logC?? /13000/
      R_(7)=R0_ep
C FDB50_vent_log.fgi( 219, 409):���������
      !��������� R_(9) = FDB50_vent_logC?? /13000/
      R_(9)=R0_ip
C FDB50_vent_log.fgi( 219, 420):���������
      R_osud = R8_op
C FDB50_vent_log.fgi( 405, 397):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_evud,4),REAL
     &(R_osud,4),R_orud,
     & R_ivud,REAL(1,4),REAL(R_esud,4),
     & REAL(R_isud,4),REAL(R_edase,4),
     & REAL(R_irud,4),I_avud,REAL(R_usud,4),L_atud,
     & REAL(R_etud,4),L_itud,L_otud,R_idase,REAL(R_asud,4
     &),REAL(R_urud,4),L_utud)
      !}
C FDB50_vlv.fgi( 156, 182):���������� ������� ��������,20FDB50CP016XQ01
      L_adase=R_idase.gt.R_edase
C FDB50_logic.fgi( 143, 461):���������� >
      L_ubase=R_idase.lt.R_edase
C FDB50_logic.fgi( 143, 455):���������� <
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_itade,4),R8_irade,I_uxade
     &,I_ibede,I_oxade,
     & C8_asade,I_ebede,R_usade,R_osade,R_imade,
     & REAL(R_umade,4),R_opade,REAL(R_arade,4),
     & R_apade,REAL(R_ipade,4),I_exade,I_obede,I_abede,I_axade
     &,L_erade,
     & L_adede,L_afede,L_upade,L_epade,
     & L_urade,L_orade,L_idede,L_omade,L_isade,L_ofede,L_atade
     &,
     & L_etade,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_ubase
     &,L_adase,L_edede,
     & I_ubede,L_efede,R_uvade,REAL(R_esade,4),L_ifede,L_amade
     &,L_ufede,L_emade,
     & L_otade,L_utade,L_avade,L_ivade,L_ovade,L_evade)
      !}

      if(L_ovade.or.L_ivade.or.L_evade.or.L_avade.or.L_utade.or.L_otade
     &) then      
                  I_ixade = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ixade = z'40000000'
      endif
C FDB50_vlv.fgi( 419, 195):���� ���������� �������� ��������,20FDB50AA134
      R_abaf = R8_up
C FDB50_vent_log.fgi( 405, 401):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_odaf,4),REAL
     &(R_abaf,4),R_axud,
     & R_udaf,REAL(1,4),REAL(R_oxud,4),
     & REAL(R_uxud,4),REAL(R_ibase,4),
     & REAL(R_uvud,4),I_idaf,REAL(R_ebaf,4),L_ibaf,
     & REAL(R_obaf,4),L_ubaf,L_adaf,R_obase,REAL(R_ixud,4
     &),REAL(R_exud,4),L_edaf)
      !}
C FDB50_vlv.fgi( 127, 182):���������� ������� ��������,20FDB50CP015XQ01
      L_ebase=R_obase.gt.R_ibase
C FDB50_logic.fgi( 143, 443):���������� >
      L_abase=R_obase.lt.R_ibase
C FDB50_logic.fgi( 143, 437):���������� <
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ileke,4),R8_ifeke,I_upeke
     &,I_ireke,I_opeke,
     & C8_akeke,I_ereke,R_ukeke,R_okeke,R_ibeke,
     & REAL(R_ubeke,4),R_odeke,REAL(R_afeke,4),
     & R_adeke,REAL(R_ideke,4),I_epeke,I_oreke,I_areke,I_apeke
     &,L_efeke,
     & L_aseke,L_ateke,L_udeke,L_edeke,
     & L_ufeke,L_ofeke,L_iseke,L_obeke,L_ikeke,L_oteke,L_aleke
     &,
     & L_eleke,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_abase
     &,L_ebase,L_eseke,
     & I_ureke,L_eteke,R_umeke,REAL(R_ekeke,4),L_iteke,L_abeke
     &,L_uteke,L_ebeke,
     & L_oleke,L_uleke,L_ameke,L_imeke,L_omeke,L_emeke)
      !}

      if(L_omeke.or.L_imeke.or.L_emeke.or.L_ameke.or.L_uleke.or.L_oleke
     &) then      
                  I_ipeke = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ipeke = z'40000000'
      endif
C FDB50_vlv.fgi( 419, 167):���� ���������� �������� ��������,20FDB50AA122
      R_asif = R8_ar
C FDB50_vent_log.fgi( 405, 406):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_otif,4),REAL
     &(R_asif,4),R_arif,
     & R_utif,REAL(1,4),REAL(R_orif,4),
     & REAL(R_urif,4),REAL(R_afase,4),
     & REAL(R_upif,4),I_itif,REAL(R_esif,4),L_isif,
     & REAL(R_osif,4),L_usif,L_atif,R_efase,REAL(R_irif,4
     &),REAL(R_erif,4),L_etif)
      !}
C FDB50_vlv.fgi( 156, 168):���������� ������� ��������,20FDB50CP011XQ01
      L_udase=R_efase.gt.R_afase
C FDB50_logic.fgi(  52, 444):���������� >
      L_odase=R_efase.lt.R_afase
C FDB50_logic.fgi(  52, 438):���������� <
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_isov,4),R8_ipov,I_uvov
     &,I_ixov,I_ovov,
     & C8_arov,I_exov,R_urov,R_orov,R_ilov,
     & REAL(R_ulov,4),R_omov,REAL(R_apov,4),
     & R_amov,REAL(R_imov,4),I_evov,I_oxov,I_axov,I_avov,L_epov
     &,
     & L_abuv,L_aduv,L_umov,L_emov,
     & L_upov,L_opov,L_ibuv,L_olov,L_irov,L_oduv,L_asov,
     & L_esov,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_odase
     &,L_udase,L_ebuv,
     & I_uxov,L_eduv,R_utov,REAL(R_erov,4),L_iduv,L_alov,L_uduv
     &,L_elov,
     & L_osov,L_usov,L_atov,L_itov,L_otov,L_etov)
      !}

      if(L_otov.or.L_itov.or.L_etov.or.L_atov.or.L_usov.or.L_osov
     &) then      
                  I_ivov = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ivov = z'40000000'
      endif
C FDB50_vlv.fgi( 476, 167):���� ���������� �������� ��������,20FDB50AA117
      R_olif = R8_er
C FDB50_vent_log.fgi( 405, 411):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_epif,4),REAL
     &(R_olif,4),R_okif,
     & R_ipif,REAL(1,4),REAL(R_elif,4),
     & REAL(R_ilif,4),REAL(R_ufase,4),
     & REAL(R_ikif,4),I_apif,REAL(R_ulif,4),L_amif,
     & REAL(R_emif,4),L_imif,L_omif,R_akase,REAL(R_alif,4
     &),REAL(R_ukif,4),L_umif)
      !}
C FDB50_vlv.fgi( 127, 168):���������� ������� ��������,20FDB50CP008XQ01
      L_ofase=R_akase.gt.R_ufase
C FDB50_logic.fgi(  52, 461):���������� >
      L_ifase=R_akase.lt.R_ufase
C FDB50_logic.fgi(  52, 455):���������� <
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_etere,4),R8_erere,I_oxere
     &,I_ebire,I_ixere,
     & C8_urere,I_abire,R_osere,R_isere,R_emere,
     & REAL(R_omere,4),R_ipere,REAL(R_upere,4),
     & R_umere,REAL(R_epere,4),I_axere,I_ibire,I_uxere,I_uvere
     &,L_arere,
     & L_ubire,L_udire,L_opere,L_apere,
     & L_orere,L_irere,L_edire,L_imere,L_esere,L_ifire,L_usere
     &,
     & L_atere,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_ifase
     &,L_ofase,L_adire,
     & I_obire,L_afire,R_overe,REAL(R_asere,4),L_efire,L_ulere
     &,L_ofire,L_amere,
     & L_itere,L_otere,L_utere,L_evere,L_ivere,L_avere)
      !}

      if(L_ivere.or.L_evere.or.L_avere.or.L_utere.or.L_otere.or.L_itere
     &) then      
                  I_exere = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_exere = z'40000000'
      endif
C FDB50_vlv.fgi( 393, 268):���� ���������� �������� ��������,20FDB50AA111
      R_ivit = R8_ir
C FDB50_vent_log.fgi( 348, 404):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_etit,R_ebot,REAL(1,4
     &),
     & REAL(R_ivit,4),I_abot,REAL(R_utit,4),
     & REAL(R_avit,4),REAL(R_atit,4),
     & REAL(R_usit,4),REAL(R_uvit,4),L_axit,REAL(R_exit,4
     &),L_ixit,
     & L_oxit,R_evit,REAL(R_otit,4),REAL(R_itit,4),L_uxit
     &)
      !}
C FDB50_vlv.fgi(  46, 237):���������� ������� ��� 2,20FDB50CP017XQ01
      R_afot = R8_or
C FDB50_vent_log.fgi( 405, 421):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ubot,R_ukot,REAL(1e
     &-3,4),
     & REAL(R_afot,4),I_okot,REAL(R_idot,4),
     & REAL(R_odot,4),REAL(R_obot,4),
     & REAL(R_ibot,4),REAL(R_ifot,4),L_ofot,REAL(R_ufot,4
     &),L_akot,
     & L_ekot,R_udot,REAL(R_edot,4),REAL(R_adot,4),L_ikot
     &)
      !}
C FDB50_vlv.fgi(  46, 252):���������� ������� ��� 2,20FDB50CP021XQ01
      R_iraf = R8_ur
C FDB50_vent_log.fgi( 348, 412):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_epaf,R_etaf,REAL(1,4
     &),
     & REAL(R_iraf,4),I_ataf,REAL(R_upaf,4),
     & REAL(R_araf,4),REAL(R_apaf,4),
     & REAL(R_umaf,4),REAL(R_uraf,4),L_asaf,REAL(R_esaf,4
     &),L_isaf,
     & L_osaf,R_eraf,REAL(R_opaf,4),REAL(R_ipaf,4),L_usaf
     &)
      !}
C FDB50_vlv.fgi( 127, 212):���������� ������� ��� 2,20FDB50CP103XQ01
      R_ofef = R8_as
C FDB50_vent_log.fgi( 348, 417):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_idef,R_ilef,REAL(1,4
     &),
     & REAL(R_ofef,4),I_elef,REAL(R_afef,4),
     & REAL(R_efef,4),REAL(R_edef,4),
     & REAL(R_adef,4),REAL(R_akef,4),L_ekef,REAL(R_ikef,4
     &),L_okef,
     & L_ukef,R_ifef,REAL(R_udef,4),REAL(R_odef,4),L_alef
     &)
      !}
C FDB50_vlv.fgi( 156, 212):���������� ������� ��� 2,20FDB50CP102XQ01
      R_idif = R8_es
C FDB50_vent_log.fgi( 348, 421):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ebif,R_ekif,REAL(1,4
     &),
     & REAL(R_idif,4),I_akif,REAL(R_ubif,4),
     & REAL(R_adif,4),REAL(R_abif,4),
     & REAL(R_uxef,4),REAL(R_udif,4),L_afif,REAL(R_efif,4
     &),L_ifif,
     & L_ofif,R_edif,REAL(R_obif,4),REAL(R_ibif,4),L_ufif
     &)
      !}
C FDB50_vlv.fgi( 185, 212):���������� ������� ��� 2,20FDB50CP101XQ01
      R_imud = R8_is
C FDB50_vent_log.fgi(  31, 377):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_elud,R_erud,REAL(1,4
     &),
     & REAL(R_imud,4),I_arud,REAL(R_ulud,4),
     & REAL(R_amud,4),REAL(R_alud,4),
     & REAL(R_ukud,4),REAL(R_umud,4),L_apud,REAL(R_epud,4
     &),L_ipud,
     & L_opud,R_emud,REAL(R_olud,4),REAL(R_ilud,4),L_upud
     &)
      !}
C FDB50_vlv.fgi( 185, 197):���������� ������� ��� 2,20FDB50CT022XQ01
      R_ukaf = R8_os
C FDB50_vent_log.fgi(  31, 381):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ofaf,R_omaf,REAL(1,4
     &),
     & REAL(R_ukaf,4),I_imaf,REAL(R_ekaf,4),
     & REAL(R_ikaf,4),REAL(R_ifaf,4),
     & REAL(R_efaf,4),REAL(R_elaf,4),L_ilaf,REAL(R_olaf,4
     &),L_ulaf,
     & L_amaf,R_okaf,REAL(R_akaf,4),REAL(R_ufaf,4),L_emaf
     &)
      !}
C FDB50_vlv.fgi( 127, 197):���������� ������� ��� 2,20FDB50CT018XQ01
      R_ovof = R8_us
C FDB50_vent_log.fgi(  31, 384):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itof,R_ibuf,REAL(1,4
     &),
     & REAL(R_ovof,4),I_ebuf,REAL(R_avof,4),
     & REAL(R_evof,4),REAL(R_etof,4),
     & REAL(R_atof,4),REAL(R_axof,4),L_exof,REAL(R_ixof,4
     &),L_oxof,
     & L_uxof,R_ivof,REAL(R_utof,4),REAL(R_otof,4),L_abuf
     &)
      !}
C FDB50_vlv.fgi( 273, 197):���������� ������� ��� 2,20FDB50CT015XQ01
      R_umuf = R8_at
C FDB50_vent_log.fgi(  31, 387):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_oluf,R_oruf,REAL(1,4
     &),
     & REAL(R_umuf,4),I_iruf,REAL(R_emuf,4),
     & REAL(R_imuf,4),REAL(R_iluf,4),
     & REAL(R_eluf,4),REAL(R_epuf,4),L_ipuf,REAL(R_opuf,4
     &),L_upuf,
     & L_aruf,R_omuf,REAL(R_amuf,4),REAL(R_uluf,4),L_eruf
     &)
      !}
C FDB50_vlv.fgi( 331, 212):���������� ������� ��� 2,20FDB50CT014XQ01
      R_adak = R8_et
C FDB50_vent_log.fgi(  31, 390):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uxuf,R_ufak,REAL(1,4
     &),
     & REAL(R_adak,4),I_ofak,REAL(R_ibak,4),
     & REAL(R_obak,4),REAL(R_oxuf,4),
     & REAL(R_ixuf,4),REAL(R_idak,4),L_odak,REAL(R_udak,4
     &),L_afak,
     & L_efak,R_ubak,REAL(R_ebak,4),REAL(R_abak,4),L_ifak
     &)
      !}
C FDB50_vlv.fgi( 302, 212):���������� ������� ��� 2,20FDB50CT013XQ01
      R_esak = R8_it
C FDB50_vent_log.fgi(  31, 393):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_arak,R_avak,REAL(1,4
     &),
     & REAL(R_esak,4),I_utak,REAL(R_orak,4),
     & REAL(R_urak,4),REAL(R_upak,4),
     & REAL(R_opak,4),REAL(R_osak,4),L_usak,REAL(R_atak,4
     &),L_etak,
     & L_itak,R_asak,REAL(R_irak,4),REAL(R_erak,4),L_otak
     &)
      !}
C FDB50_vlv.fgi( 273, 227):���������� ������� ��� 2,20FDB50CT012XQ01
      R_ikek = R8_ot
C FDB50_vent_log.fgi(  31, 397):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efek,R_emek,REAL(1,4
     &),
     & REAL(R_ikek,4),I_amek,REAL(R_ufek,4),
     & REAL(R_akek,4),REAL(R_afek,4),
     & REAL(R_udek,4),REAL(R_ukek,4),L_alek,REAL(R_elek,4
     &),L_ilek,
     & L_olek,R_ekek,REAL(R_ofek,4),REAL(R_ifek,4),L_ulek
     &)
      !}
C FDB50_vlv.fgi( 243, 227):���������� ������� ��� 2,20FDB50CT011XQ01
      R_ovek = R8_ut
C FDB50_vent_log.fgi(  31, 401):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itek,R_ibik,REAL(1,4
     &),
     & REAL(R_ovek,4),I_ebik,REAL(R_avek,4),
     & REAL(R_evek,4),REAL(R_etek,4),
     & REAL(R_atek,4),REAL(R_axek,4),L_exek,REAL(R_ixek,4
     &),L_oxek,
     & L_uxek,R_ivek,REAL(R_utek,4),REAL(R_otek,4),L_abik
     &)
      !}
C FDB50_vlv.fgi( 273, 258):���������� ������� ��� 2,20FDB50CT010XQ01
      R_umik = R8_av
C FDB50_vent_log.fgi(  31, 405):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_olik,R_orik,REAL(1,4
     &),
     & REAL(R_umik,4),I_irik,REAL(R_emik,4),
     & REAL(R_imik,4),REAL(R_ilik,4),
     & REAL(R_elik,4),REAL(R_epik,4),L_ipik,REAL(R_opik,4
     &),L_upik,
     & L_arik,R_omik,REAL(R_amik,4),REAL(R_ulik,4),L_erik
     &)
      !}
C FDB50_vlv.fgi( 243, 258):���������� ������� ��� 2,20FDB50CT009XQ01
      R_adok = R8_ev
C FDB50_vent_log.fgi(  31, 408):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uxik,R_ufok,REAL(1,4
     &),
     & REAL(R_adok,4),I_ofok,REAL(R_ibok,4),
     & REAL(R_obok,4),REAL(R_oxik,4),
     & REAL(R_ixik,4),REAL(R_idok,4),L_odok,REAL(R_udok,4
     &),L_afok,
     & L_efok,R_ubok,REAL(R_ebok,4),REAL(R_abok,4),L_ifok
     &)
      !}
C FDB50_vlv.fgi( 214, 227):���������� ������� ��� 2,20FDB50CT008XQ01
      R_esok = R8_iv
C FDB50_vent_log.fgi(  31, 412):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_arok,R_avok,REAL(1,4
     &),
     & REAL(R_esok,4),I_utok,REAL(R_orok,4),
     & REAL(R_urok,4),REAL(R_upok,4),
     & REAL(R_opok,4),REAL(R_osok,4),L_usok,REAL(R_atok,4
     &),L_etok,
     & L_itok,R_asok,REAL(R_irok,4),REAL(R_erok,4),L_otok
     &)
      !}
C FDB50_vlv.fgi( 185, 227):���������� ������� ��� 2,20FDB50CT007XQ01
      R_ikuk = R8_ov
C FDB50_vent_log.fgi(  31, 415):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efuk,R_emuk,REAL(1,4
     &),
     & REAL(R_ikuk,4),I_amuk,REAL(R_ufuk,4),
     & REAL(R_akuk,4),REAL(R_afuk,4),
     & REAL(R_uduk,4),REAL(R_ukuk,4),L_aluk,REAL(R_eluk,4
     &),L_iluk,
     & L_oluk,R_ekuk,REAL(R_ofuk,4),REAL(R_ifuk,4),L_uluk
     &)
      !}
C FDB50_vlv.fgi( 156, 227):���������� ������� ��� 2,20FDB50CT006XQ01
      R_ovuk = R8_uv
C FDB50_vent_log.fgi(  31, 418):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ituk,R_ibal,REAL(1,4
     &),
     & REAL(R_ovuk,4),I_ebal,REAL(R_avuk,4),
     & REAL(R_evuk,4),REAL(R_etuk,4),
     & REAL(R_atuk,4),REAL(R_axuk,4),L_exuk,REAL(R_ixuk,4
     &),L_oxuk,
     & L_uxuk,R_ivuk,REAL(R_utuk,4),REAL(R_otuk,4),L_abal
     &)
      !}
C FDB50_vlv.fgi( 127, 227):���������� ������� ��� 2,20FDB50CT005XQ01
      R_axaf = R8_ax
C FDB50_vent_log.fgi(  31, 421):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_utaf,R_ubef,REAL(1,4
     &),
     & REAL(R_axaf,4),I_obef,REAL(R_ivaf,4),
     & REAL(R_ovaf,4),REAL(R_otaf,4),
     & REAL(R_itaf,4),REAL(R_ixaf,4),L_oxaf,REAL(R_uxaf,4
     &),L_abef,
     & L_ebef,R_uvaf,REAL(R_evaf,4),REAL(R_avaf,4),L_ibef
     &)
      !}
C FDB50_vlv.fgi( 156, 197):���������� ������� ��� 2,20FDB50CT004XQ01
      R_uxif = R8_ex
C FDB50_vent_log.fgi(  31, 424):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovif,R_odof,REAL(1,4
     &),
     & REAL(R_uxif,4),I_idof,REAL(R_exif,4),
     & REAL(R_ixif,4),REAL(R_ivif,4),
     & REAL(R_evif,4),REAL(R_ebof,4),L_ibof,REAL(R_obof,4
     &),L_ubof,
     & L_adof,R_oxif,REAL(R_axif,4),REAL(R_uvif,4),L_edof
     &)
      !}
C FDB50_vlv.fgi( 214, 212):���������� ������� ��� 2,20FDB50CT003XQ01
      R_opod = R8_ix
C FDB50_vent_log.fgi( 289, 348):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_imod,R_isod,REAL(1,4
     &),
     & REAL(R_opod,4),I_esod,REAL(R_apod,4),
     & REAL(R_epod,4),REAL(R_emod,4),
     & REAL(R_amod,4),REAL(R_arod,4),L_erod,REAL(R_irod,4
     &),L_orod,
     & L_urod,R_ipod,REAL(R_umod,4),REAL(R_omod,4),L_asod
     &)
      !}
C FDB50_vlv.fgi(  46, 192):���������� ������� ��� 2,20FDB50CM009XQ01
      R_efar = R8_ox
C FDB50_vent_log.fgi( 289, 351):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_adar,R_alar,REAL(1,4
     &),
     & REAL(R_efar,4),I_ukar,REAL(R_odar,4),
     & REAL(R_udar,4),REAL(R_ubar,4),
     & REAL(R_obar,4),REAL(R_ofar,4),L_ufar,REAL(R_akar,4
     &),L_ekar,
     & L_ikar,R_afar,REAL(R_idar,4),REAL(R_edar,4),L_okar
     &)
      !}
C FDB50_vlv.fgi(  73, 222):���������� ������� ��� 2,20FDB50CM008XQ01
      R_ux = R8_abe
C FDB50_vent_log.fgi( 289, 355):��������
      R_ikup = R8_ebe
C FDB50_vent_log.fgi( 289, 359):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efup,R_emup,REAL(1,4
     &),
     & REAL(R_ikup,4),I_amup,REAL(R_ufup,4),
     & REAL(R_akup,4),REAL(R_afup,4),
     & REAL(R_udup,4),REAL(R_ukup,4),L_alup,REAL(R_elup,4
     &),L_ilup,
     & L_olup,R_ekup,REAL(R_ofup,4),REAL(R_ifup,4),L_ulup
     &)
      !}
C FDB50_vlv.fgi( 100, 147):���������� ������� ��� 2,20FDB50CM006XQ01
      R_utef = R8_ibe
C FDB50_vent_log.fgi( 289, 363):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_osef,R_oxef,REAL(1,4
     &),
     & REAL(R_utef,4),I_ixef,REAL(R_etef,4),
     & REAL(R_itef,4),REAL(R_isef,4),
     & REAL(R_esef,4),REAL(R_evef,4),L_ivef,REAL(R_ovef,4
     &),L_uvef,
     & L_axef,R_otef,REAL(R_atef,4),REAL(R_usef,4),L_exef
     &)
      !}
C FDB50_vlv.fgi( 302, 197):���������� ������� ��� 2,20FDB50CM005XQ01
      R_arof = R8_obe
C FDB50_vent_log.fgi( 289, 367):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umof,R_usof,REAL(1,4
     &),
     & REAL(R_arof,4),I_osof,REAL(R_ipof,4),
     & REAL(R_opof,4),REAL(R_omof,4),
     & REAL(R_imof,4),REAL(R_irof,4),L_orof,REAL(R_urof,4
     &),L_asof,
     & L_esof,R_upof,REAL(R_epof,4),REAL(R_apof,4),L_isof
     &)
      !}
C FDB50_vlv.fgi( 243, 212):���������� ������� ��� 2,20FDB50CM004XQ01
      R_umar = R8_ube
C FDB50_vent_log.fgi( 289, 371):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_olar,R_orar,REAL(1,4
     &),
     & REAL(R_umar,4),I_irar,REAL(R_emar,4),
     & REAL(R_imar,4),REAL(R_ilar,4),
     & REAL(R_elar,4),REAL(R_epar,4),L_ipar,REAL(R_opar,4
     &),L_upar,
     & L_arar,R_omar,REAL(R_amar,4),REAL(R_ular,4),L_erar
     &)
      !}
C FDB50_vlv.fgi( 100, 162):���������� ������� ��� 2,20FDB50CM003XQ01
      R_ader = R8_ade
C FDB50_vent_log.fgi( 289, 375):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uxar,R_ufer,REAL(1,4
     &),
     & REAL(R_ader,4),I_ofer,REAL(R_iber,4),
     & REAL(R_ober,4),REAL(R_oxar,4),
     & REAL(R_ixar,4),REAL(R_ider,4),L_oder,REAL(R_uder,4
     &),L_afer,
     & L_efer,R_uber,REAL(R_eber,4),REAL(R_aber,4),L_ifer
     &)
      !}
C FDB50_vlv.fgi( 100, 192):���������� ������� ��� 2,20FDB50CM002XQ01
      R_uxer = R8_ede
C FDB50_vent_log.fgi( 289, 379):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_over,R_odir,REAL(1,4
     &),
     & REAL(R_uxer,4),I_idir,REAL(R_exer,4),
     & REAL(R_ixer,4),REAL(R_iver,4),
     & REAL(R_ever,4),REAL(R_ebir,4),L_ibir,REAL(R_obir,4
     &),L_ubir,
     & L_adir,R_oxer,REAL(R_axer,4),REAL(R_uver,4),L_edir
     &)
      !}
C FDB50_vlv.fgi( 100, 222):���������� ������� ��� 2,20FDB50CM001XQ01
      R_evod = R8_ide
C FDB50_vent_log.fgi( 289, 390):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_atod,R_abud,REAL(1,4
     &),
     & REAL(R_evod,4),I_uxod,REAL(R_otod,4),
     & REAL(R_utod,4),REAL(R_usod,4),
     & REAL(R_osod,4),REAL(R_ovod,4),L_uvod,REAL(R_axod,4
     &),L_exod,
     & L_ixod,R_avod,REAL(R_itod,4),REAL(R_etod,4),L_oxod
     &)
      !}
C FDB50_vlv.fgi(  46, 207):���������� ������� ��� 2,20FDB50CQ009XQ01
      R_ovup = R8_ode
C FDB50_vent_log.fgi( 289, 393):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itup,R_ibar,REAL(1,4
     &),
     & REAL(R_ovup,4),I_ebar,REAL(R_avup,4),
     & REAL(R_evup,4),REAL(R_etup,4),
     & REAL(R_atup,4),REAL(R_axup,4),L_exup,REAL(R_ixup,4
     &),L_oxup,
     & L_uxup,R_ivup,REAL(R_utup,4),REAL(R_otup,4),L_abar
     &)
      !}
C FDB50_vlv.fgi(  73, 207):���������� ������� ��� 2,20FDB50CQ008XQ01
      R_ude = R8_afe
C FDB50_vent_log.fgi( 289, 397):��������
      R_arup = R8_efe
C FDB50_vent_log.fgi( 289, 401):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umup,R_usup,REAL(1,4
     &),
     & REAL(R_arup,4),I_osup,REAL(R_ipup,4),
     & REAL(R_opup,4),REAL(R_omup,4),
     & REAL(R_imup,4),REAL(R_irup,4),L_orup,REAL(R_urup,4
     &),L_asup,
     & L_esup,R_upup,REAL(R_epup,4),REAL(R_apup,4),L_isup
     &)
      !}
C FDB50_vlv.fgi(  73, 192):���������� ������� ��� 2,20FDB50CQ006XQ01
      R_epef = R8_ife
C FDB50_vent_log.fgi( 289, 405):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_amef,R_asef,REAL(1,4
     &),
     & REAL(R_epef,4),I_uref,REAL(R_omef,4),
     & REAL(R_umef,4),REAL(R_ulef,4),
     & REAL(R_olef,4),REAL(R_opef,4),L_upef,REAL(R_aref,4
     &),L_eref,
     & L_iref,R_apef,REAL(R_imef,4),REAL(R_emef,4),L_oref
     &)
      !}
C FDB50_vlv.fgi( 331, 197):���������� ������� ��� 2,20FDB50CQ005XQ01
      R_ikof = R8_ofe
C FDB50_vent_log.fgi( 289, 409):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efof,R_emof,REAL(1,4
     &),
     & REAL(R_ikof,4),I_amof,REAL(R_ufof,4),
     & REAL(R_akof,4),REAL(R_afof,4),
     & REAL(R_udof,4),REAL(R_ukof,4),L_alof,REAL(R_elof,4
     &),L_ilof,
     & L_olof,R_ekof,REAL(R_ofof,4),REAL(R_ifof,4),L_ulof
     &)
      !}
C FDB50_vlv.fgi( 243, 197):���������� ������� ��� 2,20FDB50CQ004XQ01
      R_itar = R8_ufe
C FDB50_vent_log.fgi( 289, 413):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_esar,R_exar,REAL(1,4
     &),
     & REAL(R_itar,4),I_axar,REAL(R_usar,4),
     & REAL(R_atar,4),REAL(R_asar,4),
     & REAL(R_urar,4),REAL(R_utar,4),L_avar,REAL(R_evar,4
     &),L_ivar,
     & L_ovar,R_etar,REAL(R_osar,4),REAL(R_isar,4),L_uvar
     &)
      !}
C FDB50_vlv.fgi( 100, 177):���������� ������� ��� 2,20FDB50CQ003XQ01
      R_oler = R8_ake
C FDB50_vent_log.fgi( 289, 417):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_iker,R_iper,REAL(1,4
     &),
     & REAL(R_oler,4),I_eper,REAL(R_aler,4),
     & REAL(R_eler,4),REAL(R_eker,4),
     & REAL(R_aker,4),REAL(R_amer,4),L_emer,REAL(R_imer,4
     &),L_omer,
     & L_umer,R_iler,REAL(R_uker,4),REAL(R_oker,4),L_aper
     &)
      !}
C FDB50_vlv.fgi( 100, 207):���������� ������� ��� 2,20FDB50CQ002XQ01
      R_ikir = R8_eke
C FDB50_vent_log.fgi( 289, 421):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efir,R_emir,REAL(1,4
     &),
     & REAL(R_ikir,4),I_amir,REAL(R_ufir,4),
     & REAL(R_akir,4),REAL(R_afir,4),
     & REAL(R_udir,4),REAL(R_ukir,4),L_alir,REAL(R_elir,4
     &),L_ilir,
     & L_olir,R_ekir,REAL(R_ofir,4),REAL(R_ifir,4),L_ulir
     &)
      !}
C FDB50_vlv.fgi( 100, 237):���������� ������� ��� 2,20FDB50CQ001XQ01
      R_efuf = R8_ike
C FDB50_vent_log.fgi(  31, 277):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_aduf,R_aluf,REAL(1,4
     &),
     & REAL(R_efuf,4),I_ukuf,REAL(R_oduf,4),
     & REAL(R_uduf,4),REAL(R_ubuf,4),
     & REAL(R_obuf,4),REAL(R_ofuf,4),L_ufuf,REAL(R_akuf,4
     &),L_ekuf,
     & L_ikuf,R_afuf,REAL(R_iduf,4),REAL(R_eduf,4),L_okuf
     &)
      !}
C FDB50_vlv.fgi( 273, 212):���������� ������� ��� 2,20FDB50DT011XQ01
      R_ituf = R8_oke
C FDB50_vent_log.fgi(  31, 281):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_esuf,R_exuf,REAL(1,4
     &),
     & REAL(R_ituf,4),I_axuf,REAL(R_usuf,4),
     & REAL(R_atuf,4),REAL(R_asuf,4),
     & REAL(R_uruf,4),REAL(R_utuf,4),L_avuf,REAL(R_evuf,4
     &),L_ivuf,
     & L_ovuf,R_etuf,REAL(R_osuf,4),REAL(R_isuf,4),L_uvuf
     &)
      !}
C FDB50_vlv.fgi( 331, 227):���������� ������� ��� 2,20FDB50DT010XQ01
      R_olak = R8_uke
C FDB50_vent_log.fgi(  31, 284):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ikak,R_ipak,REAL(1,4
     &),
     & REAL(R_olak,4),I_epak,REAL(R_alak,4),
     & REAL(R_elak,4),REAL(R_ekak,4),
     & REAL(R_akak,4),REAL(R_amak,4),L_emak,REAL(R_imak,4
     &),L_omak,
     & L_umak,R_ilak,REAL(R_ukak,4),REAL(R_okak,4),L_apak
     &)
      !}
C FDB50_vlv.fgi( 302, 227):���������� ������� ��� 2,20FDB50DT009XQ01
      R_uxak = R8_ale
C FDB50_vent_log.fgi(  31, 287):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovak,R_odek,REAL(1,4
     &),
     & REAL(R_uxak,4),I_idek,REAL(R_exak,4),
     & REAL(R_ixak,4),REAL(R_ivak,4),
     & REAL(R_evak,4),REAL(R_ebek,4),L_ibek,REAL(R_obek,4
     &),L_ubek,
     & L_adek,R_oxak,REAL(R_axak,4),REAL(R_uvak,4),L_edek
     &)
      !}
C FDB50_vlv.fgi( 273, 242):���������� ������� ��� 2,20FDB50DT008XQ01
      R_arek = R8_ele
C FDB50_vent_log.fgi(  31, 290):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umek,R_usek,REAL(1,4
     &),
     & REAL(R_arek,4),I_osek,REAL(R_ipek,4),
     & REAL(R_opek,4),REAL(R_omek,4),
     & REAL(R_imek,4),REAL(R_irek,4),L_orek,REAL(R_urek,4
     &),L_asek,
     & L_esek,R_upek,REAL(R_epek,4),REAL(R_apek,4),L_isek
     &)
      !}
C FDB50_vlv.fgi( 243, 242):���������� ������� ��� 2,20FDB50DT007XQ01
      R_efik = R8_ile
C FDB50_vent_log.fgi(  31, 294):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_adik,R_alik,REAL(1,4
     &),
     & REAL(R_efik,4),I_ukik,REAL(R_odik,4),
     & REAL(R_udik,4),REAL(R_ubik,4),
     & REAL(R_obik,4),REAL(R_ofik,4),L_ufik,REAL(R_akik,4
     &),L_ekik,
     & L_ikik,R_afik,REAL(R_idik,4),REAL(R_edik,4),L_okik
     &)
      !}
C FDB50_vlv.fgi( 273, 273):���������� ������� ��� 2,20FDB50DT006XQ01
      R_itik = R8_ole
C FDB50_vent_log.fgi(  31, 298):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_esik,R_exik,REAL(1,4
     &),
     & REAL(R_itik,4),I_axik,REAL(R_usik,4),
     & REAL(R_atik,4),REAL(R_asik,4),
     & REAL(R_urik,4),REAL(R_utik,4),L_avik,REAL(R_evik,4
     &),L_ivik,
     & L_ovik,R_etik,REAL(R_osik,4),REAL(R_isik,4),L_uvik
     &)
      !}
C FDB50_vlv.fgi( 243, 273):���������� ������� ��� 2,20FDB50DT005XQ01
      R_olok = R8_ule
C FDB50_vent_log.fgi(  31, 301):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ikok,R_ipok,REAL(1,4
     &),
     & REAL(R_olok,4),I_epok,REAL(R_alok,4),
     & REAL(R_elok,4),REAL(R_ekok,4),
     & REAL(R_akok,4),REAL(R_amok,4),L_emok,REAL(R_imok,4
     &),L_omok,
     & L_umok,R_ilok,REAL(R_ukok,4),REAL(R_okok,4),L_apok
     &)
      !}
C FDB50_vlv.fgi( 214, 242):���������� ������� ��� 2,20FDB50DT004XQ01
      R_uxok = R8_ame
C FDB50_vent_log.fgi(  31, 304):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovok,R_oduk,REAL(1,4
     &),
     & REAL(R_uxok,4),I_iduk,REAL(R_exok,4),
     & REAL(R_ixok,4),REAL(R_ivok,4),
     & REAL(R_evok,4),REAL(R_ebuk,4),L_ibuk,REAL(R_obuk,4
     &),L_ubuk,
     & L_aduk,R_oxok,REAL(R_axok,4),REAL(R_uvok,4),L_eduk
     &)
      !}
C FDB50_vlv.fgi( 185, 242):���������� ������� ��� 2,20FDB50DT003XQ01
      R_aruk = R8_eme
C FDB50_vent_log.fgi(  31, 307):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umuk,R_usuk,REAL(1,4
     &),
     & REAL(R_aruk,4),I_osuk,REAL(R_ipuk,4),
     & REAL(R_opuk,4),REAL(R_omuk,4),
     & REAL(R_imuk,4),REAL(R_iruk,4),L_oruk,REAL(R_uruk,4
     &),L_asuk,
     & L_esuk,R_upuk,REAL(R_epuk,4),REAL(R_apuk,4),L_isuk
     &)
      !}
C FDB50_vlv.fgi( 156, 242):���������� ������� ��� 2,20FDB50DT002XQ01
      R_efal = R8_ime
C FDB50_vent_log.fgi(  31, 310):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_adal,R_alal,REAL(1,4
     &),
     & REAL(R_efal,4),I_ukal,REAL(R_odal,4),
     & REAL(R_udal,4),REAL(R_ubal,4),
     & REAL(R_obal,4),REAL(R_ofal,4),L_ufal,REAL(R_akal,4
     &),L_ekal,
     & L_ikal,R_afal,REAL(R_idal,4),REAL(R_edal,4),L_okal
     &)
      !}
C FDB50_vlv.fgi( 127, 242):���������� ������� ��� 2,20FDB50DT001XQ01
      if(R8_ome.ge.0.0) then
         R_(6)=R8_ume/max(R8_ome,1.0e-10)
      else
         R_(6)=R8_ume/min(R8_ome,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 209, 402):�������� ����������
      R_udud = R_(6) * R_(5)
C FDB50_vent_log.fgi( 222, 401):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_obud,R_okud,REAL(1,4
     &),
     & REAL(R_udud,4),I_ikud,REAL(R_edud,4),
     & REAL(R_idud,4),REAL(R_ibud,4),
     & REAL(R_ebud,4),REAL(R_efud,4),L_ifud,REAL(R_ofud,4
     &),L_ufud,
     & L_akud,R_odud,REAL(R_adud,4),REAL(R_ubud,4),L_ekud
     &)
      !}
C FDB50_vlv.fgi(  46, 222):���������� ������� ��� 2,20FDB50CF021XQ01
      if(R8_ape.ge.0.0) then
         R_(8)=R8_epe/max(R8_ape,1.0e-10)
      else
         R_(8)=R8_epe/min(R8_ape,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 209, 411):�������� ����������
      R_eser = R_(8) * R_(7)
C FDB50_vent_log.fgi( 222, 410):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_arer,R_aver,REAL(1,4
     &),
     & REAL(R_eser,4),I_uter,REAL(R_orer,4),
     & REAL(R_urer,4),REAL(R_uper,4),
     & REAL(R_oper,4),REAL(R_oser,4),L_user,REAL(R_ater,4
     &),L_eter,
     & L_iter,R_aser,REAL(R_irer,4),REAL(R_erer,4),L_oter
     &)
      !}
C FDB50_vlv.fgi(  73, 237):���������� ������� ��� 2,20FDB50CF020XQ01
      if(R8_ipe.ge.0.0) then
         R_(10)=R8_ope/max(R8_ipe,1.0e-10)
      else
         R_(10)=R8_ope/min(R8_ipe,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 209, 422):�������� ����������
      R_arir = R_(10) * R_(9)
C FDB50_vent_log.fgi( 222, 421):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umir,R_usir,REAL(1,4
     &),
     & REAL(R_arir,4),I_osir,REAL(R_ipir,4),
     & REAL(R_opir,4),REAL(R_omir,4),
     & REAL(R_imir,4),REAL(R_irir,4),L_orir,REAL(R_urir,4
     &),L_asir,
     & L_esir,R_upir,REAL(R_epir,4),REAL(R_apir,4),L_isir
     &)
      !}
C FDB50_vlv.fgi(  73, 252):���������� ������� ��� 2,20FDB50CF019XQ01
      if(R8_upe.ge.0.0) then
         R_(12)=R8_are/max(R8_upe,1.0e-10)
      else
         R_(12)=R8_are/min(R8_upe,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 255):�������� ����������
      R_esul = R_(12) * R_(11)
C FDB50_vent_log.fgi( 151, 254):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_arul,R_avul,REAL(1,4
     &),
     & REAL(R_esul,4),I_utul,REAL(R_orul,4),
     & REAL(R_urul,4),REAL(R_upul,4),
     & REAL(R_opul,4),REAL(R_osul,4),L_usul,REAL(R_atul,4
     &),L_etul,
     & L_itul,R_asul,REAL(R_irul,4),REAL(R_erul,4),L_otul
     &)
      !}
C FDB50_vlv.fgi( 273, 289):���������� ������� ��� 2,20FDB50CU025XQ01
      if(R8_ere.ge.0.0) then
         R_(14)=R8_ire/max(R8_ere,1.0e-10)
      else
         R_(14)=R8_ire/min(R8_ere,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 263):�������� ����������
      R_ikam = R_(14) * R_(13)
C FDB50_vent_log.fgi( 151, 262):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efam,R_emam,REAL(1,4
     &),
     & REAL(R_ikam,4),I_amam,REAL(R_ufam,4),
     & REAL(R_akam,4),REAL(R_afam,4),
     & REAL(R_udam,4),REAL(R_ukam,4),L_alam,REAL(R_elam,4
     &),L_ilam,
     & L_olam,R_ekam,REAL(R_ofam,4),REAL(R_ifam,4),L_ulam
     &)
      !}
C FDB50_vlv.fgi( 273, 305):���������� ������� ��� 2,20FDB50CU024XQ01
      if(R8_ore.ge.0.0) then
         R_(16)=R8_ure/max(R8_ore,1.0e-10)
      else
         R_(16)=R8_ure/min(R8_ore,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 270):�������� ����������
      R_ovam = R_(16) * R_(15)
C FDB50_vent_log.fgi( 151, 269):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itam,R_ibem,REAL(1,4
     &),
     & REAL(R_ovam,4),I_ebem,REAL(R_avam,4),
     & REAL(R_evam,4),REAL(R_etam,4),
     & REAL(R_atam,4),REAL(R_axam,4),L_exam,REAL(R_ixam,4
     &),L_oxam,
     & L_uxam,R_ivam,REAL(R_utam,4),REAL(R_otam,4),L_abem
     &)
      !}
C FDB50_vlv.fgi( 273, 321):���������� ������� ��� 2,20FDB50CU023XQ01
      if(R8_ase.ge.0.0) then
         R_(18)=R8_ese/max(R8_ase,1.0e-10)
      else
         R_(18)=R8_ese/min(R8_ase,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 278):�������� ����������
      R_umem = R_(18) * R_(17)
C FDB50_vent_log.fgi( 151, 277):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_olem,R_orem,REAL(1,4
     &),
     & REAL(R_umem,4),I_irem,REAL(R_emem,4),
     & REAL(R_imem,4),REAL(R_ilem,4),
     & REAL(R_elem,4),REAL(R_epem,4),L_ipem,REAL(R_opem,4
     &),L_upem,
     & L_arem,R_omem,REAL(R_amem,4),REAL(R_ulem,4),L_erem
     &)
      !}
C FDB50_vlv.fgi( 273, 337):���������� ������� ��� 2,20FDB50CU022XQ01
      if(R8_ise.ge.0.0) then
         R_(20)=R8_ose/max(R8_ise,1.0e-10)
      else
         R_(20)=R8_ose/min(R8_ise,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 286):�������� ����������
      R_adim = R_(20) * R_(19)
C FDB50_vent_log.fgi( 151, 285):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uxem,R_ufim,REAL(1,4
     &),
     & REAL(R_adim,4),I_ofim,REAL(R_ibim,4),
     & REAL(R_obim,4),REAL(R_oxem,4),
     & REAL(R_ixem,4),REAL(R_idim,4),L_odim,REAL(R_udim,4
     &),L_afim,
     & L_efim,R_ubim,REAL(R_ebim,4),REAL(R_abim,4),L_ifim
     &)
      !}
C FDB50_vlv.fgi( 214, 257):���������� ������� ��� 2,20FDB50CU021XQ01
      if(R8_use.ge.0.0) then
         R_(22)=R8_ate/max(R8_use,1.0e-10)
      else
         R_(22)=R8_ate/min(R8_use,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 293):�������� ����������
      R_esim = R_(22) * R_(21)
C FDB50_vent_log.fgi( 151, 292):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_arim,R_avim,REAL(1,4
     &),
     & REAL(R_esim,4),I_utim,REAL(R_orim,4),
     & REAL(R_urim,4),REAL(R_upim,4),
     & REAL(R_opim,4),REAL(R_osim,4),L_usim,REAL(R_atim,4
     &),L_etim,
     & L_itim,R_asim,REAL(R_irim,4),REAL(R_erim,4),L_otim
     &)
      !}
C FDB50_vlv.fgi( 214, 273):���������� ������� ��� 2,20FDB50CU020XQ01
      if(R8_ete.ge.0.0) then
         R_(24)=R8_ite/max(R8_ete,1.0e-10)
      else
         R_(24)=R8_ite/min(R8_ete,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 301):�������� ����������
      R_ikom = R_(24) * R_(23)
C FDB50_vent_log.fgi( 151, 300):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efom,R_emom,REAL(1,4
     &),
     & REAL(R_ikom,4),I_amom,REAL(R_ufom,4),
     & REAL(R_akom,4),REAL(R_afom,4),
     & REAL(R_udom,4),REAL(R_ukom,4),L_alom,REAL(R_elom,4
     &),L_ilom,
     & L_olom,R_ekom,REAL(R_ofom,4),REAL(R_ifom,4),L_ulom
     &)
      !}
C FDB50_vlv.fgi( 214, 289):���������� ������� ��� 2,20FDB50CU019XQ01
      if(R8_ote.ge.0.0) then
         R_(26)=R8_ute/max(R8_ote,1.0e-10)
      else
         R_(26)=R8_ute/min(R8_ote,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 308):�������� ����������
      R_ovom = R_(26) * R_(25)
C FDB50_vent_log.fgi( 151, 307):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itom,R_ibum,REAL(1,4
     &),
     & REAL(R_ovom,4),I_ebum,REAL(R_avom,4),
     & REAL(R_evom,4),REAL(R_etom,4),
     & REAL(R_atom,4),REAL(R_axom,4),L_exom,REAL(R_ixom,4
     &),L_oxom,
     & L_uxom,R_ivom,REAL(R_utom,4),REAL(R_otom,4),L_abum
     &)
      !}
C FDB50_vlv.fgi( 214, 305):���������� ������� ��� 2,20FDB50CU018XQ01
      if(R8_ave.ge.0.0) then
         R_(28)=R8_eve/max(R8_ave,1.0e-10)
      else
         R_(28)=R8_eve/min(R8_ave,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 315):�������� ����������
      R_umum = R_(28) * R_(27)
C FDB50_vent_log.fgi( 151, 314):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_olum,R_orum,REAL(1,4
     &),
     & REAL(R_umum,4),I_irum,REAL(R_emum,4),
     & REAL(R_imum,4),REAL(R_ilum,4),
     & REAL(R_elum,4),REAL(R_epum,4),L_ipum,REAL(R_opum,4
     &),L_upum,
     & L_arum,R_omum,REAL(R_amum,4),REAL(R_ulum,4),L_erum
     &)
      !}
C FDB50_vlv.fgi( 214, 321):���������� ������� ��� 2,20FDB50CU017XQ01
      if(R8_ive.ge.0.0) then
         R_(30)=R8_ove/max(R8_ive,1.0e-10)
      else
         R_(30)=R8_ove/min(R8_ive,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 322):�������� ����������
      R_adap = R_(30) * R_(29)
C FDB50_vent_log.fgi( 151, 321):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uxum,R_ufap,REAL(1,4
     &),
     & REAL(R_adap,4),I_ofap,REAL(R_ibap,4),
     & REAL(R_obap,4),REAL(R_oxum,4),
     & REAL(R_ixum,4),REAL(R_idap,4),L_odap,REAL(R_udap,4
     &),L_afap,
     & L_efap,R_ubap,REAL(R_ebap,4),REAL(R_abap,4),L_ifap
     &)
      !}
C FDB50_vlv.fgi( 214, 337):���������� ������� ��� 2,20FDB50CU016XQ01
      if(R8_uve.ge.0.0) then
         R_(32)=R8_axe/max(R8_uve,1.0e-10)
      else
         R_(32)=R8_axe/min(R8_uve,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 329):�������� ����������
      R_esap = R_(32) * R_(31)
C FDB50_vent_log.fgi( 151, 328):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_arap,R_avap,REAL(1,4
     &),
     & REAL(R_esap,4),I_utap,REAL(R_orap,4),
     & REAL(R_urap,4),REAL(R_upap,4),
     & REAL(R_opap,4),REAL(R_osap,4),L_usap,REAL(R_atap,4
     &),L_etap,
     & L_itap,R_asap,REAL(R_irap,4),REAL(R_erap,4),L_otap
     &)
      !}
C FDB50_vlv.fgi( 156, 257):���������� ������� ��� 2,20FDB50CU015XQ01
      if(R8_exe.ge.0.0) then
         R_(34)=R8_ixe/max(R8_exe,1.0e-10)
      else
         R_(34)=R8_ixe/min(R8_exe,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 337):�������� ����������
      R_ikep = R_(34) * R_(33)
C FDB50_vent_log.fgi( 151, 336):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efep,R_emep,REAL(1,4
     &),
     & REAL(R_ikep,4),I_amep,REAL(R_ufep,4),
     & REAL(R_akep,4),REAL(R_afep,4),
     & REAL(R_udep,4),REAL(R_ukep,4),L_alep,REAL(R_elep,4
     &),L_ilep,
     & L_olep,R_ekep,REAL(R_ofep,4),REAL(R_ifep,4),L_ulep
     &)
      !}
C FDB50_vlv.fgi( 156, 273):���������� ������� ��� 2,20FDB50CU014XQ01
      if(R8_oxe.ge.0.0) then
         R_(36)=R8_uxe/max(R8_oxe,1.0e-10)
      else
         R_(36)=R8_uxe/min(R8_oxe,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 345):�������� ����������
      R_ovep = R_(36) * R_(35)
C FDB50_vent_log.fgi( 151, 344):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itep,R_ibip,REAL(1,4
     &),
     & REAL(R_ovep,4),I_ebip,REAL(R_avep,4),
     & REAL(R_evep,4),REAL(R_etep,4),
     & REAL(R_atep,4),REAL(R_axep,4),L_exep,REAL(R_ixep,4
     &),L_oxep,
     & L_uxep,R_ivep,REAL(R_utep,4),REAL(R_otep,4),L_abip
     &)
      !}
C FDB50_vlv.fgi( 156, 289):���������� ������� ��� 2,20FDB50CU013XQ01
      if(R8_abi.ge.0.0) then
         R_(38)=R8_ebi/max(R8_abi,1.0e-10)
      else
         R_(38)=R8_ebi/min(R8_abi,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 352):�������� ����������
      R_umip = R_(38) * R_(37)
C FDB50_vent_log.fgi( 151, 351):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_olip,R_orip,REAL(1,4
     &),
     & REAL(R_umip,4),I_irip,REAL(R_emip,4),
     & REAL(R_imip,4),REAL(R_ilip,4),
     & REAL(R_elip,4),REAL(R_epip,4),L_ipip,REAL(R_opip,4
     &),L_upip,
     & L_arip,R_omip,REAL(R_amip,4),REAL(R_ulip,4),L_erip
     &)
      !}
C FDB50_vlv.fgi( 156, 305):���������� ������� ��� 2,20FDB50CU012XQ01
      if(R8_ibi.ge.0.0) then
         R_(40)=R8_obi/max(R8_ibi,1.0e-10)
      else
         R_(40)=R8_obi/min(R8_ibi,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 359):�������� ����������
      R_adop = R_(40) * R_(39)
C FDB50_vent_log.fgi( 151, 358):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uxip,R_ufop,REAL(1,4
     &),
     & REAL(R_adop,4),I_ofop,REAL(R_ibop,4),
     & REAL(R_obop,4),REAL(R_oxip,4),
     & REAL(R_ixip,4),REAL(R_idop,4),L_odop,REAL(R_udop,4
     &),L_afop,
     & L_efop,R_ubop,REAL(R_ebop,4),REAL(R_abop,4),L_ifop
     &)
      !}
C FDB50_vlv.fgi( 156, 321):���������� ������� ��� 2,20FDB50CU011XQ01
      R_olop = R8_ubi
C FDB50_vent_log.fgi(  85, 389):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ikop,R_ipop,REAL(1,4
     &),
     & REAL(R_olop,4),I_epop,REAL(R_alop,4),
     & REAL(R_elop,4),REAL(R_ekop,4),
     & REAL(R_akop,4),REAL(R_amop,4),L_emop,REAL(R_imop,4
     &),L_omop,
     & L_umop,R_ilop,REAL(R_ukop,4),REAL(R_okop,4),L_apop
     &)
      !}
C FDB50_vlv.fgi( 127, 321):���������� ������� ��� 2,20FDB50CU011XQ02
      if(R8_adi.ge.0.0) then
         R_(42)=R8_edi/max(R8_adi,1.0e-10)
      else
         R_(42)=R8_edi/min(R8_adi,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 367):�������� ����������
      R_esop = R_(42) * R_(41)
C FDB50_vent_log.fgi( 151, 366):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_arop,R_avop,REAL(1,4
     &),
     & REAL(R_esop,4),I_utop,REAL(R_orop,4),
     & REAL(R_urop,4),REAL(R_upop,4),
     & REAL(R_opop,4),REAL(R_osop,4),L_usop,REAL(R_atop,4
     &),L_etop,
     & L_itop,R_asop,REAL(R_irop,4),REAL(R_erop,4),L_otop
     &)
      !}
C FDB50_vlv.fgi( 156, 337):���������� ������� ��� 2,20FDB50CU010XQ01
      if(R8_idi.ge.0.0) then
         R_(44)=R8_odi/max(R8_idi,1.0e-10)
      else
         R_(44)=R8_odi/min(R8_idi,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 375):�������� ����������
      R_umal = R_(44) * R_(43)
C FDB50_vent_log.fgi( 151, 374):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_olal,R_oral,REAL(1,4
     &),
     & REAL(R_umal,4),I_iral,REAL(R_emal,4),
     & REAL(R_imal,4),REAL(R_ilal,4),
     & REAL(R_elal,4),REAL(R_epal,4),L_ipal,REAL(R_opal,4
     &),L_upal,
     & L_aral,R_omal,REAL(R_amal,4),REAL(R_ulal,4),L_eral
     &)
      !}
C FDB50_vlv.fgi( 331, 242):���������� ������� ��� 2,20FDB50CU007XQ01
      if(R8_udi.ge.0.0) then
         R_(46)=R8_afi/max(R8_udi,1.0e-10)
      else
         R_(46)=R8_afi/min(R8_udi,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 383):�������� ����������
      R_adel = R_(46) * R_(45)
C FDB50_vent_log.fgi( 151, 382):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uxal,R_ufel,REAL(1,4
     &),
     & REAL(R_adel,4),I_ofel,REAL(R_ibel,4),
     & REAL(R_obel,4),REAL(R_oxal,4),
     & REAL(R_ixal,4),REAL(R_idel,4),L_odel,REAL(R_udel,4
     &),L_afel,
     & L_efel,R_ubel,REAL(R_ebel,4),REAL(R_abel,4),L_ifel
     &)
      !}
C FDB50_vlv.fgi( 331, 258):���������� ������� ��� 2,20FDB50CU006XQ01
      if(R8_efi.ge.0.0) then
         R_(48)=R8_ifi/max(R8_efi,1.0e-10)
      else
         R_(48)=R8_ifi/min(R8_efi,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 391):�������� ����������
      R_esel = R_(48) * R_(47)
C FDB50_vent_log.fgi( 151, 390):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_arel,R_avel,REAL(1,4
     &),
     & REAL(R_esel,4),I_utel,REAL(R_orel,4),
     & REAL(R_urel,4),REAL(R_upel,4),
     & REAL(R_opel,4),REAL(R_osel,4),L_usel,REAL(R_atel,4
     &),L_etel,
     & L_itel,R_asel,REAL(R_irel,4),REAL(R_erel,4),L_otel
     &)
      !}
C FDB50_vlv.fgi( 331, 273):���������� ������� ��� 2,20FDB50CU005XQ01
      if(R8_ofi.ge.0.0) then
         R_(50)=R8_ufi/max(R8_ofi,1.0e-10)
      else
         R_(50)=R8_ufi/min(R8_ofi,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 398):�������� ����������
      R_ikil = R_(50) * R_(49)
C FDB50_vent_log.fgi( 151, 397):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_efil,R_emil,REAL(1,4
     &),
     & REAL(R_ikil,4),I_amil,REAL(R_ufil,4),
     & REAL(R_akil,4),REAL(R_afil,4),
     & REAL(R_udil,4),REAL(R_ukil,4),L_alil,REAL(R_elil,4
     &),L_ilil,
     & L_olil,R_ekil,REAL(R_ofil,4),REAL(R_ifil,4),L_ulil
     &)
      !}
C FDB50_vlv.fgi( 331, 289):���������� ������� ��� 2,20FDB50CU004XQ01
      if(R8_aki.ge.0.0) then
         R_(52)=R8_eki/max(R8_aki,1.0e-10)
      else
         R_(52)=R8_eki/min(R8_aki,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 406):�������� ����������
      R_ovil = R_(52) * R_(51)
C FDB50_vent_log.fgi( 151, 405):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_itil,R_ibol,REAL(1,4
     &),
     & REAL(R_ovil,4),I_ebol,REAL(R_avil,4),
     & REAL(R_evil,4),REAL(R_etil,4),
     & REAL(R_atil,4),REAL(R_axil,4),L_exil,REAL(R_ixil,4
     &),L_oxil,
     & L_uxil,R_ivil,REAL(R_util,4),REAL(R_otil,4),L_abol
     &)
      !}
C FDB50_vlv.fgi( 331, 305):���������� ������� ��� 2,20FDB50CU003XQ01
      if(R8_iki.ge.0.0) then
         R_(54)=R8_oki/max(R8_iki,1.0e-10)
      else
         R_(54)=R8_oki/min(R8_iki,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 414):�������� ����������
      R_umol = R_(54) * R_(53)
C FDB50_vent_log.fgi( 151, 413):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_olol,R_orol,REAL(1,4
     &),
     & REAL(R_umol,4),I_irol,REAL(R_emol,4),
     & REAL(R_imol,4),REAL(R_ilol,4),
     & REAL(R_elol,4),REAL(R_epol,4),L_ipol,REAL(R_opol,4
     &),L_upol,
     & L_arol,R_omol,REAL(R_amol,4),REAL(R_ulol,4),L_erol
     &)
      !}
C FDB50_vlv.fgi( 331, 321):���������� ������� ��� 2,20FDB50CU002XQ01
      if(R8_uki.ge.0.0) then
         R_(56)=R8_ali/max(R8_uki,1.0e-10)
      else
         R_(56)=R8_ali/min(R8_uki,-1.0e-10)
      endif
C FDB50_vent_log.fgi( 138, 422):�������� ����������
      R_adul = R_(56) * R_(55)
C FDB50_vent_log.fgi( 151, 421):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uxol,R_uful,REAL(1,4
     &),
     & REAL(R_adul,4),I_oful,REAL(R_ibul,4),
     & REAL(R_obul,4),REAL(R_oxol,4),
     & REAL(R_ixol,4),REAL(R_idul,4),L_odul,REAL(R_udul,4
     &),L_aful,
     & L_eful,R_ubul,REAL(R_ebul,4),REAL(R_abul,4),L_iful
     &)
      !}
C FDB50_vlv.fgi( 331, 337):���������� ������� ��� 2,20FDB50CU001XQ01
      R_uxul = R8_eli
C FDB50_vent_log.fgi(  85, 345):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovul,R_odam,REAL(1,4
     &),
     & REAL(R_uxul,4),I_idam,REAL(R_exul,4),
     & REAL(R_ixul,4),REAL(R_ivul,4),
     & REAL(R_evul,4),REAL(R_ebam,4),L_ibam,REAL(R_obam,4
     &),L_ubam,
     & L_adam,R_oxul,REAL(R_axul,4),REAL(R_uvul,4),L_edam
     &)
      !}
C FDB50_vlv.fgi( 244, 289):���������� ������� ��� 2,20FDB50CU025XQ02
      R_aram = R8_ili
C FDB50_vent_log.fgi(  85, 348):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umam,R_usam,REAL(1,4
     &),
     & REAL(R_aram,4),I_osam,REAL(R_ipam,4),
     & REAL(R_opam,4),REAL(R_omam,4),
     & REAL(R_imam,4),REAL(R_iram,4),L_oram,REAL(R_uram,4
     &),L_asam,
     & L_esam,R_upam,REAL(R_epam,4),REAL(R_apam,4),L_isam
     &)
      !}
C FDB50_vlv.fgi( 244, 305):���������� ������� ��� 2,20FDB50CU024XQ02
      R_efem = R8_oli
C FDB50_vent_log.fgi(  85, 351):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_adem,R_alem,REAL(1,4
     &),
     & REAL(R_efem,4),I_ukem,REAL(R_odem,4),
     & REAL(R_udem,4),REAL(R_ubem,4),
     & REAL(R_obem,4),REAL(R_ofem,4),L_ufem,REAL(R_akem,4
     &),L_ekem,
     & L_ikem,R_afem,REAL(R_idem,4),REAL(R_edem,4),L_okem
     &)
      !}
C FDB50_vlv.fgi( 244, 321):���������� ������� ��� 2,20FDB50CU023XQ02
      R_item = R8_uli
C FDB50_vent_log.fgi(  85, 354):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_esem,R_exem,REAL(1,4
     &),
     & REAL(R_item,4),I_axem,REAL(R_usem,4),
     & REAL(R_atem,4),REAL(R_asem,4),
     & REAL(R_urem,4),REAL(R_utem,4),L_avem,REAL(R_evem,4
     &),L_ivem,
     & L_ovem,R_etem,REAL(R_osem,4),REAL(R_isem,4),L_uvem
     &)
      !}
C FDB50_vlv.fgi( 244, 337):���������� ������� ��� 2,20FDB50CU022XQ02
      R_olim = R8_ami
C FDB50_vent_log.fgi(  85, 357):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ikim,R_ipim,REAL(1,4
     &),
     & REAL(R_olim,4),I_epim,REAL(R_alim,4),
     & REAL(R_elim,4),REAL(R_ekim,4),
     & REAL(R_akim,4),REAL(R_amim,4),L_emim,REAL(R_imim,4
     &),L_omim,
     & L_umim,R_ilim,REAL(R_ukim,4),REAL(R_okim,4),L_apim
     &)
      !}
C FDB50_vlv.fgi( 185, 257):���������� ������� ��� 2,20FDB50CU021XQ02
      R_uxim = R8_emi
C FDB50_vent_log.fgi(  85, 360):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovim,R_odom,REAL(1,4
     &),
     & REAL(R_uxim,4),I_idom,REAL(R_exim,4),
     & REAL(R_ixim,4),REAL(R_ivim,4),
     & REAL(R_evim,4),REAL(R_ebom,4),L_ibom,REAL(R_obom,4
     &),L_ubom,
     & L_adom,R_oxim,REAL(R_axim,4),REAL(R_uvim,4),L_edom
     &)
      !}
C FDB50_vlv.fgi( 185, 273):���������� ������� ��� 2,20FDB50CU020XQ02
      R_arom = R8_imi
C FDB50_vent_log.fgi(  85, 363):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umom,R_usom,REAL(1,4
     &),
     & REAL(R_arom,4),I_osom,REAL(R_ipom,4),
     & REAL(R_opom,4),REAL(R_omom,4),
     & REAL(R_imom,4),REAL(R_irom,4),L_orom,REAL(R_urom,4
     &),L_asom,
     & L_esom,R_upom,REAL(R_epom,4),REAL(R_apom,4),L_isom
     &)
      !}
C FDB50_vlv.fgi( 185, 289):���������� ������� ��� 2,20FDB50CU019XQ02
      R_efum = R8_omi
C FDB50_vent_log.fgi(  85, 366):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_adum,R_alum,REAL(1,4
     &),
     & REAL(R_efum,4),I_ukum,REAL(R_odum,4),
     & REAL(R_udum,4),REAL(R_ubum,4),
     & REAL(R_obum,4),REAL(R_ofum,4),L_ufum,REAL(R_akum,4
     &),L_ekum,
     & L_ikum,R_afum,REAL(R_idum,4),REAL(R_edum,4),L_okum
     &)
      !}
C FDB50_vlv.fgi( 185, 305):���������� ������� ��� 2,20FDB50CU018XQ02
      R_itum = R8_umi
C FDB50_vent_log.fgi(  85, 369):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_esum,R_exum,REAL(1,4
     &),
     & REAL(R_itum,4),I_axum,REAL(R_usum,4),
     & REAL(R_atum,4),REAL(R_asum,4),
     & REAL(R_urum,4),REAL(R_utum,4),L_avum,REAL(R_evum,4
     &),L_ivum,
     & L_ovum,R_etum,REAL(R_osum,4),REAL(R_isum,4),L_uvum
     &)
      !}
C FDB50_vlv.fgi( 185, 321):���������� ������� ��� 2,20FDB50CU017XQ02
      R_olap = R8_api
C FDB50_vent_log.fgi(  85, 372):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ikap,R_ipap,REAL(1,4
     &),
     & REAL(R_olap,4),I_epap,REAL(R_alap,4),
     & REAL(R_elap,4),REAL(R_ekap,4),
     & REAL(R_akap,4),REAL(R_amap,4),L_emap,REAL(R_imap,4
     &),L_omap,
     & L_umap,R_ilap,REAL(R_ukap,4),REAL(R_okap,4),L_apap
     &)
      !}
C FDB50_vlv.fgi( 185, 337):���������� ������� ��� 2,20FDB50CU016XQ02
      R_uxap = R8_epi
C FDB50_vent_log.fgi(  85, 375):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovap,R_odep,REAL(1,4
     &),
     & REAL(R_uxap,4),I_idep,REAL(R_exap,4),
     & REAL(R_ixap,4),REAL(R_ivap,4),
     & REAL(R_evap,4),REAL(R_ebep,4),L_ibep,REAL(R_obep,4
     &),L_ubep,
     & L_adep,R_oxap,REAL(R_axap,4),REAL(R_uvap,4),L_edep
     &)
      !}
C FDB50_vlv.fgi( 127, 257):���������� ������� ��� 2,20FDB50CU015XQ02
      R_arep = R8_ipi
C FDB50_vent_log.fgi(  85, 378):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umep,R_usep,REAL(1,4
     &),
     & REAL(R_arep,4),I_osep,REAL(R_ipep,4),
     & REAL(R_opep,4),REAL(R_omep,4),
     & REAL(R_imep,4),REAL(R_irep,4),L_orep,REAL(R_urep,4
     &),L_asep,
     & L_esep,R_upep,REAL(R_epep,4),REAL(R_apep,4),L_isep
     &)
      !}
C FDB50_vlv.fgi( 127, 273):���������� ������� ��� 2,20FDB50CU014XQ02
      R_efip = R8_opi
C FDB50_vent_log.fgi(  85, 381):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_adip,R_alip,REAL(1,4
     &),
     & REAL(R_efip,4),I_ukip,REAL(R_odip,4),
     & REAL(R_udip,4),REAL(R_ubip,4),
     & REAL(R_obip,4),REAL(R_ofip,4),L_ufip,REAL(R_akip,4
     &),L_ekip,
     & L_ikip,R_afip,REAL(R_idip,4),REAL(R_edip,4),L_okip
     &)
      !}
C FDB50_vlv.fgi( 127, 289):���������� ������� ��� 2,20FDB50CU013XQ02
      R_itip = R8_upi
C FDB50_vent_log.fgi(  85, 385):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_esip,R_exip,REAL(1,4
     &),
     & REAL(R_itip,4),I_axip,REAL(R_usip,4),
     & REAL(R_atip,4),REAL(R_asip,4),
     & REAL(R_urip,4),REAL(R_utip,4),L_avip,REAL(R_evip,4
     &),L_ivip,
     & L_ovip,R_etip,REAL(R_osip,4),REAL(R_isip,4),L_uvip
     &)
      !}
C FDB50_vlv.fgi( 127, 305):���������� ������� ��� 2,20FDB50CU012XQ02
      R_uxop = R8_ari
C FDB50_vent_log.fgi(  85, 392):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovop,R_odup,REAL(1,4
     &),
     & REAL(R_uxop,4),I_idup,REAL(R_exop,4),
     & REAL(R_ixop,4),REAL(R_ivop,4),
     & REAL(R_evop,4),REAL(R_ebup,4),L_ibup,REAL(R_obup,4
     &),L_ubup,
     & L_adup,R_oxop,REAL(R_axop,4),REAL(R_uvop,4),L_edup
     &)
      !}
C FDB50_vlv.fgi( 127, 337):���������� ������� ��� 2,20FDB50CU010XQ02
      R_ital = R8_eri
C FDB50_vent_log.fgi(  85, 396):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_esal,R_exal,REAL(1,4
     &),
     & REAL(R_ital,4),I_axal,REAL(R_usal,4),
     & REAL(R_atal,4),REAL(R_asal,4),
     & REAL(R_ural,4),REAL(R_utal,4),L_aval,REAL(R_eval,4
     &),L_ival,
     & L_oval,R_etal,REAL(R_osal,4),REAL(R_isal,4),L_uval
     &)
      !}
C FDB50_vlv.fgi( 302, 242):���������� ������� ��� 2,20FDB50CU007XQ02
      R_olel = R8_iri
C FDB50_vent_log.fgi(  85, 400):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ikel,R_ipel,REAL(1,4
     &),
     & REAL(R_olel,4),I_epel,REAL(R_alel,4),
     & REAL(R_elel,4),REAL(R_ekel,4),
     & REAL(R_akel,4),REAL(R_amel,4),L_emel,REAL(R_imel,4
     &),L_omel,
     & L_umel,R_ilel,REAL(R_ukel,4),REAL(R_okel,4),L_apel
     &)
      !}
C FDB50_vlv.fgi( 302, 258):���������� ������� ��� 2,20FDB50CU006XQ02
      R_uxel = R8_ori
C FDB50_vent_log.fgi(  85, 404):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ovel,R_odil,REAL(1,4
     &),
     & REAL(R_uxel,4),I_idil,REAL(R_exel,4),
     & REAL(R_ixel,4),REAL(R_ivel,4),
     & REAL(R_evel,4),REAL(R_ebil,4),L_ibil,REAL(R_obil,4
     &),L_ubil,
     & L_adil,R_oxel,REAL(R_axel,4),REAL(R_uvel,4),L_edil
     &)
      !}
C FDB50_vlv.fgi( 302, 273):���������� ������� ��� 2,20FDB50CU005XQ02
      R_aril = R8_uri
C FDB50_vent_log.fgi(  85, 408):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_umil,R_usil,REAL(1,4
     &),
     & REAL(R_aril,4),I_osil,REAL(R_ipil,4),
     & REAL(R_opil,4),REAL(R_omil,4),
     & REAL(R_imil,4),REAL(R_iril,4),L_oril,REAL(R_uril,4
     &),L_asil,
     & L_esil,R_upil,REAL(R_epil,4),REAL(R_apil,4),L_isil
     &)
      !}
C FDB50_vlv.fgi( 302, 289):���������� ������� ��� 2,20FDB50CU004XQ02
      R_efol = R8_asi
C FDB50_vent_log.fgi(  85, 414):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_adol,R_alol,REAL(1,4
     &),
     & REAL(R_efol,4),I_ukol,REAL(R_odol,4),
     & REAL(R_udol,4),REAL(R_ubol,4),
     & REAL(R_obol,4),REAL(R_ofol,4),L_ufol,REAL(R_akol,4
     &),L_ekol,
     & L_ikol,R_afol,REAL(R_idol,4),REAL(R_edol,4),L_okol
     &)
      !}
C FDB50_vlv.fgi( 302, 305):���������� ������� ��� 2,20FDB50CU003XQ02
      R_itol = R8_esi
C FDB50_vent_log.fgi(  85, 419):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_esol,R_exol,REAL(1,4
     &),
     & REAL(R_itol,4),I_axol,REAL(R_usol,4),
     & REAL(R_atol,4),REAL(R_asol,4),
     & REAL(R_urol,4),REAL(R_utol,4),L_avol,REAL(R_evol,4
     &),L_ivol,
     & L_ovol,R_etol,REAL(R_osol,4),REAL(R_isol,4),L_uvol
     &)
      !}
C FDB50_vlv.fgi( 302, 321):���������� ������� ��� 2,20FDB50CU002XQ02
      R_olul = R8_isi
C FDB50_vent_log.fgi(  85, 424):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ikul,R_ipul,REAL(1,4
     &),
     & REAL(R_olul,4),I_epul,REAL(R_alul,4),
     & REAL(R_elul,4),REAL(R_ekul,4),
     & REAL(R_akul,4),REAL(R_amul,4),L_emul,REAL(R_imul,4
     &),L_omul,
     & L_umul,R_ilul,REAL(R_ukul,4),REAL(R_okul,4),L_apul
     &)
      !}
C FDB50_vlv.fgi( 302, 337):���������� ������� ��� 2,20FDB50CU001XQ02
      R_asut = R8_osi
C FDB50_vent_log.fgi(  31, 315):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uput,R_utut,REAL(1,4
     &),
     & REAL(R_asut,4),I_otut,REAL(R_irut,4),
     & REAL(R_orut,4),REAL(R_oput,4),
     & REAL(R_iput,4),REAL(R_isut,4),L_osut,REAL(R_usut,4
     &),L_atut,
     & L_etut,R_urut,REAL(R_erut,4),REAL(R_arut,4),L_itut
     &)
      !}
C FDB50_vlv.fgi( 100, 295):���������� ������� ��� 2,20FDB50CT044XQ01
      R_omot = R8_usi
C FDB50_vent_log.fgi(  31, 318):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ilot,R_irot,REAL(1,4
     &),
     & REAL(R_omot,4),I_erot,REAL(R_amot,4),
     & REAL(R_emot,4),REAL(R_elot,4),
     & REAL(R_alot,4),REAL(R_apot,4),L_epot,REAL(R_ipot,4
     &),L_opot,
     & L_upot,R_imot,REAL(R_ulot,4),REAL(R_olot,4),L_arot
     &)
      !}
C FDB50_vlv.fgi( 100, 267):���������� ������� ��� 2,20FDB50CT043XQ01
      R_oxut = R8_ati
C FDB50_vent_log.fgi(  31, 321):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ivut,R_idav,REAL(1,4
     &),
     & REAL(R_oxut,4),I_edav,REAL(R_axut,4),
     & REAL(R_exut,4),REAL(R_evut,4),
     & REAL(R_avut,4),REAL(R_abav,4),L_ebav,REAL(R_ibav,4
     &),L_obav,
     & L_ubav,R_ixut,REAL(R_uvut,4),REAL(R_ovut,4),L_adav
     &)
      !}
C FDB50_vlv.fgi( 100, 309):���������� ������� ��� 2,20FDB50CT042XQ01
      R_etot = R8_eti
C FDB50_vent_log.fgi(  31, 324):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_asot,R_axot,REAL(1,4
     &),
     & REAL(R_etot,4),I_uvot,REAL(R_osot,4),
     & REAL(R_usot,4),REAL(R_urot,4),
     & REAL(R_orot,4),REAL(R_otot,4),L_utot,REAL(R_avot,4
     &),L_evot,
     & L_ivot,R_atot,REAL(R_isot,4),REAL(R_esot,4),L_ovot
     &)
      !}
C FDB50_vlv.fgi( 100, 281):���������� ������� ��� 2,20FDB50CT041XQ01
      R_ubut = R8_iti
C FDB50_vent_log.fgi(  31, 327):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_oxot,R_ofut,REAL(1,4
     &),
     & REAL(R_ubut,4),I_ifut,REAL(R_ebut,4),
     & REAL(R_ibut,4),REAL(R_ixot,4),
     & REAL(R_exot,4),REAL(R_edut,4),L_idut,REAL(R_odut,4
     &),L_udut,
     & L_afut,R_obut,REAL(R_abut,4),REAL(R_uxot,4),L_efut
     &)
      !}
C FDB50_vlv.fgi(  73, 267):���������� ������� ��� 2,20FDB50CT040XQ01
      R_ekav = R8_oti
C FDB50_vent_log.fgi(  31, 330):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_afav,R_amav,REAL(1,4
     &),
     & REAL(R_ekav,4),I_ulav,REAL(R_ofav,4),
     & REAL(R_ufav,4),REAL(R_udav,4),
     & REAL(R_odav,4),REAL(R_okav,4),L_ukav,REAL(R_alav,4
     &),L_elav,
     & L_ilav,R_akav,REAL(R_ifav,4),REAL(R_efav,4),L_olav
     &)
      !}
C FDB50_vlv.fgi( 100, 323):���������� ������� ��� 2,20FDB50CT039XQ01
      R_ilut = R8_uti
C FDB50_vent_log.fgi(  31, 333):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ekut,R_eput,REAL(1,4
     &),
     & REAL(R_ilut,4),I_aput,REAL(R_ukut,4),
     & REAL(R_alut,4),REAL(R_akut,4),
     & REAL(R_ufut,4),REAL(R_ulut,4),L_amut,REAL(R_emut,4
     &),L_imut,
     & L_omut,R_elut,REAL(R_okut,4),REAL(R_ikut,4),L_umut
     &)
      !}
C FDB50_vlv.fgi(  73, 281):���������� ������� ��� 2,20FDB50CT038XQ01
      R_upav = R8_avi
C FDB50_vent_log.fgi(  31, 337):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_omav,R_osav,REAL(1,4
     &),
     & REAL(R_upav,4),I_isav,REAL(R_epav,4),
     & REAL(R_ipav,4),REAL(R_imav,4),
     & REAL(R_emav,4),REAL(R_erav,4),L_irav,REAL(R_orav,4
     &),L_urav,
     & L_asav,R_opav,REAL(R_apav,4),REAL(R_umav,4),L_esav
     &)
      !}
C FDB50_vlv.fgi( 100, 337):���������� ������� ��� 2,20FDB50CT037XQ01
      R_isuke = R8_evi
C FDB50_vent_log.fgi(  31, 341):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_eruke,R_evuke,REAL(1
     &,4),
     & REAL(R_isuke,4),I_avuke,REAL(R_uruke,4),
     & REAL(R_asuke,4),REAL(R_aruke,4),
     & REAL(R_upuke,4),REAL(R_usuke,4),L_atuke,REAL(R_etuke
     &,4),L_ituke,
     & L_otuke,R_esuke,REAL(R_oruke,4),REAL(R_iruke,4),L_utuke
     &)
      !}
C FDB50_vlv.fgi(  73, 309):���������� ������� ��� 2,20FDB50CT030XQ01
      R_abale = R8_ivi
C FDB50_vent_log.fgi(  31, 345):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uvuke,R_udale,REAL(1
     &,4),
     & REAL(R_abale,4),I_odale,REAL(R_ixuke,4),
     & REAL(R_oxuke,4),REAL(R_ovuke,4),
     & REAL(R_ivuke,4),REAL(R_ibale,4),L_obale,REAL(R_ubale
     &,4),L_adale,
     & L_edale,R_uxuke,REAL(R_exuke,4),REAL(R_axuke,4),L_idale
     &)
      !}
C FDB50_vlv.fgi(  73, 323):���������� ������� ��� 2,20FDB50CT029XQ01
      R_okale = R8_ovi
C FDB50_vent_log.fgi(  31, 349):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ifale,R_imale,REAL(1
     &,4),
     & REAL(R_okale,4),I_emale,REAL(R_akale,4),
     & REAL(R_ekale,4),REAL(R_efale,4),
     & REAL(R_afale,4),REAL(R_alale,4),L_elale,REAL(R_ilale
     &,4),L_olale,
     & L_ulale,R_ikale,REAL(R_ufale,4),REAL(R_ofale,4),L_amale
     &)
      !}
C FDB50_vlv.fgi(  73, 337):���������� ������� ��� 2,20FDB50CT028XQ01
      R_uluke = R8_uvi
C FDB50_vent_log.fgi(  31, 353):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_okuke,R_opuke,REAL(1
     &,4),
     & REAL(R_uluke,4),I_ipuke,REAL(R_eluke,4),
     & REAL(R_iluke,4),REAL(R_ikuke,4),
     & REAL(R_ekuke,4),REAL(R_emuke,4),L_imuke,REAL(R_omuke
     &,4),L_umuke,
     & L_apuke,R_oluke,REAL(R_aluke,4),REAL(R_ukuke,4),L_epuke
     &)
      !}
C FDB50_vlv.fgi(  73, 295):���������� ������� ��� 2,20FDB50CT027XQ01
      R_erale = R8_axi
C FDB50_vent_log.fgi(  31, 357):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_apale,R_atale,REAL(1
     &,4),
     & REAL(R_erale,4),I_usale,REAL(R_opale,4),
     & REAL(R_upale,4),REAL(R_umale,4),
     & REAL(R_omale,4),REAL(R_orale,4),L_urale,REAL(R_asale
     &,4),L_esale,
     & L_isale,R_arale,REAL(R_ipale,4),REAL(R_epale,4),L_osale
     &)
      !}
C FDB50_vlv.fgi(  46, 295):���������� ������� ��� 2,20FDB50CT026XQ01
      R_uvale = R8_exi
C FDB50_vent_log.fgi(  31, 363):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_otale,R_obele,REAL(1
     &,4),
     & REAL(R_uvale,4),I_ibele,REAL(R_evale,4),
     & REAL(R_ivale,4),REAL(R_itale,4),
     & REAL(R_etale,4),REAL(R_exale,4),L_ixale,REAL(R_oxale
     &,4),L_uxale,
     & L_abele,R_ovale,REAL(R_avale,4),REAL(R_utale,4),L_ebele
     &)
      !}
C FDB50_vlv.fgi(  46, 309):���������� ������� ��� 2,20FDB50CT025XQ01
      R_ifele = R8_ixi
C FDB50_vent_log.fgi(  31, 368):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_edele,R_elele,REAL(1
     &,4),
     & REAL(R_ifele,4),I_alele,REAL(R_udele,4),
     & REAL(R_afele,4),REAL(R_adele,4),
     & REAL(R_ubele,4),REAL(R_ufele,4),L_akele,REAL(R_ekele
     &,4),L_ikele,
     & L_okele,R_efele,REAL(R_odele,4),REAL(R_idele,4),L_ukele
     &)
      !}
C FDB50_vlv.fgi(  46, 323):���������� ������� ��� 2,20FDB50CT024XQ01
      R_apele = R8_oxi
C FDB50_vent_log.fgi(  31, 373):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ulele,R_urele,REAL(1
     &,4),
     & REAL(R_apele,4),I_orele,REAL(R_imele,4),
     & REAL(R_omele,4),REAL(R_olele,4),
     & REAL(R_ilele,4),REAL(R_ipele,4),L_opele,REAL(R_upele
     &,4),L_arele,
     & L_erele,R_umele,REAL(R_emele,4),REAL(R_amele,4),L_irele
     &)
      !}
C FDB50_vlv.fgi(  46, 337):���������� ������� ��� 2,20FDB50CT023XQ01
      !��������� R_(60) = FDB50_vent_logC?? /0.0/
      R_(60)=R0_ibo
C FDB50_vent_log.fgi( 449, 477):���������
      !��������� R_(59) = FDB50_vent_logC?? /0.001/
      R_(59)=R0_obo
C FDB50_vent_log.fgi( 449, 475):���������
      !��������� R_(64) = FDB50_vent_logC?? /0.0/
      R_(64)=R0_ido
C FDB50_vent_log.fgi( 398, 477):���������
      !��������� R_(63) = FDB50_vent_logC?? /0.001/
      R_(63)=R0_odo
C FDB50_vent_log.fgi( 398, 475):���������
      !��������� R_(68) = FDB50_vent_logC?? /0.0/
      R_(68)=R0_ifo
C FDB50_vent_log.fgi( 344, 477):���������
      !��������� R_(67) = FDB50_vent_logC?? /0.001/
      R_(67)=R0_ofo
C FDB50_vent_log.fgi( 344, 475):���������
      !��������� R_(72) = FDB50_vent_logC?? /0.0/
      R_(72)=R0_iko
C FDB50_vent_log.fgi( 291, 477):���������
      !��������� R_(71) = FDB50_vent_logC?? /0.001/
      R_(71)=R0_oko
C FDB50_vent_log.fgi( 291, 475):���������
      !��������� R_(76) = FDB50_vent_logC?? /0.0/
      R_(76)=R0_ilo
C FDB50_vent_log.fgi( 240, 477):���������
      !��������� R_(75) = FDB50_vent_logC?? /0.001/
      R_(75)=R0_olo
C FDB50_vent_log.fgi( 240, 475):���������
      !��������� R_(80) = FDB50_vent_logC?? /0.0/
      R_(80)=R0_imo
C FDB50_vent_log.fgi( 187, 477):���������
      !��������� R_(79) = FDB50_vent_logC?? /0.001/
      R_(79)=R0_omo
C FDB50_vent_log.fgi( 187, 475):���������
      L_(5)=R8_aro.gt.R0_umo
C FDB50_vent_log.fgi( 133, 464):���������� >
      !��������� R_(84) = FDB50_vent_logC?? /0.0/
      R_(84)=R0_opo
C FDB50_vent_log.fgi( 134, 477):���������
      !��������� R_(83) = FDB50_vent_logC?? /0.001/
      R_(83)=R0_upo
C FDB50_vent_log.fgi( 134, 475):���������
      if(L_(5)) then
         R_(82)=R_(83)
      else
         R_(82)=R_(84)
      endif
C FDB50_vent_log.fgi( 137, 475):���� RE IN LO CH7
      R8_ipo=(R0_apo*R_(81)+deltat*R_(82))/(R0_apo+deltat
     &)
C FDB50_vent_log.fgi( 145, 476):�������������� �����  
      R8_epo=R8_ipo
C FDB50_vent_log.fgi( 159, 476):������,F_FDB50CF021_G
      L_(6)=R8_iso.gt.R0_ero
C FDB50_vent_log.fgi(  77, 464):���������� >
      !��������� R_(88) = FDB50_vent_logC?? /0.0/
      R_(88)=R0_aso
C FDB50_vent_log.fgi(  78, 477):���������
      !��������� R_(87) = FDB50_vent_logC?? /0.001/
      R_(87)=R0_eso
C FDB50_vent_log.fgi(  78, 475):���������
      if(L_(6)) then
         R_(86)=R_(87)
      else
         R_(86)=R_(88)
      endif
C FDB50_vent_log.fgi(  81, 475):���� RE IN LO CH7
      R8_uro=(R0_iro*R_(85)+deltat*R_(86))/(R0_iro+deltat
     &)
C FDB50_vent_log.fgi(  90, 476):�������������� �����  
      R8_oro=R8_uro
C FDB50_vent_log.fgi( 104, 476):������,F_FDB50CF020_G
      L_(7)=R8_uto.gt.R0_oso
C FDB50_vent_log.fgi(  22, 464):���������� >
      !��������� R_(92) = FDB50_vent_logC?? /0.0/
      R_(92)=R0_ito
C FDB50_vent_log.fgi(  22, 477):���������
      !��������� R_(91) = FDB50_vent_logC?? /0.001/
      R_(91)=R0_oto
C FDB50_vent_log.fgi(  22, 475):���������
      if(L_(7)) then
         R_(90)=R_(91)
      else
         R_(90)=R_(92)
      endif
C FDB50_vent_log.fgi(  25, 475):���� RE IN LO CH7
      R8_eto=(R0_uso*R_(89)+deltat*R_(90))/(R0_uso+deltat
     &)
C FDB50_vent_log.fgi(  34, 476):�������������� �����  
      R8_ato=R8_eto
C FDB50_vent_log.fgi(  48, 476):������,F_FDB50CF019_G
      R_(94) = 0.0
C FDB50_temp_pts.fgi( 415, 355):��������� (RE4) (�������)
      R_(93) = 1.0
C FDB50_temp_pts.fgi( 415, 353):��������� (RE4) (�������)
      R_(100) = 400.0
C FDB50_temp_pts.fgi( 415, 362):��������� (RE4) (�������)
      R_(101) = 10.0
C FDB50_temp_pts.fgi( 415, 364):��������� (RE4) (�������)
      R_(102) = R_(101) * R_(100)
C FDB50_temp_pts.fgi( 429, 364):����������
      R_(97)=0.0
C FDB50_temp_pts.fgi( 434, 366):��������� ������������ ����
      !Constant L_(8) = FDB50_temp_ptsClJ10779 /.false./
      L_(8)=L0_evo
C FDB50_temp_pts.fgi( 430, 339):��������� ���. ������������� 
      R_(99) = 0.0
C FDB50_temp_pts.fgi( 401, 347):��������� (RE4) (�������)
      R_(96) = 20.0
C FDB50_temp_pts.fgi( 439, 369):��������� (RE4) (�������)
      R_(95) = 2000.0
C FDB50_temp_pts.fgi( 439, 372):��������� (RE4) (�������)
      L_(9)=I_axo.ne.I0_uvo
C FDB50_temp_pts.fgi( 411, 334):���������� �������������
      if(L_(9)) then
         R8_avo=R_(93)
      else
         R8_avo=R_(94)
      endif
C FDB50_temp_pts.fgi( 418, 354):���� RE IN LO CH7
      if(L_(9)) then
         R_(98)=R8_exo
      else
         R_(98)=R_(99)
      endif
C FDB50_temp_pts.fgi( 418, 346):���� RE IN LO CH7
      if(L_(8)) then
         R8_ovo=R_(97)
      elseif(L_(8)) then
         R8_ovo=R8_ovo
      else
         R8_ovo=R8_ovo+deltat/R_(102)*R_(98)
      endif
      if(R8_ovo.gt.R_(95)) then
         R8_ovo=R_(95)
      elseif(R8_ovo.lt.R_(96)) then
         R8_ovo=R_(96)
      endif
C FDB50_temp_pts.fgi( 443, 347):����������
      R8_ivo=R8_ovo
C FDB50_temp_pts.fgi( 477, 347):������,PST1_B25_W_intTt
      R_(104) = 0.0
C FDB50_temp_pts.fgi( 415, 411):��������� (RE4) (�������)
      R_(103) = 1.0
C FDB50_temp_pts.fgi( 415, 409):��������� (RE4) (�������)
      R_(110) = 400.0
C FDB50_temp_pts.fgi( 415, 418):��������� (RE4) (�������)
      R_(111) = 10.0
C FDB50_temp_pts.fgi( 415, 420):��������� (RE4) (�������)
      R_(112) = R_(111) * R_(110)
C FDB50_temp_pts.fgi( 429, 420):����������
      R_(107)=0.0
C FDB50_temp_pts.fgi( 434, 422):��������� ������������ ����
      !Constant L_(10) = FDB50_temp_ptsClJ10744 /.false./
      L_(10)=L0_oxo
C FDB50_temp_pts.fgi( 430, 395):��������� ���. ������������� 
      R_(109) = 0.0
C FDB50_temp_pts.fgi( 401, 403):��������� (RE4) (�������)
      R_(106) = 20.0
C FDB50_temp_pts.fgi( 439, 425):��������� (RE4) (�������)
      R_(105) = 2000.0
C FDB50_temp_pts.fgi( 439, 428):��������� (RE4) (�������)
      L_(11)=I_ibu.ne.I0_ebu
C FDB50_temp_pts.fgi( 411, 390):���������� �������������
      if(L_(11)) then
         R_(108)=R8_obu
      else
         R_(108)=R_(109)
      endif
C FDB50_temp_pts.fgi( 418, 402):���� RE IN LO CH7
      if(L_(10)) then
         R8_abu=R_(107)
      elseif(L_(10)) then
         R8_abu=R8_abu
      else
         R8_abu=R8_abu+deltat/R_(112)*R_(108)
      endif
      if(R8_abu.gt.R_(105)) then
         R8_abu=R_(105)
      elseif(R8_abu.lt.R_(106)) then
         R8_abu=R_(106)
      endif
C FDB50_temp_pts.fgi( 443, 403):����������
      R8_uxo=R8_abu
C FDB50_temp_pts.fgi( 477, 403):������,PST1_B24_W_intTt
      if(L_(11)) then
         R8_ixo=R_(103)
      else
         R8_ixo=R_(104)
      endif
C FDB50_temp_pts.fgi( 418, 410):���� RE IN LO CH7
      R_(114) = 0.0
C FDB50_temp_pts.fgi( 415, 457):��������� (RE4) (�������)
      R_(113) = 1.0
C FDB50_temp_pts.fgi( 415, 455):��������� (RE4) (�������)
      R_(120) = 400.0
C FDB50_temp_pts.fgi( 415, 464):��������� (RE4) (�������)
      R_(121) = 10.0
C FDB50_temp_pts.fgi( 415, 466):��������� (RE4) (�������)
      R_(122) = R_(121) * R_(120)
C FDB50_temp_pts.fgi( 429, 466):����������
      R_(117)=0.0
C FDB50_temp_pts.fgi( 434, 468):��������� ������������ ����
      !Constant L_(12) = FDB50_temp_ptsClJ10709 /.false./
      L_(12)=L0_adu
C FDB50_temp_pts.fgi( 430, 441):��������� ���. ������������� 
      R_(119) = 0.0
C FDB50_temp_pts.fgi( 401, 449):��������� (RE4) (�������)
      R_(116) = 20.0
C FDB50_temp_pts.fgi( 439, 471):��������� (RE4) (�������)
      R_(115) = 2000.0
C FDB50_temp_pts.fgi( 439, 474):��������� (RE4) (�������)
      L_(13)=I_udu.ne.I0_odu
C FDB50_temp_pts.fgi( 411, 436):���������� �������������
      if(L_(13)) then
         R8_ubu=R_(113)
      else
         R8_ubu=R_(114)
      endif
C FDB50_temp_pts.fgi( 418, 456):���� RE IN LO CH7
      if(L_(13)) then
         R_(118)=R8_afu
      else
         R_(118)=R_(119)
      endif
C FDB50_temp_pts.fgi( 418, 448):���� RE IN LO CH7
      if(L_(12)) then
         R8_idu=R_(117)
      elseif(L_(12)) then
         R8_idu=R8_idu
      else
         R8_idu=R8_idu+deltat/R_(122)*R_(118)
      endif
      if(R8_idu.gt.R_(115)) then
         R8_idu=R_(115)
      elseif(R8_idu.lt.R_(116)) then
         R8_idu=R_(116)
      endif
C FDB50_temp_pts.fgi( 443, 449):����������
      R8_edu=R8_idu
C FDB50_temp_pts.fgi( 477, 449):������,PST1_B23_W_intTt
      R_(124) = 0.0
C FDB50_temp_pts.fgi( 415, 506):��������� (RE4) (�������)
      R_(123) = 1.0
C FDB50_temp_pts.fgi( 415, 504):��������� (RE4) (�������)
      R_(130) = 400.0
C FDB50_temp_pts.fgi( 415, 513):��������� (RE4) (�������)
      R_(131) = 10.0
C FDB50_temp_pts.fgi( 415, 515):��������� (RE4) (�������)
      R_(132) = R_(131) * R_(130)
C FDB50_temp_pts.fgi( 429, 515):����������
      R_(127)=0.0
C FDB50_temp_pts.fgi( 434, 517):��������� ������������ ����
      !Constant L_(14) = FDB50_temp_ptsClJ10674 /.false./
      L_(14)=L0_ifu
C FDB50_temp_pts.fgi( 430, 490):��������� ���. ������������� 
      R_(129) = 0.0
C FDB50_temp_pts.fgi( 401, 498):��������� (RE4) (�������)
      R_(126) = 20.0
C FDB50_temp_pts.fgi( 439, 520):��������� (RE4) (�������)
      R_(125) = 2000.0
C FDB50_temp_pts.fgi( 439, 523):��������� (RE4) (�������)
      L_(15)=I_eku.ne.I0_aku
C FDB50_temp_pts.fgi( 411, 485):���������� �������������
      if(L_(15)) then
         R8_efu=R_(123)
      else
         R8_efu=R_(124)
      endif
C FDB50_temp_pts.fgi( 418, 505):���� RE IN LO CH7
      if(L_(15)) then
         R_(128)=R8_iku
      else
         R_(128)=R_(129)
      endif
C FDB50_temp_pts.fgi( 418, 497):���� RE IN LO CH7
      if(L_(14)) then
         R8_ufu=R_(127)
      elseif(L_(14)) then
         R8_ufu=R8_ufu
      else
         R8_ufu=R8_ufu+deltat/R_(132)*R_(128)
      endif
      if(R8_ufu.gt.R_(125)) then
         R8_ufu=R_(125)
      elseif(R8_ufu.lt.R_(126)) then
         R8_ufu=R_(126)
      endif
C FDB50_temp_pts.fgi( 443, 498):����������
      R8_ofu=R8_ufu
C FDB50_temp_pts.fgi( 477, 498):������,PST1_B22_W_intTt
      R_(134) = 0.0
C FDB50_temp_pts.fgi( 415, 552):��������� (RE4) (�������)
      R_(133) = 1.0
C FDB50_temp_pts.fgi( 415, 550):��������� (RE4) (�������)
      R_(140) = 400.0
C FDB50_temp_pts.fgi( 415, 559):��������� (RE4) (�������)
      R_(141) = 10.0
C FDB50_temp_pts.fgi( 415, 561):��������� (RE4) (�������)
      R_(142) = R_(141) * R_(140)
C FDB50_temp_pts.fgi( 429, 561):����������
      R_(137)=0.0
C FDB50_temp_pts.fgi( 434, 563):��������� ������������ ����
      !Constant L_(16) = FDB50_temp_ptsClJ10639 /.false./
      L_(16)=L0_uku
C FDB50_temp_pts.fgi( 430, 536):��������� ���. ������������� 
      R_(139) = 0.0
C FDB50_temp_pts.fgi( 401, 544):��������� (RE4) (�������)
      R_(136) = 20.0
C FDB50_temp_pts.fgi( 439, 566):��������� (RE4) (�������)
      R_(135) = 2000.0
C FDB50_temp_pts.fgi( 439, 569):��������� (RE4) (�������)
      L_(17)=I_olu.ne.I0_ilu
C FDB50_temp_pts.fgi( 411, 531):���������� �������������
      if(L_(17)) then
         R_(138)=R8_ulu
      else
         R_(138)=R_(139)
      endif
C FDB50_temp_pts.fgi( 418, 543):���� RE IN LO CH7
      if(L_(16)) then
         R8_elu=R_(137)
      elseif(L_(16)) then
         R8_elu=R8_elu
      else
         R8_elu=R8_elu+deltat/R_(142)*R_(138)
      endif
      if(R8_elu.gt.R_(135)) then
         R8_elu=R_(135)
      elseif(R8_elu.lt.R_(136)) then
         R8_elu=R_(136)
      endif
C FDB50_temp_pts.fgi( 443, 544):����������
      R8_alu=R8_elu
C FDB50_temp_pts.fgi( 477, 544):������,PST1_B21_W_intTt
      if(L_(17)) then
         R8_oku=R_(133)
      else
         R8_oku=R_(134)
      endif
C FDB50_temp_pts.fgi( 418, 551):���� RE IN LO CH7
      R_(144) = 0.0
C FDB50_temp_pts.fgi( 256, 114):��������� (RE4) (�������)
      R_(143) = 1.0
C FDB50_temp_pts.fgi( 256, 112):��������� (RE4) (�������)
      R_(150) = 400.0
C FDB50_temp_pts.fgi( 256, 121):��������� (RE4) (�������)
      R_(151) = 10.0
C FDB50_temp_pts.fgi( 256, 123):��������� (RE4) (�������)
      R_(152) = R_(151) * R_(150)
C FDB50_temp_pts.fgi( 270, 123):����������
      R_(147)=0.0
C FDB50_temp_pts.fgi( 275, 125):��������� ������������ ����
      !Constant L_(18) = FDB50_temp_ptsClJ10604 /.false./
      L_(18)=L0_emu
C FDB50_temp_pts.fgi( 271,  98):��������� ���. ������������� 
      R_(149) = 0.0
C FDB50_temp_pts.fgi( 242, 106):��������� (RE4) (�������)
      R_(146) = 20.0
C FDB50_temp_pts.fgi( 280, 128):��������� (RE4) (�������)
      R_(145) = 2000.0
C FDB50_temp_pts.fgi( 280, 131):��������� (RE4) (�������)
      L_(19)=I_apu.ne.I0_umu
C FDB50_temp_pts.fgi( 252,  93):���������� �������������
      if(L_(19)) then
         R8_amu=R_(143)
      else
         R8_amu=R_(144)
      endif
C FDB50_temp_pts.fgi( 259, 113):���� RE IN LO CH7
      if(L_(19)) then
         R_(148)=R8_epu
      else
         R_(148)=R_(149)
      endif
C FDB50_temp_pts.fgi( 259, 105):���� RE IN LO CH7
      if(L_(18)) then
         R8_omu=R_(147)
      elseif(L_(18)) then
         R8_omu=R8_omu
      else
         R8_omu=R8_omu+deltat/R_(152)*R_(148)
      endif
      if(R8_omu.gt.R_(145)) then
         R8_omu=R_(145)
      elseif(R8_omu.lt.R_(146)) then
         R8_omu=R_(146)
      endif
C FDB50_temp_pts.fgi( 284, 106):����������
      R8_imu=R8_omu
C FDB50_temp_pts.fgi( 318, 106):������,PST1_B20_W_intTt
      R_(154) = 0.0
C FDB50_temp_pts.fgi( 256, 160):��������� (RE4) (�������)
      R_(153) = 1.0
C FDB50_temp_pts.fgi( 256, 158):��������� (RE4) (�������)
      R_(160) = 400.0
C FDB50_temp_pts.fgi( 256, 167):��������� (RE4) (�������)
      R_(161) = 10.0
C FDB50_temp_pts.fgi( 256, 169):��������� (RE4) (�������)
      R_(162) = R_(161) * R_(160)
C FDB50_temp_pts.fgi( 270, 169):����������
      R_(157)=0.0
C FDB50_temp_pts.fgi( 275, 171):��������� ������������ ����
      !Constant L_(20) = FDB50_temp_ptsClJ10569 /.false./
      L_(20)=L0_opu
C FDB50_temp_pts.fgi( 271, 144):��������� ���. ������������� 
      R_(159) = 0.0
C FDB50_temp_pts.fgi( 242, 152):��������� (RE4) (�������)
      R_(156) = 20.0
C FDB50_temp_pts.fgi( 280, 174):��������� (RE4) (�������)
      R_(155) = 2000.0
C FDB50_temp_pts.fgi( 280, 177):��������� (RE4) (�������)
      L_(21)=I_iru.ne.I0_eru
C FDB50_temp_pts.fgi( 252, 139):���������� �������������
      if(L_(21)) then
         R_(158)=R8_oru
      else
         R_(158)=R_(159)
      endif
C FDB50_temp_pts.fgi( 259, 151):���� RE IN LO CH7
      if(L_(20)) then
         R8_aru=R_(157)
      elseif(L_(20)) then
         R8_aru=R8_aru
      else
         R8_aru=R8_aru+deltat/R_(162)*R_(158)
      endif
      if(R8_aru.gt.R_(155)) then
         R8_aru=R_(155)
      elseif(R8_aru.lt.R_(156)) then
         R8_aru=R_(156)
      endif
C FDB50_temp_pts.fgi( 284, 152):����������
      R8_upu=R8_aru
C FDB50_temp_pts.fgi( 318, 152):������,PST1_B19_W_intTt
      if(L_(21)) then
         R8_ipu=R_(153)
      else
         R8_ipu=R_(154)
      endif
C FDB50_temp_pts.fgi( 259, 159):���� RE IN LO CH7
      R_(164) = 0.0
C FDB50_temp_pts.fgi( 256, 214):��������� (RE4) (�������)
      R_(163) = 1.0
C FDB50_temp_pts.fgi( 256, 212):��������� (RE4) (�������)
      R_(170) = 400.0
C FDB50_temp_pts.fgi( 256, 221):��������� (RE4) (�������)
      R_(171) = 10.0
C FDB50_temp_pts.fgi( 256, 223):��������� (RE4) (�������)
      R_(172) = R_(171) * R_(170)
C FDB50_temp_pts.fgi( 270, 223):����������
      R_(167)=0.0
C FDB50_temp_pts.fgi( 275, 225):��������� ������������ ����
      !Constant L_(22) = FDB50_temp_ptsClJ10534 /.false./
      L_(22)=L0_asu
C FDB50_temp_pts.fgi( 271, 198):��������� ���. ������������� 
      R_(169) = 0.0
C FDB50_temp_pts.fgi( 242, 206):��������� (RE4) (�������)
      R_(166) = 20.0
C FDB50_temp_pts.fgi( 280, 228):��������� (RE4) (�������)
      R_(165) = 2000.0
C FDB50_temp_pts.fgi( 280, 231):��������� (RE4) (�������)
      L_(23)=I_usu.ne.I0_osu
C FDB50_temp_pts.fgi( 252, 193):���������� �������������
      if(L_(23)) then
         R_(168)=R8_atu
      else
         R_(168)=R_(169)
      endif
C FDB50_temp_pts.fgi( 259, 205):���� RE IN LO CH7
      if(L_(22)) then
         R8_isu=R_(167)
      elseif(L_(22)) then
         R8_isu=R8_isu
      else
         R8_isu=R8_isu+deltat/R_(172)*R_(168)
      endif
      if(R8_isu.gt.R_(165)) then
         R8_isu=R_(165)
      elseif(R8_isu.lt.R_(166)) then
         R8_isu=R_(166)
      endif
C FDB50_temp_pts.fgi( 284, 206):����������
      R8_esu=R8_isu
C FDB50_temp_pts.fgi( 318, 206):������,PST1_B18_W_intTt
      if(L_(23)) then
         R8_uru=R_(163)
      else
         R8_uru=R_(164)
      endif
C FDB50_temp_pts.fgi( 259, 213):���� RE IN LO CH7
      R_(174) = 0.0
C FDB50_temp_pts.fgi( 256, 260):��������� (RE4) (�������)
      R_(173) = 1.0
C FDB50_temp_pts.fgi( 256, 258):��������� (RE4) (�������)
      R_(180) = 400.0
C FDB50_temp_pts.fgi( 256, 267):��������� (RE4) (�������)
      R_(181) = 10.0
C FDB50_temp_pts.fgi( 256, 269):��������� (RE4) (�������)
      R_(182) = R_(181) * R_(180)
C FDB50_temp_pts.fgi( 270, 269):����������
      R_(177)=0.0
C FDB50_temp_pts.fgi( 275, 271):��������� ������������ ����
      !Constant L_(24) = FDB50_temp_ptsClJ10499 /.false./
      L_(24)=L0_itu
C FDB50_temp_pts.fgi( 271, 244):��������� ���. ������������� 
      R_(179) = 0.0
C FDB50_temp_pts.fgi( 242, 252):��������� (RE4) (�������)
      R_(176) = 20.0
C FDB50_temp_pts.fgi( 280, 274):��������� (RE4) (�������)
      R_(175) = 2000.0
C FDB50_temp_pts.fgi( 280, 277):��������� (RE4) (�������)
      L_(25)=I_evu.ne.I0_avu
C FDB50_temp_pts.fgi( 252, 239):���������� �������������
      if(L_(25)) then
         R8_etu=R_(173)
      else
         R8_etu=R_(174)
      endif
C FDB50_temp_pts.fgi( 259, 259):���� RE IN LO CH7
      if(L_(25)) then
         R_(178)=R8_ivu
      else
         R_(178)=R_(179)
      endif
C FDB50_temp_pts.fgi( 259, 251):���� RE IN LO CH7
      if(L_(24)) then
         R8_utu=R_(177)
      elseif(L_(24)) then
         R8_utu=R8_utu
      else
         R8_utu=R8_utu+deltat/R_(182)*R_(178)
      endif
      if(R8_utu.gt.R_(175)) then
         R8_utu=R_(175)
      elseif(R8_utu.lt.R_(176)) then
         R8_utu=R_(176)
      endif
C FDB50_temp_pts.fgi( 284, 252):����������
      R8_otu=R8_utu
C FDB50_temp_pts.fgi( 318, 252):������,PST1_B17_W_intTt
      R_(184) = 0.0
C FDB50_temp_pts.fgi( 256, 309):��������� (RE4) (�������)
      R_(183) = 1.0
C FDB50_temp_pts.fgi( 256, 307):��������� (RE4) (�������)
      R_(190) = 400.0
C FDB50_temp_pts.fgi( 256, 316):��������� (RE4) (�������)
      R_(191) = 10.0
C FDB50_temp_pts.fgi( 256, 318):��������� (RE4) (�������)
      R_(192) = R_(191) * R_(190)
C FDB50_temp_pts.fgi( 270, 318):����������
      R_(187)=0.0
C FDB50_temp_pts.fgi( 275, 320):��������� ������������ ����
      !Constant L_(26) = FDB50_temp_ptsClJ10464 /.false./
      L_(26)=L0_uvu
C FDB50_temp_pts.fgi( 271, 293):��������� ���. ������������� 
      R_(189) = 0.0
C FDB50_temp_pts.fgi( 242, 301):��������� (RE4) (�������)
      R_(186) = 20.0
C FDB50_temp_pts.fgi( 280, 323):��������� (RE4) (�������)
      R_(185) = 2000.0
C FDB50_temp_pts.fgi( 280, 326):��������� (RE4) (�������)
      L_(27)=I_oxu.ne.I0_ixu
C FDB50_temp_pts.fgi( 252, 288):���������� �������������
      if(L_(27)) then
         R8_ovu=R_(183)
      else
         R8_ovu=R_(184)
      endif
C FDB50_temp_pts.fgi( 259, 308):���� RE IN LO CH7
      if(L_(27)) then
         R_(188)=R8_uxu
      else
         R_(188)=R_(189)
      endif
C FDB50_temp_pts.fgi( 259, 300):���� RE IN LO CH7
      if(L_(26)) then
         R8_exu=R_(187)
      elseif(L_(26)) then
         R8_exu=R8_exu
      else
         R8_exu=R8_exu+deltat/R_(192)*R_(188)
      endif
      if(R8_exu.gt.R_(185)) then
         R8_exu=R_(185)
      elseif(R8_exu.lt.R_(186)) then
         R8_exu=R_(186)
      endif
C FDB50_temp_pts.fgi( 284, 301):����������
      R8_axu=R8_exu
C FDB50_temp_pts.fgi( 318, 301):������,PST1_B16_W_intTt
      R_(194) = 0.0
C FDB50_temp_pts.fgi( 256, 355):��������� (RE4) (�������)
      R_(193) = 1.0
C FDB50_temp_pts.fgi( 256, 353):��������� (RE4) (�������)
      R_(200) = 400.0
C FDB50_temp_pts.fgi( 256, 362):��������� (RE4) (�������)
      R_(201) = 10.0
C FDB50_temp_pts.fgi( 256, 364):��������� (RE4) (�������)
      R_(202) = R_(201) * R_(200)
C FDB50_temp_pts.fgi( 270, 364):����������
      R_(197)=0.0
C FDB50_temp_pts.fgi( 275, 366):��������� ������������ ����
      !Constant L_(28) = FDB50_temp_ptsClJ10429 /.false./
      L_(28)=L0_ebad
C FDB50_temp_pts.fgi( 271, 339):��������� ���. ������������� 
      R_(199) = 0.0
C FDB50_temp_pts.fgi( 242, 347):��������� (RE4) (�������)
      R_(196) = 20.0
C FDB50_temp_pts.fgi( 280, 369):��������� (RE4) (�������)
      R_(195) = 2000.0
C FDB50_temp_pts.fgi( 280, 372):��������� (RE4) (�������)
      L_(29)=I_adad.ne.I0_ubad
C FDB50_temp_pts.fgi( 252, 334):���������� �������������
      if(L_(29)) then
         R_(198)=R8_edad
      else
         R_(198)=R_(199)
      endif
C FDB50_temp_pts.fgi( 259, 346):���� RE IN LO CH7
      if(L_(28)) then
         R8_obad=R_(197)
      elseif(L_(28)) then
         R8_obad=R8_obad
      else
         R8_obad=R8_obad+deltat/R_(202)*R_(198)
      endif
      if(R8_obad.gt.R_(195)) then
         R8_obad=R_(195)
      elseif(R8_obad.lt.R_(196)) then
         R8_obad=R_(196)
      endif
C FDB50_temp_pts.fgi( 284, 347):����������
      R8_ibad=R8_obad
C FDB50_temp_pts.fgi( 318, 347):������,PST1_B15_W_intTt
      if(L_(29)) then
         R8_abad=R_(193)
      else
         R8_abad=R_(194)
      endif
C FDB50_temp_pts.fgi( 259, 354):���� RE IN LO CH7
      R_(204) = 0.0
C FDB50_temp_pts.fgi( 256, 411):��������� (RE4) (�������)
      R_(203) = 1.0
C FDB50_temp_pts.fgi( 256, 409):��������� (RE4) (�������)
      R_(210) = 400.0
C FDB50_temp_pts.fgi( 256, 418):��������� (RE4) (�������)
      R_(211) = 10.0
C FDB50_temp_pts.fgi( 256, 420):��������� (RE4) (�������)
      R_(212) = R_(211) * R_(210)
C FDB50_temp_pts.fgi( 270, 420):����������
      R_(207)=0.0
C FDB50_temp_pts.fgi( 275, 422):��������� ������������ ����
      !Constant L_(30) = FDB50_temp_ptsClJ10394 /.false./
      L_(30)=L0_odad
C FDB50_temp_pts.fgi( 271, 395):��������� ���. ������������� 
      R_(209) = 0.0
C FDB50_temp_pts.fgi( 242, 403):��������� (RE4) (�������)
      R_(206) = 20.0
C FDB50_temp_pts.fgi( 280, 425):��������� (RE4) (�������)
      R_(205) = 2000.0
C FDB50_temp_pts.fgi( 280, 428):��������� (RE4) (�������)
      L_(31)=I_ifad.ne.I0_efad
C FDB50_temp_pts.fgi( 252, 390):���������� �������������
      if(L_(31)) then
         R8_idad=R_(203)
      else
         R8_idad=R_(204)
      endif
C FDB50_temp_pts.fgi( 259, 410):���� RE IN LO CH7
      if(L_(31)) then
         R_(208)=R8_ofad
      else
         R_(208)=R_(209)
      endif
C FDB50_temp_pts.fgi( 259, 402):���� RE IN LO CH7
      if(L_(30)) then
         R8_afad=R_(207)
      elseif(L_(30)) then
         R8_afad=R8_afad
      else
         R8_afad=R8_afad+deltat/R_(212)*R_(208)
      endif
      if(R8_afad.gt.R_(205)) then
         R8_afad=R_(205)
      elseif(R8_afad.lt.R_(206)) then
         R8_afad=R_(206)
      endif
C FDB50_temp_pts.fgi( 284, 403):����������
      R8_udad=R8_afad
C FDB50_temp_pts.fgi( 318, 403):������,PST1_B14_W_intTt
      R_(214) = 0.0
C FDB50_temp_pts.fgi( 256, 457):��������� (RE4) (�������)
      R_(213) = 1.0
C FDB50_temp_pts.fgi( 256, 455):��������� (RE4) (�������)
      R_(220) = 400.0
C FDB50_temp_pts.fgi( 256, 464):��������� (RE4) (�������)
      R_(221) = 10.0
C FDB50_temp_pts.fgi( 256, 466):��������� (RE4) (�������)
      R_(222) = R_(221) * R_(220)
C FDB50_temp_pts.fgi( 270, 466):����������
      R_(217)=0.0
C FDB50_temp_pts.fgi( 275, 468):��������� ������������ ����
      !Constant L_(32) = FDB50_temp_ptsClJ10359 /.false./
      L_(32)=L0_akad
C FDB50_temp_pts.fgi( 271, 441):��������� ���. ������������� 
      R_(219) = 0.0
C FDB50_temp_pts.fgi( 242, 449):��������� (RE4) (�������)
      R_(216) = 20.0
C FDB50_temp_pts.fgi( 280, 471):��������� (RE4) (�������)
      R_(215) = 2000.0
C FDB50_temp_pts.fgi( 280, 474):��������� (RE4) (�������)
      L_(33)=I_ukad.ne.I0_okad
C FDB50_temp_pts.fgi( 252, 436):���������� �������������
      if(L_(33)) then
         R_(218)=R8_alad
      else
         R_(218)=R_(219)
      endif
C FDB50_temp_pts.fgi( 259, 448):���� RE IN LO CH7
      if(L_(32)) then
         R8_ikad=R_(217)
      elseif(L_(32)) then
         R8_ikad=R8_ikad
      else
         R8_ikad=R8_ikad+deltat/R_(222)*R_(218)
      endif
      if(R8_ikad.gt.R_(215)) then
         R8_ikad=R_(215)
      elseif(R8_ikad.lt.R_(216)) then
         R8_ikad=R_(216)
      endif
C FDB50_temp_pts.fgi( 284, 449):����������
      R8_ekad=R8_ikad
C FDB50_temp_pts.fgi( 318, 449):������,PST1_B13_W_intTt
      if(L_(33)) then
         R8_ufad=R_(213)
      else
         R8_ufad=R_(214)
      endif
C FDB50_temp_pts.fgi( 259, 456):���� RE IN LO CH7
      R_(224) = 0.0
C FDB50_temp_pts.fgi( 256, 506):��������� (RE4) (�������)
      R_(223) = 1.0
C FDB50_temp_pts.fgi( 256, 504):��������� (RE4) (�������)
      R_(230) = 400.0
C FDB50_temp_pts.fgi( 256, 513):��������� (RE4) (�������)
      R_(231) = 10.0
C FDB50_temp_pts.fgi( 256, 515):��������� (RE4) (�������)
      R_(232) = R_(231) * R_(230)
C FDB50_temp_pts.fgi( 270, 515):����������
      R_(227)=0.0
C FDB50_temp_pts.fgi( 275, 517):��������� ������������ ����
      !Constant L_(34) = FDB50_temp_ptsClJ10324 /.false./
      L_(34)=L0_ilad
C FDB50_temp_pts.fgi( 271, 490):��������� ���. ������������� 
      R_(229) = 0.0
C FDB50_temp_pts.fgi( 242, 498):��������� (RE4) (�������)
      R_(226) = 20.0
C FDB50_temp_pts.fgi( 280, 520):��������� (RE4) (�������)
      R_(225) = 2000.0
C FDB50_temp_pts.fgi( 280, 523):��������� (RE4) (�������)
      L_(35)=I_emad.ne.I0_amad
C FDB50_temp_pts.fgi( 252, 485):���������� �������������
      if(L_(35)) then
         R_(228)=R8_imad
      else
         R_(228)=R_(229)
      endif
C FDB50_temp_pts.fgi( 259, 497):���� RE IN LO CH7
      if(L_(34)) then
         R8_ulad=R_(227)
      elseif(L_(34)) then
         R8_ulad=R8_ulad
      else
         R8_ulad=R8_ulad+deltat/R_(232)*R_(228)
      endif
      if(R8_ulad.gt.R_(225)) then
         R8_ulad=R_(225)
      elseif(R8_ulad.lt.R_(226)) then
         R8_ulad=R_(226)
      endif
C FDB50_temp_pts.fgi( 284, 498):����������
      R8_olad=R8_ulad
C FDB50_temp_pts.fgi( 318, 498):������,PST1_B12_W_intTt
      if(L_(35)) then
         R8_elad=R_(223)
      else
         R8_elad=R_(224)
      endif
C FDB50_temp_pts.fgi( 259, 505):���� RE IN LO CH7
      R_(234) = 0.0
C FDB50_temp_pts.fgi( 256, 552):��������� (RE4) (�������)
      R_(233) = 1.0
C FDB50_temp_pts.fgi( 256, 550):��������� (RE4) (�������)
      R_(240) = 400.0
C FDB50_temp_pts.fgi( 256, 559):��������� (RE4) (�������)
      R_(241) = 10.0
C FDB50_temp_pts.fgi( 256, 561):��������� (RE4) (�������)
      R_(242) = R_(241) * R_(240)
C FDB50_temp_pts.fgi( 270, 561):����������
      R_(237)=0.0
C FDB50_temp_pts.fgi( 275, 563):��������� ������������ ����
      !Constant L_(36) = FDB50_temp_ptsClJ10289 /.false./
      L_(36)=L0_umad
C FDB50_temp_pts.fgi( 271, 536):��������� ���. ������������� 
      R_(239) = 0.0
C FDB50_temp_pts.fgi( 242, 544):��������� (RE4) (�������)
      R_(236) = 20.0
C FDB50_temp_pts.fgi( 280, 566):��������� (RE4) (�������)
      R_(235) = 2000.0
C FDB50_temp_pts.fgi( 280, 569):��������� (RE4) (�������)
      L_(37)=I_opad.ne.I0_ipad
C FDB50_temp_pts.fgi( 252, 531):���������� �������������
      if(L_(37)) then
         R8_omad=R_(233)
      else
         R8_omad=R_(234)
      endif
C FDB50_temp_pts.fgi( 259, 551):���� RE IN LO CH7
      if(L_(37)) then
         R_(238)=R8_upad
      else
         R_(238)=R_(239)
      endif
C FDB50_temp_pts.fgi( 259, 543):���� RE IN LO CH7
      if(L_(36)) then
         R8_epad=R_(237)
      elseif(L_(36)) then
         R8_epad=R8_epad
      else
         R8_epad=R8_epad+deltat/R_(242)*R_(238)
      endif
      if(R8_epad.gt.R_(235)) then
         R8_epad=R_(235)
      elseif(R8_epad.lt.R_(236)) then
         R8_epad=R_(236)
      endif
C FDB50_temp_pts.fgi( 284, 544):����������
      R8_apad=R8_epad
C FDB50_temp_pts.fgi( 318, 544):������,PST1_B11_W_intTt
      R_(244) = 0.0
C FDB50_temp_pts.fgi(  82, 114):��������� (RE4) (�������)
      R_(243) = 1.0
C FDB50_temp_pts.fgi(  82, 112):��������� (RE4) (�������)
      R_(250) = 400.0
C FDB50_temp_pts.fgi(  82, 121):��������� (RE4) (�������)
      R_(251) = 10.0
C FDB50_temp_pts.fgi(  82, 123):��������� (RE4) (�������)
      R_(252) = R_(251) * R_(250)
C FDB50_temp_pts.fgi(  96, 123):����������
      R_(247)=0.0
C FDB50_temp_pts.fgi( 101, 125):��������� ������������ ����
      !Constant L_(38) = FDB50_temp_ptsClJ10254 /.false./
      L_(38)=L0_erad
C FDB50_temp_pts.fgi(  97,  98):��������� ���. ������������� 
      R_(249) = 0.0
C FDB50_temp_pts.fgi(  68, 106):��������� (RE4) (�������)
      R_(246) = 20.0
C FDB50_temp_pts.fgi( 106, 128):��������� (RE4) (�������)
      R_(245) = 2000.0
C FDB50_temp_pts.fgi( 106, 131):��������� (RE4) (�������)
      L_(39)=I_asad.ne.I0_urad
C FDB50_temp_pts.fgi(  78,  93):���������� �������������
      if(L_(39)) then
         R_(248)=R8_esad
      else
         R_(248)=R_(249)
      endif
C FDB50_temp_pts.fgi(  85, 105):���� RE IN LO CH7
      if(L_(38)) then
         R8_orad=R_(247)
      elseif(L_(38)) then
         R8_orad=R8_orad
      else
         R8_orad=R8_orad+deltat/R_(252)*R_(248)
      endif
      if(R8_orad.gt.R_(245)) then
         R8_orad=R_(245)
      elseif(R8_orad.lt.R_(246)) then
         R8_orad=R_(246)
      endif
C FDB50_temp_pts.fgi( 110, 106):����������
      R8_irad=R8_orad
C FDB50_temp_pts.fgi( 144, 106):������,PST1_B10_W_intTt
      if(L_(39)) then
         R8_arad=R_(243)
      else
         R8_arad=R_(244)
      endif
C FDB50_temp_pts.fgi(  85, 113):���� RE IN LO CH7
      R_(254) = 0.0
C FDB50_temp_pts.fgi(  82, 160):��������� (RE4) (�������)
      R_(253) = 1.0
C FDB50_temp_pts.fgi(  82, 158):��������� (RE4) (�������)
      R_(260) = 400.0
C FDB50_temp_pts.fgi(  82, 167):��������� (RE4) (�������)
      R_(261) = 10.0
C FDB50_temp_pts.fgi(  82, 169):��������� (RE4) (�������)
      R_(262) = R_(261) * R_(260)
C FDB50_temp_pts.fgi(  96, 169):����������
      R_(257)=0.0
C FDB50_temp_pts.fgi( 101, 171):��������� ������������ ����
      !Constant L_(40) = FDB50_temp_ptsClJ10219 /.false./
      L_(40)=L0_osad
C FDB50_temp_pts.fgi(  97, 144):��������� ���. ������������� 
      R_(259) = 0.0
C FDB50_temp_pts.fgi(  68, 152):��������� (RE4) (�������)
      R_(256) = 20.0
C FDB50_temp_pts.fgi( 106, 174):��������� (RE4) (�������)
      R_(255) = 2000.0
C FDB50_temp_pts.fgi( 106, 177):��������� (RE4) (�������)
      L_(41)=I_itad.ne.I0_etad
C FDB50_temp_pts.fgi(  78, 139):���������� �������������
      if(L_(41)) then
         R8_isad=R_(253)
      else
         R8_isad=R_(254)
      endif
C FDB50_temp_pts.fgi(  85, 159):���� RE IN LO CH7
      if(L_(41)) then
         R_(258)=R8_otad
      else
         R_(258)=R_(259)
      endif
C FDB50_temp_pts.fgi(  85, 151):���� RE IN LO CH7
      if(L_(40)) then
         R8_atad=R_(257)
      elseif(L_(40)) then
         R8_atad=R8_atad
      else
         R8_atad=R8_atad+deltat/R_(262)*R_(258)
      endif
      if(R8_atad.gt.R_(255)) then
         R8_atad=R_(255)
      elseif(R8_atad.lt.R_(256)) then
         R8_atad=R_(256)
      endif
C FDB50_temp_pts.fgi( 110, 152):����������
      R8_usad=R8_atad
C FDB50_temp_pts.fgi( 144, 152):������,PST1_B09_W_intTt
      R_(264) = 0.0
C FDB50_temp_pts.fgi(  82, 214):��������� (RE4) (�������)
      R_(263) = 1.0
C FDB50_temp_pts.fgi(  82, 212):��������� (RE4) (�������)
      R_(270) = 400.0
C FDB50_temp_pts.fgi(  82, 221):��������� (RE4) (�������)
      R_(271) = 10.0
C FDB50_temp_pts.fgi(  82, 223):��������� (RE4) (�������)
      R_(272) = R_(271) * R_(270)
C FDB50_temp_pts.fgi(  96, 223):����������
      R_(267)=0.0
C FDB50_temp_pts.fgi( 101, 225):��������� ������������ ����
      !Constant L_(42) = FDB50_temp_ptsClJ10184 /.false./
      L_(42)=L0_avad
C FDB50_temp_pts.fgi(  97, 198):��������� ���. ������������� 
      R_(269) = 0.0
C FDB50_temp_pts.fgi(  68, 206):��������� (RE4) (�������)
      R_(266) = 20.0
C FDB50_temp_pts.fgi( 106, 228):��������� (RE4) (�������)
      R_(265) = 2000.0
C FDB50_temp_pts.fgi( 106, 231):��������� (RE4) (�������)
      L_(43)=I_uvad.ne.I0_ovad
C FDB50_temp_pts.fgi(  78, 193):���������� �������������
      if(L_(43)) then
         R8_utad=R_(263)
      else
         R8_utad=R_(264)
      endif
C FDB50_temp_pts.fgi(  85, 213):���� RE IN LO CH7
      if(L_(43)) then
         R_(268)=R8_axad
      else
         R_(268)=R_(269)
      endif
C FDB50_temp_pts.fgi(  85, 205):���� RE IN LO CH7
      if(L_(42)) then
         R8_ivad=R_(267)
      elseif(L_(42)) then
         R8_ivad=R8_ivad
      else
         R8_ivad=R8_ivad+deltat/R_(272)*R_(268)
      endif
      if(R8_ivad.gt.R_(265)) then
         R8_ivad=R_(265)
      elseif(R8_ivad.lt.R_(266)) then
         R8_ivad=R_(266)
      endif
C FDB50_temp_pts.fgi( 110, 206):����������
      R8_evad=R8_ivad
C FDB50_temp_pts.fgi( 144, 206):������,PST1_B08_W_intTt
      R_(274) = 0.0
C FDB50_temp_pts.fgi(  82, 260):��������� (RE4) (�������)
      R_(273) = 1.0
C FDB50_temp_pts.fgi(  82, 258):��������� (RE4) (�������)
      R_(280) = 400.0
C FDB50_temp_pts.fgi(  82, 267):��������� (RE4) (�������)
      R_(281) = 10.0
C FDB50_temp_pts.fgi(  82, 269):��������� (RE4) (�������)
      R_(282) = R_(281) * R_(280)
C FDB50_temp_pts.fgi(  96, 269):����������
      R_(277)=0.0
C FDB50_temp_pts.fgi( 101, 271):��������� ������������ ����
      !Constant L_(44) = FDB50_temp_ptsClJ10149 /.false./
      L_(44)=L0_ixad
C FDB50_temp_pts.fgi(  97, 244):��������� ���. ������������� 
      R_(279) = 0.0
C FDB50_temp_pts.fgi(  68, 252):��������� (RE4) (�������)
      R_(276) = 20.0
C FDB50_temp_pts.fgi( 106, 274):��������� (RE4) (�������)
      R_(275) = 2000.0
C FDB50_temp_pts.fgi( 106, 277):��������� (RE4) (�������)
      L_(45)=I_ebed.ne.I0_abed
C FDB50_temp_pts.fgi(  78, 239):���������� �������������
      if(L_(45)) then
         R_(278)=R8_ibed
      else
         R_(278)=R_(279)
      endif
C FDB50_temp_pts.fgi(  85, 251):���� RE IN LO CH7
      if(L_(44)) then
         R8_uxad=R_(277)
      elseif(L_(44)) then
         R8_uxad=R8_uxad
      else
         R8_uxad=R8_uxad+deltat/R_(282)*R_(278)
      endif
      if(R8_uxad.gt.R_(275)) then
         R8_uxad=R_(275)
      elseif(R8_uxad.lt.R_(276)) then
         R8_uxad=R_(276)
      endif
C FDB50_temp_pts.fgi( 110, 252):����������
      R8_oxad=R8_uxad
C FDB50_temp_pts.fgi( 144, 252):������,PST1_B07_W_intTt
      if(L_(45)) then
         R8_exad=R_(273)
      else
         R8_exad=R_(274)
      endif
C FDB50_temp_pts.fgi(  85, 259):���� RE IN LO CH7
      R_(284) = 0.0
C FDB50_temp_pts.fgi(  82, 309):��������� (RE4) (�������)
      R_(283) = 1.0
C FDB50_temp_pts.fgi(  82, 307):��������� (RE4) (�������)
      R_(290) = 400.0
C FDB50_temp_pts.fgi(  82, 316):��������� (RE4) (�������)
      R_(291) = 10.0
C FDB50_temp_pts.fgi(  82, 318):��������� (RE4) (�������)
      R_(292) = R_(291) * R_(290)
C FDB50_temp_pts.fgi(  96, 318):����������
      R_(287)=0.0
C FDB50_temp_pts.fgi( 101, 320):��������� ������������ ����
      !Constant L_(46) = FDB50_temp_ptsClJ10114 /.false./
      L_(46)=L0_ubed
C FDB50_temp_pts.fgi(  97, 293):��������� ���. ������������� 
      R_(289) = 0.0
C FDB50_temp_pts.fgi(  68, 301):��������� (RE4) (�������)
      R_(286) = 20.0
C FDB50_temp_pts.fgi( 106, 323):��������� (RE4) (�������)
      R_(285) = 2000.0
C FDB50_temp_pts.fgi( 106, 326):��������� (RE4) (�������)
      L_(47)=I_oded.ne.I0_ided
C FDB50_temp_pts.fgi(  78, 288):���������� �������������
      if(L_(47)) then
         R_(288)=R8_uded
      else
         R_(288)=R_(289)
      endif
C FDB50_temp_pts.fgi(  85, 300):���� RE IN LO CH7
      if(L_(46)) then
         R8_eded=R_(287)
      elseif(L_(46)) then
         R8_eded=R8_eded
      else
         R8_eded=R8_eded+deltat/R_(292)*R_(288)
      endif
      if(R8_eded.gt.R_(285)) then
         R8_eded=R_(285)
      elseif(R8_eded.lt.R_(286)) then
         R8_eded=R_(286)
      endif
C FDB50_temp_pts.fgi( 110, 301):����������
      R8_aded=R8_eded
C FDB50_temp_pts.fgi( 144, 301):������,PST1_B06_W_intTt
      if(L_(47)) then
         R8_obed=R_(283)
      else
         R8_obed=R_(284)
      endif
C FDB50_temp_pts.fgi(  85, 308):���� RE IN LO CH7
      R_(294) = 0.0
C FDB50_temp_pts.fgi(  82, 355):��������� (RE4) (�������)
      R_(293) = 1.0
C FDB50_temp_pts.fgi(  82, 353):��������� (RE4) (�������)
      R_(300) = 400.0
C FDB50_temp_pts.fgi(  82, 362):��������� (RE4) (�������)
      R_(301) = 10.0
C FDB50_temp_pts.fgi(  82, 364):��������� (RE4) (�������)
      R_(302) = R_(301) * R_(300)
C FDB50_temp_pts.fgi(  96, 364):����������
      R_(297)=0.0
C FDB50_temp_pts.fgi( 101, 366):��������� ������������ ����
      !Constant L_(48) = FDB50_temp_ptsClJ10079 /.false./
      L_(48)=L0_efed
C FDB50_temp_pts.fgi(  97, 339):��������� ���. ������������� 
      R_(299) = 0.0
C FDB50_temp_pts.fgi(  68, 347):��������� (RE4) (�������)
      R_(296) = 20.0
C FDB50_temp_pts.fgi( 106, 369):��������� (RE4) (�������)
      R_(295) = 2000.0
C FDB50_temp_pts.fgi( 106, 372):��������� (RE4) (�������)
      L_(49)=I_aked.ne.I0_ufed
C FDB50_temp_pts.fgi(  78, 334):���������� �������������
      if(L_(49)) then
         R8_afed=R_(293)
      else
         R8_afed=R_(294)
      endif
C FDB50_temp_pts.fgi(  85, 354):���� RE IN LO CH7
      if(L_(49)) then
         R_(298)=R8_eked
      else
         R_(298)=R_(299)
      endif
C FDB50_temp_pts.fgi(  85, 346):���� RE IN LO CH7
      if(L_(48)) then
         R8_ofed=R_(297)
      elseif(L_(48)) then
         R8_ofed=R8_ofed
      else
         R8_ofed=R8_ofed+deltat/R_(302)*R_(298)
      endif
      if(R8_ofed.gt.R_(295)) then
         R8_ofed=R_(295)
      elseif(R8_ofed.lt.R_(296)) then
         R8_ofed=R_(296)
      endif
C FDB50_temp_pts.fgi( 110, 347):����������
      R8_ifed=R8_ofed
C FDB50_temp_pts.fgi( 144, 347):������,PST1_B05_W_intTt
      R_(304) = 0.0
C FDB50_temp_pts.fgi(  82, 411):��������� (RE4) (�������)
      R_(303) = 1.0
C FDB50_temp_pts.fgi(  82, 409):��������� (RE4) (�������)
      R_(310) = 400.0
C FDB50_temp_pts.fgi(  82, 418):��������� (RE4) (�������)
      R_(311) = 10.0
C FDB50_temp_pts.fgi(  82, 420):��������� (RE4) (�������)
      R_(312) = R_(311) * R_(310)
C FDB50_temp_pts.fgi(  96, 420):����������
      R_(307)=0.0
C FDB50_temp_pts.fgi( 101, 422):��������� ������������ ����
      !Constant L_(50) = FDB50_temp_ptsClJ10044 /.false./
      L_(50)=L0_oked
C FDB50_temp_pts.fgi(  97, 395):��������� ���. ������������� 
      R_(309) = 0.0
C FDB50_temp_pts.fgi(  68, 403):��������� (RE4) (�������)
      R_(306) = 20.0
C FDB50_temp_pts.fgi( 106, 425):��������� (RE4) (�������)
      R_(305) = 2000.0
C FDB50_temp_pts.fgi( 106, 428):��������� (RE4) (�������)
      L_(51)=I_iled.ne.I0_eled
C FDB50_temp_pts.fgi(  78, 390):���������� �������������
      if(L_(51)) then
         R_(308)=R8_oled
      else
         R_(308)=R_(309)
      endif
C FDB50_temp_pts.fgi(  85, 402):���� RE IN LO CH7
      if(L_(50)) then
         R8_aled=R_(307)
      elseif(L_(50)) then
         R8_aled=R8_aled
      else
         R8_aled=R8_aled+deltat/R_(312)*R_(308)
      endif
      if(R8_aled.gt.R_(305)) then
         R8_aled=R_(305)
      elseif(R8_aled.lt.R_(306)) then
         R8_aled=R_(306)
      endif
C FDB50_temp_pts.fgi( 110, 403):����������
      R8_uked=R8_aled
C FDB50_temp_pts.fgi( 144, 403):������,PST1_B04_W_intTt
      if(L_(51)) then
         R8_iked=R_(303)
      else
         R8_iked=R_(304)
      endif
C FDB50_temp_pts.fgi(  85, 410):���� RE IN LO CH7
      R_(314) = 0.0
C FDB50_temp_pts.fgi(  82, 457):��������� (RE4) (�������)
      R_(313) = 1.0
C FDB50_temp_pts.fgi(  82, 455):��������� (RE4) (�������)
      R_(320) = 400.0
C FDB50_temp_pts.fgi(  82, 464):��������� (RE4) (�������)
      R_(321) = 10.0
C FDB50_temp_pts.fgi(  82, 466):��������� (RE4) (�������)
      R_(322) = R_(321) * R_(320)
C FDB50_temp_pts.fgi(  96, 466):����������
      R_(317)=0.0
C FDB50_temp_pts.fgi( 101, 468):��������� ������������ ����
      !Constant L_(52) = FDB50_temp_ptsClJ10009 /.false./
      L_(52)=L0_amed
C FDB50_temp_pts.fgi(  97, 441):��������� ���. ������������� 
      R_(319) = 0.0
C FDB50_temp_pts.fgi(  68, 449):��������� (RE4) (�������)
      R_(316) = 20.0
C FDB50_temp_pts.fgi( 106, 471):��������� (RE4) (�������)
      R_(315) = 2000.0
C FDB50_temp_pts.fgi( 106, 474):��������� (RE4) (�������)
      L_(53)=I_umed.ne.I0_omed
C FDB50_temp_pts.fgi(  78, 436):���������� �������������
      if(L_(53)) then
         R8_uled=R_(313)
      else
         R8_uled=R_(314)
      endif
C FDB50_temp_pts.fgi(  85, 456):���� RE IN LO CH7
      if(L_(53)) then
         R_(318)=R8_aped
      else
         R_(318)=R_(319)
      endif
C FDB50_temp_pts.fgi(  85, 448):���� RE IN LO CH7
      if(L_(52)) then
         R8_imed=R_(317)
      elseif(L_(52)) then
         R8_imed=R8_imed
      else
         R8_imed=R8_imed+deltat/R_(322)*R_(318)
      endif
      if(R8_imed.gt.R_(315)) then
         R8_imed=R_(315)
      elseif(R8_imed.lt.R_(316)) then
         R8_imed=R_(316)
      endif
C FDB50_temp_pts.fgi( 110, 449):����������
      R8_emed=R8_imed
C FDB50_temp_pts.fgi( 144, 449):������,PST1_B03_W_intTt
      R_(324) = 0.0
C FDB50_temp_pts.fgi(  82, 506):��������� (RE4) (�������)
      R_(323) = 1.0
C FDB50_temp_pts.fgi(  82, 504):��������� (RE4) (�������)
      R_(330) = 400.0
C FDB50_temp_pts.fgi(  82, 513):��������� (RE4) (�������)
      R_(331) = 10.0
C FDB50_temp_pts.fgi(  82, 515):��������� (RE4) (�������)
      R_(332) = R_(331) * R_(330)
C FDB50_temp_pts.fgi(  96, 515):����������
      R_(327)=0.0
C FDB50_temp_pts.fgi( 101, 517):��������� ������������ ����
      !Constant L_(54) = FDB50_temp_ptsClJ9974 /.false./
      L_(54)=L0_iped
C FDB50_temp_pts.fgi(  97, 490):��������� ���. ������������� 
      R_(329) = 0.0
C FDB50_temp_pts.fgi(  68, 498):��������� (RE4) (�������)
      R_(326) = 20.0
C FDB50_temp_pts.fgi( 106, 520):��������� (RE4) (�������)
      R_(325) = 2000.0
C FDB50_temp_pts.fgi( 106, 523):��������� (RE4) (�������)
      L_(55)=I_ered.ne.I0_ared
C FDB50_temp_pts.fgi(  78, 485):���������� �������������
      if(L_(55)) then
         R8_eped=R_(323)
      else
         R8_eped=R_(324)
      endif
C FDB50_temp_pts.fgi(  85, 505):���� RE IN LO CH7
      if(L_(55)) then
         R_(328)=R8_ired
      else
         R_(328)=R_(329)
      endif
C FDB50_temp_pts.fgi(  85, 497):���� RE IN LO CH7
      if(L_(54)) then
         R8_uped=R_(327)
      elseif(L_(54)) then
         R8_uped=R8_uped
      else
         R8_uped=R8_uped+deltat/R_(332)*R_(328)
      endif
      if(R8_uped.gt.R_(325)) then
         R8_uped=R_(325)
      elseif(R8_uped.lt.R_(326)) then
         R8_uped=R_(326)
      endif
C FDB50_temp_pts.fgi( 110, 498):����������
      R8_oped=R8_uped
C FDB50_temp_pts.fgi( 144, 498):������,PST1_B02_W_intTt
      R_(334) = 0.0
C FDB50_temp_pts.fgi(  82, 552):��������� (RE4) (�������)
      R_(333) = 1.0
C FDB50_temp_pts.fgi(  82, 550):��������� (RE4) (�������)
      R_(340) = 400.0
C FDB50_temp_pts.fgi(  82, 559):��������� (RE4) (�������)
      R_(341) = 10.0
C FDB50_temp_pts.fgi(  82, 561):��������� (RE4) (�������)
      R_(342) = R_(341) * R_(340)
C FDB50_temp_pts.fgi(  96, 561):����������
      R_(337)=0.0
C FDB50_temp_pts.fgi( 101, 563):��������� ������������ ����
      !Constant L_(56) = FDB50_temp_ptsClJ8470 /.false./
      L_(56)=L0_ured
C FDB50_temp_pts.fgi(  97, 536):��������� ���. ������������� 
      R_(339) = 0.0
C FDB50_temp_pts.fgi(  68, 544):��������� (RE4) (�������)
      R_(336) = 20.0
C FDB50_temp_pts.fgi( 106, 566):��������� (RE4) (�������)
      R_(335) = 2000.0
C FDB50_temp_pts.fgi( 106, 569):��������� (RE4) (�������)
      L_(57)=I_osed.ne.I0_ised
C FDB50_temp_pts.fgi(  78, 531):���������� �������������
      if(L_(57)) then
         R_(338)=R8_used
      else
         R_(338)=R_(339)
      endif
C FDB50_temp_pts.fgi(  85, 543):���� RE IN LO CH7
      if(L_(56)) then
         R8_esed=R_(337)
      elseif(L_(56)) then
         R8_esed=R8_esed
      else
         R8_esed=R8_esed+deltat/R_(342)*R_(338)
      endif
      if(R8_esed.gt.R_(335)) then
         R8_esed=R_(335)
      elseif(R8_esed.lt.R_(336)) then
         R8_esed=R_(336)
      endif
C FDB50_temp_pts.fgi( 110, 544):����������
      R8_ased=R8_esed
C FDB50_temp_pts.fgi( 144, 544):������,PST1_B01_W_intTt
      if(L_(57)) then
         R8_ored=R_(333)
      else
         R8_ored=R_(334)
      endif
C FDB50_temp_pts.fgi(  85, 551):���� RE IN LO CH7
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ited,R_ibid,REAL(1,4
     &),
     & REAL(R_oved,4),I_ebid,REAL(R_aved,4),
     & REAL(R_eved,4),REAL(R_eted,4),
     & REAL(R_ated,4),REAL(R_axed,4),L_exed,REAL(R_ixed,4
     &),L_oxed,
     & L_uxed,R_ived,REAL(R_uted,4),REAL(R_oted,4),L_abid
     &)
      !}
C FDB50_vlv.fgi( 242, 138):���������� ������� ��� 2,20FDB50CF001XQ01
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_odev,4),R8_oxav,I_alev
     &,I_olev,I_ukev,
     & C8_ebev,I_ilev,R_adev,R_ubev,R_otav,
     & REAL(R_avav,4),R_uvav,REAL(R_exav,4),
     & R_evav,REAL(R_ovav,4),I_ikev,I_ulev,I_elev,I_ekev,L_ixav
     &,
     & L_emev,L_epev,L_axav,L_ivav,
     & L_abev,L_uxav,L_omev,L_utav,L_obev,L_upev,L_edev,
     & L_idev,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_usav
     &,L_atav,L_imev,
     & I_amev,L_ipev,R_akev,REAL(R_ibev,4),L_opev,L_etav,L_arev
     &,L_itav,
     & L_udev,L_afev,L_efev,L_ofev,L_ufev,L_ifev)
      !}

      if(L_ufev.or.L_ofev.or.L_ifev.or.L_efev.or.L_afev.or.L_udev
     &) then      
                  I_okev = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_okev = z'40000000'
      endif
C FDB50_vlv.fgi( 445, 195):���� ���������� �������� ��������,20FDB50AA116
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_abiv,4),R8_avev,I_ifiv
     &,I_akiv,I_efiv,
     & C8_ovev,I_ufiv,R_ixev,R_exev,R_asev,
     & REAL(R_isev,4),R_etev,REAL(R_otev,4),
     & R_osev,REAL(R_atev,4),I_udiv,I_ekiv,I_ofiv,I_odiv,L_utev
     &,
     & L_okiv,L_oliv,L_itev,L_usev,
     & L_ivev,L_evev,L_aliv,L_esev,L_axev,L_emiv,L_oxev,
     & L_uxev,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_erev
     &,L_irev,L_ukiv,
     & I_ikiv,L_uliv,R_idiv,REAL(R_uvev,4),L_amiv,L_orev,L_imiv
     &,L_urev,
     & L_ebiv,L_ibiv,L_obiv,L_adiv,L_ediv,L_ubiv)
      !}

      if(L_ediv.or.L_adiv.or.L_ubiv.or.L_obiv.or.L_ibiv.or.L_ebiv
     &) then      
                  I_afiv = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_afiv = z'40000000'
      endif
C FDB50_vlv.fgi( 445, 219):���� ���������� �������� ��������,20FDB50AA114
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_iviv,4),R8_isiv,I_ubov
     &,I_idov,I_obov,
     & C8_ativ,I_edov,R_utiv,R_otiv,R_ipiv,
     & REAL(R_upiv,4),R_oriv,REAL(R_asiv,4),
     & R_ariv,REAL(R_iriv,4),I_ebov,I_odov,I_adov,I_abov,L_esiv
     &,
     & L_afov,L_akov,L_uriv,L_eriv,
     & L_usiv,L_osiv,L_ifov,L_opiv,L_itiv,L_okov,L_aviv,
     & L_eviv,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_omiv
     &,L_umiv,L_efov,
     & I_udov,L_ekov,R_uxiv,REAL(R_etiv,4),L_ikov,L_apiv,L_ukov
     &,L_epiv,
     & L_oviv,L_uviv,L_axiv,L_ixiv,L_oxiv,L_exiv)
      !}

      if(L_oxiv.or.L_ixiv.or.L_exiv.or.L_axiv.or.L_uviv.or.L_oviv
     &) then      
                  I_ibov = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ibov = z'40000000'
      endif
C FDB50_vlv.fgi( 445, 243):���� ���������� �������� ��������,20FDB50AA115
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_upuv,4),R8_uluv,I_etuv
     &,I_utuv,I_atuv,
     & C8_imuv,I_otuv,R_epuv,R_apuv,R_ufuv,
     & REAL(R_ekuv,4),R_aluv,REAL(R_iluv,4),
     & R_ikuv,REAL(R_ukuv,4),I_osuv,I_avuv,I_ituv,I_isuv,L_oluv
     &,
     & L_ivuv,L_ixuv,L_eluv,L_okuv,
     & L_emuv,L_amuv,L_uvuv,L_akuv,L_umuv,L_abax,L_ipuv,
     & L_opuv,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_afuv
     &,L_efuv,L_ovuv,
     & I_evuv,L_oxuv,R_esuv,REAL(R_omuv,4),L_uxuv,L_ifuv,L_ebax
     &,L_ofuv,
     & L_aruv,L_eruv,L_iruv,L_uruv,L_asuv,L_oruv)
      !}

      if(L_asuv.or.L_uruv.or.L_oruv.or.L_iruv.or.L_eruv.or.L_aruv
     &) then      
                  I_usuv = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_usuv = z'40000000'
      endif
C FDB50_vlv.fgi( 447, 143):���� ���������� �������� ��������,20FDB50AA113
      !{
      Call REG_MAN(deltat,REAL(R_emax,4),R8_ekax,R_usax,R_asax
     &,
     & L_evax,R_atax,R_esax,L_ivax,R_ilax,R_elax,
     & R_edax,REAL(R_odax,4),R_ifax,
     & REAL(R_ufax,4),R_udax,REAL(R_efax,4),I_upax,
     & L_akax,L_irax,L_etax,L_ofax,L_afax,
     & L_okax,L_ikax,L_urax,L_idax,L_alax,L_utax,L_ulax,
     & L_amax,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_ibax
     &,L_obax,L_orax,
     & I_erax,L_itax,R_opax,REAL(R_ukax,4),L_ubax,L_adax,L_imax
     &,L_omax,
     & L_umax,L_epax,L_ipax,L_apax)
      !}

      if(L_ipax.or.L_epax.or.L_apax.or.L_umax.or.L_omax.or.L_imax
     &) then      
                  I_arax = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_arax = z'40000000'
      endif


      R_olax=100*R8_ekax
C FDB50_vlv.fgi( 432, 117):���� ���������� ������������ ��������,20FDB50AA201
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ikex,4),R8_idex,I_umex
     &,I_ipex,I_omex,
     & C8_afex,I_epex,R_ufex,R_ofex,R_ixax,
     & REAL(R_uxax,4),R_obex,REAL(R_adex,4),
     & R_abex,REAL(R_ibex,4),I_emex,I_opex,I_apex,I_amex,L_edex
     &,
     & L_arex,L_asex,L_ubex,L_ebex,
     & L_udex,L_odex,L_irex,L_oxax,L_ifex,L_osex,L_akex,
     & L_ekex,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_ovax
     &,L_uvax,L_erex,
     & I_upex,L_esex,R_ulex,REAL(R_efex,4),L_isex,L_axax,L_usex
     &,L_exax,
     & L_okex,L_ukex,L_alex,L_ilex,L_olex,L_elex)
      !}

      if(L_olex.or.L_ilex.or.L_elex.or.L_alex.or.L_ukex.or.L_okex
     &) then      
                  I_imex = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_imex = z'40000000'
      endif
C FDB50_vlv.fgi( 461, 167):���� ���������� �������� ��������,20FDB50AA118
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_udix,4),R8_uxex,I_elix
     &,I_ulix,I_alix,
     & C8_ibix,I_olix,R_edix,R_adix,R_utex,
     & REAL(R_evex,4),R_axex,REAL(R_ixex,4),
     & R_ivex,REAL(R_uvex,4),I_okix,I_amix,I_ilix,I_ikix,L_oxex
     &,
     & L_imix,L_ipix,L_exex,L_ovex,
     & L_ebix,L_abix,L_umix,L_avex,L_ubix,L_arix,L_idix,
     & L_odix,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_atex
     &,L_etex,L_omix,
     & I_emix,L_opix,R_ekix,REAL(R_obix,4),L_upix,L_itex,L_erix
     &,L_otex,
     & L_afix,L_efix,L_ifix,L_ufix,L_akix,L_ofix)
      !}

      if(L_akix.or.L_ufix.or.L_ofix.or.L_ifix.or.L_efix.or.L_afix
     &) then      
                  I_ukix = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ukix = z'40000000'
      endif
C FDB50_vlv.fgi( 447, 167):���� ���������� �������� ��������,20FDB50AA138
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ebox,4),R8_evix,I_ofox
     &,I_ekox,I_ifox,
     & C8_uvix,I_akox,R_oxix,R_ixix,R_esix,
     & REAL(R_osix,4),R_itix,REAL(R_utix,4),
     & R_usix,REAL(R_etix,4),I_afox,I_ikox,I_ufox,I_udox,L_avix
     &,
     & L_ukox,L_ulox,L_otix,L_atix,
     & L_ovix,L_ivix,L_elox,L_isix,L_exix,L_imox,L_uxix,
     & L_abox,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_irix
     &,L_orix,L_alox,
     & I_okox,L_amox,R_odox,REAL(R_axix,4),L_emox,L_urix,L_omox
     &,L_asix,
     & L_ibox,L_obox,L_ubox,L_edox,L_idox,L_adox)
      !}

      if(L_idox.or.L_edox.or.L_adox.or.L_ubox.or.L_obox.or.L_ibox
     &) then      
                  I_efox = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_efox = z'40000000'
      endif
C FDB50_vlv.fgi( 445, 268):���� ���������� �������� ��������,20FDB50AA110
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ovox,4),R8_osox,I_adux
     &,I_odux,I_ubux,
     & C8_etox,I_idux,R_avox,R_utox,R_opox,
     & REAL(R_arox,4),R_urox,REAL(R_esox,4),
     & R_erox,REAL(R_orox,4),I_ibux,I_udux,I_edux,I_ebux,L_isox
     &,
     & L_efux,L_ekux,L_asox,L_irox,
     & L_atox,L_usox,L_ofux,L_upox,L_otox,L_ukux,L_evox,
     & L_ivox,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_umox
     &,L_apox,L_ifux,
     & I_afux,L_ikux,R_abux,REAL(R_itox,4),L_okux,L_epox,L_alux
     &,L_ipox,
     & L_uvox,L_axox,L_exox,L_oxox,L_uxox,L_ixox)
      !}

      if(L_uxox.or.L_oxox.or.L_ixox.or.L_exox.or.L_axox.or.L_uvox
     &) then      
                  I_obux = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_obux = z'40000000'
      endif
C FDB50_vlv.fgi( 432, 268):���� ���������� �������� ��������,20FDB50AA101
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_atux,4),R8_arux,I_ixux
     &,I_ababe,I_exux,
     & C8_orux,I_uxux,R_isux,R_esux,R_amux,
     & REAL(R_imux,4),R_epux,REAL(R_opux,4),
     & R_omux,REAL(R_apux,4),I_uvux,I_ebabe,I_oxux,I_ovux
     &,L_upux,
     & L_obabe,L_odabe,L_ipux,L_umux,
     & L_irux,L_erux,L_adabe,L_emux,L_asux,L_efabe,L_osux
     &,
     & L_usux,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_elux
     &,L_ilux,L_ubabe,
     & I_ibabe,L_udabe,R_ivux,REAL(R_urux,4),L_afabe,L_olux
     &,L_ifabe,L_ulux,
     & L_etux,L_itux,L_otux,L_avux,L_evux,L_utux)
      !}

      if(L_evux.or.L_avux.or.L_utux.or.L_otux.or.L_itux.or.L_etux
     &) then      
                  I_axux = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_axux = z'40000000'
      endif
C FDB50_vlv.fgi( 432, 143):���� ���������� �������� ��������,20FDB50AA104
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_irabe,4),R8_imabe,I_utabe
     &,I_ivabe,I_otabe,
     & C8_apabe,I_evabe,R_upabe,R_opabe,R_ikabe,
     & REAL(R_ukabe,4),R_olabe,REAL(R_amabe,4),
     & R_alabe,REAL(R_ilabe,4),I_etabe,I_ovabe,I_avabe,I_atabe
     &,L_emabe,
     & L_axabe,L_abebe,L_ulabe,L_elabe,
     & L_umabe,L_omabe,L_ixabe,L_okabe,L_ipabe,L_obebe,L_arabe
     &,
     & L_erabe,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_ofabe
     &,L_ufabe,L_exabe,
     & I_uvabe,L_ebebe,R_usabe,REAL(R_epabe,4),L_ibebe,L_akabe
     &,L_ubebe,L_ekabe,
     & L_orabe,L_urabe,L_asabe,L_isabe,L_osabe,L_esabe)
      !}

      if(L_osabe.or.L_isabe.or.L_esabe.or.L_asabe.or.L_urabe.or.L_orabe
     &) then      
                  I_itabe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_itabe = z'40000000'
      endif
C FDB50_vlv.fgi( 432, 195):���� ���������� �������� ��������,20FDB50AA100
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_umebe,4),R8_ukebe,I_esebe
     &,I_usebe,I_asebe,
     & C8_ilebe,I_osebe,R_emebe,R_amebe,R_udebe,
     & REAL(R_efebe,4),R_akebe,REAL(R_ikebe,4),
     & R_ifebe,REAL(R_ufebe,4),I_orebe,I_atebe,I_isebe,I_irebe
     &,L_okebe,
     & L_itebe,L_ivebe,L_ekebe,L_ofebe,
     & L_elebe,L_alebe,L_utebe,L_afebe,L_ulebe,L_axebe,L_imebe
     &,
     & L_omebe,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_adebe
     &,L_edebe,L_otebe,
     & I_etebe,L_ovebe,R_erebe,REAL(R_olebe,4),L_uvebe,L_idebe
     &,L_exebe,L_odebe,
     & L_apebe,L_epebe,L_ipebe,L_upebe,L_arebe,L_opebe)
      !}

      if(L_arebe.or.L_upebe.or.L_opebe.or.L_ipebe.or.L_epebe.or.L_apebe
     &) then      
                  I_urebe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_urebe = z'40000000'
      endif
C FDB50_vlv.fgi( 432, 219):���� ���������� �������� ��������,20FDB50AA109
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_elibe,4),R8_efibe,I_opibe
     &,I_eribe,I_ipibe,
     & C8_ufibe,I_aribe,R_okibe,R_ikibe,R_ebibe,
     & REAL(R_obibe,4),R_idibe,REAL(R_udibe,4),
     & R_ubibe,REAL(R_edibe,4),I_apibe,I_iribe,I_upibe,I_umibe
     &,L_afibe,
     & L_uribe,L_usibe,L_odibe,L_adibe,
     & L_ofibe,L_ifibe,L_esibe,L_ibibe,L_ekibe,L_itibe,L_ukibe
     &,
     & L_alibe,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_ixebe
     &,L_oxebe,L_asibe,
     & I_oribe,L_atibe,R_omibe,REAL(R_akibe,4),L_etibe,L_uxebe
     &,L_otibe,L_abibe,
     & L_ilibe,L_olibe,L_ulibe,L_emibe,L_imibe,L_amibe)
      !}

      if(L_imibe.or.L_emibe.or.L_amibe.or.L_ulibe.or.L_olibe.or.L_ilibe
     &) then      
                  I_epibe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_epibe = z'40000000'
      endif
C FDB50_vlv.fgi( 432, 167):���� ���������� �������� ��������,20FDB50AA102
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ofobe,4),R8_obobe,I_amobe
     &,I_omobe,I_ulobe,
     & C8_edobe,I_imobe,R_afobe,R_udobe,R_ovibe,
     & REAL(R_axibe,4),R_uxibe,REAL(R_ebobe,4),
     & R_exibe,REAL(R_oxibe,4),I_ilobe,I_umobe,I_emobe,I_elobe
     &,L_ibobe,
     & L_epobe,L_erobe,L_abobe,L_ixibe,
     & L_adobe,L_ubobe,L_opobe,L_uvibe,L_odobe,L_urobe,L_efobe
     &,
     & L_ifobe,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_utibe
     &,L_avibe,L_ipobe,
     & I_apobe,L_irobe,R_alobe,REAL(R_idobe,4),L_orobe,L_evibe
     &,L_asobe,L_ivibe,
     & L_ufobe,L_akobe,L_ekobe,L_okobe,L_ukobe,L_ikobe)
      !}

      if(L_ukobe.or.L_okobe.or.L_ikobe.or.L_ekobe.or.L_akobe.or.L_ufobe
     &) then      
                  I_olobe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_olobe = z'40000000'
      endif
C FDB50_vlv.fgi( 432, 243):���� ���������� �������� ��������,20FDB50AA103
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_adube,4),R8_axobe,I_ikube
     &,I_alube,I_ekube,
     & C8_oxobe,I_ukube,R_ibube,R_ebube,R_atobe,
     & REAL(R_itobe,4),R_evobe,REAL(R_ovobe,4),
     & R_otobe,REAL(R_avobe,4),I_ufube,I_elube,I_okube,I_ofube
     &,L_uvobe,
     & L_olube,L_omube,L_ivobe,L_utobe,
     & L_ixobe,L_exobe,L_amube,L_etobe,L_abube,L_epube,L_obube
     &,
     & L_ubube,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_esobe
     &,L_isobe,L_ulube,
     & I_ilube,L_umube,R_ifube,REAL(R_uxobe,4),L_apube,L_osobe
     &,L_ipube,L_usobe,
     & L_edube,L_idube,L_odube,L_afube,L_efube,L_udube)
      !}

      if(L_efube.or.L_afube.or.L_udube.or.L_odube.or.L_idube.or.L_edube
     &) then      
                  I_akube = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_akube = z'40000000'
      endif
C FDB50_vlv.fgi( 419, 268):���� ���������� �������� ��������,20FDB50AA140
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ixube,4),R8_itube,I_udade
     &,I_ifade,I_odade,
     & C8_avube,I_efade,R_uvube,R_ovube,R_irube,
     & REAL(R_urube,4),R_osube,REAL(R_atube,4),
     & R_asube,REAL(R_isube,4),I_edade,I_ofade,I_afade,I_adade
     &,L_etube,
     & L_akade,L_alade,L_usube,L_esube,
     & L_utube,L_otube,L_ikade,L_orube,L_ivube,L_olade,L_axube
     &,
     & L_exube,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_opube
     &,L_upube,L_ekade,
     & I_ufade,L_elade,R_ubade,REAL(R_evube,4),L_ilade,L_arube
     &,L_ulade,L_erube,
     & L_oxube,L_uxube,L_abade,L_ibade,L_obade,L_ebade)
      !}

      if(L_obade.or.L_ibade.or.L_ebade.or.L_abade.or.L_uxube.or.L_oxube
     &) then      
                  I_idade = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_idade = z'40000000'
      endif
C FDB50_vlv.fgi( 419, 143):���� ���������� �������� ��������,20FDB50AA135
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_urede,4),R8_umede,I_evede
     &,I_uvede,I_avede,
     & C8_ipede,I_ovede,R_erede,R_arede,R_ukede,
     & REAL(R_elede,4),R_amede,REAL(R_imede,4),
     & R_ilede,REAL(R_ulede,4),I_otede,I_axede,I_ivede,I_itede
     &,L_omede,
     & L_ixede,L_ibide,L_emede,L_olede,
     & L_epede,L_apede,L_uxede,L_alede,L_upede,L_adide,L_irede
     &,
     & L_orede,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_akede
     &,L_ekede,L_oxede,
     & I_exede,L_obide,R_etede,REAL(R_opede,4),L_ubide,L_ikede
     &,L_edide,L_okede,
     & L_asede,L_esede,L_isede,L_usede,L_atede,L_osede)
      !}

      if(L_atede.or.L_usede.or.L_osede.or.L_isede.or.L_esede.or.L_asede
     &) then      
                  I_utede = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_utede = z'40000000'
      endif
C FDB50_vlv.fgi( 419, 219):���� ���������� �������� ��������,20FDB50AA133
      !{
      Call REG_MAN(deltat,REAL(R_epide,4),R8_elide,R_utide
     &,R_atide,
     & L_exide,R_avide,R_etide,L_ixide,R_imide,R_emide,
     & R_efide,REAL(R_ofide,4),R_ikide,
     & REAL(R_ukide,4),R_ufide,REAL(R_ekide,4),I_uride,
     & L_alide,L_iside,L_evide,L_okide,L_akide,
     & L_olide,L_ilide,L_uside,L_ifide,L_amide,L_uvide,L_umide
     &,
     & L_apide,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_idide
     &,L_odide,L_oside,
     & I_eside,L_ivide,R_oride,REAL(R_ulide,4),L_udide,L_afide
     &,L_ipide,L_opide,
     & L_upide,L_eride,L_iride,L_aride)
      !}

      if(L_iride.or.L_eride.or.L_aride.or.L_upide.or.L_opide.or.L_ipide
     &) then      
                  I_aside = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_aside = z'40000000'
      endif


      R_omide=100*R8_elide
C FDB50_vlv.fgi( 419, 117):���� ���������� ������������ ��������,20FDB50AA213
      !{
      Call BOX_HANDLER(deltat,L_adode,
     & R_ubode,REAL(R_edode,4),
     & L_udode,R_odode,
     & REAL(R_afode,4),L_ebode,R_abode,
     & REAL(R_ibode,4),L_efode,I_oxide,L_idode,L_obode,
     & I_uxide)
      !}
C FDB50_vlv.fgi( 462, 303):���������� �����,20FDB50_BOX_UST_GAS
      !{
      Call BOX_HANDLER(deltat,L_ukode,
     & R_okode,REAL(R_alode,4),
     & L_olode,R_ilode,
     & REAL(R_ulode,4),L_akode,R_ufode,
     & REAL(R_ekode,4),L_amode,I_ifode,L_elode,L_ikode,
     & I_ofode)
      !}
C FDB50_vlv.fgi( 431, 303):���������� �����,20FDB50_PECH
      !{
      Call BOX_HANDLER(deltat,L_opode,
     & R_ipode,REAL(R_upode,4),
     & L_irode,R_erode,
     & REAL(R_orode,4),L_umode,R_omode,
     & REAL(R_apode,4),L_urode,I_emode,L_arode,L_epode,
     & I_imode)
      !}
C FDB50_vlv.fgi( 402, 303):���������� �����,20FDB50_PST
      !{
      Call BOX_HANDLER(deltat,L_itode,
     & R_etode,REAL(R_otode,4),
     & L_evode,R_avode,
     & REAL(R_ivode,4),L_osode,R_isode,
     & REAL(R_usode,4),L_ovode,I_asode,L_utode,L_atode,
     & I_esode)
      !}
C FDB50_vlv.fgi( 373, 303):���������� �����,20FDB50_BOX_VH_PST
      !{
      Call BOX_HANDLER(deltat,L_ebude,
     & R_abude,REAL(R_ibude,4),
     & L_adude,R_ubude,
     & REAL(R_edude,4),L_ixode,R_exode,
     & REAL(R_oxode,4),L_idude,I_uvode,L_obude,L_uxode,
     & I_axode)
      !}
C FDB50_vlv.fgi( 462, 335):���������� �����,20FDB50_BOX_VIH_PST
      !{
      Call REG_MAN(deltat,REAL(R_ipude,4),R8_ilude,R_avude
     &,R_etude,
     & L_ixude,R_evude,R_itude,L_oxude,R_omude,R_imude,
     & R_ifude,REAL(R_ufude,4),R_okude,
     & REAL(R_alude,4),R_akude,REAL(R_ikude,4),I_asude,
     & L_elude,L_osude,L_ivude,L_ukude,L_ekude,
     & L_ulude,L_olude,L_atude,L_ofude,L_emude,L_axude,L_apude
     &,
     & L_epude,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_odude
     &,L_udude,L_usude,
     & I_isude,L_ovude,R_urude,REAL(R_amude,4),L_afude,L_efude
     &,L_opude,L_upude,
     & L_arude,L_irude,L_orude,L_erude)
      !}

      if(L_orude.or.L_irude.or.L_erude.or.L_arude.or.L_upude.or.L_opude
     &) then      
                  I_esude = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_esude = z'40000000'
      endif


      R_umude=100*R8_ilude
C FDB50_vlv.fgi( 406, 117):���� ���������� ������������ ��������,20FDB50AA200
      !{
      Call REG_MAN(deltat,REAL(R_olafe,4),R8_ofafe,R_esafe
     &,R_irafe,
     & L_otafe,R_isafe,R_orafe,L_utafe,R_ukafe,R_okafe,
     & R_obafe,REAL(R_adafe,4),R_udafe,
     & REAL(R_efafe,4),R_edafe,REAL(R_odafe,4),I_epafe,
     & L_ifafe,L_upafe,L_osafe,L_afafe,L_idafe,
     & L_akafe,L_ufafe,L_erafe,L_ubafe,L_ikafe,L_etafe,L_elafe
     &,
     & L_ilafe,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_uxude
     &,L_abafe,L_arafe,
     & I_opafe,L_usafe,R_apafe,REAL(R_ekafe,4),L_ebafe,L_ibafe
     &,L_ulafe,L_amafe,
     & L_emafe,L_omafe,L_umafe,L_imafe)
      !}

      if(L_umafe.or.L_omafe.or.L_imafe.or.L_emafe.or.L_amafe.or.L_ulafe
     &) then      
                  I_ipafe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ipafe = z'40000000'
      endif


      R_alafe=100*R8_ofafe
C FDB50_vlv.fgi( 393, 117):���� ���������� ������������ ��������,20FDB50AA204
      !{
      Call REG_MAN(deltat,REAL(R_ufefe,4),R8_ubefe,R_ipefe
     &,R_omefe,
     & L_urefe,R_opefe,R_umefe,L_asefe,R_afefe,R_udefe,
     & R_uvafe,REAL(R_exafe,4),R_abefe,
     & REAL(R_ibefe,4),R_ixafe,REAL(R_uxafe,4),I_ilefe,
     & L_obefe,L_amefe,L_upefe,L_ebefe,L_oxafe,
     & L_edefe,L_adefe,L_imefe,L_axafe,L_odefe,L_irefe,L_ifefe
     &,
     & L_ofefe,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_avafe
     &,L_evafe,L_emefe,
     & I_ulefe,L_arefe,R_elefe,REAL(R_idefe,4),L_ivafe,L_ovafe
     &,L_akefe,L_ekefe,
     & L_ikefe,L_ukefe,L_alefe,L_okefe)
      !}

      if(L_alefe.or.L_ukefe.or.L_okefe.or.L_ikefe.or.L_ekefe.or.L_akefe
     &) then      
                  I_olefe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_olefe = z'40000000'
      endif


      R_efefe=100*R8_ubefe
C FDB50_vlv.fgi( 380, 117):���� ���������� ������������ ��������,20FDB50AA203
      !{
      Call REG_MAN(deltat,REAL(R_adife,4),R8_axefe,R_olife
     &,R_ukife,
     & L_apife,R_ulife,R_alife,L_epife,R_ebife,R_abife,
     & R_atefe,REAL(R_itefe,4),R_evefe,
     & REAL(R_ovefe,4),R_otefe,REAL(R_avefe,4),I_ofife,
     & L_uvefe,L_ekife,L_amife,L_ivefe,L_utefe,
     & L_ixefe,L_exefe,L_okife,L_etefe,L_uxefe,L_omife,L_obife
     &,
     & L_ubife,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_esefe
     &,L_isefe,L_ikife,
     & I_akife,L_emife,R_ifife,REAL(R_oxefe,4),L_osefe,L_usefe
     &,L_edife,L_idife,
     & L_odife,L_afife,L_efife,L_udife)
      !}

      if(L_efife.or.L_afife.or.L_udife.or.L_odife.or.L_idife.or.L_edife
     &) then      
                  I_ufife = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ufife = z'40000000'
      endif


      R_ibife=100*R8_axefe
C FDB50_vlv.fgi( 367, 117):���� ���������� ������������ ��������,20FDB50AA202
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_exife,4),R8_etife,I_odofe
     &,I_efofe,I_idofe,
     & C8_utife,I_afofe,R_ovife,R_ivife,R_erife,
     & REAL(R_orife,4),R_isife,REAL(R_usife,4),
     & R_urife,REAL(R_esife,4),I_adofe,I_ifofe,I_udofe,I_ubofe
     &,L_atife,
     & L_ufofe,L_ukofe,L_osife,L_asife,
     & L_otife,L_itife,L_ekofe,L_irife,L_evife,L_ilofe,L_uvife
     &,
     & L_axife,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_ipife
     &,L_opife,L_akofe,
     & I_ofofe,L_alofe,R_obofe,REAL(R_avife,4),L_elofe,L_upife
     &,L_olofe,L_arife,
     & L_ixife,L_oxife,L_uxife,L_ebofe,L_ibofe,L_abofe)
      !}

      if(L_ibofe.or.L_ebofe.or.L_abofe.or.L_uxife.or.L_oxife.or.L_ixife
     &) then      
                  I_edofe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_edofe = z'40000000'
      endif
C FDB50_vlv.fgi( 406, 143):���� ���������� �������� ��������,20FDB50AA139
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_otofe,4),R8_orofe,I_abufe
     &,I_obufe,I_uxofe,
     & C8_esofe,I_ibufe,R_atofe,R_usofe,R_omofe,
     & REAL(R_apofe,4),R_upofe,REAL(R_erofe,4),
     & R_epofe,REAL(R_opofe,4),I_ixofe,I_ubufe,I_ebufe,I_exofe
     &,L_irofe,
     & L_edufe,L_efufe,L_arofe,L_ipofe,
     & L_asofe,L_urofe,L_odufe,L_umofe,L_osofe,L_ufufe,L_etofe
     &,
     & L_itofe,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_ulofe
     &,L_amofe,L_idufe,
     & I_adufe,L_ifufe,R_axofe,REAL(R_isofe,4),L_ofufe,L_emofe
     &,L_akufe,L_imofe,
     & L_utofe,L_avofe,L_evofe,L_ovofe,L_uvofe,L_ivofe)
      !}

      if(L_uvofe.or.L_ovofe.or.L_ivofe.or.L_evofe.or.L_avofe.or.L_utofe
     &) then      
                  I_oxofe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_oxofe = z'40000000'
      endif
C FDB50_vlv.fgi( 393, 143):���� ���������� �������� ��������,20FDB50AA121
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_asufe,4),R8_apufe,I_ivufe
     &,I_axufe,I_evufe,
     & C8_opufe,I_uvufe,R_irufe,R_erufe,R_alufe,
     & REAL(R_ilufe,4),R_emufe,REAL(R_omufe,4),
     & R_olufe,REAL(R_amufe,4),I_utufe,I_exufe,I_ovufe,I_otufe
     &,L_umufe,
     & L_oxufe,L_obake,L_imufe,L_ulufe,
     & L_ipufe,L_epufe,L_abake,L_elufe,L_arufe,L_edake,L_orufe
     &,
     & L_urufe,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_ekufe
     &,L_ikufe,L_uxufe,
     & I_ixufe,L_ubake,R_itufe,REAL(R_upufe,4),L_adake,L_okufe
     &,L_idake,L_ukufe,
     & L_esufe,L_isufe,L_osufe,L_atufe,L_etufe,L_usufe)
      !}

      if(L_etufe.or.L_atufe.or.L_usufe.or.L_osufe.or.L_isufe.or.L_esufe
     &) then      
                  I_avufe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_avufe = z'40000000'
      endif
C FDB50_vlv.fgi( 380, 143):���� ���������� �������� ��������,20FDB50AA120
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ipake,4),R8_ilake,I_usake
     &,I_itake,I_osake,
     & C8_amake,I_etake,R_umake,R_omake,R_ifake,
     & REAL(R_ufake,4),R_okake,REAL(R_alake,4),
     & R_akake,REAL(R_ikake,4),I_esake,I_otake,I_atake,I_asake
     &,L_elake,
     & L_avake,L_axake,L_ukake,L_ekake,
     & L_ulake,L_olake,L_ivake,L_ofake,L_imake,L_oxake,L_apake
     &,
     & L_epake,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_odake
     &,L_udake,L_evake,
     & I_utake,L_exake,R_urake,REAL(R_emake,4),L_ixake,L_afake
     &,L_uxake,L_efake,
     & L_opake,L_upake,L_arake,L_irake,L_orake,L_erake)
      !}

      if(L_orake.or.L_irake.or.L_erake.or.L_arake.or.L_upake.or.L_opake
     &) then      
                  I_isake = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_isake = z'40000000'
      endif
C FDB50_vlv.fgi( 367, 143):���� ���������� �������� ��������,20FDB50AA119
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ufike,4),R8_ubike,I_emike
     &,I_umike,I_amike,
     & C8_idike,I_omike,R_efike,R_afike,R_uveke,
     & REAL(R_exeke,4),R_abike,REAL(R_ibike,4),
     & R_ixeke,REAL(R_uxeke,4),I_olike,I_apike,I_imike,I_ilike
     &,L_obike,
     & L_ipike,L_irike,L_ebike,L_oxeke,
     & L_edike,L_adike,L_upike,L_axeke,L_udike,L_asike,L_ifike
     &,
     & L_ofike,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_aveke
     &,L_eveke,L_opike,
     & I_epike,L_orike,R_elike,REAL(R_odike,4),L_urike,L_iveke
     &,L_esike,L_oveke,
     & L_akike,L_ekike,L_ikike,L_ukike,L_alike,L_okike)
      !}

      if(L_alike.or.L_ukike.or.L_okike.or.L_ikike.or.L_ekike.or.L_akike
     &) then      
                  I_ulike = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ulike = z'40000000'
      endif
C FDB50_vlv.fgi( 406, 167):���� ���������� �������� ��������,20FDB50AA124
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_edoke,4),R8_exike,I_okoke
     &,I_eloke,I_ikoke,
     & C8_uxike,I_aloke,R_oboke,R_iboke,R_etike,
     & REAL(R_otike,4),R_ivike,REAL(R_uvike,4),
     & R_utike,REAL(R_evike,4),I_akoke,I_iloke,I_ukoke,I_ufoke
     &,L_axike,
     & L_uloke,L_umoke,L_ovike,L_avike,
     & L_oxike,L_ixike,L_emoke,L_itike,L_eboke,L_ipoke,L_uboke
     &,
     & L_adoke,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_isike
     &,L_osike,L_amoke,
     & I_oloke,L_apoke,R_ofoke,REAL(R_aboke,4),L_epoke,L_usike
     &,L_opoke,L_atike,
     & L_idoke,L_odoke,L_udoke,L_efoke,L_ifoke,L_afoke)
      !}

      if(L_ifoke.or.L_efoke.or.L_afoke.or.L_udoke.or.L_odoke.or.L_idoke
     &) then      
                  I_ekoke = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ekoke = z'40000000'
      endif
C FDB50_vlv.fgi( 393, 167):���� ���������� �������� ��������,20FDB50AA123
      !{
      Call BOX_HANDLER(deltat,L_esoke,
     & R_asoke,REAL(R_isoke,4),
     & L_atoke,R_usoke,
     & REAL(R_etoke,4),L_iroke,R_eroke,
     & REAL(R_oroke,4),L_itoke,I_upoke,L_osoke,L_uroke,
     & I_aroke)
      !}
C FDB50_vlv.fgi( 402, 335):���������� �����,20FDB50_USTROISTVO_TRANSP
      !{
      Call BOX_HANDLER(deltat,L_axoke,
     & R_uvoke,REAL(R_exoke,4),
     & L_uxoke,R_oxoke,
     & REAL(R_abuke,4),L_evoke,R_avoke,
     & REAL(R_ivoke,4),L_ebuke,I_otoke,L_ixoke,L_ovoke,
     & I_utoke)
      !}
C FDB50_vlv.fgi( 373, 335):���������� �����,20FDB50_BOX_VOZVR_TRANSP
      !{
      Call BOX_HANDLER(deltat,L_uduke,
     & R_oduke,REAL(R_afuke,4),
     & L_ofuke,R_ifuke,
     & REAL(R_ufuke,4),L_aduke,R_ubuke,
     & REAL(R_eduke,4),L_akuke,I_ibuke,L_efuke,L_iduke,
     & I_obuke)
      !}
C FDB50_vlv.fgi( 431, 335):���������� �����,20FDB50_BOX_DISP
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ubile,4),R8_uvele,I_ekile
     &,I_ukile,I_akile,
     & C8_ixele,I_okile,R_ebile,R_abile,R_usele,
     & REAL(R_etele,4),R_avele,REAL(R_ivele,4),
     & R_itele,REAL(R_utele,4),I_ofile,I_alile,I_ikile,I_ifile
     &,L_ovele,
     & L_ilile,L_imile,L_evele,L_otele,
     & L_exele,L_axele,L_ulile,L_atele,L_uxele,L_apile,L_ibile
     &,
     & L_obile,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_asele
     &,L_esele,L_olile,
     & I_elile,L_omile,R_efile,REAL(R_oxele,4),L_umile,L_isele
     &,L_epile,L_osele,
     & L_adile,L_edile,L_idile,L_udile,L_afile,L_odile)
      !}

      if(L_afile.or.L_udile.or.L_odile.or.L_idile.or.L_edile.or.L_adile
     &) then      
                  I_ufile = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ufile = z'40000000'
      endif
C FDB50_vlv.fgi( 380, 167):���� ���������� �������� ��������,20FDB50AA130
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_exile,4),R8_etile,I_odole
     &,I_efole,I_idole,
     & C8_utile,I_afole,R_ovile,R_ivile,R_erile,
     & REAL(R_orile,4),R_isile,REAL(R_usile,4),
     & R_urile,REAL(R_esile,4),I_adole,I_ifole,I_udole,I_ubole
     &,L_atile,
     & L_ufole,L_ukole,L_osile,L_asile,
     & L_otile,L_itile,L_ekole,L_irile,L_evile,L_ilole,L_uvile
     &,
     & L_axile,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_ipile
     &,L_opile,L_akole,
     & I_ofole,L_alole,R_obole,REAL(R_avile,4),L_elole,L_upile
     &,L_olole,L_arile,
     & L_ixile,L_oxile,L_uxile,L_ebole,L_ibole,L_abole)
      !}

      if(L_ibole.or.L_ebole.or.L_abole.or.L_uxile.or.L_oxile.or.L_ixile
     &) then      
                  I_edole = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_edole = z'40000000'
      endif
C FDB50_vlv.fgi( 367, 167):���� ���������� �������� ��������,20FDB50AA129
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_otole,4),R8_orole,I_abule
     &,I_obule,I_uxole,
     & C8_esole,I_ibule,R_atole,R_usole,R_omole,
     & REAL(R_apole,4),R_upole,REAL(R_erole,4),
     & R_epole,REAL(R_opole,4),I_ixole,I_ubule,I_ebule,I_exole
     &,L_irole,
     & L_edule,L_efule,L_arole,L_ipole,
     & L_asole,L_urole,L_odule,L_umole,L_osole,L_ufule,L_etole
     &,
     & L_itole,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_ulole
     &,L_amole,L_idule,
     & I_adule,L_ifule,R_axole,REAL(R_isole,4),L_ofule,L_emole
     &,L_akule,L_imole,
     & L_utole,L_avole,L_evole,L_ovole,L_uvole,L_ivole)
      !}

      if(L_uvole.or.L_ovole.or.L_ivole.or.L_evole.or.L_avole.or.L_utole
     &) then      
                  I_oxole = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_oxole = z'40000000'
      endif
C FDB50_vlv.fgi( 380, 195):���� ���������� �������� ��������,20FDB50AA126
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_asule,4),R8_apule,I_ivule
     &,I_axule,I_evule,
     & C8_opule,I_uvule,R_irule,R_erule,R_alule,
     & REAL(R_ilule,4),R_emule,REAL(R_omule,4),
     & R_olule,REAL(R_amule,4),I_utule,I_exule,I_ovule,I_otule
     &,L_umule,
     & L_oxule,L_obame,L_imule,L_ulule,
     & L_ipule,L_epule,L_abame,L_elule,L_arule,L_edame,L_orule
     &,
     & L_urule,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_ekule
     &,L_ikule,L_uxule,
     & I_ixule,L_ubame,R_itule,REAL(R_upule,4),L_adame,L_okule
     &,L_idame,L_ukule,
     & L_esule,L_isule,L_osule,L_atule,L_etule,L_usule)
      !}

      if(L_etule.or.L_atule.or.L_usule.or.L_osule.or.L_isule.or.L_esule
     &) then      
                  I_avule = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_avule = z'40000000'
      endif
C FDB50_vlv.fgi( 406, 195):���� ���������� �������� ��������,20FDB50AA128
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ipame,4),R8_ilame,I_usame
     &,I_itame,I_osame,
     & C8_amame,I_etame,R_umame,R_omame,R_ifame,
     & REAL(R_ufame,4),R_okame,REAL(R_alame,4),
     & R_akame,REAL(R_ikame,4),I_esame,I_otame,I_atame,I_asame
     &,L_elame,
     & L_avame,L_axame,L_ukame,L_ekame,
     & L_ulame,L_olame,L_ivame,L_ofame,L_imame,L_oxame,L_apame
     &,
     & L_epame,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_odame
     &,L_udame,L_evame,
     & I_utame,L_exame,R_urame,REAL(R_emame,4),L_ixame,L_afame
     &,L_uxame,L_efame,
     & L_opame,L_upame,L_arame,L_irame,L_orame,L_erame)
      !}

      if(L_orame.or.L_irame.or.L_erame.or.L_arame.or.L_upame.or.L_opame
     &) then      
                  I_isame = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_isame = z'40000000'
      endif
C FDB50_vlv.fgi( 393, 195):���� ���������� �������� ��������,20FDB50AA127
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_uleme,4),R8_ufeme,I_ereme
     &,I_ureme,I_areme,
     & C8_ikeme,I_oreme,R_eleme,R_aleme,R_ubeme,
     & REAL(R_edeme,4),R_afeme,REAL(R_ifeme,4),
     & R_ideme,REAL(R_udeme,4),I_opeme,I_aseme,I_ireme,I_ipeme
     &,L_ofeme,
     & L_iseme,L_iteme,L_efeme,L_odeme,
     & L_ekeme,L_akeme,L_useme,L_ademe,L_ukeme,L_aveme,L_ileme
     &,
     & L_oleme,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_abeme
     &,L_ebeme,L_oseme,
     & I_eseme,L_oteme,R_epeme,REAL(R_okeme,4),L_uteme,L_ibeme
     &,L_eveme,L_obeme,
     & L_ameme,L_ememe,L_imeme,L_umeme,L_apeme,L_omeme)
      !}

      if(L_apeme.or.L_umeme.or.L_omeme.or.L_imeme.or.L_ememe.or.L_ameme
     &) then      
                  I_upeme = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_upeme = z'40000000'
      endif
C FDB50_vlv.fgi( 367, 195):���� ���������� �������� ��������,20FDB50AA125
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ekime,4),R8_edime,I_omime
     &,I_epime,I_imime,
     & C8_udime,I_apime,R_ofime,R_ifime,R_exeme,
     & REAL(R_oxeme,4),R_ibime,REAL(R_ubime,4),
     & R_uxeme,REAL(R_ebime,4),I_amime,I_ipime,I_umime,I_ulime
     &,L_adime,
     & L_upime,L_urime,L_obime,L_abime,
     & L_odime,L_idime,L_erime,L_ixeme,L_efime,L_isime,L_ufime
     &,
     & L_akime,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_iveme
     &,L_oveme,L_arime,
     & I_opime,L_asime,R_olime,REAL(R_afime,4),L_esime,L_uveme
     &,L_osime,L_axeme,
     & L_ikime,L_okime,L_ukime,L_elime,L_ilime,L_alime)
      !}

      if(L_ilime.or.L_elime.or.L_alime.or.L_ukime.or.L_okime.or.L_ikime
     &) then      
                  I_emime = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_emime = z'40000000'
      endif
C FDB50_vlv.fgi( 380, 219):���� ���������� �������� ��������,20FDB50AA210
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_odome,4),R8_oxime,I_alome
     &,I_olome,I_ukome,
     & C8_ebome,I_ilome,R_adome,R_ubome,R_otime,
     & REAL(R_avime,4),R_uvime,REAL(R_exime,4),
     & R_evime,REAL(R_ovime,4),I_ikome,I_ulome,I_elome,I_ekome
     &,L_ixime,
     & L_emome,L_epome,L_axime,L_ivime,
     & L_abome,L_uxime,L_omome,L_utime,L_obome,L_upome,L_edome
     &,
     & L_idome,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_usime
     &,L_atime,L_imome,
     & I_amome,L_ipome,R_akome,REAL(R_ibome,4),L_opome,L_etime
     &,L_arome,L_itime,
     & L_udome,L_afome,L_efome,L_ofome,L_ufome,L_ifome)
      !}

      if(L_ufome.or.L_ofome.or.L_ifome.or.L_efome.or.L_afome.or.L_udome
     &) then      
                  I_okome = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_okome = z'40000000'
      endif
C FDB50_vlv.fgi( 406, 219):���� ���������� �������� ��������,20FDB50AA212
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_abume,4),R8_avome,I_ifume
     &,I_akume,I_efume,
     & C8_ovome,I_ufume,R_ixome,R_exome,R_asome,
     & REAL(R_isome,4),R_etome,REAL(R_otome,4),
     & R_osome,REAL(R_atome,4),I_udume,I_ekume,I_ofume,I_odume
     &,L_utome,
     & L_okume,L_olume,L_itome,L_usome,
     & L_ivome,L_evome,L_alume,L_esome,L_axome,L_emume,L_oxome
     &,
     & L_uxome,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_erome
     &,L_irome,L_ukume,
     & I_ikume,L_ulume,R_idume,REAL(R_uvome,4),L_amume,L_orome
     &,L_imume,L_urome,
     & L_ebume,L_ibume,L_obume,L_adume,L_edume,L_ubume)
      !}

      if(L_edume.or.L_adume.or.L_ubume.or.L_obume.or.L_ibume.or.L_ebume
     &) then      
                  I_afume = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_afume = z'40000000'
      endif
C FDB50_vlv.fgi( 393, 219):���� ���������� �������� ��������,20FDB50AA211
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ivume,4),R8_isume,I_ubape
     &,I_idape,I_obape,
     & C8_atume,I_edape,R_utume,R_otume,R_ipume,
     & REAL(R_upume,4),R_orume,REAL(R_asume,4),
     & R_arume,REAL(R_irume,4),I_ebape,I_odape,I_adape,I_abape
     &,L_esume,
     & L_afape,L_akape,L_urume,L_erume,
     & L_usume,L_osume,L_ifape,L_opume,L_itume,L_okape,L_avume
     &,
     & L_evume,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_omume
     &,L_umume,L_efape,
     & I_udape,L_ekape,R_uxume,REAL(R_etume,4),L_ikape,L_apume
     &,L_ukape,L_epume,
     & L_ovume,L_uvume,L_axume,L_ixume,L_oxume,L_exume)
      !}

      if(L_oxume.or.L_ixume.or.L_exume.or.L_axume.or.L_uvume.or.L_ovume
     &) then      
                  I_ibape = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ibape = z'40000000'
      endif
C FDB50_vlv.fgi( 367, 219):���� ���������� �������� ��������,20FDB50AA209
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_usape,4),R8_upape,I_exape
     &,I_uxape,I_axape,
     & C8_irape,I_oxape,R_esape,R_asape,R_ulape,
     & REAL(R_emape,4),R_apape,REAL(R_ipape,4),
     & R_imape,REAL(R_umape,4),I_ovape,I_abepe,I_ixape,I_ivape
     &,L_opape,
     & L_ibepe,L_idepe,L_epape,L_omape,
     & L_erape,L_arape,L_ubepe,L_amape,L_urape,L_afepe,L_isape
     &,
     & L_osape,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_alape
     &,L_elape,L_obepe,
     & I_ebepe,L_odepe,R_evape,REAL(R_orape,4),L_udepe,L_ilape
     &,L_efepe,L_olape,
     & L_atape,L_etape,L_itape,L_utape,L_avape,L_otape)
      !}

      if(L_avape.or.L_utape.or.L_otape.or.L_itape.or.L_etape.or.L_atape
     &) then      
                  I_uvape = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_uvape = z'40000000'
      endif
C FDB50_vlv.fgi( 419, 243):���� ���������� �������� ��������,20FDB50AA131
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_erepe,4),R8_emepe,I_otepe
     &,I_evepe,I_itepe,
     & C8_umepe,I_avepe,R_opepe,R_ipepe,R_ekepe,
     & REAL(R_okepe,4),R_ilepe,REAL(R_ulepe,4),
     & R_ukepe,REAL(R_elepe,4),I_atepe,I_ivepe,I_utepe,I_usepe
     &,L_amepe,
     & L_uvepe,L_uxepe,L_olepe,L_alepe,
     & L_omepe,L_imepe,L_exepe,L_ikepe,L_epepe,L_ibipe,L_upepe
     &,
     & L_arepe,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_ifepe
     &,L_ofepe,L_axepe,
     & I_ovepe,L_abipe,R_osepe,REAL(R_apepe,4),L_ebipe,L_ufepe
     &,L_obipe,L_akepe,
     & L_irepe,L_orepe,L_urepe,L_esepe,L_isepe,L_asepe)
      !}

      if(L_isepe.or.L_esepe.or.L_asepe.or.L_urepe.or.L_orepe.or.L_irepe
     &) then      
                  I_etepe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_etepe = z'40000000'
      endif
C FDB50_vlv.fgi( 380, 243):���� ���������� �������� ��������,20FDB50AA206
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_omipe,4),R8_okipe,I_asipe
     &,I_osipe,I_uripe,
     & C8_elipe,I_isipe,R_amipe,R_ulipe,R_odipe,
     & REAL(R_afipe,4),R_ufipe,REAL(R_ekipe,4),
     & R_efipe,REAL(R_ofipe,4),I_iripe,I_usipe,I_esipe,I_eripe
     &,L_ikipe,
     & L_etipe,L_evipe,L_akipe,L_ifipe,
     & L_alipe,L_ukipe,L_otipe,L_udipe,L_olipe,L_uvipe,L_emipe
     &,
     & L_imipe,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_ubipe
     &,L_adipe,L_itipe,
     & I_atipe,L_ivipe,R_aripe,REAL(R_ilipe,4),L_ovipe,L_edipe
     &,L_axipe,L_idipe,
     & L_umipe,L_apipe,L_epipe,L_opipe,L_upipe,L_ipipe)
      !}

      if(L_upipe.or.L_opipe.or.L_ipipe.or.L_epipe.or.L_apipe.or.L_umipe
     &) then      
                  I_oripe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_oripe = z'40000000'
      endif
C FDB50_vlv.fgi( 406, 243):���� ���������� �������� ��������,20FDB50AA208
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_alope,4),R8_afope,I_ipope
     &,I_arope,I_epope,
     & C8_ofope,I_upope,R_ikope,R_ekope,R_abope,
     & REAL(R_ibope,4),R_edope,REAL(R_odope,4),
     & R_obope,REAL(R_adope,4),I_umope,I_erope,I_opope,I_omope
     &,L_udope,
     & L_orope,L_osope,L_idope,L_ubope,
     & L_ifope,L_efope,L_asope,L_ebope,L_akope,L_etope,L_okope
     &,
     & L_ukope,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_exipe
     &,L_ixipe,L_urope,
     & I_irope,L_usope,R_imope,REAL(R_ufope,4),L_atope,L_oxipe
     &,L_itope,L_uxipe,
     & L_elope,L_ilope,L_olope,L_amope,L_emope,L_ulope)
      !}

      if(L_emope.or.L_amope.or.L_ulope.or.L_olope.or.L_ilope.or.L_elope
     &) then      
                  I_apope = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_apope = z'40000000'
      endif
C FDB50_vlv.fgi( 393, 243):���� ���������� �������� ��������,20FDB50AA207
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ifupe,4),R8_ibupe,I_ulupe
     &,I_imupe,I_olupe,
     & C8_adupe,I_emupe,R_udupe,R_odupe,R_ivope,
     & REAL(R_uvope,4),R_oxope,REAL(R_abupe,4),
     & R_axope,REAL(R_ixope,4),I_elupe,I_omupe,I_amupe,I_alupe
     &,L_ebupe,
     & L_apupe,L_arupe,L_uxope,L_exope,
     & L_ubupe,L_obupe,L_ipupe,L_ovope,L_idupe,L_orupe,L_afupe
     &,
     & L_efupe,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_otope
     &,L_utope,L_epupe,
     & I_umupe,L_erupe,R_ukupe,REAL(R_edupe,4),L_irupe,L_avope
     &,L_urupe,L_evope,
     & L_ofupe,L_ufupe,L_akupe,L_ikupe,L_okupe,L_ekupe)
      !}

      if(L_okupe.or.L_ikupe.or.L_ekupe.or.L_akupe.or.L_ufupe.or.L_ofupe
     &) then      
                  I_ilupe = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ilupe = z'40000000'
      endif
C FDB50_vlv.fgi( 367, 243):���� ���������� �������� ��������,20FDB50AA205
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ubare,4),R8_uvupe,I_ekare
     &,I_ukare,I_akare,
     & C8_ixupe,I_okare,R_ebare,R_abare,R_usupe,
     & REAL(R_etupe,4),R_avupe,REAL(R_ivupe,4),
     & R_itupe,REAL(R_utupe,4),I_ofare,I_alare,I_ikare,I_ifare
     &,L_ovupe,
     & L_ilare,L_imare,L_evupe,L_otupe,
     & L_exupe,L_axupe,L_ulare,L_atupe,L_uxupe,L_apare,L_ibare
     &,
     & L_obare,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_asupe
     &,L_esupe,L_olare,
     & I_elare,L_omare,R_efare,REAL(R_oxupe,4),L_umare,L_isupe
     &,L_epare,L_osupe,
     & L_adare,L_edare,L_idare,L_udare,L_afare,L_odare)
      !}

      if(L_afare.or.L_udare.or.L_odare.or.L_idare.or.L_edare.or.L_adare
     &) then      
                  I_ufare = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_ufare = z'40000000'
      endif
C FDB50_vlv.fgi( 380, 268):���� ���������� �������� ��������,20FDB50AA137
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_exare,4),R8_etare,I_odere
     &,I_efere,I_idere,
     & C8_utare,I_afere,R_ovare,R_ivare,R_erare,
     & REAL(R_orare,4),R_isare,REAL(R_usare,4),
     & R_urare,REAL(R_esare,4),I_adere,I_ifere,I_udere,I_ubere
     &,L_atare,
     & L_ufere,L_ukere,L_osare,L_asare,
     & L_otare,L_itare,L_ekere,L_irare,L_evare,L_ilere,L_uvare
     &,
     & L_axare,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_ipare
     &,L_opare,L_akere,
     & I_ofere,L_alere,R_obere,REAL(R_avare,4),L_elere,L_upare
     &,L_olere,L_arare,
     & L_ixare,L_oxare,L_uxare,L_ebere,L_ibere,L_abere)
      !}

      if(L_ibere.or.L_ebere.or.L_abere.or.L_uxare.or.L_oxare.or.L_ixare
     &) then      
                  I_edere = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_edere = z'40000000'
      endif
C FDB50_vlv.fgi( 406, 268):���� ���������� �������� ��������,20FDB50AA112
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_asire,4),R8_umire,I_ivire
     &,I_axire,I_evire,
     & C8_ipire,I_uvire,R_irire,R_erire,R_ukire,
     & REAL(R_elire,4),R_amire,REAL(R_imire,4),
     & R_ilire,REAL(R_ulire,4),I_utire,I_exire,I_ovire,I_otire
     &,L_omire,
     & L_oxire,L_obore,L_emire,L_olire,
     & L_epire,L_apire,L_abore,L_alire,L_upire,L_edore,L_orire
     &,
     & L_urire,REAL(R8_okire,8),REAL(1.0,4),R8_arire,L_ufire
     &,L_akire,L_uxire,
     & I_ixire,L_ubore,R_itire,REAL(R_opire,4),L_adore,L_ekire
     &,L_idore,L_ikire,
     & L_esire,L_isire,L_osire,L_atire,L_etire,L_usire)
      !}

      if(L_etire.or.L_atire.or.L_usire.or.L_osire.or.L_isire.or.L_esire
     &) then      
                  I_avire = ior(z'000000FF',z'04000000') 
     &                           
                else
                   I_avire = z'40000000'
      endif
C FDB50_vlv.fgi( 367, 268):���� ���������� �������� ��������,20FDB50AA136
      I_(1) = z'0100001C'
C FDB50_logic.fgi( 420, 376):��������� ������������� IN (�������)
      I_(2) = z'01000023'
C FDB50_logic.fgi( 420, 378):��������� ������������� IN (�������)
      L_(58)=I_etore.eq.I0_udore
C FDB50_logic.fgi( 418, 370):���������� �������������
      if(L_(58)) then
         I_odore=I_(1)
      else
         I_odore=I_(2)
      endif
C FDB50_logic.fgi( 427, 376):���� RE IN LO CH7
      I_(3) = z'0100001C'
C FDB50_logic.fgi( 420, 362):��������� ������������� IN (�������)
      I_(4) = z'01000023'
C FDB50_logic.fgi( 420, 364):��������� ������������� IN (�������)
      L_(59)=I_etore.eq.I0_efore
C FDB50_logic.fgi( 418, 356):���������� �������������
      if(L_(59)) then
         I_afore=I_(3)
      else
         I_afore=I_(4)
      endif
C FDB50_logic.fgi( 427, 362):���� RE IN LO CH7
      I_(5) = z'0100001C'
C FDB50_logic.fgi( 420, 348):��������� ������������� IN (�������)
      I_(6) = z'01000023'
C FDB50_logic.fgi( 420, 350):��������� ������������� IN (�������)
      L_(60)=I_etore.eq.I0_ofore
C FDB50_logic.fgi( 418, 342):���������� �������������
      if(L_(60)) then
         I_ifore=I_(5)
      else
         I_ifore=I_(6)
      endif
C FDB50_logic.fgi( 427, 348):���� RE IN LO CH7
      I_(7) = z'0100001C'
C FDB50_logic.fgi( 420, 334):��������� ������������� IN (�������)
      I_(8) = z'01000023'
C FDB50_logic.fgi( 420, 336):��������� ������������� IN (�������)
      L_(61)=I_etore.eq.I0_akore
C FDB50_logic.fgi( 418, 328):���������� �������������
      if(L_(61)) then
         I_ufore=I_(7)
      else
         I_ufore=I_(8)
      endif
C FDB50_logic.fgi( 427, 334):���� RE IN LO CH7
      I_(9) = z'0100001C'
C FDB50_logic.fgi( 420, 320):��������� ������������� IN (�������)
      I_(10) = z'01000023'
C FDB50_logic.fgi( 420, 322):��������� ������������� IN (�������)
      L_(62)=I_etore.eq.I0_ikore
C FDB50_logic.fgi( 418, 314):���������� �������������
      if(L_(62)) then
         I_ekore=I_(9)
      else
         I_ekore=I_(10)
      endif
C FDB50_logic.fgi( 427, 320):���� RE IN LO CH7
      I_(11) = z'0100001C'
C FDB50_logic.fgi( 420, 305):��������� ������������� IN (�������)
      I_(12) = z'01000023'
C FDB50_logic.fgi( 420, 307):��������� ������������� IN (�������)
      L_(63)=I_etore.eq.I0_ukore
C FDB50_logic.fgi( 418, 299):���������� �������������
      if(L_(63)) then
         I_okore=I_(11)
      else
         I_okore=I_(12)
      endif
C FDB50_logic.fgi( 427, 305):���� RE IN LO CH7
      I_(13) = z'0100001C'
C FDB50_logic.fgi( 361, 305):��������� ������������� IN (�������)
      I_(14) = z'01000023'
C FDB50_logic.fgi( 361, 307):��������� ������������� IN (�������)
      L_(64)=I_etore.eq.I0_elore
C FDB50_logic.fgi( 359, 299):���������� �������������
      if(L_(64)) then
         I_alore=I_(13)
      else
         I_alore=I_(14)
      endif
C FDB50_logic.fgi( 368, 305):���� RE IN LO CH7
      I_(15) = z'0100001C'
C FDB50_logic.fgi( 361, 318):��������� ������������� IN (�������)
      I_(16) = z'01000023'
C FDB50_logic.fgi( 361, 320):��������� ������������� IN (�������)
      L_(65)=I_etore.eq.I0_olore
C FDB50_logic.fgi( 359, 312):���������� �������������
      if(L_(65)) then
         I_ilore=I_(15)
      else
         I_ilore=I_(16)
      endif
C FDB50_logic.fgi( 368, 318):���� RE IN LO CH7
      I_(17) = z'0100001C'
C FDB50_logic.fgi( 361, 331):��������� ������������� IN (�������)
      I_(18) = z'01000023'
C FDB50_logic.fgi( 361, 333):��������� ������������� IN (�������)
      L_(66)=I_etore.eq.I0_amore
C FDB50_logic.fgi( 359, 325):���������� �������������
      if(L_(66)) then
         I_ulore=I_(17)
      else
         I_ulore=I_(18)
      endif
C FDB50_logic.fgi( 368, 331):���� RE IN LO CH7
      I_(19) = z'0100001C'
C FDB50_logic.fgi( 361, 344):��������� ������������� IN (�������)
      I_(20) = z'01000023'
C FDB50_logic.fgi( 361, 346):��������� ������������� IN (�������)
      L_(67)=I_etore.eq.I0_imore
C FDB50_logic.fgi( 359, 338):���������� �������������
      if(L_(67)) then
         I_emore=I_(19)
      else
         I_emore=I_(20)
      endif
C FDB50_logic.fgi( 368, 344):���� RE IN LO CH7
      I_(21) = z'0100001C'
C FDB50_logic.fgi( 361, 357):��������� ������������� IN (�������)
      I_(22) = z'01000023'
C FDB50_logic.fgi( 361, 359):��������� ������������� IN (�������)
      L_(68)=I_etore.eq.I0_umore
C FDB50_logic.fgi( 359, 351):���������� �������������
      if(L_(68)) then
         I_omore=I_(21)
      else
         I_omore=I_(22)
      endif
C FDB50_logic.fgi( 368, 357):���� RE IN LO CH7
      I_(23) = z'0100001C'
C FDB50_logic.fgi( 361, 370):��������� ������������� IN (�������)
      I_(24) = z'01000023'
C FDB50_logic.fgi( 361, 372):��������� ������������� IN (�������)
      L_(69)=I_etore.eq.I0_epore
C FDB50_logic.fgi( 359, 364):���������� �������������
      if(L_(69)) then
         I_apore=I_(23)
      else
         I_apore=I_(24)
      endif
C FDB50_logic.fgi( 368, 370):���� RE IN LO CH7
      I_(25) = z'0100001C'
C FDB50_logic.fgi( 361, 383):��������� ������������� IN (�������)
      I_(26) = z'01000023'
C FDB50_logic.fgi( 361, 385):��������� ������������� IN (�������)
      L_(70)=I_etore.eq.I0_opore
C FDB50_logic.fgi( 359, 377):���������� �������������
      if(L_(70)) then
         I_ipore=I_(25)
      else
         I_ipore=I_(26)
      endif
C FDB50_logic.fgi( 368, 383):���� RE IN LO CH7
      I_(27) = z'0100001C'
C FDB50_logic.fgi( 361, 396):��������� ������������� IN (�������)
      I_(28) = z'01000023'
C FDB50_logic.fgi( 361, 398):��������� ������������� IN (�������)
      L_(71)=I_etore.eq.I0_arore
C FDB50_logic.fgi( 359, 390):���������� �������������
      if(L_(71)) then
         I_upore=I_(27)
      else
         I_upore=I_(28)
      endif
C FDB50_logic.fgi( 368, 396):���� RE IN LO CH7
      I_(29) = z'0100001C'
C FDB50_logic.fgi( 361, 409):��������� ������������� IN (�������)
      I_(30) = z'01000023'
C FDB50_logic.fgi( 361, 411):��������� ������������� IN (�������)
      L_(72)=I_etore.eq.I0_irore
C FDB50_logic.fgi( 359, 403):���������� �������������
      if(L_(72)) then
         I_erore=I_(29)
      else
         I_erore=I_(30)
      endif
C FDB50_logic.fgi( 368, 409):���� RE IN LO CH7
      I_(31) = z'0100001C'
C FDB50_logic.fgi( 361, 422):��������� ������������� IN (�������)
      I_(32) = z'01000023'
C FDB50_logic.fgi( 361, 424):��������� ������������� IN (�������)
      L_(73)=I_etore.eq.I0_urore
C FDB50_logic.fgi( 359, 416):���������� �������������
      if(L_(73)) then
         I_orore=I_(31)
      else
         I_orore=I_(32)
      endif
C FDB50_logic.fgi( 368, 422):���� RE IN LO CH7
      I_(33) = z'0100001C'
C FDB50_logic.fgi( 361, 435):��������� ������������� IN (�������)
      I_(34) = z'01000023'
C FDB50_logic.fgi( 361, 437):��������� ������������� IN (�������)
      L_(74)=I_etore.eq.I0_esore
C FDB50_logic.fgi( 359, 429):���������� �������������
      if(L_(74)) then
         I_asore=I_(33)
      else
         I_asore=I_(34)
      endif
C FDB50_logic.fgi( 368, 435):���� RE IN LO CH7
      I_(35) = z'0100001C'
C FDB50_logic.fgi( 361, 448):��������� ������������� IN (�������)
      I_(36) = z'01000023'
C FDB50_logic.fgi( 361, 450):��������� ������������� IN (�������)
      L_(75)=I_etore.eq.I0_osore
C FDB50_logic.fgi( 359, 442):���������� �������������
      if(L_(75)) then
         I_isore=I_(35)
      else
         I_isore=I_(36)
      endif
C FDB50_logic.fgi( 368, 448):���� RE IN LO CH7
      I_(37) = z'0100001C'
C FDB50_logic.fgi( 361, 461):��������� ������������� IN (�������)
      I_(38) = z'01000023'
C FDB50_logic.fgi( 361, 463):��������� ������������� IN (�������)
      L_(76)=I_etore.eq.I0_atore
C FDB50_logic.fgi( 359, 455):���������� �������������
      if(L_(76)) then
         I_usore=I_(37)
      else
         I_usore=I_(38)
      endif
C FDB50_logic.fgi( 368, 461):���� RE IN LO CH7
      I_(39) = z'01000003'
C FDB50_logic.fgi(  58, 193):��������� ������������� IN (�������)
      I_(40) = z'01000009'
C FDB50_logic.fgi(  58, 191):��������� ������������� IN (�������)
      C30_otore = '������������������'
C FDB50_logic.fgi(  88, 252):��������� ���������� CH20 (CH30) (�������)
      L_(77)=.true.
C FDB50_logic.fgi(  37, 207):��������� ���������� (�������)
      L0_evore=R0_ovore.ne.R0_ivore
      R0_ivore=R0_ovore
C FDB50_logic.fgi(  24, 200):���������� ������������� ������
      if(L0_evore) then
         L_(84)=L_(77)
      else
         L_(84)=.false.
      endif
C FDB50_logic.fgi(  41, 206):���� � ������������� �������
      L_(4)=L_(84)
C FDB50_logic.fgi(  41, 206):������-�������: ���������� ��� �������������� ������
      I_(41) = z'01000003'
C FDB50_logic.fgi(  94, 182):��������� ������������� IN (�������)
      I_(42) = z'01000009'
C FDB50_logic.fgi(  94, 180):��������� ������������� IN (�������)
      I_(43) = z'01000003'
C FDB50_logic.fgi(  54, 182):��������� ������������� IN (�������)
      I_(44) = z'01000009'
C FDB50_logic.fgi(  54, 180):��������� ������������� IN (�������)
      L0_ikure=R0_uxore.ne.R0_oxore
      R0_oxore=R0_uxore
C FDB50_logic.fgi(  24, 215):���������� ������������� ������
      L0_okure=R0_ebure.ne.R0_abure
      R0_abure=R0_ebure
C FDB50_logic.fgi(  24, 232):���������� ������������� ������
      L0_ukure=R0_obure.ne.R0_ibure
      R0_ibure=R0_obure
C FDB50_logic.fgi(  24, 250):���������� ������������� ������
      C30_adure = '������������'
C FDB50_logic.fgi(  78, 249):��������� ���������� CH20 (CH30) (�������)
      C30_edure = '������'
C FDB50_logic.fgi(  72, 244):��������� ���������� CH20 (CH30) (�������)
      C30_udure = '��������������'
C FDB50_logic.fgi(  57, 245):��������� ���������� CH20 (CH30) (�������)
      C30_afure = '�� ������'
C FDB50_logic.fgi(  57, 247):��������� ���������� CH20 (CH30) (�������)
      I_(45) = z'01000003'
C FDB50_logic.fgi(  96, 193):��������� ������������� IN (�������)
      I_(46) = z'01000009'
C FDB50_logic.fgi(  96, 191):��������� ������������� IN (�������)
      L_(90)=.true.
C FDB50_logic.fgi(  37, 222):��������� ���������� (�������)
      if(L0_ikure) then
         L_(87)=L_(90)
      else
         L_(87)=.false.
      endif
C FDB50_logic.fgi(  41, 221):���� � ������������� �������
      L_(3)=L_(87)
C FDB50_logic.fgi(  41, 221):������-�������: ���������� ��� �������������� ������
      L_(91)=.true.
C FDB50_logic.fgi(  37, 237):��������� ���������� (�������)
      if(L0_okure) then
         L_(88)=L_(91)
      else
         L_(88)=.false.
      endif
C FDB50_logic.fgi(  41, 236):���� � ������������� �������
      L_(2)=L_(88)
C FDB50_logic.fgi(  41, 236):������-�������: ���������� ��� �������������� ������
      L_(85) = L_(2).OR.L_(3).OR.L_(4)
C FDB50_logic.fgi(  51, 253):���
      L_(92)=.true.
C FDB50_logic.fgi(  37, 257):��������� ���������� (�������)
      if(L0_ukure) then
         L_(89)=L_(92)
      else
         L_(89)=.false.
      endif
C FDB50_logic.fgi(  41, 256):���� � ������������� �������
      L_(1)=L_(89)
C FDB50_logic.fgi(  41, 256):������-�������: ���������� ��� �������������� ������
      L_(80) = L_(1).OR.L_(2).OR.L_(4)
C FDB50_logic.fgi(  51, 218):���
      L_ufure=(L_(3).or.L_ufure).and..not.(L_(80))
      L_(81)=.not.L_ufure
C FDB50_logic.fgi(  59, 220):RS �������
      L_exore=L_ufure
C FDB50_logic.fgi(  75, 222):������,FDB5_tech_mode
      if(L_exore) then
         I_uvore=I_(42)
      else
         I_uvore=I_(41)
      endif
C FDB50_logic.fgi(  97, 180):���� RE IN LO CH7
      L_(78) = L_(1).OR.L_(3).OR.L_(2)
C FDB50_logic.fgi(  51, 203):���
      L_avore=(L_(4).or.L_avore).and..not.(L_(78))
      L_(79)=.not.L_avore
C FDB50_logic.fgi(  59, 205):RS �������
      L_utore=L_avore
C FDB50_logic.fgi(  75, 207):������,FDB5_avt_mode
      if(L_utore) then
         I_itore=I_(40)
      else
         I_itore=I_(39)
      endif
C FDB50_logic.fgi(  61, 191):���� RE IN LO CH7
      L_(82) = L_(1).OR.L_(3).OR.L_(4)
C FDB50_logic.fgi(  51, 233):���
      L_akure=(L_(2).or.L_akure).and..not.(L_(82))
      L_(83)=.not.L_akure
C FDB50_logic.fgi(  59, 235):RS �������
      L_efure=L_akure
C FDB50_logic.fgi(  75, 237):������,FDB5_ruch_mode
      if(L_efure) then
         I_ofure=I_(46)
      else
         I_ofure=I_(45)
      endif
C FDB50_logic.fgi(  99, 191):���� RE IN LO CH7
      L_ekure=(L_(1).or.L_ekure).and..not.(L_(85))
      L_(86)=.not.L_ekure
C FDB50_logic.fgi(  59, 255):RS �������
      L_ixore=L_ekure
C FDB50_logic.fgi(  77, 257):������,FDB5_automatic_mode
      if(L_ixore) then
         I_axore=I_(44)
      else
         I_axore=I_(43)
      endif
C FDB50_logic.fgi(  57, 180):���� RE IN LO CH7
      if(L_ekure) then
         C30_odure=C30_udure
      else
         C30_odure=C30_afure
      endif
C FDB50_logic.fgi(  61, 245):���� RE IN LO CH20
      if(L_akure) then
         C30_idure=C30_edure
      else
         C30_idure=C30_odure
      endif
C FDB50_logic.fgi(  76, 244):���� RE IN LO CH20
      if(L_ufure) then
         C30_ubure=C30_adure
      else
         C30_ubure=C30_idure
      endif
C FDB50_logic.fgi(  85, 243):���� RE IN LO CH20
      if(L_avore) then
         C30_ifure=C30_otore
      else
         C30_ifure=C30_ubure
      endif
C FDB50_logic.fgi(  95, 242):���� RE IN LO CH20
      L_(100) = L_ature.AND.L0_oture.AND.(.NOT.L0_usure)
C FDB50_logic.fgi( 130, 330):�
      L_(99) = (.NOT.L_(100)).AND.L_uxure.AND.L0_oture
C FDB50_logic.fgi( 140, 309):�
      L_isure = L_(100).AND.(.NOT.L_(99))
C FDB50_logic.fgi( 148, 329):�
      L_(96) = L0_oture.AND.L_isure
C FDB50_logic.fgi( 214, 311):�
      if(.not.L_(96)) then
         R0_imure=0.0
      elseif(.not.L0_omure) then
         R0_imure=R0_emure
      else
         R0_imure=max(R_(345)-deltat,0.0)
      endif
      L_ipure=L_(96).and.R0_imure.le.0.0
      L0_omure=L_(96)
C FDB50_logic.fgi( 229, 311):�������� ��������� ������,wait003
      L_(102) = (.NOT.L0_iture).AND.L_eture.AND.L0_uture
C FDB50_logic.fgi( 129, 376):�
      L_(101) = (.NOT.L_(102)).AND.L_uxure.AND.L0_uture
C FDB50_logic.fgi( 137, 347):�
      L0_urure = L_(101).OR.L_(99)
C FDB50_logic.fgi( 182, 326):���
      L_osure = L_(102).AND.(.NOT.L_(101))
C FDB50_logic.fgi( 148, 370):�
      L_(97) = L0_uture.AND.L_osure
C FDB50_logic.fgi( 216, 377):�
      if(.not.L_(97)) then
         R0_apure=0.0
      elseif(.not.L0_epure) then
         R0_apure=R0_umure
      else
         R0_apure=max(R_(346)-deltat,0.0)
      endif
      L_opure=L_(97).and.R0_apure.le.0.0
      L0_epure=L_(97)
C FDB50_logic.fgi( 230, 377):�������� ��������� ������,wait001
      L_(98) = L_osure.AND.L_isure
C FDB50_logic.fgi( 152, 342):�
      L0_avure = L0_ivure.AND.L_evure
C FDB50_logic.fgi(  99, 350):�
      L0_asure = (.NOT.L_(98)).AND.L_uxure.AND.L0_avure
C FDB50_logic.fgi( 172, 340):�
      L0_esure = L_(98).AND.(.NOT.L0_asure)
C FDB50_logic.fgi( 186, 346):�
      L_exure = (.NOT.L0_oxure).AND.L_ixure.AND.L_uxure
C FDB50_logic.fgi(  64, 359):�
      L0_axure = (.NOT.L_exure).AND.L_uxure
C FDB50_logic.fgi(  79, 355):�
      L_(104) = L_exure.AND.(.NOT.L0_axure)
C FDB50_logic.fgi(  86, 371):�
      if(.not.L_(104)) then
         R0_ulure=0.0
      elseif(.not.L0_amure) then
         R0_ulure=R0_olure
      else
         R0_ulure=max(R_(344)-deltat,0.0)
      endif
      L_uvure=L_(104).and.R0_ulure.le.0.0
      L0_amure=L_(104)
C FDB50_logic.fgi( 197, 395):�������� ��������� ������,wait02
      L_ovure=L_uvure
C FDB50_logic.fgi( 266, 395):������,ready_to_import01
      L_(103) = (.NOT.L0_avure).AND.L_uxure.AND.L_ovure
C FDB50_logic.fgi( 113, 341):�
      if(.not.L0_erure) then
         R0_elure=0.0
      elseif(.not.L0_ilure) then
         R0_elure=R0_alure
      else
         R0_elure=max(R_(343)-deltat,0.0)
      endif
      L_(93)=L0_erure.and.R0_elure.le.0.0
      L0_ilure=L0_erure
C FDB50_logic.fgi( 192, 293):�������� ��������� ������,wait005
C label 1862  try1862=try1862-1
      L_(94) = L0_ivure.OR.L_(93)
C FDB50_logic.fgi( 174, 301):���
      L0_orure=(L_(103).or.L0_orure).and..not.(L_(94))
      L_(95)=.not.L0_orure
C FDB50_logic.fgi( 183, 303):RS �������
      if(.not.L0_orure) then
         R0_arure=0.0
      elseif(.not.L0_irure) then
         R0_arure=R0_upure
      else
         R0_arure=max(R_(347)-deltat,0.0)
      endif
      L0_erure=L0_orure.and.R0_arure.le.0.0
      L0_irure=L0_orure
C FDB50_logic.fgi( 196, 305):�������� ��������� ������,wait01
C sav1=L_(93)
      if(.not.L0_erure) then
         R0_elure=0.0
      elseif(.not.L0_ilure) then
         R0_elure=R0_alure
      else
         R0_elure=max(R_(343)-deltat,0.0)
      endif
      L_(93)=L0_erure.and.R0_elure.le.0.0
      L0_ilure=L0_erure
C FDB50_logic.fgi( 192, 293):recalc:�������� ��������� ������,wait005
C if(sav1.ne.L_(93) .and. try1862.gt.0) goto 1862
      Call PUMP2_HANDLER(deltat,I_ofus,I_ifus,R_abus,
     & REAL(R_ibus,4),R_ixos,REAL(R_uxos,4),L_amus,
     & L_elus,L_upus,L_arus,L_udus,L_ikus,L_ukus,L_okus,L_alus
     &,L_ulus,
     & L_exos,L_ebus,L_axos,L_oxos,L_epus,
     & L_ipus,L_uvos,L_ovos,L_afus,I_ufus,R_imus,R_omus,L_orus
     &,
     & L_umit,L_opus,REAL(R8_okire,8),L_olus,REAL(R8_ilus
     &,8),R_erus,
     & REAL(R_akus,4),R_ekus,REAL(R8_odus,8),R_idus,R8_arire
     &,R_irus,R8_ilus,
     & REAL(R_obus,4),REAL(R_ubus,4))
C FDB50_vlv.fgi( 420,  90):���������� ���������� ������� 2,20FDB50AN002
C label 1872  try1872=try1872-1
C sav1=R_irus
C sav2=R_erus
C sav3=L_amus
C sav4=L_ulus
C sav5=L_elus
C sav6=R8_ilus
C sav7=R_ekus
C sav8=I_ufus
C sav9=I_ofus
C sav10=I_ifus
C sav11=I_efus
C sav12=L_afus
C sav13=L_udus
C sav14=R_idus
C sav15=L_ebus
C sav16=L_oxos
      Call PUMP2_HANDLER(deltat,I_ofus,I_ifus,R_abus,
     & REAL(R_ibus,4),R_ixos,REAL(R_uxos,4),L_amus,
     & L_elus,L_upus,L_arus,L_udus,L_ikus,L_ukus,L_okus,L_alus
     &,L_ulus,
     & L_exos,L_ebus,L_axos,L_oxos,L_epus,
     & L_ipus,L_uvos,L_ovos,L_afus,I_ufus,R_imus,R_omus,L_orus
     &,
     & L_umit,L_opus,REAL(R8_okire,8),L_olus,REAL(R8_ilus
     &,8),R_erus,
     & REAL(R_akus,4),R_ekus,REAL(R8_odus,8),R_idus,R8_arire
     &,R_irus,R8_ilus,
     & REAL(R_obus,4),REAL(R_ubus,4))
C FDB50_vlv.fgi( 420,  90):recalc:���������� ���������� ������� 2,20FDB50AN002
C if(sav1.ne.R_irus .and. try1872.gt.0) goto 1872
C if(sav2.ne.R_erus .and. try1872.gt.0) goto 1872
C if(sav3.ne.L_amus .and. try1872.gt.0) goto 1872
C if(sav4.ne.L_ulus .and. try1872.gt.0) goto 1872
C if(sav5.ne.L_elus .and. try1872.gt.0) goto 1872
C if(sav6.ne.R8_ilus .and. try1872.gt.0) goto 1872
C if(sav7.ne.R_ekus .and. try1872.gt.0) goto 1872
C if(sav8.ne.I_ufus .and. try1872.gt.0) goto 1872
C if(sav9.ne.I_ofus .and. try1872.gt.0) goto 1872
C if(sav10.ne.I_ifus .and. try1872.gt.0) goto 1872
C if(sav11.ne.I_efus .and. try1872.gt.0) goto 1872
C if(sav12.ne.L_afus .and. try1872.gt.0) goto 1872
C if(sav13.ne.L_udus .and. try1872.gt.0) goto 1872
C if(sav14.ne.R_idus .and. try1872.gt.0) goto 1872
C if(sav15.ne.L_ebus .and. try1872.gt.0) goto 1872
C if(sav16.ne.L_oxos .and. try1872.gt.0) goto 1872
      if(L_ulus) then
         R_(78)=R_(79)
      else
         R_(78)=R_(80)
      endif
C FDB50_vent_log.fgi( 190, 475):���� RE IN LO CH7
      R8_emo=(R0_ulo*R_(77)+deltat*R_(78))/(R0_ulo+deltat
     &)
C FDB50_vent_log.fgi( 198, 476):�������������� �����  
      R8_amo=R8_emo
C FDB50_vent_log.fgi( 212, 476):������,F_FDB50AN002_G
      Call PUMP2_HANDLER(deltat,I_ador,I_ubor,R_ivir,
     & REAL(R_uvir,4),R_utir,REAL(R_evir,4),L_ikor,
     & L_ofor,L_emor,L_imor,L_ebor,L_udor,L_efor,L_afor,L_ifor
     &,L_ekor,
     & L_otir,L_ovir,L_itir,L_avir,L_olor,
     & L_ulor,L_etir,L_atir,L_ibor,I_edor,R_ukor,R_alor,L_apor
     &,
     & L_umit,L_amor,REAL(R8_okire,8),L_akor,REAL(R8_ufor
     &,8),R_omor,
     & REAL(R_idor,4),R_odor,REAL(R8_abor,8),R_uxir,R8_arire
     &,R_umor,R8_ufor,
     & REAL(R_axir,4),REAL(R_exir,4))
C FDB50_vlv.fgi( 406,  65):���������� ���������� ������� 2,20FDB50AN014
C label 1880  try1880=try1880-1
C sav1=R_umor
C sav2=R_omor
C sav3=L_ikor
C sav4=L_ekor
C sav5=L_ofor
C sav6=R8_ufor
C sav7=R_odor
C sav8=I_edor
C sav9=I_ador
C sav10=I_ubor
C sav11=I_obor
C sav12=L_ibor
C sav13=L_ebor
C sav14=R_uxir
C sav15=L_ovir
C sav16=L_avir
      Call PUMP2_HANDLER(deltat,I_ador,I_ubor,R_ivir,
     & REAL(R_uvir,4),R_utir,REAL(R_evir,4),L_ikor,
     & L_ofor,L_emor,L_imor,L_ebor,L_udor,L_efor,L_afor,L_ifor
     &,L_ekor,
     & L_otir,L_ovir,L_itir,L_avir,L_olor,
     & L_ulor,L_etir,L_atir,L_ibor,I_edor,R_ukor,R_alor,L_apor
     &,
     & L_umit,L_amor,REAL(R8_okire,8),L_akor,REAL(R8_ufor
     &,8),R_omor,
     & REAL(R_idor,4),R_odor,REAL(R8_abor,8),R_uxir,R8_arire
     &,R_umor,R8_ufor,
     & REAL(R_axir,4),REAL(R_exir,4))
C FDB50_vlv.fgi( 406,  65):recalc:���������� ���������� ������� 2,20FDB50AN014
C if(sav1.ne.R_umor .and. try1880.gt.0) goto 1880
C if(sav2.ne.R_omor .and. try1880.gt.0) goto 1880
C if(sav3.ne.L_ikor .and. try1880.gt.0) goto 1880
C if(sav4.ne.L_ekor .and. try1880.gt.0) goto 1880
C if(sav5.ne.L_ofor .and. try1880.gt.0) goto 1880
C if(sav6.ne.R8_ufor .and. try1880.gt.0) goto 1880
C if(sav7.ne.R_odor .and. try1880.gt.0) goto 1880
C if(sav8.ne.I_edor .and. try1880.gt.0) goto 1880
C if(sav9.ne.I_ador .and. try1880.gt.0) goto 1880
C if(sav10.ne.I_ubor .and. try1880.gt.0) goto 1880
C if(sav11.ne.I_obor .and. try1880.gt.0) goto 1880
C if(sav12.ne.L_ibor .and. try1880.gt.0) goto 1880
C if(sav13.ne.L_ebor .and. try1880.gt.0) goto 1880
C if(sav14.ne.R_uxir .and. try1880.gt.0) goto 1880
C if(sav15.ne.L_ovir .and. try1880.gt.0) goto 1880
C if(sav16.ne.L_avir .and. try1880.gt.0) goto 1880
      if(L_ekor) then
         R_(74)=R_(75)
      else
         R_(74)=R_(76)
      endif
C FDB50_vent_log.fgi( 243, 475):���� RE IN LO CH7
      R8_elo=(R0_uko*R_(73)+deltat*R_(74))/(R0_uko+deltat
     &)
C FDB50_vent_log.fgi( 251, 476):�������������� �����  
      R8_alo=R8_elo
C FDB50_vent_log.fgi( 266, 476):������,F_FDB50AN014_G
      Call PUMP2_HANDLER(deltat,I_imos,I_emos,R_ufos,
     & REAL(R_ekos,4),R_efos,REAL(R_ofos,4),L_uros,
     & L_aros,L_otos,L_utos,L_olos,L_epos,L_opos,L_ipos,L_upos
     &,L_oros,
     & L_afos,L_akos,L_udos,L_ifos,L_atos,
     & L_etos,L_odos,L_idos,L_ulos,I_omos,R_esos,R_isos,L_ivos
     &,
     & L_umit,L_itos,REAL(R8_okire,8),L_iros,REAL(R8_eros
     &,8),R_avos,
     & REAL(R_umos,4),R_apos,REAL(R8_ilos,8),R_elos,R8_arire
     &,R_evos,R8_eros,
     & REAL(R_ikos,4),REAL(R_okos,4))
C FDB50_vlv.fgi( 455,  90):���������� ���������� ������� 2,20FDB50AN004
C label 1888  try1888=try1888-1
C sav1=R_evos
C sav2=R_avos
C sav3=L_uros
C sav4=L_oros
C sav5=L_aros
C sav6=R8_eros
C sav7=R_apos
C sav8=I_omos
C sav9=I_imos
C sav10=I_emos
C sav11=I_amos
C sav12=L_ulos
C sav13=L_olos
C sav14=R_elos
C sav15=L_akos
C sav16=L_ifos
      Call PUMP2_HANDLER(deltat,I_imos,I_emos,R_ufos,
     & REAL(R_ekos,4),R_efos,REAL(R_ofos,4),L_uros,
     & L_aros,L_otos,L_utos,L_olos,L_epos,L_opos,L_ipos,L_upos
     &,L_oros,
     & L_afos,L_akos,L_udos,L_ifos,L_atos,
     & L_etos,L_odos,L_idos,L_ulos,I_omos,R_esos,R_isos,L_ivos
     &,
     & L_umit,L_itos,REAL(R8_okire,8),L_iros,REAL(R8_eros
     &,8),R_avos,
     & REAL(R_umos,4),R_apos,REAL(R8_ilos,8),R_elos,R8_arire
     &,R_evos,R8_eros,
     & REAL(R_ikos,4),REAL(R_okos,4))
C FDB50_vlv.fgi( 455,  90):recalc:���������� ���������� ������� 2,20FDB50AN004
C if(sav1.ne.R_evos .and. try1888.gt.0) goto 1888
C if(sav2.ne.R_avos .and. try1888.gt.0) goto 1888
C if(sav3.ne.L_uros .and. try1888.gt.0) goto 1888
C if(sav4.ne.L_oros .and. try1888.gt.0) goto 1888
C if(sav5.ne.L_aros .and. try1888.gt.0) goto 1888
C if(sav6.ne.R8_eros .and. try1888.gt.0) goto 1888
C if(sav7.ne.R_apos .and. try1888.gt.0) goto 1888
C if(sav8.ne.I_omos .and. try1888.gt.0) goto 1888
C if(sav9.ne.I_imos .and. try1888.gt.0) goto 1888
C if(sav10.ne.I_emos .and. try1888.gt.0) goto 1888
C if(sav11.ne.I_amos .and. try1888.gt.0) goto 1888
C if(sav12.ne.L_ulos .and. try1888.gt.0) goto 1888
C if(sav13.ne.L_olos .and. try1888.gt.0) goto 1888
C if(sav14.ne.R_elos .and. try1888.gt.0) goto 1888
C if(sav15.ne.L_akos .and. try1888.gt.0) goto 1888
C if(sav16.ne.L_ifos .and. try1888.gt.0) goto 1888
      if(L_oros) then
         R_(70)=R_(71)
      else
         R_(70)=R_(72)
      endif
C FDB50_vent_log.fgi( 294, 475):���� RE IN LO CH7
      R8_eko=(R0_ufo*R_(69)+deltat*R_(70))/(R0_ufo+deltat
     &)
C FDB50_vent_log.fgi( 302, 476):�������������� �����  
      R8_ako=R8_eko
C FDB50_vent_log.fgi( 316, 476):������,F_FDB50AN004_G
      Call PUMP2_HANDLER(deltat,I_ikit,I_ekit,R_ubit,
     & REAL(R_edit,4),R_ebit,REAL(R_obit,4),L_apit,
     & L_amit,L_urit,L_asit,L_ofit,L_elit,L_olit,L_ilit,L_ulit
     &,L_omit,
     & L_abit,L_adit,L_uxet,L_ibit,L_erit,
     & L_irit,L_oxet,L_ixet,L_ufit,I_okit,R_ipit,R_opit,L_osit
     &,
     & L_umit,L_orit,REAL(R8_okire,8),L_imit,REAL(R8_emit
     &,8),R_esit,
     & REAL(R_ukit,4),R_alit,REAL(R8_ifit,8),R_efit,R8_arire
     &,R_isit,R8_emit,
     & REAL(R_idit,4),REAL(R_odit,4))
C FDB50_vlv.fgi( 367,  90):���������� ���������� ������� 2,20FDB50AN009
C label 1896  try1896=try1896-1
C sav1=R_isit
C sav2=R_esit
C sav3=L_apit
C sav4=L_omit
C sav5=L_amit
C sav6=R8_emit
C sav7=R_alit
C sav8=I_okit
C sav9=I_ikit
C sav10=I_ekit
C sav11=I_akit
C sav12=L_ufit
C sav13=L_ofit
C sav14=R_efit
C sav15=L_adit
C sav16=L_ibit
      Call PUMP2_HANDLER(deltat,I_ikit,I_ekit,R_ubit,
     & REAL(R_edit,4),R_ebit,REAL(R_obit,4),L_apit,
     & L_amit,L_urit,L_asit,L_ofit,L_elit,L_olit,L_ilit,L_ulit
     &,L_omit,
     & L_abit,L_adit,L_uxet,L_ibit,L_erit,
     & L_irit,L_oxet,L_ixet,L_ufit,I_okit,R_ipit,R_opit,L_osit
     &,
     & L_umit,L_orit,REAL(R8_okire,8),L_imit,REAL(R8_emit
     &,8),R_esit,
     & REAL(R_ukit,4),R_alit,REAL(R8_ifit,8),R_efit,R8_arire
     &,R_isit,R8_emit,
     & REAL(R_idit,4),REAL(R_odit,4))
C FDB50_vlv.fgi( 367,  90):recalc:���������� ���������� ������� 2,20FDB50AN009
C if(sav1.ne.R_isit .and. try1896.gt.0) goto 1896
C if(sav2.ne.R_esit .and. try1896.gt.0) goto 1896
C if(sav3.ne.L_apit .and. try1896.gt.0) goto 1896
C if(sav4.ne.L_omit .and. try1896.gt.0) goto 1896
C if(sav5.ne.L_amit .and. try1896.gt.0) goto 1896
C if(sav6.ne.R8_emit .and. try1896.gt.0) goto 1896
C if(sav7.ne.R_alit .and. try1896.gt.0) goto 1896
C if(sav8.ne.I_okit .and. try1896.gt.0) goto 1896
C if(sav9.ne.I_ikit .and. try1896.gt.0) goto 1896
C if(sav10.ne.I_ekit .and. try1896.gt.0) goto 1896
C if(sav11.ne.I_akit .and. try1896.gt.0) goto 1896
C if(sav12.ne.L_ufit .and. try1896.gt.0) goto 1896
C if(sav13.ne.L_ofit .and. try1896.gt.0) goto 1896
C if(sav14.ne.R_efit .and. try1896.gt.0) goto 1896
C if(sav15.ne.L_adit .and. try1896.gt.0) goto 1896
C if(sav16.ne.L_ibit .and. try1896.gt.0) goto 1896
      if(L_omit) then
         R_(62)=R_(63)
      else
         R_(62)=R_(64)
      endif
C FDB50_vent_log.fgi( 401, 475):���� RE IN LO CH7
      R8_edo=(R0_ubo*R_(61)+deltat*R_(62))/(R0_ubo+deltat
     &)
C FDB50_vent_log.fgi( 409, 476):�������������� �����  
      R8_ado=R8_edo
C FDB50_vent_log.fgi( 424, 476):������,F_FDB50AN009_G
      Call PUMP2_HANDLER(deltat,I_olas,I_ilas,R_afas,
     & REAL(R_ifas,4),R_idas,REAL(R_udas,4),L_aras,
     & L_epas,L_usas,L_atas,L_ukas,L_imas,L_umas,L_omas,L_apas
     &,L_upas,
     & L_edas,L_efas,L_adas,L_odas,L_esas,
     & L_isas,L_ubas,L_obas,L_alas,I_ulas,R_iras,R_oras,L_otas
     &,
     & L_umit,L_osas,REAL(R8_okire,8),L_opas,REAL(R8_ipas
     &,8),R_etas,
     & REAL(R_amas,4),R_emas,REAL(R8_okas,8),R_ikas,R8_arire
     &,R_itas,R8_ipas,
     & REAL(R_ofas,4),REAL(R_ufas,4))
C FDB50_vlv.fgi( 367,  65):���������� ���������� ������� 2,20FDB50AN008
C label 1904  try1904=try1904-1
C sav1=R_itas
C sav2=R_etas
C sav3=L_aras
C sav4=L_upas
C sav5=L_epas
C sav6=R8_ipas
C sav7=R_emas
C sav8=I_ulas
C sav9=I_olas
C sav10=I_ilas
C sav11=I_elas
C sav12=L_alas
C sav13=L_ukas
C sav14=R_ikas
C sav15=L_efas
C sav16=L_odas
      Call PUMP2_HANDLER(deltat,I_olas,I_ilas,R_afas,
     & REAL(R_ifas,4),R_idas,REAL(R_udas,4),L_aras,
     & L_epas,L_usas,L_atas,L_ukas,L_imas,L_umas,L_omas,L_apas
     &,L_upas,
     & L_edas,L_efas,L_adas,L_odas,L_esas,
     & L_isas,L_ubas,L_obas,L_alas,I_ulas,R_iras,R_oras,L_otas
     &,
     & L_umit,L_osas,REAL(R8_okire,8),L_opas,REAL(R8_ipas
     &,8),R_etas,
     & REAL(R_amas,4),R_emas,REAL(R8_okas,8),R_ikas,R8_arire
     &,R_itas,R8_ipas,
     & REAL(R_ofas,4),REAL(R_ufas,4))
C FDB50_vlv.fgi( 367,  65):recalc:���������� ���������� ������� 2,20FDB50AN008
C if(sav1.ne.R_itas .and. try1904.gt.0) goto 1904
C if(sav2.ne.R_etas .and. try1904.gt.0) goto 1904
C if(sav3.ne.L_aras .and. try1904.gt.0) goto 1904
C if(sav4.ne.L_upas .and. try1904.gt.0) goto 1904
C if(sav5.ne.L_epas .and. try1904.gt.0) goto 1904
C if(sav6.ne.R8_ipas .and. try1904.gt.0) goto 1904
C if(sav7.ne.R_emas .and. try1904.gt.0) goto 1904
C if(sav8.ne.I_ulas .and. try1904.gt.0) goto 1904
C if(sav9.ne.I_olas .and. try1904.gt.0) goto 1904
C if(sav10.ne.I_ilas .and. try1904.gt.0) goto 1904
C if(sav11.ne.I_elas .and. try1904.gt.0) goto 1904
C if(sav12.ne.L_alas .and. try1904.gt.0) goto 1904
C if(sav13.ne.L_ukas .and. try1904.gt.0) goto 1904
C if(sav14.ne.R_ikas .and. try1904.gt.0) goto 1904
C if(sav15.ne.L_efas .and. try1904.gt.0) goto 1904
C if(sav16.ne.L_odas .and. try1904.gt.0) goto 1904
      Call PUMP2_HANDLER(deltat,I_epet,I_apet,R_oket,
     & REAL(R_alet,4),R_aket,REAL(R_iket,4),L_oset,
     & L_uret,L_ivet,L_ovet,L_imet,L_aret,L_iret,L_eret,L_oret
     &,L_iset,
     & L_ufet,L_uket,L_ofet,L_eket,L_utet,
     & L_avet,L_ifet,L_efet,L_omet,I_ipet,R_atet,R_etet,L_exet
     &,
     & L_umit,L_evet,REAL(R8_okire,8),L_eset,REAL(R8_aset
     &,8),R_uvet,
     & REAL(R_opet,4),R_upet,REAL(R8_emet,8),R_amet,R8_arire
     &,R_axet,R8_aset,
     & REAL(R_elet,4),REAL(R_ilet,4))
C FDB50_vlv.fgi( 380,  90):���������� ���������� ������� 2,20FDB50AN010
C label 1905  try1905=try1905-1
C sav1=R_axet
C sav2=R_uvet
C sav3=L_oset
C sav4=L_iset
C sav5=L_uret
C sav6=R8_aset
C sav7=R_upet
C sav8=I_ipet
C sav9=I_epet
C sav10=I_apet
C sav11=I_umet
C sav12=L_omet
C sav13=L_imet
C sav14=R_amet
C sav15=L_uket
C sav16=L_eket
      Call PUMP2_HANDLER(deltat,I_epet,I_apet,R_oket,
     & REAL(R_alet,4),R_aket,REAL(R_iket,4),L_oset,
     & L_uret,L_ivet,L_ovet,L_imet,L_aret,L_iret,L_eret,L_oret
     &,L_iset,
     & L_ufet,L_uket,L_ofet,L_eket,L_utet,
     & L_avet,L_ifet,L_efet,L_omet,I_ipet,R_atet,R_etet,L_exet
     &,
     & L_umit,L_evet,REAL(R8_okire,8),L_eset,REAL(R8_aset
     &,8),R_uvet,
     & REAL(R_opet,4),R_upet,REAL(R8_emet,8),R_amet,R8_arire
     &,R_axet,R8_aset,
     & REAL(R_elet,4),REAL(R_ilet,4))
C FDB50_vlv.fgi( 380,  90):recalc:���������� ���������� ������� 2,20FDB50AN010
C if(sav1.ne.R_axet .and. try1905.gt.0) goto 1905
C if(sav2.ne.R_uvet .and. try1905.gt.0) goto 1905
C if(sav3.ne.L_oset .and. try1905.gt.0) goto 1905
C if(sav4.ne.L_iset .and. try1905.gt.0) goto 1905
C if(sav5.ne.L_uret .and. try1905.gt.0) goto 1905
C if(sav6.ne.R8_aset .and. try1905.gt.0) goto 1905
C if(sav7.ne.R_upet .and. try1905.gt.0) goto 1905
C if(sav8.ne.I_ipet .and. try1905.gt.0) goto 1905
C if(sav9.ne.I_epet .and. try1905.gt.0) goto 1905
C if(sav10.ne.I_apet .and. try1905.gt.0) goto 1905
C if(sav11.ne.I_umet .and. try1905.gt.0) goto 1905
C if(sav12.ne.L_omet .and. try1905.gt.0) goto 1905
C if(sav13.ne.L_imet .and. try1905.gt.0) goto 1905
C if(sav14.ne.R_amet .and. try1905.gt.0) goto 1905
C if(sav15.ne.L_uket .and. try1905.gt.0) goto 1905
C if(sav16.ne.L_eket .and. try1905.gt.0) goto 1905
      if(L_iset) then
         R_(58)=R_(59)
      else
         R_(58)=R_(60)
      endif
C FDB50_vent_log.fgi( 452, 475):���� RE IN LO CH7
      R8_ebo=(R0_uxi*R_(57)+deltat*R_(58))/(R0_uxi+deltat
     &)
C FDB50_vent_log.fgi( 460, 476):�������������� �����  
      R8_abo=R8_ebo
C FDB50_vent_log.fgi( 474, 476):������,F_FDB50AN010_G
      Call PUMP2_HANDLER(deltat,I_irur,I_erur,R_ulur,
     & REAL(R_emur,4),R_elur,REAL(R_olur,4),L_utur,
     & L_atur,L_oxur,L_uxur,L_opur,L_esur,L_osur,L_isur,L_usur
     &,L_otur,
     & L_alur,L_amur,L_ukur,L_ilur,L_axur,
     & L_exur,L_okur,L_ikur,L_upur,I_orur,R_evur,R_ivur,L_ibas
     &,
     & L_umit,L_ixur,REAL(R8_okire,8),L_itur,REAL(R8_etur
     &,8),R_abas,
     & REAL(R_urur,4),R_asur,REAL(R8_ipur,8),R_epur,R8_arire
     &,R_ebas,R8_etur,
     & REAL(R_imur,4),REAL(R_omur,4))
C FDB50_vlv.fgi( 380,  65):���������� ���������� ������� 2,20FDB50AN012
C label 1913  try1913=try1913-1
C sav1=R_ebas
C sav2=R_abas
C sav3=L_utur
C sav4=L_otur
C sav5=L_atur
C sav6=R8_etur
C sav7=R_asur
C sav8=I_orur
C sav9=I_irur
C sav10=I_erur
C sav11=I_arur
C sav12=L_upur
C sav13=L_opur
C sav14=R_epur
C sav15=L_amur
C sav16=L_ilur
      Call PUMP2_HANDLER(deltat,I_irur,I_erur,R_ulur,
     & REAL(R_emur,4),R_elur,REAL(R_olur,4),L_utur,
     & L_atur,L_oxur,L_uxur,L_opur,L_esur,L_osur,L_isur,L_usur
     &,L_otur,
     & L_alur,L_amur,L_ukur,L_ilur,L_axur,
     & L_exur,L_okur,L_ikur,L_upur,I_orur,R_evur,R_ivur,L_ibas
     &,
     & L_umit,L_ixur,REAL(R8_okire,8),L_itur,REAL(R8_etur
     &,8),R_abas,
     & REAL(R_urur,4),R_asur,REAL(R8_ipur,8),R_epur,R8_arire
     &,R_ebas,R8_etur,
     & REAL(R_imur,4),REAL(R_omur,4))
C FDB50_vlv.fgi( 380,  65):recalc:���������� ���������� ������� 2,20FDB50AN012
C if(sav1.ne.R_ebas .and. try1913.gt.0) goto 1913
C if(sav2.ne.R_abas .and. try1913.gt.0) goto 1913
C if(sav3.ne.L_utur .and. try1913.gt.0) goto 1913
C if(sav4.ne.L_otur .and. try1913.gt.0) goto 1913
C if(sav5.ne.L_atur .and. try1913.gt.0) goto 1913
C if(sav6.ne.R8_etur .and. try1913.gt.0) goto 1913
C if(sav7.ne.R_asur .and. try1913.gt.0) goto 1913
C if(sav8.ne.I_orur .and. try1913.gt.0) goto 1913
C if(sav9.ne.I_irur .and. try1913.gt.0) goto 1913
C if(sav10.ne.I_erur .and. try1913.gt.0) goto 1913
C if(sav11.ne.I_arur .and. try1913.gt.0) goto 1913
C if(sav12.ne.L_upur .and. try1913.gt.0) goto 1913
C if(sav13.ne.L_opur .and. try1913.gt.0) goto 1913
C if(sav14.ne.R_epur .and. try1913.gt.0) goto 1913
C if(sav15.ne.L_amur .and. try1913.gt.0) goto 1913
C if(sav16.ne.L_ilur .and. try1913.gt.0) goto 1913
      Call PUMP2_HANDLER(deltat,I_atat,I_usat,R_ipat,
     & REAL(R_upat,4),R_umat,REAL(R_epat,4),L_ixat,
     & L_ovat,L_edet,L_idet,L_esat,L_utat,L_evat,L_avat,L_ivat
     &,L_exat,
     & L_omat,L_opat,L_imat,L_apat,L_obet,
     & L_ubet,L_emat,L_amat,L_isat,I_etat,R_uxat,R_abet,L_afet
     &,
     & L_umit,L_adet,REAL(R8_okire,8),L_axat,REAL(R8_uvat
     &,8),R_odet,
     & REAL(R_itat,4),R_otat,REAL(R8_asat,8),R_urat,R8_arire
     &,R_udet,R8_uvat,
     & REAL(R_arat,4),REAL(R_erat,4))
C FDB50_vlv.fgi( 393,  90):���������� ���������� ������� 2,20FDB50AN011
C label 1914  try1914=try1914-1
C sav1=R_udet
C sav2=R_odet
C sav3=L_ixat
C sav4=L_exat
C sav5=L_ovat
C sav6=R8_uvat
C sav7=R_otat
C sav8=I_etat
C sav9=I_atat
C sav10=I_usat
C sav11=I_osat
C sav12=L_isat
C sav13=L_esat
C sav14=R_urat
C sav15=L_opat
C sav16=L_apat
      Call PUMP2_HANDLER(deltat,I_atat,I_usat,R_ipat,
     & REAL(R_upat,4),R_umat,REAL(R_epat,4),L_ixat,
     & L_ovat,L_edet,L_idet,L_esat,L_utat,L_evat,L_avat,L_ivat
     &,L_exat,
     & L_omat,L_opat,L_imat,L_apat,L_obet,
     & L_ubet,L_emat,L_amat,L_isat,I_etat,R_uxat,R_abet,L_afet
     &,
     & L_umit,L_adet,REAL(R8_okire,8),L_axat,REAL(R8_uvat
     &,8),R_odet,
     & REAL(R_itat,4),R_otat,REAL(R8_asat,8),R_urat,R8_arire
     &,R_udet,R8_uvat,
     & REAL(R_arat,4),REAL(R_erat,4))
C FDB50_vlv.fgi( 393,  90):recalc:���������� ���������� ������� 2,20FDB50AN011
C if(sav1.ne.R_udet .and. try1914.gt.0) goto 1914
C if(sav2.ne.R_odet .and. try1914.gt.0) goto 1914
C if(sav3.ne.L_ixat .and. try1914.gt.0) goto 1914
C if(sav4.ne.L_exat .and. try1914.gt.0) goto 1914
C if(sav5.ne.L_ovat .and. try1914.gt.0) goto 1914
C if(sav6.ne.R8_uvat .and. try1914.gt.0) goto 1914
C if(sav7.ne.R_otat .and. try1914.gt.0) goto 1914
C if(sav8.ne.I_etat .and. try1914.gt.0) goto 1914
C if(sav9.ne.I_atat .and. try1914.gt.0) goto 1914
C if(sav10.ne.I_usat .and. try1914.gt.0) goto 1914
C if(sav11.ne.I_osat .and. try1914.gt.0) goto 1914
C if(sav12.ne.L_isat .and. try1914.gt.0) goto 1914
C if(sav13.ne.L_esat .and. try1914.gt.0) goto 1914
C if(sav14.ne.R_urat .and. try1914.gt.0) goto 1914
C if(sav15.ne.L_opat .and. try1914.gt.0) goto 1914
C if(sav16.ne.L_apat .and. try1914.gt.0) goto 1914
      Call PUMP2_HANDLER(deltat,I_evor,I_avor,R_oror,
     & REAL(R_asor,4),R_aror,REAL(R_iror,4),L_obur,
     & L_uxor,L_ifur,L_ofur,L_itor,L_axor,L_ixor,L_exor,L_oxor
     &,L_ibur,
     & L_upor,L_uror,L_opor,L_eror,L_udur,
     & L_afur,L_ipor,L_epor,L_otor,I_ivor,R_adur,R_edur,L_ekur
     &,
     & L_umit,L_efur,REAL(R8_okire,8),L_ebur,REAL(R8_abur
     &,8),R_ufur,
     & REAL(R_ovor,4),R_uvor,REAL(R8_etor,8),R_ator,R8_arire
     &,R_akur,R8_abur,
     & REAL(R_esor,4),REAL(R_isor,4))
C FDB50_vlv.fgi( 393,  65):���������� ���������� ������� 2,20FDB50AN013
C label 1915  try1915=try1915-1
C sav1=R_akur
C sav2=R_ufur
C sav3=L_obur
C sav4=L_ibur
C sav5=L_uxor
C sav6=R8_abur
C sav7=R_uvor
C sav8=I_ivor
C sav9=I_evor
C sav10=I_avor
C sav11=I_utor
C sav12=L_otor
C sav13=L_itor
C sav14=R_ator
C sav15=L_uror
C sav16=L_eror
      Call PUMP2_HANDLER(deltat,I_evor,I_avor,R_oror,
     & REAL(R_asor,4),R_aror,REAL(R_iror,4),L_obur,
     & L_uxor,L_ifur,L_ofur,L_itor,L_axor,L_ixor,L_exor,L_oxor
     &,L_ibur,
     & L_upor,L_uror,L_opor,L_eror,L_udur,
     & L_afur,L_ipor,L_epor,L_otor,I_ivor,R_adur,R_edur,L_ekur
     &,
     & L_umit,L_efur,REAL(R8_okire,8),L_ebur,REAL(R8_abur
     &,8),R_ufur,
     & REAL(R_ovor,4),R_uvor,REAL(R8_etor,8),R_ator,R8_arire
     &,R_akur,R8_abur,
     & REAL(R_esor,4),REAL(R_isor,4))
C FDB50_vlv.fgi( 393,  65):recalc:���������� ���������� ������� 2,20FDB50AN013
C if(sav1.ne.R_akur .and. try1915.gt.0) goto 1915
C if(sav2.ne.R_ufur .and. try1915.gt.0) goto 1915
C if(sav3.ne.L_obur .and. try1915.gt.0) goto 1915
C if(sav4.ne.L_ibur .and. try1915.gt.0) goto 1915
C if(sav5.ne.L_uxor .and. try1915.gt.0) goto 1915
C if(sav6.ne.R8_abur .and. try1915.gt.0) goto 1915
C if(sav7.ne.R_uvor .and. try1915.gt.0) goto 1915
C if(sav8.ne.I_ivor .and. try1915.gt.0) goto 1915
C if(sav9.ne.I_evor .and. try1915.gt.0) goto 1915
C if(sav10.ne.I_avor .and. try1915.gt.0) goto 1915
C if(sav11.ne.I_utor .and. try1915.gt.0) goto 1915
C if(sav12.ne.L_otor .and. try1915.gt.0) goto 1915
C if(sav13.ne.L_itor .and. try1915.gt.0) goto 1915
C if(sav14.ne.R_ator .and. try1915.gt.0) goto 1915
C if(sav15.ne.L_uror .and. try1915.gt.0) goto 1915
C if(sav16.ne.L_eror .and. try1915.gt.0) goto 1915
      Call PUMP2_HANDLER(deltat,I_udes,I_odes,R_exas,
     & REAL(R_oxas,4),R_ovas,REAL(R_axas,4),L_eles,
     & L_ikes,L_apes,L_epes,L_ades,L_ofes,L_akes,L_ufes,L_ekes
     &,L_ales,
     & L_ivas,L_ixas,L_evas,L_uvas,L_imes,
     & L_omes,L_avas,L_utas,L_edes,I_afes,R_oles,R_ules,L_upes
     &,
     & L_umit,L_umes,REAL(R8_okire,8),L_ukes,REAL(R8_okes
     &,8),R_ipes,
     & REAL(R_efes,4),R_ifes,REAL(R8_ubes,8),R_obes,R8_arire
     &,R_opes,R8_okes,
     & REAL(R_uxas,4),REAL(R_abes,4))
C FDB50_vlv.fgi( 500,  90):���������� ���������� ������� 2,20FDB50AN007
C label 1916  try1916=try1916-1
C sav1=R_opes
C sav2=R_ipes
C sav3=L_eles
C sav4=L_ales
C sav5=L_ikes
C sav6=R8_okes
C sav7=R_ifes
C sav8=I_afes
C sav9=I_udes
C sav10=I_odes
C sav11=I_ides
C sav12=L_edes
C sav13=L_ades
C sav14=R_obes
C sav15=L_ixas
C sav16=L_uvas
      Call PUMP2_HANDLER(deltat,I_udes,I_odes,R_exas,
     & REAL(R_oxas,4),R_ovas,REAL(R_axas,4),L_eles,
     & L_ikes,L_apes,L_epes,L_ades,L_ofes,L_akes,L_ufes,L_ekes
     &,L_ales,
     & L_ivas,L_ixas,L_evas,L_uvas,L_imes,
     & L_omes,L_avas,L_utas,L_edes,I_afes,R_oles,R_ules,L_upes
     &,
     & L_umit,L_umes,REAL(R8_okire,8),L_ukes,REAL(R8_okes
     &,8),R_ipes,
     & REAL(R_efes,4),R_ifes,REAL(R8_ubes,8),R_obes,R8_arire
     &,R_opes,R8_okes,
     & REAL(R_uxas,4),REAL(R_abes,4))
C FDB50_vlv.fgi( 500,  90):recalc:���������� ���������� ������� 2,20FDB50AN007
C if(sav1.ne.R_opes .and. try1916.gt.0) goto 1916
C if(sav2.ne.R_ipes .and. try1916.gt.0) goto 1916
C if(sav3.ne.L_eles .and. try1916.gt.0) goto 1916
C if(sav4.ne.L_ales .and. try1916.gt.0) goto 1916
C if(sav5.ne.L_ikes .and. try1916.gt.0) goto 1916
C if(sav6.ne.R8_okes .and. try1916.gt.0) goto 1916
C if(sav7.ne.R_ifes .and. try1916.gt.0) goto 1916
C if(sav8.ne.I_afes .and. try1916.gt.0) goto 1916
C if(sav9.ne.I_udes .and. try1916.gt.0) goto 1916
C if(sav10.ne.I_odes .and. try1916.gt.0) goto 1916
C if(sav11.ne.I_ides .and. try1916.gt.0) goto 1916
C if(sav12.ne.L_edes .and. try1916.gt.0) goto 1916
C if(sav13.ne.L_ades .and. try1916.gt.0) goto 1916
C if(sav14.ne.R_obes .and. try1916.gt.0) goto 1916
C if(sav15.ne.L_ixas .and. try1916.gt.0) goto 1916
C if(sav16.ne.L_uvas .and. try1916.gt.0) goto 1916
      if(L_ales) then
         R_(66)=R_(67)
      else
         R_(66)=R_(68)
      endif
C FDB50_vent_log.fgi( 347, 475):���� RE IN LO CH7
      R8_efo=(R0_udo*R_(65)+deltat*R_(66))/(R0_udo+deltat
     &)
C FDB50_vent_log.fgi( 355, 476):�������������� �����  
      R8_afo=R8_efo
C FDB50_vent_log.fgi( 370, 476):������,F_FDB50AN007_G
      Call PUMP2_HANDLER(deltat,I_uxus,I_oxus,R_etus,
     & REAL(R_otus,4),R_osus,REAL(R_atus,4),L_efat,
     & L_idat,L_alat,L_elat,L_axus,L_obat,L_adat,L_ubat,L_edat
     &,L_afat,
     & L_isus,L_itus,L_esus,L_usus,L_ikat,
     & L_okat,L_asus,L_urus,L_exus,I_abat,R_ofat,R_ufat,L_ulat
     &,
     & L_umit,L_ukat,REAL(R8_okire,8),L_udat,REAL(R8_odat
     &,8),R_ilat,
     & REAL(R_ebat,4),R_ibat,REAL(R8_uvus,8),R_ovus,R8_arire
     &,R_olat,R8_odat,
     & REAL(R_utus,4),REAL(R_avus,4))
C FDB50_vlv.fgi( 406,  90):���������� ���������� ������� 2,20FDB50AN001
C label 1924  try1924=try1924-1
C sav1=R_olat
C sav2=R_ilat
C sav3=L_efat
C sav4=L_afat
C sav5=L_idat
C sav6=R8_odat
C sav7=R_ibat
C sav8=I_abat
C sav9=I_uxus
C sav10=I_oxus
C sav11=I_ixus
C sav12=L_exus
C sav13=L_axus
C sav14=R_ovus
C sav15=L_itus
C sav16=L_usus
      Call PUMP2_HANDLER(deltat,I_uxus,I_oxus,R_etus,
     & REAL(R_otus,4),R_osus,REAL(R_atus,4),L_efat,
     & L_idat,L_alat,L_elat,L_axus,L_obat,L_adat,L_ubat,L_edat
     &,L_afat,
     & L_isus,L_itus,L_esus,L_usus,L_ikat,
     & L_okat,L_asus,L_urus,L_exus,I_abat,R_ofat,R_ufat,L_ulat
     &,
     & L_umit,L_ukat,REAL(R8_okire,8),L_udat,REAL(R8_odat
     &,8),R_ilat,
     & REAL(R_ebat,4),R_ibat,REAL(R8_uvus,8),R_ovus,R8_arire
     &,R_olat,R8_odat,
     & REAL(R_utus,4),REAL(R_avus,4))
C FDB50_vlv.fgi( 406,  90):recalc:���������� ���������� ������� 2,20FDB50AN001
C if(sav1.ne.R_olat .and. try1924.gt.0) goto 1924
C if(sav2.ne.R_ilat .and. try1924.gt.0) goto 1924
C if(sav3.ne.L_efat .and. try1924.gt.0) goto 1924
C if(sav4.ne.L_afat .and. try1924.gt.0) goto 1924
C if(sav5.ne.L_idat .and. try1924.gt.0) goto 1924
C if(sav6.ne.R8_odat .and. try1924.gt.0) goto 1924
C if(sav7.ne.R_ibat .and. try1924.gt.0) goto 1924
C if(sav8.ne.I_abat .and. try1924.gt.0) goto 1924
C if(sav9.ne.I_uxus .and. try1924.gt.0) goto 1924
C if(sav10.ne.I_oxus .and. try1924.gt.0) goto 1924
C if(sav11.ne.I_ixus .and. try1924.gt.0) goto 1924
C if(sav12.ne.L_exus .and. try1924.gt.0) goto 1924
C if(sav13.ne.L_axus .and. try1924.gt.0) goto 1924
C if(sav14.ne.R_ovus .and. try1924.gt.0) goto 1924
C if(sav15.ne.L_itus .and. try1924.gt.0) goto 1924
C if(sav16.ne.L_usus .and. try1924.gt.0) goto 1924
      Call PUMP2_HANDLER(deltat,I_uxid,I_oxid,R_etid,
     & REAL(R_otid,4),R_osid,REAL(R_atid,4),L_efod,
     & L_idod,L_alod,L_elod,L_axid,L_obod,L_adod,L_ubod,L_edod
     &,L_afod,
     & L_isid,L_itid,L_esid,L_usid,L_ikod,
     & L_okod,L_asid,L_urid,L_exid,I_abod,R_ofod,R_ufod,L_ulod
     &,
     & L_umit,L_ukod,REAL(R8_okire,8),L_udod,REAL(R8_odod
     &,8),R_ilod,
     & REAL(R_ebod,4),R_ibod,REAL(R8_uvid,8),R_ovid,R8_arire
     &,R_olod,R8_odod,
     & REAL(R_utid,4),REAL(R_avid,4))
C FDB50_vlv.fgi( 437,  90):���������� ���������� ������� 2,20FDB50AN003
C label 1925  try1925=try1925-1
C sav1=R_olod
C sav2=R_ilod
C sav3=L_efod
C sav4=L_afod
C sav5=L_idod
C sav6=R8_odod
C sav7=R_ibod
C sav8=I_abod
C sav9=I_uxid
C sav10=I_oxid
C sav11=I_ixid
C sav12=L_exid
C sav13=L_axid
C sav14=R_ovid
C sav15=L_itid
C sav16=L_usid
      Call PUMP2_HANDLER(deltat,I_uxid,I_oxid,R_etid,
     & REAL(R_otid,4),R_osid,REAL(R_atid,4),L_efod,
     & L_idod,L_alod,L_elod,L_axid,L_obod,L_adod,L_ubod,L_edod
     &,L_afod,
     & L_isid,L_itid,L_esid,L_usid,L_ikod,
     & L_okod,L_asid,L_urid,L_exid,I_abod,R_ofod,R_ufod,L_ulod
     &,
     & L_umit,L_ukod,REAL(R8_okire,8),L_udod,REAL(R8_odod
     &,8),R_ilod,
     & REAL(R_ebod,4),R_ibod,REAL(R8_uvid,8),R_ovid,R8_arire
     &,R_olod,R8_odod,
     & REAL(R_utid,4),REAL(R_avid,4))
C FDB50_vlv.fgi( 437,  90):recalc:���������� ���������� ������� 2,20FDB50AN003
C if(sav1.ne.R_olod .and. try1925.gt.0) goto 1925
C if(sav2.ne.R_ilod .and. try1925.gt.0) goto 1925
C if(sav3.ne.L_efod .and. try1925.gt.0) goto 1925
C if(sav4.ne.L_afod .and. try1925.gt.0) goto 1925
C if(sav5.ne.L_idod .and. try1925.gt.0) goto 1925
C if(sav6.ne.R8_odod .and. try1925.gt.0) goto 1925
C if(sav7.ne.R_ibod .and. try1925.gt.0) goto 1925
C if(sav8.ne.I_abod .and. try1925.gt.0) goto 1925
C if(sav9.ne.I_uxid .and. try1925.gt.0) goto 1925
C if(sav10.ne.I_oxid .and. try1925.gt.0) goto 1925
C if(sav11.ne.I_ixid .and. try1925.gt.0) goto 1925
C if(sav12.ne.L_exid .and. try1925.gt.0) goto 1925
C if(sav13.ne.L_axid .and. try1925.gt.0) goto 1925
C if(sav14.ne.R_ovid .and. try1925.gt.0) goto 1925
C if(sav15.ne.L_itid .and. try1925.gt.0) goto 1925
C if(sav16.ne.L_usid .and. try1925.gt.0) goto 1925
      Call PUMP2_HANDLER(deltat,I_esis,I_asis,R_omis,
     & REAL(R_apis,4),R_amis,REAL(R_imis,4),L_ovis,
     & L_utis,L_ibos,L_obos,L_iris,L_atis,L_itis,L_etis,L_otis
     &,L_ivis,
     & L_ulis,L_umis,L_olis,L_emis,L_uxis,
     & L_abos,L_ilis,L_elis,L_oris,I_isis,R_axis,R_exis,L_edos
     &,
     & L_umit,L_ebos,REAL(R8_okire,8),L_evis,REAL(R8_avis
     &,8),R_ubos,
     & REAL(R_osis,4),R_usis,REAL(R8_eris,8),R_aris,R8_arire
     &,R_ados,R8_avis,
     & REAL(R_epis,4),REAL(R_ipis,4))
C FDB50_vlv.fgi( 470,  90):���������� ���������� ������� 2,20FDB50AN005
C label 1926  try1926=try1926-1
C sav1=R_ados
C sav2=R_ubos
C sav3=L_ovis
C sav4=L_ivis
C sav5=L_utis
C sav6=R8_avis
C sav7=R_usis
C sav8=I_isis
C sav9=I_esis
C sav10=I_asis
C sav11=I_uris
C sav12=L_oris
C sav13=L_iris
C sav14=R_aris
C sav15=L_umis
C sav16=L_emis
      Call PUMP2_HANDLER(deltat,I_esis,I_asis,R_omis,
     & REAL(R_apis,4),R_amis,REAL(R_imis,4),L_ovis,
     & L_utis,L_ibos,L_obos,L_iris,L_atis,L_itis,L_etis,L_otis
     &,L_ivis,
     & L_ulis,L_umis,L_olis,L_emis,L_uxis,
     & L_abos,L_ilis,L_elis,L_oris,I_isis,R_axis,R_exis,L_edos
     &,
     & L_umit,L_ebos,REAL(R8_okire,8),L_evis,REAL(R8_avis
     &,8),R_ubos,
     & REAL(R_osis,4),R_usis,REAL(R8_eris,8),R_aris,R8_arire
     &,R_ados,R8_avis,
     & REAL(R_epis,4),REAL(R_ipis,4))
C FDB50_vlv.fgi( 470,  90):recalc:���������� ���������� ������� 2,20FDB50AN005
C if(sav1.ne.R_ados .and. try1926.gt.0) goto 1926
C if(sav2.ne.R_ubos .and. try1926.gt.0) goto 1926
C if(sav3.ne.L_ovis .and. try1926.gt.0) goto 1926
C if(sav4.ne.L_ivis .and. try1926.gt.0) goto 1926
C if(sav5.ne.L_utis .and. try1926.gt.0) goto 1926
C if(sav6.ne.R8_avis .and. try1926.gt.0) goto 1926
C if(sav7.ne.R_usis .and. try1926.gt.0) goto 1926
C if(sav8.ne.I_isis .and. try1926.gt.0) goto 1926
C if(sav9.ne.I_esis .and. try1926.gt.0) goto 1926
C if(sav10.ne.I_asis .and. try1926.gt.0) goto 1926
C if(sav11.ne.I_uris .and. try1926.gt.0) goto 1926
C if(sav12.ne.L_oris .and. try1926.gt.0) goto 1926
C if(sav13.ne.L_iris .and. try1926.gt.0) goto 1926
C if(sav14.ne.R_aris .and. try1926.gt.0) goto 1926
C if(sav15.ne.L_umis .and. try1926.gt.0) goto 1926
C if(sav16.ne.L_emis .and. try1926.gt.0) goto 1926
      Call PUMP2_HANDLER(deltat,I_axes,I_uves,R_ises,
     & REAL(R_uses,4),R_ures,REAL(R_eses,4),L_idis,
     & L_obis,L_ekis,L_ikis,L_eves,L_uxes,L_ebis,L_abis,L_ibis
     &,L_edis,
     & L_ores,L_oses,L_ires,L_ases,L_ofis,
     & L_ufis,L_eres,L_ares,L_ives,I_exes,R_udis,R_afis,L_alis
     &,
     & L_umit,L_akis,REAL(R8_okire,8),L_adis,REAL(R8_ubis
     &,8),R_okis,
     & REAL(R_ixes,4),R_oxes,REAL(R8_aves,8),R_utes,R8_arire
     &,R_ukis,R8_ubis,
     & REAL(R_ates,4),REAL(R_etes,4))
C FDB50_vlv.fgi( 485,  90):���������� ���������� ������� 2,20FDB50AN006
C label 1927  try1927=try1927-1
C sav1=R_ukis
C sav2=R_okis
C sav3=L_idis
C sav4=L_edis
C sav5=L_obis
C sav6=R8_ubis
C sav7=R_oxes
C sav8=I_exes
C sav9=I_axes
C sav10=I_uves
C sav11=I_oves
C sav12=L_ives
C sav13=L_eves
C sav14=R_utes
C sav15=L_oses
C sav16=L_ases
      Call PUMP2_HANDLER(deltat,I_axes,I_uves,R_ises,
     & REAL(R_uses,4),R_ures,REAL(R_eses,4),L_idis,
     & L_obis,L_ekis,L_ikis,L_eves,L_uxes,L_ebis,L_abis,L_ibis
     &,L_edis,
     & L_ores,L_oses,L_ires,L_ases,L_ofis,
     & L_ufis,L_eres,L_ares,L_ives,I_exes,R_udis,R_afis,L_alis
     &,
     & L_umit,L_akis,REAL(R8_okire,8),L_adis,REAL(R8_ubis
     &,8),R_okis,
     & REAL(R_ixes,4),R_oxes,REAL(R8_aves,8),R_utes,R8_arire
     &,R_ukis,R8_ubis,
     & REAL(R_ates,4),REAL(R_etes,4))
C FDB50_vlv.fgi( 485,  90):recalc:���������� ���������� ������� 2,20FDB50AN006
C if(sav1.ne.R_ukis .and. try1927.gt.0) goto 1927
C if(sav2.ne.R_okis .and. try1927.gt.0) goto 1927
C if(sav3.ne.L_idis .and. try1927.gt.0) goto 1927
C if(sav4.ne.L_edis .and. try1927.gt.0) goto 1927
C if(sav5.ne.L_obis .and. try1927.gt.0) goto 1927
C if(sav6.ne.R8_ubis .and. try1927.gt.0) goto 1927
C if(sav7.ne.R_oxes .and. try1927.gt.0) goto 1927
C if(sav8.ne.I_exes .and. try1927.gt.0) goto 1927
C if(sav9.ne.I_axes .and. try1927.gt.0) goto 1927
C if(sav10.ne.I_uves .and. try1927.gt.0) goto 1927
C if(sav11.ne.I_oves .and. try1927.gt.0) goto 1927
C if(sav12.ne.L_ives .and. try1927.gt.0) goto 1927
C if(sav13.ne.L_eves .and. try1927.gt.0) goto 1927
C if(sav14.ne.R_utes .and. try1927.gt.0) goto 1927
C if(sav15.ne.L_oses .and. try1927.gt.0) goto 1927
C if(sav16.ne.L_ases .and. try1927.gt.0) goto 1927
      End
