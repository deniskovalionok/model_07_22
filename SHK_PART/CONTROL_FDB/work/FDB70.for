      Subroutine FDB70(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDB70.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      Call FDB70_ConIn
      R_(34)=R8_efe
C FDB70_vent_log.fgi( 214, 170):pre: �������������� �����  
      R_(38)=R8_eke
C FDB70_vent_log.fgi( 137, 170):pre: �������������� �����  
      R_(42)=R8_ele
C FDB70_vent_log.fgi(  61, 170):pre: �������������� �����  
      R_(68)=R0_osem
C FDB70_logic.fgi( 135, 188):pre: �������� ��������� ������
      R_(67)=R0_asem
C FDB70_logic.fgi( 135, 181):pre: �������� ��������� ������
      R_(26)=R8_ot
C FDB70_vent_log.fgi(  64,  90):pre: �������������� �����  
      R_(18)=R8_ar
C FDB70_vent_log.fgi( 193,  84):pre: �������������� �����  
      R_(10)=R8_il
C FDB70_vent_log.fgi( 288,  84):pre: �������������� �����  
      !��������� R_(1) = FDB70_vent_logC?? /0.0005/
      R_(1)=R0_e
C FDB70_vent_log.fgi(  39,  38):���������
      L_(1)=R8_i.gt.R_(1)
C FDB70_vent_log.fgi(  43,  39):���������� >
      !��������� R_(3) = FDB70_vent_logC?? /0.0/
      R_(3)=R0_o
C FDB70_vent_log.fgi(  49,  45):���������
      !��������� R_(2) = FDB70_vent_logC?? /500/
      R_(2)=R0_u
C FDB70_vent_log.fgi(  49,  43):���������
      if(L_(1)) then
         R_(33)=R_(2)
      else
         R_(33)=R_(3)
      endif
C FDB70_vent_log.fgi(  52,  43):���� RE IN LO CH7
      !��������� R_(4) = FDB70_vent_logC?? /0.0005/
      R_(4)=R0_ad
C FDB70_vent_log.fgi( 263,  41):���������
      L_(2)=R8_ed.gt.R_(4)
C FDB70_vent_log.fgi( 267,  42):���������� >
      !��������� R_(6) = FDB70_vent_logC?? /0.0/
      R_(6)=R0_id
C FDB70_vent_log.fgi( 273,  48):���������
      !��������� R_(5) = FDB70_vent_logC?? /500/
      R_(5)=R0_od
C FDB70_vent_log.fgi( 273,  46):���������
      if(L_(2)) then
         R_(17)=R_(5)
      else
         R_(17)=R_(6)
      endif
C FDB70_vent_log.fgi( 276,  46):���� RE IN LO CH7
      !��������� R_(7) = FDB70_vent_logC?? /0.0005/
      R_(7)=R0_ef
C FDB70_vent_log.fgi( 168,  41):���������
      L_(3)=R8_if.gt.R_(7)
C FDB70_vent_log.fgi( 172,  42):���������� >
      !��������� R_(9) = FDB70_vent_logC?? /0.0/
      R_(9)=R0_of
C FDB70_vent_log.fgi( 178,  48):���������
      !��������� R_(8) = FDB70_vent_logC?? /500/
      R_(8)=R0_uf
C FDB70_vent_log.fgi( 178,  46):���������
      if(L_(3)) then
         R_(25)=R_(8)
      else
         R_(25)=R_(9)
      endif
C FDB70_vent_log.fgi( 181,  46):���� RE IN LO CH7
      R_(13) = 1.0
C FDB70_vent_log.fgi( 275,  82):��������� (RE4) (�������)
      R_(14) = 0.0
C FDB70_vent_log.fgi( 275,  84):��������� (RE4) (�������)
      !��������� R_(11) = FDB70_vent_logC?? /2.6/
      R_(11)=R0_ik
C FDB70_vent_log.fgi( 270, 109):���������
      !��������� R_(15) = FDB70_vent_logC?? /1/
      R_(15)=R0_ol
C FDB70_vent_log.fgi( 271,  57):���������
      L_(7)=R8_ux.lt.R_(15)
C FDB70_vent_log.fgi( 275,  58):���������� <
      L_am=L_ul.or.(L_am.and..not.(L_(7)))
      L_(8)=.not.L_am
C FDB70_vent_log.fgi( 305,  60):RS �������
      L_im=L_am
C FDB70_vent_log.fgi( 319,  62):������,20FDB73CW001_OUT
      !��������� R_(16) = FDB70_vent_logC?? /-50000/
      R_(16)=R0_em
C FDB70_vent_log.fgi( 302,  45):���������
      if(L_im) then
         R8_om=R_(16)
      else
         R8_om=R_(17)
      endif
C FDB70_vent_log.fgi( 305,  45):���� RE IN LO CH7
      R_(21) = 1.0
C FDB70_vent_log.fgi( 180,  82):��������� (RE4) (�������)
      R_(22) = 0.0
C FDB70_vent_log.fgi( 180,  84):��������� (RE4) (�������)
      !��������� R_(19) = FDB70_vent_logC?? /2.6/
      R_(19)=R0_ap
C FDB70_vent_log.fgi( 176, 109):���������
      !��������� R_(23) = FDB70_vent_logC?? /1/
      R_(23)=R0_er
C FDB70_vent_log.fgi( 176,  57):���������
      L_(12)=R8_ube.lt.R_(23)
C FDB70_vent_log.fgi( 180,  58):���������� <
      L_or=L_ir.or.(L_or.and..not.(L_(12)))
      L_(13)=.not.L_or
C FDB70_vent_log.fgi( 210,  60):RS �������
      L_as=L_or
C FDB70_vent_log.fgi( 224,  62):������,20FDB72CW001_OUT
      !��������� R_(24) = FDB70_vent_logC?? /-50000/
      R_(24)=R0_ur
C FDB70_vent_log.fgi( 207,  45):���������
      if(L_as) then
         R8_es=R_(24)
      else
         R8_es=R_(25)
      endif
C FDB70_vent_log.fgi( 210,  45):���� RE IN LO CH7
      R_(29) = 1.0
C FDB70_vent_log.fgi(  51,  88):��������� (RE4) (�������)
      R_(30) = 0.0
C FDB70_vent_log.fgi(  51,  90):��������� (RE4) (�������)
      !��������� R_(27) = FDB70_vent_logC?? /2.6/
      R_(27)=R0_os
C FDB70_vent_log.fgi(  46, 109):���������
      !��������� R_(31) = FDB70_vent_logC?? /1/
      R_(31)=R0_ut
C FDB70_vent_log.fgi(  47,  65):���������
      L_(17)=R8_epe.lt.R_(31)
C FDB70_vent_log.fgi(  51,  66):���������� <
      L_ev=L_av.or.(L_ev.and..not.(L_(17)))
      L_(18)=.not.L_ev
C FDB70_vent_log.fgi(  81,  68):RS �������
      L_ov=L_ev
C FDB70_vent_log.fgi(  96,  70):������,20FDB71CW001_OUT
      !��������� R_(32) = FDB70_vent_logC?? /-50000/
      R_(32)=R0_iv
C FDB70_vent_log.fgi(  77,  42):���������
      if(L_ov) then
         R8_uv=R_(32)
      else
         R8_uv=R_(33)
      endif
C FDB70_vent_log.fgi(  80,  42):���� RE IN LO CH7
      if(R8_ux.le.R0_ox) then
         R_efem=R0_ix
      elseif(R8_ux.gt.R0_ex) then
         R_efem=R0_ax
      else
         R_efem=R0_ix+(R8_ux-(R0_ox))*(R0_ax-(R0_ix))/(R0_ex
     &-(R0_ox))
      endif
C FDB70_vent_log.fgi( 209, 265):��������������� ���������
      L_(4)=R_efem.gt.R_(11)
C FDB70_vent_log.fgi( 274, 110):���������� >
      L_(5) = L_(4).OR.L_ud
C FDB70_vent_log.fgi( 299, 109):���
      L_uk=L_ok.or.(L_uk.and..not.(L_(5)))
      L_(6)=.not.L_uk
C FDB70_vent_log.fgi( 305, 111):RS �������
      L_al=L_uk
C FDB70_vent_log.fgi( 319, 113):������,FDB73dust_start
      if(L_al) then
         R_(12)=R_(13)
      else
         R_(12)=R_(14)
      endif
C FDB70_vent_log.fgi( 278,  83):���� RE IN LO CH7
      R8_il=(R0_ek*R_(10)+deltat*R_(12))/(R0_ek+deltat)
C FDB70_vent_log.fgi( 288,  84):�������������� �����  
      R8_el=R8_il
C FDB70_vent_log.fgi( 306,  84):������,PW21_klap
      !{
      Call DAT_ANA_HANDLER(deltat,R_adem,R_ukem,REAL(1,4)
     &,
     & REAL(R_odem,4),REAL(R_udem,4),
     & REAL(R_ubem,4),REAL(R_obem,4),I_okem,
     & REAL(R_ifem,4),L_ofem,REAL(R_ufem,4),L_akem,L_ekem
     &,R_afem,
     & REAL(R_idem,4),REAL(R_edem,4),L_ikem,REAL(R_efem,4
     &))
      !}
C FDB70_vlv.fgi( 150, 188):���������� �������,20FDB73CW001XQ01
      if(R8_ube.le.R0_obe) then
         R_ital=R0_ibe
      elseif(R8_ube.gt.R0_ebe) then
         R_ital=R0_abe
      else
         R_ital=R0_ibe+(R8_ube-(R0_obe))*(R0_abe-(R0_ibe)
     &)/(R0_ebe-(R0_obe))
      endif
C FDB70_vent_log.fgi( 132, 265):��������������� ���������
      L_(9)=R_ital.gt.R_(19)
C FDB70_vent_log.fgi( 180, 110):���������� >
      L_(10) = L_(9).OR.L_af
C FDB70_vent_log.fgi( 204, 109):���
      L_ip=L_ep.or.(L_ip.and..not.(L_(10)))
      L_(11)=.not.L_ip
C FDB70_vent_log.fgi( 210, 111):RS �������
      L_op=L_ip
C FDB70_vent_log.fgi( 224, 113):������,FDB72dust_start
      if(L_op) then
         R_(20)=R_(21)
      else
         R_(20)=R_(22)
      endif
C FDB70_vent_log.fgi( 183,  83):���� RE IN LO CH7
      R8_ar=(R0_um*R_(18)+deltat*R_(20))/(R0_um+deltat)
C FDB70_vent_log.fgi( 193,  84):�������������� �����  
      R8_up=R8_ar
C FDB70_vent_log.fgi( 211,  84):������,PW11_klap
      !{
      Call DAT_ANA_HANDLER(deltat,R_esal,R_axal,REAL(1,4)
     &,
     & REAL(R_usal,4),REAL(R_atal,4),
     & REAL(R_asal,4),REAL(R_ural,4),I_uval,
     & REAL(R_otal,4),L_utal,REAL(R_aval,4),L_eval,L_ival
     &,R_etal,
     & REAL(R_osal,4),REAL(R_isal,4),L_oval,REAL(R_ital,4
     &))
      !}
C FDB70_vlv.fgi( 183, 132):���������� �������,20FDB72CW001XQ01
      if(R8_epe.le.R0_ode) then
         R_ubel=R0_ide
      elseif(R8_epe.gt.R0_ede) then
         R_ubel=R0_ade
      else
         R_ubel=R0_ide+(R8_epe-(R0_ode))*(R0_ade-(R0_ide)
     &)/(R0_ede-(R0_ode))
      endif
C FDB70_vent_log.fgi(  56, 265):��������������� ���������
      L_(14)=R_ubel.gt.R_(27)
C FDB70_vent_log.fgi(  50, 110):���������� >
      L_(15) = L_(14).OR.L_ak
C FDB70_vent_log.fgi(  75, 109):���
      L_at=L_us.or.(L_at.and..not.(L_(15)))
      L_(16)=.not.L_at
C FDB70_vent_log.fgi(  81, 111):RS �������
      L_et=L_at
C FDB70_vent_log.fgi(  95, 113):������,FDB71dust_start
      if(L_et) then
         R_(28)=R_(29)
      else
         R_(28)=R_(30)
      endif
C FDB70_vent_log.fgi(  54,  89):���� RE IN LO CH7
      R8_ot=(R0_is*R_(26)+deltat*R_(28))/(R0_is+deltat)
C FDB70_vent_log.fgi(  64,  90):�������������� �����  
      R8_it=R8_ot
C FDB70_vent_log.fgi(  83,  90):������,PW1_klap
      !{
      Call DAT_ANA_HANDLER(deltat,R_oxal,R_ifel,REAL(1,4)
     &,
     & REAL(R_ebel,4),REAL(R_ibel,4),
     & REAL(R_ixal,4),REAL(R_exal,4),I_efel,
     & REAL(R_adel,4),L_edel,REAL(R_idel,4),L_odel,L_udel
     &,R_obel,
     & REAL(R_abel,4),REAL(R_uxal,4),L_afel,REAL(R_ubel,4
     &))
      !}
C FDB70_vlv.fgi( 151, 118):���������� �������,20FDB71CW001XQ01
      !��������� R_(37) = FDB70_vent_logC?? /0.0/
      R_(37)=R0_ife
C FDB70_vent_log.fgi( 201, 171):���������
      !��������� R_(36) = FDB70_vent_logC?? /0.001/
      R_(36)=R0_ofe
C FDB70_vent_log.fgi( 201, 169):���������
      !��������� R_(41) = FDB70_vent_logC?? /0.0/
      R_(41)=R0_ike
C FDB70_vent_log.fgi( 124, 171):���������
      !��������� R_(40) = FDB70_vent_logC?? /0.001/
      R_(40)=R0_oke
C FDB70_vent_log.fgi( 124, 169):���������
      !��������� R_(45) = FDB70_vent_logC?? /0.0/
      R_(45)=R0_ile
C FDB70_vent_log.fgi(  46, 171):���������
      !��������� R_(44) = FDB70_vent_logC?? /0.001/
      R_(44)=R0_ole
C FDB70_vent_log.fgi(  46, 169):���������
      R_(46) = 100
C FDB70_vent_log.fgi( 203, 271):��������� (RE4) (�������)
      R_(48) = 1e-3
C FDB70_vent_log.fgi( 209, 216):��������� (RE4) (�������)
      R_(49) = R8_ule * R_(48)
C FDB70_vent_log.fgi( 212, 218):����������
      R_ikad = R_(49)
C FDB70_vent_log.fgi( 216, 218):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_efad,R_amad,REAL(1,4)
     &,
     & REAL(R_ufad,4),REAL(R_akad,4),
     & REAL(R_afad,4),REAL(R_udad,4),I_ulad,
     & REAL(R_okad,4),L_ukad,REAL(R_alad,4),L_elad,L_ilad
     &,R_ekad,
     & REAL(R_ofad,4),REAL(R_ifad,4),L_olad,REAL(R_ikad,4
     &))
      !}
C FDB70_vlv.fgi( 182,  89):���������� �������,20FDB73CP002XQ01
      R_(51) = R8_use + (-R8_ame)
C FDB70_vent_log.fgi( 201, 255):��������
      R_(50) = 1e-3
C FDB70_vent_log.fgi( 209, 252):��������� (RE4) (�������)
      R_(52) = R_(51) * R_(50)
C FDB70_vent_log.fgi( 212, 254):����������
      R_osu = R_(52)
C FDB70_vent_log.fgi( 216, 254):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_iru,R_evu,REAL(1,4),
     & REAL(R_asu,4),REAL(R_esu,4),
     & REAL(R_eru,4),REAL(R_aru,4),I_avu,
     & REAL(R_usu,4),L_atu,REAL(R_etu,4),L_itu,L_otu,R_isu
     &,
     & REAL(R_uru,4),REAL(R_oru,4),L_utu,REAL(R_osu,4))
      !}
C FDB70_vlv.fgi( 150,  88):���������� �������,20FDB73CP001XQ01
      R_abad = R8_eme
C FDB70_vent_log.fgi( 210, 242):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_uvu,R_odad,REAL(1,4),
     & REAL(R_ixu,4),REAL(R_oxu,4),
     & REAL(R_ovu,4),REAL(R_ivu,4),I_idad,
     & REAL(R_ebad,4),L_ibad,REAL(R_obad,4),L_ubad,L_adad
     &,R_uxu,
     & REAL(R_exu,4),REAL(R_axu,4),L_edad,REAL(R_abad,4))
      !}
C FDB70_vlv.fgi( 120,  88):���������� �������,20FDB73CT001XQ01
      R_upad = R8_ime
C FDB70_vent_log.fgi( 210, 230):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_omad,R_isad,REAL(1,4)
     &,
     & REAL(R_epad,4),REAL(R_ipad,4),
     & REAL(R_imad,4),REAL(R_emad,4),I_esad,
     & REAL(R_arad,4),L_erad,REAL(R_irad,4),L_orad,L_urad
     &,R_opad,
     & REAL(R_apad,4),REAL(R_umad,4),L_asad,REAL(R_upad,4
     &))
      !}
C FDB70_vlv.fgi( 183, 104):���������� �������,20FDB73CM001XQ01
      R_udu = R8_ome
C FDB70_vent_log.fgi( 300, 272):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_obu,R_iku,REAL(1,4),
     & REAL(R_edu,4),REAL(R_idu,4),
     & REAL(R_ibu,4),REAL(R_ebu,4),I_eku,
     & REAL(R_afu,4),L_efu,REAL(R_ifu,4),L_ofu,L_ufu,R_odu
     &,
     & REAL(R_adu,4),REAL(R_ubu,4),L_aku,REAL(R_udu,4))
      !}
C FDB70_vlv.fgi( 150, 104):���������� �������,20FDB74CW002XQ01
      R_oded = R8_ume
C FDB70_vent_log.fgi( 210, 193):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ibed,R_eked,REAL(1,4)
     &,
     & REAL(R_aded,4),REAL(R_eded,4),
     & REAL(R_ebed,4),REAL(R_abed,4),I_aked,
     & REAL(R_uded,4),L_afed,REAL(R_efed,4),L_ifed,L_ofed
     &,R_ided,
     & REAL(R_ubed,4),REAL(R_obed,4),L_ufed,REAL(R_oded,4
     &))
      !}
C FDB70_vlv.fgi( 120, 104):���������� �������,20FDB73CU001XQ01
      R_evad = R8_ape
C FDB70_vent_log.fgi( 210, 206):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_atad,R_uxad,REAL(1,4)
     &,
     & REAL(R_otad,4),REAL(R_utad,4),
     & REAL(R_usad,4),REAL(R_osad,4),I_oxad,
     & REAL(R_ivad,4),L_ovad,REAL(R_uvad,4),L_axad,L_exad
     &,R_avad,
     & REAL(R_itad,4),REAL(R_etad,4),L_ixad,REAL(R_evad,4
     &))
      !}
C FDB70_vlv.fgi( 183, 118):���������� �������,20FDB73CF001XQ01
      R_elel = R8_ipe
C FDB70_vent_log.fgi( 133, 193):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_akel,R_umel,REAL(1,4)
     &,
     & REAL(R_okel,4),REAL(R_ukel,4),
     & REAL(R_ufel,4),REAL(R_ofel,4),I_omel,
     & REAL(R_ilel,4),L_olel,REAL(R_ulel,4),L_amel,L_emel
     &,R_alel,
     & REAL(R_ikel,4),REAL(R_ekel,4),L_imel,REAL(R_elel,4
     &))
      !}
C FDB70_vlv.fgi( 120, 118):���������� �������,20FDB72CU001XQ01
      R_obol = R8_ope
C FDB70_vent_log.fgi( 133, 206):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ixil,R_efol,REAL(1,4)
     &,
     & REAL(R_abol,4),REAL(R_ebol,4),
     & REAL(R_exil,4),REAL(R_axil,4),I_afol,
     & REAL(R_ubol,4),L_adol,REAL(R_edol,4),L_idol,L_odol
     &,R_ibol,
     & REAL(R_uxil,4),REAL(R_oxil,4),L_udol,REAL(R_obol,4
     &))
      !}
C FDB70_vlv.fgi( 151, 132):���������� �������,20FDB72CF001XQ01
      R_orel = R8_upe
C FDB70_vent_log.fgi(  57, 193):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ipel,R_etel,REAL(1,4)
     &,
     & REAL(R_arel,4),REAL(R_erel,4),
     & REAL(R_epel,4),REAL(R_apel,4),I_atel,
     & REAL(R_urel,4),L_asel,REAL(R_esel,4),L_isel,L_osel
     &,R_irel,
     & REAL(R_upel,4),REAL(R_opel,4),L_usel,REAL(R_orel,4
     &))
      !}
C FDB70_vlv.fgi( 120, 132):���������� �������,20FDB71CU001XQ01
      R_(53) = 1e-3
C FDB70_vent_log.fgi( 132, 216):��������� (RE4) (�������)
      R_(54) = R8_are * R_(53)
C FDB70_vent_log.fgi( 135, 218):����������
      R_irol = R_(54)
C FDB70_vent_log.fgi( 139, 218):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_epol,R_atol,REAL(1,4)
     &,
     & REAL(R_upol,4),REAL(R_arol,4),
     & REAL(R_apol,4),REAL(R_umol,4),I_usol,
     & REAL(R_orol,4),L_urol,REAL(R_asol,4),L_esol,L_isol
     &,R_erol,
     & REAL(R_opol,4),REAL(R_ipol,4),L_osol,REAL(R_irol,4
     &))
      !}
C FDB70_vlv.fgi( 182, 146):���������� �������,20FDB72CP002XQ01
      R_alol = R8_ere
C FDB70_vent_log.fgi(  57, 206):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ufol,R_omol,REAL(1,4)
     &,
     & REAL(R_ikol,4),REAL(R_okol,4),
     & REAL(R_ofol,4),REAL(R_ifol,4),I_imol,
     & REAL(R_elol,4),L_ilol,REAL(R_olol,4),L_ulol,L_amol
     &,R_ukol,
     & REAL(R_ekol,4),REAL(R_akol,4),L_emol,REAL(R_alol,4
     &))
      !}
C FDB70_vlv.fgi( 151, 146):���������� �������,20FDB71CF001XQ01
      R_axel = R8_ire
C FDB70_vent_log.fgi( 133, 230):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_utel,R_obil,REAL(1,4)
     &,
     & REAL(R_ivel,4),REAL(R_ovel,4),
     & REAL(R_otel,4),REAL(R_itel,4),I_ibil,
     & REAL(R_exel,4),L_ixel,REAL(R_oxel,4),L_uxel,L_abil
     &,R_uvel,
     & REAL(R_evel,4),REAL(R_avel,4),L_ebil,REAL(R_axel,4
     &))
      !}
C FDB70_vlv.fgi( 120, 146):���������� �������,20FDB72CM001XQ01
      R_(55) = 1e-3
C FDB70_vent_log.fgi(  56, 217):��������� (RE4) (�������)
      R_(56) = R8_ore * R_(55)
C FDB70_vent_log.fgi(  59, 219):����������
      R_uvol = R_(56)
C FDB70_vent_log.fgi(  63, 219):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_otol,R_ibul,REAL(1,4)
     &,
     & REAL(R_evol,4),REAL(R_ivol,4),
     & REAL(R_itol,4),REAL(R_etol,4),I_ebul,
     & REAL(R_axol,4),L_exol,REAL(R_ixol,4),L_oxol,L_uxol
     &,R_ovol,
     & REAL(R_avol,4),REAL(R_utol,4),L_abul,REAL(R_uvol,4
     &))
      !}
C FDB70_vlv.fgi( 182, 160):���������� �������,20FDB71CP002XQ01
      R_atul = R8_ure
C FDB70_vent_log.fgi( 133, 242):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_urul,R_ovul,REAL(1,4)
     &,
     & REAL(R_isul,4),REAL(R_osul,4),
     & REAL(R_orul,4),REAL(R_irul,4),I_ivul,
     & REAL(R_etul,4),L_itul,REAL(R_otul,4),L_utul,L_avul
     &,R_usul,
     & REAL(R_esul,4),REAL(R_asul,4),L_evul,REAL(R_atul,4
     &))
      !}
C FDB70_vlv.fgi( 151, 160):���������� �������,20FDB72CT001XQ01
      R_ifil = R8_ase
C FDB70_vent_log.fgi(  57, 230):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_edil,R_alil,REAL(1,4)
     &,
     & REAL(R_udil,4),REAL(R_afil,4),
     & REAL(R_adil,4),REAL(R_ubil,4),I_ukil,
     & REAL(R_ofil,4),L_ufil,REAL(R_akil,4),L_ekil,L_ikil
     &,R_efil,
     & REAL(R_odil,4),REAL(R_idil,4),L_okil,REAL(R_ifil,4
     &))
      !}
C FDB70_vlv.fgi( 120, 160):���������� �������,20FDB71CM001XQ01
      R_(58) = R8_use + (-R8_ese)
C FDB70_vent_log.fgi( 124, 255):��������
      R_(57) = 1e-3
C FDB70_vent_log.fgi( 132, 252):��������� (RE4) (�������)
      R_(59) = R_(58) * R_(57)
C FDB70_vent_log.fgi( 135, 254):����������
      R_eful = R_(59)
C FDB70_vent_log.fgi( 139, 254):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_adul,R_ukul,REAL(1,4)
     &,
     & REAL(R_odul,4),REAL(R_udul,4),
     & REAL(R_ubul,4),REAL(R_obul,4),I_okul,
     & REAL(R_iful,4),L_oful,REAL(R_uful,4),L_akul,L_ekul
     &,R_aful,
     & REAL(R_idul,4),REAL(R_edul,4),L_ikul,REAL(R_eful,4
     &))
      !}
C FDB70_vlv.fgi( 182, 173):���������� �������,20FDB72CP001XQ01
      R_ibam = R8_ise
C FDB70_vent_log.fgi(  57, 242):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_exul,R_afam,REAL(1,4)
     &,
     & REAL(R_uxul,4),REAL(R_abam,4),
     & REAL(R_axul,4),REAL(R_uvul,4),I_udam,
     & REAL(R_obam,4),L_ubam,REAL(R_adam,4),L_edam,L_idam
     &,R_ebam,
     & REAL(R_oxul,4),REAL(R_ixul,4),L_odam,REAL(R_ibam,4
     &))
      !}
C FDB70_vlv.fgi( 151, 174):���������� �������,20FDB71CT001XQ01
      R_(60) = 100
C FDB70_vent_log.fgi( 126, 271):��������� (RE4) (�������)
      R_(63) = R8_use + (-R8_ose)
C FDB70_vent_log.fgi(  48, 255):��������
      R_(62) = 1e-3
C FDB70_vent_log.fgi(  56, 252):��������� (RE4) (�������)
      R_(64) = R_(63) * R_(62)
C FDB70_vent_log.fgi(  59, 254):����������
      R_omul = R_(64)
C FDB70_vent_log.fgi(  63, 254):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_ilul,R_erul,REAL(1,4)
     &,
     & REAL(R_amul,4),REAL(R_emul,4),
     & REAL(R_elul,4),REAL(R_alul,4),I_arul,
     & REAL(R_umul,4),L_apul,REAL(R_epul,4),L_ipul,L_opul
     &,R_imul,
     & REAL(R_ulul,4),REAL(R_olul,4),L_upul,REAL(R_omul,4
     &))
      !}
C FDB70_vlv.fgi( 182, 187):���������� �������,20FDB71CP001XQ01
      R_(65) = 100
C FDB70_vent_log.fgi(  50, 271):��������� (RE4) (�������)
      !{
      Call DAT_ANA_HANDLER(deltat,R_ite,R_ebi,REAL(1,4),
     & REAL(R_ave,4),REAL(R_eve,4),
     & REAL(R_ete,4),REAL(R_ate,4),I_abi,
     & REAL(R_uve,4),L_axe,REAL(R_exe,4),L_ixe,L_oxe,R_ive
     &,
     & REAL(R_ute,4),REAL(R_ote,4),L_uxe,REAL(R_ove,4))
      !}
C FDB70_vlv.fgi( 182,  42):���������� �������,20FDB73CP006XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_ubi,R_oki,REAL(1,4),
     & REAL(R_idi,4),REAL(R_odi,4),
     & REAL(R_obi,4),REAL(R_ibi,4),I_iki,
     & REAL(R_efi,4),L_ifi,REAL(R_ofi,4),L_ufi,L_aki,R_udi
     &,
     & REAL(R_edi,4),REAL(R_adi,4),L_eki,REAL(R_afi,4))
      !}
C FDB70_vlv.fgi( 150,  42):���������� �������,20FDB73CP005XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_eli,R_ari,REAL(1,4),
     & REAL(R_uli,4),REAL(R_ami,4),
     & REAL(R_ali,4),REAL(R_uki,4),I_upi,
     & REAL(R_omi,4),L_umi,REAL(R_api,4),L_epi,L_ipi,R_emi
     &,
     & REAL(R_oli,4),REAL(R_ili,4),L_opi,REAL(R_imi,4))
      !}
C FDB70_vlv.fgi( 119,  42):���������� �������,20FDB73CP004XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_ori,R_ivi,REAL(1,4),
     & REAL(R_esi,4),REAL(R_isi,4),
     & REAL(R_iri,4),REAL(R_eri,4),I_evi,
     & REAL(R_ati,4),L_eti,REAL(R_iti,4),L_oti,L_uti,R_osi
     &,
     & REAL(R_asi,4),REAL(R_uri,4),L_avi,REAL(R_usi,4))
      !}
C FDB70_vlv.fgi( 182,  57):���������� �������,20FDB73CP003XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_axi,R_udo,REAL(1,4),
     & REAL(R_oxi,4),REAL(R_uxi,4),
     & REAL(R_uvi,4),REAL(R_ovi,4),I_odo,
     & REAL(R_ibo,4),L_obo,REAL(R_ubo,4),L_ado,L_edo,R_abo
     &,
     & REAL(R_ixi,4),REAL(R_exi,4),L_ido,REAL(R_ebo,4))
      !}
C FDB70_vlv.fgi( 150,  57):���������� �������,20FDB72CP005XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_ifo,R_emo,REAL(1,4),
     & REAL(R_ako,4),REAL(R_eko,4),
     & REAL(R_efo,4),REAL(R_afo,4),I_amo,
     & REAL(R_uko,4),L_alo,REAL(R_elo,4),L_ilo,L_olo,R_iko
     &,
     & REAL(R_ufo,4),REAL(R_ofo,4),L_ulo,REAL(R_oko,4))
      !}
C FDB70_vlv.fgi( 119,  57):���������� �������,20FDB72CP004XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_umo,R_oso,REAL(1,4),
     & REAL(R_ipo,4),REAL(R_opo,4),
     & REAL(R_omo,4),REAL(R_imo,4),I_iso,
     & REAL(R_ero,4),L_iro,REAL(R_oro,4),L_uro,L_aso,R_upo
     &,
     & REAL(R_epo,4),REAL(R_apo,4),L_eso,REAL(R_aro,4))
      !}
C FDB70_vlv.fgi( 182,  72):���������� �������,20FDB72CP003XQ01
      !{
      Call DAT_ANA_HANDLER(deltat,R_eto,R_abu,REAL(1,4),
     & REAL(R_uto,4),REAL(R_avo,4),
     & REAL(R_ato,4),REAL(R_uso,4),I_uxo,
     & REAL(R_ovo,4),L_uvo,REAL(R_axo,4),L_exo,L_ixo,R_evo
     &,
     & REAL(R_oto,4),REAL(R_ito,4),L_oxo,REAL(R_ivo,4))
      !}
C FDB70_vlv.fgi( 150,  72):���������� �������,20FDB71CP003XQ01
      !{
      Call BVALVE_MAN(deltat,REAL(R_ired,4),L_uved,L_axed
     &,R8_imed,L_uxuk,
     & L_ibal,L_emal,I_uted,I_aved,R_iked,
     & REAL(R_uked,4),R_oled,REAL(R_amed,4),
     & R_aled,REAL(R_iled,4),I_ated,I_eved,I_oted,I_ited,L_emed
     &,
     & L_ived,L_uxed,L_uled,L_eled,
     & L_eped,L_aped,L_exed,L_oked,L_oped,L_ibid,L_oved,
     & L_ared,L_ered,REAL(R8_ofam,8),REAL(1.0,4),R8_apam,L_umed
     &,
     & L_omed,L_abid,R_used,REAL(R_iped,4),L_ebid,L_obid,L_ored
     &,
     & L_ured,L_ased,L_ised,L_osed,L_esed)
      !}

      if(L_osed.or.L_ised.or.L_esed.or.L_ased.or.L_ured.or.L_ored
     &) then      
                  I_eted = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_eted = z'40000000'
      endif
C FDB70_vlv.fgi(  58,  81):���� ���������� ������ �������,20FDB73AA104
      !{
      Call BVALVE_MAN(deltat,REAL(R_ulid,4),L_esid,L_isid
     &,R8_ufid,L_uxuk,
     & L_ibal,L_emal,I_erid,I_irid,R_ubid,
     & REAL(R_edid,4),R_afid,REAL(R_ifid,4),
     & R_idid,REAL(R_udid,4),I_ipid,I_orid,I_arid,I_upid,L_ofid
     &,
     & L_urid,L_etid,L_efid,L_odid,
     & L_okid,L_ikid,L_osid,L_adid,L_alid,L_utid,L_asid,
     & L_ilid,L_olid,REAL(R8_ofam,8),REAL(1.0,4),R8_apam,L_ekid
     &,
     & L_akid,L_itid,R_epid,REAL(R_ukid,4),L_otid,L_avid,L_amid
     &,
     & L_emid,L_imid,L_umid,L_apid,L_omid)
      !}

      if(L_apid.or.L_umid.or.L_omid.or.L_imid.or.L_emid.or.L_amid
     &) then      
                  I_opid = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_opid = z'40000000'
      endif
C FDB70_vlv.fgi(  37,  81):���� ���������� ������ �������,20FDB73AA103
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ofod,4),L_apod,L_epod
     &,R8_ebod,C30_odod,
     & L_uxuk,L_ibal,L_emal,I_amod,I_emod,R_afod,R_udod,
     & R_evid,REAL(R_ovid,4),R_ixid,
     & REAL(R_uxid,4),R_uvid,REAL(R_exid,4),I_elod,
     & I_imod,I_ulod,I_olod,L_abod,L_omod,L_arod,L_oxid,
     & L_axid,L_adod,L_ubod,L_ipod,L_ivid,L_idod,
     & L_orod,L_umod,L_efod,L_ifod,REAL(R8_ofam,8),REAL(1.0
     &,4),R8_apam,
     & L_obod,L_ibod,L_erod,R_alod,REAL(R_edod,4),L_irod,L_urod
     &,
     & L_ufod,L_akod,L_ekod,L_okod,L_ukod,L_ikod)
      !}

      if(L_ukod.or.L_okod.or.L_ikod.or.L_ekod.or.L_akod.or.L_ufod
     &) then      
                  I_ilod = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ilod = z'40000000'
      endif
C FDB70_vlv.fgi(  16,  81):���� ���������� �������� ��������,20FDB73AA101
      R_(47) = R_alod * R_(46)
C FDB70_vent_log.fgi( 206, 273):����������
      R_emu = R_(47)
C FDB70_vent_log.fgi( 210, 273):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_alu,R_upu,REAL(1,4),
     & REAL(R_olu,4),REAL(R_ulu,4),
     & REAL(R_uku,4),REAL(R_oku,4),I_opu,
     & REAL(R_imu,4),L_omu,REAL(R_umu,4),L_apu,L_epu,R_amu
     &,
     & REAL(R_ilu,4),REAL(R_elu,4),L_ipu,REAL(R_emu,4))
      !}
C FDB70_vlv.fgi( 120,  72):���������� �������,20FDB73CG001XQ01
      !{
      Call BVALVE_MAN(deltat,REAL(R_abud,4),L_ikud,L_okud
     &,R8_avod,L_uxuk,
     & L_ibal,L_emal,I_ifud,I_ofud,R_asod,
     & REAL(R_isod,4),R_etod,REAL(R_otod,4),
     & R_osod,REAL(R_atod,4),I_odud,I_ufud,I_efud,I_afud,L_utod
     &,
     & L_akud,L_ilud,L_itod,L_usod,
     & L_uvod,L_ovod,L_ukud,L_esod,L_exod,L_amud,L_ekud,
     & L_oxod,L_uxod,REAL(R8_ofam,8),REAL(1.0,4),R8_apam,L_ivod
     &,
     & L_evod,L_olud,R_idud,REAL(R_axod,4),L_ulud,L_emud,L_ebud
     &,
     & L_ibud,L_obud,L_adud,L_edud,L_ubud)
      !}

      if(L_edud.or.L_adud.or.L_ubud.or.L_obud.or.L_ibud.or.L_ebud
     &) then      
                  I_udud = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_udud = z'40000000'
      endif
C FDB70_vlv.fgi(  77, 106):���� ���������� ������ �������,20FDB73AA102
      !{
      Call BVALVE_MAN(deltat,REAL(R_itud,4),L_ubaf,L_adaf
     &,R8_irud,L_uxuk,
     & L_ibal,L_emal,I_uxud,I_abaf,R_imud,
     & REAL(R_umud,4),R_opud,REAL(R_arud,4),
     & R_apud,REAL(R_ipud,4),I_axud,I_ebaf,I_oxud,I_ixud,L_erud
     &,
     & L_ibaf,L_udaf,L_upud,L_epud,
     & L_esud,L_asud,L_edaf,L_omud,L_osud,L_ifaf,L_obaf,
     & L_atud,L_etud,REAL(R8_ofam,8),REAL(1.0,4),R8_apam,L_urud
     &,
     & L_orud,L_afaf,R_uvud,REAL(R_isud,4),L_efaf,L_ofaf,L_otud
     &,
     & L_utud,L_avud,L_ivud,L_ovud,L_evud)
      !}

      if(L_ovud.or.L_ivud.or.L_evud.or.L_avud.or.L_utud.or.L_otud
     &) then      
                  I_exud = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_exud = z'40000000'
      endif
C FDB70_vlv.fgi(  58, 106):���� ���������� ������ �������,20FDB73CF101KA02
      !{
      Call BVALVE_MAN(deltat,REAL(R_olef,4),L_asef,L_esef
     &,R8_ofef,L_uxuk,
     & L_ibal,L_emal,I_aref,I_eref,R_obef,
     & REAL(R_adef,4),R_udef,REAL(R_efef,4),
     & R_edef,REAL(R_odef,4),I_epef,I_iref,I_upef,I_opef,L_ifef
     &,
     & L_oref,L_atef,L_afef,L_idef,
     & L_ikef,L_ekef,L_isef,L_ubef,L_ukef,L_otef,L_uref,
     & L_elef,L_ilef,REAL(R8_ofam,8),REAL(1.0,4),R8_apam,L_akef
     &,
     & L_ufef,L_etef,R_apef,REAL(R_okef,4),L_itef,L_utef,L_ulef
     &,
     & L_amef,L_emef,L_omef,L_umef,L_imef)
      !}

      if(L_umef.or.L_omef.or.L_imef.or.L_emef.or.L_amef.or.L_ulef
     &) then      
                  I_ipef = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ipef = z'40000000'
      endif
C FDB70_vlv.fgi(  37, 106):���� ���������� ������ �������,20FDB72CF101KA02
      !{
      Call BVALVE_MAN(deltat,REAL(R_afif,4),L_imif,L_omif
     &,R8_abif,L_uxuk,
     & L_ibal,L_emal,I_ilif,I_olif,R_avef,
     & REAL(R_ivef,4),R_exef,REAL(R_oxef,4),
     & R_ovef,REAL(R_axef,4),I_okif,I_ulif,I_elif,I_alif,L_uxef
     &,
     & L_amif,L_ipif,L_ixef,L_uvef,
     & L_ubif,L_obif,L_umif,L_evef,L_edif,L_arif,L_emif,
     & L_odif,L_udif,REAL(R8_ofam,8),REAL(1.0,4),R8_apam,L_ibif
     &,
     & L_ebif,L_opif,R_ikif,REAL(R_adif,4),L_upif,L_erif,L_efif
     &,
     & L_ifif,L_ofif,L_akif,L_ekif,L_ufif)
      !}

      if(L_ekif.or.L_akif.or.L_ufif.or.L_ofif.or.L_ifif.or.L_efif
     &) then      
                  I_ukif = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ukif = z'40000000'
      endif
C FDB70_vlv.fgi(  16, 106):���� ���������� ������ �������,20FDB71CF101KA02
      !{
      Call BVALVE_MAN(deltat,REAL(R_ixif,4),L_ufof,L_akof
     &,R8_itif,L_uxuk,
     & L_ibal,L_emal,I_udof,I_afof,R_irif,
     & REAL(R_urif,4),R_osif,REAL(R_atif,4),
     & R_asif,REAL(R_isif,4),I_adof,I_efof,I_odof,I_idof,L_etif
     &,
     & L_ifof,L_ukof,L_usif,L_esif,
     & L_evif,L_avif,L_ekof,L_orif,L_ovif,L_ilof,L_ofof,
     & L_axif,L_exif,REAL(R8_ofam,8),REAL(1.0,4),R8_apam,L_utif
     &,
     & L_otif,L_alof,R_ubof,REAL(R_ivif,4),L_elof,L_olof,L_oxif
     &,
     & L_uxif,L_abof,L_ibof,L_obof,L_ebof)
      !}

      if(L_obof.or.L_ibof.or.L_ebof.or.L_abof.or.L_uxif.or.L_oxif
     &) then      
                  I_edof = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_edof = z'40000000'
      endif
C FDB70_vlv.fgi(  77, 131):���� ���������� ������ �������,20FDB72AA101
      !{
      Call BVALVE_MAN(deltat,REAL(R_usof,4),L_ebuf,L_ibuf
     &,R8_upof,L_uxuk,
     & L_ibal,L_emal,I_exof,I_ixof,R_ulof,
     & REAL(R_emof,4),R_apof,REAL(R_ipof,4),
     & R_imof,REAL(R_umof,4),I_ivof,I_oxof,I_axof,I_uvof,L_opof
     &,
     & L_uxof,L_eduf,L_epof,L_omof,
     & L_orof,L_irof,L_obuf,L_amof,L_asof,L_uduf,L_abuf,
     & L_isof,L_osof,REAL(R8_ofam,8),REAL(1.0,4),R8_apam,L_erof
     &,
     & L_arof,L_iduf,R_evof,REAL(R_urof,4),L_oduf,L_afuf,L_atof
     &,
     & L_etof,L_itof,L_utof,L_avof,L_otof)
      !}

      if(L_avof.or.L_utof.or.L_otof.or.L_itof.or.L_etof.or.L_atof
     &) then      
                  I_ovof = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ovof = z'40000000'
      endif
C FDB70_vlv.fgi(  58, 131):���� ���������� ������ �������,20FDB71AA104
      !{
      Call BVALVE_MAN(deltat,REAL(R_epuf,4),L_otuf,L_utuf
     &,R8_eluf,L_uxuk,
     & L_ibal,L_emal,I_osuf,I_usuf,R_efuf,
     & REAL(R_ofuf,4),R_ikuf,REAL(R_ukuf,4),
     & R_ufuf,REAL(R_ekuf,4),I_uruf,I_atuf,I_isuf,I_esuf,L_aluf
     &,
     & L_etuf,L_ovuf,L_okuf,L_akuf,
     & L_amuf,L_uluf,L_avuf,L_ifuf,L_imuf,L_exuf,L_ituf,
     & L_umuf,L_apuf,REAL(R8_ofam,8),REAL(1.0,4),R8_apam,L_oluf
     &,
     & L_iluf,L_uvuf,R_oruf,REAL(R_emuf,4),L_axuf,L_ixuf,L_ipuf
     &,
     & L_opuf,L_upuf,L_eruf,L_iruf,L_aruf)
      !}

      if(L_iruf.or.L_eruf.or.L_aruf.or.L_upuf.or.L_opuf.or.L_ipuf
     &) then      
                  I_asuf = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_asuf = z'40000000'
      endif
C FDB70_vlv.fgi(  77, 156):���� ���������� ������ �������,20FDB72AA103
      !{
      Call BVALVE_MAN(deltat,REAL(R_okak,4),L_arak,L_erak
     &,R8_odak,L_uxuk,
     & L_ibal,L_emal,I_apak,I_epak,R_oxuf,
     & REAL(R_abak,4),R_ubak,REAL(R_edak,4),
     & R_ebak,REAL(R_obak,4),I_emak,I_ipak,I_umak,I_omak,L_idak
     &,
     & L_opak,L_asak,L_adak,L_ibak,
     & L_ifak,L_efak,L_irak,L_uxuf,L_ufak,L_osak,L_upak,
     & L_ekak,L_ikak,REAL(R8_ofam,8),REAL(1.0,4),R8_apam,L_afak
     &,
     & L_udak,L_esak,R_amak,REAL(R_ofak,4),L_isak,L_usak,L_ukak
     &,
     & L_alak,L_elak,L_olak,L_ulak,L_ilak)
      !}

      if(L_ulak.or.L_olak.or.L_ilak.or.L_elak.or.L_alak.or.L_ukak
     &) then      
                  I_imak = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_imak = z'40000000'
      endif
C FDB70_vlv.fgi(  58, 156):���� ���������� ������ �������,20FDB71AA103
      !{
      Call BVALVE_MAN(deltat,REAL(R_adek,4),L_ilek,L_olek
     &,R8_axak,L_uxuk,
     & L_ibal,L_emal,I_ikek,I_okek,R_atak,
     & REAL(R_itak,4),R_evak,REAL(R_ovak,4),
     & R_otak,REAL(R_avak,4),I_ofek,I_ukek,I_ekek,I_akek,L_uvak
     &,
     & L_alek,L_imek,L_ivak,L_utak,
     & L_uxak,L_oxak,L_ulek,L_etak,L_ebek,L_apek,L_elek,
     & L_obek,L_ubek,REAL(R8_ofam,8),REAL(1.0,4),R8_apam,L_ixak
     &,
     & L_exak,L_omek,R_ifek,REAL(R_abek,4),L_umek,L_epek,L_edek
     &,
     & L_idek,L_odek,L_afek,L_efek,L_udek)
      !}

      if(L_efek.or.L_afek.or.L_udek.or.L_odek.or.L_idek.or.L_edek
     &) then      
                  I_ufek = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ufek = z'40000000'
      endif
C FDB70_vlv.fgi(  37, 131):���� ���������� ������ �������,20FDB72AA102
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_uvek,4),L_efik,L_ifik
     &,R8_isek,C30_utek,
     & L_uxuk,L_ibal,L_emal,I_edik,I_idik,R_evek,R_avek,
     & R_ipek,REAL(R_upek,4),R_orek,
     & REAL(R_asek,4),R_arek,REAL(R_irek,4),I_ibik,
     & I_odik,I_adik,I_ubik,L_esek,L_udik,L_ekik,L_urek,
     & L_erek,L_etek,L_atek,L_ofik,L_opek,L_otek,
     & L_ukik,L_afik,L_ivek,L_ovek,REAL(R8_ofam,8),REAL(1.0
     &,4),R8_apam,
     & L_usek,L_osek,L_ikik,R_ebik,REAL(R_itek,4),L_okik,L_alik
     &,
     & L_axek,L_exek,L_ixek,L_uxek,L_abik,L_oxek)
      !}

      if(L_abik.or.L_uxek.or.L_oxek.or.L_ixek.or.L_exek.or.L_axek
     &) then      
                  I_obik = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_obik = z'40000000'
      endif
C FDB70_vlv.fgi(  37, 156):���� ���������� �������� ��������,20FDB72AA104
      R_(61) = R_ebik * R_(60)
C FDB70_vent_log.fgi( 129, 273):����������
      R_umil = R_(61)
C FDB70_vent_log.fgi( 133, 273):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_olil,R_iril,REAL(1,4)
     &,
     & REAL(R_emil,4),REAL(R_imil,4),
     & REAL(R_ilil,4),REAL(R_elil,4),I_eril,
     & REAL(R_apil,4),L_epil,REAL(R_ipil,4),L_opil,L_upil
     &,R_omil,
     & REAL(R_amil,4),REAL(R_ulil,4),L_aril,REAL(R_umil,4
     &))
      !}
C FDB70_vlv.fgi( 120, 174):���������� �������,20FDB72CG001XQ01
      !{
      Call DSHUTTER_MAN(deltat,REAL(R_ipok,4),L_utok,L_avok
     &,R8_alok,C30_imok,
     & L_uxuk,L_ibal,L_emal,I_usok,I_atok,R_umok,R_omok,
     & R_afok,REAL(R_ifok,4),R_ekok,
     & REAL(R_okok,4),R_ofok,REAL(R_akok,4),I_asok,
     & I_etok,I_osok,I_isok,L_ukok,L_itok,L_uvok,L_ikok,
     & L_ufok,L_ulok,L_olok,L_evok,L_efok,L_emok,
     & L_ixok,L_otok,L_apok,L_epok,REAL(R8_ofam,8),REAL(1.0
     &,4),R8_apam,
     & L_ilok,L_elok,L_axok,R_urok,REAL(R_amok,4),L_exok,L_oxok
     &,
     & L_opok,L_upok,L_arok,L_irok,L_orok,L_erok)
      !}

      if(L_orok.or.L_irok.or.L_erok.or.L_arok.or.L_upok.or.L_opok
     &) then      
                  I_esok = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_esok = z'40000000'
      endif
C FDB70_vlv.fgi(  16, 156):���� ���������� �������� ��������,20FDB71AA101
      R_(66) = R_urok * R_(65)
C FDB70_vent_log.fgi(  53, 273):����������
      R_etil = R_(66)
C FDB70_vent_log.fgi(  57, 273):��������
      !{
      Call DAT_ANA_HANDLER(deltat,R_asil,R_uvil,REAL(1,4)
     &,
     & REAL(R_osil,4),REAL(R_usil,4),
     & REAL(R_uril,4),REAL(R_oril,4),I_ovil,
     & REAL(R_itil,4),L_otil,REAL(R_util,4),L_avil,L_evil
     &,R_atil,
     & REAL(R_isil,4),REAL(R_esil,4),L_ivil,REAL(R_etil,4
     &))
      !}
C FDB70_vlv.fgi( 120, 187):���������� �������,20FDB71CG001XQ01
      !{
      Call BVALVE_MAN(deltat,REAL(R_ukuk,4),L_eruk,L_iruk
     &,R8_uduk,L_uxuk,
     & L_ibal,L_emal,I_epuk,I_ipuk,R_uxok,
     & REAL(R_ebuk,4),R_aduk,REAL(R_iduk,4),
     & R_ibuk,REAL(R_ubuk,4),I_imuk,I_opuk,I_apuk,I_umuk,L_oduk
     &,
     & L_upuk,L_esuk,L_eduk,L_obuk,
     & L_ofuk,L_ifuk,L_oruk,L_abuk,L_akuk,L_usuk,L_aruk,
     & L_ikuk,L_okuk,REAL(R8_ofam,8),REAL(1.0,4),R8_apam,L_efuk
     &,
     & L_afuk,L_isuk,R_emuk,REAL(R_ufuk,4),L_osuk,L_atuk,L_aluk
     &,
     & L_eluk,L_iluk,L_uluk,L_amuk,L_oluk)
      !}

      if(L_amuk.or.L_uluk.or.L_oluk.or.L_iluk.or.L_eluk.or.L_aluk
     &) then      
                  I_omuk = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_omuk = z'40000000'
      endif
C FDB70_vlv.fgi(  16, 131):���� ���������� ������ �������,20FDB71AA102
      L_(39)=I_alem.ne.0
C FDB70_logic.fgi(  52,  60):��������� 1->LO
      L_(27)=I_ulem.ne.0
C FDB70_logic.fgi(  46,  75):��������� 1->LO
      L_(30)=I_emem.ne.0
C FDB70_logic.fgi(  46,  84):��������� 1->LO
      L_(34) = L_(30).OR.L_(27)
C FDB70_logic.fgi(  55,  89):���
      L_(33)=I_omem.ne.0
C FDB70_logic.fgi(  46,  93):��������� 1->LO
      L_umem=(L_(33).or.L_umem).and..not.(L_(34))
      L_(35)=.not.L_umem
C FDB70_logic.fgi(  62,  91):RS �������
      L_olem=L_umem
C FDB70_logic.fgi( 101,  93):������,20FDB72AE702YU27
      L_(28) = L_(30).OR.L_(33)
C FDB70_logic.fgi(  55,  71):���
      L_amem=(L_(27).or.L_amem).and..not.(L_(28))
      L_(29)=.not.L_amem
C FDB70_logic.fgi(  62,  73):RS �������
      L_elem=L_amem
C FDB70_logic.fgi( 101,  75):������,20FDB72AE702YU29
      L_(31) = L_(33).OR.L_(27)
C FDB70_logic.fgi(  55,  80):���
      L_imem=(L_(30).or.L_imem).and..not.(L_(31))
      L_(32)=.not.L_imem
C FDB70_logic.fgi(  62,  82):RS �������
      L_ilem=L_imem
C FDB70_logic.fgi( 101,  84):������,20FDB72AE702YU28
      L_(37)=I_epem.ne.0
C FDB70_logic.fgi(  50, 102):��������� 1->LO
      L_(36)=I_ipem.ne.0
C FDB70_logic.fgi(  50, 106):��������� 1->LO
      L_opem=(L_(36).or.L_opem).and..not.(L_(37))
      L_upem=.not.L_opem
C FDB70_logic.fgi(  60, 104):RS �������
      L_apem=L_opem
C FDB70_logic.fgi(  81, 106):������,20FDB72AL101YU21
      L_(38)=I_erem.ne.0
C FDB70_logic.fgi(  52,  64):��������� 1->LO
      L_irem=(L_(38).or.L_irem).and..not.(L_(39))
      L_orem=.not.L_irem
C FDB70_logic.fgi(  62,  62):RS �������
      L_arem=L_irem
C FDB70_logic.fgi(  83,  64):������,20FDB72AE702YU22
      L_(44)=I_item.ne.0
C FDB70_logic.fgi( 123, 195):��������� 1->LO
      L_(42)=I_otem.ne.0
C FDB70_logic.fgi( 123, 200):��������� 1->LO
      L_(46)=I_exem.ne.0
C FDB70_logic.fgi( 123, 222):��������� 1->LO
      L_(49)=I_evem.ne.0
C FDB70_logic.fgi( 123, 206):��������� 1->LO
      L_(47)=I_ivem.ne.0
C FDB70_logic.fgi( 123, 211):��������� 1->LO
      L_(52)=I_axem.ne.0
C FDB70_logic.fgi( 123, 217):��������� 1->LO
      L_uxem=L_abim
C FDB70_logic.fgi(  84, 136):������,20FDB71AA001YA21
      !{
      Call ZATVOR_MAN(deltat,REAL(R_aram,4),R8_ulam,I_itam
     &,I_avam,I_etam,C8_imam,
     & I_utam,R_ipam,R_epam,R_ufam,REAL(R_ekam,4),
     & R_alam,REAL(R_ilam,4),R_ikam,
     & REAL(R_ukam,4),I_usam,I_evam,I_otam,I_osam,L_olam,L_ovam
     &,
     & L_oxam,L_elam,L_emam,L_amam,L_axam,
     & L_okam,L_akam,L_umam,L_ebem,L_opam,L_upam,
     & REAL(R8_ofam,8),REAL(1.0,4),R8_apam,L_(26),L_efam,L_
     &(25),L_uxem,
     & L_uvam,I_ivam,L_uxam,R_isam,REAL(R_omam,4),L_abem,L_ifam
     &,L_ibem,
     & L_atem,L_eram,L_iram,L_oram,L_asam,L_esam,L_uram)
      !}

      if(L_esam.or.L_asam.or.L_uram.or.L_oram.or.L_iram.or.L_eram
     &) then      
                  I_atam = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_atam = z'40000000'
      endif
C FDB70_vlv.fgi(  16, 181):���� ���������� ��������,20FDB71AA001
      L_(51) = L_(46).OR.L_atem
C FDB70_logic.fgi( 133, 221):���
      L_ixem=(L_(51).or.L_ixem).and..not.(L_(52))
      L_oxem=.not.L_ixem
C FDB70_logic.fgi( 139, 219):RS �������
      L_uvem=L_ixem
C FDB70_logic.fgi( 157, 221):������,20FDB71AM001YU21
      if(.not.L_atem) then
         R0_asem=0.0
      elseif(.not.L0_esem) then
         R0_asem=R0_urem
      else
         R0_asem=max(R_(67)-deltat,0.0)
      endif
      L_(41)=L_atem.and.R0_asem.le.0.0
      L0_esem=L_atem
C FDB70_logic.fgi( 135, 181):�������� ��������� ������
      L_(48) = L_(47).OR.L_(41)
C FDB70_logic.fgi( 133, 210):���
      L_ovem=(L_(48).or.L_ovem).and..not.(L_(49))
      L_(50)=.not.L_ovem
C FDB70_logic.fgi( 139, 208):RS �������
      L_avem=L_ovem
C FDB70_logic.fgi( 157, 210):������,20FDB71AJ002YA11
      if(.not.L_atem) then
         R0_osem=0.0
      elseif(.not.L0_usem) then
         R0_osem=R0_isem
      else
         R0_osem=max(R_(68)-deltat,0.0)
      endif
      L_(40)=L_atem.and.R0_osem.le.0.0
      L0_usem=L_atem
C FDB70_logic.fgi( 135, 188):�������� ��������� ������
      L_(43) = L_(42).OR.L_(40)
C FDB70_logic.fgi( 133, 199):���
      L_utem=(L_(43).or.L_utem).and..not.(L_(44))
      L_(45)=.not.L_utem
C FDB70_logic.fgi( 139, 197):RS �������
      L_etem=L_utem
C FDB70_logic.fgi( 157, 199):������,20FDB71CM001YA11
      L_(53)=I_ibim.ne.0
C FDB70_logic.fgi(  56, 151):��������� 1->LO
      L_obim=(L_(53).or.L_obim).and..not.(L_(53))
      L_ubim=.not.L_obim
C FDB70_logic.fgi(  66, 149):RS �������
      L_ebim=L_obim
C FDB70_logic.fgi(  84, 151):������,20FDB74AE403YU21
      L_(59)=I_adim.ne.0
C FDB70_logic.fgi(  47, 163):��������� 1->LO
      L_(58)=I_edim.ne.0
C FDB70_logic.fgi(  47, 172):��������� 1->LO
      L_(60)=I_ofim.ne.0
C FDB70_logic.fgi(  47, 181):��������� 1->LO
      L_(64) = L_(60).OR.L_(59).OR.L_(58)
C FDB70_logic.fgi(  57, 186):���
      L_(63)=I_akim.ne.0
C FDB70_logic.fgi(  47, 190):��������� 1->LO
      L_ekim=(L_(63).or.L_ekim).and..not.(L_(64))
      L_(65)=.not.L_ekim
C FDB70_logic.fgi(  63, 188):RS �������
      L_ifim=L_ekim
C FDB70_logic.fgi(  85, 190):������,20FDB71AE801YU21
      L_(61) = L_(63).OR.L_(59).OR.L_(58)
C FDB70_logic.fgi(  57, 177):���
      L_ufim=(L_(60).or.L_ufim).and..not.(L_(61))
      L_(62)=.not.L_ufim
C FDB70_logic.fgi(  63, 179):RS �������
      L_efim=L_ufim
C FDB70_logic.fgi(  85, 181):������,20FDB71AE801YU24
      L_(56) = L_(63).OR.L_(60).OR.L_(59)
C FDB70_logic.fgi(  57, 168):���
      L_afim=(L_(58).or.L_afim).and..not.(L_(56))
      L_(57)=.not.L_afim
C FDB70_logic.fgi(  63, 170):RS �������
      L_udim=L_afim
C FDB70_logic.fgi(  85, 172):������,20FDB71AE801YU22
      L_(54) = L_(63).OR.L_(60).OR.L_(58)
C FDB70_logic.fgi(  57, 159):���
      L_odim=(L_(59).or.L_odim).and..not.(L_(54))
      L_(55)=.not.L_odim
C FDB70_logic.fgi(  63, 161):RS �������
      L_idim=L_odim
C FDB70_logic.fgi(  85, 163):������,20FDB71AE801YU23
      L_(66)=I_ikim.ne.0
C FDB70_logic.fgi(  45, 203):��������� 1->LO
      L_(69)=I_okim.ne.0
C FDB70_logic.fgi(  45, 212):��������� 1->LO
      L_(73) = L_(69).OR.L_(66)
C FDB70_logic.fgi(  54, 217):���
      L_(72)=I_ukim.ne.0
C FDB70_logic.fgi(  45, 221):��������� 1->LO
      L_elim=(L_(72).or.L_elim).and..not.(L_(73))
      L_(74)=.not.L_elim
C FDB70_logic.fgi(  61, 219):RS �������
      L_alim=L_elim
C FDB70_logic.fgi(  82, 221):������,20FDB71AE202YU21
      L_(67) = L_(69).OR.L_(72)
C FDB70_logic.fgi(  54, 199):���
      L_amim=(L_(66).or.L_amim).and..not.(L_(67))
      L_(68)=.not.L_amim
C FDB70_logic.fgi(  61, 201):RS �������
      L_ulim=L_amim
C FDB70_logic.fgi(  82, 203):������,20FDB71AE202YU22
      L_(70) = L_(72).OR.L_(66)
C FDB70_logic.fgi(  54, 208):���
      L_olim=(L_(69).or.L_olim).and..not.(L_(70))
      L_(71)=.not.L_olim
C FDB70_logic.fgi(  61, 210):RS �������
      L_ilim=L_olim
C FDB70_logic.fgi(  82, 212):������,20FDB71AE202YU23
      R_(69) = 20.0
C FDB70_logic.fgi(  43, 263):��������� (RE4) (�������)
      Call PUMP_HANDLER(deltat,C30_abal,I_udal,L_uxuk,L_ibal
     &,L_emal,
     & I_afal,I_odal,R_ovuk,REAL(R_axuk,4),
     & R_avuk,REAL(R_ivuk,4),I_efal,L_ulal,L_ukal,L_upal,
     & L_aral,L_adal,L_akal,L_ikal,L_ekal,L_okal,L_ilal,L_utuk
     &,
     & L_uvuk,L_otuk,L_evuk,L_epal,L_(24),
     & L_ipal,L_(23),L_ituk,L_etuk,L_edal,I_ifal,R_imal,R_omal
     &,
     & L_oral,L_olal,L_opal,REAL(R8_ofam,8),L_elal,
     & REAL(R8_alal,8),R_eral,REAL(R_ofal,4),R_ufal,REAL(R8_ubal
     &,8),R_obal,
     & R8_apam,R_iral,R8_alal,REAL(R_exuk,4),REAL(R_ixuk,4
     &))
C FDB70_vlv.fgi(  37, 182):���������� ���������� �������,20FDB71CU001KN01
C label 445  try445=try445-1
C sav1=R_iral
C sav2=R_eral
C sav3=L_ulal
C sav4=L_ilal
C sav5=L_ukal
C sav6=R8_alal
C sav7=R_ufal
C sav8=I_ifal
C sav9=I_efal
C sav10=I_afal
C sav11=I_udal
C sav12=I_odal
C sav13=I_idal
C sav14=L_edal
C sav15=L_adal
C sav16=R_obal
C sav17=C30_abal
C sav18=L_uvuk
C sav19=L_evuk
      Call PUMP_HANDLER(deltat,C30_abal,I_udal,L_uxuk,L_ibal
     &,L_emal,
     & I_afal,I_odal,R_ovuk,REAL(R_axuk,4),
     & R_avuk,REAL(R_ivuk,4),I_efal,L_ulal,L_ukal,L_upal,
     & L_aral,L_adal,L_akal,L_ikal,L_ekal,L_okal,L_ilal,L_utuk
     &,
     & L_uvuk,L_otuk,L_evuk,L_epal,L_(24),
     & L_ipal,L_(23),L_ituk,L_etuk,L_edal,I_ifal,R_imal,R_omal
     &,
     & L_oral,L_olal,L_opal,REAL(R8_ofam,8),L_elal,
     & REAL(R8_alal,8),R_eral,REAL(R_ofal,4),R_ufal,REAL(R8_ubal
     &,8),R_obal,
     & R8_apam,R_iral,R8_alal,REAL(R_exuk,4),REAL(R_ixuk,4
     &))
C FDB70_vlv.fgi(  37, 182):recalc:���������� ���������� �������,20FDB71CU001KN01
C if(sav1.ne.R_iral .and. try445.gt.0) goto 445
C if(sav2.ne.R_eral .and. try445.gt.0) goto 445
C if(sav3.ne.L_ulal .and. try445.gt.0) goto 445
C if(sav4.ne.L_ilal .and. try445.gt.0) goto 445
C if(sav5.ne.L_ukal .and. try445.gt.0) goto 445
C if(sav6.ne.R8_alal .and. try445.gt.0) goto 445
C if(sav7.ne.R_ufal .and. try445.gt.0) goto 445
C if(sav8.ne.I_ifal .and. try445.gt.0) goto 445
C if(sav9.ne.I_efal .and. try445.gt.0) goto 445
C if(sav10.ne.I_afal .and. try445.gt.0) goto 445
C if(sav11.ne.I_udal .and. try445.gt.0) goto 445
C if(sav12.ne.I_odal .and. try445.gt.0) goto 445
C if(sav13.ne.I_idal .and. try445.gt.0) goto 445
C if(sav14.ne.L_edal .and. try445.gt.0) goto 445
C if(sav15.ne.L_adal .and. try445.gt.0) goto 445
C if(sav16.ne.R_obal .and. try445.gt.0) goto 445
C if(sav17.ne.C30_abal .and. try445.gt.0) goto 445
C if(sav18.ne.L_uvuk .and. try445.gt.0) goto 445
C if(sav19.ne.L_evuk .and. try445.gt.0) goto 445
      if(L_ilal) then
         R_(43)=R_(44)
      else
         R_(43)=R_(45)
      endif
C FDB70_vent_log.fgi(  49, 169):���� RE IN LO CH7
      R8_ele=(R0_uke*R_(42)+deltat*R_(43))/(R0_uke+deltat
     &)
C FDB70_vent_log.fgi(  61, 170):�������������� �����  
      R8_ale=R8_ele
C FDB70_vent_log.fgi(  78, 170):������,F_FDB71CU001_G
      Call PUMP_HANDLER(deltat,C30_upik,I_isik,L_uxuk,L_ibal
     &,L_emal,
     & I_osik,I_esik,R_omik,REAL(R_apik,4),
     & R_amik,REAL(R_imik,4),I_usik,L_exik,L_ivik,L_adok,
     & L_edok,L_orik,L_otik,L_avik,L_utik,L_evik,L_axik,L_ulik
     &,
     & L_umik,L_olik,L_emik,L_ibok,L_(22),
     & L_obok,L_(21),L_ilik,L_elik,L_urik,I_atik,R_oxik,R_uxik
     &,
     & L_udok,L_olal,L_ubok,REAL(R8_ofam,8),L_uvik,
     & REAL(R8_ovik,8),R_idok,REAL(R_etik,4),R_itik,REAL(R8_irik
     &,8),R_erik,
     & R8_apam,R_odok,R8_ovik,REAL(R_epik,4),REAL(R_ipik,4
     &))
C FDB70_vlv.fgi(  58, 182):���������� ���������� �������,20FDB72CU001KN01
C label 453  try453=try453-1
C sav1=R_odok
C sav2=R_idok
C sav3=L_exik
C sav4=L_axik
C sav5=L_ivik
C sav6=R8_ovik
C sav7=R_itik
C sav8=I_atik
C sav9=I_usik
C sav10=I_osik
C sav11=I_isik
C sav12=I_esik
C sav13=I_asik
C sav14=L_urik
C sav15=L_orik
C sav16=R_erik
C sav17=C30_upik
C sav18=L_umik
C sav19=L_emik
      Call PUMP_HANDLER(deltat,C30_upik,I_isik,L_uxuk,L_ibal
     &,L_emal,
     & I_osik,I_esik,R_omik,REAL(R_apik,4),
     & R_amik,REAL(R_imik,4),I_usik,L_exik,L_ivik,L_adok,
     & L_edok,L_orik,L_otik,L_avik,L_utik,L_evik,L_axik,L_ulik
     &,
     & L_umik,L_olik,L_emik,L_ibok,L_(22),
     & L_obok,L_(21),L_ilik,L_elik,L_urik,I_atik,R_oxik,R_uxik
     &,
     & L_udok,L_olal,L_ubok,REAL(R8_ofam,8),L_uvik,
     & REAL(R8_ovik,8),R_idok,REAL(R_etik,4),R_itik,REAL(R8_irik
     &,8),R_erik,
     & R8_apam,R_odok,R8_ovik,REAL(R_epik,4),REAL(R_ipik,4
     &))
C FDB70_vlv.fgi(  58, 182):recalc:���������� ���������� �������,20FDB72CU001KN01
C if(sav1.ne.R_odok .and. try453.gt.0) goto 453
C if(sav2.ne.R_idok .and. try453.gt.0) goto 453
C if(sav3.ne.L_exik .and. try453.gt.0) goto 453
C if(sav4.ne.L_axik .and. try453.gt.0) goto 453
C if(sav5.ne.L_ivik .and. try453.gt.0) goto 453
C if(sav6.ne.R8_ovik .and. try453.gt.0) goto 453
C if(sav7.ne.R_itik .and. try453.gt.0) goto 453
C if(sav8.ne.I_atik .and. try453.gt.0) goto 453
C if(sav9.ne.I_usik .and. try453.gt.0) goto 453
C if(sav10.ne.I_osik .and. try453.gt.0) goto 453
C if(sav11.ne.I_isik .and. try453.gt.0) goto 453
C if(sav12.ne.I_esik .and. try453.gt.0) goto 453
C if(sav13.ne.I_asik .and. try453.gt.0) goto 453
C if(sav14.ne.L_urik .and. try453.gt.0) goto 453
C if(sav15.ne.L_orik .and. try453.gt.0) goto 453
C if(sav16.ne.R_erik .and. try453.gt.0) goto 453
C if(sav17.ne.C30_upik .and. try453.gt.0) goto 453
C if(sav18.ne.L_umik .and. try453.gt.0) goto 453
C if(sav19.ne.L_emik .and. try453.gt.0) goto 453
      if(L_axik) then
         R_(39)=R_(40)
      else
         R_(39)=R_(41)
      endif
C FDB70_vent_log.fgi( 127, 169):���� RE IN LO CH7
      R8_eke=(R0_ufe*R_(38)+deltat*R_(39))/(R0_ufe+deltat
     &)
C FDB70_vent_log.fgi( 137, 170):�������������� �����  
      R8_ake=R8_eke
C FDB70_vent_log.fgi( 156, 170):������,F_FDB72CU001_G
      Call PUMP_HANDLER(deltat,C30_imaf,I_araf,L_uxuk,L_ibal
     &,L_emal,
     & I_eraf,I_upaf,R_elaf,REAL(R_olaf,4),
     & R_okaf,REAL(R_alaf,4),I_iraf,L_utaf,L_ataf,L_oxaf,
     & L_uxaf,L_epaf,L_esaf,L_osaf,L_isaf,L_usaf,L_otaf,L_ikaf
     &,
     & L_ilaf,L_ekaf,L_ukaf,L_axaf,L_(20),
     & L_exaf,L_(19),L_akaf,L_ufaf,L_ipaf,I_oraf,R_evaf,R_ivaf
     &,
     & L_ibef,L_olal,L_ixaf,REAL(R8_ofam,8),L_itaf,
     & REAL(R8_etaf,8),R_abef,REAL(R_uraf,4),R_asaf,REAL(R8_apaf
     &,8),R_umaf,
     & R8_apam,R_ebef,R8_etaf,REAL(R_ulaf,4),REAL(R_amaf,4
     &))
C FDB70_vlv.fgi(  77, 182):���������� ���������� �������,20FDB73CU001KN01
C label 461  try461=try461-1
C sav1=R_ebef
C sav2=R_abef
C sav3=L_utaf
C sav4=L_otaf
C sav5=L_ataf
C sav6=R8_etaf
C sav7=R_asaf
C sav8=I_oraf
C sav9=I_iraf
C sav10=I_eraf
C sav11=I_araf
C sav12=I_upaf
C sav13=I_opaf
C sav14=L_ipaf
C sav15=L_epaf
C sav16=R_umaf
C sav17=C30_imaf
C sav18=L_ilaf
C sav19=L_ukaf
      Call PUMP_HANDLER(deltat,C30_imaf,I_araf,L_uxuk,L_ibal
     &,L_emal,
     & I_eraf,I_upaf,R_elaf,REAL(R_olaf,4),
     & R_okaf,REAL(R_alaf,4),I_iraf,L_utaf,L_ataf,L_oxaf,
     & L_uxaf,L_epaf,L_esaf,L_osaf,L_isaf,L_usaf,L_otaf,L_ikaf
     &,
     & L_ilaf,L_ekaf,L_ukaf,L_axaf,L_(20),
     & L_exaf,L_(19),L_akaf,L_ufaf,L_ipaf,I_oraf,R_evaf,R_ivaf
     &,
     & L_ibef,L_olal,L_ixaf,REAL(R8_ofam,8),L_itaf,
     & REAL(R8_etaf,8),R_abef,REAL(R_uraf,4),R_asaf,REAL(R8_apaf
     &,8),R_umaf,
     & R8_apam,R_ebef,R8_etaf,REAL(R_ulaf,4),REAL(R_amaf,4
     &))
C FDB70_vlv.fgi(  77, 182):recalc:���������� ���������� �������,20FDB73CU001KN01
C if(sav1.ne.R_ebef .and. try461.gt.0) goto 461
C if(sav2.ne.R_abef .and. try461.gt.0) goto 461
C if(sav3.ne.L_utaf .and. try461.gt.0) goto 461
C if(sav4.ne.L_otaf .and. try461.gt.0) goto 461
C if(sav5.ne.L_ataf .and. try461.gt.0) goto 461
C if(sav6.ne.R8_etaf .and. try461.gt.0) goto 461
C if(sav7.ne.R_asaf .and. try461.gt.0) goto 461
C if(sav8.ne.I_oraf .and. try461.gt.0) goto 461
C if(sav9.ne.I_iraf .and. try461.gt.0) goto 461
C if(sav10.ne.I_eraf .and. try461.gt.0) goto 461
C if(sav11.ne.I_araf .and. try461.gt.0) goto 461
C if(sav12.ne.I_upaf .and. try461.gt.0) goto 461
C if(sav13.ne.I_opaf .and. try461.gt.0) goto 461
C if(sav14.ne.L_ipaf .and. try461.gt.0) goto 461
C if(sav15.ne.L_epaf .and. try461.gt.0) goto 461
C if(sav16.ne.R_umaf .and. try461.gt.0) goto 461
C if(sav17.ne.C30_imaf .and. try461.gt.0) goto 461
C if(sav18.ne.L_ilaf .and. try461.gt.0) goto 461
C if(sav19.ne.L_ukaf .and. try461.gt.0) goto 461
      if(L_otaf) then
         R_(35)=R_(36)
      else
         R_(35)=R_(37)
      endif
C FDB70_vent_log.fgi( 204, 169):���� RE IN LO CH7
      R8_efe=(R0_ude*R_(34)+deltat*R_(35))/(R0_ude+deltat
     &)
C FDB70_vent_log.fgi( 214, 170):�������������� �����  
      R8_afe=R8_efe
C FDB70_vent_log.fgi( 233, 170):������,F_FDB73CU001_G
      R_apim = R_apim + R_(69)
C FDB70_logic.fgi(  46, 265):��������
C label 469  try469=try469-1
C sav1=R_apim
      R_apim = R_apim + R_(69)
C FDB70_logic.fgi(  46, 265):recalc:��������
C if(sav1.ne.R_apim .and. try469.gt.0) goto 469
      End
