      Subroutine FDB30(ext_deltat)
      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      Include 'FDB30.fh'

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R_(13)=R8_ube
C FDB30_vent_log.fgi( 122, 267):pre: �������������� �����  
      R_(28)=R_okuf
C FDB30_logic.fgi(  67,  54):pre: �������������� �����  
      R_(17)=R8_ude
C FDB30_vent_log.fgi(  49, 267):pre: �������������� �����  
      R_(22)=R_ovof
C FDB30_logic.fgi( 162, 104):pre: �������������� �����  
      R_(25)=R_aduf
C FDB30_logic.fgi(  71, 104):pre: �������������� �����  
      R_oke = R8_e
C FDB30_vent_log.fgi(  48, 219):��������
      !{
      Call DAT_ANA_PATM_HANDLER(deltat,REAL(R_eme,4),REAL
     &(R_oke,4),R_ofe,
     & R_ime,REAL(1,4),REAL(R_eke,4),
     & REAL(R_ike,4),REAL(R_atof,4),
     & REAL(R_ife,4),I_ame,REAL(R_uke,4),L_ale,
     & REAL(R_ele,4),L_ile,L_ole,R_etof,REAL(R_ake,4),REAL
     &(R_ufe,4),L_ule)
      !}
C FDB30_vlv.fgi(  58, 152):���������� ������� ��������,20FDB30CP001XQ01
      L_usof=R_etof.gt.R_atof
C FDB30_logic.fgi( 174, 148):���������� >
      L_ufuf=R_etof.lt.R_atof
C FDB30_logic.fgi( 174, 142):���������� <
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_odef,4),R8_oxaf,I_alef
     &,I_olef,I_ukef,
     & C8_ebef,I_ilef,R_adef,R_ubef,R_otaf,
     & REAL(R_avaf,4),R_uvaf,REAL(R_exaf,4),
     & R_evaf,REAL(R_ovaf,4),I_ikef,I_ulef,I_elef,I_ekef,L_ixaf
     &,
     & L_emef,L_apef,L_axaf,L_ivaf,
     & L_abef,L_uxaf,L_imef,L_utaf,L_obef,L_opef,L_edef,
     & L_idef,REAL(R8_uref,8),REAL(1.0,4),R8_exef,L_ufuf,L_usof
     &,L_ukuf,
     & I_amef,L_epef,R_akef,REAL(R_ibef,4),L_ipef,L_etaf,L_upef
     &,L_itaf,
     & L_udef,L_afef,L_efef,L_ofef,L_ufef,L_ifef)
      !}

      if(L_ufef.or.L_ofef.or.L_ifef.or.L_efef.or.L_afef.or.L_udef
     &) then      
                  I_okef = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_okef = z'40000000'
      endif
C FDB30_vlv.fgi( 165, 120):���� ���������� �������� ��������,20FDB30AA100
      L_(39) = (.NOT.L_ukuf).AND.(.NOT.L_ufuf)
C FDB30_logic.fgi(  52,  43):�
      Call REG_RAS_MAN(deltat,.TRUE.,L_om,REAL(R_im,4),REAL
     &(R_of,4),R_ap,
     & REAL(R_ud,4),R_um,R_op,R_ip,R_af,REAL(R_if,4),
     & R_o,REAL(R_ad,4),R_ed,REAL(R_od,4),L_ef,
     & R_ar,R_er,L_ep,L_u,L_id,I_ol,R_ir,R_il,
     & R_up,R8_el,REAL(100000,4),REAL(R_etof,4),REAL(R_uf
     &,4),
     & REAL(R_em,4),REAL(R_am,4),REAL(R_ul,4))
C FDB30_vent_log.fgi(  38, 133):���������� ���������� �������,20FDB30DF001
      R_(1) = 1
C FDB30_vent_log.fgi(  90, 132):��������� (RE4) (�������)
      R_ire = R8_i * R_(1)
C FDB30_vent_log.fgi(  93, 134):����������
      !{
      Call DAT_ANA_HANDLER(deltat,R_epe,R_ate,REAL(1,4),
     & REAL(R_upe,4),REAL(R_are,4),
     & REAL(R_ape,4),REAL(R_ume,4),I_use,
     & REAL(R_ore,4),L_ure,REAL(R_ase,4),L_ese,L_ise,R_ere
     &,
     & REAL(R_ope,4),REAL(R_ipe,4),L_ose,REAL(R_ire,4))
      !}
C FDB30_vlv.fgi(  81,  45):���������� �������,20FDB30DF001ZQ01
      R_(2) = 1.0
C FDB30_vent_log.fgi( 219, 266):��������� (RE4) (�������)
      R8_or = R_(2) + (-R8_ur)
C FDB30_vent_log.fgi( 222, 266):��������
      !��������� R_(9) = FDB30_vent_logC?? /4000/
      R_(9)=R0_as
C FDB30_vent_log.fgi( 129, 206):���������
      !��������� R_(11) = FDB30_vent_logC?? /4000/
      R_(11)=R0_es
C FDB30_vent_log.fgi( 129, 216):���������
      R_obuf = R8_is
C FDB30_vent_log.fgi( 220, 194):��������
      R_evof = R8_os
C FDB30_vent_log.fgi( 220, 213):��������
      R_uvu = R8_us
C FDB30_vent_log.fgi( 220, 204):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_otu,R_obad,REAL(1,4
     &),
     & REAL(R_uvu,4),I_ibad,REAL(R_evu,4),
     & REAL(R_ivu,4),REAL(R_itu,4),
     & REAL(R_etu,4),REAL(R_exu,4),L_ixu,REAL(R_oxu,4),L_uxu
     &,
     & L_abad,R_ovu,REAL(R_avu,4),REAL(R_utu,4),L_ebad)
      !}
C FDB30_vlv.fgi(  54,  79):���������� ������� ��� 2,20FDB30CQ001XQ01
      R_atad = R8_at
C FDB30_vent_log.fgi( 220, 219):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_urad,R_uvad,REAL(1,4
     &),
     & REAL(R_atad,4),I_ovad,REAL(R_isad,4),
     & REAL(R_osad,4),REAL(R_orad,4),
     & REAL(R_irad,4),REAL(R_itad,4),L_otad,REAL(R_utad,4
     &),L_avad,
     & L_evad,R_usad,REAL(R_esad,4),REAL(R_asad,4),L_ivad
     &)
      !}
C FDB30_vlv.fgi(  27,  94):���������� ������� ��� 2,20FDB30CM001XQ01
      R_elu = R8_et + (-R8_ut)
C FDB30_vent_log.fgi(  43, 205):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_aku,R_apu,REAL(0.000001
     &,4),
     & REAL(R_elu,4),I_umu,REAL(R_oku,4),
     & REAL(R_uku,4),REAL(R_ufu,4),
     & REAL(R_ofu,4),REAL(R_olu,4),L_ulu,REAL(R_amu,4),L_emu
     &,
     & L_imu,R_alu,REAL(R_iku,4),REAL(R_eku,4),L_omu)
      !}
C FDB30_vlv.fgi(  81,  79):���������� ������� ��� 2,20FDB30CP002XQ01
      R_obu = R8_it + (-R8_ut)
C FDB30_vent_log.fgi(  43, 185):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ixo,R_ifu,REAL(0.001
     &,4),
     & REAL(R_obu,4),I_efu,REAL(R_abu,4),
     & REAL(R_ebu,4),REAL(R_exo,4),
     & REAL(R_axo,4),REAL(R_adu,4),L_edu,REAL(R_idu,4),L_odu
     &,
     & L_udu,R_ibu,REAL(R_uxo,4),REAL(R_oxo,4),L_afu)
      !}
C FDB30_vlv.fgi(  81,  94):���������� ������� ��� 2,20FDB30CP004XQ01
      R_ifad = R8_ot + (-R8_ut)
C FDB30_vent_log.fgi(  43, 195):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_edad,R_elad,REAL(0.001
     &,4),
     & REAL(R_ifad,4),I_alad,REAL(R_udad,4),
     & REAL(R_afad,4),REAL(R_adad,4),
     & REAL(R_ubad,4),REAL(R_ufad,4),L_akad,REAL(R_ekad,4
     &),L_ikad,
     & L_okad,R_efad,REAL(R_odad,4),REAL(R_idad,4),L_ukad
     &)
      !}
C FDB30_vlv.fgi(  27,  79):���������� ������� ��� 2,20FDB30CP003XQ01
      R_(3) = 60000
C FDB30_vent_log.fgi( 129, 168):��������� (RE4) (�������)
      if(R8_av.ge.0.0) then
         R_(4)=R8_ev/max(R8_av,1.0e-10)
      else
         R_(4)=R8_ev/min(R8_av,-1.0e-10)
      endif
C FDB30_vent_log.fgi( 119, 171):�������� ����������
      R_ato = R_(4) * R_(3)
C FDB30_vent_log.fgi( 132, 170):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_uro,R_uvo,REAL(1,4)
     &,
     & REAL(R_ato,4),I_ovo,REAL(R_iso,4),
     & REAL(R_oso,4),REAL(R_oro,4),
     & REAL(R_iro,4),REAL(R_ito,4),L_oto,REAL(R_uto,4),L_avo
     &,
     & L_evo,R_uso,REAL(R_eso,4),REAL(R_aso,4),L_ivo)
      !}
C FDB30_vlv.fgi(  81, 124):���������� ������� ��� 2,20FDB30CF005XQ01
      R_(5) = 60000
C FDB30_vent_log.fgi( 129, 179):��������� (RE4) (�������)
      if(R8_iv.ge.0.0) then
         R_(6)=R8_ov/max(R8_iv,1.0e-10)
      else
         R_(6)=R8_ov/min(R8_iv,-1.0e-10)
      endif
C FDB30_vent_log.fgi( 119, 182):�������� ����������
      R_imo = R_(6) * R_(5)
C FDB30_vent_log.fgi( 132, 181):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_elo,R_ero,REAL(1,4)
     &,
     & REAL(R_imo,4),I_aro,REAL(R_ulo,4),
     & REAL(R_amo,4),REAL(R_alo,4),
     & REAL(R_uko,4),REAL(R_umo,4),L_apo,REAL(R_epo,4),L_ipo
     &,
     & L_opo,R_emo,REAL(R_olo,4),REAL(R_ilo,4),L_upo)
      !}
C FDB30_vlv.fgi(  81, 109):���������� ������� ��� 2,20FDB30CF004XQ01
      R_(7) = 60000
C FDB30_vent_log.fgi( 129, 191):��������� (RE4) (�������)
      if(R8_uv.ge.0.0) then
         R_(8)=R8_ax/max(R8_uv,1.0e-10)
      else
         R_(8)=R8_ax/min(R8_uv,-1.0e-10)
      endif
C FDB30_vent_log.fgi( 119, 194):�������� ����������
      R_eled = R_(8) * R_(7)
C FDB30_vent_log.fgi( 132, 193):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_aked,R_aped,REAL(1,4
     &),
     & REAL(R_eled,4),I_umed,REAL(R_oked,4),
     & REAL(R_uked,4),REAL(R_ufed,4),
     & REAL(R_ofed,4),REAL(R_oled,4),L_uled,REAL(R_amed,4
     &),L_emed,
     & L_imed,R_aled,REAL(R_iked,4),REAL(R_eked,4),L_omed
     &)
      !}
C FDB30_vlv.fgi(  27, 109):���������� ������� ��� 2,20FDB30CF003XQ01
      if(R8_ex.ge.0.0) then
         R_(10)=R8_ix/max(R8_ex,1.0e-10)
      else
         R_(10)=R8_ix/min(R8_ex,-1.0e-10)
      endif
C FDB30_vent_log.fgi( 119, 208):�������� ����������
      R_ured = R_(10) * R_(9)
C FDB30_vent_log.fgi( 132, 207):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_oped,R_oted,REAL(1,4
     &),
     & REAL(R_ured,4),I_ited,REAL(R_ered,4),
     & REAL(R_ired,4),REAL(R_iped,4),
     & REAL(R_eped,4),REAL(R_esed,4),L_ised,REAL(R_osed,4
     &),L_used,
     & L_ated,R_ored,REAL(R_ared,4),REAL(R_uped,4),L_eted
     &)
      !}
C FDB30_vlv.fgi(  54, 124):���������� ������� ��� 2,20FDB30CF002XQ01
      if(R8_ox.ge.0.0) then
         R_(12)=R8_ux/max(R8_ox,1.0e-10)
      else
         R_(12)=R8_ux/min(R8_ox,-1.0e-10)
      endif
C FDB30_vent_log.fgi( 119, 218):�������� ����������
      R_obed = R_(12) * R_(11)
C FDB30_vent_log.fgi( 132, 217):����������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ixad,R_ifed,REAL(1,4
     &),
     & REAL(R_obed,4),I_efed,REAL(R_abed,4),
     & REAL(R_ebed,4),REAL(R_exad,4),
     & REAL(R_axad,4),REAL(R_aded,4),L_eded,REAL(R_ided,4
     &),L_oded,
     & L_uded,R_ibed,REAL(R_uxad,4),REAL(R_oxad,4),L_afed
     &)
      !}
C FDB30_vlv.fgi(  54, 109):���������� ������� ��� 2,20FDB30CF001XQ01
      R_ixed = R8_abe
C FDB30_vent_log.fgi( 321, 208):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_eved,R_edid,REAL(1,4
     &),
     & REAL(R_ixed,4),I_adid,REAL(R_uved,4),
     & REAL(R_axed,4),REAL(R_aved,4),
     & REAL(R_uted,4),REAL(R_uxed,4),L_abid,REAL(R_ebid,4
     &),L_ibid,
     & L_obid,R_exed,REAL(R_oved,4),REAL(R_ived,4),L_ubid
     &)
      !}
C FDB30_vlv.fgi(  27, 124):���������� ������� ��� 2,20FDB30CP100XQ01
      R_akid = R8_ebe
C FDB30_vent_log.fgi( 321, 217):��������
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_udid,R_ulid,REAL(1,4
     &),
     & REAL(R_akid,4),I_olid,REAL(R_ifid,4),
     & REAL(R_ofid,4),REAL(R_odid,4),
     & REAL(R_idid,4),REAL(R_ikid,4),L_okid,REAL(R_ukid,4
     &),L_alid,
     & L_elid,R_ufid,REAL(R_efid,4),REAL(R_afid,4),L_ilid
     &)
      !}
C FDB30_vlv.fgi(  27, 138):���������� ������� ��� 2,20FDB30CT001XQ01
      !��������� R_(16) = FDB30_vent_logC?? /0.0/
      R_(16)=R0_ade
C FDB30_vent_log.fgi( 107, 268):���������
      !��������� R_(15) = FDB30_vent_logC?? /0.001/
      R_(15)=R0_ede
C FDB30_vent_log.fgi( 107, 266):���������
      !��������� R_(20) = FDB30_vent_logC?? /0.0/
      R_(20)=R0_afe
C FDB30_vent_log.fgi(  34, 268):���������
      !��������� R_(19) = FDB30_vent_logC?? /0.001/
      R_(19)=R0_efe
C FDB30_vent_log.fgi(  34, 266):���������
      !{
      Call REG_MAN(deltat,REAL(R_orod,4),R8_omod,R_exod,R_ivod
     &,
     & L_obud,R_ixod,R_ovod,L_ubud,R_upod,R_opod,
     & R_okod,REAL(R_alod,4),R_ulod,
     & REAL(R_emod,4),R_elod,REAL(R_olod,4),I_etod,
     & L_imod,L_utod,L_oxod,L_amod,L_ilod,
     & L_apod,L_umod,L_evod,L_ukod,L_ipod,L_ebud,L_erod,
     & L_irod,REAL(R8_uref,8),REAL(1.0,4),R8_exef,L_ufod,L_akod
     &,L_avod,
     & I_otod,L_uxod,R_atod,REAL(R_epod,4),L_ekod,L_ikod,L_urod
     &,L_asod,
     & L_esod,L_osod,L_usod,L_isod)
      !}

      if(L_usod.or.L_osod.or.L_isod.or.L_esod.or.L_asod.or.L_urod
     &) then      
                  I_itod = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_itod = z'40000000'
      endif


      R_arod=100*R8_omod
C FDB30_vlv.fgi( 179, 120):���� ���������� ������������ ��������,20FDB30AA200
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_okaf,4),R8_odaf,I_apaf
     &,I_opaf,I_umaf,
     & C8_efaf,I_ipaf,R_akaf,R_ufaf,R_oxud,
     & REAL(R_abaf,4),R_ubaf,REAL(R_edaf,4),
     & R_ebaf,REAL(R_obaf,4),I_imaf,I_upaf,I_epaf,I_emaf,L_idaf
     &,
     & L_eraf,L_esaf,L_adaf,L_ibaf,
     & L_afaf,L_udaf,L_oraf,L_uxud,L_ofaf,L_usaf,L_ekaf,
     & L_ikaf,REAL(R8_uref,8),REAL(1.0,4),R8_exef,L_uvud,L_axud
     &,L_iraf,
     & I_araf,L_isaf,R_amaf,REAL(R_ifaf,4),L_osaf,L_exud,L_ataf
     &,L_ixud,
     & L_ukaf,L_alaf,L_elaf,L_olaf,L_ulaf,L_ilaf)
      !}

      if(L_ulaf.or.L_olaf.or.L_ilaf.or.L_elaf.or.L_alaf.or.L_ukaf
     &) then      
                  I_omaf = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_omaf = z'40000000'
      endif
C FDB30_vlv.fgi( 151,  96):���� ���������� �������� ��������,20FDB30AA102
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_ebif,4),R8_avef,I_ofif
     &,I_ekif,I_ifif,
     & C8_ovef,I_akif,R_oxef,R_ixef,R_asef,
     & REAL(R_isef,4),R_etef,REAL(R_otef,4),
     & R_osef,REAL(R_atef,4),I_afif,I_ikif,I_ufif,I_udif,L_utef
     &,
     & L_ukif,L_ulif,L_itef,L_usef,
     & L_ivef,L_evef,L_elif,L_esef,L_axef,L_imif,L_uxef,
     & L_abif,REAL(R8_uref,8),REAL(1.0,4),R8_exef,L_aref,L_eref
     &,L_alif,
     & I_okif,L_amif,R_odif,REAL(R_uvef,4),L_emif,L_iref,L_omif
     &,L_oref,
     & L_ibif,L_obif,L_ubif,L_edif,L_idif,L_adif)
      !}

      if(L_idif.or.L_edif.or.L_adif.or.L_ubif.or.L_obif.or.L_ibif
     &) then      
                  I_efif = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_efif = z'40000000'
      endif
C FDB30_vlv.fgi( 151, 120):���� ���������� �������� ��������,20FDB30AA101
      !{
      Call BOX_HANDLER(deltat,L_erif,
     & R_arif,REAL(R_irif,4),
     & L_asif,R_urif,
     & REAL(R_esif,4),L_ipif,R_epif,
     & REAL(R_opif,4),L_isif,I_umif,L_orif,L_upif,
     & I_apif)
      !}
C FDB30_vlv.fgi( 207, 160):���������� �����,20FDB30_SHLUZ_UST_PRESS
      !{
      Call BOX_HANDLER(deltat,L_avif,
     & R_utif,REAL(R_evif,4),
     & L_uvif,R_ovif,
     & REAL(R_axif,4),L_etif,R_atif,
     & REAL(R_itif,4),L_exif,I_osif,L_ivif,L_otif,
     & I_usif)
      !}
C FDB30_vlv.fgi( 178, 160):���������� �����,20FDB30_BOX_ZAGR_LOD
      !{
      Call BOX_HANDLER(deltat,L_ubof,
     & R_obof,REAL(R_adof,4),
     & L_odof,R_idof,
     & REAL(R_udof,4),L_abof,R_uxif,
     & REAL(R_ebof,4),L_afof,I_ixif,L_edof,L_ibof,
     & I_oxif)
      !}
C FDB30_vlv.fgi( 149, 160):���������� �����,20FDB30_BOX_PRESS_INSTR
      I_(1) = z'01000003'
C FDB30_logic.fgi( 291,  94):��������� ������������� IN (�������)
      I_(2) = z'01000009'
C FDB30_logic.fgi( 291,  92):��������� ������������� IN (�������)
      C30_ifof = '������������������'
C FDB30_logic.fgi( 321, 153):��������� ���������� CH20 (CH30) (�������)
      L_(5)=.true.
C FDB30_logic.fgi( 270, 108):��������� ���������� (�������)
      L0_akof=R0_ikof.ne.R0_ekof
      R0_ekof=R0_ikof
C FDB30_logic.fgi( 257, 101):���������� ������������� ������
      if(L0_akof) then
         L_(12)=L_(5)
      else
         L_(12)=.false.
      endif
C FDB30_logic.fgi( 274, 107):���� � ������������� �������
      L_(4)=L_(12)
C FDB30_logic.fgi( 274, 107):������-�������: ���������� ��� �������������� ������
      I_(3) = z'01000003'
C FDB30_logic.fgi( 327,  83):��������� ������������� IN (�������)
      I_(4) = z'01000009'
C FDB30_logic.fgi( 327,  81):��������� ������������� IN (�������)
      I_(5) = z'01000003'
C FDB30_logic.fgi( 287,  83):��������� ������������� IN (�������)
      I_(6) = z'01000009'
C FDB30_logic.fgi( 287,  81):��������� ������������� IN (�������)
      L0_esof=R0_olof.ne.R0_ilof
      R0_ilof=R0_olof
C FDB30_logic.fgi( 257, 116):���������� ������������� ������
      L0_isof=R0_amof.ne.R0_ulof
      R0_ulof=R0_amof
C FDB30_logic.fgi( 257, 133):���������� ������������� ������
      L0_osof=R0_imof.ne.R0_emof
      R0_emof=R0_imof
C FDB30_logic.fgi( 257, 151):���������� ������������� ������
      C30_umof = '������������'
C FDB30_logic.fgi( 311, 150):��������� ���������� CH20 (CH30) (�������)
      C30_apof = '������'
C FDB30_logic.fgi( 305, 145):��������� ���������� CH20 (CH30) (�������)
      C30_opof = '��������������'
C FDB30_logic.fgi( 290, 146):��������� ���������� CH20 (CH30) (�������)
      C30_upof = '�� ������'
C FDB30_logic.fgi( 290, 148):��������� ���������� CH20 (CH30) (�������)
      I_(7) = z'01000003'
C FDB30_logic.fgi( 329,  94):��������� ������������� IN (�������)
      I_(8) = z'01000009'
C FDB30_logic.fgi( 329,  92):��������� ������������� IN (�������)
      L_(18)=.true.
C FDB30_logic.fgi( 270, 123):��������� ���������� (�������)
      if(L0_esof) then
         L_(15)=L_(18)
      else
         L_(15)=.false.
      endif
C FDB30_logic.fgi( 274, 122):���� � ������������� �������
      L_(3)=L_(15)
C FDB30_logic.fgi( 274, 122):������-�������: ���������� ��� �������������� ������
      L_(19)=.true.
C FDB30_logic.fgi( 270, 138):��������� ���������� (�������)
      if(L0_isof) then
         L_(16)=L_(19)
      else
         L_(16)=.false.
      endif
C FDB30_logic.fgi( 274, 137):���� � ������������� �������
      L_(2)=L_(16)
C FDB30_logic.fgi( 274, 137):������-�������: ���������� ��� �������������� ������
      L_(13) = L_(2).OR.L_(3).OR.L_(4)
C FDB30_logic.fgi( 284, 154):���
      L_(20)=.true.
C FDB30_logic.fgi( 270, 158):��������� ���������� (�������)
      if(L0_osof) then
         L_(17)=L_(20)
      else
         L_(17)=.false.
      endif
C FDB30_logic.fgi( 274, 157):���� � ������������� �������
      L_(1)=L_(17)
C FDB30_logic.fgi( 274, 157):������-�������: ���������� ��� �������������� ������
      L_(8) = L_(1).OR.L_(2).OR.L_(4)
C FDB30_logic.fgi( 284, 119):���
      L_orof=(L_(3).or.L_orof).and..not.(L_(8))
      L_(9)=.not.L_orof
C FDB30_logic.fgi( 292, 121):RS �������
      L_alof=L_orof
C FDB30_logic.fgi( 308, 123):������,FDB3_tech_mode
      if(L_alof) then
         I_okof=I_(4)
      else
         I_okof=I_(3)
      endif
C FDB30_logic.fgi( 330,  81):���� RE IN LO CH7
      L_(6) = L_(1).OR.L_(3).OR.L_(2)
C FDB30_logic.fgi( 284, 104):���
      L_ufof=(L_(4).or.L_ufof).and..not.(L_(6))
      L_(7)=.not.L_ufof
C FDB30_logic.fgi( 292, 106):RS �������
      L_ofof=L_ufof
C FDB30_logic.fgi( 308, 108):������,FDB3_avt_mode
      if(L_ofof) then
         I_efof=I_(2)
      else
         I_efof=I_(1)
      endif
C FDB30_logic.fgi( 294,  92):���� RE IN LO CH7
      L_(10) = L_(1).OR.L_(3).OR.L_(4)
C FDB30_logic.fgi( 284, 134):���
      L_urof=(L_(2).or.L_urof).and..not.(L_(10))
      L_(11)=.not.L_urof
C FDB30_logic.fgi( 292, 136):RS �������
      L_arof=L_urof
C FDB30_logic.fgi( 308, 138):������,FDB3_ruch_mode
      if(L_arof) then
         I_irof=I_(8)
      else
         I_irof=I_(7)
      endif
C FDB30_logic.fgi( 332,  92):���� RE IN LO CH7
      L_asof=(L_(1).or.L_asof).and..not.(L_(13))
      L_(14)=.not.L_asof
C FDB30_logic.fgi( 292, 156):RS �������
      L_elof=L_asof
C FDB30_logic.fgi( 310, 158):������,FDB3_automatic_mode
      if(L_elof) then
         I_ukof=I_(6)
      else
         I_ukof=I_(5)
      endif
C FDB30_logic.fgi( 290,  81):���� RE IN LO CH7
      if(L_asof) then
         C30_ipof=C30_opof
      else
         C30_ipof=C30_upof
      endif
C FDB30_logic.fgi( 294, 146):���� RE IN LO CH20
      if(L_urof) then
         C30_epof=C30_apof
      else
         C30_epof=C30_ipof
      endif
C FDB30_logic.fgi( 309, 145):���� RE IN LO CH20
      if(L_orof) then
         C30_omof=C30_umof
      else
         C30_omof=C30_epof
      endif
C FDB30_logic.fgi( 318, 144):���� RE IN LO CH20
      if(L_ufof) then
         C30_erof=C30_ifof
      else
         C30_erof=C30_omof
      endif
C FDB30_logic.fgi( 328, 143):���� RE IN LO CH20
      R_(21) = 3.0
C FDB30_logic.fgi( 145, 102):��������� (RE4) (�������)
      R_(24) = 30.0
C FDB30_logic.fgi(  54, 102):��������� (RE4) (�������)
      R_(27) = -400.0
C FDB30_logic.fgi(  51,  52):��������� (RE4) (�������)
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_olad,R_erad,REAL(1,4
     &),
     & REAL(R_utof,4),I_arad,REAL(R_emad,4),
     & REAL(R_imad,4),REAL(R_ilad,4),
     & REAL(R_uduf,4),REAL(R_umad,4),L_apad,REAL(R_epad,4
     &),L_ipad,
     & L_opad,R_afuf,REAL(R_amad,4),REAL(R_ulad,4),L_upad
     &)
      !}
C FDB30_vlv.fgi(  54,  94):���������� ������� ��� 2,20FDB30CM002XQ01
C label 230  try230=try230-1
      L_(22)=R_ovof.lt.R0_otof
C FDB30_logic.fgi( 143,  77):���������� <
      L_(21)=R_ovof.gt.R0_itof
C FDB30_logic.fgi( 143,  69):���������� >
      L_(23) = L_(22).AND.L_(21)
C FDB30_logic.fgi( 150,  76):�
      L_(29)=R_aduf.lt.R0_axof
C FDB30_logic.fgi(  52,  77):���������� <
      L_(28)=R_aduf.gt.R0_uvof
C FDB30_logic.fgi(  52,  69):���������� >
      L_(30) = L_(29).AND.L_(28)
C FDB30_logic.fgi(  59,  76):�
      L_(32)=R_ixof.lt.R_exof
C FDB30_logic.fgi(  69, 148):���������� <
      L_(27)=R_afuf.lt.R_uduf
C FDB30_logic.fgi(  69, 141):���������� <
      L_uxof = L_(32).AND.L_(27)
C FDB30_logic.fgi(  81, 147):�
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_itid,4),R8_irid,I_uxid
     &,I_ibod,I_oxid,
     & C8_asid,I_ebod,R_usid,R_osid,R_imid,
     & REAL(R_umid,4),R_opid,REAL(R_arid,4),
     & R_apid,REAL(R_ipid,4),I_exid,I_obod,I_abod,I_axid,L_erid
     &,
     & L_adod,L_udod,L_upid,L_epid,
     & L_urid,L_orid,L_edod,L_omid,L_isid,L_ifod,L_atid,
     & L_etid,REAL(R8_uref,8),REAL(1.0,4),R8_exef,L_uxof,L_oduf
     &,L_ebuf,
     & I_ubod,L_afod,R_uvid,REAL(R_esid,4),L_efod,L_amid,L_ofod
     &,L_emid,
     & L_otid,L_utid,L_avid,L_ivid,L_ovid,L_evid)
      !}

      if(L_ovid.or.L_ivid.or.L_evid.or.L_avid.or.L_utid.or.L_otid
     &) then      
                  I_ixid = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_ixid = z'40000000'
      endif
C FDB30_vlv.fgi( 179,  96):���� ���������� �������� ��������,20FDB30AA104
      L_abuf=L_uxof
C FDB30_logic.fgi( 104, 147):������,20FDB30AA103YA22
      if(L0_ibuf) then
         R_(26)=R_(24)
      else
         R_(26)=R_obuf
      endif
C FDB30_logic.fgi(  60, 103):���� RE IN LO CH7
      R_aduf=(R0_ubuf*R_(25)+deltat*R_(26))/(R0_ubuf+deltat
     &)
C FDB30_logic.fgi(  71, 104):�������������� �����  
      R_oxof=R_aduf
C FDB30_logic.fgi(  96, 104):������,20FDB30CQ002XQ01_input
      !{
      Call DAT_ANA_2_HANDLER(deltat,R_ipu,R_atu,REAL(1,4)
     &,
     & REAL(R_oxof,4),I_usu,REAL(R_aru,4),
     & REAL(R_eru,4),REAL(R_epu,4),
     & REAL(R_exof,4),REAL(R_oru,4),L_uru,REAL(R_asu,4),L_esu
     &,
     & L_isu,R_ixof,REAL(R_upu,4),REAL(R_opu,4),L_osu)
      !}
C FDB30_vlv.fgi(  81,  64):���������� ������� ��� 2,20FDB30CQ002XQ01
      L_(33)=R_ixof.gt.R_exof
C FDB30_logic.fgi(  69, 130):���������� >
      L_(26)=R_afuf.gt.R_uduf
C FDB30_logic.fgi(  69, 123):���������� >
      L_oduf = L_(33).OR.L_(26)
C FDB30_logic.fgi(  90, 129):���
      L_iduf=L_oduf
C FDB30_logic.fgi( 114, 129):������,20FDB30AA103YA21
      !{
      Call ZAPVALVE_MAN(deltat,REAL(R_imud,4),R8_ikud,I_urud
     &,I_isud,I_orud,
     & C8_alud,I_esud,R_ulud,R_olud,R_idud,
     & REAL(R_udud,4),R_ofud,REAL(R_akud,4),
     & R_afud,REAL(R_ifud,4),I_erud,I_osud,I_asud,I_arud,L_ekud
     &,
     & L_atud,L_utud,L_ufud,L_efud,
     & L_ukud,L_okud,L_etud,L_odud,L_ilud,L_ivud,L_amud,
     & L_emud,REAL(R8_uref,8),REAL(1.0,4),R8_exef,L_abuf,L_iduf
     &,L_eduf,
     & I_usud,L_avud,R_upud,REAL(R_elud,4),L_evud,L_adud,L_ovud
     &,L_edud,
     & L_omud,L_umud,L_apud,L_ipud,L_opud,L_epud)
      !}

      if(L_opud.or.L_ipud.or.L_epud.or.L_apud.or.L_umud.or.L_omud
     &) then      
                  I_irud = ior(z'000000FF',z'04000000')  
     &                          
                else
                   I_irud = z'40000000'
      endif
C FDB30_vlv.fgi( 165,  96):���� ���������� �������� ��������,20FDB30AA103
      L_(25) = (.NOT.L_eduf).AND.(.NOT.L_ebuf).AND.(.NOT.L_abuf
     &).AND.(.NOT.L_uxof).AND.L_(26)
C FDB30_logic.fgi( 149,  92):�
      L0_avof=(L_(25).or.L0_avof).and..not.(L_(23))
      L_(24)=.not.L0_avof
C FDB30_logic.fgi( 156,  90):RS �������
      if(L0_avof) then
         R_(23)=R_(21)
      else
         R_(23)=R_evof
      endif
C FDB30_logic.fgi( 151, 103):���� RE IN LO CH7
      R_ovof=(R0_ivof*R_(22)+deltat*R_(23))/(R0_ivof+deltat
     &)
C FDB30_logic.fgi( 162, 104):�������������� �����  
      R_utof=R_ovof
C FDB30_logic.fgi( 187, 104):������,20FDB30CM002XQ01_input
      L_(34) = (.NOT.L_eduf).AND.(.NOT.L_ebuf).AND.(.NOT.L_abuf
     &).AND.(.NOT.L_uxof).AND.L_(33)
C FDB30_logic.fgi(  58,  92):�
      L0_ibuf=(L_(34).or.L0_ibuf).and..not.(L_(30))
      L_(31)=.not.L0_ibuf
C FDB30_logic.fgi(  65,  90):RS �������
      Call PUMP2_HANDLER(deltat,I_ivi,I_evi,R_uri,
     & REAL(R_esi,4),R_eri,REAL(R_ori,4),L_ado,
     & L_abo,L_ufo,L_ako,L_oti,L_exi,L_oxi,L_ixi,L_uxi,L_obo
     &,
     & L_ari,L_asi,L_upi,L_iri,L_efo,
     & L_ifo,L_opi,L_ipi,L_uti,I_ovi,R_ido,R_odo,L_oko,
     & L_ubo,L_ofo,REAL(R8_uref,8),L_ibo,REAL(R8_ebo,8),R_eko
     &,
     & REAL(R_uvi,4),R_axi,REAL(R8_iti,8),R_eti,R8_exef,R_iko
     &,R8_ebo,
     & REAL(R_isi,4),REAL(R_osi,4))
C FDB30_vlv.fgi( 151,  71):���������� ���������� ������� 2,20FDB30AN001
C label 319  try319=try319-1
C sav1=R_iko
C sav2=R_eko
C sav3=L_ado
C sav4=L_obo
C sav5=L_abo
C sav6=R8_ebo
C sav7=R_axi
C sav8=I_ovi
C sav9=I_ivi
C sav10=I_evi
C sav11=I_avi
C sav12=L_uti
C sav13=L_oti
C sav14=R_eti
C sav15=L_asi
C sav16=L_iri
      Call PUMP2_HANDLER(deltat,I_ivi,I_evi,R_uri,
     & REAL(R_esi,4),R_eri,REAL(R_ori,4),L_ado,
     & L_abo,L_ufo,L_ako,L_oti,L_exi,L_oxi,L_ixi,L_uxi,L_obo
     &,
     & L_ari,L_asi,L_upi,L_iri,L_efo,
     & L_ifo,L_opi,L_ipi,L_uti,I_ovi,R_ido,R_odo,L_oko,
     & L_ubo,L_ofo,REAL(R8_uref,8),L_ibo,REAL(R8_ebo,8),R_eko
     &,
     & REAL(R_uvi,4),R_axi,REAL(R8_iti,8),R_eti,R8_exef,R_iko
     &,R8_ebo,
     & REAL(R_isi,4),REAL(R_osi,4))
C FDB30_vlv.fgi( 151,  71):recalc:���������� ���������� ������� 2,20FDB30AN001
C if(sav1.ne.R_iko .and. try319.gt.0) goto 319
C if(sav2.ne.R_eko .and. try319.gt.0) goto 319
C if(sav3.ne.L_ado .and. try319.gt.0) goto 319
C if(sav4.ne.L_obo .and. try319.gt.0) goto 319
C if(sav5.ne.L_abo .and. try319.gt.0) goto 319
C if(sav6.ne.R8_ebo .and. try319.gt.0) goto 319
C if(sav7.ne.R_axi .and. try319.gt.0) goto 319
C if(sav8.ne.I_ovi .and. try319.gt.0) goto 319
C if(sav9.ne.I_ivi .and. try319.gt.0) goto 319
C if(sav10.ne.I_evi .and. try319.gt.0) goto 319
C if(sav11.ne.I_avi .and. try319.gt.0) goto 319
C if(sav12.ne.L_uti .and. try319.gt.0) goto 319
C if(sav13.ne.L_oti .and. try319.gt.0) goto 319
C if(sav14.ne.R_eti .and. try319.gt.0) goto 319
C if(sav15.ne.L_asi .and. try319.gt.0) goto 319
C if(sav16.ne.L_iri .and. try319.gt.0) goto 319
      if(L_obo) then
         R_(18)=R_(19)
      else
         R_(18)=R_(20)
      endif
C FDB30_vent_log.fgi(  37, 266):���� RE IN LO CH7
      R8_ude=(R0_ide*R_(17)+deltat*R_(18))/(R0_ide+deltat
     &)
C FDB30_vent_log.fgi(  49, 267):�������������� �����  
      R8_ode=R8_ude
C FDB30_vent_log.fgi(  66, 267):������,F_FDB30AN001_G
      L_(36)=R_okuf.lt.R0_ifuf
C FDB30_logic.fgi(  48,  33):���������� <
C label 327  try327=try327-1
      L_(35)=R_okuf.gt.R0_efuf
C FDB30_logic.fgi(  48,  25):���������� >
      L_(37) = L_(36).AND.L_(35)
C FDB30_logic.fgi(  55,  32):�
      L0_akuf=(L_(39).or.L0_akuf).and..not.(L_(37))
      L_(38)=.not.L0_akuf
C FDB30_logic.fgi(  61,  41):RS �������
      if(L0_akuf) then
         R_(29)=R_(27)
      else
         R_(29)=R_ekuf
      endif
C FDB30_logic.fgi(  56,  53):���� RE IN LO CH7
      R_okuf=(R0_ikuf*R_(28)+deltat*R_(29))/(R0_ikuf+deltat
     &)
C FDB30_logic.fgi(  67,  54):�������������� �����  
      R_ofuf=R_okuf
C FDB30_logic.fgi(  92,  54):������,___20FDB30CP001XQ01_input
      Call PUMP2_HANDLER(deltat,I_edi,I_adi,R_ove,
     & REAL(R_axe,4),R_ave,REAL(R_ive,4),L_oki,
     & L_ufi,L_imi,L_omi,L_ibi,L_afi,L_ifi,L_efi,L_ofi,L_iki
     &,
     & L_ute,L_uve,L_ote,L_eve,L_uli,
     & L_ami,L_ite,L_ete,L_obi,I_idi,R_ali,R_eli,L_epi,
     & L_ubo,L_emi,REAL(R8_uref,8),L_eki,REAL(R8_aki,8),R_umi
     &,
     & REAL(R_odi,4),R_udi,REAL(R8_ebi,8),R_abi,R8_exef,R_api
     &,R8_aki,
     & REAL(R_exe,4),REAL(R_ixe,4))
C FDB30_vlv.fgi( 165,  71):���������� ���������� ������� 2,20FDB30AN002
C label 341  try341=try341-1
C sav1=R_api
C sav2=R_umi
C sav3=L_oki
C sav4=L_iki
C sav5=L_ufi
C sav6=R8_aki
C sav7=R_udi
C sav8=I_idi
C sav9=I_edi
C sav10=I_adi
C sav11=I_ubi
C sav12=L_obi
C sav13=L_ibi
C sav14=R_abi
C sav15=L_uve
C sav16=L_eve
      Call PUMP2_HANDLER(deltat,I_edi,I_adi,R_ove,
     & REAL(R_axe,4),R_ave,REAL(R_ive,4),L_oki,
     & L_ufi,L_imi,L_omi,L_ibi,L_afi,L_ifi,L_efi,L_ofi,L_iki
     &,
     & L_ute,L_uve,L_ote,L_eve,L_uli,
     & L_ami,L_ite,L_ete,L_obi,I_idi,R_ali,R_eli,L_epi,
     & L_ubo,L_emi,REAL(R8_uref,8),L_eki,REAL(R8_aki,8),R_umi
     &,
     & REAL(R_odi,4),R_udi,REAL(R8_ebi,8),R_abi,R8_exef,R_api
     &,R8_aki,
     & REAL(R_exe,4),REAL(R_ixe,4))
C FDB30_vlv.fgi( 165,  71):recalc:���������� ���������� ������� 2,20FDB30AN002
C if(sav1.ne.R_api .and. try341.gt.0) goto 341
C if(sav2.ne.R_umi .and. try341.gt.0) goto 341
C if(sav3.ne.L_oki .and. try341.gt.0) goto 341
C if(sav4.ne.L_iki .and. try341.gt.0) goto 341
C if(sav5.ne.L_ufi .and. try341.gt.0) goto 341
C if(sav6.ne.R8_aki .and. try341.gt.0) goto 341
C if(sav7.ne.R_udi .and. try341.gt.0) goto 341
C if(sav8.ne.I_idi .and. try341.gt.0) goto 341
C if(sav9.ne.I_edi .and. try341.gt.0) goto 341
C if(sav10.ne.I_adi .and. try341.gt.0) goto 341
C if(sav11.ne.I_ubi .and. try341.gt.0) goto 341
C if(sav12.ne.L_obi .and. try341.gt.0) goto 341
C if(sav13.ne.L_ibi .and. try341.gt.0) goto 341
C if(sav14.ne.R_abi .and. try341.gt.0) goto 341
C if(sav15.ne.L_uve .and. try341.gt.0) goto 341
C if(sav16.ne.L_eve .and. try341.gt.0) goto 341
      if(L_iki) then
         R_(14)=R_(15)
      else
         R_(14)=R_(16)
      endif
C FDB30_vent_log.fgi( 110, 266):���� RE IN LO CH7
      R8_ube=(R0_ibe*R_(13)+deltat*R_(14))/(R0_ibe+deltat
     &)
C FDB30_vent_log.fgi( 122, 267):�������������� �����  
      R8_obe=R8_ube
C FDB30_vent_log.fgi( 140, 267):������,F_FDB30AN002_G
      End
