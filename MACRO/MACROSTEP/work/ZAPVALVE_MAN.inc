      Interface
      Subroutine ZAPVALVE_MAN(ext_deltat,R_oki,R8_ete,I_e
     &,I_u,I_id,C8_ik,I_ok,R_il,R_ul,R_am,R_em,R_im,R_om,R_um
     &,R_ap,I_ep,I_up,I_ir,I_as,L_is,L_us,L_at,L_et,L_it,L_ut
     &,L_av,L_uv,L_ax,L_ox,L_ux,L_ebe,L_obe,R8_ade,R_ude,R8_ife
     &,L_ofe,L_ake,L_ile,I_ole,L_ose,R_ote,R_oxe,L_adi,L_idi
     &,L_ofi,L_eki,L_uki,L_ali,L_eli,L_ili,L_oli,L_uli)
C |R_oki         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |R8_ete        |4 8 O|16 value        |��� �������� ��������|0.5|
C |I_e           |2 4 O|LOPO            |����� �����������||
C |I_u           |2 4 O|LCLC            |����� �����������||
C |I_id          |2 4 O|LREADY          |����� ����������||
C |C8_ik         |3 8 O|TEXT            |�������� ���������||
C |I_ok          |2 4 O|LF              |����� �������������||
C |R_il          |4 4 O|POS_CL          |��������||
C |R_ul          |4 4 O|POS_OP          |��������||
C |R_am          |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_em          |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_im          |4 4 S|vmclose_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_om          |4 4 I|vmclose_button  |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_um          |4 4 S|vmopen_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ap          |4 4 I|vmopen_button   |������� ������ ������ "������� ������� �� ���������" |0.0|
C |I_ep          |2 4 O|LST             |����� ����||
C |I_up          |2 4 O|LCL             |����� �������||
C |I_ir          |2 4 O|LOP             |����� �������||
C |I_as          |2 4 O|LZM             |������ "�������"||
C |L_is          |1 1 I|vlv_kvit        |||
C |L_us          |1 1 I|instr_fault     |||
C |L_at          |1 1 S|_qffJ453*       |�������� ������ Q RS-��������  |F|
C |L_et          |1 1 O|vmclose_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_it          |1 1 O|vmopen_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ut          |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_av          |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_uv          |1 1 O|block           |||
C |L_ax          |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_ox          |1 1 O|STOP            |�������||
C |L_ux          |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_ebe         |1 1 O|norm            |�����||
C |L_obe         |1 1 O|nopower         |��� ����������||
C |R8_ade        |4 8 I|voltage         |[��]���������� �� ������||
C |R_ude         |4 4 I|power           |�������� ��������||
C |R8_ife        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_ofe         |1 1 I|YA22            |������� ������� �� ����������|F|
C |L_ake         |1 1 I|YA21            |������� ������� �� ����������|F|
C |L_ile         |1 1 O|fault           |�������������||
C |I_ole         |2 4 O|LAM             |������ "�������"||
C |L_ose         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_ote         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_oxe         |4 4 I|tcl_top         |����� ����|30.0|
C |L_adi         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_idi         |1 1 O|XH52            |�� ������� (���)|F|
C |L_ofi         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_eki         |1 1 O|XH51            |�� ������� (���)|F|
C |L_uki         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_ali         |1 1 I|mlf23           |������� ������� �����||
C |L_eli         |1 1 I|mlf22           |����� ����� ��������||
C |L_ili         |1 1 I|mlf04           |�������� �� ��������||
C |L_oli         |1 1 I|mlf03           |�������� �� ��������||
C |L_uli         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      INTEGER*4 I_e,I_u,I_id
      CHARACTER*8 C8_ik
      INTEGER*4 I_ok
      REAL*4 R_il,R_ul,R_am,R_em,R_im,R_om,R_um,R_ap
      INTEGER*4 I_ep,I_up,I_ir,I_as
      LOGICAL*1 L_is,L_us,L_at,L_et,L_it,L_ut,L_av,L_uv,L_ax
     &,L_ox,L_ux,L_ebe,L_obe
      REAL*8 R8_ade
      REAL*4 R_ude
      REAL*8 R8_ife
      LOGICAL*1 L_ofe,L_ake,L_ile
      INTEGER*4 I_ole
      LOGICAL*1 L_ose
      REAL*8 R8_ete
      REAL*4 R_ote,R_oxe
      LOGICAL*1 L_adi,L_idi,L_ofi,L_eki
      REAL*4 R_oki
      LOGICAL*1 L_uki,L_ali,L_eli,L_ili,L_oli,L_uli
      End subroutine ZAPVALVE_MAN
      End interface
