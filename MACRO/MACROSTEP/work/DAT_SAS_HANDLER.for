      Subroutine DAT_SAS_HANDLER(ext_deltat,R_od,R_ud,L_af
     &,R_uf,R_ak,I_al,R_el,R_il,R_ap,R_ep,L_ip,R_er,R_ir,I_is
     &,R_os,R_us,R_iv,R_ov,L_uv,R_ox,R_ux,I_ube,R_ade,R_ede
     &)
C |R_od          |4 4 I|LOWER_warning_setpoint3|||
C |R_ud          |4 4 I|LOWER_alarm_setpoint3|||
C |L_af          |1 1 I|fault3          |�����3||
C |R_uf          |4 4 I|UPPER_alarm_setpoint3|||
C |R_ak          |4 4 I|UPPER_warning_setpoint3|||
C |I_al          |2 4 O|dat3_color      |���� ��������3||
C |R_el          |4 4 O|KKS3            |�����3||
C |R_il          |4 4 I|input3          |����3||
C |R_ap          |4 4 I|LOWER_warning_setpoint2|||
C |R_ep          |4 4 I|LOWER_alarm_setpoint2|||
C |L_ip          |1 1 I|fault2          |�����2||
C |R_er          |4 4 I|UPPER_alarm_setpoint2|||
C |R_ir          |4 4 I|UPPER_warning_setpoint2|||
C |I_is          |2 4 O|dat2_color      |���� ��������2||
C |R_os          |4 4 O|KKS2            |�����2||
C |R_us          |4 4 I|input2          |����2||
C |R_iv          |4 4 I|LOWER_warning_setpoint1|||
C |R_ov          |4 4 I|LOWER_alarm_setpoint1|||
C |L_uv          |1 1 I|fault1          |�����1||
C |R_ox          |4 4 I|UPPER_alarm_setpoint1|||
C |R_ux          |4 4 I|UPPER_warning_setpoint1|||
C |I_ube         |2 4 O|dat1_color      |���� ��������1||
C |R_ade         |4 4 O|KKS1            |�����1||
C |R_ede         |4 4 I|input1          |����1||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      INTEGER*4 I0_e,I0_i,I0_o,I0_u,I0_ad
      LOGICAL*1 L0_ed,L0_id
      REAL*4 R_od,R_ud
      LOGICAL*1 L_af
      INTEGER*4 I0_ef,I0_if
      LOGICAL*1 L0_of
      REAL*4 R_uf,R_ak
      LOGICAL*1 L0_ek
      INTEGER*4 I0_ik,I0_ok,I0_uk,I_al
      REAL*4 R_el,R_il
      INTEGER*4 I0_ol,I0_ul,I0_am,I0_em,I0_im
      LOGICAL*1 L0_om,L0_um
      REAL*4 R_ap,R_ep
      LOGICAL*1 L_ip
      INTEGER*4 I0_op,I0_up
      LOGICAL*1 L0_ar
      REAL*4 R_er,R_ir
      LOGICAL*1 L0_or
      INTEGER*4 I0_ur,I0_as,I0_es,I_is
      REAL*4 R_os,R_us
      INTEGER*4 I0_at,I0_et,I0_it,I0_ot,I0_ut
      LOGICAL*1 L0_av,L0_ev
      REAL*4 R_iv,R_ov
      LOGICAL*1 L_uv
      INTEGER*4 I0_ax,I0_ex
      LOGICAL*1 L0_ix
      REAL*4 R_ox,R_ux
      LOGICAL*1 L0_abe
      INTEGER*4 I0_ebe,I0_ibe,I0_obe,I_ube
      REAL*4 R_ade,R_ede

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I0_e = z'01000015'
C DAT_SAS_HANDLER.fmg( 357, 234):��������� ������������� IN (�������)
      I0_o = z'01000017'
C DAT_SAS_HANDLER.fmg( 339, 236):��������� ������������� IN (�������)
      I0_ad = z'01000028'
C DAT_SAS_HANDLER.fmg( 322, 236):��������� ������������� IN (�������)
      I0_ok = z'01000010'
C DAT_SAS_HANDLER.fmg( 306, 238):��������� ������������� IN (�������)
      I0_uk = z'01000021'
C DAT_SAS_HANDLER.fmg( 306, 240):��������� ������������� IN (�������)
      I0_ef = z'01000054'
C DAT_SAS_HANDLER.fmg( 379, 234):��������� ������������� IN (�������)
      L0_ed=R_il.lt.R_od
C DAT_SAS_HANDLER.fmg( 338, 218):���������� <
      L0_id=R_il.lt.R_ud
C DAT_SAS_HANDLER.fmg( 357, 212):���������� <
      L0_of=R_il.gt.R_uf
C DAT_SAS_HANDLER.fmg( 319, 224):���������� >
      L0_ek=R_il.gt.R_ak
C DAT_SAS_HANDLER.fmg( 302, 230):���������� >
      if(L0_ek) then
         I0_ik=I0_ok
      else
         I0_ik=I0_uk
      endif
C DAT_SAS_HANDLER.fmg( 309, 238):���� RE IN LO CH7
      if(L0_of) then
         I0_u=I0_ad
      else
         I0_u=I0_ik
      endif
C DAT_SAS_HANDLER.fmg( 326, 237):���� RE IN LO CH7
      if(L0_ed) then
         I0_i=I0_o
      else
         I0_i=I0_u
      endif
C DAT_SAS_HANDLER.fmg( 342, 236):���� RE IN LO CH7
      if(L0_id) then
         I0_if=I0_e
      else
         I0_if=I0_i
      endif
C DAT_SAS_HANDLER.fmg( 360, 235):���� RE IN LO CH7
      if(L_af) then
         I_al=I0_ef
      else
         I_al=I0_if
      endif
C DAT_SAS_HANDLER.fmg( 382, 234):���� RE IN LO CH7
      R_el=R_il
C DAT_SAS_HANDLER.fmg( 331, 252):������,KKS3
      I0_ol = z'01000015'
C DAT_SAS_HANDLER.fmg( 225, 234):��������� ������������� IN (�������)
      I0_am = z'01000017'
C DAT_SAS_HANDLER.fmg( 207, 236):��������� ������������� IN (�������)
      I0_im = z'01000028'
C DAT_SAS_HANDLER.fmg( 190, 236):��������� ������������� IN (�������)
      I0_as = z'01000010'
C DAT_SAS_HANDLER.fmg( 174, 238):��������� ������������� IN (�������)
      I0_es = z'01000021'
C DAT_SAS_HANDLER.fmg( 174, 240):��������� ������������� IN (�������)
      I0_op = z'01000054'
C DAT_SAS_HANDLER.fmg( 247, 234):��������� ������������� IN (�������)
      L0_om=R_us.lt.R_ap
C DAT_SAS_HANDLER.fmg( 206, 218):���������� <
      L0_um=R_us.lt.R_ep
C DAT_SAS_HANDLER.fmg( 225, 212):���������� <
      L0_ar=R_us.gt.R_er
C DAT_SAS_HANDLER.fmg( 187, 224):���������� >
      L0_or=R_us.gt.R_ir
C DAT_SAS_HANDLER.fmg( 170, 230):���������� >
      if(L0_or) then
         I0_ur=I0_as
      else
         I0_ur=I0_es
      endif
C DAT_SAS_HANDLER.fmg( 177, 238):���� RE IN LO CH7
      if(L0_ar) then
         I0_em=I0_im
      else
         I0_em=I0_ur
      endif
C DAT_SAS_HANDLER.fmg( 194, 237):���� RE IN LO CH7
      if(L0_om) then
         I0_ul=I0_am
      else
         I0_ul=I0_em
      endif
C DAT_SAS_HANDLER.fmg( 210, 236):���� RE IN LO CH7
      if(L0_um) then
         I0_up=I0_ol
      else
         I0_up=I0_ul
      endif
C DAT_SAS_HANDLER.fmg( 228, 235):���� RE IN LO CH7
      if(L_ip) then
         I_is=I0_op
      else
         I_is=I0_up
      endif
C DAT_SAS_HANDLER.fmg( 250, 234):���� RE IN LO CH7
      R_os=R_us
C DAT_SAS_HANDLER.fmg( 199, 252):������,KKS2
      I0_at = z'01000015'
C DAT_SAS_HANDLER.fmg(  92, 234):��������� ������������� IN (�������)
      I0_it = z'01000017'
C DAT_SAS_HANDLER.fmg(  74, 236):��������� ������������� IN (�������)
      I0_ut = z'01000028'
C DAT_SAS_HANDLER.fmg(  58, 236):��������� ������������� IN (�������)
      I0_ibe = z'01000010'
C DAT_SAS_HANDLER.fmg(  40, 238):��������� ������������� IN (�������)
      I0_obe = z'01000021'
C DAT_SAS_HANDLER.fmg(  40, 240):��������� ������������� IN (�������)
      I0_ax = z'01000054'
C DAT_SAS_HANDLER.fmg( 114, 234):��������� ������������� IN (�������)
      L0_av=R_ede.lt.R_iv
C DAT_SAS_HANDLER.fmg(  74, 218):���������� <
      L0_ev=R_ede.lt.R_ov
C DAT_SAS_HANDLER.fmg(  92, 212):���������� <
      L0_ix=R_ede.gt.R_ox
C DAT_SAS_HANDLER.fmg(  54, 224):���������� >
      L0_abe=R_ede.gt.R_ux
C DAT_SAS_HANDLER.fmg(  38, 230):���������� >
      if(L0_abe) then
         I0_ebe=I0_ibe
      else
         I0_ebe=I0_obe
      endif
C DAT_SAS_HANDLER.fmg(  44, 238):���� RE IN LO CH7
      if(L0_ix) then
         I0_ot=I0_ut
      else
         I0_ot=I0_ebe
      endif
C DAT_SAS_HANDLER.fmg(  61, 237):���� RE IN LO CH7
      if(L0_av) then
         I0_et=I0_it
      else
         I0_et=I0_ot
      endif
C DAT_SAS_HANDLER.fmg(  78, 236):���� RE IN LO CH7
      if(L0_ev) then
         I0_ex=I0_at
      else
         I0_ex=I0_et
      endif
C DAT_SAS_HANDLER.fmg(  96, 235):���� RE IN LO CH7
      if(L_uv) then
         I_ube=I0_ax
      else
         I_ube=I0_ex
      endif
C DAT_SAS_HANDLER.fmg( 118, 234):���� RE IN LO CH7
      R_ade=R_ede
C DAT_SAS_HANDLER.fmg(  66, 252):������,KKS1
      End
