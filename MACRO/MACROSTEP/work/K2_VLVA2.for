      Subroutine K2_VLVA2(ext_deltat,R_o,R_ad,L_ed,R_od,R8_ik
     &,R8_ul,I_om,R_ap,R_ep,R_es,R_uv,R_ax,R_ix,R8_ebe,R_obe
     &,R_ake,L_eke,R_ale,L_ole,L_ule,L_eme,L_ime,L_ome,L_ume
     &,L_ape,L_epe,L_ipe,L_ere,L_ire,L_ure,L_ove,L_uve,L_exe
     &,L_adi,L_edi,L_idi,L_odi,L_udi,L_afi,L_efi,L_ifi,L_ofi
     &,L_ufi,L_aki,L_eki,L_iki,L_oki)
C |R_o           |4 4 K|_tdel1          |[]�������� ������� �������� ������|2.0|
C |R_ad          |4 4 S|_sdel1*         |[���]���������� ��������� �������� ������ |0.0|
C |L_ed          |1 1 S|_idel1*         |[TF]���������� ��������� �������� ������ |f|
C |R_od          |4 4 K|_c02            |�������� ��������� ����������|`power_c`|
C |R8_ik         |4 8 I|voltage         |[�]���������� ������ �������� �������||
C |R8_ul         |4 8 O|sn_power        |�������� ���� ��|3.0|
C |I_om          |2 4 I|SF1SWT          |������� ������� �������||
C |R_ap          |4 4 O|out_value_prc   |���.��������� ����� � %|50|
C |R_ep          |4 4 S|_slpb1*         |���������� ��������� ||
C |R_es          |4 4 I|tau             |���������� ������� �������|0.0|
C |R_uv          |4 4 I|tclose          |������ ����� ��������|30.0|
C |R_ax          |4 4 I|topen           |������ ����� ��������|30.0|
C |R_ix          |4 4 O|_oapr1*         |�������� ������ ���������. ����� |0.0|
C |R8_ebe        |4 8 O|value           |���.�������� ��������|0.5|
C |R_obe         |4 4 O|out_value       |���.��������� �����|0.5|
C |R_ake         |4 4 I|GM19P           |��������� ������������|0.6|
C |L_eke         |1 1 S|_qff3*          |�������� ������ Q RS-��������  |F|
C |R_ale         |4 4 O|posfbst         |��������� ���.��|0.5|
C |L_ole         |1 1 I|GM47            |��� ��������� ���������||
C |L_ule         |1 1 I|blockreg        |���������� ���� �����������|F|
C |L_eme         |1 1 I|uluclose        |������� ������� (���)||
C |L_ime         |1 1 I|uluopen         |������� ������� (���)||
C |L_ome         |1 1 I|blockblk        |���������� ���� ����������|F|
C |L_ume         |1 1 O|cbc             |�� ������� (���)|F|
C |L_ape         |1 1 O|cbo             |�� ������� (���)|F|
C |L_epe         |1 1 O|cbcm            |������� 0.001|F|
C |L_ipe         |1 1 O|outc            |��� ������� �������|F|
C |L_ere         |1 1 O|RASP            |��� �������� � ����.�������|F|
C |L_ire         |1 1 O|outo            |��� ������� �������|F|
C |L_ure         |1 1 O|cbom            |������� 0.999|F|
C |L_ove         |1 1 O|cmdcl           |������� ������� (���)||
C |L_uve         |1 1 I|arstate         |��������� ����������|F|
C |L_exe         |1 1 O|cmdop           |������� ������� (���)||
C |L_adi         |1 1 I|tzclose         |������� ������� (��)|F|
C |L_edi         |1 1 I|blkclose        |���� �������� (��)||
C |L_idi         |1 1 I|blkopen         |����. �������� (��)||
C |L_odi         |1 1 I|tzopen          |������� ������� (��)|F|
C |L_udi         |1 1 I|GM45            |����. ������ �� ���||
C |L_afi         |1 1 I|GM03            |������� � ��������||
C |L_efi         |1 1 I|GM04            |������� � ��������||
C |L_ifi         |1 1 I|GM19            |���� ������������||
C |L_ofi         |1 1 I|GM08            |������� �� "�������"||
C |L_ufi         |1 1 I|GM09            |������� �� "�������"||
C |L_aki         |1 1 I|GM22            |����� ����� ��������||
C |L_eki         |1 1 I|GM23            |������� ������� �����||
C |L_iki         |1 1 I|GM24            |��� ���������� �� � ����������||
C |L_oki         |1 1 I|GM46            |�� �����������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      INTEGER*4 I0_e,I0_i
      REAL*4 R_o,R0_u,R_ad
      LOGICAL*1 L_ed,L0_id
      REAL*4 R_od,R0_ud,R0_af,R0_ef,R0_if,R0_of,R0_uf
      LOGICAL*1 L0_ak
      REAL*4 R0_ek
      REAL*8 R8_ik
      LOGICAL*1 L0_ok,L0_uk
      REAL*4 R0_al,R0_el,R0_il,R0_ol
      REAL*8 R8_ul
      LOGICAL*1 L0_am
      INTEGER*4 I0_em
      LOGICAL*1 L0_im
      INTEGER*4 I_om
      REAL*4 R0_um,R_ap,R_ep,R0_ip,R0_op,R0_up,R0_ar,R0_er
     &,R0_ir,R0_or,R0_ur,R0_as,R_es
      LOGICAL*1 L0_is
      REAL*4 R0_os,R0_us,R0_at,R0_et,R0_it,R0_ot,R0_ut,R0_av
     &,R0_ev,R0_iv,R0_ov,R_uv,R_ax,R0_ex,R_ix,R0_ox,R0_ux
      LOGICAL*1 L0_abe
      REAL*8 R8_ebe
      REAL*4 R0_ibe,R_obe,R0_ube,R0_ade,R0_ede,R0_ide,R0_ode
      LOGICAL*1 L0_ude
      REAL*4 R0_afe,R0_efe,R0_ife,R0_ofe
      LOGICAL*1 L0_ufe
      REAL*4 R_ake
      LOGICAL*1 L_eke
      REAL*4 R0_ike,R0_oke,R0_uke,R_ale,R0_ele,R0_ile
      LOGICAL*1 L_ole,L_ule,L0_ame,L_eme,L_ime,L_ome,L_ume
     &,L_ape,L_epe,L_ipe,L0_ope,L0_upe,L0_are,L_ere,L_ire
     &,L0_ore,L_ure,L0_ase,L0_ese,L0_ise,L0_ose,L0_use
      LOGICAL*1 L0_ate,L0_ete,L0_ite,L0_ote,L0_ute,L0_ave
     &,L0_eve,L0_ive,L_ove,L_uve,L0_axe,L_exe,L0_ixe,L0_oxe
     &,L0_uxe,L0_abi,L0_ebi,L0_ibi,L0_obi,L0_ubi,L_adi,L_edi
      LOGICAL*1 L_idi,L_odi,L_udi,L_afi,L_efi,L_ifi,L_ofi
     &,L_ufi,L_aki,L_eki,L_iki,L_oki

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_u=R_ad
C K2_VLVA2.fmg( 158, 279):pre: �������� ��������� ������,1
      R0_ux=R_ep
C K2_VLVA2.fmg( 455, 453):pre: ����������� ��,1
      R0_os=R_ix
C K2_VLVA2.fmg( 368, 451):pre: �������������� �����  ,1
      R0_us = 1.0
C K2_VLVA2.fmg( 115, 362):��������� (RE4) (�������)
      R0_et = R_ax * R0_us
C K2_VLVA2.fmg( 337, 453):����������
      R0_at = R_uv * R0_us
C K2_VLVA2.fmg( 337, 433):����������
      L0_ame=.true.
C K2_VLVA2.fmg( 113, 356):��������� ���������� (�������)
      L0_oxe = L_ime.AND.L0_ame
C K2_VLVA2.fmg( 130, 448):�
      L0_ave = L_eme.AND.L0_ame
C K2_VLVA2.fmg( 130, 399):�
      L0_ive=.false.
C K2_VLVA2.fmg( 140, 414):��������� ���������� (�������)
      L_ove = L0_ive.AND.(.NOT.L_uve)
C K2_VLVA2.fmg( 144, 413):�
      L0_ote = (.NOT.L_oki).AND.L_ove
C K2_VLVA2.fmg( 151, 414):�
      L0_axe=.false.
C K2_VLVA2.fmg( 140, 442):��������� ���������� (�������)
      L_exe = L0_axe.AND.(.NOT.L_uve)
C K2_VLVA2.fmg( 144, 441):�
      L0_ete = L_exe.AND.(.NOT.L_oki)
C K2_VLVA2.fmg( 152, 440):�
      I0_i = 1
C K2_VLVA2.fmg( 160, 275):��������� ������������� IN (�������)
      L0_ak = L_iki.OR.L_eki.OR.L_aki
C K2_VLVA2.fmg( 134, 324):���
      !��������� R0_ud = K2_VLVA2C02 /`power_c`/
      R0_ud=R_od
C K2_VLVA2.fmg( 111, 333):���������,02
      R0_of = 3
C K2_VLVA2.fmg( 144, 327):��������� (RE4) (�������)
      R0_af = 0.2
C K2_VLVA2.fmg( 136, 328):��������� (RE4) (�������)
      R0_ef = 1
C K2_VLVA2.fmg( 136, 330):��������� (RE4) (�������)
      if(L0_ak) then
         R0_uf=R0_af
      else
         R0_uf=R0_ef
      endif
C K2_VLVA2.fmg( 139, 329):���� RE IN LO CH7
      R0_ek = 95.0
C K2_VLVA2.fmg( 174, 257):��������� (RE4) (�������)
      L0_ok=R8_ik.lt.R0_ek
C K2_VLVA2.fmg( 178, 259):���������� <
      R0_el = 0.0
C K2_VLVA2.fmg( 166, 331):��������� (RE4) (�������)
      I0_em = 1
C K2_VLVA2.fmg( 174, 274):��������� ������������� IN (�������)
      L0_am=I0_em.eq.I_om
C K2_VLVA2.fmg( 178, 273):���������� �������������
      L_ere = L0_am.OR.L0_ok
C K2_VLVA2.fmg( 186, 272):���
      R0_um = 100.0
C K2_VLVA2.fmg( 458, 475):��������� (RE4) (�������)
      R0_ip = 0.0
C K2_VLVA2.fmg( 444, 469):��������� (RE4) (�������)
      R0_op = 0.1
C K2_VLVA2.fmg( 426, 417):��������� (RE4) (�������)
      if(L_ufi) then
         R0_up=1
      else
         R0_up=0
      endif
C K2_VLVA2.fmg( 423, 414):��������� LO->1
      R0_ar = R0_op * R0_up
C K2_VLVA2.fmg( 429, 415):����������
      R0_er = 0.000001
C K2_VLVA2.fmg( 426, 419):��������� (RE4) (�������)
      R0_ide = R0_er + R0_ar
C K2_VLVA2.fmg( 434, 419):��������
      R0_ir = 0.1
C K2_VLVA2.fmg( 426, 427):��������� (RE4) (�������)
      if(L_ofi) then
         R0_or=1
      else
         R0_or=0
      endif
C K2_VLVA2.fmg( 423, 424):��������� LO->1
      R0_ur = R0_ir * R0_or
C K2_VLVA2.fmg( 429, 425):����������
      R0_as = 0.999999
C K2_VLVA2.fmg( 426, 429):��������� (RE4) (�������)
      R0_ode = R0_as + R0_ur
C K2_VLVA2.fmg( 434, 429):��������
      R0_it = DeltaT
C K2_VLVA2.fmg( 337, 456):��������� (RE4) (�������)
      if(R0_et.ge.0.0) then
         R0_ut=R0_it/max(R0_et,1.0e-10)
      else
         R0_ut=R0_it/min(R0_et,-1.0e-10)
      endif
C K2_VLVA2.fmg( 345, 455):�������� ����������
      if(R0_at.ge.0.0) then
         R0_ot=R0_it/max(R0_at,1.0e-10)
      else
         R0_ot=R0_it/min(R0_at,-1.0e-10)
      endif
C K2_VLVA2.fmg( 345, 435):�������� ����������
      R0_ov = 0.0
C K2_VLVA2.fmg( 349, 447):��������� (RE4) (�������)
      R0_ex = 0.0
C K2_VLVA2.fmg( 375, 448):��������� (RE4) (�������)
      R0_ox = 0.0
C K2_VLVA2.fmg( 450, 446):��������� (RE4) (�������)
      if(L_aki) then
         R0_ibe=R0_ox
      else
         R0_ibe=R0_ux
      endif
C K2_VLVA2.fmg( 453, 447):���� RE IN LO CH7
      L0_abe = L_eki.OR.L_aki
C K2_VLVA2.fmg( 443, 442):���
      R0_ube = 1.0
C K2_VLVA2.fmg( 427, 456):��������� (RE4) (�������)
      R0_ade = 0.0
C K2_VLVA2.fmg( 419, 456):��������� (RE4) (�������)
      R0_efe = 1.0e-10
C K2_VLVA2.fmg( 396, 434):��������� (RE4) (�������)
      R0_ike = 0.0
C K2_VLVA2.fmg( 407, 447):��������� (RE4) (�������)
      L0_ixe = L_oki.OR.L_ule.OR.L_udi
C K2_VLVA2.fmg( 124, 424):���
      L0_uxe = L0_oxe.AND.(.NOT.L0_ixe)
C K2_VLVA2.fmg( 144, 447):�
      L0_ute = L0_uxe.AND.(.NOT.L0_ote)
C K2_VLVA2.fmg( 172, 446):�
      L0_ate = L0_ute.OR.L0_ete
C K2_VLVA2.fmg( 178, 445):���
      L0_eve = (.NOT.L0_ixe).AND.L0_ave
C K2_VLVA2.fmg( 144, 400):�
      L0_ite = (.NOT.L0_ete).AND.L0_eve
C K2_VLVA2.fmg( 172, 401):�
      L0_use = L0_ote.OR.L0_ite
C K2_VLVA2.fmg( 178, 402):���
      L0_obi = L_ome.OR.L_oki
C K2_VLVA2.fmg( 124, 436):���
      L0_ubi = L_odi.AND.(.NOT.L0_obi)
C K2_VLVA2.fmg( 144, 465):�
      L0_ebi = L_afi.OR.L0_ubi
C K2_VLVA2.fmg( 175, 466):���
      L0_abi = (.NOT.L_efi).AND.L0_ebi
C K2_VLVA2.fmg( 188, 467):�
      L0_ibi = (.NOT.L0_obi).AND.L_idi
C K2_VLVA2.fmg( 144, 456):�
      L0_ose = L_edi.AND.(.NOT.L0_obi)
C K2_VLVA2.fmg( 144, 389):�
      L0_are = (.NOT.L0_ebi).AND.L0_use.AND.(.NOT.L0_ose)
C K2_VLVA2.fmg( 192, 402):�
      L0_ise = (.NOT.L0_obi).AND.L_adi
C K2_VLVA2.fmg( 144, 384):�
      L0_ase = L0_ise.OR.L_efi
C K2_VLVA2.fmg( 175, 383):���
      L0_ese = (.NOT.L0_ibi).AND.L0_ate.AND.(.NOT.L0_ase)
C K2_VLVA2.fmg( 192, 445):�
      L0_ore = L0_abi.OR.L0_ese
C K2_VLVA2.fmg( 199, 446):���
      L0_ope = L0_ase.AND.(.NOT.L_afi)
C K2_VLVA2.fmg( 188, 382):�
      L0_upe = L0_are.OR.L0_ope
C K2_VLVA2.fmg( 199, 401):���
      L_ire = (.NOT.L_ure).AND.L0_ore.AND.(.NOT.L_ere)
C K2_VLVA2.fmg( 222, 446):�
C label 155  try155=try155-1
      if(.NOT.L0_abe) then
         R_ale=R8_ebe
      endif
C K2_VLVA2.fmg( 464, 456):���� � ������������� �������
      if(L_ire) then
         R0_ev=R0_ut
      else
         R0_ev=R0_ov
      endif
C K2_VLVA2.fmg( 353, 455):���� RE IN LO CH7
      L_ipe = L0_upe.AND.(.NOT.L_epe).AND.(.NOT.L_ere)
C K2_VLVA2.fmg( 222, 399):�
      if(L_ipe) then
         R0_av=R0_ot
      else
         R0_av=R0_ov
      endif
C K2_VLVA2.fmg( 353, 435):���� RE IN LO CH7
      R0_iv = R0_ev + (-R0_av)
C K2_VLVA2.fmg( 359, 451):��������
      L0_is =.NOT.(L_ire.OR.L_ipe)
C K2_VLVA2.fmg( 361, 442):���
      if(L0_is) then
         R_ix=R0_iv
      else
         R_ix=(R_es*R0_os+deltat*R0_iv)/(R_es+deltat)
      endif
C K2_VLVA2.fmg( 368, 451):�������������� �����  ,1
      if(L_iki) then
         R0_oke=R0_ex
      else
         R0_oke=R_ix
      endif
C K2_VLVA2.fmg( 378, 449):���� RE IN LO CH7
      R0_ofe = R_ale + (-R_ake)
C K2_VLVA2.fmg( 385, 440):��������
      R0_afe = R0_oke + R0_ofe
C K2_VLVA2.fmg( 391, 441):��������
      R0_ife = R0_afe * R0_ofe
C K2_VLVA2.fmg( 395, 440):����������
      L0_ufe=R0_ife.lt.R0_efe
C K2_VLVA2.fmg( 400, 439):���������� <
      L_eke=(L0_ufe.or.L_eke).and..not.(.NOT.L_ifi)
      L0_ude=.not.L_eke
C K2_VLVA2.fmg( 407, 437):RS �������,3
      if(L_eke) then
         R_ale=R_ake
      endif
C K2_VLVA2.fmg( 402, 456):���� � ������������� �������
      if(L_eke) then
         R0_uke=R0_ike
      else
         R0_uke=R0_oke
      endif
C K2_VLVA2.fmg( 410, 448):���� RE IN LO CH7
      R0_ele = R_ale + R0_uke
C K2_VLVA2.fmg( 416, 450):��������
      R0_ede=MAX(R0_ade,R0_ele)
C K2_VLVA2.fmg( 424, 451):��������
      R0_ile=MIN(R0_ube,R0_ede)
C K2_VLVA2.fmg( 432, 452):�������
      L_ure=R0_ile.gt.R0_ode
C K2_VLVA2.fmg( 443, 430):���������� >
C sav1=L_ire
      L_ire = (.NOT.L_ure).AND.L0_ore.AND.(.NOT.L_ere)
C K2_VLVA2.fmg( 222, 446):recalc:�
C if(sav1.ne.L_ire .and. try155.gt.0) goto 155
      if(L0_abe) then
         R8_ebe=R0_ibe
      else
         R8_ebe=R0_ile
      endif
C K2_VLVA2.fmg( 457, 450):���� RE IN LO CH7
      R_ep=R8_ebe
C K2_VLVA2.fmg( 455, 453):����������� ��,1
      if(L_ole) then
         R_obe=R0_ip
      else
         R_obe=R0_ile
      endif
C K2_VLVA2.fmg( 447, 470):���� RE IN LO CH7
      R_ap = R0_um * R_obe
C K2_VLVA2.fmg( 461, 475):����������
      L_epe=R0_ile.lt.R0_ide
C K2_VLVA2.fmg( 443, 420):���������� <
C sav1=L_ipe
      L_ipe = L0_upe.AND.(.NOT.L_epe).AND.(.NOT.L_ere)
C K2_VLVA2.fmg( 222, 399):recalc:�
C if(sav1.ne.L_ipe .and. try175.gt.0) goto 175
      L_ume = (.NOT.L_ure).AND.L_epe.AND.(.NOT.L_ere)
C K2_VLVA2.fmg( 222, 375):�
      L_ape = L_ure.AND.(.NOT.L_epe).AND.(.NOT.L_ere)
C K2_VLVA2.fmg( 222, 472):�
      if(L0_abe) then
         R_ale=R0_ile
      endif
C K2_VLVA2.fmg( 446, 456):���� � ������������� �������
C sav1=R0_ofe
      R0_ofe = R_ale + (-R_ake)
C K2_VLVA2.fmg( 385, 440):recalc:��������
C if(sav1.ne.R0_ofe .and. try196.gt.0) goto 196
C sav1=R0_ele
      R0_ele = R_ale + R0_uke
C K2_VLVA2.fmg( 416, 450):recalc:��������
C if(sav1.ne.R0_ele .and. try217.gt.0) goto 217
      if(L_eke) then
         R0_if=R0_of
      else
         R0_if=R0_uf
      endif
C K2_VLVA2.fmg( 147, 328):���� RE IN LO CH7
      R0_al = R0_ud * R0_if
C K2_VLVA2.fmg( 152, 330):����������
      L0_uk = L_ire.OR.L_ipe
C K2_VLVA2.fmg( 129, 306):���
      if(L0_uk) then
         R0_ol=R0_al
      else
         R0_ol=R0_el
      endif
C K2_VLVA2.fmg( 169, 330):���� RE IN LO CH7
      L0_id = L_eke.AND.L0_uk
C K2_VLVA2.fmg( 150, 279):�
      if(.not.L0_id) then
         R_ad=0.0
      elseif(.not.L_ed) then
         R_ad=R_o
      else
         R_ad=max(R0_u-deltat,0.0)
      endif
      L0_im=L0_id.and.R_ad.le.0.0
      L_ed=L0_id
C K2_VLVA2.fmg( 158, 279):�������� ��������� ������,1
      if(L0_im) then
         I0_e=I0_i
      endif
C K2_VLVA2.fmg( 163, 276):���� � ������������� �������
      R0_il = R8_ul
C K2_VLVA2.fmg( 164, 340):��������
C label 279  try279=try279-1
      R8_ul = R0_ol + R0_il
C K2_VLVA2.fmg( 175, 339):��������
C sav1=R0_il
      R0_il = R8_ul
C K2_VLVA2.fmg( 164, 340):recalc:��������
C if(sav1.ne.R0_il .and. try279.gt.0) goto 279
      End
