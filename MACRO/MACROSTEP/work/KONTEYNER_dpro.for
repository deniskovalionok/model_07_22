      Subroutine KONTEYNER_dpro(ext_deltat,R_i,R_o,L_u,L_ad
     &,L_ed,L_id,L_od,L_ud,L_af,L_ef,L_if,L_of,L_uf,L_ak,L_ek
     &,L_ik,L_ok,L_uk,L_al,L_el,L_il,L_ol,L_ul,L_at,L_et,L_it
     &,L_ot,L_ut,L_av,L_ev,L_iv,L_ov,L_uv,L_ix,R_ox,R_ux,R_abe
     &,R_ebe,L_obe,L_ade,L_ede,R_ude,R_efe,R_ofe,L_ufe,R_ake
     &,R_eke,R_ike,L_oke,L_ale,L_ile,R_ame,R_ome,R_ape,R_ipe
     &,R_upe,R_ere,R_ase,R_ose,R_ete,R_ote,R_ute,R_ave,R_eve
     &,R_ive,R_ove,L_uve,L_axe,L_exe,L_ixe,L_oxe,L_uxe,L_abi
     &,L_ebi,L_ibi,L_obi,R_upi,R_ari,L_eri,R_iri,L_ori,L_esi
     &,R_eti,R_uti,R_evi,R_ivi,L_ovi,R_uvi,L_axi,L_oxi,R_obo
     &,R_edo,R_odo,R_udo,L_afo,R_efo,L_ifo,L_ako,R_alo,R_olo
     &,R_amo,R_emo,L_imo,R_omo,L_umo,L_ipo,R_iro,R_aso,R_iso
     &,R_oso,L_uso,R_ato,L_eto,L_uto,R_uvo,R_ixo,R_uxo,R_abu
     &,L_ebu,R_ibu,L_obu,L_edu,R_efu,R_ufu,R_eku,R_iku,L_oku
     &,R_uku,L_alu,L_emu,L_imu,R_umu,R_ipu,R_aru,R_iru,R_uru
     &,R_asu,R_esu,L_isu,R_osu,L_usu,L_avu,L_evu,R_ovu,R_exu
     &,R_uxu,R_ebad,R_obad,R_ubad,R_adad,L_edad,R_idad,L_odad
     &,L_ufad,L_akad,R_ikad,R_alad,R_olad,R_amad,R_imad,R_apad
     &,R_epad,L_ipad,R_opad,L_upad,L_asad,L_esad,R_usad,R_etad
     &,R_avad,R_ivad,R_uvad,R_ixad,R_oxad,R_uxad,R_abed,R_ebed
     &)
C |R_i           |4 4 S|_simpftime*     |[���]���������� ��������� ������������� |0.0|
C |R_o           |4 4 K|_timpftime      |[���]������������ �������� �������������|5|
C |L_u           |1 1 S|_limpftime*     |[TF]���������� ��������� ������������� |F|
C |L_ad          |1 1 S|_splsi10-3*     |[TF]���������� ��������� ������������� |F|
C |L_ed          |1 1 S|_splsi10-2*     |[TF]���������� ��������� ������������� |F|
C |L_id          |1 1 S|_splsi9-3*      |[TF]���������� ��������� ������������� |F|
C |L_od          |1 1 S|_splsi9-2*      |[TF]���������� ��������� ������������� |F|
C |L_ud          |1 1 S|_splsi8-3*      |[TF]���������� ��������� ������������� |F|
C |L_af          |1 1 S|_splsi8-2*      |[TF]���������� ��������� ������������� |F|
C |L_ef          |1 1 S|_splsi7-3*      |[TF]���������� ��������� ������������� |F|
C |L_if          |1 1 S|_splsi7-2*      |[TF]���������� ��������� ������������� |F|
C |L_of          |1 1 S|_splsi6-3*      |[TF]���������� ��������� ������������� |F|
C |L_uf          |1 1 S|_splsi6-2*      |[TF]���������� ��������� ������������� |F|
C |L_ak          |1 1 S|_splsi5-3*      |[TF]���������� ��������� ������������� |F|
C |L_ek          |1 1 S|_splsi5-2*      |[TF]���������� ��������� ������������� |F|
C |L_ik          |1 1 S|_splsi4-3*      |[TF]���������� ��������� ������������� |F|
C |L_ok          |1 1 S|_splsi4-2*      |[TF]���������� ��������� ������������� |F|
C |L_uk          |1 1 S|_splsi3-3*      |[TF]���������� ��������� ������������� |F|
C |L_al          |1 1 S|_splsi3-2*      |[TF]���������� ��������� ������������� |F|
C |L_el          |1 1 S|_splsi2-3*      |[TF]���������� ��������� ������������� |F|
C |L_il          |1 1 S|_splsi2-2*      |[TF]���������� ��������� ������������� |F|
C |L_ol          |1 1 S|_splsi1-3*      |[TF]���������� ��������� ������������� |F|
C |L_ul          |1 1 S|_splsi1-2*      |[TF]���������� ��������� ������������� |F|
C |L_at          |1 1 I|FDA20TRAN01_C2_CBC|������ ������||
C |L_et          |1 1 I|FDA20TRAN02_C2_CBC|������ ������||
C |L_it          |1 1 I|FDA20TRAN03_C2_CBC|������ ������||
C |L_ot          |1 1 I|FDA20TRAN04_C2_CBC|������ ������||
C |L_ut          |1 1 I|FDA20TRAN05_C2_CBC|������ ������||
C |L_av          |1 1 I|FDA20TRAN06_C2_CBC|������ ������||
C |L_ev          |1 1 I|FDA20TRAN07_C2_CBC|������ ������||
C |L_iv          |1 1 I|FDA20TRAN08_C2_CBC|������ ������||
C |L_ov          |1 1 I|FDA20TRAN09_C2_CBC|������ ������||
C |L_uv          |1 1 I|FDA20TRAN10_C2_CBC|������ ������||
C |L_ix          |1 1 I|FDA20UNL01      |�������||
C |R_ox          |4 4 K|_utunweight     |����������� ������ ��������� ������|11.0|
C |R_ux          |4 4 K|_ttunweight     |������ ����� ��������� �������|21|
C |R_abe         |4 4 S|_stunweight*    |��������� ����������  ||
C |R_ebe         |4 4 K|_ltunweight     |����������� ������ ��������� �����|7.0|
C |L_obe         |1 1 S|_uc0tunweight*  |��������� ����� ���������� |F|
C |L_ade         |1 1 S|_lc0tunweight*  |��������� ����� ���������� |F|
C |L_ede         |1 1 I|FDA20KANT01_C10XH54|��������� ����������||
C |R_ude         |4 4 K|_lcmpc11-8      |[]�������� ������ �����������|0.1|
C |R_efe         |4 4 K|_lcmpc11-7      |[]�������� ������ �����������|600|
C |R_ofe         |4 4 K|_lcmpc11-6      |[]�������� ������ �����������|1950|
C |L_ufe         |1 1 S|_splsi11-1*     |[TF]���������� ��������� ������������� |F|
C |R_ake         |4 4 I|FDA20KANT01_C1_MFVAL|��������� ����� ����������||
C |R_eke         |4 4 I|FDA20KANT01VZ01 |���������� ����������� �� OZ||
C |R_ike         |4 4 I|FDA20KANT01VX01 |���������� ����������� �� OX||
C |L_oke         |1 1 O|TAKE11          |��������� �������� � �����������||
C |L_ale         |1 1 S|_qfftr11*       |�������� ������ Q RS-��������  |F|
C |L_ile         |1 1 I|FDA20KANT01_C2_CBC|������ ������||
C |R_ame         |4 4 K|_lcmpc11-3      |[]�������� ������ �����������|5|
C |R_ome         |4 4 K|_lcmpc11-1      |[]�������� ������ �����������|5|
C |R_ape         |4 4 S|_memadd_t4*     |����������� �������� ������� ||
C |R_ipe         |4 4 S|_memadd_t3*     |����������� �������� ������� ||
C |R_upe         |4 4 S|_memadd_t1*     |����������� �������� ������� ||
C |R_ere         |4 4 I|START_T         |||
C |R_ase         |4 4 S|_memadd_t2*     |����������� �������� ������� ||
C |R_ose         |4 4 O|R1VT01          |����������� ����������||
C |R_ete         |4 4 S|_memwtng_u*     |����������� �������� ������� ||
C |R_ote         |4 4 S|_memwtng_f*     |����������� �������� ������� ||
C |R_ute         |4 4 S|_memwtng_c*     |����������� �������� ������� ||
C |R_ave         |4 4 O|R1VW01_u        |����� ���������� ����� �������||
C |R_eve         |4 4 O|R1VW01_f        |����� ���������� ����� �������||
C |R_ive         |4 4 O|R1VW01_c        |����� ���������� �������||
C |R_ove         |4 4 O|R1VW01          |����� ����������|`START_W`|
C |L_uve         |1 1 O|INABOX01        |� ����������� �����||
C |L_axe         |1 1 O|INABOX10        |� ������������� �����||
C |L_exe         |1 1 O|INABOX09        |� ����� ���������� 3||
C |L_ixe         |1 1 O|INABOX08        |� ����� ���������� 2||
C |L_oxe         |1 1 O|INABOX07        |� ����� ���������� 1||
C |L_uxe         |1 1 O|INABOX06        |� ����� ����������||
C |L_abi         |1 1 O|INABOX05        |� ����� �������� �������||
C |L_ebi         |1 1 O|INABOX04        |� ����� �������� ������||
C |L_ibi         |1 1 O|INABOX03        |� ����� �������� ��������||
C |L_obi         |1 1 O|INABOX02        |� ����� �������� �������� ����� ���������||
C |R_upi         |4 4 K|_lcmpc1-0       |[]�������� ������ �����������|20000|
C |R_ari         |4 4 O|VY01            |��������� ���������� �� OY|0|
C |L_eri         |1 1 S|_splsi10-1*     |[TF]���������� ��������� ������������� |F|
C |R_iri         |4 4 I|FDA20TRAN10_C1_MFVAL|��������� �����||
C |L_ori         |1 1 O|TAKE10          |��������� �������� � ����������� 10||
C |L_esi         |1 1 S|_qfftr10*       |�������� ������ Q RS-��������  |F|
C |R_eti         |4 4 K|_lcmpc10-3      |[]�������� ������ �����������|100|
C |R_uti         |4 4 K|_lcmpc10-1      |[]�������� ������ �����������|100|
C |R_evi         |4 4 I|FDA20TRAN10VZ01 |���������� ������������ �� OZ||
C |R_ivi         |4 4 I|FDA20TRAN10VX01 |���������� ������������ �� OX||
C |L_ovi         |1 1 S|_splsi9-1*      |[TF]���������� ��������� ������������� |F|
C |R_uvi         |4 4 I|FDA20TRAN09_C1_MFVAL|��������� �����||
C |L_axi         |1 1 O|TAKE9           |��������� �������� � ����������� 9||
C |L_oxi         |1 1 S|_qfftr9*        |�������� ������ Q RS-��������  |F|
C |R_obo         |4 4 K|_lcmpc9-3       |[]�������� ������ �����������|100|
C |R_edo         |4 4 K|_lcmpc9-1       |[]�������� ������ �����������|100|
C |R_odo         |4 4 I|FDA20TRAN09VZ01 |���������� ������������ �� OZ||
C |R_udo         |4 4 I|FDA20TRAN09VX01 |���������� ������������ �� OX||
C |L_afo         |1 1 S|_splsi8-1*      |[TF]���������� ��������� ������������� |F|
C |R_efo         |4 4 I|FDA20TRAN08_C1_MFVAL|��������� �����||
C |L_ifo         |1 1 O|TAKE8           |��������� �������� � ����������� 8||
C |L_ako         |1 1 S|_qfftr8*        |�������� ������ Q RS-��������  |F|
C |R_alo         |4 4 K|_lcmpc8-3       |[]�������� ������ �����������|100|
C |R_olo         |4 4 K|_lcmpc8-1       |[]�������� ������ �����������|100|
C |R_amo         |4 4 I|FDA20TRAN08VZ01 |���������� ������������ �� OZ||
C |R_emo         |4 4 I|FDA20TRAN08VX01 |���������� ������������ �� OX||
C |L_imo         |1 1 S|_splsi7-1*      |[TF]���������� ��������� ������������� |F|
C |R_omo         |4 4 I|FDA20TRAN07_C1_MFVAL|��������� �����||
C |L_umo         |1 1 O|TAKE7           |��������� �������� � ����������� 7||
C |L_ipo         |1 1 S|_qfftr7*        |�������� ������ Q RS-��������  |F|
C |R_iro         |4 4 K|_lcmpc7-3       |[]�������� ������ �����������|100|
C |R_aso         |4 4 K|_lcmpc7-1       |[]�������� ������ �����������|100|
C |R_iso         |4 4 I|FDA20TRAN07VZ01 |���������� ������������ �� OZ||
C |R_oso         |4 4 I|FDA20TRAN07VX01 |���������� ������������ �� OX||
C |L_uso         |1 1 S|_splsi6-1*      |[TF]���������� ��������� ������������� |F|
C |R_ato         |4 4 I|FDA20TRAN06_C1_MFVAL|��������� �����||
C |L_eto         |1 1 O|TAKE6           |��������� �������� � ����������� 6||
C |L_uto         |1 1 S|_qfftr6*        |�������� ������ Q RS-��������  |F|
C |R_uvo         |4 4 K|_lcmpc6-3       |[]�������� ������ �����������|100|
C |R_ixo         |4 4 K|_lcmpc6-1       |[]�������� ������ �����������|100|
C |R_uxo         |4 4 I|FDA20TRAN06VZ01 |���������� ������������ �� OZ||
C |R_abu         |4 4 I|FDA20TRAN06VX01 |���������� ������������ �� OX||
C |L_ebu         |1 1 S|_splsi5-1*      |[TF]���������� ��������� ������������� |F|
C |R_ibu         |4 4 I|FDA20TRAN05_C1_MFVAL|��������� �����||
C |L_obu         |1 1 O|TAKE5           |��������� �������� � ����������� 5||
C |L_edu         |1 1 S|_qfftr5*        |�������� ������ Q RS-��������  |F|
C |R_efu         |4 4 K|_lcmpc5-3       |[]�������� ������ �����������|100|
C |R_ufu         |4 4 K|_lcmpc5-1       |[]�������� ������ �����������|100|
C |R_eku         |4 4 I|FDA20TRAN05VZ01 |���������� ������������ �� OZ||
C |R_iku         |4 4 I|FDA20TRAN05VX01 |���������� ������������ �� OX||
C |L_oku         |1 1 S|_splsi4-1*      |[TF]���������� ��������� ������������� |F|
C |R_uku         |4 4 I|FDA20TRAN04_C1_MFVAL|��������� �����||
C |L_alu         |1 1 O|TAKE4           |��������� �������� � ����������� 4||
C |L_emu         |1 1 S|_qff4*          |�������� ������ Q RS-��������  |F|
C |L_imu         |1 1 O|FULL4           |��������� ����� � ������� PU||
C |R_umu         |4 4 K|_lcmpc4-3       |[]�������� ������ �����������|100|
C |R_ipu         |4 4 K|_lcmpc4-1       |[]�������� ������ �����������|100|
C |R_aru         |4 4 K|_lcmpc4-8       |[]�������� ������ �����������|0.1|
C |R_iru         |4 4 K|_lcmpc4-7       |[]�������� ������ �����������|480|
C |R_uru         |4 4 K|_lcmpc4-6       |[]�������� ������ �����������|7950|
C |R_asu         |4 4 I|FDA20TRAN04VZ01 |���������� ������������ �� OZ||
C |R_esu         |4 4 I|FDA20TRAN04VX01 |���������� ������������ �� OX||
C |L_isu         |1 1 S|_splsi3-1*      |[TF]���������� ��������� ������������� |F|
C |R_osu         |4 4 I|FDA20TRAN03_C1_MFVAL|��������� �����||
C |L_usu         |1 1 O|TAKE3           |��������� �������� � ����������� 3||
C |L_avu         |1 1 S|_qfftr3*        |�������� ������ Q RS-��������  |F|
C |L_evu         |1 1 O|FULL3           |��������� ����� � ������� ������||
C |R_ovu         |4 4 K|_lcmpc3-3       |[]�������� ������ �����������|100|
C |R_exu         |4 4 K|_lcmpc3-1       |[]�������� ������ �����������|100|
C |R_uxu         |4 4 K|_lcmpc3-8       |[]�������� ������ �����������|0.1|
C |R_ebad        |4 4 K|_lcmpc3-7       |[]�������� ������ �����������|480|
C |R_obad        |4 4 K|_lcmpc3-6       |[]�������� ������ �����������|5950|
C |R_ubad        |4 4 I|FDA20TRAN03VZ01 |���������� ������������ �� OZ||
C |R_adad        |4 4 I|FDA20TRAN03VX01 |���������� ������������ �� OX||
C |L_edad        |1 1 S|_splsi2-1*      |[TF]���������� ��������� ������������� |F|
C |R_idad        |4 4 I|FDA20TRAN02_C1_MFVAL|��������� �����||
C |L_odad        |1 1 O|TAKE2           |��������� �������� � ����������� 2||
C |L_ufad        |1 1 S|_qfftr2*        |�������� ������ Q RS-��������  |F|
C |L_akad        |1 1 O|FULL2           |��������� ����� � ������� ��������||
C |R_ikad        |4 4 K|_lcmpc2-3       |[]�������� ������ �����������|100|
C |R_alad        |4 4 K|_lcmpc2-1       |[]�������� ������ �����������|100|
C |R_olad        |4 4 K|_lcmpc2-8       |[]�������� ������ �����������|0.1|
C |R_amad        |4 4 K|_lcmpc2-7       |[]�������� ������ �����������|480|
C |R_imad        |4 4 K|_lcmpc2-6       |[]�������� ������ �����������|3950|
C |R_apad        |4 4 I|FDA20TRAN02VZ01 |���������� ������������ �� OZ||
C |R_epad        |4 4 I|FDA20TRAN02VX01 |���������� ������������ �� OX||
C |L_ipad        |1 1 S|_splsi1-1*      |[TF]���������� ��������� ������������� |F|
C |R_opad        |4 4 I|FDA20TRAN01_C1_MFVAL|��������� �����||
C |L_upad        |1 1 O|TAKE1           |��������� �������� � ����������� 1||
C |L_asad        |1 1 S|_qfftr1*        |�������� ������ Q RS-��������  |F|
C |L_esad        |1 1 O|FULL1           |��������� ����� � ������� �������� �����||
C |R_usad        |4 4 K|_lcmpc1-3       |[]�������� ������ �����������|100|
C |R_etad        |4 4 K|_lcmpc1-1       |[]�������� ������ �����������|100|
C |R_avad        |4 4 K|_lcmpc1-8       |[]�������� ������ �����������|0.1|
C |R_ivad        |4 4 K|_lcmpc1-7       |[]�������� ������ �����������|480|
C |R_uvad        |4 4 K|_lcmpc1-6       |[]�������� ������ �����������|1950|
C |R_ixad        |4 4 O|VV01            |��������� ����� ����������||
C |R_oxad        |4 4 I|FDA20TRAN01VZ01 |���������� ������������ �� OZ||
C |R_uxad        |4 4 O|VZ01            |��������� ���������� �� OZ|`START_Z`|
C |R_abed        |4 4 I|FDA20TRAN01VX01 |���������� ������������ �� OX||
C |R_ebed        |4 4 O|VX01            |��������� ���������� �� OX|`START_X`|

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R0_e,R_i,R_o
      LOGICAL*1 L_u,L_ad,L_ed,L_id,L_od,L_ud,L_af,L_ef,L_if
     &,L_of,L_uf,L_ak,L_ek,L_ik,L_ok,L_uk,L_al,L_el,L_il,L_ol
     &,L_ul
      REAL*4 R0_am,R0_em,R0_im,R0_om,R0_um,R0_ap,R0_ep,R0_ip
     &,R0_op,R0_up,R0_ar,R0_er,R0_ir,R0_or,R0_ur,R0_as,R0_es
     &,R0_is
      LOGICAL*1 L0_os,L0_us,L_at,L_et,L_it,L_ot,L_ut,L_av
     &,L_ev,L_iv,L_ov,L_uv,L0_ax,L0_ex,L_ix
      REAL*4 R_ox,R_ux,R_abe,R_ebe
      INTEGER*4 I0_ibe
      LOGICAL*1 L_obe,L0_ube,L_ade,L_ede,L0_ide,L0_ode
      REAL*4 R_ude
      LOGICAL*1 L0_afe
      REAL*4 R_efe
      LOGICAL*1 L0_ife
      REAL*4 R_ofe
      LOGICAL*1 L_ufe
      REAL*4 R_ake,R_eke,R_ike
      LOGICAL*1 L_oke,L0_uke,L_ale,L0_ele,L_ile,L0_ole,L0_ule
      REAL*4 R_ame,R0_eme
      LOGICAL*1 L0_ime
      REAL*4 R_ome,R0_ume,R_ape,R0_epe,R_ipe,R0_ope,R_upe
     &,R0_are,R_ere,R0_ire,R0_ore,R0_ure,R_ase,R0_ese,R0_ise
     &,R_ose
      LOGICAL*1 L0_use
      REAL*4 R0_ate,R_ete
      LOGICAL*1 L0_ite
      REAL*4 R_ote,R_ute,R_ave,R_eve,R_ive,R_ove
      LOGICAL*1 L_uve,L_axe,L_exe,L_ixe,L_oxe,L_uxe,L_abi
     &,L_ebi,L_ibi,L_obi,L0_ubi
      REAL*4 R0_adi,R0_edi,R0_idi,R0_odi,R0_udi,R0_afi,R0_efi
     &,R0_ifi,R0_ofi,R0_ufi,R0_aki,R0_eki,R0_iki,R0_oki,R0_uki
     &,R0_ali,R0_eli,R0_ili,R0_oli,R0_uli,R0_ami,R0_emi
      REAL*4 R0_imi,R0_omi,R0_umi,R0_api,R0_epi,R0_ipi
      LOGICAL*1 L0_opi
      REAL*4 R_upi,R_ari
      LOGICAL*1 L_eri
      REAL*4 R_iri
      LOGICAL*1 L_ori,L0_uri,L0_asi,L_esi,L0_isi,L0_osi,L0_usi
     &,L0_ati
      REAL*4 R_eti,R0_iti
      LOGICAL*1 L0_oti
      REAL*4 R_uti,R0_avi,R_evi,R_ivi
      LOGICAL*1 L_ovi
      REAL*4 R_uvi
      LOGICAL*1 L_axi,L0_exi,L0_ixi,L_oxi,L0_uxi,L0_abo,L0_ebo
     &,L0_ibo
      REAL*4 R_obo,R0_ubo
      LOGICAL*1 L0_ado
      REAL*4 R_edo,R0_ido,R_odo,R_udo
      LOGICAL*1 L_afo
      REAL*4 R_efo
      LOGICAL*1 L_ifo,L0_ofo,L0_ufo,L_ako,L0_eko,L0_iko,L0_oko
     &,L0_uko
      REAL*4 R_alo,R0_elo
      LOGICAL*1 L0_ilo
      REAL*4 R_olo,R0_ulo,R_amo,R_emo
      LOGICAL*1 L_imo
      REAL*4 R_omo
      LOGICAL*1 L_umo,L0_apo,L0_epo,L_ipo,L0_opo,L0_upo,L0_aro
     &,L0_ero
      REAL*4 R_iro,R0_oro
      LOGICAL*1 L0_uro
      REAL*4 R_aso,R0_eso,R_iso,R_oso
      LOGICAL*1 L_uso
      REAL*4 R_ato
      LOGICAL*1 L_eto,L0_ito,L0_oto,L_uto,L0_avo,L0_evo,L0_ivo
     &,L0_ovo
      REAL*4 R_uvo,R0_axo
      LOGICAL*1 L0_exo
      REAL*4 R_ixo,R0_oxo,R_uxo,R_abu
      LOGICAL*1 L_ebu
      REAL*4 R_ibu
      LOGICAL*1 L_obu,L0_ubu,L0_adu,L_edu,L0_idu,L0_odu,L0_udu
     &,L0_afu
      REAL*4 R_efu,R0_ifu
      LOGICAL*1 L0_ofu
      REAL*4 R_ufu,R0_aku,R_eku,R_iku
      LOGICAL*1 L_oku
      REAL*4 R_uku
      LOGICAL*1 L_alu,L0_elu,L0_ilu,L0_olu,L0_ulu,L0_amu,L_emu
     &,L_imu,L0_omu
      REAL*4 R_umu,R0_apu
      LOGICAL*1 L0_epu
      REAL*4 R_ipu,R0_opu
      LOGICAL*1 L0_upu
      REAL*4 R_aru
      LOGICAL*1 L0_eru
      REAL*4 R_iru
      LOGICAL*1 L0_oru
      REAL*4 R_uru,R_asu,R_esu
      LOGICAL*1 L_isu
      REAL*4 R_osu
      LOGICAL*1 L_usu,L0_atu,L0_etu,L0_itu,L0_otu,L0_utu,L_avu
     &,L_evu,L0_ivu
      REAL*4 R_ovu,R0_uvu
      LOGICAL*1 L0_axu
      REAL*4 R_exu,R0_ixu
      LOGICAL*1 L0_oxu
      REAL*4 R_uxu
      LOGICAL*1 L0_abad
      REAL*4 R_ebad
      LOGICAL*1 L0_ibad
      REAL*4 R_obad,R_ubad,R_adad
      LOGICAL*1 L_edad
      REAL*4 R_idad
      LOGICAL*1 L_odad,L0_udad,L0_afad,L0_efad,L0_ifad,L0_ofad
     &,L_ufad,L_akad,L0_ekad
      REAL*4 R_ikad,R0_okad
      LOGICAL*1 L0_ukad
      REAL*4 R_alad,R0_elad
      LOGICAL*1 L0_ilad
      REAL*4 R_olad
      LOGICAL*1 L0_ulad
      REAL*4 R_amad
      LOGICAL*1 L0_emad
      REAL*4 R_imad,R0_omad,R0_umad,R_apad,R_epad
      LOGICAL*1 L_ipad
      REAL*4 R_opad
      LOGICAL*1 L_upad,L0_arad,L0_erad,L0_irad,L0_orad,L0_urad
     &,L_asad,L_esad,L0_isad,L0_osad
      REAL*4 R_usad
      LOGICAL*1 L0_atad
      REAL*4 R_etad,R0_itad,R0_otad
      LOGICAL*1 L0_utad
      REAL*4 R_avad
      LOGICAL*1 L0_evad
      REAL*4 R_ivad
      LOGICAL*1 L0_ovad
      REAL*4 R_uvad,R0_axad,R0_exad,R_ixad,R_oxad,R_uxad,R_abed
     &,R_ebed

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_e=R_i
C KONTEYNER_dpro.fmg( 231, 534):pre: ������������  �� T,ftime
      L0_ufo=.NOT.L_iv.and..not.L_ud
      L_ud=.NOT.L_iv
C KONTEYNER_dpro.fmg( 227, 732):������������  �� 1 ���,i8-3
      L0_iko=L_iv.and..not.L_af
      L_af=L_iv
C KONTEYNER_dpro.fmg( 227, 738):������������  �� 1 ���,i8-2
      L0_epo=.NOT.L_ev.and..not.L_ef
      L_ef=.NOT.L_ev
C KONTEYNER_dpro.fmg( 227, 772):������������  �� 1 ���,i7-3
      L0_upo=L_ev.and..not.L_if
      L_if=L_ev
C KONTEYNER_dpro.fmg( 227, 778):������������  �� 1 ���,i7-2
      L0_oto=.NOT.L_av.and..not.L_of
      L_of=.NOT.L_av
C KONTEYNER_dpro.fmg( 227, 810):������������  �� 1 ���,i6-3
      L0_evo=L_av.and..not.L_uf
      L_uf=L_av
C KONTEYNER_dpro.fmg( 227, 816):������������  �� 1 ���,i6-2
      L0_adu=.NOT.L_ut.and..not.L_ak
      L_ak=.NOT.L_ut
C KONTEYNER_dpro.fmg( 227, 848):������������  �� 1 ���,i5-3
      L0_odu=L_ut.and..not.L_ek
      L_ek=L_ut
C KONTEYNER_dpro.fmg( 227, 854):������������  �� 1 ���,i5-2
      L0_ilu=.NOT.L_ot.and..not.L_ik
      L_ik=.NOT.L_ot
C KONTEYNER_dpro.fmg(  46, 676):������������  �� 1 ���,i4-3
      L0_ulu=L_ot.and..not.L_ok
      L_ok=L_ot
C KONTEYNER_dpro.fmg(  46, 682):������������  �� 1 ���,i4-2
      L0_etu=.NOT.L_it.and..not.L_uk
      L_uk=.NOT.L_it
C KONTEYNER_dpro.fmg(  46, 730):������������  �� 1 ���,i3-3
      L0_otu=L_it.and..not.L_al
      L_al=L_it
C KONTEYNER_dpro.fmg(  46, 736):������������  �� 1 ���,i3-2
      L0_afad=.NOT.L_et.and..not.L_el
      L_el=.NOT.L_et
C KONTEYNER_dpro.fmg(  46, 784):������������  �� 1 ���,i2-3
      L0_ifad=L_et.and..not.L_il
      L_il=L_et
C KONTEYNER_dpro.fmg(  46, 790):������������  �� 1 ���,i2-2
      L0_erad=.NOT.L_at.and..not.L_ol
      L_ol=.NOT.L_at
C KONTEYNER_dpro.fmg(  46, 842):������������  �� 1 ���,i1-3
      L0_isad=L_at.and..not.L_ul
      L_ul=L_at
C KONTEYNER_dpro.fmg(  46, 848):������������  �� 1 ���,i1-2
      I0_ibe = 1
C KONTEYNER_dpro.fmg( 246, 529):��������� ������������� IN (�������)
      R0_epe = -20
C KONTEYNER_dpro.fmg(  66, 512):��������� (RE4) (�������)
      R0_ope = -20
C KONTEYNER_dpro.fmg(  56, 514):��������� (RE4) (�������)
      R0_are = 60
C KONTEYNER_dpro.fmg(  37, 518):��������� (RE4) (�������)
      R0_ise = -20
C KONTEYNER_dpro.fmg(  47, 516):��������� (RE4) (�������)
      R0_ate = 20000
C KONTEYNER_dpro.fmg(  44, 584):��������� (RE4) (�������)
      R0_adi = 200
C KONTEYNER_dpro.fmg(  44, 523):��������� (RE4) (�������)
      R0_ufi = 18000
C KONTEYNER_dpro.fmg(  44, 528):��������� (RE4) (�������)
      R0_eki = 16000
C KONTEYNER_dpro.fmg(  44, 534):��������� (RE4) (�������)
      R0_oki = 14000
C KONTEYNER_dpro.fmg(  44, 540):��������� (RE4) (�������)
      R0_ali = 12000
C KONTEYNER_dpro.fmg(  44, 546):��������� (RE4) (�������)
      R0_ili = 10000
C KONTEYNER_dpro.fmg(  44, 552):��������� (RE4) (�������)
      R0_uli = 8000
C KONTEYNER_dpro.fmg(  44, 558):��������� (RE4) (�������)
      R0_emi = 6000
C KONTEYNER_dpro.fmg(  44, 564):��������� (RE4) (�������)
      R0_omi = 4000
C KONTEYNER_dpro.fmg(  44, 570):��������� (RE4) (�������)
      R0_epi = 2000
C KONTEYNER_dpro.fmg(  44, 576):��������� (RE4) (�������)
      R0_exad = R_ebed + (-R_abed)
C KONTEYNER_dpro.fmg(  30, 864):��������
C label 61  try61=try61-1
      if(L_ale) then
         R_ari=R_ike
      endif
C KONTEYNER_dpro.fmg( 290, 602):���� � ������������� �������
      R0_em = R_ari + (-R_ike)
C KONTEYNER_dpro.fmg( 212, 607):��������
      R0_ume=abs(R0_em)
C KONTEYNER_dpro.fmg( 220, 607):���������� ��������
      L0_ime=R0_ume.lt.R_ome
C KONTEYNER_dpro.fmg( 228, 607):���������� <,c11-1
      L0_us = L_ov.AND.(.NOT.L_oke)
C KONTEYNER_dpro.fmg( 212, 694):�
      L0_ixi=.NOT.L0_us.and..not.L_id
      L_id=.NOT.L0_us
C KONTEYNER_dpro.fmg( 227, 694):������������  �� 1 ���,i9-3
      if(L_avu) then
         R_ebed=R_adad
      endif
C KONTEYNER_dpro.fmg( 110, 748):���� � ������������� �������
      R0_otad=abs(R0_exad)
C KONTEYNER_dpro.fmg(  38, 864):���������� ��������
      L0_opi=R0_otad.gt.R_upi
C KONTEYNER_dpro.fmg(  47, 870):���������� >,c1-0
      L0_atad=R0_otad.lt.R_etad
C KONTEYNER_dpro.fmg(  47, 864):���������� <,c1-1
      L0_orad = L0_opi.OR.L0_atad
C KONTEYNER_dpro.fmg(  54, 867):���
      if(L_asad) then
         R_uxad=R_oxad
      endif
C KONTEYNER_dpro.fmg( 104, 852):���� � ������������� �������
      if(L_ufad) then
         R_ebed=R_epad
      endif
C KONTEYNER_dpro.fmg( 110, 802):���� � ������������� �������
      R0_umad = R_ebed + (-R_epad)
C KONTEYNER_dpro.fmg(  30, 805):��������
      R0_elad=abs(R0_umad)
C KONTEYNER_dpro.fmg(  38, 805):���������� ��������
      L0_ukad=R0_elad.lt.R_alad
C KONTEYNER_dpro.fmg(  47, 805):���������� <,c2-1
      if(L_ale) then
         R_uxad=R_eke
      endif
C KONTEYNER_dpro.fmg( 290, 594):���� � ������������� �������
      if(L_esi) then
         R_uxad=R_evi
      endif
C KONTEYNER_dpro.fmg( 290, 662):���� � ������������� �������
      if(L_oxi) then
         R_uxad=R_odo
      endif
C KONTEYNER_dpro.fmg( 290, 702):���� � ������������� �������
      if(L_ako) then
         R_uxad=R_amo
      endif
C KONTEYNER_dpro.fmg( 290, 741):���� � ������������� �������
      if(L_ipo) then
         R_uxad=R_iso
      endif
C KONTEYNER_dpro.fmg( 290, 780):���� � ������������� �������
      if(L_uto) then
         R_uxad=R_uxo
      endif
C KONTEYNER_dpro.fmg( 290, 818):���� � ������������� �������
      if(L_edu) then
         R_uxad=R_eku
      endif
C KONTEYNER_dpro.fmg( 290, 858):���� � ������������� �������
      if(L_emu) then
         R_uxad=R_asu
      endif
C KONTEYNER_dpro.fmg( 110, 685):���� � ������������� �������
      R0_omad = R_uxad + (-R_apad)
C KONTEYNER_dpro.fmg(  30, 797):��������
      R0_okad=abs(R0_omad)
C KONTEYNER_dpro.fmg(  38, 797):���������� ��������
      L0_ekad=R0_okad.lt.R_ikad
C KONTEYNER_dpro.fmg(  47, 797):���������� <,c2-3
      L0_ofad = L0_ukad.AND.L0_ekad.AND.L0_ifad
C KONTEYNER_dpro.fmg(  56, 797):�
      L0_udad=L0_ofad.and..not.L_edad
      L_edad=L0_ofad
C KONTEYNER_dpro.fmg(  62, 797):������������  �� 1 ���,i2-1
      L_ufad=L0_udad.or.(L_ufad.and..not.(L0_afad))
      L0_efad=.not.L_ufad
C KONTEYNER_dpro.fmg(  70, 795):RS �������,tr2
      if(L_ufad) then
         R_uxad=R_apad
      endif
C KONTEYNER_dpro.fmg( 110, 793):���� � ������������� �������
      R0_axad = R_uxad + (-R_oxad)
C KONTEYNER_dpro.fmg(  30, 856):��������
      R0_itad=abs(R0_axad)
C KONTEYNER_dpro.fmg(  38, 856):���������� ��������
      L0_osad=R0_itad.lt.R_usad
C KONTEYNER_dpro.fmg(  47, 856):���������� <,c1-3
      L0_urad = L0_orad.AND.L0_osad.AND.L0_isad
C KONTEYNER_dpro.fmg(  56, 856):�
      L0_arad=L0_urad.and..not.L_ipad
      L_ipad=L0_urad
C KONTEYNER_dpro.fmg(  62, 856):������������  �� 1 ���,i1-1
      L_asad=L0_arad.or.(L_asad.and..not.(L0_erad))
      L0_irad=.not.L_asad
C KONTEYNER_dpro.fmg(  70, 854):RS �������,tr1
      if(L_asad) then
         R_ebed=R_abed
      endif
C KONTEYNER_dpro.fmg( 104, 860):���� � ������������� �������
      R0_as = R_ebed + (-R_esu)
C KONTEYNER_dpro.fmg(  30, 697):��������
      R0_opu=abs(R0_as)
C KONTEYNER_dpro.fmg(  38, 697):���������� ��������
      L0_epu=R0_opu.lt.R_ipu
C KONTEYNER_dpro.fmg(  47, 697):���������� <,c4-1
      R0_ur = R_uxad + (-R_asu)
C KONTEYNER_dpro.fmg(  30, 689):��������
      R0_apu=abs(R0_ur)
C KONTEYNER_dpro.fmg(  38, 689):���������� ��������
      L0_omu=R0_apu.lt.R_umu
C KONTEYNER_dpro.fmg(  47, 689):���������� <,c4-3
      L0_amu = L0_epu.AND.L0_omu.AND.L0_ulu
C KONTEYNER_dpro.fmg(  56, 689):�
      L0_elu=L0_amu.and..not.L_oku
      L_oku=L0_amu
C KONTEYNER_dpro.fmg(  62, 689):������������  �� 1 ���,i4-1
      L_emu=L0_elu.or.(L_emu.and..not.(L0_ilu))
      L0_olu=.not.L_emu
C KONTEYNER_dpro.fmg(  70, 687):RS �������,4
      if(L_emu) then
         R_ebed=R_esu
      endif
C KONTEYNER_dpro.fmg( 110, 694):���� � ������������� �������
      R0_or = R_ebed + (-R_iku)
C KONTEYNER_dpro.fmg( 212, 870):��������
      R0_aku=abs(R0_or)
C KONTEYNER_dpro.fmg( 220, 870):���������� ��������
      L0_ofu=R0_aku.lt.R_ufu
C KONTEYNER_dpro.fmg( 228, 870):���������� <,c5-1
      R0_ir = R_uxad + (-R_eku)
C KONTEYNER_dpro.fmg( 212, 862):��������
      R0_ifu=abs(R0_ir)
C KONTEYNER_dpro.fmg( 220, 862):���������� ��������
      L0_afu=R0_ifu.lt.R_efu
C KONTEYNER_dpro.fmg( 228, 862):���������� <,c5-3
      L0_udu = L0_ofu.AND.L0_afu.AND.L0_odu
C KONTEYNER_dpro.fmg( 237, 862):�
      L0_ubu=L0_udu.and..not.L_ebu
      L_ebu=L0_udu
C KONTEYNER_dpro.fmg( 243, 862):������������  �� 1 ���,i5-1
      L_edu=L0_ubu.or.(L_edu.and..not.(L0_adu))
      L0_idu=.not.L_edu
C KONTEYNER_dpro.fmg( 251, 860):RS �������,tr5
      if(L_edu) then
         R_ebed=R_iku
      endif
C KONTEYNER_dpro.fmg( 290, 866):���� � ������������� �������
      R0_er = R_ebed + (-R_abu)
C KONTEYNER_dpro.fmg( 212, 830):��������
      R0_oxo=abs(R0_er)
C KONTEYNER_dpro.fmg( 220, 830):���������� ��������
      L0_exo=R0_oxo.lt.R_ixo
C KONTEYNER_dpro.fmg( 228, 830):���������� <,c6-1
      R0_ar = R_uxad + (-R_uxo)
C KONTEYNER_dpro.fmg( 212, 822):��������
      R0_axo=abs(R0_ar)
C KONTEYNER_dpro.fmg( 220, 822):���������� ��������
      L0_ovo=R0_axo.lt.R_uvo
C KONTEYNER_dpro.fmg( 228, 822):���������� <,c6-3
      L0_ivo = L0_exo.AND.L0_ovo.AND.L0_evo
C KONTEYNER_dpro.fmg( 237, 822):�
      L0_ito=L0_ivo.and..not.L_uso
      L_uso=L0_ivo
C KONTEYNER_dpro.fmg( 243, 822):������������  �� 1 ���,i6-1
      L_uto=L0_ito.or.(L_uto.and..not.(L0_oto))
      L0_avo=.not.L_uto
C KONTEYNER_dpro.fmg( 251, 820):RS �������,tr6
      if(L_uto) then
         R_ebed=R_abu
      endif
C KONTEYNER_dpro.fmg( 290, 827):���� � ������������� �������
      R0_up = R_ebed + (-R_oso)
C KONTEYNER_dpro.fmg( 212, 792):��������
      R0_eso=abs(R0_up)
C KONTEYNER_dpro.fmg( 220, 792):���������� ��������
      L0_uro=R0_eso.lt.R_aso
C KONTEYNER_dpro.fmg( 228, 792):���������� <,c7-1
      R0_op = R_uxad + (-R_iso)
C KONTEYNER_dpro.fmg( 212, 784):��������
      R0_oro=abs(R0_op)
C KONTEYNER_dpro.fmg( 220, 784):���������� ��������
      L0_ero=R0_oro.lt.R_iro
C KONTEYNER_dpro.fmg( 228, 784):���������� <,c7-3
      L0_aro = L0_uro.AND.L0_ero.AND.L0_upo
C KONTEYNER_dpro.fmg( 237, 784):�
      L0_apo=L0_aro.and..not.L_imo
      L_imo=L0_aro
C KONTEYNER_dpro.fmg( 243, 784):������������  �� 1 ���,i7-1
      L_ipo=L0_apo.or.(L_ipo.and..not.(L0_epo))
      L0_opo=.not.L_ipo
C KONTEYNER_dpro.fmg( 251, 782):RS �������,tr7
      if(L_ipo) then
         R_ebed=R_oso
      endif
C KONTEYNER_dpro.fmg( 290, 789):���� � ������������� �������
      R0_ip = R_ebed + (-R_emo)
C KONTEYNER_dpro.fmg( 212, 753):��������
      R0_ulo=abs(R0_ip)
C KONTEYNER_dpro.fmg( 220, 753):���������� ��������
      L0_ilo=R0_ulo.lt.R_olo
C KONTEYNER_dpro.fmg( 228, 753):���������� <,c8-1
      R0_ep = R_uxad + (-R_amo)
C KONTEYNER_dpro.fmg( 212, 745):��������
      R0_elo=abs(R0_ep)
C KONTEYNER_dpro.fmg( 220, 745):���������� ��������
      L0_uko=R0_elo.lt.R_alo
C KONTEYNER_dpro.fmg( 228, 745):���������� <,c8-3
      L0_oko = L0_ilo.AND.L0_uko.AND.L0_iko
C KONTEYNER_dpro.fmg( 237, 745):�
      L0_ofo=L0_oko.and..not.L_afo
      L_afo=L0_oko
C KONTEYNER_dpro.fmg( 243, 745):������������  �� 1 ���,i8-1
      L_ako=L0_ofo.or.(L_ako.and..not.(L0_ufo))
      L0_eko=.not.L_ako
C KONTEYNER_dpro.fmg( 251, 743):RS �������,tr8
      if(L_ako) then
         R_ebed=R_emo
      endif
C KONTEYNER_dpro.fmg( 290, 750):���� � ������������� �������
      R0_ap = R_ebed + (-R_udo)
C KONTEYNER_dpro.fmg( 212, 714):��������
      R0_ido=abs(R0_ap)
C KONTEYNER_dpro.fmg( 220, 714):���������� ��������
      L0_ado=R0_ido.lt.R_edo
C KONTEYNER_dpro.fmg( 228, 714):���������� <,c9-1
      R0_um = R_uxad + (-R_odo)
C KONTEYNER_dpro.fmg( 212, 706):��������
      R0_ubo=abs(R0_um)
C KONTEYNER_dpro.fmg( 220, 706):���������� ��������
      L0_ibo=R0_ubo.lt.R_obo
C KONTEYNER_dpro.fmg( 228, 706):���������� <,c9-3
      L0_abo=L0_us.and..not.L_od
      L_od=L0_us
C KONTEYNER_dpro.fmg( 227, 700):������������  �� 1 ���,i9-2
      L0_ebo = L0_ado.AND.L0_ibo.AND.L0_abo
C KONTEYNER_dpro.fmg( 237, 706):�
      L0_exi=L0_ebo.and..not.L_ovi
      L_ovi=L0_ebo
C KONTEYNER_dpro.fmg( 243, 706):������������  �� 1 ���,i9-1
      L_oxi=L0_exi.or.(L_oxi.and..not.(L0_ixi))
      L0_uxi=.not.L_oxi
C KONTEYNER_dpro.fmg( 251, 704):RS �������,tr9
      if(L_oxi) then
         R_ebed=R_udo
      endif
C KONTEYNER_dpro.fmg( 290, 711):���� � ������������� �������
      R0_is = R_ebed + (-R_adad)
C KONTEYNER_dpro.fmg(  30, 752):��������
      R0_ixu=abs(R0_is)
C KONTEYNER_dpro.fmg(  38, 752):���������� ��������
      L0_axu=R0_ixu.lt.R_exu
C KONTEYNER_dpro.fmg(  47, 752):���������� <,c3-1
      R0_es = R_uxad + (-R_ubad)
C KONTEYNER_dpro.fmg(  30, 744):��������
      R0_uvu=abs(R0_es)
C KONTEYNER_dpro.fmg(  38, 744):���������� ��������
      L0_ivu=R0_uvu.lt.R_ovu
C KONTEYNER_dpro.fmg(  47, 744):���������� <,c3-3
      L0_utu = L0_axu.AND.L0_ivu.AND.L0_otu
C KONTEYNER_dpro.fmg(  56, 744):�
      L0_atu=L0_utu.and..not.L_isu
      L_isu=L0_utu
C KONTEYNER_dpro.fmg(  62, 744):������������  �� 1 ���,i3-1
      L_avu=L0_atu.or.(L_avu.and..not.(L0_etu))
      L0_itu=.not.L_avu
C KONTEYNER_dpro.fmg(  70, 742):RS �������,tr3
      if(L_avu) then
         R_uxad=R_ubad
      endif
C KONTEYNER_dpro.fmg( 110, 740):���� � ������������� �������
C sav1=R0_ir
      R0_ir = R_uxad + (-R_eku)
C KONTEYNER_dpro.fmg( 212, 862):recalc:��������
C if(sav1.ne.R0_ir .and. try317.gt.0) goto 317
C sav1=R0_ep
      R0_ep = R_uxad + (-R_amo)
C KONTEYNER_dpro.fmg( 212, 745):recalc:��������
C if(sav1.ne.R0_ep .and. try386.gt.0) goto 386
C sav1=R0_es
      R0_es = R_uxad + (-R_ubad)
C KONTEYNER_dpro.fmg(  30, 744):recalc:��������
C if(sav1.ne.R0_es .and. try434.gt.0) goto 434
C sav1=R0_op
      R0_op = R_uxad + (-R_iso)
C KONTEYNER_dpro.fmg( 212, 784):recalc:��������
C if(sav1.ne.R0_op .and. try363.gt.0) goto 363
C sav1=R0_ur
      R0_ur = R_uxad + (-R_asu)
C KONTEYNER_dpro.fmg(  30, 689):recalc:��������
C if(sav1.ne.R0_ur .and. try293.gt.0) goto 293
C sav1=R0_ar
      R0_ar = R_uxad + (-R_uxo)
C KONTEYNER_dpro.fmg( 212, 822):recalc:��������
C if(sav1.ne.R0_ar .and. try340.gt.0) goto 340
C sav1=R0_um
      R0_um = R_uxad + (-R_odo)
C KONTEYNER_dpro.fmg( 212, 706):recalc:��������
C if(sav1.ne.R0_um .and. try409.gt.0) goto 409
C sav1=R0_omad
      R0_omad = R_uxad + (-R_apad)
C KONTEYNER_dpro.fmg(  30, 797):recalc:��������
C if(sav1.ne.R0_omad .and. try251.gt.0) goto 251
C sav1=R0_axad
      R0_axad = R_uxad + (-R_oxad)
C KONTEYNER_dpro.fmg(  30, 856):recalc:��������
C if(sav1.ne.R0_axad .and. try269.gt.0) goto 269
      R0_am = R_uxad + (-R_eke)
C KONTEYNER_dpro.fmg( 212, 599):��������
      R0_eme=abs(R0_am)
C KONTEYNER_dpro.fmg( 220, 599):���������� ��������
      L0_ule=R0_eme.lt.R_ame
C KONTEYNER_dpro.fmg( 228, 599):���������� <,c11-3
      R0_aki = R_ebed + (-R0_ufi)
C KONTEYNER_dpro.fmg(  47, 529):��������
      R0_edi=abs(R0_aki)
C KONTEYNER_dpro.fmg(  56, 529):���������� ��������
      L_axe=R0_edi.lt.R0_adi
C KONTEYNER_dpro.fmg(  67, 528):���������� <
      L0_ole = L0_ime.AND.L0_ule.AND.L_axe.AND.L_ile
C KONTEYNER_dpro.fmg( 237, 598):�
      L0_uke=L0_ole.and..not.L_ufe
      L_ufe=L0_ole
C KONTEYNER_dpro.fmg( 243, 598):������������  �� 1 ���,i11-1
      L_ale=L0_uke.or.(L_ale.and..not.(.NOT.L_ile))
      L0_ele=.not.L_ale
C KONTEYNER_dpro.fmg( 251, 596):RS �������,tr11
      L_oke=L_ale
C KONTEYNER_dpro.fmg( 280, 608):������,TAKE11
C sav1=L0_us
      L0_us = L_ov.AND.(.NOT.L_oke)
C KONTEYNER_dpro.fmg( 212, 694):recalc:�
C if(sav1.ne.L0_us .and. try76.gt.0) goto 76
      L0_os = L_uv.AND.(.NOT.L_oke)
C KONTEYNER_dpro.fmg( 212, 653):�
      L0_asi=.NOT.L0_os.and..not.L_ad
      L_ad=.NOT.L0_os
C KONTEYNER_dpro.fmg( 227, 653):������������  �� 1 ���,i10-3
      R0_om = R_ebed + (-R_ivi)
C KONTEYNER_dpro.fmg( 212, 674):��������
      R0_avi=abs(R0_om)
C KONTEYNER_dpro.fmg( 220, 674):���������� ��������
      L0_oti=R0_avi.lt.R_uti
C KONTEYNER_dpro.fmg( 228, 674):���������� <,c10-1
      R0_im = R_uxad + (-R_evi)
C KONTEYNER_dpro.fmg( 212, 666):��������
      R0_iti=abs(R0_im)
C KONTEYNER_dpro.fmg( 220, 666):���������� ��������
      L0_ati=R0_iti.lt.R_eti
C KONTEYNER_dpro.fmg( 228, 666):���������� <,c10-3
      L0_osi=L0_os.and..not.L_ed
      L_ed=L0_os
C KONTEYNER_dpro.fmg( 227, 659):������������  �� 1 ���,i10-2
      L0_usi = L0_oti.AND.L0_ati.AND.L0_osi
C KONTEYNER_dpro.fmg( 237, 666):�
      L0_uri=L0_usi.and..not.L_eri
      L_eri=L0_usi
C KONTEYNER_dpro.fmg( 243, 666):������������  �� 1 ���,i10-1
      L_esi=L0_uri.or.(L_esi.and..not.(L0_asi))
      L0_isi=.not.L_esi
C KONTEYNER_dpro.fmg( 251, 664):RS �������,tr10
      if(L_esi) then
         R_ebed=R_ivi
      endif
C KONTEYNER_dpro.fmg( 290, 670):���� � ������������� �������
C sav1=R0_om
      R0_om = R_ebed + (-R_ivi)
C KONTEYNER_dpro.fmg( 212, 674):recalc:��������
C if(sav1.ne.R0_om .and. try489.gt.0) goto 489
C sav1=R0_ap
      R0_ap = R_ebed + (-R_udo)
C KONTEYNER_dpro.fmg( 212, 714):recalc:��������
C if(sav1.ne.R0_ap .and. try403.gt.0) goto 403
C sav1=R0_ip
      R0_ip = R_ebed + (-R_emo)
C KONTEYNER_dpro.fmg( 212, 753):recalc:��������
C if(sav1.ne.R0_ip .and. try380.gt.0) goto 380
C sav1=R0_up
      R0_up = R_ebed + (-R_oso)
C KONTEYNER_dpro.fmg( 212, 792):recalc:��������
C if(sav1.ne.R0_up .and. try357.gt.0) goto 357
C sav1=R0_aki
      R0_aki = R_ebed + (-R0_ufi)
C KONTEYNER_dpro.fmg(  47, 529):recalc:��������
C if(sav1.ne.R0_aki .and. try458.gt.0) goto 458
C sav1=R0_er
      R0_er = R_ebed + (-R_abu)
C KONTEYNER_dpro.fmg( 212, 830):recalc:��������
C if(sav1.ne.R0_er .and. try334.gt.0) goto 334
C sav1=R0_or
      R0_or = R_ebed + (-R_iku)
C KONTEYNER_dpro.fmg( 212, 870):recalc:��������
C if(sav1.ne.R0_or .and. try311.gt.0) goto 311
C sav1=R0_as
      R0_as = R_ebed + (-R_esu)
C KONTEYNER_dpro.fmg(  30, 697):recalc:��������
C if(sav1.ne.R0_as .and. try287.gt.0) goto 287
C sav1=R0_is
      R0_is = R_ebed + (-R_adad)
C KONTEYNER_dpro.fmg(  30, 752):recalc:��������
C if(sav1.ne.R0_is .and. try428.gt.0) goto 428
C sav1=R0_umad
      R0_umad = R_ebed + (-R_epad)
C KONTEYNER_dpro.fmg(  30, 805):recalc:��������
C if(sav1.ne.R0_umad .and. try237.gt.0) goto 237
C sav1=R0_exad
      R0_exad = R_ebed + (-R_abed)
C KONTEYNER_dpro.fmg(  30, 864):recalc:��������
C if(sav1.ne.R0_exad .and. try61.gt.0) goto 61
      L0_ovad=R_ebed.gt.R_uvad
C KONTEYNER_dpro.fmg(  47, 836):���������� >,c1-6
      L0_emad=R_ebed.gt.R_imad
C KONTEYNER_dpro.fmg(  47, 778):���������� >,c2-6
      L0_ibad=R_ebed.gt.R_obad
C KONTEYNER_dpro.fmg(  47, 724):���������� >,c3-6
      L0_oru=R_ebed.gt.R_uru
C KONTEYNER_dpro.fmg(  47, 670):���������� >,c4-6
      L0_ubi=R_ebed.lt.R0_adi
C KONTEYNER_dpro.fmg(  67, 582):���������� <
      L0_use=R_ebed.gt.R0_ate
C KONTEYNER_dpro.fmg(  67, 586):���������� >
      L_uve = L0_use.OR.L0_ubi
C KONTEYNER_dpro.fmg(  74, 583):���
      R0_ipi = R_ebed + (-R0_epi)
C KONTEYNER_dpro.fmg(  47, 577):��������
      R0_api=abs(R0_ipi)
C KONTEYNER_dpro.fmg(  56, 577):���������� ��������
      L_obi=R0_api.lt.R0_adi
C KONTEYNER_dpro.fmg(  67, 576):���������� <
      R0_umi = R_ebed + (-R0_omi)
C KONTEYNER_dpro.fmg(  47, 571):��������
      R0_ofi=abs(R0_umi)
C KONTEYNER_dpro.fmg(  56, 571):���������� ��������
      L_ibi=R0_ofi.lt.R0_adi
C KONTEYNER_dpro.fmg(  67, 570):���������� <
      R0_imi = R_ebed + (-R0_emi)
C KONTEYNER_dpro.fmg(  47, 565):��������
      R0_ifi=abs(R0_imi)
C KONTEYNER_dpro.fmg(  56, 565):���������� ��������
      L_ebi=R0_ifi.lt.R0_adi
C KONTEYNER_dpro.fmg(  67, 564):���������� <
      R0_ami = R_ebed + (-R0_uli)
C KONTEYNER_dpro.fmg(  47, 559):��������
      R0_efi=abs(R0_ami)
C KONTEYNER_dpro.fmg(  56, 559):���������� ��������
      L_abi=R0_efi.lt.R0_adi
C KONTEYNER_dpro.fmg(  67, 558):���������� <
      L0_ite = L_uve.OR.L_abi
C KONTEYNER_dpro.fmg(  62, 472):���
      R0_oli = R_ebed + (-R0_ili)
C KONTEYNER_dpro.fmg(  47, 553):��������
      R0_afi=abs(R0_oli)
C KONTEYNER_dpro.fmg(  56, 553):���������� ��������
      L_uxe=R0_afi.lt.R0_adi
C KONTEYNER_dpro.fmg(  67, 552):���������� <
      if(L_uxe) then
         R_upe=R0_are
      endif
      R0_ure=R_upe
C KONTEYNER_dpro.fmg(  41, 518):�������-��������,add_t1
      R0_eli = R_ebed + (-R0_ali)
C KONTEYNER_dpro.fmg(  47, 547):��������
      R0_udi=abs(R0_eli)
C KONTEYNER_dpro.fmg(  56, 547):���������� ��������
      L_oxe=R0_udi.lt.R0_adi
C KONTEYNER_dpro.fmg(  67, 546):���������� <
      if(L_oxe) then
         R_ase=R0_ise
      endif
      R0_ese=R_ase
C KONTEYNER_dpro.fmg(  51, 516):�������-��������,add_t2
      R0_uki = R_ebed + (-R0_oki)
C KONTEYNER_dpro.fmg(  47, 541):��������
      R0_odi=abs(R0_uki)
C KONTEYNER_dpro.fmg(  56, 541):���������� ��������
      L_ixe=R0_odi.lt.R0_adi
C KONTEYNER_dpro.fmg(  67, 540):���������� <
      if(L_ixe) then
         R_ipe=R0_ope
      endif
      R0_ore=R_ipe
C KONTEYNER_dpro.fmg(  60, 514):�������-��������,add_t3
      R0_iki = R_ebed + (-R0_eki)
C KONTEYNER_dpro.fmg(  47, 535):��������
      R0_idi=abs(R0_iki)
C KONTEYNER_dpro.fmg(  56, 535):���������� ��������
      L_exe=R0_idi.lt.R0_adi
C KONTEYNER_dpro.fmg(  67, 534):���������� <
      if(L_exe) then
         R_ape=R0_epe
      endif
      R0_ire=R_ape
C KONTEYNER_dpro.fmg(  70, 512):�������-��������,add_t4
      R_ose = R0_ure + R0_ese + R0_ore + R0_ire + R_ere
C KONTEYNER_dpro.fmg(  90, 515):��������
      L_ori=L_esi
C KONTEYNER_dpro.fmg( 280, 676):������,TAKE10
      if(L_esi) then
         R_ixad=R_iri
      endif
C KONTEYNER_dpro.fmg( 290, 654):���� � ������������� �������
      if(L_ale) then
         R_ixad=R_ake
      endif
C KONTEYNER_dpro.fmg( 290, 586):���� � ������������� �������
      L0_evad=R_uxad.gt.R_ivad
C KONTEYNER_dpro.fmg(  47, 830):���������� >,c1-7
      L0_eru=R_uxad.gt.R_iru
C KONTEYNER_dpro.fmg(  47, 664):���������� >,c4-7
      L0_ulad=R_uxad.gt.R_amad
C KONTEYNER_dpro.fmg(  47, 772):���������� >,c2-7
      L0_afe=R_uxad.gt.R_efe
C KONTEYNER_dpro.fmg( 224, 568):���������� >,c11-7
      L0_abad=R_uxad.gt.R_ebad
C KONTEYNER_dpro.fmg(  47, 718):���������� >,c3-7
      L_usu=L_avu
C KONTEYNER_dpro.fmg( 100, 754):������,TAKE3
      if(L_avu) then
         R_ixad=R_osu
      endif
C KONTEYNER_dpro.fmg( 110, 732):���� � ������������� �������
      L_axi=L_oxi
C KONTEYNER_dpro.fmg( 280, 716):������,TAKE9
      if(L_oxi) then
         R_ixad=R_uvi
      endif
C KONTEYNER_dpro.fmg( 290, 694):���� � ������������� �������
      L_ifo=L_ako
C KONTEYNER_dpro.fmg( 280, 755):������,TAKE8
      if(L_ako) then
         R_ixad=R_efo
      endif
C KONTEYNER_dpro.fmg( 290, 733):���� � ������������� �������
      L_umo=L_ipo
C KONTEYNER_dpro.fmg( 280, 794):������,TAKE7
      if(L_ipo) then
         R_ixad=R_omo
      endif
C KONTEYNER_dpro.fmg( 290, 772):���� � ������������� �������
      L_eto=L_uto
C KONTEYNER_dpro.fmg( 280, 832):������,TAKE6
      if(L_uto) then
         R_ixad=R_ato
      endif
C KONTEYNER_dpro.fmg( 290, 810):���� � ������������� �������
      L_obu=L_edu
C KONTEYNER_dpro.fmg( 280, 872):������,TAKE5
      if(L_edu) then
         R_ixad=R_ibu
      endif
C KONTEYNER_dpro.fmg( 290, 850):���� � ������������� �������
      L_alu=L_emu
C KONTEYNER_dpro.fmg( 100, 699):������,TAKE4
      if(L_emu) then
         R_ixad=R_uku
      endif
C KONTEYNER_dpro.fmg( 110, 677):���� � ������������� �������
      L_upad=L_asad
C KONTEYNER_dpro.fmg(  95, 866):������,TAKE1
      if(L_asad) then
         R_ixad=R_opad
      endif
C KONTEYNER_dpro.fmg( 104, 844):���� � ������������� �������
      L_odad=L_ufad
C KONTEYNER_dpro.fmg( 100, 807):������,TAKE2
      if(L_ufad) then
         R_ixad=R_idad
      endif
C KONTEYNER_dpro.fmg( 110, 784):���� � ������������� �������
      L0_utad=R_ixad.gt.R_avad
C KONTEYNER_dpro.fmg(  47, 824):���������� >,c1-8
      L_esad = L_asad.AND.L0_ovad.AND.L0_evad.AND.L0_utad
C KONTEYNER_dpro.fmg(  78, 831):�
      L0_upu=R_ixad.gt.R_aru
C KONTEYNER_dpro.fmg(  47, 658):���������� >,c4-8
      L_imu = L_emu.AND.L0_oru.AND.L0_eru.AND.L0_upu
C KONTEYNER_dpro.fmg(  78, 664):�
      L0_ode=R_ixad.gt.R_ude
C KONTEYNER_dpro.fmg( 224, 562):���������� >,c11-8
      L0_ilad=R_ixad.gt.R_olad
C KONTEYNER_dpro.fmg(  47, 766):���������� >,c2-8
      L_akad = L_ufad.AND.L0_emad.AND.L0_ulad.AND.L0_ilad
C KONTEYNER_dpro.fmg(  78, 772):�
      L0_oxu=R_ixad.gt.R_uxu
C KONTEYNER_dpro.fmg(  47, 712):���������� >,c3-8
      L_evu = L_avu.AND.L0_ibad.AND.L0_abad.AND.L0_oxu
C KONTEYNER_dpro.fmg(  78, 720):�
      L0_ax = L_esad.OR.L_akad.OR.L_evu.OR.L_imu
C KONTEYNER_dpro.fmg( 215, 528):���
      L0_ex = L_ix.AND.L0_ax
C KONTEYNER_dpro.fmg( 222, 534):�
      if(L0_ex.and..not.L_u) then
         R_i=R_o
      else
         R_i=max(R0_e-deltat,0.0)
      endif
      L0_ube=R_i.gt.0.0
      L_u=L0_ex
C KONTEYNER_dpro.fmg( 231, 534):������������  �� T,ftime
      L0_ife=R_ari.gt.R_ofe
C KONTEYNER_dpro.fmg( 224, 574):���������� >,c11-6
      L0_ide = L_ale.AND.L0_ife.AND.L0_afe.AND.L0_ode.AND.L_ede
C KONTEYNER_dpro.fmg( 259, 568):�
      R_ove=R_ove
C KONTEYNER_dpro.fmg( 268, 514):������,R1VW01
C label 733  try733=try733-1
C sav1=R_ove
C KONTEYNER_dpro.fmg( 268, 514):recalc:������,R1VW01
C if(sav1.ne.R_ove .and. try733.gt.0) goto 733
      if(L_uve) then
         R_ute=R_ove
      endif
      R_ive=R_ute
C KONTEYNER_dpro.fmg(  70, 485):�������-��������,wtng_c
      if(L_uve) then
         R_ete=R_ove
      endif
      R_ave=R_ete
C KONTEYNER_dpro.fmg(  70, 468):�������-��������,wtng_u
      if(L0_ite) then
         R_ote=R_ove
      endif
      R_eve=R_ote
C KONTEYNER_dpro.fmg(  70, 476):�������-��������,wtng_f
      if(I0_ibe.ne.0) then
        iv1=I0_ibe
        if(0) then
          if(L0_ube.and..not.L_obe) then
            R_abe=R_ux
            R_ove=R_ove+1
          elseif(L0_ide.and..not.L_ade) then
            R_abe=R_ux
            R_ove=R_ove-1
          elseif(L0_ube) then
            R_abe=R_abe-deltat
            if(R_abe.lt.0.0) then
              R_abe=R_ux
              R_ove=R_ove+1
            endif
          elseif(L0_ide) then
            R_abe=R_abe-deltat
            if(R_abe.lt.0.0) then
              R_abe=R_ux
              R_ove=R_ove-1
            endif
          endif
          L_obe=L0_ube
          L_ade=L0_ide
        else
            R_abe=R_ox-R_ebe
            if(L0_ube.and..not.L0_ide) then
               R_ove=R_ove+R_abe*deltat/max(deltat,R_ux)
            elseif(L0_ide.and..not.L0_ube) then
               R_ove=R_ove-R_abe*deltat/max(deltat,R_ux)
            endif   
         endif
         if(0) then
            if(R_ove.gt.R_ox) then
               R_ove=R_ebe
            elseif(R_ove.lt.R_ebe) then
               R_ove=R_ox
            endif
         else
            if(R_ove.gt.R_ox) then
               R_ove=R_ox
            elseif(R_ove.lt.R_ebe) then
               R_ove=R_ebe
            endif
         endif
      endif
C KONTEYNER_dpro.fmg( 245, 538):��������� ���������,weight
      End
