      Interface
      Subroutine TIAGA_HANDLER(ext_deltat,R_ati,R8_ofi,I_u
     &,I_id,R_od,R_ud,R_af,R_ef,I_if,I_ak,I_al,I_ol,C20_om
     &,C20_op,C8_or,R_it,R_ot,L_ut,R_av,R_ev,I_iv,R_ex,R_ox
     &,I_ux,I_ibe,I_ade,L_ode,L_afe,L_efe,L_ofe,L_ake,L_ike
     &,L_oke,R_ale,L_ame,L_ime,L_ape,L_epe,L_ope,L_are,R8_ire
     &,R_ese,R8_use,L_ate,L_ite,L_ute,I_ave,L_afi,R_aki,R_ami
     &,L_ipi,L_upi,L_asi,L_osi,L_eti,L_iti,L_oti,L_uti,L_avi
     &,L_evi)
C |R_ati         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |R8_ofi        |4 8 O|16 value        |��� �������� ��������|0.5|
C |I_u           |2 4 O|LREADY          |����� ����������||
C |I_id          |2 4 O|LBUSY           |����� �����||
C |R_od          |4 4 S|vminit_button_ST*|��������� ������ "������� � �������� �� ���������" |0.0|
C |R_ud          |4 4 I|vminit_button   |������� ������ ������ "������� � �������� �� ���������" |0.0|
C |R_af          |4 4 S|vmwork_button_ST*|��������� ������ "������� � ������� �� ���������" |0.0|
C |R_ef          |4 4 I|vmwork_button   |������� ������ ������ "������� � ������� �� ���������" |0.0|
C |I_if          |2 4 O|state1          |��������� 1||
C |I_ak          |2 4 O|state2          |��������� 2||
C |I_al          |2 4 O|LWORKO          |����� � �������||
C |I_ol          |2 4 O|LINITC          |����� � ��������||
C |C20_om        |3 20 O|task_state      |���������||
C |C20_op        |3 20 O|task_name       |������� ���������||
C |C8_or         |3 8 I|task            |������� ���������||
C |R_it          |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ot          |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_ut          |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_av          |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_ev          |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_iv          |2 4 O|LERROR          |����� �������������||
C |R_ex          |4 4 O|POS_CL          |��������||
C |R_ox          |4 4 O|POS_OP          |��������||
C |I_ux          |2 4 O|LINIT           |����� ��������||
C |I_ibe         |2 4 O|LWORK           |����� �������||
C |I_ade         |2 4 O|LZM             |������ "�������"||
C |L_ode         |1 1 I|vlv_kvit        |||
C |L_afe         |1 1 I|instr_fault     |||
C |L_efe         |1 1 S|_qffJ1618*      |�������� ������ Q RS-��������  |F|
C |L_ofe         |1 1 O|vminit_button_CMD*|[TF]����� ������ ������� � �������� �� ���������|F|
C |L_ake         |1 1 O|vmwork_button_CMD*|[TF]����� ������ ������� � ������� �� ���������|F|
C |L_ike         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_oke         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |R_ale         |4 4 I|tinit           |����� ���� � ��������|30.0|
C |L_ame         |1 1 O|block           |||
C |L_ime         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_ape         |1 1 O|STOP            |�������||
C |L_epe         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_ope         |1 1 O|norm            |�����||
C |L_are         |1 1 O|nopower         |��� ����������||
C |R8_ire        |4 8 I|voltage         |[��]���������� �� ������||
C |R_ese         |4 4 I|power           |�������� ��������||
C |R8_use        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_ate         |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_ite         |1 1 I|YA26            |������� ������� �� ����������|F|
C |L_ute         |1 1 O|fault           |�������������||
C |I_ave         |2 4 O|LAM             |������ "�������"||
C |L_afi         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_aki         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_ami         |4 4 I|twork           |����� ���� � �������|30.0|
C |L_ipi         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_upi         |1 1 O|XH53            |�� ������� (���)|F|
C |L_asi         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_osi         |1 1 O|XH54            |�� ������� (���)|F|
C |L_eti         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_iti         |1 1 I|mlf23           |������� ������� �����||
C |L_oti         |1 1 I|mlf22           |����� ����� ��������||
C |L_uti         |1 1 I|mlf04           |�������� �� ��������||
C |L_avi         |1 1 I|mlf03           |�������� �� ��������||
C |L_evi         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      INTEGER*4 I_u,I_id
      REAL*4 R_od,R_ud,R_af,R_ef
      INTEGER*4 I_if,I_ak,I_al,I_ol
      CHARACTER*20 C20_om,C20_op
      CHARACTER*8 C8_or
      REAL*4 R_it,R_ot
      LOGICAL*1 L_ut
      REAL*4 R_av,R_ev
      INTEGER*4 I_iv
      REAL*4 R_ex,R_ox
      INTEGER*4 I_ux,I_ibe,I_ade
      LOGICAL*1 L_ode,L_afe,L_efe,L_ofe,L_ake,L_ike,L_oke
      REAL*4 R_ale
      LOGICAL*1 L_ame,L_ime,L_ape,L_epe,L_ope,L_are
      REAL*8 R8_ire
      REAL*4 R_ese
      REAL*8 R8_use
      LOGICAL*1 L_ate,L_ite,L_ute
      INTEGER*4 I_ave
      LOGICAL*1 L_afi
      REAL*8 R8_ofi
      REAL*4 R_aki,R_ami
      LOGICAL*1 L_ipi,L_upi,L_asi,L_osi
      REAL*4 R_ati
      LOGICAL*1 L_eti,L_iti,L_oti,L_uti,L_avi,L_evi
      End subroutine TIAGA_HANDLER
      End interface
