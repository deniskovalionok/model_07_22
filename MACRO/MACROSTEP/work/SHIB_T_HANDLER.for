      Subroutine SHIB_T_HANDLER(ext_deltat,R_ive,R8_ade,R_i
     &,R_u,C30_af,R_uf,R_ak,R_ek,R_ik,R_ok,R_uk,R_el,R_ol
     &,L_ul,L_em,L_im,L_om,R_um,L_ep,L_ip,L_op,L_up,L_ar,L_or
     &,L_ur,L_es,L_is,L_us,R8_et,R_av,R8_ov,L_uv,L_ax,L_ix
     &,L_ox,L_oke,R_ile,R_ule,L_ure,L_ese,L_ite,L_ave,L_ove
     &,L_uve,L_axe,L_exe,L_ixe,L_oxe)
C |R_ive         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |R8_ade        |4 8 O|16 value        |��� �������� ��������|0.5|
C |R_i           |4 4 O|VY01            |��������� �� OY||
C |R_u           |4 4 O|VY02            |��������||
C |C30_af        |3 30 O|state           |||
C |R_uf          |4 4 S|vmclose_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ak          |4 4 I|vmclose_button  |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_ek          |4 4 S|vmopen_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ik          |4 4 I|vmopen_button   |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_ok          |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_uk          |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_el          |4 4 O|POS_CL          |��������||
C |R_ol          |4 4 O|POS_OP          |��������||
C |L_ul          |1 1 I|vlv_kvit        |||
C |L_em          |1 1 I|instr_fault     |||
C |L_im          |1 1 S|_qffJ1618*      |�������� ������ Q RS-��������  |F|
C |L_om          |1 1 O|vmclose_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |R_um          |4 4 I|tclose          |����� ���� �������|30.0|
C |L_ep          |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_ip          |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_op          |1 1 O|block           |||
C |L_up          |1 1 O|vmopen_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ar          |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_or          |1 1 O|STOP            |�������||
C |L_ur          |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_es          |1 1 O|fault           |�������������||
C |L_is          |1 1 O|norm            |�����||
C |L_us          |1 1 O|nopower         |��� ����������||
C |R8_et         |4 8 I|voltage         |[��]���������� �� ������||
C |R_av          |4 4 I|power           |�������� ��������||
C |R8_ov         |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_uv          |1 1 I|YA22            |������� ������� �������|F|
C |L_ax          |1 1 I|YA12            |������� ������� �� ����������|F|
C |L_ix          |1 1 I|YA21            |������� ������� �������|F|
C |L_ox          |1 1 I|YA11            |������� ������� �� ����������|F|
C |L_oke         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_ile         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_ule         |4 4 I|topen           |����� ���� �������|30.0|
C |L_ure         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_ese         |1 1 O|YV12            |�� ������� (���)|F|
C |L_ite         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ave         |1 1 O|YV11            |�� ������� (���)|F|
C |L_ove         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_uve         |1 1 I|mlf23           |������� ������� �����||
C |L_axe         |1 1 I|mlf22           |����� ����� ��������||
C |L_exe         |1 1 I|mlf04           |�������� �� ��������||
C |L_ixe         |1 1 I|mlf03           |�������� �� ��������||
C |L_oxe         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R0_e,R_i,R0_o,R_u
      CHARACTER*30 C30_ad,C30_ed,C30_id,C30_od,C30_ud,C30_af
     &,C30_ef,C30_if,C30_of
      REAL*4 R_uf,R_ak,R_ek,R_ik,R_ok,R_uk,R0_al,R_el,R0_il
     &,R_ol
      LOGICAL*1 L_ul,L0_am,L_em,L_im,L_om
      REAL*4 R_um,R0_ap
      LOGICAL*1 L_ep,L_ip,L_op,L_up,L_ar,L0_er,L0_ir,L_or
     &,L_ur,L0_as,L_es,L_is,L0_os,L_us
      REAL*4 R0_at
      REAL*8 R8_et
      LOGICAL*1 L0_it,L0_ot
      REAL*4 R0_ut,R_av,R0_ev,R0_iv
      REAL*8 R8_ov
      LOGICAL*1 L_uv,L_ax,L0_ex,L_ix,L_ox,L0_ux,L0_abe,L0_ebe
      REAL*4 R0_ibe,R0_obe
      LOGICAL*1 L0_ube
      REAL*8 R8_ade
      REAL*4 R0_ede,R0_ide,R0_ode,R0_ude,R0_afe,R0_efe
      LOGICAL*1 L0_ife
      REAL*4 R0_ofe,R0_ufe,R0_ake,R0_eke
      LOGICAL*1 L0_ike,L_oke
      REAL*4 R0_uke,R0_ale,R0_ele,R_ile,R0_ole,R_ule,R0_ame
     &,R0_eme,R0_ime,R0_ome,R0_ume,R0_ape,R0_epe,R0_ipe
      LOGICAL*1 L0_ope,L0_upe,L0_are,L0_ere,L0_ire,L0_ore
     &,L_ure,L0_ase,L_ese,L0_ise,L0_ose,L0_use,L0_ate,L0_ete
     &,L_ite,L0_ote,L0_ute,L_ave,L0_eve
      REAL*4 R_ive
      LOGICAL*1 L_ove,L_uve,L_axe,L_exe,L_ixe,L_oxe

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_e = 1200
C SHIB_T_HANDLER.fmg( 398, 237):��������� (RE4) (�������)
      R0_o = 10000.0
C SHIB_T_HANDLER.fmg( 297, 264):��������� (RE4) (�������)
      C30_id = '����� �����������'
C SHIB_T_HANDLER.fmg( 174, 222):��������� ���������� CH20 (CH30) (�������)
      C30_ad = '����� �����������'
C SHIB_T_HANDLER.fmg( 196, 221):��������� ���������� CH20 (CH30) (�������)
      C30_od = '����� ������'
C SHIB_T_HANDLER.fmg( 131, 224):��������� ���������� CH20 (CH30) (�������)
      C30_ud = ''
C SHIB_T_HANDLER.fmg( 131, 226):��������� ���������� CH20 (CH30) (�������)
      C30_if = '����� ������'
C SHIB_T_HANDLER.fmg( 151, 223):��������� ���������� CH20 (CH30) (�������)
      L_om=R_ak.ne.R_uf
      R_uf=R_ak
C SHIB_T_HANDLER.fmg(  14, 194):���������� ������������� ������
      L0_ex = (.NOT.L_ep).AND.L_om
C SHIB_T_HANDLER.fmg(  66, 194):�
      L0_ise = L0_ex.OR.L_ax.OR.L_uv
C SHIB_T_HANDLER.fmg(  70, 192):���
      L_up=R_ik.ne.R_ek
      R_ek=R_ik
C SHIB_T_HANDLER.fmg(  16, 236):���������� ������������� ������
      L0_ux = L_up.AND.(.NOT.L_ip)
C SHIB_T_HANDLER.fmg(  66, 236):�
      L0_ute = L0_ux.OR.L_ox.OR.L_ix
C SHIB_T_HANDLER.fmg(  70, 234):���
      L0_er = L0_ute.OR.L0_ise
C SHIB_T_HANDLER.fmg(  76, 203):���
      L_ar=R_uk.ne.R_ok
      R_ok=R_uk
C SHIB_T_HANDLER.fmg(  14, 208):���������� ������������� ������
      L_ur=(L_ar.or.L_ur).and..not.(L0_er)
      L0_ir=.not.L_ur
C SHIB_T_HANDLER.fmg( 106, 205):RS �������,10
      L_or=L_ur
C SHIB_T_HANDLER.fmg( 125, 207):������,STOP
      L0_are = L_ur.OR.L_ove
C SHIB_T_HANDLER.fmg(  98, 214):���
      R0_al = 100
C SHIB_T_HANDLER.fmg( 378, 273):��������� (RE4) (�������)
      R0_il = 100
C SHIB_T_HANDLER.fmg( 365, 267):��������� (RE4) (�������)
      L_im=(L_em.or.L_im).and..not.(L_ul)
      L0_am=.not.L_im
C SHIB_T_HANDLER.fmg( 326, 178):RS �������
      R0_ap = DeltaT
C SHIB_T_HANDLER.fmg( 250, 254):��������� (RE4) (�������)
      if(R_um.ge.0.0) then
         R0_ime=R0_ap/max(R_um,1.0e-10)
      else
         R0_ime=R0_ap/min(R_um,-1.0e-10)
      endif
C SHIB_T_HANDLER.fmg( 259, 252):�������� ����������
      L_op = L_ip.OR.L_ep
C SHIB_T_HANDLER.fmg(  68, 210):���
      L0_os =.NOT.(L_ixe.OR.L_exe.OR.L_oxe.OR.L_axe.OR.L_uve.OR.L_ove
     &)
C SHIB_T_HANDLER.fmg( 319, 191):���
      L0_as =.NOT.(L0_os)
C SHIB_T_HANDLER.fmg( 328, 185):���
      L_es = L0_as.OR.L_im
C SHIB_T_HANDLER.fmg( 332, 184):���
      L_is = L0_os.OR.L_em
C SHIB_T_HANDLER.fmg( 328, 190):���
      R0_at = 0.1
C SHIB_T_HANDLER.fmg( 254, 160):��������� (RE4) (�������)
      L_us=R8_et.lt.R0_at
C SHIB_T_HANDLER.fmg( 259, 162):���������� <
      R0_ut = 0.0
C SHIB_T_HANDLER.fmg( 266, 182):��������� (RE4) (�������)
      R0_afe = 0.000001
C SHIB_T_HANDLER.fmg( 354, 208):��������� (RE4) (�������)
      R0_efe = 0.999999
C SHIB_T_HANDLER.fmg( 354, 224):��������� (RE4) (�������)
      R0_ibe = 0.0
C SHIB_T_HANDLER.fmg( 296, 244):��������� (RE4) (�������)
      R0_obe = 0.0
C SHIB_T_HANDLER.fmg( 370, 242):��������� (RE4) (�������)
      L0_ube = L_uve.OR.L_axe
C SHIB_T_HANDLER.fmg( 363, 237):���
      R0_ide = 1.0
C SHIB_T_HANDLER.fmg( 348, 252):��������� (RE4) (�������)
      R0_ode = 0.0
C SHIB_T_HANDLER.fmg( 340, 252):��������� (RE4) (�������)
      R0_ufe = 1.0e-10
C SHIB_T_HANDLER.fmg( 318, 228):��������� (RE4) (�������)
      R0_uke = 0.0
C SHIB_T_HANDLER.fmg( 328, 242):��������� (RE4) (�������)
      R0_ame = DeltaT
C SHIB_T_HANDLER.fmg( 250, 264):��������� (RE4) (�������)
      if(R_ule.ge.0.0) then
         R0_ome=R0_ame/max(R_ule,1.0e-10)
      else
         R0_ome=R0_ame/min(R_ule,-1.0e-10)
      endif
C SHIB_T_HANDLER.fmg( 259, 262):�������� ����������
      R0_ipe = 0.0
C SHIB_T_HANDLER.fmg( 282, 244):��������� (RE4) (�������)
      L0_ere = (.NOT.L_or).AND.L_ese
C SHIB_T_HANDLER.fmg( 103, 166):�
C label 96  try96=try96-1
      if(L_axe) then
         R0_ede=R0_obe
      else
         R0_ede=R8_ade
      endif
C SHIB_T_HANDLER.fmg( 373, 242):���� RE IN LO CH7
      if(.NOT.L0_ube) then
         R_ile=R8_ade
      endif
C SHIB_T_HANDLER.fmg( 384, 252):���� � ������������� �������
      L0_use = (.NOT.L_or).AND.L_ave
C SHIB_T_HANDLER.fmg( 105, 260):�
      L0_ete = L_ave.OR.L0_are.OR.L0_ise
C SHIB_T_HANDLER.fmg( 104, 229):���
      L0_eve = (.NOT.L_ave).AND.L0_ute
C SHIB_T_HANDLER.fmg( 104, 235):�
      L_ite=(L0_eve.or.L_ite).and..not.(L0_ete)
      L0_ote=.not.L_ite
C SHIB_T_HANDLER.fmg( 111, 233):RS �������,1
      L0_ate = (.NOT.L0_use).AND.L_ite
C SHIB_T_HANDLER.fmg( 130, 236):�
      L0_ebe = L0_ate.AND.(.NOT.L_exe)
C SHIB_T_HANDLER.fmg( 252, 242):�
      L0_upe = L0_ebe.OR.L_ixe
C SHIB_T_HANDLER.fmg( 265, 241):���
      if(L0_upe) then
         R0_ape=R0_ome
      else
         R0_ape=R0_ipe
      endif
C SHIB_T_HANDLER.fmg( 287, 254):���� RE IN LO CH7
      L0_ore = L0_are.OR.L_ese.OR.L0_ute
C SHIB_T_HANDLER.fmg( 104, 185):���
      L0_ose = L0_ise.AND.(.NOT.L_ese)
C SHIB_T_HANDLER.fmg( 104, 191):�
      L_ure=(L0_ose.or.L_ure).and..not.(L0_ore)
      L0_ase=.not.L_ure
C SHIB_T_HANDLER.fmg( 111, 189):RS �������,2
      L0_ire = L_ure.AND.(.NOT.L0_ere)
C SHIB_T_HANDLER.fmg( 130, 190):�
      L0_abe = L0_ire.AND.(.NOT.L_ixe)
C SHIB_T_HANDLER.fmg( 252, 228):�
      L0_ope = L0_abe.OR.L_exe
C SHIB_T_HANDLER.fmg( 265, 227):���
      if(L0_ope) then
         R0_ume=R0_ime
      else
         R0_ume=R0_ipe
      endif
C SHIB_T_HANDLER.fmg( 287, 236):���� RE IN LO CH7
      R0_epe = R0_ape + (-R0_ume)
C SHIB_T_HANDLER.fmg( 293, 246):��������
      if(L_ove) then
         R0_ale=R0_ibe
      else
         R0_ale=R0_epe
      endif
C SHIB_T_HANDLER.fmg( 300, 244):���� RE IN LO CH7
      R0_eke = R_ile + (-R_ive)
C SHIB_T_HANDLER.fmg( 308, 235):��������
      R0_ofe = R0_ale + R0_eke
C SHIB_T_HANDLER.fmg( 314, 236):��������
      R0_ake = R0_ofe * R0_eke
C SHIB_T_HANDLER.fmg( 318, 235):����������
      L0_ike=R0_ake.lt.R0_ufe
C SHIB_T_HANDLER.fmg( 323, 234):���������� <
      L_oke=(L0_ike.or.L_oke).and..not.(.NOT.L_oxe)
      L0_ife=.not.L_oke
C SHIB_T_HANDLER.fmg( 330, 232):RS �������,6
      if(L_oke) then
         R_ile=R_ive
      endif
C SHIB_T_HANDLER.fmg( 324, 252):���� � ������������� �������
      if(L_oke) then
         R0_ele=R0_uke
      else
         R0_ele=R0_ale
      endif
C SHIB_T_HANDLER.fmg( 332, 244):���� RE IN LO CH7
      R0_ole = R_ile + R0_ele
C SHIB_T_HANDLER.fmg( 338, 245):��������
      R0_ude=MAX(R0_ode,R0_ole)
C SHIB_T_HANDLER.fmg( 346, 246):��������
      R0_eme=MIN(R0_ide,R0_ude)
C SHIB_T_HANDLER.fmg( 354, 247):�������
      L_ese=R0_eme.lt.R0_afe
C SHIB_T_HANDLER.fmg( 363, 210):���������� <
C sav1=L0_ore
      L0_ore = L0_are.OR.L_ese.OR.L0_ute
C SHIB_T_HANDLER.fmg( 104, 185):recalc:���
C if(sav1.ne.L0_ore .and. try134.gt.0) goto 134
C sav1=L0_ose
      L0_ose = L0_ise.AND.(.NOT.L_ese)
C SHIB_T_HANDLER.fmg( 104, 191):recalc:�
C if(sav1.ne.L0_ose .and. try136.gt.0) goto 136
C sav1=L0_ere
      L0_ere = (.NOT.L_or).AND.L_ese
C SHIB_T_HANDLER.fmg( 103, 166):recalc:�
C if(sav1.ne.L0_ere .and. try96.gt.0) goto 96
      if(L0_ube) then
         R8_ade=R0_ede
      else
         R8_ade=R0_eme
      endif
C SHIB_T_HANDLER.fmg( 377, 246):���� RE IN LO CH7
      R_ol = R0_il * R8_ade
C SHIB_T_HANDLER.fmg( 368, 266):����������
      R_el = R0_al + (-R_ol)
C SHIB_T_HANDLER.fmg( 382, 272):��������
      R_i = R0_e * R8_ade
C SHIB_T_HANDLER.fmg( 401, 236):����������
      L_ave=R0_eme.gt.R0_efe
C SHIB_T_HANDLER.fmg( 363, 225):���������� >
C sav1=L0_ete
      L0_ete = L_ave.OR.L0_are.OR.L0_ise
C SHIB_T_HANDLER.fmg( 104, 229):recalc:���
C if(sav1.ne.L0_ete .and. try114.gt.0) goto 114
C sav1=L0_eve
      L0_eve = (.NOT.L_ave).AND.L0_ute
C SHIB_T_HANDLER.fmg( 104, 235):recalc:�
C if(sav1.ne.L0_eve .and. try116.gt.0) goto 116
C sav1=L0_use
      L0_use = (.NOT.L_or).AND.L_ave
C SHIB_T_HANDLER.fmg( 105, 260):recalc:�
C if(sav1.ne.L0_use .and. try110.gt.0) goto 110
      if(L_ave) then
         C30_of=C30_od
      else
         C30_of=C30_ud
      endif
C SHIB_T_HANDLER.fmg( 135, 224):���� RE IN LO CH20
      if(L_ese) then
         C30_ef=C30_if
      else
         C30_ef=C30_of
      endif
C SHIB_T_HANDLER.fmg( 155, 224):���� RE IN LO CH20
      if(L0_ate) then
         C30_ed=C30_id
      else
         C30_ed=C30_ef
      endif
C SHIB_T_HANDLER.fmg( 178, 222):���� RE IN LO CH20
      if(L0_ire) then
         C30_af=C30_ad
      else
         C30_af=C30_ed
      endif
C SHIB_T_HANDLER.fmg( 200, 222):���� RE IN LO CH20
      if(L0_ube) then
         R_ile=R0_eme
      endif
C SHIB_T_HANDLER.fmg( 366, 252):���� � ������������� �������
C sav1=R0_eke
      R0_eke = R_ile + (-R_ive)
C SHIB_T_HANDLER.fmg( 308, 235):recalc:��������
C if(sav1.ne.R0_eke .and. try160.gt.0) goto 160
C sav1=R0_ole
      R0_ole = R_ile + R0_ele
C SHIB_T_HANDLER.fmg( 338, 245):recalc:��������
C if(sav1.ne.R0_ole .and. try177.gt.0) goto 177
      R_u = R0_o * R0_epe
C SHIB_T_HANDLER.fmg( 300, 263):����������
      L0_it = L0_ate.OR.L0_ire
C SHIB_T_HANDLER.fmg( 251, 174):���
      L0_ot = L0_it.AND.(.NOT.L_us)
C SHIB_T_HANDLER.fmg( 266, 173):�
      if(L0_ot) then
         R0_iv=R_av
      else
         R0_iv=R0_ut
      endif
C SHIB_T_HANDLER.fmg( 269, 180):���� RE IN LO CH7
      R0_ev = R8_ov
C SHIB_T_HANDLER.fmg( 264, 190):��������
C label 244  try244=try244-1
      R8_ov = R0_iv + R0_ev
C SHIB_T_HANDLER.fmg( 275, 189):��������
C sav1=R0_ev
      R0_ev = R8_ov
C SHIB_T_HANDLER.fmg( 264, 190):recalc:��������
C if(sav1.ne.R0_ev .and. try244.gt.0) goto 244
      End
