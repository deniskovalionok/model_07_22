      Subroutine LODOCHKA_HANDLER_FDA60_COORD(ext_deltat,R_e
     &,I_i,I_u,R_ad,I_ed,I_od,R_ud,I_af,I_if,I_of,I_uf,R_ak
     &,I_ek,R_ik,I_ok,R_uk,I_al,R_el,I_il,R_ol,I_ul,R_am,I_em
     &,R_im,I_om,R_um,I_ep,I_ip,R_op,I_ar,R_er,I_or,I_ur,R_as
     &,I_is,I_os,R_us,I_et,I_it,R_ot,I_av,I_ev,R_iv,I_uv,I_ax
     &,R_ex,I_ox,I_ux,R_abe,I_ibe,I_obe,R_ube,I_ede,I_ide
     &,I_ode,I_ude,I_afe,I_efe,I_ife,I_ofe,I_ufe,I_ake,I_eke
     &,I_ike,I_oke,I_uke,I_ale,I_ele,I_ile,I_ole,I_ule,I_ame
     &,I_eme,I_ime,I_ome,I_ume,I_ape,I_epe,I_ipe,I_ope,I_upe
     &,I_are,I_ere,I_ire,I_ore,I_ure,I_ase,I_ese,I_ise,I_ose
     &,I_use,I_ate,I_ete,I_ite,I_ote,I_ute,I_ave,I_eve,I_ive
     &,I_ove,I_uve,I_axe,I_exe,I_ixe,I_oxe,I_uxe,I_abi,I_ebi
     &,I_ibi,I_obi,I_ubi,I_adi,I_edi,I_idi,I_odi,I_udi,I_afi
     &,I_efi,I_ifi,I_ofi,I_ufi,I_aki,I_eki,I_iki,R_oki,R_uki
     &,R_ali,R_eli,R_ili,R_oli,R_uli,R_ami,R_emi,R_imi,R_omi
     &,R_umi,R_api,R_epi,R_ipi,R_opi,R_upi,R_ari,R_eri,R_iri
     &,R_ori,R_uri,R_asi,R_esi,R_isi,R_osi,R_usi,R_ati,R_eti
     &,R_iti,R_oti,R_uti,R_avi,R_evi,R_ivi,R_ovi,R_uvi,R_axi
     &,R_exi,R_ixi,R_oxi,R_uxi,R_abo,R_ebo,R_ibo,R_obo,R_ubo
     &,R_ado,R_edo,R_ido,R_odo,R_udo,R_afo,R_efo,R_ifo,R_ofo
     &,R_ufo,R_ako,R_eko,R_iko,R_oko,R_uko,R_alo,R_elo,R_ilo
     &,R_olo,R_ulo,R_amo,R_emo,R_imo,R_omo,L_oro,L_uro,L_aso
     &,L_eso,L_iso,L_oso,L_uso,L_ato,L_eto,L_ito,L_oto,L_uto
     &,I_abu,I_ebu,I_ibu,I_obu,I_ubu,I_adu,I_edu,I_idu,I_odu
     &,I_udu,I_alu,I_elu,I_ilu,I_olu,I_ulu,I_amu,I_emu,I_imu
     &,I_omu,I_umu,I_asu,I_esu,I_isu,I_osu,I_usu,I_atu,I_etu
     &,I_itu,I_otu,I_utu,I_abad,I_ebad,I_ibad,I_obad,I_ubad
     &,I_adad,I_edad,I_idad,I_odad,I_udad,I_alad,I_elad,I_ilad
     &,I_olad,I_ulad,I_amad,I_emad,I_imad,I_omad,I_umad,I_esad
     &,I_isad,I_osad,I_usad,I_atad,I_etad,I_itad,I_otad,I_utad
     &,I_avad,I_evad,I_ivad,I_ovad,I_uvad,I_axad,I_exad,I_ixad
     &,I_oxad,I_uxad,I_abed,I_ebed)
C |R_e           |4 4 O|CIL_20FDA60_MASS|||
C |I_i           |2 4 K|_lcmpJ10279     |�������� ������ �����������|5|
C |I_u           |2 4 O|CIL_20FDA60_N   |||
C |R_ad          |4 4 O|CIL_20FDA70_MASS|||
C |I_ed          |2 4 K|_lcmpJ10261     |�������� ������ �����������|7001|
C |I_od          |2 4 O|CIL_20FDA70_N   |||
C |R_ud          |4 4 O|CIL_20FDA40_MASS|||
C |I_af          |2 4 K|_lcmpJ10222     |�������� ������ �����������|4001|
C |I_if          |2 4 O|CIL_20FDA40_N   |||
C |I_of          |2 4 K|_lcmpJ10126     |�������� ������ �����������|6|
C |I_uf          |2 4 O|CIL_20FDA60AE502_N7|||
C |R_ak          |4 4 O|CIL_20FDA60AE502_MASS7|||
C |I_ek          |2 4 O|CIL_20FDA60AE502_N6|||
C |R_ik          |4 4 O|CIL_20FDA60AE502_MASS6|||
C |I_ok          |2 4 O|CIL_20FDA60AE502_N5|||
C |R_uk          |4 4 O|CIL_20FDA60AE502_MASS5|||
C |I_al          |2 4 O|CIL_20FDA60AE502_N4|||
C |R_el          |4 4 O|CIL_20FDA60AE502_MASS4|||
C |I_il          |2 4 O|CIL_20FDA60AE502_N3|||
C |R_ol          |4 4 O|CIL_20FDA60AE502_MASS3|||
C |I_ul          |2 4 O|CIL_20FDA60AE502_N2|||
C |R_am          |4 4 O|CIL_20FDA60AE502_MASS2|||
C |I_em          |2 4 O|CIL_20FDA60AE502_N1|||
C |R_im          |4 4 O|CIL_20FDA60AE502_MASS1|||
C |I_om          |2 4 K|_lcmpJ9970      |�������� ������ �����������|35|
C |R_um          |4 4 O|CIL_20FDA60AE515_MASS|||
C |I_ep          |2 4 O|CIL_20FDA60AE515_N|||
C |I_ip          |2 4 K|_lcmpJ9956      |�������� ������ �����������|11|
C |R_op          |4 4 O|CIL_20FDA60AE509_MASS|||
C |I_ar          |2 4 O|CIL_20FDA60AE509_N|||
C |R_er          |4 4 O|CIL_20FDA60AE403_MASS|||
C |I_or          |2 4 O|CIL_20FDA60AE403_N|||
C |I_ur          |2 4 K|_lcmpJ9928      |�������� ������ �����������|5|
C |R_as          |4 4 O|CIL_20FDA60AE500_MASS2|||
C |I_is          |2 4 O|CIL_20FDA60AE500_N2|||
C |I_os          |2 4 K|_lcmpJ9914      |�������� ������ �����������|4|
C |R_us          |4 4 O|CIL_20FDA60AE500_MASS1|||
C |I_et          |2 4 O|CIL_20FDA60AE500_N1|||
C |I_it          |2 4 K|_lcmpJ9900      |�������� ������ �����������|101|
C |R_ot          |4 4 O|CIL_20FDA60AE402_MASS|||
C |I_av          |2 4 O|CIL_20FDA60AE402_N|||
C |I_ev          |2 4 K|_lcmpJ9886      |�������� ������ �����������|100|
C |R_iv          |4 4 O|CIL_20FDA60AE201_MASS|||
C |I_uv          |2 4 O|CIL_20FDA60AE201_N|||
C |I_ax          |2 4 K|_lcmpJ9872      |�������� ������ �����������|99|
C |R_ex          |4 4 O|CIL_20FDA60AE401_MASS|||
C |I_ox          |2 4 O|CIL_20FDA60AE401_N|||
C |I_ux          |2 4 K|_lcmpJ9858      |�������� ������ �����������|98|
C |R_abe         |4 4 O|CIL_20FDA60AE200_MASS|||
C |I_ibe         |2 4 O|CIL_20FDA60AE200_N|||
C |I_obe         |2 4 K|_lcmpJ9844      |�������� ������ �����������|97|
C |R_ube         |4 4 O|CIL_20FDA60AE400_MASS|||
C |I_ede         |2 4 O|CIL_20FDA60AE400_N|||
C |I_ide         |2 4 K|_lcmpJ9767      |�������� ������ �����������|27|
C |I_ode         |2 4 K|_lcmpJ9766      |�������� ������ �����������|26|
C |I_ude         |2 4 K|_lcmpJ9765      |�������� ������ �����������|25|
C |I_afe         |2 4 K|_lcmpJ9764      |�������� ������ �����������|24|
C |I_efe         |2 4 K|_lcmpJ9763      |�������� ������ �����������|23|
C |I_ife         |2 4 K|_lcmpJ9757      |�������� ������ �����������|92|
C |I_ofe         |2 4 K|_lcmpJ9756      |�������� ������ �����������|96|
C |I_ufe         |2 4 K|_lcmpJ9755      |�������� ������ �����������|95|
C |I_ake         |2 4 K|_lcmpJ9754      |�������� ������ �����������|94|
C |I_eke         |2 4 K|_lcmpJ9753      |�������� ������ �����������|93|
C |I_ike         |2 4 K|_lcmpJ9751      |�������� ������ �����������|87|
C |I_oke         |2 4 K|_lcmpJ9749      |�������� ������ �����������|91|
C |I_uke         |2 4 K|_lcmpJ9747      |�������� ������ �����������|90|
C |I_ale         |2 4 K|_lcmpJ9745      |�������� ������ �����������|89|
C |I_ele         |2 4 K|_lcmpJ9744      |�������� ������ �����������|88|
C |I_ile         |2 4 K|_lcmpJ9738      |�������� ������ �����������|82|
C |I_ole         |2 4 K|_lcmpJ9737      |�������� ������ �����������|86|
C |I_ule         |2 4 K|_lcmpJ9736      |�������� ������ �����������|85|
C |I_ame         |2 4 K|_lcmpJ9735      |�������� ������ �����������|84|
C |I_eme         |2 4 K|_lcmpJ9734      |�������� ������ �����������|83|
C |I_ime         |2 4 K|_lcmpJ9732      |�������� ������ �����������|77|
C |I_ome         |2 4 K|_lcmpJ9730      |�������� ������ �����������|81|
C |I_ume         |2 4 K|_lcmpJ9728      |�������� ������ �����������|80|
C |I_ape         |2 4 K|_lcmpJ9726      |�������� ������ �����������|79|
C |I_epe         |2 4 K|_lcmpJ9724      |�������� ������ �����������|78|
C |I_ipe         |2 4 K|_lcmpJ9717      |�������� ������ �����������|62|
C |I_ope         |2 4 K|_lcmpJ9716      |�������� ������ �����������|66|
C |I_upe         |2 4 K|_lcmpJ9715      |�������� ������ �����������|65|
C |I_are         |2 4 K|_lcmpJ9714      |�������� ������ �����������|64|
C |I_ere         |2 4 K|_lcmpJ9713      |�������� ������ �����������|63|
C |I_ire         |2 4 K|_lcmpJ9711      |�������� ������ �����������|57|
C |I_ore         |2 4 K|_lcmpJ9709      |�������� ������ �����������|61|
C |I_ure         |2 4 K|_lcmpJ9707      |�������� ������ �����������|60|
C |I_ase         |2 4 K|_lcmpJ9705      |�������� ������ �����������|59|
C |I_ese         |2 4 K|_lcmpJ9703      |�������� ������ �����������|58|
C |I_ise         |2 4 K|_lcmpJ9696      |�������� ������ �����������|52|
C |I_ose         |2 4 K|_lcmpJ9695      |�������� ������ �����������|56|
C |I_use         |2 4 K|_lcmpJ9694      |�������� ������ �����������|55|
C |I_ate         |2 4 K|_lcmpJ9693      |�������� ������ �����������|54|
C |I_ete         |2 4 K|_lcmpJ9692      |�������� ������ �����������|53|
C |I_ite         |2 4 K|_lcmpJ9690      |�������� ������ �����������|47|
C |I_ote         |2 4 K|_lcmpJ9688      |�������� ������ �����������|51|
C |I_ute         |2 4 K|_lcmpJ9686      |�������� ������ �����������|50|
C |I_ave         |2 4 K|_lcmpJ9684      |�������� ������ �����������|49|
C |I_eve         |2 4 K|_lcmpJ9682      |�������� ������ �����������|48|
C |I_ive         |2 4 K|_lcmpJ9653      |�������� ������ �����������|72|
C |I_ove         |2 4 K|_lcmpJ9652      |�������� ������ �����������|76|
C |I_uve         |2 4 K|_lcmpJ9651      |�������� ������ �����������|75|
C |I_axe         |2 4 K|_lcmpJ9650      |�������� ������ �����������|74|
C |I_exe         |2 4 K|_lcmpJ9649      |�������� ������ �����������|73|
C |I_ixe         |2 4 K|_lcmpJ9647      |�������� ������ �����������|67|
C |I_oxe         |2 4 K|_lcmpJ9645      |�������� ������ �����������|71|
C |I_uxe         |2 4 K|_lcmpJ9643      |�������� ������ �����������|70|
C |I_abi         |2 4 K|_lcmpJ9641      |�������� ������ �����������|69|
C |I_ebi         |2 4 K|_lcmpJ9640      |�������� ������ �����������|68|
C |I_ibi         |2 4 K|_lcmpJ9634      |�������� ������ �����������|42|
C |I_obi         |2 4 K|_lcmpJ9633      |�������� ������ �����������|46|
C |I_ubi         |2 4 K|_lcmpJ9632      |�������� ������ �����������|45|
C |I_adi         |2 4 K|_lcmpJ9631      |�������� ������ �����������|44|
C |I_edi         |2 4 K|_lcmpJ9630      |�������� ������ �����������|43|
C |I_idi         |2 4 K|_lcmpJ9614      |�������� ������ �����������|16|
C |I_odi         |2 4 K|_lcmpJ9613      |�������� ������ �����������|15|
C |I_udi         |2 4 K|_lcmpJ9612      |�������� ������ �����������|14|
C |I_afi         |2 4 K|_lcmpJ9611      |�������� ������ �����������|13|
C |I_efi         |2 4 K|_lcmpJ9609      |�������� ������ �����������|12|
C |I_ifi         |2 4 K|_lcmpJ9605      |�������� ������ �����������|37|
C |I_ofi         |2 4 K|_lcmpJ9602      |�������� ������ �����������|41|
C |I_ufi         |2 4 K|_lcmpJ9600      |�������� ������ �����������|40|
C |I_aki         |2 4 K|_lcmpJ9598      |�������� ������ �����������|39|
C |I_eki         |2 4 K|_lcmpJ9595      |�������� ������ �����������|38|
C |I_iki         |2 4 I|CR              |������ ������������||
C |R_oki         |4 4 O|CIL_PKS6_MASS1  |||
C |R_uki         |4 4 O|CIL_PKS6_MASS2  |||
C |R_ali         |4 4 O|CIL_PKS6_MASS3  |||
C |R_eli         |4 4 O|CIL_PKS6_MASS4  |||
C |R_ili         |4 4 O|CIL_PKS6_MASS5  |||
C |R_oli         |4 4 O|CIL_KO6_MASS1   |||
C |R_uli         |4 4 O|CIL_KO6_MASS2   |||
C |R_ami         |4 4 O|CIL_KO6_MASS3   |||
C |R_emi         |4 4 O|CIL_KO6_MASS4   |||
C |R_imi         |4 4 O|CIL_KO6_MASS5   |||
C |R_omi         |4 4 O|CIL_KO5_MASS5   |||
C |R_umi         |4 4 O|CIL_KO5_MASS4   |||
C |R_api         |4 4 O|CIL_KO5_MASS3   |||
C |R_epi         |4 4 O|CIL_KO5_MASS2   |||
C |R_ipi         |4 4 O|CIL_KO5_MASS1   |||
C |R_opi         |4 4 O|CIL_PKS5_MASS5  |||
C |R_upi         |4 4 O|CIL_PKS5_MASS4  |||
C |R_ari         |4 4 O|CIL_PKS5_MASS3  |||
C |R_eri         |4 4 O|CIL_PKS5_MASS2  |||
C |R_iri         |4 4 O|CIL_PKS5_MASS1  |||
C |R_ori         |4 4 O|CIL_KO4_MASS5   |||
C |R_uri         |4 4 O|CIL_KO4_MASS4   |||
C |R_asi         |4 4 O|CIL_KO4_MASS3   |||
C |R_esi         |4 4 O|CIL_KO4_MASS2   |||
C |R_isi         |4 4 O|CIL_KO4_MASS1   |||
C |R_osi         |4 4 O|CIL_PKS4_MASS5  |||
C |R_usi         |4 4 O|CIL_PKS4_MASS4  |||
C |R_ati         |4 4 O|CIL_PKS4_MASS3  |||
C |R_eti         |4 4 O|CIL_PKS4_MASS2  |||
C |R_iti         |4 4 O|CIL_PKS4_MASS1  |||
C |R_oti         |4 4 O|CIL_20FDA60AE408_MASS1|||
C |R_uti         |4 4 O|CIL_20FDA60AE408_MASS2|||
C |R_avi         |4 4 O|CIL_20FDA60AE408_MASS3|||
C |R_evi         |4 4 O|CIL_20FDA60AE408_MASS4|||
C |R_ivi         |4 4 O|CIL_20FDA60AE408_MASS5|||
C |R_ovi         |4 4 O|CIL_20FDA60AE413_MASS1|||
C |R_uvi         |4 4 O|CIL_20FDA60AE413_MASS2|||
C |R_axi         |4 4 O|CIL_20FDA60AE413_MASS3|||
C |R_exi         |4 4 O|CIL_20FDA60AE413_MASS4|||
C |R_ixi         |4 4 O|CIL_20FDA60AE413_MASS5|||
C |R_oxi         |4 4 O|CIL_KO3_MASS5   |||
C |R_uxi         |4 4 O|CIL_KO3_MASS4   |||
C |R_abo         |4 4 O|CIL_KO3_MASS3   |||
C |R_ebo         |4 4 O|CIL_KO3_MASS2   |||
C |R_ibo         |4 4 O|CIL_KO3_MASS1   |||
C |R_obo         |4 4 O|CIL_PKS3_MASS5  |||
C |R_ubo         |4 4 O|CIL_PKS3_MASS4  |||
C |R_ado         |4 4 O|CIL_PKS3_MASS3  |||
C |R_edo         |4 4 O|CIL_PKS3_MASS2  |||
C |R_ido         |4 4 O|CIL_PKS3_MASS1  |||
C |R_odo         |4 4 O|CIL_PKS2_MASS1  |||
C |R_udo         |4 4 O|CIL_PKS2_MASS2  |||
C |R_afo         |4 4 O|CIL_PKS2_MASS3  |||
C |R_efo         |4 4 O|CIL_PKS2_MASS4  |||
C |R_ifo         |4 4 O|CIL_PKS2_MASS5  |||
C |R_ofo         |4 4 O|CIL_KO2_MASS1   |||
C |R_ufo         |4 4 O|CIL_KO2_MASS2   |||
C |R_ako         |4 4 O|CIL_KO2_MASS3   |||
C |R_eko         |4 4 O|CIL_KO2_MASS4   |||
C |R_iko         |4 4 O|CIL_KO2_MASS5   |||
C |R_oko         |4 4 O|CIL_KO1_MASS5   |||
C |R_uko         |4 4 O|CIL_KO1_MASS4   |||
C |R_alo         |4 4 O|CIL_KO1_MASS3   |||
C |R_elo         |4 4 O|CIL_KO1_MASS2   |||
C |R_ilo         |4 4 O|CIL_KO1_MASS1   |||
C |R_olo         |4 4 O|CIL_PKS1_MASS5  |||
C |R_ulo         |4 4 O|CIL_PKS1_MASS4  |||
C |R_amo         |4 4 O|CIL_PKS1_MASS3  |||
C |R_emo         |4 4 O|CIL_PKS1_MASS2  |||
C |R_imo         |4 4 I|MASS            |����� �������||
C |R_omo         |4 4 O|CIL_PKS1_MASS1  |||
C |L_oro         |1 1 O|IN_KO4          |||
C |L_uro         |1 1 O|IN_KO5          |||
C |L_aso         |1 1 O|IN_KO6          |||
C |L_eso         |1 1 O|IN_KO3          |||
C |L_iso         |1 1 O|IN_KO2          |||
C |L_oso         |1 1 O|IN_KO1          |||
C |L_uso         |1 1 O|IN_PKS4         |||
C |L_ato         |1 1 O|IN_PKS5         |||
C |L_eto         |1 1 O|IN_PKS6         |||
C |L_ito         |1 1 O|IN_PKS3         |||
C |L_oto         |1 1 O|IN_PKS2         |||
C |L_uto         |1 1 O|IN_PKS1         |||
C |I_abu         |2 4 O|CIL_PKS6_N1     |||
C |I_ebu         |2 4 O|CIL_PKS6_N2     |||
C |I_ibu         |2 4 O|CIL_KO6_N5      |||
C |I_obu         |2 4 O|CIL_KO6_N4      |||
C |I_ubu         |2 4 O|CIL_KO6_N3      |||
C |I_adu         |2 4 O|CIL_KO6_N2      |||
C |I_edu         |2 4 O|CIL_KO6_N1      |||
C |I_idu         |2 4 O|CIL_PKS6_N5     |||
C |I_odu         |2 4 O|CIL_PKS6_N4     |||
C |I_udu         |2 4 O|CIL_PKS6_N3     |||
C |I_alu         |2 4 O|CIL_PKS5_N1     |||
C |I_elu         |2 4 O|CIL_PKS5_N2     |||
C |I_ilu         |2 4 O|CIL_KO5_N5      |||
C |I_olu         |2 4 O|CIL_KO5_N4      |||
C |I_ulu         |2 4 O|CIL_KO5_N3      |||
C |I_amu         |2 4 O|CIL_KO5_N2      |||
C |I_emu         |2 4 O|CIL_KO5_N1      |||
C |I_imu         |2 4 O|CIL_PKS5_N5     |||
C |I_omu         |2 4 O|CIL_PKS5_N4     |||
C |I_umu         |2 4 O|CIL_PKS5_N3     |||
C |I_asu         |2 4 O|CIL_PKS4_N1     |||
C |I_esu         |2 4 O|CIL_PKS4_N2     |||
C |I_isu         |2 4 O|CIL_KO4_N5      |||
C |I_osu         |2 4 O|CIL_KO4_N4      |||
C |I_usu         |2 4 O|CIL_KO4_N3      |||
C |I_atu         |2 4 O|CIL_KO4_N2      |||
C |I_etu         |2 4 O|CIL_KO4_N1      |||
C |I_itu         |2 4 O|CIL_PKS4_N5     |||
C |I_otu         |2 4 O|CIL_PKS4_N4     |||
C |I_utu         |2 4 O|CIL_PKS4_N3     |||
C |I_abad        |2 4 O|CIL_PKS3_N1     |||
C |I_ebad        |2 4 O|CIL_PKS3_N2     |||
C |I_ibad        |2 4 O|CIL_KO3_N5      |||
C |I_obad        |2 4 O|CIL_KO3_N4      |||
C |I_ubad        |2 4 O|CIL_KO3_N3      |||
C |I_adad        |2 4 O|CIL_KO3_N2      |||
C |I_edad        |2 4 O|CIL_KO3_N1      |||
C |I_idad        |2 4 O|CIL_PKS3_N5     |||
C |I_odad        |2 4 O|CIL_PKS3_N4     |||
C |I_udad        |2 4 O|CIL_PKS3_N3     |||
C |I_alad        |2 4 O|CIL_PKS2_N1     |||
C |I_elad        |2 4 O|CIL_PKS2_N2     |||
C |I_ilad        |2 4 O|CIL_KO2_N5      |||
C |I_olad        |2 4 O|CIL_KO2_N4      |||
C |I_ulad        |2 4 O|CIL_KO2_N3      |||
C |I_amad        |2 4 O|CIL_KO2_N2      |||
C |I_emad        |2 4 O|CIL_KO2_N1      |||
C |I_imad        |2 4 O|CIL_PKS2_N5     |||
C |I_omad        |2 4 O|CIL_PKS2_N4     |||
C |I_umad        |2 4 O|CIL_PKS2_N3     |||
C |I_esad        |2 4 O|CIL_PKS1_N1     |||
C |I_isad        |2 4 O|CIL_PKS1_N2     |||
C |I_osad        |2 4 O|CIL_KO1_N5      |||
C |I_usad        |2 4 O|CIL_KO1_N4      |||
C |I_atad        |2 4 O|CIL_KO1_N3      |||
C |I_etad        |2 4 O|CIL_KO1_N2      |||
C |I_itad        |2 4 O|CIL_KO1_N1      |||
C |I_otad        |2 4 O|CIL_PKS1_N5     |||
C |I_utad        |2 4 O|CIL_PKS1_N4     |||
C |I_avad        |2 4 O|CIL_PKS1_N3     |||
C |I_evad        |2 4 O|CIL_20FDA60AE413_N5|||
C |I_ivad        |2 4 O|CIL_20FDA60AE413_N4|||
C |I_ovad        |2 4 O|CIL_20FDA60AE413_N3|||
C |I_uvad        |2 4 O|CIL_20FDA60AE413_N2|||
C |I_axad        |2 4 O|CIL_20FDA60AE413_N1|||
C |I_exad        |2 4 P|num_boat        |����� �������||
C |I_ixad        |2 4 O|CIL_20FDA60AE408_N5|||
C |I_oxad        |2 4 O|CIL_20FDA60AE408_N4|||
C |I_uxad        |2 4 O|CIL_20FDA60AE408_N3|||
C |I_abed        |2 4 O|CIL_20FDA60AE408_N2|||
C |I_ebed        |2 4 O|CIL_20FDA60AE408_N1|||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R_e
      INTEGER*4 I_i
      LOGICAL*1 L0_o
      INTEGER*4 I_u
      REAL*4 R_ad
      INTEGER*4 I_ed
      LOGICAL*1 L0_id
      INTEGER*4 I_od
      REAL*4 R_ud
      INTEGER*4 I_af
      LOGICAL*1 L0_ef
      INTEGER*4 I_if,I_of,I_uf
      REAL*4 R_ak
      INTEGER*4 I_ek
      REAL*4 R_ik
      INTEGER*4 I_ok
      REAL*4 R_uk
      INTEGER*4 I_al
      REAL*4 R_el
      INTEGER*4 I_il
      REAL*4 R_ol
      INTEGER*4 I_ul
      REAL*4 R_am
      INTEGER*4 I_em
      REAL*4 R_im
      INTEGER*4 I_om
      REAL*4 R_um
      LOGICAL*1 L0_ap
      INTEGER*4 I_ep,I_ip
      REAL*4 R_op
      LOGICAL*1 L0_up
      INTEGER*4 I_ar
      REAL*4 R_er
      LOGICAL*1 L0_ir
      INTEGER*4 I_or,I_ur
      REAL*4 R_as
      LOGICAL*1 L0_es
      INTEGER*4 I_is,I_os
      REAL*4 R_us
      LOGICAL*1 L0_at
      INTEGER*4 I_et,I_it
      REAL*4 R_ot
      LOGICAL*1 L0_ut
      INTEGER*4 I_av,I_ev
      REAL*4 R_iv
      LOGICAL*1 L0_ov
      INTEGER*4 I_uv,I_ax
      REAL*4 R_ex
      LOGICAL*1 L0_ix
      INTEGER*4 I_ox,I_ux
      REAL*4 R_abe
      LOGICAL*1 L0_ebe
      INTEGER*4 I_ibe,I_obe
      REAL*4 R_ube
      LOGICAL*1 L0_ade
      INTEGER*4 I_ede,I_ide,I_ode,I_ude,I_afe,I_efe,I_ife
     &,I_ofe,I_ufe,I_ake,I_eke,I_ike,I_oke,I_uke,I_ale,I_ele
     &,I_ile,I_ole,I_ule,I_ame,I_eme,I_ime
      INTEGER*4 I_ome,I_ume,I_ape,I_epe,I_ipe,I_ope,I_upe
     &,I_are,I_ere,I_ire,I_ore,I_ure,I_ase,I_ese,I_ise,I_ose
     &,I_use,I_ate,I_ete,I_ite,I_ote,I_ute
      INTEGER*4 I_ave,I_eve,I_ive,I_ove,I_uve,I_axe,I_exe
     &,I_ixe,I_oxe,I_uxe,I_abi,I_ebi,I_ibi,I_obi,I_ubi,I_adi
     &,I_edi,I_idi,I_odi,I_udi,I_afi,I_efi
      INTEGER*4 I_ifi,I_ofi,I_ufi,I_aki,I_eki,I_iki
      REAL*4 R_oki,R_uki,R_ali,R_eli,R_ili,R_oli,R_uli,R_ami
     &,R_emi,R_imi,R_omi,R_umi,R_api,R_epi,R_ipi,R_opi,R_upi
     &,R_ari,R_eri,R_iri,R_ori,R_uri
      REAL*4 R_asi,R_esi,R_isi,R_osi,R_usi,R_ati,R_eti,R_iti
     &,R_oti,R_uti,R_avi,R_evi,R_ivi,R_ovi,R_uvi,R_axi,R_exi
     &,R_ixi,R_oxi,R_uxi,R_abo,R_ebo
      REAL*4 R_ibo,R_obo,R_ubo,R_ado,R_edo,R_ido,R_odo,R_udo
     &,R_afo,R_efo,R_ifo,R_ofo,R_ufo,R_ako,R_eko,R_iko,R_oko
     &,R_uko,R_alo,R_elo,R_ilo,R_olo
      REAL*4 R_ulo,R_amo,R_emo,R_imo,R_omo
      LOGICAL*1 L0_umo,L0_apo,L0_epo,L0_ipo,L0_opo,L0_upo
     &,L0_aro,L0_ero,L0_iro,L_oro,L_uro,L_aso,L_eso,L_iso
     &,L_oso,L_uso,L_ato,L_eto,L_ito,L_oto,L_uto,L0_avo
      LOGICAL*1 L0_evo,L0_ivo,L0_ovo,L0_uvo,L0_axo,L0_exo
     &,L0_ixo,L0_oxo,L0_uxo
      INTEGER*4 I_abu,I_ebu,I_ibu,I_obu,I_ubu,I_adu,I_edu
     &,I_idu,I_odu,I_udu
      LOGICAL*1 L0_afu,L0_efu,L0_ifu,L0_ofu,L0_ufu,L0_aku
     &,L0_eku,L0_iku,L0_oku,L0_uku
      INTEGER*4 I_alu,I_elu,I_ilu,I_olu,I_ulu,I_amu,I_emu
     &,I_imu,I_omu,I_umu
      LOGICAL*1 L0_apu,L0_epu,L0_ipu,L0_opu,L0_upu,L0_aru
     &,L0_eru,L0_iru,L0_oru,L0_uru
      INTEGER*4 I_asu,I_esu,I_isu,I_osu,I_usu,I_atu,I_etu
     &,I_itu,I_otu,I_utu
      LOGICAL*1 L0_avu,L0_evu,L0_ivu,L0_ovu,L0_uvu,L0_axu
     &,L0_exu,L0_ixu,L0_oxu,L0_uxu
      INTEGER*4 I_abad,I_ebad,I_ibad,I_obad,I_ubad,I_adad
     &,I_edad,I_idad,I_odad,I_udad
      LOGICAL*1 L0_afad,L0_efad,L0_ifad,L0_ofad,L0_ufad,L0_akad
     &,L0_ekad,L0_ikad,L0_okad,L0_ukad
      INTEGER*4 I_alad,I_elad,I_ilad,I_olad,I_ulad,I_amad
     &,I_emad,I_imad,I_omad,I_umad
      LOGICAL*1 L0_apad,L0_epad,L0_ipad,L0_opad,L0_upad,L0_arad
     &,L0_erad,L0_irad,L0_orad,L0_urad,L0_asad
      INTEGER*4 I_esad,I_isad,I_osad,I_usad,I_atad,I_etad
     &,I_itad,I_otad,I_utad,I_avad,I_evad,I_ivad,I_ovad,I_uvad
     &,I_axad,I_exad,I_ixad,I_oxad,I_uxad,I_abed,I_ebed

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      L0_o=I_iki.eq.I_i
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1708):���������� �������������
      if(L0_o) then
         I_u=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1715):���� � ������������� �������
      if(L0_o) then
         R_e=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1720):���� � ������������� �������
      L0_id=I_iki.eq.I_ed
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1670):���������� �������������
      if(L0_id) then
         I_od=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1676):���� � ������������� �������
      if(L0_id) then
         R_ad=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1682):���� � ������������� �������
      L0_ef=I_iki.eq.I_af
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1750):���������� �������������
      if(L0_ef) then
         I_if=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1756):���� � ������������� �������
      if(L0_ef) then
         R_ud=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1762):���� � ������������� �������
      L0_ir=I_iki.eq.I_of
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1428):���������� �������������
      if(L0_ir) then
         I_or=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1434):���� � ������������� �������
      if(L0_ir) then
         R_er=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1439):���� � ������������� �������
      I_uf = 0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 102,1306):��������� ������������� IN (�������)
      I_ek = 0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 102,1316):��������� ������������� IN (�������)
      I_ok = 0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 102,1326):��������� ������������� IN (�������)
      I_al = 0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 102,1336):��������� ������������� IN (�������)
      I_il = 0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 102,1346):��������� ������������� IN (�������)
      I_ul = 0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 101,1355):��������� ������������� IN (�������)
      I_em = 0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 101,1364):��������� ������������� IN (�������)
      R_ak = 0.0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 102,1301):��������� (RE4) (�������)
      R_ik = 0.0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 102,1312):��������� (RE4) (�������)
      R_uk = 0.0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 102,1321):��������� (RE4) (�������)
      R_el = 0.0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 102,1331):��������� (RE4) (�������)
      R_ol = 0.0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 102,1340):��������� (RE4) (�������)
      R_am = 0.0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 101,1350):��������� (RE4) (�������)
      R_im = 0.0
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 101,1360):��������� (RE4) (�������)
      L0_ap=I_iki.eq.I_om
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1392):���������� �������������
      if(L0_ap) then
         I_ep=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1397):���� � ������������� �������
      if(L0_ap) then
         R_um=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1402):���� � ������������� �������
      L0_up=I_iki.eq.I_ip
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1410):���������� �������������
      if(L0_up) then
         I_ar=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1416):���� � ������������� �������
      if(L0_up) then
         R_op=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1421):���� � ������������� �������
      L0_es=I_iki.eq.I_ur
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1446):���������� �������������
      if(L0_es) then
         I_is=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1451):���� � ������������� �������
      if(L0_es) then
         R_as=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1456):���� � ������������� �������
      L0_at=I_iki.eq.I_os
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1462):���������� �������������
      if(L0_at) then
         I_et=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1468):���� � ������������� �������
      if(L0_at) then
         R_us=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1473):���� � ������������� �������
      L0_ut=I_iki.eq.I_it
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1480):���������� �������������
      if(L0_ut) then
         I_av=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1485):���� � ������������� �������
      if(L0_ut) then
         R_ot=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1490):���� � ������������� �������
      L0_ov=I_iki.eq.I_ev
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1497):���������� �������������
      if(L0_ov) then
         I_uv=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1502):���� � ������������� �������
      if(L0_ov) then
         R_iv=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1508):���� � ������������� �������
      L0_ix=I_iki.eq.I_ax
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1514):���������� �������������
      if(L0_ix) then
         I_ox=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1520):���� � ������������� �������
      if(L0_ix) then
         R_ex=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1524):���� � ������������� �������
      L0_ebe=I_iki.eq.I_ux
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1532):���������� �������������
      if(L0_ebe) then
         I_ibe=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1538):���� � ������������� �������
      if(L0_ebe) then
         R_abe=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1543):���� � ������������� �������
      L0_ade=I_iki.eq.I_obe
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1552):���������� �������������
      if(L0_ade) then
         I_ede=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1557):���� � ������������� �������
      if(L0_ade) then
         R_ube=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1562):���� � ������������� �������
      L0_umo=I_iki.eq.I_ide
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1787):���������� �������������
      if(L0_umo) then
         I_evad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1794):���� � ������������� �������
      if(L0_umo) then
         R_ixi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1799):���� � ������������� �������
      L0_apo=I_iki.eq.I_ode
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1807):���������� �������������
      if(L0_apo) then
         I_ivad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1814):���� � ������������� �������
      if(L0_apo) then
         R_exi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1819):���� � ������������� �������
      L0_epo=I_iki.eq.I_ude
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1828):���������� �������������
      if(L0_epo) then
         I_ovad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1835):���� � ������������� �������
      if(L0_epo) then
         R_axi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1840):���� � ������������� �������
      L0_ipo=I_iki.eq.I_afe
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1848):���������� �������������
      if(L0_ipo) then
         I_uvad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1856):���� � ������������� �������
      if(L0_ipo) then
         R_uvi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1860):���� � ������������� �������
      L0_opo=I_iki.eq.I_efe
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1870):���������� �������������
      if(L0_opo) then
         I_axad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1878):���� � ������������� �������
      if(L0_opo) then
         R_ovi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1882):���� � ������������� �������
      L0_uvo=I_iki.eq.I_ife
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1650):���������� �������������
      if(L0_uvo) then
         I_edu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1656):���� � ������������� �������
      if(L0_uvo) then
         R_oli=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1661):���� � ������������� �������
      L0_avo=I_iki.eq.I_ofe
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1573):���������� �������������
      if(L0_avo) then
         I_ibu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 317,1578):���� � ������������� �������
      if(L0_avo) then
         R_imi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 317,1584):���� � ������������� �������
      L0_evo=I_iki.eq.I_ufe
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1592):���������� �������������
      if(L0_evo) then
         I_obu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1598):���� � ������������� �������
      if(L0_evo) then
         R_emi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1603):���� � ������������� �������
      L0_ivo=I_iki.eq.I_ake
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1612):���������� �������������
      if(L0_ivo) then
         I_ubu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1617):���� � ������������� �������
      if(L0_ivo) then
         R_ami=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1622):���� � ������������� �������
      L0_ovo=I_iki.eq.I_eke
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1631):���������� �������������
      if(L0_ovo) then
         I_adu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1636):���� � ������������� �������
      if(L0_ovo) then
         R_uli=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1642):���� � ������������� �������
      L_aso = L0_uvo.OR.L0_ovo.OR.L0_ivo.OR.L0_evo.OR.L0_avo
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 354,1612):���
      L0_uxo=I_iki.eq.I_ike
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1754):���������� �������������
      if(L0_uxo) then
         I_abu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 317,1760):���� � ������������� �������
      if(L0_uxo) then
         R_oki=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 317,1764):���� � ������������� �������
      L0_axo=I_iki.eq.I_oke
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1670):���������� �������������
      if(L0_axo) then
         I_idu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1676):���� � ������������� �������
      if(L0_axo) then
         R_ili=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1680):���� � ������������� �������
      L0_exo=I_iki.eq.I_uke
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1690):���������� �������������
      if(L0_exo) then
         I_odu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1696):���� � ������������� �������
      if(L0_exo) then
         R_eli=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1700):���� � ������������� �������
      L0_ixo=I_iki.eq.I_ale
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1711):���������� �������������
      if(L0_ixo) then
         I_udu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1716):���� � ������������� �������
      if(L0_ixo) then
         R_ali=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1721):���� � ������������� �������
      L0_oxo=I_iki.eq.I_ele
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1732):���������� �������������
      if(L0_oxo) then
         I_ebu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1736):���� � ������������� �������
      if(L0_oxo) then
         R_uki=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1742):���� � ������������� �������
      L_eto = L0_uxo.OR.L0_oxo.OR.L0_ixo.OR.L0_exo.OR.L0_axo
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 353,1711):���
      L0_ufu=I_iki.eq.I_ile
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1650):���������� �������������
      if(L0_ufu) then
         I_emu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1656):���� � ������������� �������
      if(L0_ufu) then
         R_ipi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1661):���� � ������������� �������
      L0_afu=I_iki.eq.I_ole
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1573):���������� �������������
      if(L0_afu) then
         I_ilu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1578):���� � ������������� �������
      if(L0_afu) then
         R_omi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1584):���� � ������������� �������
      L0_efu=I_iki.eq.I_ule
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 179,1592):���������� �������������
      if(L0_efu) then
         I_olu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1598):���� � ������������� �������
      if(L0_efu) then
         R_umi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1603):���� � ������������� �������
      L0_ifu=I_iki.eq.I_ame
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 179,1612):���������� �������������
      if(L0_ifu) then
         I_ulu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1617):���� � ������������� �������
      if(L0_ifu) then
         R_api=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1622):���� � ������������� �������
      L0_ofu=I_iki.eq.I_eme
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 179,1631):���������� �������������
      if(L0_ofu) then
         I_amu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1636):���� � ������������� �������
      if(L0_ofu) then
         R_epi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1642):���� � ������������� �������
      L_uro = L0_ufu.OR.L0_ofu.OR.L0_ifu.OR.L0_efu.OR.L0_afu
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 240,1612):���
      L0_uku=I_iki.eq.I_ime
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1754):���������� �������������
      if(L0_uku) then
         I_alu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1760):���� � ������������� �������
      if(L0_uku) then
         R_iri=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1764):���� � ������������� �������
      L0_aku=I_iki.eq.I_ome
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1670):���������� �������������
      if(L0_aku) then
         I_imu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1676):���� � ������������� �������
      if(L0_aku) then
         R_opi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1680):���� � ������������� �������
      L0_eku=I_iki.eq.I_ume
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1690):���������� �������������
      if(L0_eku) then
         I_omu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1696):���� � ������������� �������
      if(L0_eku) then
         R_upi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1700):���� � ������������� �������
      L0_iku=I_iki.eq.I_ape
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1711):���������� �������������
      if(L0_iku) then
         I_umu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1716):���� � ������������� �������
      if(L0_iku) then
         R_ari=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1721):���� � ������������� �������
      L0_oku=I_iki.eq.I_epe
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1732):���������� �������������
      if(L0_oku) then
         I_elu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1736):���� � ������������� �������
      if(L0_oku) then
         R_eri=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1742):���� � ������������� �������
      L_ato = L0_uku.OR.L0_oku.OR.L0_iku.OR.L0_eku.OR.L0_aku
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 240,1711):���
      L0_uvu=I_iki.eq.I_ipe
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1872):���������� �������������
      if(L0_uvu) then
         I_edad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1878):���� � ������������� �������
      if(L0_uvu) then
         R_ibo=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1883):���� � ������������� �������
      L0_avu=I_iki.eq.I_ope
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1795):���������� �������������
      if(L0_avu) then
         I_ibad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 317,1800):���� � ������������� �������
      if(L0_avu) then
         R_oxi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 317,1806):���� � ������������� �������
      L0_evu=I_iki.eq.I_upe
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1814):���������� �������������
      if(L0_evu) then
         I_obad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1820):���� � ������������� �������
      if(L0_evu) then
         R_uxi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1825):���� � ������������� �������
      L0_ivu=I_iki.eq.I_are
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1834):���������� �������������
      if(L0_ivu) then
         I_ubad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1839):���� � ������������� �������
      if(L0_ivu) then
         R_abo=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1844):���� � ������������� �������
      L0_ovu=I_iki.eq.I_ere
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1853):���������� �������������
      if(L0_ovu) then
         I_adad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1858):���� � ������������� �������
      if(L0_ovu) then
         R_ebo=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1864):���� � ������������� �������
      L_eso = L0_uvu.OR.L0_ovu.OR.L0_ivu.OR.L0_evu.OR.L0_avu
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 354,1834):���
      L0_uxu=I_iki.eq.I_ire
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1976):���������� �������������
      if(L0_uxu) then
         I_abad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 317,1982):���� � ������������� �������
      if(L0_uxu) then
         R_ido=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 317,1986):���� � ������������� �������
      L0_axu=I_iki.eq.I_ore
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1892):���������� �������������
      if(L0_axu) then
         I_idad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1898):���� � ������������� �������
      if(L0_axu) then
         R_obo=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1902):���� � ������������� �������
      L0_exu=I_iki.eq.I_ure
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1912):���������� �������������
      if(L0_exu) then
         I_odad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1918):���� � ������������� �������
      if(L0_exu) then
         R_ubo=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1922):���� � ������������� �������
      L0_ixu=I_iki.eq.I_ase
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1933):���������� �������������
      if(L0_ixu) then
         I_udad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1938):���� � ������������� �������
      if(L0_ixu) then
         R_ado=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1943):���� � ������������� �������
      L0_oxu=I_iki.eq.I_ese
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 292,1954):���������� �������������
      if(L0_oxu) then
         I_ebad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1958):���� � ������������� �������
      if(L0_oxu) then
         R_edo=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 318,1964):���� � ������������� �������
      L_ito = L0_uxu.OR.L0_oxu.OR.L0_ixu.OR.L0_exu.OR.L0_axu
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 353,1933):���
      L0_ufad=I_iki.eq.I_ise
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1872):���������� �������������
      if(L0_ufad) then
         I_emad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1878):���� � ������������� �������
      if(L0_ufad) then
         R_ofo=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1883):���� � ������������� �������
      L0_afad=I_iki.eq.I_ose
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1795):���������� �������������
      if(L0_afad) then
         I_ilad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1800):���� � ������������� �������
      if(L0_afad) then
         R_iko=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1806):���� � ������������� �������
      L0_efad=I_iki.eq.I_use
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 179,1814):���������� �������������
      if(L0_efad) then
         I_olad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1820):���� � ������������� �������
      if(L0_efad) then
         R_eko=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1825):���� � ������������� �������
      L0_ifad=I_iki.eq.I_ate
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 179,1834):���������� �������������
      if(L0_ifad) then
         I_ulad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1839):���� � ������������� �������
      if(L0_ifad) then
         R_ako=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1844):���� � ������������� �������
      L0_ofad=I_iki.eq.I_ete
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 179,1853):���������� �������������
      if(L0_ofad) then
         I_amad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1858):���� � ������������� �������
      if(L0_ofad) then
         R_ufo=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1864):���� � ������������� �������
      L_iso = L0_ufad.OR.L0_ofad.OR.L0_ifad.OR.L0_efad.OR.L0_afad
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 240,1834):���
      L0_ukad=I_iki.eq.I_ite
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1976):���������� �������������
      if(L0_ukad) then
         I_alad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1982):���� � ������������� �������
      if(L0_ukad) then
         R_odo=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1986):���� � ������������� �������
      L0_akad=I_iki.eq.I_ote
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1892):���������� �������������
      if(L0_akad) then
         I_imad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1898):���� � ������������� �������
      if(L0_akad) then
         R_ifo=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1902):���� � ������������� �������
      L0_ekad=I_iki.eq.I_ute
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1912):���������� �������������
      if(L0_ekad) then
         I_omad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1918):���� � ������������� �������
      if(L0_ekad) then
         R_efo=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 205,1922):���� � ������������� �������
      L0_ikad=I_iki.eq.I_ave
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1933):���������� �������������
      if(L0_ikad) then
         I_umad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1938):���� � ������������� �������
      if(L0_ikad) then
         R_afo=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1943):���� � ������������� �������
      L0_okad=I_iki.eq.I_eve
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 178,1954):���������� �������������
      if(L0_okad) then
         I_elad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1958):���� � ������������� �������
      if(L0_okad) then
         R_udo=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 204,1964):���� � ������������� �������
      L_oto = L0_ukad.OR.L0_okad.OR.L0_ikad.OR.L0_ekad.OR.L0_akad
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 240,1933):���
      L0_upu=I_iki.eq.I_ive
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1651):���������� �������������
      if(L0_upu) then
         I_etu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1656):���� � ������������� �������
      if(L0_upu) then
         R_isi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1662):���� � ������������� �������
      L0_apu=I_iki.eq.I_ove
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1574):���������� �������������
      if(L0_apu) then
         I_isu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1579):���� � ������������� �������
      if(L0_apu) then
         R_ori=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1584):���� � ������������� �������
      L0_epu=I_iki.eq.I_uve
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  44,1593):���������� �������������
      if(L0_epu) then
         I_osu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1598):���� � ������������� �������
      if(L0_epu) then
         R_uri=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1604):���� � ������������� �������
      L0_ipu=I_iki.eq.I_axe
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1612):���������� �������������
      if(L0_ipu) then
         I_usu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1618):���� � ������������� �������
      if(L0_ipu) then
         R_asi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1622):���� � ������������� �������
      L0_opu=I_iki.eq.I_exe
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1632):���������� �������������
      if(L0_opu) then
         I_atu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1637):���� � ������������� �������
      if(L0_opu) then
         R_esi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1642):���� � ������������� �������
      L_oro = L0_upu.OR.L0_opu.OR.L0_ipu.OR.L0_epu.OR.L0_apu
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 134,1612):���
      L0_uru=I_iki.eq.I_ixe
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  44,1754):���������� �������������
      if(L0_uru) then
         I_asu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1760):���� � ������������� �������
      if(L0_uru) then
         R_iti=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1765):���� � ������������� �������
      L0_aru=I_iki.eq.I_oxe
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1670):���������� �������������
      if(L0_aru) then
         I_itu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1676):���� � ������������� �������
      if(L0_aru) then
         R_osi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1681):���� � ������������� �������
      L0_eru=I_iki.eq.I_uxe
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1690):���������� �������������
      if(L0_eru) then
         I_otu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1696):���� � ������������� �������
      if(L0_eru) then
         R_usi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1701):���� � ������������� �������
      L0_iru=I_iki.eq.I_abi
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1712):���������� �������������
      if(L0_iru) then
         I_utu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1716):���� � ������������� �������
      if(L0_iru) then
         R_ati=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1722):���� � ������������� �������
      L0_oru=I_iki.eq.I_ebi
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1732):���������� �������������
      if(L0_oru) then
         I_esu=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  97,1737):���� � ������������� �������
      if(L0_oru) then
         R_eti=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  97,1742):���� � ������������� �������
      L_uso = L0_uru.OR.L0_oru.OR.L0_iru.OR.L0_eru.OR.L0_aru
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 132,1712):���
      L0_upad=I_iki.eq.I_ibi
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1873):���������� �������������
      if(L0_upad) then
         I_itad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  97,1878):���� � ������������� �������
      if(L0_upad) then
         R_ilo=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  97,1884):���� � ������������� �������
      L0_apad=I_iki.eq.I_obi
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1796):���������� �������������
      if(L0_apad) then
         I_osad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1801):���� � ������������� �������
      if(L0_apad) then
         R_oko=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1806):���� � ������������� �������
      L0_epad=I_iki.eq.I_ubi
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  44,1815):���������� �������������
      if(L0_epad) then
         I_usad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1820):���� � ������������� �������
      if(L0_epad) then
         R_uko=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1826):���� � ������������� �������
      L0_ipad=I_iki.eq.I_adi
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  44,1834):���������� �������������
      if(L0_ipad) then
         I_atad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1840):���� � ������������� �������
      if(L0_ipad) then
         R_alo=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1844):���� � ������������� �������
      L0_opad=I_iki.eq.I_edi
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  44,1854):���������� �������������
      if(L0_opad) then
         I_etad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1859):���� � ������������� �������
      if(L0_opad) then
         R_elo=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1864):���� � ������������� �������
      L_oso = L0_upad.OR.L0_opad.OR.L0_ipad.OR.L0_epad.OR.L0_apad
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 133,1834):���
      L0_upo=I_iki.eq.I_idi
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1891):���������� �������������
      if(L0_upo) then
         I_ixad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1898):���� � ������������� �������
      if(L0_upo) then
         R_ivi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1903):���� � ������������� �������
      L0_aro=I_iki.eq.I_odi
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1911):���������� �������������
      if(L0_aro) then
         I_oxad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1918):���� � ������������� �������
      if(L0_aro) then
         R_evi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1923):���� � ������������� �������
      L0_ero=I_iki.eq.I_udi
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1932):���������� �������������
      if(L0_ero) then
         I_uxad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1939):���� � ������������� �������
      if(L0_ero) then
         R_avi=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1944):���� � ������������� �������
      L0_iro=I_iki.eq.I_afi
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1952):���������� �������������
      if(L0_iro) then
         I_abed=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1960):���� � ������������� �������
      if(L0_iro) then
         R_uti=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1964):���� � ������������� �������
      L0_asad=I_iki.eq.I_efi
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 424,1974):���������� �������������
      if(L0_asad) then
         I_ebed=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1982):���� � ������������� �������
      if(L0_asad) then
         R_oti=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 447,1986):���� � ������������� �������
      L0_urad=I_iki.eq.I_ifi
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  44,1978):���������� �������������
      if(L0_urad) then
         I_esad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1982):���� � ������������� �������
      if(L0_urad) then
         R_omo=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1987):���� � ������������� �������
      L0_arad=I_iki.eq.I_ofi
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1892):���������� �������������
      if(L0_arad) then
         I_otad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1898):���� � ������������� �������
      if(L0_arad) then
         R_olo=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1903):���� � ������������� �������
      L0_erad=I_iki.eq.I_ufi
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1912):���������� �������������
      if(L0_erad) then
         I_utad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1918):���� � ������������� �������
      if(L0_erad) then
         R_ulo=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  98,1923):���� � ������������� �������
      L0_irad=I_iki.eq.I_aki
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  43,1934):���������� �������������
      if(L0_irad) then
         I_avad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  97,1938):���� � ������������� �������
      if(L0_irad) then
         R_amo=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  97,1944):���� � ������������� �������
      L0_orad=I_iki.eq.I_eki
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  44,1954):���������� �������������
      if(L0_orad) then
         I_isad=I_exad
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1959):���� � ������������� �������
      if(L0_orad) then
         R_emo=R_imo
      endif
C LODOCHKA_HANDLER_FDA60_COORD.fmg(  96,1964):���� � ������������� �������
      L_uto = L0_urad.OR.L0_orad.OR.L0_irad.OR.L0_erad.OR.L0_arad
C LODOCHKA_HANDLER_FDA60_COORD.fmg( 132,1934):���
      End
