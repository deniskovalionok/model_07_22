      Subroutine TOLKATEL_HANDLER(ext_deltat,R_afi,R_e,R_i
     &,R_ed,R_af,R_uk,R_el,R_il,R_ol,R_ul,L_is,R_ot,R_ede
     &,L_ide,R_ude,L_afe,L_eke,L_uke,R_ume,R_ope,L_are,R_ire
     &,L_ure,R_ase,R_ese,L_ose,L_ote,L_ave,L_eve,L_ove,L_exe
     &,L_uxe,L_ebi,R_efi,L_ufi,L_eki,L_oki,L_uki,L_ili,L_oli
     &,C20_upi,L_ari,L_iri,L_ori,L_esi,L_isi,L_osi,L_usi,L_ati
     &,L_eti,L_iti,L_oti,L_uti,L_avi,L_evi,L_ivi,L_ovi,L_uvi
     &,L_axi,L_exi,L_ixi,R_oxi,R_uxi,R_abo,R_ebo,R_ibo,I_obo
     &,R_edo,R_ido,L_afo,C20_ulo,C20_umo,I_opo,I_ero,R_iro
     &,R_oro,R_uro,R_aso,L_eso,L_iso,I_oso,I_eto,I_evo,I_uvo
     &,C20_uxo,C8_ubu,R_iku,R_oku,L_uku,R_alu,R_elu,I_ilu
     &,L_amu,L_emu,L_imu,I_umu,I_ipu,L_iru,L_uru,L_asu,L_esu
     &,L_isu,L_osu,L_otu,L_ovu,L_axu,L_exu,R8_oxu,R_ibad,R8_adad
     &,L_odad,L_efad,I_ifad,L_akad,L_ekad,L_elad,L_umad,L_upad
     &)
C |R_afi         |4 4 I|11 mlfpar19     ||0.6|
C |R_e           |4 4 I|TIMESCALE       |������� �������||
C |R_i           |4 4 K|_cJ4002         |�������� ��������� ����������|`p_UP`|
C |R_ed          |4 4 K|_cJ3997         |�������� ��������� ����������|`p_LOW`|
C |R_af          |4 4 S|_slpbJ3982*     |���������� ��������� ||
C |R_uk          |4 4 I|p_TOLKATEL_XH54 |||
C |R_el          |4 4 S|_slpbJ3940*     |���������� ��������� ||
C |R_il          |4 4 S|_slpbJ3939*     |���������� ��������� ||
C |R_ol          |4 4 S|_slpbJ3938*     |���������� ��������� ||
C |R_ul          |4 4 S|_slpbJ3932*     |���������� ��������� ||
C |L_is          |1 1 S|_qffJ3888*      |�������� ������ Q RS-��������  |F|
C |R_ot          |4 4 I|mlfpar19        |��������� ������������|0.6|
C |R_ede         |4 4 S|_sdelnv2dyn$100Dasha*|[���]���������� ��������� |0|
C |L_ide         |1 1 S|_idelnv2dyn$100Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ude         |4 4 S|_sdelnv2dyn$99Dasha*|[���]���������� ��������� |0|
C |L_afe         |1 1 S|_idelnv2dyn$99Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_eke         |1 1 S|_qff156*        |�������� ������ Q RS-��������  |F|
C |L_uke         |1 1 S|_qff155*        |�������� ������ Q RS-��������  |F|
C |R_ume         |4 4 I|value_pos       |��� �������� ��������|0.5|
C |R_ope         |4 4 S|_sdelnv2dyn$83Dasha*|[���]���������� ��������� |0|
C |L_are         |1 1 S|_idelnv2dyn$83Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ire         |4 4 S|_sdelnv2dyn$82Dasha*|[���]���������� ��������� |0|
C |L_ure         |1 1 S|_idelnv2dyn$82Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ase         |4 4 I|tpos1           |����� ���� � ��������� 1|30.0|
C |R_ese         |4 4 I|tpos2           |����� ���� � ��������� 2|30.0|
C |L_ose         |1 1 S|_qffnv2dyn$106Dasha*|�������� ������ Q RS-��������  |F|
C |L_ote         |1 1 S|_qffnv2dyn$104Dasha*|�������� ������ Q RS-��������  |F|
C |L_ave         |1 1 S|_qffnv2dyn$105Dasha*|�������� ������ Q RS-��������  |F|
C |L_eve         |1 1 I|OUTC            |�����||
C |L_ove         |1 1 S|_qffnv2dyn$103Dasha*|�������� ������ Q RS-��������  |F|
C |L_exe         |1 1 S|_qffnv2dyn$102Dasha*|�������� ������ Q RS-��������  |F|
C |L_uxe         |1 1 S|_qffnv2dyn$101Dasha*|�������� ������ Q RS-��������  |F|
C |L_ebi         |1 1 I|OUTO            |������||
C |R_efi         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |L_ufi         |1 1 S|_qffnv2dyn$87Dasha*|�������� ������ Q RS-��������  |F|
C |L_eki         |1 1 S|_qffnv2dyn$86Dasha*|�������� ������ Q RS-��������  |F|
C |L_oki         |1 1 S|_qffnv2dyn$96Dasha*|�������� ������ Q RS-��������  |F|
C |L_uki         |1 1 S|_qffnv2dyn$95Dasha*|�������� ������ Q RS-��������  |F|
C |L_ili         |1 1 O|limit_switch_error|||
C |L_oli         |1 1 O|flag_mlf19      |||
C |C20_upi       |3 20 O|task_state      |���������||
C |L_ari         |1 1 I|vlv_kvit        |||
C |L_iri         |1 1 I|instr_fault     |||
C |L_ori         |1 1 S|_qffJ3170*      |�������� ������ Q RS-��������  |F|
C |L_esi         |1 1 O|norm            |�����||
C |L_isi         |1 1 I|mlf17           |������ ���������������� ����� �����||
C |L_osi         |1 1 I|mlf16           |������ ������������ ����� �����||
C |L_usi         |1 1 I|mlf15           |������ ���������������� ����� ������||
C |L_ati         |1 1 I|mlf14           |������ ������������ ����� ������||
C |L_eti         |1 1 I|mlf07           |���������� ���� �������||
C |L_iti         |1 1 I|mlf28           |������ ���������������� ��������� �����||
C |L_oti         |1 1 I|mlf09           |����� ��������� �����||
C |L_uti         |1 1 I|mlf08           |����� ��������� ������||
C |L_avi         |1 1 I|mlf26           |������ ���������������� ��������� ������||
C |L_evi         |1 1 I|mlf27           |������ ������������ ��������� �����||
C |L_ivi         |1 1 I|mlf25           |������ ������������ ��������� ������||
C |L_ovi         |1 1 I|mlf23           |������� ������� �����||
C |L_uvi         |1 1 I|mlf22           |����� ����� ��������||
C |L_axi         |1 1 I|mlf19           |���� ������������||
C |L_exi         |1 1 I|YA25C           |������� ������� �� ����������|F|
C |L_ixi         |1 1 I|YA26C           |������� ������� �� ���������� (��)|F|
C |R_oxi         |4 4 I|tcl_top         |�������� �������|20|
C |R_uxi         |4 4 K|_uintT_INT      |����������� ������ ����������� ������|`p_UP`|
C |R_abo         |4 4 K|_tintT_INT      |[���]�������� T �����������|1|
C |R_ebo         |4 4 O|_ointT_INT*     |�������� ������ ����������� |`p_START`|
C |R_ibo         |4 4 K|_lintT_INT      |����������� ������ ����������� �����|`p_LOW`|
C |I_obo         |2 4 O|LWORK           |����� �������||
C |R_edo         |4 4 O|VX01            |��������� ��������� �� ��� X||
C |R_ido         |4 4 O|VX02            |�������� ����������� ���������||
C |L_afo         |1 1 I|mlf04           |���������������� �������� �����||
C |C20_ulo       |3 20 O|task_name3      |������� ��������� ��� ������� ������������ ��������||
C |C20_umo       |3 20 O|task_name2      |������� ��������� ��� ������� ������������ ��������||
C |I_opo         |2 4 O|LREADY          |����� ����������||
C |I_ero         |2 4 O|LBUSY           |����� �����||
C |R_iro         |4 4 S|vminit_button_ST*|��������� ������ "������� � �������� �� ���������" |0.0|
C |R_oro         |4 4 I|vminit_button   |������� ������ ������ "������� � �������� �� ���������" |0.0|
C |R_uro         |4 4 S|vmwork_button_ST*|��������� ������ "������� � ������� �� ���������" |0.0|
C |R_aso         |4 4 I|vmwork_button   |������� ������ ������ "������� � ������� �� ���������" |0.0|
C |L_eso         |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_iso         |1 1 I|YA26            |������� ������� �� ����������|F|
C |I_oso         |2 4 O|state1          |��������� 1||
C |I_eto         |2 4 O|state2          |��������� 2||
C |I_evo         |2 4 O|LWORKO          |����� � �������||
C |I_uvo         |2 4 O|LINITC          |����� � ��������||
C |C20_uxo       |3 20 O|task_name       |������� ���������||
C |C8_ubu        |3 8 I|task            |������� ���������||
C |R_iku         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_oku         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_uku         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_alu         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_elu         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_ilu         |2 4 O|LERROR          |����� �������������||
C |L_amu         |1 1 I|YA27C           |������� ���� �� ���������� (��)|F|
C |L_emu         |1 1 I|stop_avt        |������� ���� �� ����������|F|
C |L_imu         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |I_umu         |2 4 O|LINIT           |����� ��������||
C |I_ipu         |2 4 O|LZM             |������ "�������"||
C |L_iru         |1 1 O|vminit_button_CMD*|[TF]����� ������ ������� � �������� �� ���������|F|
C |L_uru         |1 1 O|vmwork_button_CMD*|[TF]����� ������ ������� � ������� �� ���������|F|
C |L_asu         |1 1 I|mlf06           |������������� ������� "�����"||
C |L_esu         |1 1 I|mlf05           |������������� ������� "������"||
C |L_isu         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_osu         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_otu         |1 1 O|block           |||
C |L_ovu         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_axu         |1 1 O|STOP            |�������||
C |L_exu         |1 1 O|nopower         |��� ����������||
C |R8_oxu        |4 8 I|voltage         |[��]���������� �� ������||
C |R_ibad        |4 4 I|power           |�������� ��������||
C |R8_adad       |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_odad        |1 1 I|mlf03           |���������������� �������� ������||
C |L_efad        |1 1 O|fault           |�������������||
C |I_ifad        |2 4 O|LAM             |������ "�������"||
C |L_akad        |1 1 O|XH53            |�� ����� (���)|F|
C |L_ekad        |1 1 O|XH54            |�� ������ (���)|F|
C |L_elad        |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_umad        |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_upad        |1 1 I|mlf24           |��� ���������� � ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R_e,R_i,R0_o,R0_u,R0_ad,R_ed,R0_id,R0_od,R0_ud
     &,R_af,R0_ef,R0_if,R0_of,R0_uf
      LOGICAL*1 L0_ak,L0_ek,L0_ik,L0_ok
      REAL*4 R_uk,R0_al,R_el,R_il,R_ol,R_ul,R0_am,R0_em,R0_im
     &,R0_om,R0_um,R0_ap,R0_ep,R0_ip,R0_op,R0_up
      LOGICAL*1 L0_ar
      REAL*4 R0_er,R0_ir,R0_or,R0_ur,R0_as
      LOGICAL*1 L0_es,L_is
      REAL*4 R0_os,R0_us,R0_at,R0_et,R0_it,R_ot,R0_ut,R0_av
     &,R0_ev
      LOGICAL*1 L0_iv,L0_ov
      REAL*4 R0_uv
      LOGICAL*1 L0_ax,L0_ex
      REAL*4 R0_ix,R0_ox
      LOGICAL*1 L0_ux,L0_abe,L0_ebe,L0_ibe,L0_obe,L0_ube
      REAL*4 R0_ade,R_ede
      LOGICAL*1 L_ide
      REAL*4 R0_ode,R_ude
      LOGICAL*1 L_afe,L0_efe,L0_ife,L0_ofe,L0_ufe,L0_ake,L_eke
     &,L0_ike,L0_oke,L_uke,L0_ale,L0_ele,L0_ile
      REAL*4 R0_ole,R0_ule
      LOGICAL*1 L0_ame,L0_eme
      REAL*4 R0_ime,R0_ome,R_ume
      LOGICAL*1 L0_ape,L0_epe
      REAL*4 R0_ipe,R_ope
      LOGICAL*1 L0_upe,L_are
      REAL*4 R0_ere,R_ire
      LOGICAL*1 L0_ore,L_ure
      REAL*4 R_ase,R_ese
      LOGICAL*1 L0_ise,L_ose,L0_use,L0_ate,L0_ete,L0_ite,L_ote
     &,L0_ute,L_ave,L_eve,L0_ive,L_ove,L0_uve,L0_axe,L_exe
     &,L0_ixe,L0_oxe,L_uxe,L0_abi,L_ebi,L0_ibi
      REAL*4 R0_obi,R0_ubi,R0_adi
      LOGICAL*1 L0_edi
      REAL*4 R0_idi
      LOGICAL*1 L0_odi,L0_udi
      REAL*4 R_afi,R_efi
      LOGICAL*1 L0_ifi,L0_ofi,L_ufi,L0_aki,L_eki,L0_iki,L_oki
     &,L_uki,L0_ali,L0_eli,L_ili,L_oli,L0_uli,L0_ami,L0_emi
     &,L0_imi,L0_omi,L0_umi
      CHARACTER*20 C20_api,C20_epi,C20_ipi,C20_opi,C20_upi
      LOGICAL*1 L_ari,L0_eri,L_iri,L_ori,L0_uri,L0_asi,L_esi
     &,L_isi,L_osi,L_usi,L_ati,L_eti,L_iti,L_oti,L_uti,L_avi
     &,L_evi,L_ivi,L_ovi,L_uvi,L_axi,L_exi
      LOGICAL*1 L_ixi
      REAL*4 R_oxi,R_uxi,R_abo,R_ebo,R_ibo
      INTEGER*4 I_obo,I0_ubo,I0_ado
      REAL*4 R_edo,R_ido
      LOGICAL*1 L0_odo,L0_udo,L_afo
      REAL*4 R0_efo,R0_ifo,R0_ofo,R0_ufo,R0_ako,R0_eko,R0_iko
      LOGICAL*1 L0_oko,L0_uko
      CHARACTER*20 C20_alo,C20_elo,C20_ilo,C20_olo,C20_ulo
     &,C20_amo,C20_emo,C20_imo,C20_omo,C20_umo
      INTEGER*4 I0_apo,I0_epo
      LOGICAL*1 L0_ipo
      INTEGER*4 I_opo,I0_upo,I0_aro,I_ero
      REAL*4 R_iro,R_oro,R_uro,R_aso
      LOGICAL*1 L_eso,L_iso
      INTEGER*4 I_oso,I0_uso,I0_ato,I_eto,I0_ito,I0_oto,I0_uto
     &,I0_avo,I_evo,I0_ivo,I0_ovo,I_uvo
      CHARACTER*20 C20_axo,C20_exo,C20_ixo,C20_oxo,C20_uxo
      CHARACTER*8 C8_abu
      LOGICAL*1 L0_ebu
      CHARACTER*8 C8_ibu
      LOGICAL*1 L0_obu
      CHARACTER*8 C8_ubu
      LOGICAL*1 L0_adu
      INTEGER*4 I0_edu
      LOGICAL*1 L0_idu
      INTEGER*4 I0_odu
      LOGICAL*1 L0_udu
      INTEGER*4 I0_afu,I0_efu,I0_ifu
      LOGICAL*1 L0_ofu
      INTEGER*4 I0_ufu,I0_aku,I0_eku
      REAL*4 R_iku,R_oku
      LOGICAL*1 L_uku
      REAL*4 R_alu,R_elu
      INTEGER*4 I_ilu,I0_olu,I0_ulu
      LOGICAL*1 L_amu,L_emu,L_imu,L0_omu
      INTEGER*4 I_umu,I0_apu,I0_epu,I_ipu,I0_opu,I0_upu
      LOGICAL*1 L0_aru,L0_eru,L_iru,L0_oru,L_uru,L_asu,L_esu
     &,L_isu,L_osu,L0_usu,L0_atu,L0_etu,L0_itu,L_otu,L0_utu
     &,L0_avu,L0_evu,L0_ivu,L_ovu,L0_uvu,L_axu,L_exu
      REAL*4 R0_ixu
      REAL*8 R8_oxu
      LOGICAL*1 L0_uxu,L0_abad
      REAL*4 R0_ebad,R_ibad,R0_obad,R0_ubad
      REAL*8 R8_adad
      LOGICAL*1 L0_edad,L0_idad,L_odad,L0_udad,L0_afad,L_efad
      INTEGER*4 I_ifad,I0_ofad,I0_ufad
      LOGICAL*1 L_akad,L_ekad,L0_ikad,L0_okad,L0_ukad,L0_alad
     &,L_elad,L0_ilad,L0_olad,L0_ulad,L0_amad,L0_emad,L0_imad
     &,L0_omad,L_umad,L0_apad,L0_epad,L0_ipad,L0_opad,L_upad

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_ox=R_af
C TOLKATEL_HANDLER.fmg( 349, 461):pre: ����������� ��
      R0_om=R_il
C TOLKATEL_HANDLER.fmg( 394, 406):pre: ����������� ��
      R0_ap=R_el
C TOLKATEL_HANDLER.fmg( 386, 412):pre: ����������� ��
      R0_at=R_ol
C TOLKATEL_HANDLER.fmg( 349, 414):pre: ����������� ��
      R0_ur=R_ul
C TOLKATEL_HANDLER.fmg( 319, 403):pre: ����������� ��
      R0_ode=R_ude
C TOLKATEL_HANDLER.fmg( 260, 346):pre: �������� ��������� ������,nv2dyn$99Dasha
      R0_ade=R_ede
C TOLKATEL_HANDLER.fmg( 260, 334):pre: �������� ��������� ������,nv2dyn$100Dasha
      R0_ere=R_ire
C TOLKATEL_HANDLER.fmg( 245, 310):pre: �������� ��������� ������,nv2dyn$82Dasha
      R0_ipe=R_ope
C TOLKATEL_HANDLER.fmg( 245, 296):pre: �������� ��������� ������,nv2dyn$83Dasha
      !��������� R0_u = TOLKATEL_HANDLERC?? /`p_UP`/
      R0_u=R_i
C TOLKATEL_HANDLER.fmg( 374, 460):���������
      R0_o = 0.01
C TOLKATEL_HANDLER.fmg( 374, 457):��������� (RE4) (�������)
      R0_ad = R0_u + (-R0_o)
C TOLKATEL_HANDLER.fmg( 377, 458):��������
      !��������� R0_od = TOLKATEL_HANDLERC?? /`p_LOW`/
      R0_od=R_ed
C TOLKATEL_HANDLER.fmg( 372, 439):���������
      R0_id = 0.01
C TOLKATEL_HANDLER.fmg( 372, 436):��������� (RE4) (�������)
      R0_ud = R0_od + R0_id
C TOLKATEL_HANDLER.fmg( 375, 438):��������
      R0_ef = 1.0
C TOLKATEL_HANDLER.fmg( 360, 422):��������� (RE4) (�������)
      R0_if = 1.0
C TOLKATEL_HANDLER.fmg( 360, 428):��������� (RE4) (�������)
      R0_al = R_ot * R_uk
C TOLKATEL_HANDLER.fmg( 352, 442):����������
      R0_of = R0_al + (-R0_ef)
C TOLKATEL_HANDLER.fmg( 363, 424):��������
      R0_uf = R0_al + R0_if
C TOLKATEL_HANDLER.fmg( 363, 430):��������
      R0_am = 0.0
C TOLKATEL_HANDLER.fmg( 428, 414):��������� (RE4) (�������),nv2dyn$57Dasha
      R0_im = 0.99
C TOLKATEL_HANDLER.fmg( 410, 416):��������� (RE4) (�������),nv2dyn$60Dasha
      if(.NOT.L_ovi) then
         R0_up=R0_om
      endif
C TOLKATEL_HANDLER.fmg( 398, 412):���� � ������������� �������
      R0_ep = 1.0
C TOLKATEL_HANDLER.fmg( 363, 412):��������� (RE4) (�������)
      R0_ip = 0.0
C TOLKATEL_HANDLER.fmg( 355, 412):��������� (RE4) (�������)
      R0_ir = 1.0e-10
C TOLKATEL_HANDLER.fmg( 333, 390):��������� (RE4) (�������)
      R0_os = 0.0
C TOLKATEL_HANDLER.fmg( 343, 404):��������� (RE4) (�������)
      R0_as = R0_ur + (-R_ot)
C TOLKATEL_HANDLER.fmg( 322, 396):��������
      R0_ut = 0.33
C TOLKATEL_HANDLER.fmg( 284, 473):��������� (RE4) (�������),nv2dyn$59Dasha
      R0_av = R_oxi * R0_ut
C TOLKATEL_HANDLER.fmg( 290, 474):����������
      if(L_eti) then
         R0_ofo=R0_av
      else
         R0_ofo=R_oxi
      endif
C TOLKATEL_HANDLER.fmg( 296, 476):���� RE IN LO CH7,nv2dyn$40Dasha
      R0_ev = 0.999999
C TOLKATEL_HANDLER.fmg( 146, 476):��������� (RE4) (�������),nv2dyn$60Dasha
      L0_iv=R_ume.gt.R0_ev
C TOLKATEL_HANDLER.fmg( 154, 477):���������� >,nv2dyn$45Dasha
      L0_ov = L_avi.AND.L0_iv
C TOLKATEL_HANDLER.fmg( 164, 483):�
      R0_uv = 0.000001
C TOLKATEL_HANDLER.fmg( 151, 426):��������� (RE4) (�������),nv2dyn$61Dasha
      L0_ax=R_ume.lt.R0_uv
C TOLKATEL_HANDLER.fmg( 158, 428):���������� <,nv2dyn$46Dasha
      L0_ex = L_iti.AND.L0_ax
C TOLKATEL_HANDLER.fmg( 164, 432):�
      R0_ole = 0.000001
C TOLKATEL_HANDLER.fmg( 218, 258):��������� (RE4) (�������),nv2dyn$61Dasha
      R0_ule = 0.999999
C TOLKATEL_HANDLER.fmg( 217, 264):��������� (RE4) (�������),nv2dyn$60Dasha
      R0_ime = 0.000001
C TOLKATEL_HANDLER.fmg( 218, 275):��������� (RE4) (�������),nv2dyn$61Dasha
      L0_ape=R_ume.lt.R0_ime
C TOLKATEL_HANDLER.fmg( 222, 276):���������� <,nv2dyn$46Dasha
      L0_uli = L_iti.AND.L0_ape
C TOLKATEL_HANDLER.fmg( 254, 274):�
      R0_ome = 0.999999
C TOLKATEL_HANDLER.fmg( 216, 281):��������� (RE4) (�������),nv2dyn$60Dasha
      L0_epe=R_ume.gt.R0_ome
C TOLKATEL_HANDLER.fmg( 222, 282):���������� >,nv2dyn$45Dasha
      L0_ami = L_avi.AND.L0_epe
C TOLKATEL_HANDLER.fmg( 254, 286):�
      if(.not.L_eti) then
         R_ope=0.0
      elseif(.not.L_are) then
         R_ope=R_ase
      else
         R_ope=max(R0_ipe-deltat,0.0)
      endif
      L0_upe=L_eti.and.R_ope.le.0.0
      L_are=L_eti
C TOLKATEL_HANDLER.fmg( 245, 296):�������� ��������� ������,nv2dyn$83Dasha
      if(.not.L_eti) then
         R_ire=0.0
      elseif(.not.L_ure) then
         R_ire=R_ese
      else
         R_ire=max(R0_ere-deltat,0.0)
      endif
      L0_ore=L_eti.and.R_ire.le.0.0
      L_ure=L_eti
C TOLKATEL_HANDLER.fmg( 245, 310):�������� ��������� ������,nv2dyn$82Dasha
      R0_obi = 0.01
C TOLKATEL_HANDLER.fmg(  36, 282):��������� (RE4) (�������),nv2dyn$74Dasha
      R0_ubi = R0_obi + R_afi
C TOLKATEL_HANDLER.fmg(  40, 281):��������
      R0_adi = 0.01
C TOLKATEL_HANDLER.fmg(  36, 288):��������� (RE4) (�������),nv2dyn$74Dasha
      R0_idi = (-R0_adi) + R_afi
C TOLKATEL_HANDLER.fmg(  40, 288):��������
      C20_ipi = '��������� 2'
C TOLKATEL_HANDLER.fmg(  28, 312):��������� ���������� CH20 (CH30) (�������)
      C20_opi = '������� ���'
C TOLKATEL_HANDLER.fmg(  28, 314):��������� ���������� CH20 (CH30) (�������)
      C20_api = '��������� 1'
C TOLKATEL_HANDLER.fmg(  44, 310):��������� ���������� CH20 (CH30) (�������)
      L_ori=(L_iri.or.L_ori).and..not.(L_ari)
      L0_eri=.not.L_ori
C TOLKATEL_HANDLER.fmg( 296, 274):RS �������
      L0_edad = L_eso.OR.L_exi.OR.L_afo
C TOLKATEL_HANDLER.fmg(  74, 387):���
      L0_udad = L_iso.OR.L_ixi
C TOLKATEL_HANDLER.fmg(  71, 432):���
      I0_ado = z'01000003'
C TOLKATEL_HANDLER.fmg( 130, 471):��������� ������������� IN (�������)
      I0_ubo = z'0100000A'
C TOLKATEL_HANDLER.fmg( 130, 469):��������� ������������� IN (�������)
      R0_ifo = 0.0
C TOLKATEL_HANDLER.fmg( 320, 467):��������� (RE4) (�������)
      R0_iko = 0.0
C TOLKATEL_HANDLER.fmg( 306, 467):��������� (RE4) (�������)
      C20_elo = ''
C TOLKATEL_HANDLER.fmg( 176, 359):��������� ���������� CH20 (CH30) (�������)
      C20_alo = '�������'
C TOLKATEL_HANDLER.fmg( 176, 357):��������� ���������� CH20 (CH30) (�������)
      C20_ilo = '������'
C TOLKATEL_HANDLER.fmg( 197, 360):��������� ���������� CH20 (CH30) (�������)
      C20_emo = ''
C TOLKATEL_HANDLER.fmg(  82, 361):��������� ���������� CH20 (CH30) (�������)
      C20_amo = '� ��������� 2'
C TOLKATEL_HANDLER.fmg(  82, 359):��������� ���������� CH20 (CH30) (�������)
      C20_imo = '� ��������� 1'
C TOLKATEL_HANDLER.fmg( 104, 360):��������� ���������� CH20 (CH30) (�������)
      I0_apo = z'0100000A'
C TOLKATEL_HANDLER.fmg( 228, 471):��������� ������������� IN (�������)
      I0_epo = z'01000003'
C TOLKATEL_HANDLER.fmg( 228, 473):��������� ������������� IN (�������)
      I0_aro = z'0100000A'
C TOLKATEL_HANDLER.fmg( 228, 492):��������� ������������� IN (�������)
      I0_upo = z'01000003'
C TOLKATEL_HANDLER.fmg( 228, 490):��������� ������������� IN (�������)
      L_iru=R_oro.ne.R_iro
      R_iro=R_oro
C TOLKATEL_HANDLER.fmg(  22, 396):���������� ������������� ������
      L_uru=R_aso.ne.R_uro
      R_uro=R_aso
C TOLKATEL_HANDLER.fmg(  27, 452):���������� ������������� ������
      I0_ato = z'01000003'
C TOLKATEL_HANDLER.fmg( 146, 364):��������� ������������� IN (�������)
      I0_uso = z'01000010'
C TOLKATEL_HANDLER.fmg( 146, 362):��������� ������������� IN (�������)
      I0_oto = z'01000003'
C TOLKATEL_HANDLER.fmg( 125, 454):��������� ������������� IN (�������)
      I0_ito = z'01000010'
C TOLKATEL_HANDLER.fmg( 125, 452):��������� ������������� IN (�������)
      I0_avo = z'01000003'
C TOLKATEL_HANDLER.fmg( 193, 432):��������� ������������� IN (�������)
      I0_uto = z'0100000A'
C TOLKATEL_HANDLER.fmg( 193, 430):��������� ������������� IN (�������)
      I0_ovo = z'01000003'
C TOLKATEL_HANDLER.fmg( 201, 382):��������� ������������� IN (�������)
      I0_ivo = z'0100000A'
C TOLKATEL_HANDLER.fmg( 201, 380):��������� ������������� IN (�������)
      I0_epu = z'01000003'
C TOLKATEL_HANDLER.fmg( 123, 378):��������� ������������� IN (�������)
      I0_apu = z'0100000A'
C TOLKATEL_HANDLER.fmg( 123, 376):��������� ������������� IN (�������)
      C20_axo = '� ��������'
C TOLKATEL_HANDLER.fmg(  58, 374):��������� ���������� CH20 (CH30) (�������)
      C20_ixo = '� �������'
C TOLKATEL_HANDLER.fmg(  43, 374):��������� ���������� CH20 (CH30) (�������)
      C20_oxo = ''
C TOLKATEL_HANDLER.fmg(  43, 376):��������� ���������� CH20 (CH30) (�������)
      C8_abu = 'init'
C TOLKATEL_HANDLER.fmg(  26, 383):��������� ���������� CH8 (�������)
      call chcomp(C8_ubu,C8_abu,L0_ebu)
C TOLKATEL_HANDLER.fmg(  31, 386):���������� ���������
      C8_ibu = 'work'
C TOLKATEL_HANDLER.fmg(  26, 425):��������� ���������� CH8 (�������)
      call chcomp(C8_ubu,C8_ibu,L0_obu)
C TOLKATEL_HANDLER.fmg(  31, 428):���������� ���������
      if(L0_obu) then
         C20_omo=C20_amo
      else
         C20_omo=C20_emo
      endif
C TOLKATEL_HANDLER.fmg(  86, 360):���� RE IN LO CH20
      if(L0_ebu) then
         C20_umo=C20_imo
      else
         C20_umo=C20_omo
      endif
C TOLKATEL_HANDLER.fmg( 108, 360):���� RE IN LO CH20
      if(L0_obu) then
         C20_olo=C20_alo
      else
         C20_olo=C20_elo
      endif
C TOLKATEL_HANDLER.fmg( 180, 358):���� RE IN LO CH20
      if(L0_ebu) then
         C20_ulo=C20_ilo
      else
         C20_ulo=C20_olo
      endif
C TOLKATEL_HANDLER.fmg( 202, 360):���� RE IN LO CH20
      if(L0_obu) then
         C20_exo=C20_ixo
      else
         C20_exo=C20_oxo
      endif
C TOLKATEL_HANDLER.fmg(  47, 375):���� RE IN LO CH20
      if(L0_ebu) then
         C20_uxo=C20_axo
      else
         C20_uxo=C20_exo
      endif
C TOLKATEL_HANDLER.fmg(  63, 374):���� RE IN LO CH20
      I0_edu = z'0100008E'
C TOLKATEL_HANDLER.fmg( 178, 398):��������� ������������� IN (�������)
      I0_odu = z'01000086'
C TOLKATEL_HANDLER.fmg( 172, 451):��������� ������������� IN (�������)
      I0_efu = z'01000003'
C TOLKATEL_HANDLER.fmg( 160, 464):��������� ������������� IN (�������)
      I0_ifu = z'01000010'
C TOLKATEL_HANDLER.fmg( 160, 466):��������� ������������� IN (�������)
      I0_eku = z'01000003'
C TOLKATEL_HANDLER.fmg( 167, 412):��������� ������������� IN (�������)
      I0_aku = z'01000010'
C TOLKATEL_HANDLER.fmg( 167, 410):��������� ������������� IN (�������)
      L_imu=R_oku.ne.R_iku
      R_iku=R_oku
C TOLKATEL_HANDLER.fmg(  22, 416):���������� ������������� ������
      L0_omu = L_imu.OR.L_emu.OR.L_amu
C TOLKATEL_HANDLER.fmg(  56, 413):���
      L_uku=R_elu.ne.R_alu
      R_alu=R_elu
C TOLKATEL_HANDLER.fmg(  27, 438):���������� ������������� ������
      L0_oru = L_uku.AND.L0_obu
C TOLKATEL_HANDLER.fmg(  38, 436):�
      L0_avu = L_uru.OR.L0_oru
C TOLKATEL_HANDLER.fmg(  60, 438):���
      L0_ile = L_esu.AND.L0_avu
C TOLKATEL_HANDLER.fmg( 240, 346):�
      L0_ake = L0_avu.OR.(.NOT.L_asu)
C TOLKATEL_HANDLER.fmg( 244, 330):���
      L0_eru = L_uku.AND.L0_ebu
C TOLKATEL_HANDLER.fmg(  37, 394):�
      L0_utu = L_iru.OR.L0_eru
C TOLKATEL_HANDLER.fmg(  60, 396):���
      L0_ele = L_asu.AND.L0_utu
C TOLKATEL_HANDLER.fmg( 240, 334):�
      L_eke=(L0_ele.or.L_eke).and..not.(L0_ake)
      L0_ike=.not.L_eke
C TOLKATEL_HANDLER.fmg( 251, 332):RS �������,156
      if(.not.L_eke) then
         R_ede=0.0
      elseif(.not.L_ide) then
         R_ede=R_ase
      else
         R_ede=max(R0_ade-deltat,0.0)
      endif
      L0_efe=L_eke.and.R_ede.le.0.0
      L_ide=L_eke
C TOLKATEL_HANDLER.fmg( 260, 334):�������� ��������� ������,nv2dyn$100Dasha
      L_oki=(L0_efe.or.L_oki).and..not.(L_ebi)
      L0_ife=.not.L_oki
C TOLKATEL_HANDLER.fmg( 276, 332):RS �������,nv2dyn$96Dasha
      L0_oke = (.NOT.L_esu).OR.L0_utu
C TOLKATEL_HANDLER.fmg( 246, 342):���
      L_uke=(L0_ile.or.L_uke).and..not.(L0_oke)
      L0_ale=.not.L_uke
C TOLKATEL_HANDLER.fmg( 251, 344):RS �������,155
      if(.not.L_uke) then
         R_ude=0.0
      elseif(.not.L_afe) then
         R_ude=R_ese
      else
         R_ude=max(R0_ode-deltat,0.0)
      endif
      L0_ofe=L_uke.and.R_ude.le.0.0
      L_afe=L_uke
C TOLKATEL_HANDLER.fmg( 260, 346):�������� ��������� ������,nv2dyn$99Dasha
      L_uki=(L0_ofe.or.L_uki).and..not.(L_eve)
      L0_ufe=.not.L_uki
C TOLKATEL_HANDLER.fmg( 276, 344):RS �������,nv2dyn$95Dasha
      I0_opu = z'0100000E'
C TOLKATEL_HANDLER.fmg( 197, 402):��������� ������������� IN (�������)
      I0_olu = z'01000007'
C TOLKATEL_HANDLER.fmg( 154, 383):��������� ������������� IN (�������)
      I0_ulu = z'01000003'
C TOLKATEL_HANDLER.fmg( 154, 385):��������� ������������� IN (�������)
      I0_ofad = z'0100000E'
C TOLKATEL_HANDLER.fmg( 190, 458):��������� ������������� IN (�������)
      L0_itu=.false.
C TOLKATEL_HANDLER.fmg(  72, 418):��������� ���������� (�������)
      L0_etu=.false.
C TOLKATEL_HANDLER.fmg(  72, 416):��������� ���������� (�������)
      R0_ixu = 0.1
C TOLKATEL_HANDLER.fmg( 254, 366):��������� (RE4) (�������)
      L_exu=R8_oxu.lt.R0_ixu
C TOLKATEL_HANDLER.fmg( 258, 367):���������� <
      R0_ebad = 0.0
C TOLKATEL_HANDLER.fmg( 265, 386):��������� (RE4) (�������)
      L0_uve = (.NOT.L_axi).AND.L_ove
C TOLKATEL_HANDLER.fmg( 111, 277):�
C label 246  try246=try246-1
      L0_axe = L_eve.AND.L0_uve
C TOLKATEL_HANDLER.fmg( 115, 278):�
      L_ove=(L_axi.or.L_ove).and..not.(L0_axe)
      L0_ive=.not.L_ove
C TOLKATEL_HANDLER.fmg( 106, 274):RS �������,nv2dyn$103Dasha
C sav1=L0_uve
      L0_uve = (.NOT.L_axi).AND.L_ove
C TOLKATEL_HANDLER.fmg( 111, 277):recalc:�
C if(sav1.ne.L0_uve .and. try246.gt.0) goto 246
      L0_use = (.NOT.L_axi).AND.L_ose
C TOLKATEL_HANDLER.fmg( 116, 263):�
C label 250  try250=try250-1
      L0_ate = L_ebi.AND.L0_use
C TOLKATEL_HANDLER.fmg( 120, 264):�
      L_ose=(L_axi.or.L_ose).and..not.(L0_ate)
      L0_ise=.not.L_ose
C TOLKATEL_HANDLER.fmg( 112, 260):RS �������,nv2dyn$106Dasha
C sav1=L0_use
      L0_use = (.NOT.L_axi).AND.L_ose
C TOLKATEL_HANDLER.fmg( 116, 263):recalc:�
C if(sav1.ne.L0_use .and. try250.gt.0) goto 250
      L0_edi=R_efi.gt.R0_idi
C TOLKATEL_HANDLER.fmg(  45, 292):���������� >,nv2dyn$84Dasha
C label 255  try255=try255-1
      if(L_ovi) then
         R0_ix=R0_ox
      else
         R0_ix=R_ebo
      endif
C TOLKATEL_HANDLER.fmg( 348, 455):���� RE IN LO CH7
      L0_ek=R_edo.lt.R0_uf
C TOLKATEL_HANDLER.fmg( 343, 430):���������� <
      L0_ak=R_edo.gt.R0_of
C TOLKATEL_HANDLER.fmg( 343, 424):���������� >
      L0_ik = L0_ek.AND.L0_ak
C TOLKATEL_HANDLER.fmg( 350, 428):�
      L0_ok = L_axi.AND.L0_ik
C TOLKATEL_HANDLER.fmg( 354, 434):�
      if(L0_ok) then
         R_edo=R0_al
      else
         R_edo=R0_ix
      endif
C TOLKATEL_HANDLER.fmg( 358, 454):���� RE IN LO CH7
      L0_obe=R_edo.lt.R0_ud
C TOLKATEL_HANDLER.fmg( 384, 439):���������� <
      L0_ibe = L0_obe.AND.(.NOT.L_iti)
C TOLKATEL_HANDLER.fmg( 426, 438):�
      L_akad = L_oti.OR.L0_ibe.OR.L_evi
C TOLKATEL_HANDLER.fmg( 430, 438):���
      L0_umi = L_uti.AND.L_akad
C TOLKATEL_HANDLER.fmg( 262, 320):�
      L0_ube=R_edo.gt.R0_ad
C TOLKATEL_HANDLER.fmg( 386, 460):���������� >
      L0_ebe = L0_ube.AND.(.NOT.L_avi)
C TOLKATEL_HANDLER.fmg( 423, 458):�
      L_ekad = L_uti.OR.L0_ebe.OR.L_ivi
C TOLKATEL_HANDLER.fmg( 429, 458):���
      L0_omi = L_oti.AND.L_ekad
C TOLKATEL_HANDLER.fmg( 262, 316):�
      L0_imi = L0_ore.AND.(.NOT.L_ekad)
C TOLKATEL_HANDLER.fmg( 254, 308):�
      L0_emi = L0_upe.AND.(.NOT.L_akad)
C TOLKATEL_HANDLER.fmg( 250, 295):�
      L0_ibi=R_efi.lt.R0_ubi
C TOLKATEL_HANDLER.fmg(  47, 286):���������� <,nv2dyn$85Dasha
      L0_odi = L0_edi.AND.L0_ibi
C TOLKATEL_HANDLER.fmg(  52, 291):�
      L0_udi = L0_odi.AND.L_axi
C TOLKATEL_HANDLER.fmg(  64, 283):�
      L_uxe=(L_ebi.or.L_uxe).and..not.(L_akad)
      L0_abi=.not.L_uxe
C TOLKATEL_HANDLER.fmg(  64, 276):RS �������,nv2dyn$101Dasha
      L0_oxe = L0_udi.AND.L_uxe
C TOLKATEL_HANDLER.fmg(  83, 282):�
      L_exe=(L0_oxe.or.L_exe).and..not.(L0_axe)
      L0_ixe=.not.L_exe
C TOLKATEL_HANDLER.fmg( 120, 280):RS �������,nv2dyn$102Dasha
      L_ote=(L_eve.or.L_ote).and..not.(L_ekad)
      L0_ite=.not.L_ote
C TOLKATEL_HANDLER.fmg(  70, 265):RS �������,nv2dyn$104Dasha
      L0_ute = L0_udi.AND.L_ote
C TOLKATEL_HANDLER.fmg(  83, 268):�
      L_ave=(L0_ute.or.L_ave).and..not.(L0_ate)
      L0_ete=.not.L_ave
C TOLKATEL_HANDLER.fmg( 128, 266):RS �������,nv2dyn$105Dasha
      L_oli = L_exe.OR.L_ave
C TOLKATEL_HANDLER.fmg( 142, 281):���
      L_eki=(L_ekad.or.L_eki).and..not.(L_akad)
      L0_ofi=.not.L_eki
C TOLKATEL_HANDLER.fmg( 117, 304):RS �������,nv2dyn$86Dasha
      L0_iki = L_ivi.AND.(.NOT.L_eki)
C TOLKATEL_HANDLER.fmg( 126, 307):�
      L_ufi=(L_akad.or.L_ufi).and..not.(L_ekad)
      L0_ifi=.not.L_ufi
C TOLKATEL_HANDLER.fmg( 116, 293):RS �������,nv2dyn$87Dasha
      L0_aki = L_evi.AND.(.NOT.L_ufi)
C TOLKATEL_HANDLER.fmg( 126, 298):�
      L_ili = L0_iki.OR.L0_aki
C TOLKATEL_HANDLER.fmg( 132, 306):���
      L0_eme=R_efi.gt.R0_ule
C TOLKATEL_HANDLER.fmg( 222, 265):���������� >,nv2dyn$45Dasha
      L0_eli = L_usi.AND.L0_eme
C TOLKATEL_HANDLER.fmg( 254, 268):�
      L0_ame=R_efi.lt.R0_ole
C TOLKATEL_HANDLER.fmg( 222, 259):���������� <,nv2dyn$46Dasha
      L0_ali = L_isi.AND.L0_ame
C TOLKATEL_HANDLER.fmg( 254, 257):�
      L0_uri =.NOT.(L_odad.OR.L_afo.OR.L0_umi.OR.L0_omi.OR.L0_imi.OR.L0_
     &emi.OR.L0_ami.OR.L0_uli.OR.L_ovi.OR.L_ati.OR.L_osi.OR.L_oli.OR.L_i
     &li.OR.L0_eli.OR.L0_ali.OR.L_uki.OR.L_oki)
C TOLKATEL_HANDLER.fmg( 288, 308):���
      L0_asi =.NOT.(L0_uri)
C TOLKATEL_HANDLER.fmg( 298, 280):���
      L_efad = L0_asi.OR.L_ori
C TOLKATEL_HANDLER.fmg( 302, 280):���
      L0_atu = L_efad.OR.L_isu
C TOLKATEL_HANDLER.fmg(  66, 450):���
      L0_afad = L0_avu.AND.(.NOT.L0_atu).AND.(.NOT.L_esu)
C TOLKATEL_HANDLER.fmg(  74, 443):�
      L0_epad = L0_afad.OR.L0_udad.OR.L_odad
C TOLKATEL_HANDLER.fmg(  78, 436):���
      L0_usu = L_efad.OR.L_osu
C TOLKATEL_HANDLER.fmg(  63, 404):���
      L0_idad = (.NOT.L0_usu).AND.L0_utu.AND.(.NOT.L_asu)
C TOLKATEL_HANDLER.fmg(  74, 396):�
      L0_ulad = L0_idad.OR.L0_edad
C TOLKATEL_HANDLER.fmg(  78, 394):���
      L0_ivu = L0_epad.OR.L0_ulad
C TOLKATEL_HANDLER.fmg(  84, 406):���
      L0_evu = L_akad.OR.L0_omu.OR.L_ekad
C TOLKATEL_HANDLER.fmg(  68, 410):���
      L_ovu=(L0_evu.or.L_ovu).and..not.(L0_ivu)
      L0_uvu=.not.L_ovu
C TOLKATEL_HANDLER.fmg( 114, 408):RS �������,10
      L_axu = L_emu.OR.L_ovu
C TOLKATEL_HANDLER.fmg( 125, 410):���
      L0_ipad = L_ekad.AND.(.NOT.L_uti)
C TOLKATEL_HANDLER.fmg(  82, 462):�
      L0_emad = (.NOT.L_axu).AND.L0_ipad
C TOLKATEL_HANDLER.fmg( 113, 462):�
      L0_ikad = L_axu.OR.L_upad
C TOLKATEL_HANDLER.fmg( 106, 416):���
      L0_abe = L_ekad.AND.L_osi
C TOLKATEL_HANDLER.fmg(  86, 348):�
      L0_ux = L_akad.AND.L_ati
C TOLKATEL_HANDLER.fmg(  86, 336):�
      L0_aru = L0_abe.OR.L0_ux
C TOLKATEL_HANDLER.fmg(  94, 346):���
      L0_omad = L0_ipad.OR.L0_ikad.OR.L0_ulad.OR.L0_aru
C TOLKATEL_HANDLER.fmg( 112, 430):���
      L0_opad = (.NOT.L0_ipad).AND.L0_epad
C TOLKATEL_HANDLER.fmg( 112, 438):�
      L_umad=(L0_opad.or.L_umad).and..not.(L0_omad)
      L0_apad=.not.L_umad
C TOLKATEL_HANDLER.fmg( 119, 436):RS �������,1
      L0_imad = (.NOT.L0_emad).AND.L_umad
C TOLKATEL_HANDLER.fmg( 138, 438):�
      L0_udo = L0_imad.AND.(.NOT.L_afo)
C TOLKATEL_HANDLER.fmg( 276, 466):�
      L0_uko = L0_udo.OR.L_odad
C TOLKATEL_HANDLER.fmg( 289, 464):���
      if(L0_uko) then
         R0_ako=R0_ofo
      else
         R0_ako=R0_iko
      endif
C TOLKATEL_HANDLER.fmg( 311, 477):���� RE IN LO CH7
      L0_olad = L_akad.AND.(.NOT.L_oti)
C TOLKATEL_HANDLER.fmg(  54, 365):�
      L0_alad = L0_ikad.OR.L0_olad.OR.L0_epad.OR.L0_aru
C TOLKATEL_HANDLER.fmg( 112, 386):���
      L0_amad = L0_ulad.AND.(.NOT.L0_olad)
C TOLKATEL_HANDLER.fmg( 112, 394):�
      L_elad=(L0_amad.or.L_elad).and..not.(L0_alad)
      L0_ilad=.not.L_elad
C TOLKATEL_HANDLER.fmg( 119, 392):RS �������,2
      L0_okad = (.NOT.L_axu).AND.L0_olad
C TOLKATEL_HANDLER.fmg( 111, 368):�
      L0_ukad = L_elad.AND.(.NOT.L0_okad)
C TOLKATEL_HANDLER.fmg( 138, 392):�
      L0_odo = L0_ukad.AND.(.NOT.L_odad)
C TOLKATEL_HANDLER.fmg( 276, 452):�
      L0_oko = L0_odo.OR.L_afo
C TOLKATEL_HANDLER.fmg( 289, 450):���
      if(L0_oko) then
         R0_ufo=R0_ofo
      else
         R0_ufo=R0_iko
      endif
C TOLKATEL_HANDLER.fmg( 311, 459):���� RE IN LO CH7
      R0_eko = R0_ako + (-R0_ufo)
C TOLKATEL_HANDLER.fmg( 317, 470):��������
      if(L_upad) then
         R0_efo=R0_ifo
      else
         R0_efo=R0_eko
      endif
C TOLKATEL_HANDLER.fmg( 324, 468):���� RE IN LO CH7
      R0_er = R0_efo + R0_as
C TOLKATEL_HANDLER.fmg( 328, 397):��������
      R0_or = R0_er * R0_as
C TOLKATEL_HANDLER.fmg( 332, 396):����������
      L0_es=R0_or.lt.R0_ir
C TOLKATEL_HANDLER.fmg( 338, 395):���������� <
      L_is=(L0_es.or.L_is).and..not.(.NOT.L_axi)
      L0_ar=.not.L_is
C TOLKATEL_HANDLER.fmg( 344, 393):RS �������
      if(L_is) then
         R0_us=R0_os
      else
         R0_us=R0_efo
      endif
C TOLKATEL_HANDLER.fmg( 346, 404):���� RE IN LO CH7
      R0_et = R0_at + R0_us
C TOLKATEL_HANDLER.fmg( 352, 406):��������
      R0_op=MAX(R0_ip,R0_et)
C TOLKATEL_HANDLER.fmg( 360, 407):��������
      R0_it=MIN(R0_ep,R0_op)
C TOLKATEL_HANDLER.fmg( 368, 408):�������
      if(L_ovi) then
         R0_up=R0_it
      endif
C TOLKATEL_HANDLER.fmg( 380, 412):���� � ������������� �������
      if(L_is) then
         R0_up=R_ot
      endif
C TOLKATEL_HANDLER.fmg( 338, 412):���� � ������������� �������
      if(L_ati) then
         R0_em=R0_im
      else
         R0_em=R0_up
      endif
C TOLKATEL_HANDLER.fmg( 414, 416):���� RE IN LO CH7,nv2dyn$56Dasha
      if(L_osi) then
         R_efi=R0_am
      else
         R_efi=R0_em
      endif
C TOLKATEL_HANDLER.fmg( 432, 416):���� RE IN LO CH7,nv2dyn$56Dasha
      R_ul=R0_up
C TOLKATEL_HANDLER.fmg( 319, 403):����������� ��
      R_ol=R0_up
C TOLKATEL_HANDLER.fmg( 349, 414):����������� ��
      if(L_ovi) then
         R0_um=R0_ap
      else
         R0_um=R0_it
      endif
C TOLKATEL_HANDLER.fmg( 387, 406):���� RE IN LO CH7
      R_el=R0_um
C TOLKATEL_HANDLER.fmg( 386, 412):����������� ��
      R_il=R0_um
C TOLKATEL_HANDLER.fmg( 394, 406):����������� ��
      R_ebo=R_ebo+deltat/R_abo*R0_efo
      if(R_ebo.gt.R_uxi) then
         R_ebo=R_uxi
      elseif(R_ebo.lt.R_ibo) then
         R_ebo=R_ibo
      endif
C TOLKATEL_HANDLER.fmg( 338, 455):����������,T_INT
      R_ido = R_e * R0_efo
C TOLKATEL_HANDLER.fmg( 355, 470):����������,2
      if(L0_ukad) then
         I_uvo=I0_ivo
      else
         I_uvo=I0_ovo
      endif
C TOLKATEL_HANDLER.fmg( 204, 380):���� RE IN LO CH7
      L0_adu = L0_ex.OR.L0_ukad
C TOLKATEL_HANDLER.fmg( 182, 396):���
      L0_uxu = L0_imad.OR.L0_ukad
C TOLKATEL_HANDLER.fmg( 250, 379):���
      L0_abad = L0_uxu.AND.(.NOT.L_exu)
C TOLKATEL_HANDLER.fmg( 266, 378):�
      if(L0_abad) then
         R0_ubad=R_ibad
      else
         R0_ubad=R0_ebad
      endif
C TOLKATEL_HANDLER.fmg( 268, 386):���� RE IN LO CH7
      if(L0_olad) then
         I_oso=I0_uso
      else
         I_oso=I0_ato
      endif
C TOLKATEL_HANDLER.fmg( 150, 363):���� RE IN LO CH7
      if(L0_olad) then
         I_umu=I0_apu
      else
         I_umu=I0_epu
      endif
C TOLKATEL_HANDLER.fmg( 126, 376):���� RE IN LO CH7
      if(L0_imad) then
         I_evo=I0_uto
      else
         I_evo=I0_avo
      endif
C TOLKATEL_HANDLER.fmg( 196, 430):���� RE IN LO CH7
      L0_idu = L0_ov.OR.L0_imad
C TOLKATEL_HANDLER.fmg( 174, 444):���
      if(L0_ipad) then
         I_obo=I0_ubo
      else
         I_obo=I0_ado
      endif
C TOLKATEL_HANDLER.fmg( 134, 470):���� RE IN LO CH7
      if(L0_ipad) then
         I_eto=I0_ito
      else
         I_eto=I0_oto
      endif
C TOLKATEL_HANDLER.fmg( 128, 452):���� RE IN LO CH7
      L_otu = L_efad.OR.L0_itu.OR.L0_etu.OR.L0_atu.OR.L0_usu
C TOLKATEL_HANDLER.fmg(  76, 416):���
      L0_ipo = L_efad.OR.L_otu
C TOLKATEL_HANDLER.fmg( 226, 483):���
      if(L0_ipo) then
         I_opo=I0_upo
      else
         I_opo=I0_aro
      endif
C TOLKATEL_HANDLER.fmg( 232, 491):���� RE IN LO CH7
      if(L_otu) then
         I_ero=I0_apo
      else
         I_ero=I0_epo
      endif
C TOLKATEL_HANDLER.fmg( 232, 472):���� RE IN LO CH7
      if(L_efad) then
         I_ilu=I0_olu
      else
         I_ilu=I0_ulu
      endif
C TOLKATEL_HANDLER.fmg( 158, 384):���� RE IN LO CH7
      L_esi = L0_uri.OR.L_iri
C TOLKATEL_HANDLER.fmg( 298, 286):���
      if(L_ekad) then
         C20_epi=C20_ipi
      else
         C20_epi=C20_opi
      endif
C TOLKATEL_HANDLER.fmg(  32, 312):���� RE IN LO CH20
      if(L_akad) then
         C20_upi=C20_api
      else
         C20_upi=C20_epi
      endif
C TOLKATEL_HANDLER.fmg(  48, 311):���� RE IN LO CH20
      L0_ofu = (.NOT.L0_ov).AND.L_ekad
C TOLKATEL_HANDLER.fmg( 165, 404):�
      if(L0_ofu) then
         I0_ufu=I0_aku
      else
         I0_ufu=I0_eku
      endif
C TOLKATEL_HANDLER.fmg( 170, 410):���� RE IN LO CH7
      if(L0_adu) then
         I0_upu=I0_edu
      else
         I0_upu=I0_ufu
      endif
C TOLKATEL_HANDLER.fmg( 181, 409):���� RE IN LO CH7
      if(L_efad) then
         I_ipu=I0_opu
      else
         I_ipu=I0_upu
      endif
C TOLKATEL_HANDLER.fmg( 200, 408):���� RE IN LO CH7
      L0_udu = L_akad.AND.(.NOT.L0_ex)
C TOLKATEL_HANDLER.fmg( 158, 458):�
      if(L0_udu) then
         I0_afu=I0_efu
      else
         I0_afu=I0_ifu
      endif
C TOLKATEL_HANDLER.fmg( 164, 465):���� RE IN LO CH7
      if(L0_idu) then
         I0_ufad=I0_odu
      else
         I0_ufad=I0_afu
      endif
C TOLKATEL_HANDLER.fmg( 176, 464):���� RE IN LO CH7
      if(L_efad) then
         I_ifad=I0_ofad
      else
         I_ifad=I0_ufad
      endif
C TOLKATEL_HANDLER.fmg( 194, 463):���� RE IN LO CH7
      R_af=R0_ix
C TOLKATEL_HANDLER.fmg( 349, 461):����������� ��
      R0_obad = R8_adad
C TOLKATEL_HANDLER.fmg( 264, 395):��������
C label 645  try645=try645-1
      R8_adad = R0_ubad + R0_obad
C TOLKATEL_HANDLER.fmg( 274, 394):��������
C sav1=R0_obad
      R0_obad = R8_adad
C TOLKATEL_HANDLER.fmg( 264, 395):recalc:��������
C if(sav1.ne.R0_obad .and. try645.gt.0) goto 645
      End
