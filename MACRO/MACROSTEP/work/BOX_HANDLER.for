      Subroutine BOX_HANDLER(ext_deltat,L_e,R_i,R_o,L_u,R_ad
     &,R_ed,L_id,R_od,R_ud,L_af,I_ek,L_ik,L_ok,I_ol)
C |L_e           |1 1 O|high_concentration_button_CMD*|[TF]����� ������ |F|
C |R_i           |4 4 S|high_concentration_button_ST*|��������� ������ "" |0.0|
C |R_o           |4 4 I|high_concentration_button|������� ������ ������ "" |0.0|
C |L_u           |1 1 O|alarm_vent_button_CMD*|[TF]����� ������ |F|
C |R_ad          |4 4 S|alarm_vent_button_ST*|��������� ������ "" |0.0|
C |R_ed          |4 4 I|alarm_vent_button|������� ������ ������ "" |0.0|
C |L_id          |1 1 O|inert_gas_button_CMD*|[TF]����� ������ |F|
C |R_od          |4 4 S|inert_gas_button_ST*|��������� ������ "" |0.0|
C |R_ud          |4 4 I|inert_gas_button|������� ������ ������ "" |0.0|
C |L_af          |1 1 I|alarm_vent      |��������� ����������||
C |I_ek          |2 4 O|LSR             |��������� ����� �����||
C |L_ik          |1 1 I|high_concentration|���������� ������������ ���������/�����||
C |L_ok          |1 1 I|inert_gas       |�������� �������� �����||
C |I_ol          |2 4 O|LS              |��������� �����||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      LOGICAL*1 L_e
      REAL*4 R_i,R_o
      LOGICAL*1 L_u
      REAL*4 R_ad,R_ed
      LOGICAL*1 L_id
      REAL*4 R_od,R_ud
      LOGICAL*1 L_af,L0_ef
      INTEGER*4 I0_if,I0_of
      LOGICAL*1 L0_uf
      INTEGER*4 I0_ak,I_ek
      LOGICAL*1 L_ik,L_ok,L0_uk
      INTEGER*4 I0_al,I0_el,I0_il,I_ol

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      L_e=R_o.ne.R_i
      R_i=R_o
C BOX_HANDLER.fmg( 172, 161):���������� ������������� ������
      L0_uf = L_ik.OR.L_e
C BOX_HANDLER.fmg( 204, 171):���
      L_u=R_ed.ne.R_ad
      R_ad=R_ed
C BOX_HANDLER.fmg( 146, 191):���������� ������������� ������
      L0_ef = L_af.OR.L_u
C BOX_HANDLER.fmg( 181, 194):���
      L_id=R_ud.ne.R_od
      R_od=R_ud
C BOX_HANDLER.fmg( 140, 168):���������� ������������� ������
      L0_uk = L_ok.OR.L_id
C BOX_HANDLER.fmg( 164, 176):���
      I0_ak = z'01000017'
C BOX_HANDLER.fmg( 206, 182):��������� ������������� IN (�������)
      I0_if = z'01000022'
C BOX_HANDLER.fmg( 182, 204):��������� ������������� IN (�������)
      I0_of = z'01000007'
C BOX_HANDLER.fmg( 182, 202):��������� ������������� IN (�������)
      if(L0_ef) then
         I_ek=I0_of
      else
         I_ek=I0_if
      endif
C BOX_HANDLER.fmg( 185, 202):���� RE IN LO CH7
      I0_el = z'01000023'
C BOX_HANDLER.fmg( 166, 185):��������� ������������� IN (�������)
      I0_il = z'01000005'
C BOX_HANDLER.fmg( 166, 183):��������� ������������� IN (�������)
      if(L0_uk) then
         I0_al=I0_il
      else
         I0_al=I0_el
      endif
C BOX_HANDLER.fmg( 170, 184):���� RE IN LO CH7
      if(L0_uf) then
         I_ol=I0_ak
      else
         I_ol=I0_al
      endif
C BOX_HANDLER.fmg( 209, 182):���� RE IN LO CH7
      End
