      Interface
      Subroutine TRANSPORTER(ext_deltat,R_e,R_i,L_o,L_u,L_ad
     &,L_ed,L_id,L_od,L_ud,R_af,R_ef,R_if,R_of,R_uf,R_ak,R_ek
     &,R_ik,R_ok,R_uk,R_al,R_el,R_il,R_ol,R_ul,R_am,R_em,R_im
     &,R_om,R_um,R_ap,L_ep,L_ip,L_op,L_up,L_ar,L_er,L_ir,L_or
     &,L_ur,L_as,L_es,L_is,L_os,L_us,L_at,L_et,L_it,L_ot,L_ut
     &,L_av,L_ev,L_iv,L_ov,L_uv,L_ax,L_ex,L_ix,L_ox,L_ux,L_abe
     &,L_ebe,L_ibe,L_obe,L_ube,L_ade,L_ede,L_ide,L_ode,L_ude
     &,L_afe,L_efe,L_ife,L_ofe,L_ufe,L_ake,L_eke,L_ike,L_oke
     &,L_uke,L_ale,L_ele,L_ile,L_ole,L_ule,L_ame,L_eme,L_ime
     &,L_ome,L_ume,L_ape,L_epe,L_ipe,L_ope,L_upe,L_are,L_ere
     &,L_ire,L_ore,L_ure,L_ase)
C |R_e           |4 4 O|VS02            |�������� ����������||
C |R_i           |4 4 I|�10_VZ02        |��������� �����������||
C |L_o           |1 1 O|L11             |������ ������||
C |L_u           |1 1 O|L12             |��������� ����������||
C |L_ad          |1 1 O|�11_uluop       |������� ������||
C |L_ed          |1 1 O|�11_ulucl       |������� ������||
C |L_id          |1 1 I|�11_CBC         |������ ������||
C |L_od          |1 1 I|�11B            |�������/������� ������||
C |L_ud          |1 1 I|�11_CBO         |������ ������||
C |R_af          |4 4 O|VH01            |��������� �������||
C |R_ef          |4 4 I|�2_MFVAL        |������ ������||
C |R_if          |4 4 O|VS01            |��������� �����������||
C |R_of          |4 4 O|VZ02            |�������� ������������||
C |R_uf          |4 4 O|VZ01            |��������� ������������||
C |R_ak          |4 4 O|VX02            |�������� ������������||
C |R_ek          |4 4 O|VX01            |��������� ������������||
C |R_ik          |4 4 I|�10_VZ01        |��������� �����������||
C |R_ok          |4 4 I|C8VX02          |�������� ���������||
C |R_uk          |4 4 I|C7VX02          |�������� ���������||
C |R_al          |4 4 I|C6VX02          |�������� ���������||
C |R_el          |4 4 I|CORRECT_Z       |��������� �������||
C |R_il          |4 4 I|C8VX01          |��������� �������||
C |R_ol          |4 4 I|C7VX01          |��������� �������||
C |R_ul          |4 4 I|C6VX01          |��������� �������||
C |R_am          |4 4 I|C5VX02          |�������� ���������||
C |R_em          |4 4 I|C4VX02          |�������� ���������||
C |R_im          |4 4 I|C3VX02          |�������� ���������||
C |R_om          |4 4 I|C5VX01          |��������� �������||
C |R_um          |4 4 I|C4VX01          |��������� �������||
C |R_ap          |4 4 I|C3VX01          |��������� �������||
C |L_ep          |1 1 O|L9              |���� ���� - �����||
C |L_ip          |1 1 O|L10             |���� ���� - �����||
C |L_op          |1 1 O|L8              |������ ������||
C |L_up          |1 1 O|L7              |��������� ������||
C |L_ar          |1 1 O|L6_input        |���� ���� - �������||
C |L_er          |1 1 O|L4              |��������� ����||
C |L_ir          |1 1 O|L5              |���� ���� - �����||
C |L_or          |1 1 O|L3              |�������� ���������||
C |L_ur          |1 1 O|L1_input        |���������� ����||
C |L_as          |1 1 O|L2              |���������� ����||
C |L_es          |1 1 O|L10_input       |���� ���� - �����||
C |L_is          |1 1 O|L9_input        |���� ���� - �����||
C |L_os          |1 1 O|L6              |���� ���� - �������||
C |L_us          |1 1 O|L5_input        |���� ���� - �����||
C |L_at          |1 1 O|L1              |���������� ����||
C |L_et          |1 1 O|L4_input        |��������� ����||
C |L_it          |1 1 O|L3_input        |�������� ���������||
C |L_ot          |1 1 O|L2_input        |���������� ����||
C |L_ut          |1 1 I|C7XH54          |������� ��������||
C |L_av          |1 1 I|C4XH54          |����� ��������||
C |L_ev          |1 1 I|C3XH54          |����� ��������||
C |L_iv          |1 1 I|C6XH54          |������� ��������||
C |L_ov          |1 1 I|C5XH54          |����� ��������||
C |L_uv          |1 1 I|C8XH53          |������ ��������||
C |L_ax          |1 1 I|C7XH53          |������ ��������||
C |L_ex          |1 1 I|C6XH53          |������ ��������||
C |L_ix          |1 1 I|C5XH53          |������ ��������||
C |L_ox          |1 1 I|C4XH53          |������ ��������||
C |L_ux          |1 1 I|C3XH53          |������ ��������||
C |L_abe         |1 1 O|C8YA26          |������� � �������||
C |L_ebe         |1 1 O|C8YA25          |������� � �������||
C |L_ibe         |1 1 O|C7YA26          |������� � �������||
C |L_obe         |1 1 O|C7YA25          |������� � �������||
C |L_ube         |1 1 O|C6YA26          |������� � �������||
C |L_ade         |1 1 O|C6YA25          |������� � �������||
C |L_ede         |1 1 O|C5YA25          |������� � �������||
C |L_ide         |1 1 O|C4YA25          |������� � �������||
C |L_ode         |1 1 O|C3YA25          |������� � �������||
C |L_ude         |1 1 O|C5YA26          |������� � �������||
C |L_afe         |1 1 O|C4YA26          |������� � �������||
C |L_efe         |1 1 O|C3YA26          |������� � �������||
C |L_ife         |1 1 O|�2_uluop        |������� ������||
C |L_ofe         |1 1 O|�2_ulucl        |������� ������||
C |L_ufe         |1 1 O|�1_uluop        |������� ����||
C |L_ake         |1 1 O|�1_ulucl        |������� ����||
C |L_eke         |1 1 O|�10_YA26        |��������� ������� ����||
C |L_ike         |1 1 O|�10_YA25        |��������� ������� �����||
C |L_oke         |1 1 I|�10B            |��������� ����������||
C |L_uke         |1 1 I|C8YA26_B        |������� � �������||
C |L_ale         |1 1 I|C8YA25_B        |������� � �������||
C |L_ele         |1 1 I|C7YA26_B        |������� � �������||
C |L_ile         |1 1 I|C7YA25_B        |������� � �������||
C |L_ole         |1 1 I|C6YA26_B        |������� � �������||
C |L_ule         |1 1 I|C6YA25_B        |������� � �������||
C |L_ame         |1 1 I|C5YA25_B        |������� � �������||
C |L_eme         |1 1 I|C4YA25_B        |������� � �������||
C |L_ime         |1 1 I|PREV            |������� � ���� ����||
C |L_ome         |1 1 I|C3YA25_B        |������� � �������||
C |L_ume         |1 1 I|C5YA26_B        |������� � �������||
C |L_ape         |1 1 I|C4YA26_B        |������� � �������||
C |L_epe         |1 1 I|NEXT            |������� � ���� ����||
C |L_ipe         |1 1 I|C3YA26_B        |������� � �������||
C |L_ope         |1 1 I|�2_CBC          |������ ������||
C |L_upe         |1 1 I|�2B             |�������/������� ������||
C |L_are         |1 1 I|�2_CBO          |������ ������||
C |L_ere         |1 1 I|�1_CBC          |���� ������||
C |L_ire         |1 1 I|�1B             |�������/������� ����||
C |L_ore         |1 1 I|�1_CBO          |���� ������||
C |L_ure         |1 1 I|�10XH53         |��������� �� ����������||
C |L_ase         |1 1 I|�10XH54         |��������� ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_e,R_i
      LOGICAL*1 L_o,L_u,L_ad,L_ed,L_id,L_od,L_ud
      REAL*4 R_af,R_ef,R_if,R_of,R_uf,R_ak,R_ek,R_ik,R_ok
     &,R_uk,R_al,R_el,R_il,R_ol,R_ul,R_am,R_em,R_im,R_om,R_um
     &,R_ap
      LOGICAL*1 L_ep,L_ip,L_op,L_up,L_ar,L_er,L_ir,L_or,L_ur
     &,L_as,L_es,L_is,L_os,L_us,L_at,L_et,L_it,L_ot,L_ut,L_av
     &,L_ev,L_iv
      LOGICAL*1 L_ov,L_uv,L_ax,L_ex,L_ix,L_ox,L_ux,L_abe,L_ebe
     &,L_ibe,L_obe,L_ube,L_ade,L_ede,L_ide,L_ode,L_ude,L_afe
     &,L_efe,L_ife,L_ofe,L_ufe
      LOGICAL*1 L_ake,L_eke,L_ike,L_oke,L_uke,L_ale,L_ele
     &,L_ile,L_ole,L_ule,L_ame,L_eme,L_ime,L_ome,L_ume,L_ape
     &,L_epe,L_ipe,L_ope,L_upe,L_are,L_ere
      LOGICAL*1 L_ire,L_ore,L_ure,L_ase
      End subroutine TRANSPORTER
      End interface
