      Interface
      Subroutine TOLKATEL_OZ_HANDLER(ext_deltat,R_obi,R_e
     &,R_ed,R_ef,R_of,R_uf,R_ak,R_ek,L_up,R_as,R_ox,L_ux,R_ibe
     &,L_obe,L_ude,L_ife,R_ile,R_ime,L_ume,R_ipe,L_upe,L_ere
     &,L_ese,L_ose,L_use,L_ete,L_ute,L_ive,L_uve,R_ubi,L_idi
     &,L_udi,L_efi,L_ifi,L_aki,L_eki,C20_imi,L_omi,L_api,L_epi
     &,L_upi,L_ari,L_eri,L_iri,L_ori,L_uri,L_asi,L_esi,L_isi
     &,L_osi,L_usi,L_ati,L_eti,L_iti,L_oti,R_uti,R_avi,R_evi
     &,R_ivi,R_uvi,R_exi,I_ixi,R_abo,R_ubo,C20_eko,C20_elo
     &,I_amo,I_omo,R_umo,R_apo,R_epo,R_ipo,I_opo,I_ero,I_eso
     &,I_uso,C20_uto,C8_uvo,R_idu,R_odu,L_udu,R_afu,R_efu
     &,I_ifu,L_aku,L_eku,I_oku,I_elu,L_emu,L_omu,L_umu,L_apu
     &,L_epu,L_ipu,L_iru,L_isu,L_usu,L_atu,R8_itu,R_evu,R8_uvu
     &,L_axu,L_exu,L_oxu,L_uxu,L_ebad,I_ibad,L_adad,L_edad
     &,L_efad,L_ukad,L_ulad)
C |R_obi         |4 4 I|11 mlfpar19     ||0.6|
C |R_e           |4 4 S|_slpbJ3525*     |���������� ��������� ||
C |R_ed          |4 4 I|VX01            |||
C |R_ef          |4 4 I|p_TOLKATEL_XH54 |||
C |R_of          |4 4 S|_slpbJ3478*     |���������� ��������� ||
C |R_uf          |4 4 S|_slpbJ3477*     |���������� ��������� ||
C |R_ak          |4 4 S|_slpbJ3476*     |���������� ��������� ||
C |R_ek          |4 4 S|_slpbJ3470*     |���������� ��������� ||
C |L_up          |1 1 S|_qffJ3426*      |�������� ������ Q RS-��������  |F|
C |R_as          |4 4 I|mlfpar19        |��������� ������������|0.6|
C |R_ox          |4 4 S|_sdelnv2dyn$100Dasha*|[���]���������� ��������� |0|
C |L_ux          |1 1 S|_idelnv2dyn$100Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ibe         |4 4 S|_sdelnv2dyn$99Dasha*|[���]���������� ��������� |0|
C |L_obe         |1 1 S|_idelnv2dyn$99Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_ude         |1 1 S|_qff156*        |�������� ������ Q RS-��������  |F|
C |L_ife         |1 1 S|_qff155*        |�������� ������ Q RS-��������  |F|
C |R_ile         |4 4 I|value_pos       |��� �������� ��������|0.5|
C |R_ime         |4 4 S|_sdelnv2dyn$83Dasha*|[���]���������� ��������� |0|
C |L_ume         |1 1 S|_idelnv2dyn$83Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ipe         |4 4 S|_sdelnv2dyn$82Dasha*|[���]���������� ��������� |0|
C |L_upe         |1 1 S|_idelnv2dyn$82Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_ere         |1 1 S|_qffnv2dyn$106Dasha*|�������� ������ Q RS-��������  |F|
C |L_ese         |1 1 S|_qffnv2dyn$104Dasha*|�������� ������ Q RS-��������  |F|
C |L_ose         |1 1 S|_qffnv2dyn$105Dasha*|�������� ������ Q RS-��������  |F|
C |L_use         |1 1 I|OUTC            |�����||
C |L_ete         |1 1 S|_qffnv2dyn$103Dasha*|�������� ������ Q RS-��������  |F|
C |L_ute         |1 1 S|_qffnv2dyn$102Dasha*|�������� ������ Q RS-��������  |F|
C |L_ive         |1 1 S|_qffnv2dyn$101Dasha*|�������� ������ Q RS-��������  |F|
C |L_uve         |1 1 I|OUTO            |������||
C |R_ubi         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |L_idi         |1 1 S|_qffnv2dyn$87Dasha*|�������� ������ Q RS-��������  |F|
C |L_udi         |1 1 S|_qffnv2dyn$86Dasha*|�������� ������ Q RS-��������  |F|
C |L_efi         |1 1 S|_qffnv2dyn$96Dasha*|�������� ������ Q RS-��������  |F|
C |L_ifi         |1 1 S|_qffnv2dyn$95Dasha*|�������� ������ Q RS-��������  |F|
C |L_aki         |1 1 O|limit_switch_error|||
C |L_eki         |1 1 O|flag_mlf19      |||
C |C20_imi       |3 20 O|task_state      |���������||
C |L_omi         |1 1 I|vlv_kvit        |||
C |L_api         |1 1 I|instr_fault     |||
C |L_epi         |1 1 S|_qffJ2999*      |�������� ������ Q RS-��������  |F|
C |L_upi         |1 1 O|norm            |�����||
C |L_ari         |1 1 I|mlf17           |������ ���������������� ����� �����||
C |L_eri         |1 1 I|mlf16           |������ ������������ ����� �����||
C |L_iri         |1 1 I|mlf15           |������ ���������������� ����� ������||
C |L_ori         |1 1 I|mlf14           |������ ������������ ����� ������||
C |L_uri         |1 1 I|mlf07           |���������� ���� �������||
C |L_asi         |1 1 I|mlf28           |������ ���������������� ��������� �����||
C |L_esi         |1 1 I|mlf09           |����� ��������� �����||
C |L_isi         |1 1 I|mlf08           |����� ��������� ������||
C |L_osi         |1 1 I|mlf26           |������ ���������������� ��������� ������||
C |L_usi         |1 1 I|mlf27           |������ ������������ ��������� �����||
C |L_ati         |1 1 I|mlf25           |������ ������������ ��������� ������||
C |L_eti         |1 1 I|mlf23           |������� ������� �����||
C |L_iti         |1 1 I|mlf22           |����� ����� ��������||
C |L_oti         |1 1 I|mlf19           |���� ������������||
C |R_uti         |4 4 K|_uintT_INT      |����������� ������ ����������� ������|10000|
C |R_avi         |4 4 K|_tintT_INT      |[���]�������� T �����������|1|
C |R_evi         |4 4 O|_ointT_INT*     |�������� ������ ����������� |`p_TOLKATEL_XH54`|
C |R_ivi         |4 4 K|_lintT_INT      |����������� ������ ����������� �����|0.0|
C |R_uvi         |4 4 K|_lcmpJ2903      |[]�������� ������ �����������|`p_TOLKATEL_XH54`|
C |R_exi         |4 4 K|_lcmpJ2873      |[]�������� ������ �����������|5.0|
C |I_ixi         |2 4 O|LWORK           |����� �������||
C |R_abo         |4 4 O|VZ01            |��������� ��������� �� ��� Z||
C |R_ubo         |4 4 O|VZ02            |�������� ����������� ���������||
C |C20_eko       |3 20 O|task_name3      |������� ��������� ��� ������� ������������ ��������||
C |C20_elo       |3 20 O|task_name2      |������� ��������� ��� ������� ������������ ��������||
C |I_amo         |2 4 O|LREADY          |����� ����������||
C |I_omo         |2 4 O|LBUSY           |����� �����||
C |R_umo         |4 4 S|vminit_button_ST*|��������� ������ "������� � �������� �� ���������" |0.0|
C |R_apo         |4 4 I|vminit_button   |������� ������ ������ "������� � �������� �� ���������" |0.0|
C |R_epo         |4 4 S|vmwork_button_ST*|��������� ������ "������� � ������� �� ���������" |0.0|
C |R_ipo         |4 4 I|vmwork_button   |������� ������ ������ "������� � ������� �� ���������" |0.0|
C |I_opo         |2 4 O|state1          |��������� 1||
C |I_ero         |2 4 O|state2          |��������� 2||
C |I_eso         |2 4 O|LWORKO          |����� � �������||
C |I_uso         |2 4 O|LINITC          |����� � ��������||
C |C20_uto       |3 20 O|task_name       |������� ���������||
C |C8_uvo        |3 8 I|task            |������� ���������||
C |R_idu         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_odu         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_udu         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_afu         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_efu         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_ifu         |2 4 O|LERROR          |����� �������������||
C |L_aku         |1 1 I|stop_avt        |������� ���� �� ����������|F|
C |L_eku         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |I_oku         |2 4 O|LINIT           |����� ��������||
C |I_elu         |2 4 O|LZM             |������ "�������"||
C |L_emu         |1 1 O|vminit_button_CMD*|[TF]����� ������ ������� � �������� �� ���������|F|
C |L_omu         |1 1 O|vmwork_button_CMD*|[TF]����� ������ ������� � ������� �� ���������|F|
C |L_umu         |1 1 I|mlf06           |������������� ������� "�����"||
C |L_apu         |1 1 I|mlf05           |������������� ������� "������"||
C |L_epu         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_ipu         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_iru         |1 1 O|block           |||
C |L_isu         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_usu         |1 1 O|STOP            |�������||
C |L_atu         |1 1 O|nopower         |��� ����������||
C |R8_itu        |4 8 I|voltage         |[��]���������� �� ������||
C |R_evu         |4 4 I|power           |�������� ��������||
C |R8_uvu        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_axu         |1 1 I|mlf04           |���������������� �������� �����||
C |L_exu         |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_oxu         |1 1 I|mlf03           |���������������� �������� ������||
C |L_uxu         |1 1 I|YA26            |������� ������� �� ����������|F|
C |L_ebad        |1 1 O|fault           |�������������||
C |I_ibad        |2 4 O|LAM             |������ "�������"||
C |L_adad        |1 1 O|XH53            |�� ����� (���)|F|
C |L_edad        |1 1 O|XH54            |�� ������ (���)|F|
C |L_efad        |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_ukad        |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ulad        |1 1 I|mlf24           |��� ���������� � ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_e,R_ed,R_ef,R_of,R_uf,R_ak,R_ek
      LOGICAL*1 L_up
      REAL*4 R_as,R_ox
      LOGICAL*1 L_ux
      REAL*4 R_ibe
      LOGICAL*1 L_obe,L_ude,L_ife
      REAL*4 R_ile,R_ime
      LOGICAL*1 L_ume
      REAL*4 R_ipe
      LOGICAL*1 L_upe,L_ere,L_ese,L_ose,L_use,L_ete,L_ute
     &,L_ive,L_uve
      REAL*4 R_obi,R_ubi
      LOGICAL*1 L_idi,L_udi,L_efi,L_ifi,L_aki,L_eki
      CHARACTER*20 C20_imi
      LOGICAL*1 L_omi,L_api,L_epi,L_upi,L_ari,L_eri,L_iri
     &,L_ori,L_uri,L_asi,L_esi,L_isi,L_osi,L_usi,L_ati,L_eti
     &,L_iti,L_oti
      REAL*4 R_uti,R_avi,R_evi,R_ivi,R_uvi,R_exi
      INTEGER*4 I_ixi
      REAL*4 R_abo,R_ubo
      CHARACTER*20 C20_eko,C20_elo
      INTEGER*4 I_amo,I_omo
      REAL*4 R_umo,R_apo,R_epo,R_ipo
      INTEGER*4 I_opo,I_ero,I_eso,I_uso
      CHARACTER*20 C20_uto
      CHARACTER*8 C8_uvo
      REAL*4 R_idu,R_odu
      LOGICAL*1 L_udu
      REAL*4 R_afu,R_efu
      INTEGER*4 I_ifu
      LOGICAL*1 L_aku,L_eku
      INTEGER*4 I_oku,I_elu
      LOGICAL*1 L_emu,L_omu,L_umu,L_apu,L_epu,L_ipu,L_iru
     &,L_isu,L_usu,L_atu
      REAL*8 R8_itu
      REAL*4 R_evu
      REAL*8 R8_uvu
      LOGICAL*1 L_axu,L_exu,L_oxu,L_uxu,L_ebad
      INTEGER*4 I_ibad
      LOGICAL*1 L_adad,L_edad,L_efad,L_ukad,L_ulad
      End subroutine TOLKATEL_OZ_HANDLER
      End interface
