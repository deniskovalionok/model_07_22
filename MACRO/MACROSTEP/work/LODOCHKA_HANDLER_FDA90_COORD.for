      Subroutine LODOCHKA_HANDLER_FDA90_COORD(ext_deltat,L_ud
     &,L_um,L_up,L_ir,L_is,L_av,L_ebe,L_ibe,L_ide,L_ike,L_ele
     &,L_ame,L_epe,L_ire,L_ore,I_ise)
C |L_ud          |1 1 I|FDA71AE202_CATCH|������� ��������� ���. �������� ��. ���������||
C |L_um          |1 1 I|FDA91AE013_CATCH|������� ��������� �� ��. ���������||
C |L_up          |1 1 I|FDA91AE005KE01_CATCH|������� ��������� �||
C |L_ir          |1 1 I|FDA91AE002KE01_CATCH|������� ��������� �||
C |L_is          |1 1 I|FDA60AE402_CATCH|������� ��������� � ��. �������||
C |L_av          |1 1 I|FDA52AE401_CATCH|������� ��������� � ��. �������� ||
C |L_ebe         |1 1 I|FDA91AE003KE01_CATCH|������� ��������� �||
C |L_ibe         |1 1 I|FDA91AE018_CATCH|������� ��������� � ��. �����������||
C |L_ide         |1 1 I|FDA91AE001KE01_CATCH|������� ��������� �1||
C |L_ike         |1 1 I|FDA91AE009_CATCH|������� ��������� �6||
C |L_ele         |1 1 I|FDA91AE014_CATCH|������� ��������� �7||
C |L_ame         |1 1 I|FDA91AE008_CATCH|������� ��������� �5||
C |L_epe         |1 1 I|FDA91AE007_CATCH|������� ��������� �4||
C |L_ire         |1 1 I|FDA91AE006_CATCH|������� ��������� �3||
C |L_ore         |1 1 I|FDA91AE012_CATCH|������� ��������� �1||
C |I_ise         |2 4 O|CR              |�������������� � �������������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      INTEGER*4 I0_e
      LOGICAL*1 L0_i,L0_o
      INTEGER*4 I0_u,I0_ad
      LOGICAL*1 L0_ed,L0_id
      INTEGER*4 I0_od
      LOGICAL*1 L_ud
      INTEGER*4 I0_af,I0_ef,I0_if
      LOGICAL*1 L0_of,L0_uf,L0_ak,L0_ek
      INTEGER*4 I0_ik,I0_ok,I0_uk,I0_al
      LOGICAL*1 L0_el
      INTEGER*4 I0_il,I0_ol
      LOGICAL*1 L0_ul
      INTEGER*4 I0_am
      LOGICAL*1 L0_em,L0_im
      INTEGER*4 I0_om
      LOGICAL*1 L_um,L0_ap,L0_ep
      INTEGER*4 I0_ip
      LOGICAL*1 L0_op,L_up,L0_ar,L0_er,L_ir,L0_or
      INTEGER*4 I0_ur
      LOGICAL*1 L0_as,L0_es,L_is,L0_os
      INTEGER*4 I0_us
      LOGICAL*1 L0_at,L0_et,L0_it,L0_ot
      INTEGER*4 I0_ut
      LOGICAL*1 L_av,L0_ev,L0_iv
      INTEGER*4 I0_ov,I0_uv
      LOGICAL*1 L0_ax,L0_ex
      INTEGER*4 I0_ix
      LOGICAL*1 L0_ox,L0_ux,L0_abe,L_ebe,L_ibe,L0_obe
      INTEGER*4 I0_ube
      LOGICAL*1 L0_ade,L0_ede,L_ide,L0_ode,L0_ude
      INTEGER*4 I0_afe
      LOGICAL*1 L0_efe,L0_ife,L0_ofe
      INTEGER*4 I0_ufe
      LOGICAL*1 L0_ake,L0_eke,L_ike
      INTEGER*4 I0_oke
      LOGICAL*1 L0_uke,L0_ale,L_ele,L0_ile
      INTEGER*4 I0_ole,I0_ule
      LOGICAL*1 L_ame,L0_eme,L0_ime,L0_ome
      INTEGER*4 I0_ume
      LOGICAL*1 L0_ape,L_epe,L0_ipe
      INTEGER*4 I0_ope
      LOGICAL*1 L0_upe,L0_are
      INTEGER*4 I0_ere
      LOGICAL*1 L_ire,L_ore,L0_ure,L0_ase
      INTEGER*4 I0_ese,I_ise

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I0_e = 9003
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 372,1521):��������� ������������� IN (�������)
      I0_u = 7001
C LODOCHKA_HANDLER_FDA90_COORD.fmg(  80,1508):��������� ������������� IN (�������)
      I0_ad = 7001
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 372,1536):��������� ������������� IN (�������)
      I0_od = 9003
C LODOCHKA_HANDLER_FDA90_COORD.fmg(  80,1531):��������� ������������� IN (�������)
      I0_af = 9004
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 372,1562):��������� ������������� IN (�������)
      I0_ef = 9003
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 370,1592):��������� ������������� IN (�������)
      I0_if = 9002
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 370,1620):��������� ������������� IN (�������)
      I0_ik = 9001
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 368,1678):��������� ������������� IN (�������)
      I0_ok = 9096
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 368,1691):��������� ������������� IN (�������)
      I0_uk = 5081
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 368,1710):��������� ������������� IN (�������)
      I0_al = 4001
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 368,1728):��������� ������������� IN (�������)
      I0_il = 9010
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 366,1805):��������� ������������� IN (�������)
      I0_ol = 9011
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 366,1830):��������� ������������� IN (�������)
      I0_am = 9009
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 366,1868):��������� ������������� IN (�������)
      I0_om = 9011
C LODOCHKA_HANDLER_FDA90_COORD.fmg(  80,1576):��������� ������������� IN (�������)
      I0_ip = 9009
C LODOCHKA_HANDLER_FDA90_COORD.fmg(  80,1602):��������� ������������� IN (�������)
      I0_ur = 9008
C LODOCHKA_HANDLER_FDA90_COORD.fmg(  80,1630):��������� ������������� IN (�������)
      I0_us = 9007
C LODOCHKA_HANDLER_FDA90_COORD.fmg(  80,1690):��������� ������������� IN (�������)
      I0_ut = 9096
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 119,1742):��������� ������������� IN (�������)
      I0_ov = 9010
C LODOCHKA_HANDLER_FDA90_COORD.fmg(  80,1709):��������� ������������� IN (�������)
      I0_uv = 5081
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 118,1756):��������� ������������� IN (�������)
      I0_ix = 9006
C LODOCHKA_HANDLER_FDA90_COORD.fmg(  80,1658):��������� ������������� IN (�������)
      I0_ube = 9005
C LODOCHKA_HANDLER_FDA90_COORD.fmg(  80,1726):��������� ������������� IN (�������)
      I0_afe = 4001
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 119,1772):��������� ������������� IN (�������)
      I0_ufe = 9008
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 364,1905):��������� ������������� IN (�������)
      I0_oke = 9004
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 119,1805):��������� ������������� IN (�������)
      I0_ole = 9003
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 119,1844):��������� ������������� IN (�������)
      I0_ule = 9002
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 119,1882):��������� ������������� IN (�������)
      I0_ume = 9007
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 326,1918):��������� ������������� IN (�������)
      I0_ope = 9006
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 306,1930):��������� ������������� IN (�������)
      I0_ere = 9005
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 287,1942):��������� ������������� IN (�������)
      I0_ese = 9001
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 118,1942):��������� ������������� IN (�������)
      L0_o=I_ise.eq.I0_u
C LODOCHKA_HANDLER_FDA90_COORD.fmg(  86,1510):���������� �������������
C label 81  try81=try81-1
      L0_i = L_up.AND.L0_o
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1510):�
      if(L0_i) then
         I_ise=I0_e
      endif
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 378,1520):���� � ������������� �������
      L0_ile=I_ise.eq.I0_ole
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 126,1846):���������� �������������
      L0_ap = L0_ile.AND.L_um
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1822):�
      if(L0_ap) then
         I_ise=I0_ol
      endif
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 373,1830):���� � ������������� �������
      L0_ife = L0_ile.AND.L_ike
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1836):�
      L0_ex=I_ise.eq.I0_uv
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 126,1758):���������� �������������
      L0_ax = L0_ex.AND.L_ele
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1750):�
      L0_iv=I_ise.eq.I0_ov
C LODOCHKA_HANDLER_FDA90_COORD.fmg(  87,1710):���������� �������������
      L0_ev = L0_iv.AND.L_av
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1702):�
      if(L0_ev) then
         I_ise=I0_uk
      endif
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 375,1710):���� � ������������� �������
      L0_eme=I_ise.eq.I0_ule
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 126,1883):���������� �������������
      L0_ime = L0_eme.AND.L_ame
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1874):�
      L0_eke = L0_eme.AND.L_ike
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1860):�
      L0_ul = L0_eke.OR.L0_ife
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 358,1859):���
      if(L0_ul) then
         I_ise=I0_am
      endif
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 372,1868):���� � ������������� �������
      L0_et=I_ise.eq.I0_us
C LODOCHKA_HANDLER_FDA90_COORD.fmg(  87,1690):���������� �������������
      L0_os = L0_et.AND.L_is
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1682):�
      if(L0_os) then
         I_ise=I0_ok
      endif
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 375,1690):���� � ������������� �������
      L0_ede=I_ise.eq.I0_ube
C LODOCHKA_HANDLER_FDA90_COORD.fmg(  87,1728):���������� �������������
      L0_ade = L0_ede.AND.L_ide
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1669):�
      L0_at = L0_et.AND.L_ide
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1641):�
      L0_obe = L0_ede.AND.L_ibe
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1720):�
      if(L0_obe) then
         I_ise=I0_al
      endif
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 375,1728):���� � ������������� �������
      L0_es=I_ise.eq.I0_ur
C LODOCHKA_HANDLER_FDA90_COORD.fmg(  87,1630):���������� �������������
      L0_or = L0_es.AND.L_ir
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1612):�
      L0_as = L0_es.AND.L_ide
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1626):�
      L0_ot=I_ise.eq.I0_ut
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 126,1744):���������� �������������
      L0_it = L0_ot.AND.L_epe
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1735):�
      L0_er=I_ise.eq.I0_ip
C LODOCHKA_HANDLER_FDA90_COORD.fmg(  87,1602):���������� �������������
      L0_ar = L0_er.AND.L_up
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 153,1584):�
      L0_op = L0_er.AND.L_ir
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1598):�
      L0_ak = L0_or.OR.L0_op
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 358,1611):���
      if(L0_ak) then
         I_ise=I0_if
      endif
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 377,1620):���� � ������������� �������
      L0_im=I_ise.eq.I0_om
C LODOCHKA_HANDLER_FDA90_COORD.fmg(  87,1576):���������� �������������
      L0_em = L0_im.AND.L_up
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 153,1567):�
      L0_uf = L0_ar.OR.L0_em
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 358,1584):���
      if(L0_uf) then
         I_ise=I0_ef
      endif
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 377,1592):���� � ������������� �������
      L0_uke=I_ise.eq.I0_oke
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 126,1806):���������� �������������
      L0_efe = L0_uke.AND.L_ire
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1782):�
      L0_ale = L0_uke.AND.L_ele
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1797):�
      L0_el = L0_ale.OR.L0_ax
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 358,1796):���
      if(L0_el) then
         I_ise=I0_il
      endif
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 373,1804):���� � ������������� �������
      L0_abe=I_ise.eq.I0_ix
C LODOCHKA_HANDLER_FDA90_COORD.fmg(  87,1659):���������� �������������
      L0_ux = L0_abe.AND.L_ebe
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 153,1554):�
      L0_ep = L0_iv.AND.L_ebe
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1540):�
      L0_of = L0_ux.OR.L0_ep
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 359,1554):���
      if(L0_of) then
         I_ise=I0_af
      endif
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 378,1562):���� � ������������� �������
      L0_ox = L0_abe.AND.L_ide
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1655):�
      L0_ek = L0_ade.OR.L0_ox.OR.L0_at.OR.L0_as
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 360,1666):���
      if(L0_ek) then
         I_ise=I0_ik
      endif
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 375,1677):���� � ������������� �������
      L0_ase=I_ise.eq.I0_ese
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 126,1942):���������� �������������
      L0_ure = L0_ase.AND.L_ore
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1934):�
      L0_upe = L0_ase.AND.L_ire
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1922):�
      L0_ipe = L0_upe.OR.L0_efe
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 298,1921):���
      if(L0_ipe) then
         I_ise=I0_ope
      endif
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 313,1930):���� � ������������� �������
      L0_ape = L0_ase.AND.L_epe
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1910):�
      L0_ome = L0_ape.OR.L0_it
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 318,1908):���
      if(L0_ome) then
         I_ise=I0_ume
      endif
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 332,1917):���� � ������������� �������
      L0_ake = L0_ase.AND.L_ame
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1897):�
      L0_ofe = L0_ake.OR.L0_ime
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 356,1896):���
      if(L0_ofe) then
         I_ise=I0_ufe
      endif
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 371,1904):���� � ������������� �������
      L0_ude=I_ise.eq.I0_afe
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 126,1772):���������� �������������
      L0_ode = L0_ude.AND.L_ore
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1764):�
      L0_are = L0_ure.OR.L0_ode
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 279,1934):���
      if(L0_are) then
         I_ise=I0_ere
      endif
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 294,1941):���� � ������������� �������
      L0_id=I_ise.eq.I0_od
C LODOCHKA_HANDLER_FDA90_COORD.fmg(  86,1532):���������� �������������
      L0_ed = L0_id.AND.L_ud
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 152,1526):�
      if(L0_ed) then
         I_ise=I0_ad
      endif
C LODOCHKA_HANDLER_FDA90_COORD.fmg( 378,1536):���� � ������������� �������
      End
