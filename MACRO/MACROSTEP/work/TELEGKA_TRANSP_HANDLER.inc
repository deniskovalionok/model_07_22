      Interface
      Subroutine TELEGKA_TRANSP_HANDLER(ext_deltat,L_e,L_i
     &,L_o,L_u,R_ed,R_id,R_ud,R_af,L_ef,R_of,R_uf,L_ak,L_ek
     &,L_al,L_im,L_om,R_um,R_ap,R_ep,R_ip,R_op,L_ar,L_ir,R_ur
     &,R_as,R_it,R_ot,L_ut,R_av,R_ev,L_iv,R_ov,R_uv,L_ax,L_ix
     &,L_ox,L_ux,L_ebe,L_ibe,L_obe,L_ade,L_afe,L_ufe,L_eke
     &)
C |L_e           |1 1 O|UNCATCH         |��������� �������|F|
C |L_i           |1 1 I|YA31            |������� ��������� �������|F|
C |L_o           |1 1 O|CATCH           |������ �������|F|
C |L_u           |1 1 I|YA30            |������� ������ �������|F|
C |R_ed          |4 4 K|_cJ3237         |�������� ��������� ����������|`max_c`|
C |R_id          |4 4 K|_cJ3236         |�������� ��������� ����������|`min_c`|
C |R_ud          |4 4 S|_simpJ3201*     |[���]���������� ��������� ������������� |0.0|
C |R_af          |4 4 K|_timpJ3201      |[���]������������ �������� �������������|0.4|
C |L_ef          |1 1 S|_limpJ3201*     |[TF]���������� ��������� ������������� |F|
C |R_of          |4 4 S|_simpJ3198*     |[���]���������� ��������� ������������� |0.0|
C |R_uf          |4 4 K|_timpJ3198      |[���]������������ �������� �������������|0.4|
C |L_ak          |1 1 S|_limpJ3198*     |[TF]���������� ��������� ������������� |F|
C |L_ek          |1 1 O|XH54            |�������� ������� MAX (��)||
C |L_al          |1 1 O|XH53            |�������� ������� MIN (��)||
C |L_im          |1 1 I|YA27            |������� ���� �� ����������|F|
C |L_om          |1 1 I|forward         |������� �� ��������� ������||
C |R_um          |4 4 K|_uintV_INT_PU   |����������� ������ ����������� ������|`max_c`|
C |R_ap          |4 4 K|_tintV_INT_PU   |[���]�������� T �����������|1|
C |R_ep          |4 4 K|_lintV_INT_PU   |����������� ������ ����������� �����|`min_c`|
C |R_ip          |4 4 O|POS             |��������� ��||
C |R_op          |4 4 O|_ointV_INT_PU*  |�������� ������ ����������� |`state_c`|
C |L_ar          |1 1 I|mlf03           |�������� �� ��������||
C |L_ir          |1 1 I|mlf04           |�������� �� ��������||
C |R_ur          |4 4 O|V01             |�������� ����������� ��||
C |R_as          |4 4 I|vel             |����� ����|30.0|
C |R_it          |4 4 S|USER_BACK_ST*   |��������� ������ "������� ����� �� ���������" |0.0|
C |R_ot          |4 4 I|USER_BACK       |������� ������ ������ "������� ����� �� ���������" |0.0|
C |L_ut          |1 1 O|USER_FORWARD_CMD*|[TF]����� ������ ������� ������ �� ���������|F|
C |R_av          |4 4 S|USER_FORWARD_ST*|��������� ������ "������� ������ �� ���������" |0.0|
C |R_ev          |4 4 I|USER_FORWARD    |������� ������ ������ "������� ������ �� ���������" |0.0|
C |L_iv          |1 1 O|USER_STOP_CMD*  |[TF]����� ������ ������� ���� �� ���������|F|
C |R_ov          |4 4 S|USER_STOP_ST*   |��������� ������ "������� ���� �� ���������" |0.0|
C |R_uv          |4 4 I|USER_STOP       |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_ax          |1 1 I|back            |������� �� ��������� �����||
C |L_ix          |1 1 I|ulubforward     |���������� ������ �� ���������|F|
C |L_ox          |1 1 I|ulubback        |���������� ����� �� ���������|F|
C |L_ux          |1 1 O|USER_BACK_CMD*  |[TF]����� ������ ������� ����� �� ���������|F|
C |L_ebe         |1 1 O|stop_inner      |�������||
C |L_ibe         |1 1 S|_qffJ3299*      |�������� ������ Q RS-��������  |F|
C |L_obe         |1 1 I|YA25            |������� ����� �� ����������|F|
C |L_ade         |1 1 I|YA26            |������� ������ �� ����������|F|
C |L_afe         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_ufe         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_eke         |1 1 I|mlf24           |��� ���������� � ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      LOGICAL*1 L_e,L_i,L_o,L_u
      REAL*4 R_ed,R_id,R_ud,R_af
      LOGICAL*1 L_ef
      REAL*4 R_of,R_uf
      LOGICAL*1 L_ak,L_ek,L_al,L_im,L_om
      REAL*4 R_um,R_ap,R_ep,R_ip,R_op
      LOGICAL*1 L_ar,L_ir
      REAL*4 R_ur,R_as,R_it,R_ot
      LOGICAL*1 L_ut
      REAL*4 R_av,R_ev
      LOGICAL*1 L_iv
      REAL*4 R_ov,R_uv
      LOGICAL*1 L_ax,L_ix,L_ox,L_ux,L_ebe,L_ibe,L_obe,L_ade
     &,L_afe,L_ufe,L_eke
      End subroutine TELEGKA_TRANSP_HANDLER
      End interface
