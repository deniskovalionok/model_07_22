      Interface
      Subroutine KONTEYNER_FDA30(ext_deltat,L_e,R_o,R_ad,R_od
     &,R_af,R_if,R_of,R_uf,R_ak,R_ek,R_ik,R_ok,R_uk,R_al,R_el
     &,R_il,R_ol,R_am,R_em,R_im,R_om,R_ap,L_op,R_up,R_or,R_ur
     &,R_es,R_is,R_os,R_at,L_et,L_ut,R_ov,R_uv,R_ox,R_ux,R_ebe
     &,R_ibe,R_obe,R_ade,L_ede,L_ude,R_ofe,L_ufe,L_ake,R_eke
     &,L_uke,L_ole,R_ame,R_ome,R_ipe,R_ope,R_ere,R_ire,R_ore
     &,R_ure,R_ase,R_ese,R_ise,R_ose,R_use,R_ate,R_ite,L_ote
     &,L_eve,R_axe,R_exe)
C |L_e           |1 1 O|TRIG_VAL        |��������� �������||
C |R_o           |4 4 K|_lcmpJ7014      |[]�������� ������ �����������|-5|
C |R_ad          |4 4 K|_lcmpJ7011      |[]�������� ������ �����������|10.0|
C |R_od          |4 4 K|_lcmpJ7005      |[]�������� ������ �����������|-5|
C |R_af          |4 4 K|_lcmpJ7002      |[]�������� ������ �����������|10.0|
C |R_if          |4 4 K|_uintJ6943      |����������� ������ ����������� ������|13146|
C |R_of          |4 4 K|_tintJ6943      |[���]�������� T �����������|1|
C |R_uf          |4 4 K|_lintJ6943      |����������� ������ ����������� �����|0.0|
C |R_ak          |4 4 K|_uintJ6942      |����������� ������ ����������� ������|4992|
C |R_ek          |4 4 K|_tintJ6942      |[���]�������� T �����������|1|
C |R_ik          |4 4 K|_lintJ6942      |����������� ������ ����������� �����|0|
C |R_ok          |4 4 K|_lcmpJ6909      |[]�������� ������ �����������|4260|
C |R_uk          |4 4 K|_lcmpJ6908      |[]�������� ������ �����������|4240|
C |R_al          |4 4 K|_lcmpJ6907      |[]�������� ������ �����������|0.1|
C |R_el          |4 4 K|_lcmpJ6875      |[]�������� ������ �����������|260|
C |R_il          |4 4 K|_lcmpJ6874      |[]�������� ������ �����������|270|
C |R_ol          |4 4 K|_lcmpJ6873      |[]�������� ������ �����������|280|
C |R_am          |4 4 I|FDA32AM001VS01  |��������� ���������||
C |R_em          |4 4 K|_lcmpJ6683      |[]�������� ������ �����������|11889|
C |R_im          |4 4 K|_lcmpJ6682      |[]�������� ������ �����������|11895|
C |R_om          |4 4 O|VS02            |�������� ��������||
C |R_ap          |4 4 I|FDA32AM001VS02  |�������� ���������||
C |L_op          |1 1 O|VP04            |��������� �������||
C |R_up          |4 4 K|_lcmpJ6609      |[]�������� ������ �����������|280|
C |R_or          |4 4 K|_lcmpJ6603      |[]�������� ������ �����������|297|
C |R_ur          |4 4 K|_lcmpJ6597      |[]�������� ������ �����������|303|
C |R_es          |4 4 I|FDA34BB003KE03VY01|���������� ������� �� OY||
C |R_is          |4 4 K|_lcmpJ6592      |[]�������� ������ �����������|6052|
C |R_os          |4 4 K|_lcmpJ6591      |[]�������� ������ �����������|6062|
C |R_at          |4 4 I|FDA34BB003KE03VY02|�������� ������������ �� OX||
C |L_et          |1 1 S|_qffJ6608*      |�������� ������ Q RS-��������  |F|
C |L_ut          |1 1 O|VP03            |��������� �������||
C |R_ov          |4 4 K|_lcmpJ6578      |[]�������� ������ �����������|0.1|
C |R_uv          |4 4 K|_lcmpJ6457      |[]�������� ������ �����������|280|
C |R_ox          |4 4 K|_lcmpJ6451      |[]�������� ������ �����������|297|
C |R_ux          |4 4 K|_lcmpJ6445      |[]�������� ������ �����������|303|
C |R_ebe         |4 4 I|FDA34BB002KE03VY01|���������� ������� �� OY||
C |R_ibe         |4 4 K|_lcmpJ6440      |[]�������� ������ �����������|5152|
C |R_obe         |4 4 K|_lcmpJ6439      |[]�������� ������ �����������|5162|
C |R_ade         |4 4 I|FDA34BB002KE03VY02|�������� ������������ �� OX||
C |L_ede         |1 1 S|_qffJ6456*      |�������� ������ Q RS-��������  |F|
C |L_ude         |1 1 O|VP02            |��������� �������||
C |R_ofe         |4 4 K|_lcmpJ6426      |[]�������� ������ �����������|0.1|
C |L_ufe         |1 1 O|Block3          |��������� ������������ ��������||
C |L_ake         |1 1 O|Block2          |��������� ������������ ��������||
C |R_eke         |4 4 K|_lcmpJ6405      |[]�������� ������ �����������|0.1|
C |L_uke         |1 1 O|Block1          |��������� ������������ ��������||
C |L_ole         |1 1 O|Block           |��������� ������������ ��������||
C |R_ame         |4 4 I|FDA34BB001KE03VY01|���������� ������� �� OY||
C |R_ome         |4 4 I|FDA30AE401-M02VY02|�������� �������  �� OY||
C |R_ipe         |4 4 I|FDA30AE401-M02VY01|���������� ���� �� OY||
C |R_ope         |4 4 I|FDA30AE401-M01VX01|���������� �������  �� OX||
C |R_ere         |4 4 I|FDA30AE401-M01VX02|�������� �������  �� OX||
C |R_ire         |4 4 O|_ointJ6943*     |�������� ������ ����������� |`START_X`|
C |R_ore         |4 4 O|VX02            |�������� ���������� �� X||
C |R_ure         |4 4 O|VY01            |��������� ���������� �� OY||
C |R_ase         |4 4 O|_ointJ6942*     |�������� ������ ����������� |`START_Y`|
C |R_ese         |4 4 O|R3VY02          |�������� ���������� �� OY||
C |R_ise         |4 4 O|R2VY02          |�������� ���������� �� OY||
C |R_ose         |4 4 O|VY02            |�������� ���������� �� OY||
C |R_use         |4 4 O|R0VY02          |�������� ���������� �� Y||
C |R_ate         |4 4 O|R1VY02          |�������� ���������� �� OY||
C |R_ite         |4 4 I|FDA34BB001KE03VY02|�������� ������������ �� OX||
C |L_ote         |1 1 S|_qffJ6362*      |�������� ������ Q RS-��������  |F|
C |L_eve         |1 1 O|VP01            |��������� �������||
C |R_axe         |4 4 O|VS01            |��������� ����������||
C |R_exe         |4 4 O|VX01            |���������� ���������� �� X||

      IMPLICIT NONE
      REAL*4 ext_deltat
      LOGICAL*1 L_e
      REAL*4 R_o,R_ad,R_od,R_af,R_if,R_of,R_uf,R_ak,R_ek,R_ik
     &,R_ok,R_uk,R_al,R_el,R_il,R_ol,R_am,R_em,R_im,R_om,R_ap
      LOGICAL*1 L_op
      REAL*4 R_up,R_or,R_ur,R_es,R_is,R_os,R_at
      LOGICAL*1 L_et,L_ut
      REAL*4 R_ov,R_uv,R_ox,R_ux,R_ebe,R_ibe,R_obe,R_ade
      LOGICAL*1 L_ede,L_ude
      REAL*4 R_ofe
      LOGICAL*1 L_ufe,L_ake
      REAL*4 R_eke
      LOGICAL*1 L_uke,L_ole
      REAL*4 R_ame,R_ome,R_ipe,R_ope,R_ere,R_ire,R_ore,R_ure
     &,R_ase,R_ese,R_ise,R_ose,R_use,R_ate,R_ite
      LOGICAL*1 L_ote,L_eve
      REAL*4 R_axe,R_exe
      End subroutine KONTEYNER_FDA30
      End interface
