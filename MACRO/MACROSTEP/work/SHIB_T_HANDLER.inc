      Interface
      Subroutine SHIB_T_HANDLER(ext_deltat,R_ive,R8_ade,R_i
     &,R_u,C30_af,R_uf,R_ak,R_ek,R_ik,R_ok,R_uk,R_el,R_ol
     &,L_ul,L_em,L_im,L_om,R_um,L_ep,L_ip,L_op,L_up,L_ar,L_or
     &,L_ur,L_es,L_is,L_us,R8_et,R_av,R8_ov,L_uv,L_ax,L_ix
     &,L_ox,L_oke,R_ile,R_ule,L_ure,L_ese,L_ite,L_ave,L_ove
     &,L_uve,L_axe,L_exe,L_ixe,L_oxe)
C |R_ive         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |R8_ade        |4 8 O|16 value        |��� �������� ��������|0.5|
C |R_i           |4 4 O|VY01            |��������� �� OY||
C |R_u           |4 4 O|VY02            |��������||
C |C30_af        |3 30 O|state           |||
C |R_uf          |4 4 S|vmclose_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ak          |4 4 I|vmclose_button  |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_ek          |4 4 S|vmopen_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ik          |4 4 I|vmopen_button   |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_ok          |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_uk          |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_el          |4 4 O|POS_CL          |��������||
C |R_ol          |4 4 O|POS_OP          |��������||
C |L_ul          |1 1 I|vlv_kvit        |||
C |L_em          |1 1 I|instr_fault     |||
C |L_im          |1 1 S|_qffJ1618*      |�������� ������ Q RS-��������  |F|
C |L_om          |1 1 O|vmclose_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |R_um          |4 4 I|tclose          |����� ���� �������|30.0|
C |L_ep          |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_ip          |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_op          |1 1 O|block           |||
C |L_up          |1 1 O|vmopen_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ar          |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_or          |1 1 O|STOP            |�������||
C |L_ur          |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_es          |1 1 O|fault           |�������������||
C |L_is          |1 1 O|norm            |�����||
C |L_us          |1 1 O|nopower         |��� ����������||
C |R8_et         |4 8 I|voltage         |[��]���������� �� ������||
C |R_av          |4 4 I|power           |�������� ��������||
C |R8_ov         |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_uv          |1 1 I|YA22            |������� ������� �������|F|
C |L_ax          |1 1 I|YA12            |������� ������� �� ����������|F|
C |L_ix          |1 1 I|YA21            |������� ������� �������|F|
C |L_ox          |1 1 I|YA11            |������� ������� �� ����������|F|
C |L_oke         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_ile         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_ule         |4 4 I|topen           |����� ���� �������|30.0|
C |L_ure         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_ese         |1 1 O|YV12            |�� ������� (���)|F|
C |L_ite         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ave         |1 1 O|YV11            |�� ������� (���)|F|
C |L_ove         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_uve         |1 1 I|mlf23           |������� ������� �����||
C |L_axe         |1 1 I|mlf22           |����� ����� ��������||
C |L_exe         |1 1 I|mlf04           |�������� �� ��������||
C |L_ixe         |1 1 I|mlf03           |�������� �� ��������||
C |L_oxe         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_i,R_u
      CHARACTER*30 C30_af
      REAL*4 R_uf,R_ak,R_ek,R_ik,R_ok,R_uk,R_el,R_ol
      LOGICAL*1 L_ul,L_em,L_im,L_om
      REAL*4 R_um
      LOGICAL*1 L_ep,L_ip,L_op,L_up,L_ar,L_or,L_ur,L_es,L_is
     &,L_us
      REAL*8 R8_et
      REAL*4 R_av
      REAL*8 R8_ov
      LOGICAL*1 L_uv,L_ax,L_ix,L_ox
      REAL*8 R8_ade
      LOGICAL*1 L_oke
      REAL*4 R_ile,R_ule
      LOGICAL*1 L_ure,L_ese,L_ite,L_ave
      REAL*4 R_ive
      LOGICAL*1 L_ove,L_uve,L_axe,L_exe,L_ixe,L_oxe
      End subroutine SHIB_T_HANDLER
      End interface
