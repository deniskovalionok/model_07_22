      Interface
      Subroutine DAT_SAS_3_HANDLER(ext_deltat,R_od,R_ud,L_af
     &,R_uf,R_ak,I_al,R_el,R_il,R_ap,R_ep,L_ip,R_er,R_ir,I_is
     &,R_os,R_us,R_iv,R_ov,L_uv,R_ox,R_ux,I_ube,R_ade,R_ede
     &,R_ufe,R_ake,L_eke,R_ale,R_ele,I_eme,R_ime,R_ome)
C |R_od          |4 4 I|LOWER_warning_setpoint4|||
C |R_ud          |4 4 I|LOWER_alarm_setpoint4|||
C |L_af          |1 1 I|fault4          |�����4||
C |R_uf          |4 4 I|UPPER_alarm_setpoint4|||
C |R_ak          |4 4 I|UPPER_warning_setpoint4|||
C |I_al          |2 4 O|dat4_color      |���� ��������4||
C |R_el          |4 4 O|KKS4            |�����4||
C |R_il          |4 4 I|input4          |����4||
C |R_ap          |4 4 I|LOWER_warning_setpoint3|||
C |R_ep          |4 4 I|LOWER_alarm_setpoint3|||
C |L_ip          |1 1 I|fault3          |�����3||
C |R_er          |4 4 I|UPPER_alarm_setpoint3|||
C |R_ir          |4 4 I|UPPER_warning_setpoint3|||
C |I_is          |2 4 O|dat3_color      |���� ��������3||
C |R_os          |4 4 O|KKS3            |�����3||
C |R_us          |4 4 I|input3          |����3||
C |R_iv          |4 4 I|LOWER_warning_setpoint2|||
C |R_ov          |4 4 I|LOWER_alarm_setpoint2|||
C |L_uv          |1 1 I|fault2          |�����2||
C |R_ox          |4 4 I|UPPER_alarm_setpoint2|||
C |R_ux          |4 4 I|UPPER_warning_setpoint2|||
C |I_ube         |2 4 O|dat2_color      |���� ��������2||
C |R_ade         |4 4 O|KKS2            |�����2||
C |R_ede         |4 4 I|input2          |����2||
C |R_ufe         |4 4 I|LOWER_warning_setpoint1|||
C |R_ake         |4 4 I|LOWER_alarm_setpoint1|||
C |L_eke         |1 1 I|fault1          |�����1||
C |R_ale         |4 4 I|UPPER_alarm_setpoint1|||
C |R_ele         |4 4 I|UPPER_warning_setpoint1|||
C |I_eme         |2 4 O|dat1_color      |���� ��������1||
C |R_ime         |4 4 O|KKS1            |�����1||
C |R_ome         |4 4 I|input1          |����1||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_od,R_ud
      LOGICAL*1 L_af
      REAL*4 R_uf,R_ak
      INTEGER*4 I_al
      REAL*4 R_el,R_il,R_ap,R_ep
      LOGICAL*1 L_ip
      REAL*4 R_er,R_ir
      INTEGER*4 I_is
      REAL*4 R_os,R_us,R_iv,R_ov
      LOGICAL*1 L_uv
      REAL*4 R_ox,R_ux
      INTEGER*4 I_ube
      REAL*4 R_ade,R_ede,R_ufe,R_ake
      LOGICAL*1 L_eke
      REAL*4 R_ale,R_ele
      INTEGER*4 I_eme
      REAL*4 R_ime,R_ome
      End subroutine DAT_SAS_3_HANDLER
      End interface
