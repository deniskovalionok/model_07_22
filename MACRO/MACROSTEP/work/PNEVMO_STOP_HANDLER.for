      Subroutine PNEVMO_STOP_HANDLER(ext_deltat,R_ote,R8_ebe
     &,C30_ed,R_af,R_ef,R_if,R_of,R_uf,R_ak,R_ik,R_uk,L_al
     &,L_il,L_ol,L_ul,R_am,L_im,L_om,L_um,L_ap,L_ep,L_up,L_ar
     &,L_ir,L_or,L_as,R8_is,R_et,R8_ut,L_av,L_ev,L_ov,L_uv
     &,L_ufe,R_oke,R_ale,L_are,L_ire,L_ose,L_ete,L_ute,L_ave
     &,L_eve,L_ive,L_ove,L_uve)
C |R_ote         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |R8_ebe        |4 8 O|16 value        |��� �������� ��������|0.5|
C |C30_ed        |3 30 O|state           |||
C |R_af          |4 4 S|vmclose_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ef          |4 4 I|vmclose_button  |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_if          |4 4 S|vmopen_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_of          |4 4 I|vmopen_button   |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_uf          |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ak          |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_ik          |4 4 O|POS_CL          |��������||
C |R_uk          |4 4 O|POS_OP          |��������||
C |L_al          |1 1 I|vlv_kvit        |||
C |L_il          |1 1 I|instr_fault     |||
C |L_ol          |1 1 S|_qffJ1618*      |�������� ������ Q RS-��������  |F|
C |L_ul          |1 1 O|vmclose_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |R_am          |4 4 I|tclose          |����� ���� �������|30.0|
C |L_im          |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_om          |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_um          |1 1 O|block           |||
C |L_ap          |1 1 O|vmopen_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ep          |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_up          |1 1 O|STOP            |�������||
C |L_ar          |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_ir          |1 1 O|fault           |�������������||
C |L_or          |1 1 O|norm            |�����||
C |L_as          |1 1 O|nopower         |��� ����������||
C |R8_is         |4 8 I|voltage         |[��]���������� �� ������||
C |R_et          |4 4 I|power           |�������� ��������||
C |R8_ut         |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_av          |1 1 I|YA22            |������� ������� �������|F|
C |L_ev          |1 1 I|YA12            |������� ������� �� ����������|F|
C |L_ov          |1 1 I|YA21            |������� ������� �������|F|
C |L_uv          |1 1 I|YA11            |������� ������� �� ����������|F|
C |L_ufe         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_oke         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_ale         |4 4 I|topen           |����� ���� �������|30.0|
C |L_are         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_ire         |1 1 O|YV12            |�� ������� (���)|F|
C |L_ose         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ete         |1 1 O|YV11            |�� ������� (���)|F|
C |L_ute         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_ave         |1 1 I|mlf23           |������� ������� �����||
C |L_eve         |1 1 I|mlf22           |����� ����� ��������||
C |L_ive         |1 1 I|mlf04           |�������� �� ��������||
C |L_ove         |1 1 I|mlf03           |�������� �� ��������||
C |L_uve         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      CHARACTER*30 C30_e,C30_i,C30_o,C30_u,C30_ad,C30_ed,C30_id
     &,C30_od,C30_ud
      REAL*4 R_af,R_ef,R_if,R_of,R_uf,R_ak,R0_ek,R_ik,R0_ok
     &,R_uk
      LOGICAL*1 L_al,L0_el,L_il,L_ol,L_ul
      REAL*4 R_am,R0_em
      LOGICAL*1 L_im,L_om,L_um,L_ap,L_ep,L0_ip,L0_op,L_up
     &,L_ar,L0_er,L_ir,L_or,L0_ur,L_as
      REAL*4 R0_es
      REAL*8 R8_is
      LOGICAL*1 L0_os,L0_us
      REAL*4 R0_at,R_et,R0_it,R0_ot
      REAL*8 R8_ut
      LOGICAL*1 L_av,L_ev,L0_iv,L_ov,L_uv,L0_ax,L0_ex,L0_ix
      REAL*4 R0_ox,R0_ux
      LOGICAL*1 L0_abe
      REAL*8 R8_ebe
      REAL*4 R0_ibe,R0_obe,R0_ube,R0_ade,R0_ede,R0_ide
      LOGICAL*1 L0_ode
      REAL*4 R0_ude,R0_afe,R0_efe,R0_ife
      LOGICAL*1 L0_ofe,L_ufe
      REAL*4 R0_ake,R0_eke,R0_ike,R_oke,R0_uke,R_ale,R0_ele
     &,R0_ile,R0_ole,R0_ule,R0_ame,R0_eme,R0_ime,R0_ome
      LOGICAL*1 L0_ume,L0_ape,L0_epe,L0_ipe,L0_ope,L0_upe
     &,L_are,L0_ere,L_ire,L0_ore,L0_ure,L0_ase,L0_ese,L0_ise
     &,L_ose,L0_use,L0_ate,L_ete,L0_ite
      REAL*4 R_ote
      LOGICAL*1 L_ute,L_ave,L_eve,L_ive,L_ove,L_uve

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      C30_o = '���������� �����������'
C PNEVMO_STOP_HANDLER.fmg( 173, 222):��������� ���������� CH20 (CH30) (�������)
      C30_e = '���������� �����������'
C PNEVMO_STOP_HANDLER.fmg( 200, 221):��������� ���������� CH20 (CH30) (�������)
      C30_u = '���������� ������'
C PNEVMO_STOP_HANDLER.fmg( 122, 224):��������� ���������� CH20 (CH30) (�������)
      C30_ad = ''
C PNEVMO_STOP_HANDLER.fmg( 122, 226):��������� ���������� CH20 (CH30) (�������)
      C30_od = '���������� ������'
C PNEVMO_STOP_HANDLER.fmg( 144, 223):��������� ���������� CH20 (CH30) (�������)
      L_ul=R_ef.ne.R_af
      R_af=R_ef
C PNEVMO_STOP_HANDLER.fmg(  14, 194):���������� ������������� ������
      L0_iv = (.NOT.L_im).AND.L_ul
C PNEVMO_STOP_HANDLER.fmg(  66, 194):�
      L0_ore = L0_iv.OR.L_ev.OR.L_av
C PNEVMO_STOP_HANDLER.fmg(  70, 192):���
      L_ap=R_of.ne.R_if
      R_if=R_of
C PNEVMO_STOP_HANDLER.fmg(  16, 236):���������� ������������� ������
      L0_ax = L_ap.AND.(.NOT.L_om)
C PNEVMO_STOP_HANDLER.fmg(  66, 236):�
      L0_ate = L0_ax.OR.L_uv.OR.L_ov
C PNEVMO_STOP_HANDLER.fmg(  70, 234):���
      L0_ip = L0_ate.OR.L0_ore
C PNEVMO_STOP_HANDLER.fmg(  76, 203):���
      L_ep=R_ak.ne.R_uf
      R_uf=R_ak
C PNEVMO_STOP_HANDLER.fmg(  14, 208):���������� ������������� ������
      L_ar=(L_ep.or.L_ar).and..not.(L0_ip)
      L0_op=.not.L_ar
C PNEVMO_STOP_HANDLER.fmg( 106, 205):RS �������,10
      L_up=L_ar
C PNEVMO_STOP_HANDLER.fmg( 125, 207):������,STOP
      L0_epe = L_ar.OR.L_ute
C PNEVMO_STOP_HANDLER.fmg(  98, 214):���
      R0_ek = 100
C PNEVMO_STOP_HANDLER.fmg( 378, 273):��������� (RE4) (�������)
      R0_ok = 100
C PNEVMO_STOP_HANDLER.fmg( 365, 267):��������� (RE4) (�������)
      L_ol=(L_il.or.L_ol).and..not.(L_al)
      L0_el=.not.L_ol
C PNEVMO_STOP_HANDLER.fmg( 326, 178):RS �������
      R0_em = DeltaT
C PNEVMO_STOP_HANDLER.fmg( 250, 254):��������� (RE4) (�������)
      if(R_am.ge.0.0) then
         R0_ole=R0_em/max(R_am,1.0e-10)
      else
         R0_ole=R0_em/min(R_am,-1.0e-10)
      endif
C PNEVMO_STOP_HANDLER.fmg( 259, 252):�������� ����������
      L_um = L_om.OR.L_im
C PNEVMO_STOP_HANDLER.fmg(  68, 210):���
      L0_ur =.NOT.(L_ove.OR.L_ive.OR.L_uve.OR.L_eve.OR.L_ave.OR.L_ute
     &)
C PNEVMO_STOP_HANDLER.fmg( 319, 191):���
      L0_er =.NOT.(L0_ur)
C PNEVMO_STOP_HANDLER.fmg( 328, 185):���
      L_ir = L0_er.OR.L_ol
C PNEVMO_STOP_HANDLER.fmg( 332, 184):���
      L_or = L0_ur.OR.L_il
C PNEVMO_STOP_HANDLER.fmg( 328, 190):���
      R0_es = 0.1
C PNEVMO_STOP_HANDLER.fmg( 254, 160):��������� (RE4) (�������)
      L_as=R8_is.lt.R0_es
C PNEVMO_STOP_HANDLER.fmg( 259, 162):���������� <
      R0_at = 0.0
C PNEVMO_STOP_HANDLER.fmg( 266, 182):��������� (RE4) (�������)
      R0_ede = 0.000001
C PNEVMO_STOP_HANDLER.fmg( 354, 208):��������� (RE4) (�������)
      R0_ide = 0.999999
C PNEVMO_STOP_HANDLER.fmg( 354, 224):��������� (RE4) (�������)
      R0_ox = 0.0
C PNEVMO_STOP_HANDLER.fmg( 296, 244):��������� (RE4) (�������)
      R0_ux = 0.0
C PNEVMO_STOP_HANDLER.fmg( 370, 242):��������� (RE4) (�������)
      L0_abe = L_ave.OR.L_eve
C PNEVMO_STOP_HANDLER.fmg( 363, 237):���
      R0_obe = 1.0
C PNEVMO_STOP_HANDLER.fmg( 348, 252):��������� (RE4) (�������)
      R0_ube = 0.0
C PNEVMO_STOP_HANDLER.fmg( 340, 252):��������� (RE4) (�������)
      R0_afe = 1.0e-10
C PNEVMO_STOP_HANDLER.fmg( 318, 228):��������� (RE4) (�������)
      R0_ake = 0.0
C PNEVMO_STOP_HANDLER.fmg( 328, 242):��������� (RE4) (�������)
      R0_ele = DeltaT
C PNEVMO_STOP_HANDLER.fmg( 250, 264):��������� (RE4) (�������)
      if(R_ale.ge.0.0) then
         R0_ule=R0_ele/max(R_ale,1.0e-10)
      else
         R0_ule=R0_ele/min(R_ale,-1.0e-10)
      endif
C PNEVMO_STOP_HANDLER.fmg( 259, 262):�������� ����������
      R0_ome = 0.0
C PNEVMO_STOP_HANDLER.fmg( 282, 244):��������� (RE4) (�������)
      L0_ipe = (.NOT.L_up).AND.L_ire
C PNEVMO_STOP_HANDLER.fmg( 103, 166):�
C label 96  try96=try96-1
      if(L_eve) then
         R0_ibe=R0_ux
      else
         R0_ibe=R8_ebe
      endif
C PNEVMO_STOP_HANDLER.fmg( 373, 242):���� RE IN LO CH7
      if(.NOT.L0_abe) then
         R_oke=R8_ebe
      endif
C PNEVMO_STOP_HANDLER.fmg( 384, 252):���� � ������������� �������
      L0_ase = (.NOT.L_up).AND.L_ete
C PNEVMO_STOP_HANDLER.fmg( 105, 260):�
      L0_ise = L_ete.OR.L0_epe.OR.L0_ore
C PNEVMO_STOP_HANDLER.fmg( 104, 229):���
      L0_ite = (.NOT.L_ete).AND.L0_ate
C PNEVMO_STOP_HANDLER.fmg( 104, 235):�
      L_ose=(L0_ite.or.L_ose).and..not.(L0_ise)
      L0_use=.not.L_ose
C PNEVMO_STOP_HANDLER.fmg( 111, 233):RS �������,1
      L0_ese = (.NOT.L0_ase).AND.L_ose
C PNEVMO_STOP_HANDLER.fmg( 130, 236):�
      L0_ix = L0_ese.AND.(.NOT.L_ive)
C PNEVMO_STOP_HANDLER.fmg( 252, 242):�
      L0_ape = L0_ix.OR.L_ove
C PNEVMO_STOP_HANDLER.fmg( 265, 241):���
      if(L0_ape) then
         R0_eme=R0_ule
      else
         R0_eme=R0_ome
      endif
C PNEVMO_STOP_HANDLER.fmg( 287, 254):���� RE IN LO CH7
      L0_upe = L0_epe.OR.L_ire.OR.L0_ate
C PNEVMO_STOP_HANDLER.fmg( 104, 185):���
      L0_ure = L0_ore.AND.(.NOT.L_ire)
C PNEVMO_STOP_HANDLER.fmg( 104, 191):�
      L_are=(L0_ure.or.L_are).and..not.(L0_upe)
      L0_ere=.not.L_are
C PNEVMO_STOP_HANDLER.fmg( 111, 189):RS �������,2
      L0_ope = L_are.AND.(.NOT.L0_ipe)
C PNEVMO_STOP_HANDLER.fmg( 130, 190):�
      L0_ex = L0_ope.AND.(.NOT.L_ove)
C PNEVMO_STOP_HANDLER.fmg( 252, 228):�
      L0_ume = L0_ex.OR.L_ive
C PNEVMO_STOP_HANDLER.fmg( 265, 227):���
      if(L0_ume) then
         R0_ame=R0_ole
      else
         R0_ame=R0_ome
      endif
C PNEVMO_STOP_HANDLER.fmg( 287, 236):���� RE IN LO CH7
      R0_ime = R0_eme + (-R0_ame)
C PNEVMO_STOP_HANDLER.fmg( 293, 246):��������
      if(L_ute) then
         R0_eke=R0_ox
      else
         R0_eke=R0_ime
      endif
C PNEVMO_STOP_HANDLER.fmg( 300, 244):���� RE IN LO CH7
      R0_ife = R_oke + (-R_ote)
C PNEVMO_STOP_HANDLER.fmg( 308, 235):��������
      R0_ude = R0_eke + R0_ife
C PNEVMO_STOP_HANDLER.fmg( 314, 236):��������
      R0_efe = R0_ude * R0_ife
C PNEVMO_STOP_HANDLER.fmg( 318, 235):����������
      L0_ofe=R0_efe.lt.R0_afe
C PNEVMO_STOP_HANDLER.fmg( 323, 234):���������� <
      L_ufe=(L0_ofe.or.L_ufe).and..not.(.NOT.L_uve)
      L0_ode=.not.L_ufe
C PNEVMO_STOP_HANDLER.fmg( 330, 232):RS �������,6
      if(L_ufe) then
         R_oke=R_ote
      endif
C PNEVMO_STOP_HANDLER.fmg( 324, 252):���� � ������������� �������
      if(L_ufe) then
         R0_ike=R0_ake
      else
         R0_ike=R0_eke
      endif
C PNEVMO_STOP_HANDLER.fmg( 332, 244):���� RE IN LO CH7
      R0_uke = R_oke + R0_ike
C PNEVMO_STOP_HANDLER.fmg( 338, 245):��������
      R0_ade=MAX(R0_ube,R0_uke)
C PNEVMO_STOP_HANDLER.fmg( 346, 246):��������
      R0_ile=MIN(R0_obe,R0_ade)
C PNEVMO_STOP_HANDLER.fmg( 354, 247):�������
      L_ire=R0_ile.lt.R0_ede
C PNEVMO_STOP_HANDLER.fmg( 363, 210):���������� <
C sav1=L0_upe
      L0_upe = L0_epe.OR.L_ire.OR.L0_ate
C PNEVMO_STOP_HANDLER.fmg( 104, 185):recalc:���
C if(sav1.ne.L0_upe .and. try134.gt.0) goto 134
C sav1=L0_ure
      L0_ure = L0_ore.AND.(.NOT.L_ire)
C PNEVMO_STOP_HANDLER.fmg( 104, 191):recalc:�
C if(sav1.ne.L0_ure .and. try136.gt.0) goto 136
C sav1=L0_ipe
      L0_ipe = (.NOT.L_up).AND.L_ire
C PNEVMO_STOP_HANDLER.fmg( 103, 166):recalc:�
C if(sav1.ne.L0_ipe .and. try96.gt.0) goto 96
      if(L0_abe) then
         R8_ebe=R0_ibe
      else
         R8_ebe=R0_ile
      endif
C PNEVMO_STOP_HANDLER.fmg( 377, 246):���� RE IN LO CH7
      R_uk = R0_ok * R8_ebe
C PNEVMO_STOP_HANDLER.fmg( 368, 266):����������
      R_ik = R0_ek + (-R_uk)
C PNEVMO_STOP_HANDLER.fmg( 382, 272):��������
      L_ete=R0_ile.gt.R0_ide
C PNEVMO_STOP_HANDLER.fmg( 363, 225):���������� >
C sav1=L0_ise
      L0_ise = L_ete.OR.L0_epe.OR.L0_ore
C PNEVMO_STOP_HANDLER.fmg( 104, 229):recalc:���
C if(sav1.ne.L0_ise .and. try114.gt.0) goto 114
C sav1=L0_ite
      L0_ite = (.NOT.L_ete).AND.L0_ate
C PNEVMO_STOP_HANDLER.fmg( 104, 235):recalc:�
C if(sav1.ne.L0_ite .and. try116.gt.0) goto 116
C sav1=L0_ase
      L0_ase = (.NOT.L_up).AND.L_ete
C PNEVMO_STOP_HANDLER.fmg( 105, 260):recalc:�
C if(sav1.ne.L0_ase .and. try110.gt.0) goto 110
      if(L_ete) then
         C30_ud=C30_u
      else
         C30_ud=C30_ad
      endif
C PNEVMO_STOP_HANDLER.fmg( 126, 224):���� RE IN LO CH20
      if(L_ire) then
         C30_id=C30_od
      else
         C30_id=C30_ud
      endif
C PNEVMO_STOP_HANDLER.fmg( 148, 224):���� RE IN LO CH20
      if(L0_ese) then
         C30_i=C30_o
      else
         C30_i=C30_id
      endif
C PNEVMO_STOP_HANDLER.fmg( 176, 222):���� RE IN LO CH20
      if(L0_ope) then
         C30_ed=C30_e
      else
         C30_ed=C30_i
      endif
C PNEVMO_STOP_HANDLER.fmg( 205, 222):���� RE IN LO CH20
      if(L0_abe) then
         R_oke=R0_ile
      endif
C PNEVMO_STOP_HANDLER.fmg( 366, 252):���� � ������������� �������
C sav1=R0_ife
      R0_ife = R_oke + (-R_ote)
C PNEVMO_STOP_HANDLER.fmg( 308, 235):recalc:��������
C if(sav1.ne.R0_ife .and. try159.gt.0) goto 159
C sav1=R0_uke
      R0_uke = R_oke + R0_ike
C PNEVMO_STOP_HANDLER.fmg( 338, 245):recalc:��������
C if(sav1.ne.R0_uke .and. try176.gt.0) goto 176
      L0_os = L0_ese.OR.L0_ope
C PNEVMO_STOP_HANDLER.fmg( 251, 174):���
      L0_us = L0_os.AND.(.NOT.L_as)
C PNEVMO_STOP_HANDLER.fmg( 266, 173):�
      if(L0_us) then
         R0_ot=R_et
      else
         R0_ot=R0_at
      endif
C PNEVMO_STOP_HANDLER.fmg( 269, 180):���� RE IN LO CH7
      R0_it = R8_ut
C PNEVMO_STOP_HANDLER.fmg( 264, 190):��������
C label 236  try236=try236-1
      R8_ut = R0_ot + R0_it
C PNEVMO_STOP_HANDLER.fmg( 275, 189):��������
C sav1=R0_it
      R0_it = R8_ut
C PNEVMO_STOP_HANDLER.fmg( 264, 190):recalc:��������
C if(sav1.ne.R0_it .and. try236.gt.0) goto 236
      End
