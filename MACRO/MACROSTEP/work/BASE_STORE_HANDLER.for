      Subroutine BASE_STORE_HANDLER(ext_deltat,R_aro,R8_obo
     &,I_id,I_ud,L_ef,I_uf,I_ak,I_ek,I_al,I_ol,I_em,I_um,I_ip
     &,I_ar,I_or,I_ov,I_ex,I_ix,R_ox,R_ux,R_abe,R_ebe,I_ube
     &,I_ide,C20_ife,L_ake,C20_uke,C8_ule,R_ope,R_upe,L_are
     &,R_ere,R_ire,I_ore,R_ise,R_use,I_ate,I_ote,I_eve,L_uve
     &,L_exe,L_ixe,L_oxe,L_abi,L_ibi,R_ubi,L_edi,L_idi,L_ifi
     &,L_iki,L_ali,L_eli,L_oli,L_ami,R8_imi,R_epi,R8_upi,L_ari
     &,L_iri,L_uri,I_asi,L_abo,R_ado,R_ako,L_ilo,L_ulo,L_apo
     &,L_opo,L_ero,L_iro,L_oro,L_uro,L_aso,L_eso)
C |R_aro         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |R8_obo        |4 8 O|16 value        |��� �������� ��������|0.5|
C |I_id          |2 4 K|_lcmpJ3357      |�������� ������ �����������|1|
C |I_ud          |2 4 K|_lcmpJ3354      |�������� ������ �����������|7|
C |L_ef          |1 1 S|_qffJ3425*      |�������� ������ Q RS-��������  |F|
C |I_uf          |2 4 I|POS             |�������� �������||
C |I_ak          |2 4 K|_lcmpJ3108      |�������� ������ �����������|7|
C |I_ek          |2 4 K|_lcmpJ3104      |�������� ������ �����������|1|
C |I_al          |2 4 O|LREADY          |����� ����������||
C |I_ol          |2 4 O|LLEVEL7         |����� ������� 7||
C |I_em          |2 4 O|LLEVEL6         |����� ������� 6||
C |I_um          |2 4 O|LLEVEL5         |����� ������� 5||
C |I_ip          |2 4 O|LLEVEL4         |����� ������� 4||
C |I_ar          |2 4 O|LLEVEL3         |����� ������� 3||
C |I_or          |2 4 O|LLEVEL2         |����� ������� 2||
C |I_ov          |2 4 O|LLEVEL1         |����� ������� 1||
C |I_ex          |2 4 O|count_state     ||1|
C |I_ix          |2 4 O|LBUSY           |����� �����||
C |R_ox          |4 4 S|vmdown_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ux          |4 4 I|vmdown_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_abe         |4 4 S|vmup_button_ST* |��������� ������ "������� ����� �� ���������" |0.0|
C |R_ebe         |4 4 I|vmup_button     |������� ������ ������ "������� ����� �� ���������" |0.0|
C |I_ube         |2 4 O|LUPO            |����� �����||
C |I_ide         |2 4 O|LDOWNC          |����� ����||
C |C20_ife       |3 20 O|task_state      |���������||
C |L_ake         |1 1 S|_qffJ3391*      |�������� ������ Q RS-��������  |F|
C |C20_uke       |3 20 O|task_name       |������� ���������||
C |C8_ule        |3 8 I|task            |������� ���������||
C |R_ope         |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_upe         |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_are         |1 1 O|vmstart_button_CMD*|[TF]����� ������ ������� �������� ��������� �� ���������|F|
C |R_ere         |4 4 S|vmstart_button_ST*|��������� ������ "������� �������� ��������� �� ���������" |0.0|
C |R_ire         |4 4 I|vmstart_button  |������� ������ ������ "������� �������� ��������� �� ���������" |0.0|
C |I_ore         |2 4 O|LERROR          |����� �������������||
C |R_ise         |4 4 O|POS_CL          |��������||
C |R_use         |4 4 O|POS_OP          |��������||
C |I_ate         |2 4 O|LDOWN           |����� �����||
C |I_ote         |2 4 O|LUP             |����� ������||
C |I_eve         |2 4 O|LZM             |������ "�������"||
C |L_uve         |1 1 I|vlv_kvit        |||
C |L_exe         |1 1 I|instr_fault     |||
C |L_ixe         |1 1 S|_qffJ1618*      |�������� ������ Q RS-��������  |F|
C |L_oxe         |1 1 I|FHDOS_M31_8     |���������� ����. ������. �� OS||
C |L_abi         |1 1 O|vmdown_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_ibi         |1 1 O|vmup_button_CMD*|[TF]����� ������ ������� ����� �� ���������|F|
C |R_ubi         |4 4 I|tdown           |����� ���� ����|30.0|
C |L_edi         |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_idi         |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_ifi         |1 1 O|block           |||
C |L_iki         |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_ali         |1 1 O|STOP            |�������||
C |L_eli         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_oli         |1 1 O|norm            |�����||
C |L_ami         |1 1 O|nopower         |��� ����������||
C |R8_imi        |4 8 I|voltage         |[��]���������� �� ������||
C |R_epi         |4 4 I|power           |�������� ��������||
C |R8_upi        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_ari         |1 1 I|YA25            |������� ������� �� ����������|F|
C |L_iri         |1 1 I|YA26            |������� ������� �� ����������|F|
C |L_uri         |1 1 O|fault           |�������������||
C |I_asi         |2 4 O|LAM             |������ "�������"||
C |L_abo         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_ado         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_ako         |4 4 I|tup             |����� ���� �����|30.0|
C |L_ilo         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_ulo         |1 1 O|XH53            |�� ������� (���)|F|
C |L_apo         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_opo         |1 1 O|XH54            |�� ������� (���)|F|
C |L_ero         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_iro         |1 1 I|mlf23           |������� ������� �����||
C |L_oro         |1 1 I|mlf22           |����� ����� ��������||
C |L_uro         |1 1 I|mlf04           |�������� �� ��������||
C |L_aso         |1 1 I|mlf03           |�������� �� ��������||
C |L_eso         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      LOGICAL*1 L0_e,L0_i,L0_o,L0_u,L0_ad,L0_ed
      INTEGER*4 I_id
      LOGICAL*1 L0_od
      INTEGER*4 I_ud
      LOGICAL*1 L0_af,L_ef
      INTEGER*4 I0_if
      LOGICAL*1 L0_of
      INTEGER*4 I_uf,I_ak,I_ek,I0_ik,I0_ok
      LOGICAL*1 L0_uk
      INTEGER*4 I_al,I0_el,I0_il,I_ol,I0_ul,I0_am,I_em,I0_im
     &,I0_om,I_um,I0_ap,I0_ep,I_ip,I0_op,I0_up,I_ar,I0_er
     &,I0_ir,I_or,I0_ur,I0_as
      LOGICAL*1 L0_es,L0_is,L0_os,L0_us,L0_at,L0_et,L0_it
     &,L0_ot,L0_ut,L0_av,L0_ev,L0_iv
      INTEGER*4 I_ov,I0_uv,I0_ax,I_ex,I_ix
      REAL*4 R_ox,R_ux,R_abe,R_ebe
      INTEGER*4 I0_ibe,I0_obe,I_ube,I0_ade,I0_ede,I_ide
      CHARACTER*20 C20_ode,C20_ude,C20_afe,C20_efe,C20_ife
      LOGICAL*1 L0_ofe
      CHARACTER*20 C20_ufe
      LOGICAL*1 L_ake
      CHARACTER*20 C20_eke,C20_ike,C20_oke,C20_uke
      CHARACTER*8 C8_ale
      LOGICAL*1 L0_ele
      CHARACTER*8 C8_ile
      LOGICAL*1 L0_ole
      CHARACTER*8 C8_ule
      INTEGER*4 I0_ame,I0_eme,I0_ime,I0_ome,I0_ume,I0_ape
     &,I0_epe,I0_ipe
      REAL*4 R_ope,R_upe
      LOGICAL*1 L_are
      REAL*4 R_ere,R_ire
      INTEGER*4 I_ore,I0_ure,I0_ase
      REAL*4 R0_ese,R_ise,R0_ose,R_use
      INTEGER*4 I_ate,I0_ete,I0_ite,I_ote,I0_ute,I0_ave,I_eve
     &,I0_ive,I0_ove
      LOGICAL*1 L_uve,L0_axe,L_exe,L_ixe,L_oxe,L0_uxe,L_abi
     &,L0_ebi,L_ibi
      REAL*4 R0_obi,R_ubi
      LOGICAL*1 L0_adi,L_edi,L_idi,L0_odi,L0_udi,L0_afi,L0_efi
     &,L_ifi,L0_ofi,L0_ufi,L0_aki,L0_eki,L_iki,L0_oki,L0_uki
     &,L_ali,L_eli,L0_ili,L_oli,L0_uli,L_ami
      REAL*4 R0_emi
      REAL*8 R8_imi
      LOGICAL*1 L0_omi,L0_umi
      REAL*4 R0_api,R_epi,R0_ipi,R0_opi
      REAL*8 R8_upi
      LOGICAL*1 L_ari,L0_eri,L_iri,L0_ori,L_uri
      INTEGER*4 I_asi,I0_esi,I0_isi
      LOGICAL*1 L0_osi,L0_usi
      REAL*4 R0_ati,R0_eti
      LOGICAL*1 L0_iti
      REAL*4 R0_oti,R0_uti,R0_avi,R0_evi,R0_ivi,R0_ovi
      LOGICAL*1 L0_uvi
      REAL*4 R0_axi,R0_exi,R0_ixi,R0_oxi
      LOGICAL*1 L0_uxi,L_abo
      REAL*4 R0_ebo,R0_ibo
      REAL*8 R8_obo
      REAL*4 R0_ubo,R_ado,R0_edo,R0_ido,R0_odo,R0_udo,R0_afo
     &,R0_efo,R0_ifo,R0_ofo,R0_ufo,R_ako
      LOGICAL*1 L0_eko,L0_iko,L0_oko,L0_uko,L0_alo,L0_elo
     &,L_ilo,L0_olo,L_ulo,L0_amo,L0_emo,L0_imo,L0_omo,L0_umo
     &,L_apo,L0_epo,L0_ipo,L_opo,L0_upo
      REAL*4 R_aro
      LOGICAL*1 L_ero,L_iro,L_oro,L_uro,L_aso,L_eso

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I0_ik = z'0100000A'
C BASE_STORE_HANDLER.fmg( 153, 337):��������� ������������� IN (�������)
      I0_ok = z'01000003'
C BASE_STORE_HANDLER.fmg( 153, 339):��������� ������������� IN (�������)
      I0_il = z'0100000A'
C BASE_STORE_HANDLER.fmg( 153, 358):��������� ������������� IN (�������)
      I0_el = z'01000003'
C BASE_STORE_HANDLER.fmg( 153, 356):��������� ������������� IN (�������)
      I0_am = z'01000003'
C BASE_STORE_HANDLER.fmg( 246, 298):��������� ������������� IN (�������)
      I0_ul = z'01000010'
C BASE_STORE_HANDLER.fmg( 246, 296):��������� ������������� IN (�������)
      I0_om = z'01000003'
C BASE_STORE_HANDLER.fmg( 246, 309):��������� ������������� IN (�������)
      I0_im = z'01000010'
C BASE_STORE_HANDLER.fmg( 246, 307):��������� ������������� IN (�������)
      I0_ep = z'01000003'
C BASE_STORE_HANDLER.fmg( 246, 320):��������� ������������� IN (�������)
      I0_ap = z'01000010'
C BASE_STORE_HANDLER.fmg( 246, 318):��������� ������������� IN (�������)
      I0_up = z'01000003'
C BASE_STORE_HANDLER.fmg( 246, 330):��������� ������������� IN (�������)
      I0_op = z'01000010'
C BASE_STORE_HANDLER.fmg( 246, 328):��������� ������������� IN (�������)
      I0_ir = z'01000003'
C BASE_STORE_HANDLER.fmg( 246, 340):��������� ������������� IN (�������)
      I0_er = z'01000010'
C BASE_STORE_HANDLER.fmg( 246, 338):��������� ������������� IN (�������)
      I0_as = z'01000003'
C BASE_STORE_HANDLER.fmg( 246, 350):��������� ������������� IN (�������)
      I0_ur = z'01000010'
C BASE_STORE_HANDLER.fmg( 246, 348):��������� ������������� IN (�������)
      I0_ax = z'01000003'
C BASE_STORE_HANDLER.fmg( 246, 360):��������� ������������� IN (�������)
      I0_uv = z'01000010'
C BASE_STORE_HANDLER.fmg( 246, 358):��������� ������������� IN (�������)
      L_abi=R_ux.ne.R_ox
      R_ox=R_ux
C BASE_STORE_HANDLER.fmg(  14, 396):���������� ������������� ������
      L0_ofe = L_abi.OR.L_ari
C BASE_STORE_HANDLER.fmg(  32, 444):���
      L_ibi=R_ebe.ne.R_abe
      R_abe=R_ebe
C BASE_STORE_HANDLER.fmg(  12, 452):���������� ������������� ������
      L0_o = L_ibi.OR.L_iri
C BASE_STORE_HANDLER.fmg(  23, 460):���
      I0_obe = z'01000003'
C BASE_STORE_HANDLER.fmg( 185, 432):��������� ������������� IN (�������)
      I0_ibe = z'0100000A'
C BASE_STORE_HANDLER.fmg( 185, 430):��������� ������������� IN (�������)
      I0_ede = z'01000003'
C BASE_STORE_HANDLER.fmg( 193, 382):��������� ������������� IN (�������)
      I0_ade = z'0100000A'
C BASE_STORE_HANDLER.fmg( 193, 380):��������� ������������� IN (�������)
      I0_ite = z'01000003'
C BASE_STORE_HANDLER.fmg( 115, 367):��������� ������������� IN (�������)
      I0_ete = z'0100000A'
C BASE_STORE_HANDLER.fmg( 115, 365):��������� ������������� IN (�������)
      C20_afe = '������'
C BASE_STORE_HANDLER.fmg(  29, 364):��������� ���������� CH20 (CH30) (�������)
      C20_efe = '������� ���'
C BASE_STORE_HANDLER.fmg(  29, 366):��������� ���������� CH20 (CH30) (�������)
      C20_ode = '�����'
C BASE_STORE_HANDLER.fmg(  44, 363):��������� ���������� CH20 (CH30) (�������)
      C20_ufe = '����'
C BASE_STORE_HANDLER.fmg(  50, 374):��������� ���������� CH20 (CH30) (�������)
      C20_ike = '�����'
C BASE_STORE_HANDLER.fmg(  35, 376):��������� ���������� CH20 (CH30) (�������)
      C20_oke = ''
C BASE_STORE_HANDLER.fmg(  35, 378):��������� ���������� CH20 (CH30) (�������)
      C8_ale = 'down'
C BASE_STORE_HANDLER.fmg(  18, 384):��������� ���������� CH8 (�������)
      call chcomp(C8_ule,C8_ale,L0_ele)
C BASE_STORE_HANDLER.fmg(  23, 388):���������� ���������
      C8_ile = 'up'
C BASE_STORE_HANDLER.fmg(  18, 426):��������� ���������� CH8 (�������)
      call chcomp(C8_ule,C8_ile,L0_ole)
C BASE_STORE_HANDLER.fmg(  23, 430):���������� ���������
      I0_ame = z'0100008E'
C BASE_STORE_HANDLER.fmg( 170, 397):��������� ������������� IN (�������)
      I0_eme = z'01000086'
C BASE_STORE_HANDLER.fmg( 164, 444):��������� ������������� IN (�������)
      I0_ome = z'01000003'
C BASE_STORE_HANDLER.fmg( 152, 459):��������� ������������� IN (�������)
      I0_ume = z'01000010'
C BASE_STORE_HANDLER.fmg( 152, 461):��������� ������������� IN (�������)
      I0_ipe = z'01000003'
C BASE_STORE_HANDLER.fmg( 159, 412):��������� ������������� IN (�������)
      I0_epe = z'01000010'
C BASE_STORE_HANDLER.fmg( 159, 410):��������� ������������� IN (�������)
      L_iki=R_upe.ne.R_ope
      R_ope=R_upe
C BASE_STORE_HANDLER.fmg(  14, 411):���������� ������������� ������
      L_are=R_ire.ne.R_ere
      R_ere=R_ire
C BASE_STORE_HANDLER.fmg(  19, 439):���������� ������������� ������
      L0_ebi = L_are.AND.L0_ole
C BASE_STORE_HANDLER.fmg(  30, 438):�
      L0_aki = L_ibi.OR.L0_ebi
C BASE_STORE_HANDLER.fmg(  52, 438):���
      L0_ori = L0_aki.AND.(.NOT.L_idi)
C BASE_STORE_HANDLER.fmg(  66, 440):�
      L0_ipo = L0_ori.OR.L_iri
C BASE_STORE_HANDLER.fmg(  70, 438):���
      L0_uxe = L_are.AND.L0_ele
C BASE_STORE_HANDLER.fmg(  29, 396):�
      L0_ofi = L_abi.OR.L0_uxe
C BASE_STORE_HANDLER.fmg(  52, 396):���
      L0_eri = (.NOT.L_edi).AND.L0_ofi
C BASE_STORE_HANDLER.fmg(  66, 398):�
      L0_amo = L0_eri.OR.L_ari
C BASE_STORE_HANDLER.fmg(  70, 396):���
      L0_oki = L0_ipo.OR.L_oxe.OR.L0_amo
C BASE_STORE_HANDLER.fmg(  82, 406):���
      L_eli=(L_iki.or.L_eli).and..not.(L0_oki)
      L0_uki=.not.L_eli
C BASE_STORE_HANDLER.fmg( 106, 408):RS �������,10
      L_ali=L_eli
C BASE_STORE_HANDLER.fmg( 125, 410):������,STOP
      L0_oko = L_eli.OR.L_ero
C BASE_STORE_HANDLER.fmg(  98, 418):���
      I0_ive = z'0100000E'
C BASE_STORE_HANDLER.fmg( 189, 404):��������� ������������� IN (�������)
      I0_ure = z'01000007'
C BASE_STORE_HANDLER.fmg( 150, 384):��������� ������������� IN (�������)
      I0_ase = z'01000003'
C BASE_STORE_HANDLER.fmg( 150, 386):��������� ������������� IN (�������)
      R0_ese = 100
C BASE_STORE_HANDLER.fmg( 378, 491):��������� (RE4) (�������)
      R0_ose = 100
C BASE_STORE_HANDLER.fmg( 365, 485):��������� (RE4) (�������)
      I0_ave = z'01000003'
C BASE_STORE_HANDLER.fmg( 122, 456):��������� ������������� IN (�������)
      I0_ute = z'0100000A'
C BASE_STORE_HANDLER.fmg( 122, 454):��������� ������������� IN (�������)
      I0_esi = z'0100000E'
C BASE_STORE_HANDLER.fmg( 182, 452):��������� ������������� IN (�������)
      L_ixe=(L_exe.or.L_ixe).and..not.(L_uve)
      L0_axe=.not.L_ixe
C BASE_STORE_HANDLER.fmg( 326, 396):RS �������
      L0_afi=.false.
C BASE_STORE_HANDLER.fmg(  64, 420):��������� ���������� (�������)
      L0_udi=.false.
C BASE_STORE_HANDLER.fmg(  64, 418):��������� ���������� (�������)
      L0_odi=.false.
C BASE_STORE_HANDLER.fmg(  64, 416):��������� ���������� (�������)
      L_ifi = L0_afi.OR.L0_udi.OR.L0_odi.OR.L_idi.OR.L_edi
C BASE_STORE_HANDLER.fmg(  68, 416):���
      if(L_ifi) then
         I_ix=I0_ik
      else
         I_ix=I0_ok
      endif
C BASE_STORE_HANDLER.fmg( 156, 338):���� RE IN LO CH7
      R0_obi = DeltaT
C BASE_STORE_HANDLER.fmg( 250, 472):��������� (RE4) (�������)
      if(R_ubi.ge.0.0) then
         R0_udo=R0_obi/max(R_ubi,1.0e-10)
      else
         R0_udo=R0_obi/min(R_ubi,-1.0e-10)
      endif
C BASE_STORE_HANDLER.fmg( 259, 470):�������� ����������
      L0_uli =.NOT.(L_aso.OR.L_uro.OR.L_eso.OR.L_oro.OR.L_iro.OR.L_ero
     &)
C BASE_STORE_HANDLER.fmg( 319, 409):���
      L0_ili =.NOT.(L0_uli)
C BASE_STORE_HANDLER.fmg( 328, 403):���
      L_uri = L0_ili.OR.L_ixe
C BASE_STORE_HANDLER.fmg( 332, 402):���
      L0_uk = L_uri.OR.L_ifi
C BASE_STORE_HANDLER.fmg( 152, 349):���
      if(L0_uk) then
         I_al=I0_el
      else
         I_al=I0_il
      endif
C BASE_STORE_HANDLER.fmg( 156, 357):���� RE IN LO CH7
      if(L_uri) then
         I_ore=I0_ure
      else
         I_ore=I0_ase
      endif
C BASE_STORE_HANDLER.fmg( 153, 384):���� RE IN LO CH7
      L_oli = L0_uli.OR.L_exe
C BASE_STORE_HANDLER.fmg( 328, 408):���
      R0_emi = 0.1
C BASE_STORE_HANDLER.fmg( 254, 378):��������� (RE4) (�������)
      L_ami=R8_imi.lt.R0_emi
C BASE_STORE_HANDLER.fmg( 259, 380):���������� <
      R0_api = 0.0
C BASE_STORE_HANDLER.fmg( 266, 400):��������� (RE4) (�������)
      R0_ivi = 0.000001
C BASE_STORE_HANDLER.fmg( 354, 426):��������� (RE4) (�������)
      R0_ovi = 0.999999
C BASE_STORE_HANDLER.fmg( 354, 442):��������� (RE4) (�������)
      R0_ati = 0.0
C BASE_STORE_HANDLER.fmg( 296, 462):��������� (RE4) (�������)
      R0_eti = 0.0
C BASE_STORE_HANDLER.fmg( 370, 460):��������� (RE4) (�������)
      L0_iti = L_iro.OR.L_oro
C BASE_STORE_HANDLER.fmg( 363, 455):���
      R0_uti = 1.0
C BASE_STORE_HANDLER.fmg( 348, 470):��������� (RE4) (�������)
      R0_avi = 0.0
C BASE_STORE_HANDLER.fmg( 340, 470):��������� (RE4) (�������)
      R0_exi = 1.0e-10
C BASE_STORE_HANDLER.fmg( 318, 446):��������� (RE4) (�������)
      R0_ebo = 0.0
C BASE_STORE_HANDLER.fmg( 328, 460):��������� (RE4) (�������)
      R0_ido = DeltaT
C BASE_STORE_HANDLER.fmg( 250, 482):��������� (RE4) (�������)
      if(R_ako.ge.0.0) then
         R0_afo=R0_ido/max(R_ako,1.0e-10)
      else
         R0_afo=R0_ido/min(R_ako,-1.0e-10)
      endif
C BASE_STORE_HANDLER.fmg( 259, 480):�������� ����������
      R0_ufo = 0.0
C BASE_STORE_HANDLER.fmg( 282, 462):��������� (RE4) (�������)
      L0_of=I_uf.le.I_ex
C BASE_STORE_HANDLER.fmg(  20, 468):���������� �������������
C label 200  try200=try200-1
      L0_od=I_ex.gt.I_ud
C BASE_STORE_HANDLER.fmg(  20, 464):���������� �������������
      L0_u = L0_of.OR.L0_od
C BASE_STORE_HANDLER.fmg(  28, 465):���
      L_ake=(L0_o.or.L_ake).and..not.(L0_u)
      L0_ad=.not.L_ake
C BASE_STORE_HANDLER.fmg(  28, 458):RS �������
      if(L_ake) then
         I0_if=I_uf
      else
         I0_if=I_ex
      endif
C BASE_STORE_HANDLER.fmg(  32, 472):���� RE IN LO CH7
      L0_af=I_uf.ge.I_ex
C BASE_STORE_HANDLER.fmg(  53, 460):���������� �������������
      L0_ed=I_ex.lt.I_id
C BASE_STORE_HANDLER.fmg(  54, 453):���������� �������������
      L0_e = L0_af.OR.L0_ed
C BASE_STORE_HANDLER.fmg(  60, 458):���
      L_ef=(L0_ofe.or.L_ef).and..not.(L0_e)
      L0_i=.not.L_ef
C BASE_STORE_HANDLER.fmg(  65, 460):RS �������
      if(L_ef) then
         I_ex=I_uf
      else
         I_ex=I0_if
      endif
C BASE_STORE_HANDLER.fmg(  69, 470):���� RE IN LO CH7
      L0_iv=I_ex.eq.1
      L0_ot=I_ex.eq.2
      L0_it=I_ex.eq.3
      L0_et=I_ex.eq.4
      L0_at=I_ex.eq.5
      L0_us=I_ex.eq.6
      L0_os=I_ex.eq.7
      L0_is=I_ex.eq.8
      L0_es=I_ex.eq.9
      L0_ev=I_ex.eq.10
      L0_av=I_ex.eq.11
      L0_ut=I_ex.eq.12
C BASE_STORE_HANDLER.fmg( 216, 344):���������� �� 12
      if(L0_os) then
         I_ol=I0_ul
      else
         I_ol=I0_am
      endif
C BASE_STORE_HANDLER.fmg( 250, 297):���� RE IN LO CH7
      if(L0_us) then
         I_em=I0_im
      else
         I_em=I0_om
      endif
C BASE_STORE_HANDLER.fmg( 250, 308):���� RE IN LO CH7
      if(L0_at) then
         I_um=I0_ap
      else
         I_um=I0_ep
      endif
C BASE_STORE_HANDLER.fmg( 250, 318):���� RE IN LO CH7
      if(L0_et) then
         I_ip=I0_op
      else
         I_ip=I0_up
      endif
C BASE_STORE_HANDLER.fmg( 250, 328):���� RE IN LO CH7
      if(L0_it) then
         I_ar=I0_er
      else
         I_ar=I0_ir
      endif
C BASE_STORE_HANDLER.fmg( 250, 338):���� RE IN LO CH7
      if(L0_ot) then
         I_or=I0_ur
      else
         I_or=I0_as
      endif
C BASE_STORE_HANDLER.fmg( 250, 348):���� RE IN LO CH7
      if(L0_iv) then
         I_ov=I0_uv
      else
         I_ov=I0_ax
      endif
C BASE_STORE_HANDLER.fmg( 250, 358):���� RE IN LO CH7
      L0_uko=I_ex.eq.I_ek
C BASE_STORE_HANDLER.fmg(  98, 362):���������� �������������
      if(L0_uko) then
         I_ate=I0_ete
      else
         I_ate=I0_ite
      endif
C BASE_STORE_HANDLER.fmg( 118, 366):���� RE IN LO CH7
      L0_imo=I_ex.eq.I_ak
C BASE_STORE_HANDLER.fmg( 104, 451):���������� �������������
      if(L0_imo) then
         I_ote=I0_ute
      else
         I_ote=I0_ave
      endif
C BASE_STORE_HANDLER.fmg( 126, 454):���� RE IN LO CH7
      if(L_ake) then
         C20_eke=C20_ike
      else
         C20_eke=C20_oke
      endif
C BASE_STORE_HANDLER.fmg(  39, 376):���� RE IN LO CH20
      if(L0_ofe) then
         C20_uke=C20_ufe
      else
         C20_uke=C20_eke
      endif
C BASE_STORE_HANDLER.fmg(  55, 375):���� RE IN LO CH20
      L0_upo = (.NOT.L_opo).AND.L0_ipo
C BASE_STORE_HANDLER.fmg( 104, 438):�
C label 289  try289=try289-1
      if(L_oro) then
         R0_oti=R0_eti
      else
         R0_oti=R8_obo
      endif
C BASE_STORE_HANDLER.fmg( 373, 460):���� RE IN LO CH7
      if(.NOT.L0_iti) then
         R_ado=R8_obo
      endif
C BASE_STORE_HANDLER.fmg( 384, 470):���� � ������������� �������
      L0_umo = L_opo.OR.L0_oko.OR.L0_amo
C BASE_STORE_HANDLER.fmg( 104, 432):���
      L_apo=(L0_upo.or.L_apo).and..not.(L0_umo)
      L0_epo=.not.L_apo
C BASE_STORE_HANDLER.fmg( 111, 436):RS �������,1
      L0_omo = (.NOT.L0_imo).AND.L_apo
C BASE_STORE_HANDLER.fmg( 130, 440):�
      L0_usi = L0_omo.AND.(.NOT.L_uro)
C BASE_STORE_HANDLER.fmg( 252, 460):�
      L0_iko = L0_usi.OR.L_aso
C BASE_STORE_HANDLER.fmg( 265, 459):���
      if(L0_iko) then
         R0_ifo=R0_afo
      else
         R0_ifo=R0_ufo
      endif
C BASE_STORE_HANDLER.fmg( 287, 472):���� RE IN LO CH7
      L0_elo = L0_oko.OR.L_ulo.OR.L0_ipo
C BASE_STORE_HANDLER.fmg( 104, 388):���
      L0_emo = L0_amo.AND.(.NOT.L_ulo)
C BASE_STORE_HANDLER.fmg( 104, 394):�
      L_ilo=(L0_emo.or.L_ilo).and..not.(L0_elo)
      L0_olo=.not.L_ilo
C BASE_STORE_HANDLER.fmg( 111, 392):RS �������,2
      L0_alo = L_ilo.AND.(.NOT.L0_uko)
C BASE_STORE_HANDLER.fmg( 130, 394):�
      L0_osi = L0_alo.AND.(.NOT.L_aso)
C BASE_STORE_HANDLER.fmg( 252, 446):�
      L0_eko = L0_osi.OR.L_uro
C BASE_STORE_HANDLER.fmg( 265, 445):���
      if(L0_eko) then
         R0_efo=R0_udo
      else
         R0_efo=R0_ufo
      endif
C BASE_STORE_HANDLER.fmg( 287, 454):���� RE IN LO CH7
      R0_ofo = R0_ifo + (-R0_efo)
C BASE_STORE_HANDLER.fmg( 293, 464):��������
      if(L_ero) then
         R0_ibo=R0_ati
      else
         R0_ibo=R0_ofo
      endif
C BASE_STORE_HANDLER.fmg( 300, 462):���� RE IN LO CH7
      R0_oxi = R_ado + (-R_aro)
C BASE_STORE_HANDLER.fmg( 308, 453):��������
      R0_axi = R0_ibo + R0_oxi
C BASE_STORE_HANDLER.fmg( 314, 454):��������
      R0_ixi = R0_axi * R0_oxi
C BASE_STORE_HANDLER.fmg( 318, 453):����������
      L0_uxi=R0_ixi.lt.R0_exi
C BASE_STORE_HANDLER.fmg( 323, 452):���������� <
      L_abo=(L0_uxi.or.L_abo).and..not.(.NOT.L_eso)
      L0_uvi=.not.L_abo
C BASE_STORE_HANDLER.fmg( 330, 450):RS �������,6
      if(L_abo) then
         R_ado=R_aro
      endif
C BASE_STORE_HANDLER.fmg( 324, 470):���� � ������������� �������
      if(L_abo) then
         R0_ubo=R0_ebo
      else
         R0_ubo=R0_ibo
      endif
C BASE_STORE_HANDLER.fmg( 332, 462):���� RE IN LO CH7
      R0_edo = R_ado + R0_ubo
C BASE_STORE_HANDLER.fmg( 338, 463):��������
      R0_evi=MAX(R0_avi,R0_edo)
C BASE_STORE_HANDLER.fmg( 346, 464):��������
      R0_odo=MIN(R0_uti,R0_evi)
C BASE_STORE_HANDLER.fmg( 354, 465):�������
      L_opo=R0_odo.gt.R0_ovi
C BASE_STORE_HANDLER.fmg( 363, 443):���������� >
C sav1=L0_umo
      L0_umo = L_opo.OR.L0_oko.OR.L0_amo
C BASE_STORE_HANDLER.fmg( 104, 432):recalc:���
C if(sav1.ne.L0_umo .and. try301.gt.0) goto 301
C sav1=L0_upo
      L0_upo = (.NOT.L_opo).AND.L0_ipo
C BASE_STORE_HANDLER.fmg( 104, 438):recalc:�
C if(sav1.ne.L0_upo .and. try289.gt.0) goto 289
      if(L_opo) then
         C20_ude=C20_afe
      else
         C20_ude=C20_efe
      endif
C BASE_STORE_HANDLER.fmg(  33, 364):���� RE IN LO CH20
      if(L_opo) then
         I0_ape=I0_epe
      else
         I0_ape=I0_ipe
      endif
C BASE_STORE_HANDLER.fmg( 162, 411):���� RE IN LO CH7
      if(L0_alo) then
         I0_ove=I0_ame
      else
         I0_ove=I0_ape
      endif
C BASE_STORE_HANDLER.fmg( 173, 410):���� RE IN LO CH7
      if(L_uri) then
         I_eve=I0_ive
      else
         I_eve=I0_ove
      endif
C BASE_STORE_HANDLER.fmg( 192, 409):���� RE IN LO CH7
      L0_ufi = L0_ofi.AND.L_opo
C BASE_STORE_HANDLER.fmg(  60, 396):�
      L0_adi = L0_ofi.AND.(.NOT.L0_ufi)
C BASE_STORE_HANDLER.fmg(  62, 416):�
      if(L0_iti) then
         R8_obo=R0_oti
      else
         R8_obo=R0_odo
      endif
C BASE_STORE_HANDLER.fmg( 377, 464):���� RE IN LO CH7
      R_use = R0_ose * R8_obo
C BASE_STORE_HANDLER.fmg( 368, 484):����������
      R_ise = R0_ese + (-R_use)
C BASE_STORE_HANDLER.fmg( 382, 490):��������
      L_ulo=R0_odo.lt.R0_ivi
C BASE_STORE_HANDLER.fmg( 363, 428):���������� <
C sav1=L0_elo
      L0_elo = L0_oko.OR.L_ulo.OR.L0_ipo
C BASE_STORE_HANDLER.fmg( 104, 388):recalc:���
C if(sav1.ne.L0_elo .and. try320.gt.0) goto 320
C sav1=L0_emo
      L0_emo = L0_amo.AND.(.NOT.L_ulo)
C BASE_STORE_HANDLER.fmg( 104, 394):recalc:�
C if(sav1.ne.L0_emo .and. try322.gt.0) goto 322
      if(L_ulo) then
         C20_ife=C20_ode
      else
         C20_ife=C20_ude
      endif
C BASE_STORE_HANDLER.fmg(  49, 364):���� RE IN LO CH20
      if(L_ulo) then
         I0_ime=I0_ome
      else
         I0_ime=I0_ume
      endif
C BASE_STORE_HANDLER.fmg( 156, 460):���� RE IN LO CH7
      if(L0_omo) then
         I0_isi=I0_eme
      else
         I0_isi=I0_ime
      endif
C BASE_STORE_HANDLER.fmg( 168, 458):���� RE IN LO CH7
      if(L_uri) then
         I_asi=I0_esi
      else
         I_asi=I0_isi
      endif
C BASE_STORE_HANDLER.fmg( 186, 458):���� RE IN LO CH7
      L0_eki = L0_aki.AND.L_ulo
C BASE_STORE_HANDLER.fmg(  60, 438):�
      L0_efi = L0_aki.AND.(.NOT.L0_eki)
C BASE_STORE_HANDLER.fmg(  62, 420):�
      if(L0_iti) then
         R_ado=R0_odo
      endif
C BASE_STORE_HANDLER.fmg( 366, 470):���� � ������������� �������
C sav1=R0_oxi
      R0_oxi = R_ado + (-R_aro)
C BASE_STORE_HANDLER.fmg( 308, 453):recalc:��������
C if(sav1.ne.R0_oxi .and. try346.gt.0) goto 346
C sav1=R0_edo
      R0_edo = R_ado + R0_ubo
C BASE_STORE_HANDLER.fmg( 338, 463):recalc:��������
C if(sav1.ne.R0_edo .and. try363.gt.0) goto 363
      if(L0_alo) then
         I_ide=I0_ade
      else
         I_ide=I0_ede
      endif
C BASE_STORE_HANDLER.fmg( 196, 381):���� RE IN LO CH7
      L0_omi = L0_omo.OR.L0_alo
C BASE_STORE_HANDLER.fmg( 251, 392):���
      L0_umi = L0_omi.AND.(.NOT.L_ami)
C BASE_STORE_HANDLER.fmg( 266, 391):�
      if(L0_umi) then
         R0_opi=R_epi
      else
         R0_opi=R0_api
      endif
C BASE_STORE_HANDLER.fmg( 269, 398):���� RE IN LO CH7
      if(L0_omo) then
         I_ube=I0_ibe
      else
         I_ube=I0_obe
      endif
C BASE_STORE_HANDLER.fmg( 188, 431):���� RE IN LO CH7
      R0_ipi = R8_upi
C BASE_STORE_HANDLER.fmg( 264, 408):��������
C label 453  try453=try453-1
      R8_upi = R0_opi + R0_ipi
C BASE_STORE_HANDLER.fmg( 275, 407):��������
C sav1=R0_ipi
      R0_ipi = R8_upi
C BASE_STORE_HANDLER.fmg( 264, 408):recalc:��������
C if(sav1.ne.R0_ipi .and. try453.gt.0) goto 453
      End
