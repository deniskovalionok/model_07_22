      Interface
      Subroutine PUMP2_HANDLER(ext_deltat,I_o,I_u,R_id,R_od
     &,R_ud,R_af,L_al,L_el,L_il,L_ol,L_ul,L_am,L_em,L_im,L_om
     &,L_um,L_ap,L_ep,L_ip,L_op,L_up,L_ir,L_as,L_es,L_is,I_et
     &,R_av,R_iv,L_ov,L_ex,L_ix,R8_ebe,L_ibe,R8_obe,R_afe
     &,R_ofe,R_ike,R8_oke,R_ale,R8_ele,R_ule,R8_ame,R_ime
     &,R_ome)
C |I_o           |2 4 O|LOFF            |����� ��������||
C |I_u           |2 4 O|LON             |����� �������||
C |R_id          |4 4 S|turn_off_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_od          |4 4 I|turn_off_button |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_ud          |4 4 S|turn_on_button_ST*|��������� ������ "������� �������� �� ���������" |0.0|
C |R_af          |4 4 I|turn_on_button  |������� ������ ������ "������� �������� �� ���������" |0.0|
C |L_al          |1 1 O|block           |||
C |L_el          |1 1 O|fault           |�������������||
C |L_il          |1 1 S|_qff156*        |�������� ������ Q RS-��������  |F|
C |L_ol          |1 1 S|_qff155*        |�������� ������ Q RS-��������  |F|
C |L_ul          |1 1 O|norm            |�����||
C |L_am          |1 1 I|gm14            |������������� ������� ���������||
C |L_em          |1 1 I|gm12            |������������� ������� ��������||
C |L_im          |1 1 I|gm13            |���������������� ����������||
C |L_om          |1 1 I|gm11            |���������������� ���������||
C |L_um          |1 1 O|cb_out          |��������� �������� ����.||
C |L_ap          |1 1 I|uluboff         |���������� ���������� �� ���������|F|
C |L_ep          |1 1 O|turn_off_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ip          |1 1 I|ulubon          |���������� ��������� �� ���������|F|
C |L_op          |1 1 O|turn_on_button_CMD*|[TF]����� ������ ������� �������� �� ���������|F|
C |L_up          |1 1 S|_qffJ1545*      |�������� ������ Q RS-��������  |F|
C |L_ir          |1 1 S|_qffJ1541*      |�������� ������ Q RS-��������  |F|
C |L_as          |1 1 I|uluoff          |������� ��������� �� ����������|F|
C |L_es          |1 1 I|uluon           |������� �������� �� ����������|F|
C |L_is          |1 1 O|lstate          |��������� ����������� ���������||
C |I_et          |2 4 O|LBM             |||
C |R_av          |4 4 K|_tdel1          |[]�������� ������� �������� ������|2|
C |R_iv          |4 4 S|_sdel1*         |[���]���������� ��������� �������� ������ |0.0|
C |L_ov          |1 1 S|_idel1*         |[TF]���������� ��������� �������� ������ |f|
C |L_ex          |1 1 I|block_ele       |���������� ������������� �������||
C |L_ix          |1 1 S|_qff3*          |�������� ������ Q RS-��������  |F|
C |R8_ebe        |4 8 I|voltage         |[��]���������� �� ������||
C |L_ibe         |1 1 I|el_use          |����� ������||
C |R8_obe        |4 8 I|estate          |������������� �������� EN_CAD|0.0|
C |R_afe         |4 4 O|_oapr5*         |�������� ������ ���������. ����� |0.0|
C |R_ofe         |4 4 I|kratn           |��������� ��������� ����|3.0|
C |R_ike         |4 4 O|idvig           |[�] ��� ��������� |3.0|
C |R8_oke        |4 8 I|pnagr           |[��] �������� �������� ||
C |R_ale         |4 4 O|power           |�������� ���������|3.0|
C |R8_ele        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |R_ule         |4 4 O|_oapr1*         |�������� ������ ���������. ����� |0.0|
C |R8_ame        |4 8 O|fizstate        |������������� ��������|0.0|
C |R_ime         |4 4 I|timeon          |���������� ������� ���������|3.0|
C |R_ome         |4 4 I|timeoff         |���������� ������� ����������|3.0|

      IMPLICIT NONE
      REAL*4 ext_deltat
      INTEGER*4 I_o,I_u
      REAL*4 R_id,R_od,R_ud,R_af
      LOGICAL*1 L_al,L_el,L_il,L_ol,L_ul,L_am,L_em,L_im,L_om
     &,L_um,L_ap,L_ep,L_ip,L_op,L_up,L_ir,L_as,L_es,L_is
      INTEGER*4 I_et
      REAL*4 R_av,R_iv
      LOGICAL*1 L_ov,L_ex,L_ix
      REAL*8 R8_ebe
      LOGICAL*1 L_ibe
      REAL*8 R8_obe
      REAL*4 R_afe,R_ofe,R_ike
      REAL*8 R8_oke
      REAL*4 R_ale
      REAL*8 R8_ele
      REAL*4 R_ule
      REAL*8 R8_ame
      REAL*4 R_ime,R_ome
      End subroutine PUMP2_HANDLER
      End interface
