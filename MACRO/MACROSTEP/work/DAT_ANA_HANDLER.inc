      Interface
      Subroutine DAT_ANA_HANDLER(ext_deltat,R_e,R_i,R_ed,R_el
     &,R_ol,R_em,R_om,I_op,R_es,L_is,R_us,L_at,L_et,R_ot,R_ix
     &,R_ux,L_ebe,R_ibe)
C |R_e           |4 4 O|nom             |||
C |R_i           |4 4 K|_cJ1450         |�������� ��������� ����������|0.3|
C |R_ed          |4 4 I|koeff_units     |����������� �����������||
C |R_el          |4 4 I|LOWER_warning_setpoint|||
C |R_ol          |4 4 I|LOWER_alarm_setpoint|||
C |R_em          |4 4 I|UPPER_alarm_setpoint|||
C |R_om          |4 4 I|UPPER_warning_setpoint|||
C |I_op          |2 4 O|dat_color       |���� ��������||
C |R_es          |4 4 I|GM05V           |���������� ����������� �������||
C |L_is          |1 1 I|GM05            |���������� ����������� �������||
C |R_us          |4 4 I|GM04V           |������ �������� ���������� ��������||
C |L_at          |1 1 I|GM04            |������ �������� ���������� ��������||
C |L_et          |1 1 I|GM02            |����� �� ������������ ������ ���������||
C |R_ot          |4 4 O|KKS             |�����||
C |R_ix          |4 4 I|MAXIMUM         |||
C |R_ux          |4 4 I|MINIMUM         |||
C |L_ebe         |1 1 I|GM01            |����� �� ����������� ������ ���������||
C |R_ibe         |4 4 I|input           |����||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_e,R_i,R_ed,R_el,R_ol,R_em,R_om
      INTEGER*4 I_op
      REAL*4 R_es
      LOGICAL*1 L_is
      REAL*4 R_us
      LOGICAL*1 L_at,L_et
      REAL*4 R_ot,R_ix,R_ux
      LOGICAL*1 L_ebe
      REAL*4 R_ibe
      End subroutine DAT_ANA_HANDLER
      End interface
