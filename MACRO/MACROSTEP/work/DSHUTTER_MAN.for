      Subroutine DSHUTTER_MAN(ext_deltat,R_eli,L_uki,L_afi
     &,R8_ute,C30_ed,L_af,L_uf,L_ak,I_uk,I_al,R_ul,R_em,R_im
     &,R_om,R_um,R_ap,R_ep,R_ip,I_op,I_er,I_ur,I_os,L_us,L_et
     &,L_it,L_ot,L_ut,L_ev,L_iv,L_ex,L_ix,L_abe,L_ebe,L_obe
     &,L_ube,L_ede,R8_ode,R_ife,R8_ake,L_eke,L_oke,L_ete,R_eve
     &,R_ove,L_odi,L_eki,L_ili,L_oli,L_uli,L_ami,L_emi,L_imi
     &)
C |R_eli         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |L_uki         |1 1 O|13 cbo          |�� ������� (���)|F|
C |L_afi         |1 1 O|14 cbc          |�� ������� (���)|F|
C |R8_ute        |4 8 O|16 value        |��� �������� ��������|0.5|
C |C30_ed        |3 30 O|state           |||
C |L_af          |1 1 I|tech_mode       |||
C |L_uf          |1 1 I|ruch_mode       |||
C |L_ak          |1 1 I|avt_mode        |||
C |I_uk          |2 4 O|LM              |�����||
C |I_al          |2 4 O|LF              |����� �������������||
C |R_ul          |4 4 O|POS_CL          |��������||
C |R_em          |4 4 O|POS_OP          |��������||
C |R_im          |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_om          |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_um          |4 4 S|vmclose_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ap          |4 4 I|vmclose_button  |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_ep          |4 4 S|vmopen_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ip          |4 4 I|vmopen_button   |������� ������ ������ "������� ������� �� ���������" |0.0|
C |I_op          |2 4 O|LST             |����� ����||
C |I_er          |2 4 O|LCL             |����� �������||
C |I_ur          |2 4 O|LOP             |����� �������||
C |I_os          |2 4 O|LS              |��������� �������||
C |L_us          |1 1 I|vlv_kvit        |||
C |L_et          |1 1 I|instr_fault     |||
C |L_it          |1 1 S|_qffJ453*       |�������� ������ Q RS-��������  |F|
C |L_ot          |1 1 O|vmclose_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ut          |1 1 O|vmopen_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ev          |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_iv          |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_ex          |1 1 O|block           |||
C |L_ix          |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_abe         |1 1 O|STOP            |�������||
C |L_ebe         |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_obe         |1 1 O|fault           |�������������||
C |L_ube         |1 1 O|norm            |�����||
C |L_ede         |1 1 O|nopower         |��� ����������||
C |R8_ode        |4 8 I|voltage         |[��]���������� �� ������||
C |R_ife         |4 4 I|power           |�������� ��������||
C |R8_ake        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_eke         |1 1 I|uluclose        |������� ������� �� ����������|F|
C |L_oke         |1 1 I|uluopen         |������� ������� �� ����������|F|
C |L_ete         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_eve         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_ove         |4 4 I|tcl_top         |����� ����|30.0|
C |L_odi         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_eki         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ili         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_oli         |1 1 I|mlf23           |������� ������� �����||
C |L_uli         |1 1 I|mlf22           |����� ����� ��������||
C |L_ami         |1 1 I|mlf04           |�������� �� ��������||
C |L_emi         |1 1 I|mlf03           |�������� �� ��������||
C |L_imi         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      CHARACTER*30 C30_e,C30_i,C30_o,C30_u,C30_ad,C30_ed,C30_id
     &,C30_od,C30_ud
      LOGICAL*1 L_af
      INTEGER*4 I0_ef,I0_if,I0_of
      LOGICAL*1 L_uf,L_ak
      INTEGER*4 I0_ek,I0_ik,I0_ok,I_uk,I_al,I0_el,I0_il
      REAL*4 R0_ol,R_ul,R0_am,R_em,R_im,R_om,R_um,R_ap,R_ep
     &,R_ip
      INTEGER*4 I_op,I0_up,I0_ar,I_er,I0_ir,I0_or,I_ur,I0_as
     &,I0_es,I0_is,I_os
      LOGICAL*1 L_us,L0_at,L_et,L_it,L_ot,L_ut
      REAL*4 R0_av
      LOGICAL*1 L_ev,L_iv,L0_ov,L0_uv,L0_ax,L_ex,L_ix,L0_ox
     &,L0_ux,L_abe,L_ebe,L0_ibe,L_obe,L_ube,L0_ade,L_ede
      REAL*4 R0_ide
      REAL*8 R8_ode
      LOGICAL*1 L0_ude,L0_afe
      REAL*4 R0_efe,R_ife,R0_ofe,R0_ufe
      REAL*8 R8_ake
      LOGICAL*1 L_eke,L0_ike,L_oke,L0_uke
      INTEGER*4 I0_ale,I0_ele,I0_ile,I0_ole,I0_ule,I0_ame
     &,I0_eme,I0_ime,I0_ome
      LOGICAL*1 L0_ume,L0_ape
      REAL*4 R0_epe,R0_ipe
      LOGICAL*1 L0_ope
      REAL*4 R0_upe,R0_are,R0_ere,R0_ire,R0_ore,R0_ure
      LOGICAL*1 L0_ase
      REAL*4 R0_ese,R0_ise,R0_ose,R0_use
      LOGICAL*1 L0_ate,L_ete
      REAL*4 R0_ite,R0_ote
      REAL*8 R8_ute
      REAL*4 R0_ave,R_eve,R0_ive,R_ove,R0_uve,R0_axe,R0_exe
     &,R0_ixe,R0_oxe,R0_uxe,R0_abi,R0_ebi
      LOGICAL*1 L0_ibi,L0_obi,L0_ubi,L0_adi,L0_edi,L0_idi
     &,L_odi,L0_udi,L_afi,L0_efi,L0_ifi,L0_ofi,L0_ufi,L0_aki
     &,L_eki,L0_iki,L0_oki,L_uki,L0_ali
      REAL*4 R_eli
      LOGICAL*1 L_ili,L_oli,L_uli,L_ami,L_emi,L_imi

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      C30_o = '�����������'
C DSHUTTER_MAN.fmg( 189, 222):��������� ���������� CH20 (CH30) (�������)
      C30_e = '�����������'
C DSHUTTER_MAN.fmg( 207, 221):��������� ���������� CH20 (CH30) (�������)
      C30_u = '������'
C DSHUTTER_MAN.fmg( 152, 224):��������� ���������� CH20 (CH30) (�������)
      C30_ad = ''
C DSHUTTER_MAN.fmg( 152, 226):��������� ���������� CH20 (CH30) (�������)
      C30_od = '������'
C DSHUTTER_MAN.fmg( 172, 223):��������� ���������� CH20 (CH30) (�������)
      I0_ef = z'01000010'
C DSHUTTER_MAN.fmg( 201, 160):��������� ������������� IN (�������)
      I0_ik = z'01000022'
C DSHUTTER_MAN.fmg( 163, 162):��������� ������������� IN (�������)
      I0_of = z'0100000A'
C DSHUTTER_MAN.fmg( 185, 161):��������� ������������� IN (�������)
      I0_ok = z'0100000A'
C DSHUTTER_MAN.fmg( 163, 164):��������� ������������� IN (�������)
      if(L_ak) then
         I0_ek=I0_ik
      else
         I0_ek=I0_ok
      endif
C DSHUTTER_MAN.fmg( 170, 162):���� RE IN LO CH7
      if(L_uf) then
         I0_if=I0_of
      else
         I0_if=I0_ek
      endif
C DSHUTTER_MAN.fmg( 188, 162):���� RE IN LO CH7
      if(L_af) then
         I_uk=I0_ef
      else
         I_uk=I0_if
      endif
C DSHUTTER_MAN.fmg( 204, 160):���� RE IN LO CH7
      I0_el = z'01000007'
C DSHUTTER_MAN.fmg( 148, 180):��������� ������������� IN (�������)
      I0_il = z'01000003'
C DSHUTTER_MAN.fmg( 148, 182):��������� ������������� IN (�������)
      R0_ol = 100
C DSHUTTER_MAN.fmg( 378, 273):��������� (RE4) (�������)
      R0_am = 100
C DSHUTTER_MAN.fmg( 365, 267):��������� (RE4) (�������)
      L_ix=R_om.ne.R_im
      R_im=R_om
C DSHUTTER_MAN.fmg(  22, 208):���������� ������������� ������
      L_ot=R_ap.ne.R_um
      R_um=R_ap
C DSHUTTER_MAN.fmg(  21, 194):���������� ������������� ������
      L0_ike = (.NOT.L_ev).AND.L_ot
C DSHUTTER_MAN.fmg(  65, 194):�
      L0_efi = L0_ike.OR.L_eke
C DSHUTTER_MAN.fmg(  69, 192):���
      L_ut=R_ip.ne.R_ep
      R_ep=R_ip
C DSHUTTER_MAN.fmg(  22, 238):���������� ������������� ������
      L0_uke = L_ut.AND.(.NOT.L_iv)
C DSHUTTER_MAN.fmg(  65, 236):�
      L0_oki = L0_uke.OR.L_oke
C DSHUTTER_MAN.fmg(  69, 234):���
      L0_ox = L0_oki.OR.L0_efi
C DSHUTTER_MAN.fmg(  74, 203):���
      L_ebe=(L_ix.or.L_ebe).and..not.(L0_ox)
      L0_ux=.not.L_ebe
C DSHUTTER_MAN.fmg( 104, 205):RS �������,10
      L_abe=L_ebe
C DSHUTTER_MAN.fmg( 132, 207):������,STOP
      L0_ubi = L_ebe.OR.L_ili
C DSHUTTER_MAN.fmg(  96, 214):���
      I0_up = z'01000007'
C DSHUTTER_MAN.fmg( 112, 214):��������� ������������� IN (�������)
      I0_ar = z'01000003'
C DSHUTTER_MAN.fmg( 112, 216):��������� ������������� IN (�������)
      if(L_ebe) then
         I_op=I0_up
      else
         I_op=I0_ar
      endif
C DSHUTTER_MAN.fmg( 116, 214):���� RE IN LO CH7
      I0_ir = z'01000010'
C DSHUTTER_MAN.fmg( 114, 169):��������� ������������� IN (�������)
      I0_or = z'01000003'
C DSHUTTER_MAN.fmg( 114, 171):��������� ������������� IN (�������)
      I0_es = z'01000003'
C DSHUTTER_MAN.fmg( 120, 265):��������� ������������� IN (�������)
      I0_as = z'0100000A'
C DSHUTTER_MAN.fmg( 120, 263):��������� ������������� IN (�������)
      I0_is = z'01000097'
C DSHUTTER_MAN.fmg( 188, 200):��������� ������������� IN (�������)
      I0_ame = z'0100000A'
C DSHUTTER_MAN.fmg( 152, 264):��������� ������������� IN (�������)
      I0_ule = z'01000010'
C DSHUTTER_MAN.fmg( 152, 262):��������� ������������� IN (�������)
      I0_ale = z'0100000A'
C DSHUTTER_MAN.fmg( 152, 207):��������� ������������� IN (�������)
      I0_ime = z'01000087'
C DSHUTTER_MAN.fmg( 166, 256):��������� ������������� IN (�������)
      I0_ile = z'01000086'
C DSHUTTER_MAN.fmg( 166, 201):��������� ������������� IN (�������)
      L_it=(L_et.or.L_it).and..not.(L_us)
      L0_at=.not.L_it
C DSHUTTER_MAN.fmg( 326, 178):RS �������
      L0_ax=.false.
C DSHUTTER_MAN.fmg(  63, 217):��������� ���������� (�������)
      L0_uv=.false.
C DSHUTTER_MAN.fmg(  63, 215):��������� ���������� (�������)
      L0_ov=.false.
C DSHUTTER_MAN.fmg(  63, 213):��������� ���������� (�������)
      L_ex = L0_ax.OR.L0_uv.OR.L0_ov.OR.L_iv.OR.L_ev
C DSHUTTER_MAN.fmg(  67, 213):���
      R0_av = DeltaT
C DSHUTTER_MAN.fmg( 250, 254):��������� (RE4) (�������)
      if(R_ove.ge.0.0) then
         R0_exe=R0_av/max(R_ove,1.0e-10)
      else
         R0_exe=R0_av/min(R_ove,-1.0e-10)
      endif
C DSHUTTER_MAN.fmg( 259, 252):�������� ����������
      L0_ade =.NOT.(L_emi.OR.L_ami.OR.L_imi.OR.L_uli.OR.L_oli.OR.L_ili
     &)
C DSHUTTER_MAN.fmg( 319, 191):���
      L0_ibe =.NOT.(L0_ade)
C DSHUTTER_MAN.fmg( 328, 185):���
      L_obe = L0_ibe.OR.L_it
C DSHUTTER_MAN.fmg( 332, 184):���
      if(L_obe) then
         I_al=I0_el
      else
         I_al=I0_il
      endif
C DSHUTTER_MAN.fmg( 152, 181):���� RE IN LO CH7
      L_ube = L0_ade.OR.L_et
C DSHUTTER_MAN.fmg( 328, 190):���
      R0_ide = 0.1
C DSHUTTER_MAN.fmg( 254, 160):��������� (RE4) (�������)
      L_ede=R8_ode.lt.R0_ide
C DSHUTTER_MAN.fmg( 259, 162):���������� <
      R0_efe = 0.0
C DSHUTTER_MAN.fmg( 266, 182):��������� (RE4) (�������)
      R0_ore = 0.000001
C DSHUTTER_MAN.fmg( 354, 208):��������� (RE4) (�������)
      R0_ure = 0.999999
C DSHUTTER_MAN.fmg( 354, 224):��������� (RE4) (�������)
      R0_epe = 0.0
C DSHUTTER_MAN.fmg( 296, 244):��������� (RE4) (�������)
      R0_ipe = 0.0
C DSHUTTER_MAN.fmg( 370, 242):��������� (RE4) (�������)
      L0_ope = L_oli.OR.L_uli
C DSHUTTER_MAN.fmg( 363, 237):���
      R0_are = 1.0
C DSHUTTER_MAN.fmg( 348, 252):��������� (RE4) (�������)
      R0_ere = 0.0
C DSHUTTER_MAN.fmg( 340, 252):��������� (RE4) (�������)
      R0_ise = 1.0e-10
C DSHUTTER_MAN.fmg( 318, 228):��������� (RE4) (�������)
      R0_ite = 0.0
C DSHUTTER_MAN.fmg( 328, 242):��������� (RE4) (�������)
      R0_uve = DeltaT
C DSHUTTER_MAN.fmg( 250, 264):��������� (RE4) (�������)
      if(R_ove.ge.0.0) then
         R0_ixe=R0_uve/max(R_ove,1.0e-10)
      else
         R0_ixe=R0_uve/min(R_ove,-1.0e-10)
      endif
C DSHUTTER_MAN.fmg( 259, 262):�������� ����������
      R0_ebi = 0.0
C DSHUTTER_MAN.fmg( 282, 244):��������� (RE4) (�������)
      L0_adi = (.NOT.L_abe).AND.L_afi
C DSHUTTER_MAN.fmg( 102, 166):�
C label 137  try137=try137-1
      if(L_uli) then
         R0_upe=R0_ipe
      else
         R0_upe=R8_ute
      endif
C DSHUTTER_MAN.fmg( 373, 242):���� RE IN LO CH7
      if(.NOT.L0_ope) then
         R_eve=R8_ute
      endif
C DSHUTTER_MAN.fmg( 384, 252):���� � ������������� �������
      L0_ofi = (.NOT.L_abe).AND.L_uki
C DSHUTTER_MAN.fmg( 104, 260):�
      L0_aki = L_uki.OR.L0_ubi.OR.L0_efi
C DSHUTTER_MAN.fmg( 102, 229):���
      L0_ali = (.NOT.L_uki).AND.L0_oki
C DSHUTTER_MAN.fmg( 102, 235):�
      L_eki=(L0_ali.or.L_eki).and..not.(L0_aki)
      L0_iki=.not.L_eki
C DSHUTTER_MAN.fmg( 110, 233):RS �������,1
      L0_ufi = (.NOT.L0_ofi).AND.L_eki
C DSHUTTER_MAN.fmg( 128, 236):�
      L0_ape = L0_ufi.AND.(.NOT.L_ami)
C DSHUTTER_MAN.fmg( 252, 242):�
      L0_obi = L0_ape.OR.L_emi
C DSHUTTER_MAN.fmg( 265, 241):���
      if(L0_obi) then
         R0_uxe=R0_ixe
      else
         R0_uxe=R0_ebi
      endif
C DSHUTTER_MAN.fmg( 287, 254):���� RE IN LO CH7
      L0_idi = L0_ubi.OR.L_afi.OR.L0_oki
C DSHUTTER_MAN.fmg( 102, 185):���
      L0_ifi = L0_efi.AND.(.NOT.L_afi)
C DSHUTTER_MAN.fmg( 102, 191):�
      L_odi=(L0_ifi.or.L_odi).and..not.(L0_idi)
      L0_udi=.not.L_odi
C DSHUTTER_MAN.fmg( 110, 189):RS �������,2
      L0_edi = L_odi.AND.(.NOT.L0_adi)
C DSHUTTER_MAN.fmg( 128, 190):�
      L0_ume = L0_edi.AND.(.NOT.L_emi)
C DSHUTTER_MAN.fmg( 252, 228):�
      L0_ibi = L0_ume.OR.L_ami
C DSHUTTER_MAN.fmg( 265, 227):���
      if(L0_ibi) then
         R0_oxe=R0_exe
      else
         R0_oxe=R0_ebi
      endif
C DSHUTTER_MAN.fmg( 287, 236):���� RE IN LO CH7
      R0_abi = R0_uxe + (-R0_oxe)
C DSHUTTER_MAN.fmg( 293, 246):��������
      if(L_ili) then
         R0_ote=R0_epe
      else
         R0_ote=R0_abi
      endif
C DSHUTTER_MAN.fmg( 300, 244):���� RE IN LO CH7
      R0_use = R_eve + (-R_eli)
C DSHUTTER_MAN.fmg( 308, 235):��������
      R0_ese = R0_ote + R0_use
C DSHUTTER_MAN.fmg( 314, 236):��������
      R0_ose = R0_ese * R0_use
C DSHUTTER_MAN.fmg( 318, 235):����������
      L0_ate=R0_ose.lt.R0_ise
C DSHUTTER_MAN.fmg( 323, 234):���������� <
      L_ete=(L0_ate.or.L_ete).and..not.(.NOT.L_imi)
      L0_ase=.not.L_ete
C DSHUTTER_MAN.fmg( 330, 232):RS �������,6
      if(L_ete) then
         R_eve=R_eli
      endif
C DSHUTTER_MAN.fmg( 324, 252):���� � ������������� �������
      if(L_ete) then
         R0_ave=R0_ite
      else
         R0_ave=R0_ote
      endif
C DSHUTTER_MAN.fmg( 332, 244):���� RE IN LO CH7
      R0_ive = R_eve + R0_ave
C DSHUTTER_MAN.fmg( 338, 245):��������
      R0_ire=MAX(R0_ere,R0_ive)
C DSHUTTER_MAN.fmg( 346, 246):��������
      R0_axe=MIN(R0_are,R0_ire)
C DSHUTTER_MAN.fmg( 354, 247):�������
      L_afi=R0_axe.lt.R0_ore
C DSHUTTER_MAN.fmg( 363, 210):���������� <
C sav1=L0_idi
      L0_idi = L0_ubi.OR.L_afi.OR.L0_oki
C DSHUTTER_MAN.fmg( 102, 185):recalc:���
C if(sav1.ne.L0_idi .and. try178.gt.0) goto 178
C sav1=L0_ifi
      L0_ifi = L0_efi.AND.(.NOT.L_afi)
C DSHUTTER_MAN.fmg( 102, 191):recalc:�
C if(sav1.ne.L0_ifi .and. try180.gt.0) goto 180
C sav1=L0_adi
      L0_adi = (.NOT.L_abe).AND.L_afi
C DSHUTTER_MAN.fmg( 102, 166):recalc:�
C if(sav1.ne.L0_adi .and. try137.gt.0) goto 137
      if(L0_ope) then
         R8_ute=R0_upe
      else
         R8_ute=R0_axe
      endif
C DSHUTTER_MAN.fmg( 377, 246):���� RE IN LO CH7
      R_em = R0_am * R8_ute
C DSHUTTER_MAN.fmg( 368, 266):����������
      R_ul = R0_ol + (-R_em)
C DSHUTTER_MAN.fmg( 382, 272):��������
      L_uki=R0_axe.gt.R0_ure
C DSHUTTER_MAN.fmg( 363, 225):���������� >
C sav1=L0_aki
      L0_aki = L_uki.OR.L0_ubi.OR.L0_efi
C DSHUTTER_MAN.fmg( 102, 229):recalc:���
C if(sav1.ne.L0_aki .and. try157.gt.0) goto 157
C sav1=L0_ali
      L0_ali = (.NOT.L_uki).AND.L0_oki
C DSHUTTER_MAN.fmg( 102, 235):recalc:�
C if(sav1.ne.L0_ali .and. try159.gt.0) goto 159
C sav1=L0_ofi
      L0_ofi = (.NOT.L_abe).AND.L_uki
C DSHUTTER_MAN.fmg( 104, 260):recalc:�
C if(sav1.ne.L0_ofi .and. try152.gt.0) goto 152
      if(L_uki) then
         C30_ud=C30_u
      else
         C30_ud=C30_ad
      endif
C DSHUTTER_MAN.fmg( 156, 224):���� RE IN LO CH20
      if(L_afi) then
         C30_id=C30_od
      else
         C30_id=C30_ud
      endif
C DSHUTTER_MAN.fmg( 176, 224):���� RE IN LO CH20
      if(L0_ufi) then
         C30_i=C30_o
      else
         C30_i=C30_id
      endif
C DSHUTTER_MAN.fmg( 193, 222):���� RE IN LO CH20
      if(L0_edi) then
         C30_ed=C30_e
      else
         C30_ed=C30_i
      endif
C DSHUTTER_MAN.fmg( 211, 222):���� RE IN LO CH20
      if(L0_ope) then
         R_eve=R0_axe
      endif
C DSHUTTER_MAN.fmg( 366, 252):���� � ������������� �������
C sav1=R0_use
      R0_use = R_eve + (-R_eli)
C DSHUTTER_MAN.fmg( 308, 235):recalc:��������
C if(sav1.ne.R0_use .and. try204.gt.0) goto 204
C sav1=R0_ive
      R0_ive = R_eve + R0_ave
C DSHUTTER_MAN.fmg( 338, 245):recalc:��������
C if(sav1.ne.R0_ive .and. try221.gt.0) goto 221
      L0_ude = L0_ufi.OR.L0_edi
C DSHUTTER_MAN.fmg( 251, 174):���
      L0_afe = L0_ude.AND.(.NOT.L_ede)
C DSHUTTER_MAN.fmg( 266, 173):�
      if(L0_afe) then
         R0_ufe=R_ife
      else
         R0_ufe=R0_efe
      endif
C DSHUTTER_MAN.fmg( 269, 180):���� RE IN LO CH7
      if(L0_ofi) then
         I_ur=I0_as
      else
         I_ur=I0_es
      endif
C DSHUTTER_MAN.fmg( 124, 264):���� RE IN LO CH7
      if(L0_adi) then
         I_er=I0_ir
      else
         I_er=I0_or
      endif
C DSHUTTER_MAN.fmg( 117, 170):���� RE IN LO CH7
      if(L0_adi) then
         I0_ome=I0_ule
      else
         I0_ome=I0_ame
      endif
C DSHUTTER_MAN.fmg( 155, 262):���� RE IN LO CH7
      if(L0_ufi) then
         I0_eme=I0_ime
      else
         I0_eme=I0_ome
      endif
C DSHUTTER_MAN.fmg( 170, 262):���� RE IN LO CH7
      if(L0_ofi) then
         I0_ole=I0_ale
      else
         I0_ole=I0_eme
      endif
C DSHUTTER_MAN.fmg( 155, 208):���� RE IN LO CH7
      if(L0_edi) then
         I0_ele=I0_ile
      else
         I0_ele=I0_ole
      endif
C DSHUTTER_MAN.fmg( 170, 206):���� RE IN LO CH7
      if(L_obe) then
         I_os=I0_is
      else
         I_os=I0_ele
      endif
C DSHUTTER_MAN.fmg( 191, 206):���� RE IN LO CH7
      R0_ofe = R8_ake
C DSHUTTER_MAN.fmg( 264, 190):��������
C label 300  try300=try300-1
      R8_ake = R0_ufe + R0_ofe
C DSHUTTER_MAN.fmg( 275, 189):��������
C sav1=R0_ofe
      R0_ofe = R8_ake
C DSHUTTER_MAN.fmg( 264, 190):recalc:��������
C if(sav1.ne.R0_ofe .and. try300.gt.0) goto 300
      End
