      Interface
      Subroutine K2_P27(ext_deltat,R_ed,R_ud,R_uf,R_ek,R_ox
     &,R_ux,L_ube,L_ufe,L_ake,L_eke,R_ike,R_ale,R_ule,R_ime
     &,R_ume,R_are,R_ire,L_ure,R_ese,R_ise,R_use,R_ite,R_ute
     &,R_ave,R_ove,R_axe,R_exe,R_uxe,R_ibi,R_obi,R_adi)
C |R_ed          |4 4 S|_slpb1*         |���������� ��������� ||
C |R_ud          |4 4 I|xb              |[�] ���� �����������||
C |R_uf          |4 4 O|kzamedl         |����-� ����������||
C |R_ek          |4 4 O|minzone         |���. ������||
C |R_ox          |4 4 O|z20             |����� �������. ��������||
C |R_ux          |4 4 I|taudf           |[���] ���������� ������� ��������|5|
C |L_ube         |1 1 I|intset          |���������� ������ �����������|F|
C |L_ufe         |1 1 O|z12             |������ "������"|F|
C |L_ake         |1 1 O|z11             |������ "������"|F|
C |L_eke         |1 1 I|block           |������ �������� ����������|F|
C |R_ike         |4 4 I|taui            |[���] ���������� �����������|10.0|
C |R_ale         |4 4 I|ti              |[���] ������������ ������������� ��������|0.2|
C |R_ule         |4 4 I|alphap          |[���/%] �-� �������� �����|0.5|
C |R_ime         |4 4 I|zone            |[%] ���� ������������������ ��������|1.0|
C |R_ume         |4 4 O|_oapr1*         |�������� ������ ���������. ����� |0.0|
C |R_are         |4 4 O|delta           |[�] �������� ������� ������|0|
C |R_ire         |4 4 S|_oint1*         |����� ����������� |0.0|
C |L_ure         |1 1 I|difset          |���������� ���������������|T|
C |R_ese         |4 4 S|_sdif1*         |���������� ��������� |0.0|
C |R_ise         |4 4 O|_odif1*         |�������� ������ |0.0|
C |R_use         |4 4 I|taud            |[���] ���������� ������� ���-��|5|
C |R_ite         |4 4 I|x41max          |�������� ��������� ������� ��������||
C |R_ute         |4 4 O|epsil           |[%] (17)����� � (0-100%)||
C |R_ave         |4 4 I|setpoint        |[�.�.] (23)������� ������� ���������||
C |R_ove         |4 4 I|alpha4          |�-� �������� �� ����� �4|1|
C |R_axe         |4 4 I|x41             |(20)���� �41 (0-5��)||
C |R_exe         |4 4 I|alpha3          |�-� �������� �� ����� �3|1|
C |R_uxe         |4 4 I|alpha2          |�-� �������� �� ����� �2|1|
C |R_ibi         |4 4 I|x3              |(16)���� �3 (0-5��)||
C |R_obi         |4 4 I|x2              |(12)���� �2 (0-5��)||
C |R_adi         |4 4 I|x1              |(8)���� �1 (0-5��)||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_ed,R_ud,R_uf,R_ek,R_ox,R_ux
      LOGICAL*1 L_ube,L_ufe,L_ake,L_eke
      REAL*4 R_ike,R_ale,R_ule,R_ime,R_ume,R_are,R_ire
      LOGICAL*1 L_ure
      REAL*4 R_ese,R_ise,R_use,R_ite,R_ute,R_ave,R_ove,R_axe
     &,R_exe,R_uxe,R_ibi,R_obi,R_adi
      End subroutine K2_P27
      End interface
