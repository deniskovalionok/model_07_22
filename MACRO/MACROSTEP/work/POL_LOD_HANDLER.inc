      Interface
      Subroutine POL_LOD_HANDLER(ext_deltat,L_u,R_ad,R_ed
     &,L_id,R_od,R_ud,L_af,R_ef,R_if,L_of,L_ek,L_ik,I_il)
C |L_u           |1 1 O|fail_pol_lod_button_CMD*|[TF]����� ������ |F|
C |R_ad          |4 4 S|fail_pol_lod_button_ST*|��������� ������ "" |0.0|
C |R_ed          |4 4 I|fail_pol_lod_button|������� ������ ������ "" |0.0|
C |L_id          |1 1 O|lod_button_CMD* |[TF]����� ������ |F|
C |R_od          |4 4 S|lod_button_ST*  |��������� ������ "" |0.0|
C |R_ud          |4 4 I|lod_button      |������� ������ ������ "" |0.0|
C |L_af          |1 1 O|pod_button_CMD* |[TF]����� ������ |F|
C |R_ef          |4 4 S|pod_button_ST*  |��������� ������ "" |0.0|
C |R_if          |4 4 I|pod_button      |������� ������ ������ "" |0.0|
C |L_of          |1 1 I|lod             |������� �������||
C |L_ek          |1 1 I|fail_pol_lod    |���������� �������� � �������||
C |L_ik          |1 1 I|pod             |������� ��������||
C |I_il          |2 4 O|LS              |��������� �����||

      IMPLICIT NONE
      REAL*4 ext_deltat
      LOGICAL*1 L_u
      REAL*4 R_ad,R_ed
      LOGICAL*1 L_id
      REAL*4 R_od,R_ud
      LOGICAL*1 L_af
      REAL*4 R_ef,R_if
      LOGICAL*1 L_of,L_ek,L_ik
      INTEGER*4 I_il
      End subroutine POL_LOD_HANDLER
      End interface
