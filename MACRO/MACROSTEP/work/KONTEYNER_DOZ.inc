      Interface
      Subroutine KONTEYNER_DOZ(ext_deltat,R_i,R_o,L_u,L_ad
     &,L_ed,L_id,L_od,L_ud,L_af,L_ef,L_if,L_of,L_uf,L_ak,L_ek
     &,L_ik,L_ok,L_uk,L_al,L_el,L_il,L_ol,L_ul,L_at,L_et,L_it
     &,L_ot,L_ut,L_av,L_ev,L_iv,L_ov,L_uv,L_ix,R_ox,R_ux,R_abe
     &,R_ebe,L_obe,L_ade,L_ede,L_ide,R_afe,R_ife,R_ufe,L_ake
     &,R_eke,R_ike,R_oke,L_uke,L_ile,L_ule,R_ime,R_ape,R_ipe
     &,R_upe,R_ere,R_ore,R_ise,R_ate,R_ote,R_ave,R_eve,R_ive
     &,R_ove,R_uve,R_axe,L_exe,L_ixe,L_oxe,L_uxe,L_abi,L_ebi
     &,L_ibi,L_obi,L_ubi,L_adi,R_eri,R_iri,L_ori,R_uri,L_asi
     &,L_osi,R_oti,R_evi,R_ovi,R_uvi,L_axi,R_exi,L_ixi,L_abo
     &,R_ado,R_odo,R_afo,R_efo,L_ifo,R_ofo,L_ufo,L_iko,R_ilo
     &,R_amo,R_imo,R_omo,L_umo,R_apo,L_epo,L_upo,R_uro,R_iso
     &,R_uso,R_ato,L_eto,R_ito,L_oto,L_evo,R_exo,R_uxo,R_ebu
     &,R_ibu,L_obu,R_ubu,L_adu,L_odu,R_ofu,R_eku,R_oku,R_uku
     &,L_alu,R_elu,L_ilu,L_omu,L_umu,R_epu,R_upu,R_iru,R_uru
     &,R_esu,R_isu,R_osu,L_usu,R_atu,L_etu,L_ivu,L_ovu,R_axu
     &,R_oxu,R_ebad,R_obad,R_adad,R_edad,R_idad,L_odad,R_udad
     &,L_afad,L_ekad,L_ikad,R_ukad,R_ilad,R_amad,R_imad,R_umad
     &,R_ipad,R_opad,L_upad,R_arad,L_erad,L_isad,L_osad,R_etad
     &,R_otad,R_ivad,R_uvad,R_exad,R_uxad,R_abed,R_ebed,R_ibed
     &,R_obed)
C |R_i           |4 4 S|_simpftime*     |[���]���������� ��������� ������������� |0.0|
C |R_o           |4 4 K|_timpftime      |[���]������������ �������� �������������|5|
C |L_u           |1 1 S|_limpftime*     |[TF]���������� ��������� ������������� |F|
C |L_ad          |1 1 S|_splsi10-3*     |[TF]���������� ��������� ������������� |F|
C |L_ed          |1 1 S|_splsi10-2*     |[TF]���������� ��������� ������������� |F|
C |L_id          |1 1 S|_splsi9-3*      |[TF]���������� ��������� ������������� |F|
C |L_od          |1 1 S|_splsi9-2*      |[TF]���������� ��������� ������������� |F|
C |L_ud          |1 1 S|_splsi8-3*      |[TF]���������� ��������� ������������� |F|
C |L_af          |1 1 S|_splsi8-2*      |[TF]���������� ��������� ������������� |F|
C |L_ef          |1 1 S|_splsi7-3*      |[TF]���������� ��������� ������������� |F|
C |L_if          |1 1 S|_splsi7-2*      |[TF]���������� ��������� ������������� |F|
C |L_of          |1 1 S|_splsi6-3*      |[TF]���������� ��������� ������������� |F|
C |L_uf          |1 1 S|_splsi6-2*      |[TF]���������� ��������� ������������� |F|
C |L_ak          |1 1 S|_splsi5-3*      |[TF]���������� ��������� ������������� |F|
C |L_ek          |1 1 S|_splsi5-2*      |[TF]���������� ��������� ������������� |F|
C |L_ik          |1 1 S|_splsi4-3*      |[TF]���������� ��������� ������������� |F|
C |L_ok          |1 1 S|_splsi4-2*      |[TF]���������� ��������� ������������� |F|
C |L_uk          |1 1 S|_splsi3-3*      |[TF]���������� ��������� ������������� |F|
C |L_al          |1 1 S|_splsi3-2*      |[TF]���������� ��������� ������������� |F|
C |L_el          |1 1 S|_splsi2-3*      |[TF]���������� ��������� ������������� |F|
C |L_il          |1 1 S|_splsi2-2*      |[TF]���������� ��������� ������������� |F|
C |L_ol          |1 1 S|_splsi1-3*      |[TF]���������� ��������� ������������� |F|
C |L_ul          |1 1 S|_splsi1-2*      |[TF]���������� ��������� ������������� |F|
C |L_at          |1 1 I|FDA20TRAN01_C2_CBC|������ ������||
C |L_et          |1 1 I|FDA20TRAN02_C2_CBC|������ ������||
C |L_it          |1 1 I|FDA20TRAN03_C2_CBC|������ ������||
C |L_ot          |1 1 I|FDA20TRAN04_C2_CBC|������ ������||
C |L_ut          |1 1 I|FDA20TRAN05_C2_CBC|������ ������||
C |L_av          |1 1 I|FDA20TRAN06_C2_CBC|������ ������||
C |L_ev          |1 1 I|FDA20TRAN07_C2_CBC|������ ������||
C |L_iv          |1 1 I|FDA20TRAN08_C2_CBC|������ ������||
C |L_ov          |1 1 I|FDA20TRAN09_C2_CBC|������ ������||
C |L_uv          |1 1 I|FDA20TRAN10_C2_CBC|������ ������||
C |L_ix          |1 1 I|FDA20UNL01      |�������||
C |R_ox          |4 4 K|_utunweight     |����������� ������ ��������� ������|11.0|
C |R_ux          |4 4 K|_ttunweight     |������ ����� ��������� �������|21|
C |R_abe         |4 4 S|_stunweight*    |��������� ����������  ||
C |R_ebe         |4 4 K|_ltunweight     |����������� ������ ��������� �����|7.0|
C |L_obe         |1 1 S|_uc0tunweight*  |��������� ����� ���������� |F|
C |L_ade         |1 1 S|_lc0tunweight*  |��������� ����� ���������� |F|
C |L_ede         |1 1 I|FDA20KANT01_C10_XH53|��������� �� ����������||
C |L_ide         |1 1 I|FDA20KANT01_C11_CBC|��������� �������||
C |R_afe         |4 4 K|_lcmpc11-8      |[]�������� ������ �����������|0.1|
C |R_ife         |4 4 K|_lcmpc11-7      |[]�������� ������ �����������|600|
C |R_ufe         |4 4 K|_lcmpc11-6      |[]�������� ������ �����������|1950|
C |L_ake         |1 1 S|_splsi11-1*     |[TF]���������� ��������� ������������� |F|
C |R_eke         |4 4 I|FDA20KANT01_C1_MFVAL|��������� ����� ����������||
C |R_ike         |4 4 I|FDA20KANT01VZ01 |���������� ����������� �� OZ||
C |R_oke         |4 4 I|FDA20KANT01VX01 |���������� ����������� �� OX||
C |L_uke         |1 1 O|TAKE11          |��������� �������� � �����������||
C |L_ile         |1 1 S|_qfftr11*       |�������� ������ Q RS-��������  |F|
C |L_ule         |1 1 I|FDA20KANT01_C2_CBC|������ ������||
C |R_ime         |4 4 K|_lcmpc11-3      |[]�������� ������ �����������|5|
C |R_ape         |4 4 K|_lcmpc11-1      |[]�������� ������ �����������|5|
C |R_ipe         |4 4 S|_memadd_t4*     |����������� �������� ������� ||
C |R_upe         |4 4 S|_memadd_t3*     |����������� �������� ������� ||
C |R_ere         |4 4 S|_memadd_t1*     |����������� �������� ������� ||
C |R_ore         |4 4 I|START_T         |||
C |R_ise         |4 4 S|_memadd_t2*     |����������� �������� ������� ||
C |R_ate         |4 4 O|R1VT01          |����������� ����������||
C |R_ote         |4 4 S|_memwtng_u*     |����������� �������� ������� ||
C |R_ave         |4 4 S|_memwtng_f*     |����������� �������� ������� ||
C |R_eve         |4 4 S|_memwtng_c*     |����������� �������� ������� ||
C |R_ive         |4 4 O|R1VW01_u        |����� ���������� ����� �������||
C |R_ove         |4 4 O|R1VW01_f        |����� ���������� ����� �������||
C |R_uve         |4 4 O|R1VW01_c        |����� ���������� �������||
C |R_axe         |4 4 O|R1VW01          |����� ����������|`START_W`|
C |L_exe         |1 1 O|INABOX01        |� ����������� �����||
C |L_ixe         |1 1 O|INABOX10        |� ������������� �����||
C |L_oxe         |1 1 O|INABOX09        |� ����� ���������� 3||
C |L_uxe         |1 1 O|INABOX08        |� ����� ���������� 2||
C |L_abi         |1 1 O|INABOX07        |� ����� ���������� 1||
C |L_ebi         |1 1 O|INABOX06        |� ����� ����������||
C |L_ibi         |1 1 O|INABOX05        |� ����� �������� �������||
C |L_obi         |1 1 O|INABOX04        |� ����� �������� ������||
C |L_ubi         |1 1 O|INABOX03        |� ����� �������� ��������||
C |L_adi         |1 1 O|INABOX02        |� ����� �������� �������� ����� ���������||
C |R_eri         |4 4 K|_lcmpc1-0       |[]�������� ������ �����������|20000|
C |R_iri         |4 4 O|VY01            |��������� ���������� �� OY|0|
C |L_ori         |1 1 S|_splsi10-1*     |[TF]���������� ��������� ������������� |F|
C |R_uri         |4 4 I|FDA20TRAN10_C1_MFVAL|��������� �����||
C |L_asi         |1 1 O|TAKE10          |��������� �������� � ����������� 10||
C |L_osi         |1 1 S|_qfftr10*       |�������� ������ Q RS-��������  |F|
C |R_oti         |4 4 K|_lcmpc10-3      |[]�������� ������ �����������|100|
C |R_evi         |4 4 K|_lcmpc10-1      |[]�������� ������ �����������|100|
C |R_ovi         |4 4 I|FDA20TRAN10VZ01 |���������� ������������ �� OZ||
C |R_uvi         |4 4 I|FDA20TRAN10VX01 |���������� ������������ �� OX||
C |L_axi         |1 1 S|_splsi9-1*      |[TF]���������� ��������� ������������� |F|
C |R_exi         |4 4 I|FDA20TRAN09_C1_MFVAL|��������� �����||
C |L_ixi         |1 1 O|TAKE9           |��������� �������� � ����������� 9||
C |L_abo         |1 1 S|_qfftr9*        |�������� ������ Q RS-��������  |F|
C |R_ado         |4 4 K|_lcmpc9-3       |[]�������� ������ �����������|100|
C |R_odo         |4 4 K|_lcmpc9-1       |[]�������� ������ �����������|100|
C |R_afo         |4 4 I|FDA20TRAN09VZ01 |���������� ������������ �� OZ||
C |R_efo         |4 4 I|FDA20TRAN09VX01 |���������� ������������ �� OX||
C |L_ifo         |1 1 S|_splsi8-1*      |[TF]���������� ��������� ������������� |F|
C |R_ofo         |4 4 I|FDA20TRAN08_C1_MFVAL|��������� �����||
C |L_ufo         |1 1 O|TAKE8           |��������� �������� � ����������� 8||
C |L_iko         |1 1 S|_qfftr8*        |�������� ������ Q RS-��������  |F|
C |R_ilo         |4 4 K|_lcmpc8-3       |[]�������� ������ �����������|100|
C |R_amo         |4 4 K|_lcmpc8-1       |[]�������� ������ �����������|100|
C |R_imo         |4 4 I|FDA20TRAN08VZ01 |���������� ������������ �� OZ||
C |R_omo         |4 4 I|FDA20TRAN08VX01 |���������� ������������ �� OX||
C |L_umo         |1 1 S|_splsi7-1*      |[TF]���������� ��������� ������������� |F|
C |R_apo         |4 4 I|FDA20TRAN07_C1_MFVAL|��������� �����||
C |L_epo         |1 1 O|TAKE7           |��������� �������� � ����������� 7||
C |L_upo         |1 1 S|_qfftr7*        |�������� ������ Q RS-��������  |F|
C |R_uro         |4 4 K|_lcmpc7-3       |[]�������� ������ �����������|100|
C |R_iso         |4 4 K|_lcmpc7-1       |[]�������� ������ �����������|100|
C |R_uso         |4 4 I|FDA20TRAN07VZ01 |���������� ������������ �� OZ||
C |R_ato         |4 4 I|FDA20TRAN07VX01 |���������� ������������ �� OX||
C |L_eto         |1 1 S|_splsi6-1*      |[TF]���������� ��������� ������������� |F|
C |R_ito         |4 4 I|FDA20TRAN06_C1_MFVAL|��������� �����||
C |L_oto         |1 1 O|TAKE6           |��������� �������� � ����������� 6||
C |L_evo         |1 1 S|_qfftr6*        |�������� ������ Q RS-��������  |F|
C |R_exo         |4 4 K|_lcmpc6-3       |[]�������� ������ �����������|100|
C |R_uxo         |4 4 K|_lcmpc6-1       |[]�������� ������ �����������|100|
C |R_ebu         |4 4 I|FDA20TRAN06VZ01 |���������� ������������ �� OZ||
C |R_ibu         |4 4 I|FDA20TRAN06VX01 |���������� ������������ �� OX||
C |L_obu         |1 1 S|_splsi5-1*      |[TF]���������� ��������� ������������� |F|
C |R_ubu         |4 4 I|FDA20TRAN05_C1_MFVAL|��������� �����||
C |L_adu         |1 1 O|TAKE5           |��������� �������� � ����������� 5||
C |L_odu         |1 1 S|_qfftr5*        |�������� ������ Q RS-��������  |F|
C |R_ofu         |4 4 K|_lcmpc5-3       |[]�������� ������ �����������|100|
C |R_eku         |4 4 K|_lcmpc5-1       |[]�������� ������ �����������|100|
C |R_oku         |4 4 I|FDA20TRAN05VZ01 |���������� ������������ �� OZ||
C |R_uku         |4 4 I|FDA20TRAN05VX01 |���������� ������������ �� OX||
C |L_alu         |1 1 S|_splsi4-1*      |[TF]���������� ��������� ������������� |F|
C |R_elu         |4 4 I|FDA20TRAN04_C1_MFVAL|��������� �����||
C |L_ilu         |1 1 O|TAKE4           |��������� �������� � ����������� 4||
C |L_omu         |1 1 S|_qff4*          |�������� ������ Q RS-��������  |F|
C |L_umu         |1 1 O|FULL4           |��������� ����� � ������� PU||
C |R_epu         |4 4 K|_lcmpc4-3       |[]�������� ������ �����������|100|
C |R_upu         |4 4 K|_lcmpc4-1       |[]�������� ������ �����������|100|
C |R_iru         |4 4 K|_lcmpc4-8       |[]�������� ������ �����������|0.1|
C |R_uru         |4 4 K|_lcmpc4-7       |[]�������� ������ �����������|480|
C |R_esu         |4 4 K|_lcmpc4-6       |[]�������� ������ �����������|7950|
C |R_isu         |4 4 I|FDA20TRAN04VZ01 |���������� ������������ �� OZ||
C |R_osu         |4 4 I|FDA20TRAN04VX01 |���������� ������������ �� OX||
C |L_usu         |1 1 S|_splsi3-1*      |[TF]���������� ��������� ������������� |F|
C |R_atu         |4 4 I|FDA20TRAN03_C1_MFVAL|��������� �����||
C |L_etu         |1 1 O|TAKE3           |��������� �������� � ����������� 3||
C |L_ivu         |1 1 S|_qfftr3*        |�������� ������ Q RS-��������  |F|
C |L_ovu         |1 1 O|FULL3           |��������� ����� � ������� ������||
C |R_axu         |4 4 K|_lcmpc3-3       |[]�������� ������ �����������|100|
C |R_oxu         |4 4 K|_lcmpc3-1       |[]�������� ������ �����������|100|
C |R_ebad        |4 4 K|_lcmpc3-8       |[]�������� ������ �����������|0.1|
C |R_obad        |4 4 K|_lcmpc3-7       |[]�������� ������ �����������|480|
C |R_adad        |4 4 K|_lcmpc3-6       |[]�������� ������ �����������|5950|
C |R_edad        |4 4 I|FDA20TRAN03VZ01 |���������� ������������ �� OZ||
C |R_idad        |4 4 I|FDA20TRAN03VX01 |���������� ������������ �� OX||
C |L_odad        |1 1 S|_splsi2-1*      |[TF]���������� ��������� ������������� |F|
C |R_udad        |4 4 I|FDA20TRAN02_C1_MFVAL|��������� �����||
C |L_afad        |1 1 O|TAKE2           |��������� �������� � ����������� 2||
C |L_ekad        |1 1 S|_qfftr2*        |�������� ������ Q RS-��������  |F|
C |L_ikad        |1 1 O|FULL2           |��������� ����� � ������� ��������||
C |R_ukad        |4 4 K|_lcmpc2-3       |[]�������� ������ �����������|100|
C |R_ilad        |4 4 K|_lcmpc2-1       |[]�������� ������ �����������|100|
C |R_amad        |4 4 K|_lcmpc2-8       |[]�������� ������ �����������|0.1|
C |R_imad        |4 4 K|_lcmpc2-7       |[]�������� ������ �����������|480|
C |R_umad        |4 4 K|_lcmpc2-6       |[]�������� ������ �����������|3950|
C |R_ipad        |4 4 I|FDA20TRAN02VZ01 |���������� ������������ �� OZ||
C |R_opad        |4 4 I|FDA20TRAN02VX01 |���������� ������������ �� OX||
C |L_upad        |1 1 S|_splsi1-1*      |[TF]���������� ��������� ������������� |F|
C |R_arad        |4 4 I|FDA20TRAN01_C1_MFVAL|��������� �����||
C |L_erad        |1 1 O|TAKE1           |��������� �������� � ����������� 1||
C |L_isad        |1 1 S|_qfftr1*        |�������� ������ Q RS-��������  |F|
C |L_osad        |1 1 O|FULL1           |��������� ����� � ������� �������� �����||
C |R_etad        |4 4 K|_lcmpc1-3       |[]�������� ������ �����������|100|
C |R_otad        |4 4 K|_lcmpc1-1       |[]�������� ������ �����������|100|
C |R_ivad        |4 4 K|_lcmpc1-8       |[]�������� ������ �����������|0.1|
C |R_uvad        |4 4 K|_lcmpc1-7       |[]�������� ������ �����������|480|
C |R_exad        |4 4 K|_lcmpc1-6       |[]�������� ������ �����������|1950|
C |R_uxad        |4 4 O|VV01            |��������� ����� ����������||
C |R_abed        |4 4 I|FDA20TRAN01VZ01 |���������� ������������ �� OZ||
C |R_ebed        |4 4 O|VZ01            |��������� ���������� �� OZ|`START_Z`|
C |R_ibed        |4 4 I|FDA20TRAN01VX01 |���������� ������������ �� OX||
C |R_obed        |4 4 O|VX01            |��������� ���������� �� OX|`START_X`|

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_i,R_o
      LOGICAL*1 L_u,L_ad,L_ed,L_id,L_od,L_ud,L_af,L_ef,L_if
     &,L_of,L_uf,L_ak,L_ek,L_ik,L_ok,L_uk,L_al,L_el,L_il,L_ol
     &,L_ul,L_at
      LOGICAL*1 L_et,L_it,L_ot,L_ut,L_av,L_ev,L_iv,L_ov,L_uv
     &,L_ix
      REAL*4 R_ox,R_ux,R_abe,R_ebe
      LOGICAL*1 L_obe,L_ade,L_ede,L_ide
      REAL*4 R_afe,R_ife,R_ufe
      LOGICAL*1 L_ake
      REAL*4 R_eke,R_ike,R_oke
      LOGICAL*1 L_uke,L_ile,L_ule
      REAL*4 R_ime,R_ape,R_ipe,R_upe,R_ere,R_ore,R_ise,R_ate
     &,R_ote,R_ave,R_eve,R_ive,R_ove,R_uve,R_axe
      LOGICAL*1 L_exe,L_ixe,L_oxe,L_uxe,L_abi,L_ebi,L_ibi
     &,L_obi,L_ubi,L_adi
      REAL*4 R_eri,R_iri
      LOGICAL*1 L_ori
      REAL*4 R_uri
      LOGICAL*1 L_asi,L_osi
      REAL*4 R_oti,R_evi,R_ovi,R_uvi
      LOGICAL*1 L_axi
      REAL*4 R_exi
      LOGICAL*1 L_ixi,L_abo
      REAL*4 R_ado,R_odo,R_afo,R_efo
      LOGICAL*1 L_ifo
      REAL*4 R_ofo
      LOGICAL*1 L_ufo,L_iko
      REAL*4 R_ilo,R_amo,R_imo,R_omo
      LOGICAL*1 L_umo
      REAL*4 R_apo
      LOGICAL*1 L_epo,L_upo
      REAL*4 R_uro,R_iso,R_uso,R_ato
      LOGICAL*1 L_eto
      REAL*4 R_ito
      LOGICAL*1 L_oto,L_evo
      REAL*4 R_exo,R_uxo,R_ebu,R_ibu
      LOGICAL*1 L_obu
      REAL*4 R_ubu
      LOGICAL*1 L_adu,L_odu
      REAL*4 R_ofu,R_eku,R_oku,R_uku
      LOGICAL*1 L_alu
      REAL*4 R_elu
      LOGICAL*1 L_ilu,L_omu,L_umu
      REAL*4 R_epu,R_upu,R_iru,R_uru,R_esu,R_isu,R_osu
      LOGICAL*1 L_usu
      REAL*4 R_atu
      LOGICAL*1 L_etu,L_ivu,L_ovu
      REAL*4 R_axu,R_oxu,R_ebad,R_obad,R_adad,R_edad,R_idad
      LOGICAL*1 L_odad
      REAL*4 R_udad
      LOGICAL*1 L_afad,L_ekad,L_ikad
      REAL*4 R_ukad,R_ilad,R_amad,R_imad,R_umad,R_ipad,R_opad
      LOGICAL*1 L_upad
      REAL*4 R_arad
      LOGICAL*1 L_erad,L_isad,L_osad
      REAL*4 R_etad,R_otad,R_ivad,R_uvad,R_exad,R_uxad,R_abed
     &,R_ebed,R_ibed,R_obed
      End subroutine KONTEYNER_DOZ
      End interface
