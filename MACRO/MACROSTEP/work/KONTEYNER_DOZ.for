      Subroutine KONTEYNER_DOZ(ext_deltat,R_i,R_o,L_u,L_ad
     &,L_ed,L_id,L_od,L_ud,L_af,L_ef,L_if,L_of,L_uf,L_ak,L_ek
     &,L_ik,L_ok,L_uk,L_al,L_el,L_il,L_ol,L_ul,L_at,L_et,L_it
     &,L_ot,L_ut,L_av,L_ev,L_iv,L_ov,L_uv,L_ix,R_ox,R_ux,R_abe
     &,R_ebe,L_obe,L_ade,L_ede,L_ide,R_afe,R_ife,R_ufe,L_ake
     &,R_eke,R_ike,R_oke,L_uke,L_ile,L_ule,R_ime,R_ape,R_ipe
     &,R_upe,R_ere,R_ore,R_ise,R_ate,R_ote,R_ave,R_eve,R_ive
     &,R_ove,R_uve,R_axe,L_exe,L_ixe,L_oxe,L_uxe,L_abi,L_ebi
     &,L_ibi,L_obi,L_ubi,L_adi,R_eri,R_iri,L_ori,R_uri,L_asi
     &,L_osi,R_oti,R_evi,R_ovi,R_uvi,L_axi,R_exi,L_ixi,L_abo
     &,R_ado,R_odo,R_afo,R_efo,L_ifo,R_ofo,L_ufo,L_iko,R_ilo
     &,R_amo,R_imo,R_omo,L_umo,R_apo,L_epo,L_upo,R_uro,R_iso
     &,R_uso,R_ato,L_eto,R_ito,L_oto,L_evo,R_exo,R_uxo,R_ebu
     &,R_ibu,L_obu,R_ubu,L_adu,L_odu,R_ofu,R_eku,R_oku,R_uku
     &,L_alu,R_elu,L_ilu,L_omu,L_umu,R_epu,R_upu,R_iru,R_uru
     &,R_esu,R_isu,R_osu,L_usu,R_atu,L_etu,L_ivu,L_ovu,R_axu
     &,R_oxu,R_ebad,R_obad,R_adad,R_edad,R_idad,L_odad,R_udad
     &,L_afad,L_ekad,L_ikad,R_ukad,R_ilad,R_amad,R_imad,R_umad
     &,R_ipad,R_opad,L_upad,R_arad,L_erad,L_isad,L_osad,R_etad
     &,R_otad,R_ivad,R_uvad,R_exad,R_uxad,R_abed,R_ebed,R_ibed
     &,R_obed)
C |R_i           |4 4 S|_simpftime*     |[���]���������� ��������� ������������� |0.0|
C |R_o           |4 4 K|_timpftime      |[���]������������ �������� �������������|5|
C |L_u           |1 1 S|_limpftime*     |[TF]���������� ��������� ������������� |F|
C |L_ad          |1 1 S|_splsi10-3*     |[TF]���������� ��������� ������������� |F|
C |L_ed          |1 1 S|_splsi10-2*     |[TF]���������� ��������� ������������� |F|
C |L_id          |1 1 S|_splsi9-3*      |[TF]���������� ��������� ������������� |F|
C |L_od          |1 1 S|_splsi9-2*      |[TF]���������� ��������� ������������� |F|
C |L_ud          |1 1 S|_splsi8-3*      |[TF]���������� ��������� ������������� |F|
C |L_af          |1 1 S|_splsi8-2*      |[TF]���������� ��������� ������������� |F|
C |L_ef          |1 1 S|_splsi7-3*      |[TF]���������� ��������� ������������� |F|
C |L_if          |1 1 S|_splsi7-2*      |[TF]���������� ��������� ������������� |F|
C |L_of          |1 1 S|_splsi6-3*      |[TF]���������� ��������� ������������� |F|
C |L_uf          |1 1 S|_splsi6-2*      |[TF]���������� ��������� ������������� |F|
C |L_ak          |1 1 S|_splsi5-3*      |[TF]���������� ��������� ������������� |F|
C |L_ek          |1 1 S|_splsi5-2*      |[TF]���������� ��������� ������������� |F|
C |L_ik          |1 1 S|_splsi4-3*      |[TF]���������� ��������� ������������� |F|
C |L_ok          |1 1 S|_splsi4-2*      |[TF]���������� ��������� ������������� |F|
C |L_uk          |1 1 S|_splsi3-3*      |[TF]���������� ��������� ������������� |F|
C |L_al          |1 1 S|_splsi3-2*      |[TF]���������� ��������� ������������� |F|
C |L_el          |1 1 S|_splsi2-3*      |[TF]���������� ��������� ������������� |F|
C |L_il          |1 1 S|_splsi2-2*      |[TF]���������� ��������� ������������� |F|
C |L_ol          |1 1 S|_splsi1-3*      |[TF]���������� ��������� ������������� |F|
C |L_ul          |1 1 S|_splsi1-2*      |[TF]���������� ��������� ������������� |F|
C |L_at          |1 1 I|FDA20TRAN01_C2_CBC|������ ������||
C |L_et          |1 1 I|FDA20TRAN02_C2_CBC|������ ������||
C |L_it          |1 1 I|FDA20TRAN03_C2_CBC|������ ������||
C |L_ot          |1 1 I|FDA20TRAN04_C2_CBC|������ ������||
C |L_ut          |1 1 I|FDA20TRAN05_C2_CBC|������ ������||
C |L_av          |1 1 I|FDA20TRAN06_C2_CBC|������ ������||
C |L_ev          |1 1 I|FDA20TRAN07_C2_CBC|������ ������||
C |L_iv          |1 1 I|FDA20TRAN08_C2_CBC|������ ������||
C |L_ov          |1 1 I|FDA20TRAN09_C2_CBC|������ ������||
C |L_uv          |1 1 I|FDA20TRAN10_C2_CBC|������ ������||
C |L_ix          |1 1 I|FDA20UNL01      |�������||
C |R_ox          |4 4 K|_utunweight     |����������� ������ ��������� ������|11.0|
C |R_ux          |4 4 K|_ttunweight     |������ ����� ��������� �������|21|
C |R_abe         |4 4 S|_stunweight*    |��������� ����������  ||
C |R_ebe         |4 4 K|_ltunweight     |����������� ������ ��������� �����|7.0|
C |L_obe         |1 1 S|_uc0tunweight*  |��������� ����� ���������� |F|
C |L_ade         |1 1 S|_lc0tunweight*  |��������� ����� ���������� |F|
C |L_ede         |1 1 I|FDA20KANT01_C10_XH53|��������� �� ����������||
C |L_ide         |1 1 I|FDA20KANT01_C11_CBC|��������� �������||
C |R_afe         |4 4 K|_lcmpc11-8      |[]�������� ������ �����������|0.1|
C |R_ife         |4 4 K|_lcmpc11-7      |[]�������� ������ �����������|600|
C |R_ufe         |4 4 K|_lcmpc11-6      |[]�������� ������ �����������|1950|
C |L_ake         |1 1 S|_splsi11-1*     |[TF]���������� ��������� ������������� |F|
C |R_eke         |4 4 I|FDA20KANT01_C1_MFVAL|��������� ����� ����������||
C |R_ike         |4 4 I|FDA20KANT01VZ01 |���������� ����������� �� OZ||
C |R_oke         |4 4 I|FDA20KANT01VX01 |���������� ����������� �� OX||
C |L_uke         |1 1 O|TAKE11          |��������� �������� � �����������||
C |L_ile         |1 1 S|_qfftr11*       |�������� ������ Q RS-��������  |F|
C |L_ule         |1 1 I|FDA20KANT01_C2_CBC|������ ������||
C |R_ime         |4 4 K|_lcmpc11-3      |[]�������� ������ �����������|5|
C |R_ape         |4 4 K|_lcmpc11-1      |[]�������� ������ �����������|5|
C |R_ipe         |4 4 S|_memadd_t4*     |����������� �������� ������� ||
C |R_upe         |4 4 S|_memadd_t3*     |����������� �������� ������� ||
C |R_ere         |4 4 S|_memadd_t1*     |����������� �������� ������� ||
C |R_ore         |4 4 I|START_T         |||
C |R_ise         |4 4 S|_memadd_t2*     |����������� �������� ������� ||
C |R_ate         |4 4 O|R1VT01          |����������� ����������||
C |R_ote         |4 4 S|_memwtng_u*     |����������� �������� ������� ||
C |R_ave         |4 4 S|_memwtng_f*     |����������� �������� ������� ||
C |R_eve         |4 4 S|_memwtng_c*     |����������� �������� ������� ||
C |R_ive         |4 4 O|R1VW01_u        |����� ���������� ����� �������||
C |R_ove         |4 4 O|R1VW01_f        |����� ���������� ����� �������||
C |R_uve         |4 4 O|R1VW01_c        |����� ���������� �������||
C |R_axe         |4 4 O|R1VW01          |����� ����������|`START_W`|
C |L_exe         |1 1 O|INABOX01        |� ����������� �����||
C |L_ixe         |1 1 O|INABOX10        |� ������������� �����||
C |L_oxe         |1 1 O|INABOX09        |� ����� ���������� 3||
C |L_uxe         |1 1 O|INABOX08        |� ����� ���������� 2||
C |L_abi         |1 1 O|INABOX07        |� ����� ���������� 1||
C |L_ebi         |1 1 O|INABOX06        |� ����� ����������||
C |L_ibi         |1 1 O|INABOX05        |� ����� �������� �������||
C |L_obi         |1 1 O|INABOX04        |� ����� �������� ������||
C |L_ubi         |1 1 O|INABOX03        |� ����� �������� ��������||
C |L_adi         |1 1 O|INABOX02        |� ����� �������� �������� ����� ���������||
C |R_eri         |4 4 K|_lcmpc1-0       |[]�������� ������ �����������|20000|
C |R_iri         |4 4 O|VY01            |��������� ���������� �� OY|0|
C |L_ori         |1 1 S|_splsi10-1*     |[TF]���������� ��������� ������������� |F|
C |R_uri         |4 4 I|FDA20TRAN10_C1_MFVAL|��������� �����||
C |L_asi         |1 1 O|TAKE10          |��������� �������� � ����������� 10||
C |L_osi         |1 1 S|_qfftr10*       |�������� ������ Q RS-��������  |F|
C |R_oti         |4 4 K|_lcmpc10-3      |[]�������� ������ �����������|100|
C |R_evi         |4 4 K|_lcmpc10-1      |[]�������� ������ �����������|100|
C |R_ovi         |4 4 I|FDA20TRAN10VZ01 |���������� ������������ �� OZ||
C |R_uvi         |4 4 I|FDA20TRAN10VX01 |���������� ������������ �� OX||
C |L_axi         |1 1 S|_splsi9-1*      |[TF]���������� ��������� ������������� |F|
C |R_exi         |4 4 I|FDA20TRAN09_C1_MFVAL|��������� �����||
C |L_ixi         |1 1 O|TAKE9           |��������� �������� � ����������� 9||
C |L_abo         |1 1 S|_qfftr9*        |�������� ������ Q RS-��������  |F|
C |R_ado         |4 4 K|_lcmpc9-3       |[]�������� ������ �����������|100|
C |R_odo         |4 4 K|_lcmpc9-1       |[]�������� ������ �����������|100|
C |R_afo         |4 4 I|FDA20TRAN09VZ01 |���������� ������������ �� OZ||
C |R_efo         |4 4 I|FDA20TRAN09VX01 |���������� ������������ �� OX||
C |L_ifo         |1 1 S|_splsi8-1*      |[TF]���������� ��������� ������������� |F|
C |R_ofo         |4 4 I|FDA20TRAN08_C1_MFVAL|��������� �����||
C |L_ufo         |1 1 O|TAKE8           |��������� �������� � ����������� 8||
C |L_iko         |1 1 S|_qfftr8*        |�������� ������ Q RS-��������  |F|
C |R_ilo         |4 4 K|_lcmpc8-3       |[]�������� ������ �����������|100|
C |R_amo         |4 4 K|_lcmpc8-1       |[]�������� ������ �����������|100|
C |R_imo         |4 4 I|FDA20TRAN08VZ01 |���������� ������������ �� OZ||
C |R_omo         |4 4 I|FDA20TRAN08VX01 |���������� ������������ �� OX||
C |L_umo         |1 1 S|_splsi7-1*      |[TF]���������� ��������� ������������� |F|
C |R_apo         |4 4 I|FDA20TRAN07_C1_MFVAL|��������� �����||
C |L_epo         |1 1 O|TAKE7           |��������� �������� � ����������� 7||
C |L_upo         |1 1 S|_qfftr7*        |�������� ������ Q RS-��������  |F|
C |R_uro         |4 4 K|_lcmpc7-3       |[]�������� ������ �����������|100|
C |R_iso         |4 4 K|_lcmpc7-1       |[]�������� ������ �����������|100|
C |R_uso         |4 4 I|FDA20TRAN07VZ01 |���������� ������������ �� OZ||
C |R_ato         |4 4 I|FDA20TRAN07VX01 |���������� ������������ �� OX||
C |L_eto         |1 1 S|_splsi6-1*      |[TF]���������� ��������� ������������� |F|
C |R_ito         |4 4 I|FDA20TRAN06_C1_MFVAL|��������� �����||
C |L_oto         |1 1 O|TAKE6           |��������� �������� � ����������� 6||
C |L_evo         |1 1 S|_qfftr6*        |�������� ������ Q RS-��������  |F|
C |R_exo         |4 4 K|_lcmpc6-3       |[]�������� ������ �����������|100|
C |R_uxo         |4 4 K|_lcmpc6-1       |[]�������� ������ �����������|100|
C |R_ebu         |4 4 I|FDA20TRAN06VZ01 |���������� ������������ �� OZ||
C |R_ibu         |4 4 I|FDA20TRAN06VX01 |���������� ������������ �� OX||
C |L_obu         |1 1 S|_splsi5-1*      |[TF]���������� ��������� ������������� |F|
C |R_ubu         |4 4 I|FDA20TRAN05_C1_MFVAL|��������� �����||
C |L_adu         |1 1 O|TAKE5           |��������� �������� � ����������� 5||
C |L_odu         |1 1 S|_qfftr5*        |�������� ������ Q RS-��������  |F|
C |R_ofu         |4 4 K|_lcmpc5-3       |[]�������� ������ �����������|100|
C |R_eku         |4 4 K|_lcmpc5-1       |[]�������� ������ �����������|100|
C |R_oku         |4 4 I|FDA20TRAN05VZ01 |���������� ������������ �� OZ||
C |R_uku         |4 4 I|FDA20TRAN05VX01 |���������� ������������ �� OX||
C |L_alu         |1 1 S|_splsi4-1*      |[TF]���������� ��������� ������������� |F|
C |R_elu         |4 4 I|FDA20TRAN04_C1_MFVAL|��������� �����||
C |L_ilu         |1 1 O|TAKE4           |��������� �������� � ����������� 4||
C |L_omu         |1 1 S|_qff4*          |�������� ������ Q RS-��������  |F|
C |L_umu         |1 1 O|FULL4           |��������� ����� � ������� PU||
C |R_epu         |4 4 K|_lcmpc4-3       |[]�������� ������ �����������|100|
C |R_upu         |4 4 K|_lcmpc4-1       |[]�������� ������ �����������|100|
C |R_iru         |4 4 K|_lcmpc4-8       |[]�������� ������ �����������|0.1|
C |R_uru         |4 4 K|_lcmpc4-7       |[]�������� ������ �����������|480|
C |R_esu         |4 4 K|_lcmpc4-6       |[]�������� ������ �����������|7950|
C |R_isu         |4 4 I|FDA20TRAN04VZ01 |���������� ������������ �� OZ||
C |R_osu         |4 4 I|FDA20TRAN04VX01 |���������� ������������ �� OX||
C |L_usu         |1 1 S|_splsi3-1*      |[TF]���������� ��������� ������������� |F|
C |R_atu         |4 4 I|FDA20TRAN03_C1_MFVAL|��������� �����||
C |L_etu         |1 1 O|TAKE3           |��������� �������� � ����������� 3||
C |L_ivu         |1 1 S|_qfftr3*        |�������� ������ Q RS-��������  |F|
C |L_ovu         |1 1 O|FULL3           |��������� ����� � ������� ������||
C |R_axu         |4 4 K|_lcmpc3-3       |[]�������� ������ �����������|100|
C |R_oxu         |4 4 K|_lcmpc3-1       |[]�������� ������ �����������|100|
C |R_ebad        |4 4 K|_lcmpc3-8       |[]�������� ������ �����������|0.1|
C |R_obad        |4 4 K|_lcmpc3-7       |[]�������� ������ �����������|480|
C |R_adad        |4 4 K|_lcmpc3-6       |[]�������� ������ �����������|5950|
C |R_edad        |4 4 I|FDA20TRAN03VZ01 |���������� ������������ �� OZ||
C |R_idad        |4 4 I|FDA20TRAN03VX01 |���������� ������������ �� OX||
C |L_odad        |1 1 S|_splsi2-1*      |[TF]���������� ��������� ������������� |F|
C |R_udad        |4 4 I|FDA20TRAN02_C1_MFVAL|��������� �����||
C |L_afad        |1 1 O|TAKE2           |��������� �������� � ����������� 2||
C |L_ekad        |1 1 S|_qfftr2*        |�������� ������ Q RS-��������  |F|
C |L_ikad        |1 1 O|FULL2           |��������� ����� � ������� ��������||
C |R_ukad        |4 4 K|_lcmpc2-3       |[]�������� ������ �����������|100|
C |R_ilad        |4 4 K|_lcmpc2-1       |[]�������� ������ �����������|100|
C |R_amad        |4 4 K|_lcmpc2-8       |[]�������� ������ �����������|0.1|
C |R_imad        |4 4 K|_lcmpc2-7       |[]�������� ������ �����������|480|
C |R_umad        |4 4 K|_lcmpc2-6       |[]�������� ������ �����������|3950|
C |R_ipad        |4 4 I|FDA20TRAN02VZ01 |���������� ������������ �� OZ||
C |R_opad        |4 4 I|FDA20TRAN02VX01 |���������� ������������ �� OX||
C |L_upad        |1 1 S|_splsi1-1*      |[TF]���������� ��������� ������������� |F|
C |R_arad        |4 4 I|FDA20TRAN01_C1_MFVAL|��������� �����||
C |L_erad        |1 1 O|TAKE1           |��������� �������� � ����������� 1||
C |L_isad        |1 1 S|_qfftr1*        |�������� ������ Q RS-��������  |F|
C |L_osad        |1 1 O|FULL1           |��������� ����� � ������� �������� �����||
C |R_etad        |4 4 K|_lcmpc1-3       |[]�������� ������ �����������|100|
C |R_otad        |4 4 K|_lcmpc1-1       |[]�������� ������ �����������|100|
C |R_ivad        |4 4 K|_lcmpc1-8       |[]�������� ������ �����������|0.1|
C |R_uvad        |4 4 K|_lcmpc1-7       |[]�������� ������ �����������|480|
C |R_exad        |4 4 K|_lcmpc1-6       |[]�������� ������ �����������|1950|
C |R_uxad        |4 4 O|VV01            |��������� ����� ����������||
C |R_abed        |4 4 I|FDA20TRAN01VZ01 |���������� ������������ �� OZ||
C |R_ebed        |4 4 O|VZ01            |��������� ���������� �� OZ|`START_Z`|
C |R_ibed        |4 4 I|FDA20TRAN01VX01 |���������� ������������ �� OX||
C |R_obed        |4 4 O|VX01            |��������� ���������� �� OX|`START_X`|

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R0_e,R_i,R_o
      LOGICAL*1 L_u,L_ad,L_ed,L_id,L_od,L_ud,L_af,L_ef,L_if
     &,L_of,L_uf,L_ak,L_ek,L_ik,L_ok,L_uk,L_al,L_el,L_il,L_ol
     &,L_ul
      REAL*4 R0_am,R0_em,R0_im,R0_om,R0_um,R0_ap,R0_ep,R0_ip
     &,R0_op,R0_up,R0_ar,R0_er,R0_ir,R0_or,R0_ur,R0_as,R0_es
     &,R0_is
      LOGICAL*1 L0_os,L0_us,L_at,L_et,L_it,L_ot,L_ut,L_av
     &,L_ev,L_iv,L_ov,L_uv,L0_ax,L0_ex,L_ix
      REAL*4 R_ox,R_ux,R_abe,R_ebe
      INTEGER*4 I0_ibe
      LOGICAL*1 L_obe,L0_ube,L_ade,L_ede,L_ide,L0_ode,L0_ude
      REAL*4 R_afe
      LOGICAL*1 L0_efe
      REAL*4 R_ife
      LOGICAL*1 L0_ofe
      REAL*4 R_ufe
      LOGICAL*1 L_ake
      REAL*4 R_eke,R_ike,R_oke
      LOGICAL*1 L_uke,L0_ale,L0_ele,L_ile,L0_ole,L_ule,L0_ame
     &,L0_eme
      REAL*4 R_ime,R0_ome
      LOGICAL*1 L0_ume
      REAL*4 R_ape,R0_epe,R_ipe,R0_ope,R_upe,R0_are,R_ere
     &,R0_ire,R_ore,R0_ure,R0_ase,R0_ese,R_ise,R0_ose,R0_use
     &,R_ate
      LOGICAL*1 L0_ete
      REAL*4 R0_ite,R_ote
      LOGICAL*1 L0_ute
      REAL*4 R_ave,R_eve,R_ive,R_ove,R_uve,R_axe
      LOGICAL*1 L_exe,L_ixe,L_oxe,L_uxe,L_abi,L_ebi,L_ibi
     &,L_obi,L_ubi,L_adi,L0_edi
      REAL*4 R0_idi,R0_odi,R0_udi,R0_afi,R0_efi,R0_ifi,R0_ofi
     &,R0_ufi,R0_aki,R0_eki,R0_iki,R0_oki,R0_uki,R0_ali,R0_eli
     &,R0_ili,R0_oli,R0_uli,R0_ami,R0_emi,R0_imi,R0_omi
      REAL*4 R0_umi,R0_api,R0_epi,R0_ipi,R0_opi,R0_upi
      LOGICAL*1 L0_ari
      REAL*4 R_eri,R_iri
      LOGICAL*1 L_ori
      REAL*4 R_uri
      LOGICAL*1 L_asi,L0_esi,L0_isi,L_osi,L0_usi,L0_ati,L0_eti
     &,L0_iti
      REAL*4 R_oti,R0_uti
      LOGICAL*1 L0_avi
      REAL*4 R_evi,R0_ivi,R_ovi,R_uvi
      LOGICAL*1 L_axi
      REAL*4 R_exi
      LOGICAL*1 L_ixi,L0_oxi,L0_uxi,L_abo,L0_ebo,L0_ibo,L0_obo
     &,L0_ubo
      REAL*4 R_ado,R0_edo
      LOGICAL*1 L0_ido
      REAL*4 R_odo,R0_udo,R_afo,R_efo
      LOGICAL*1 L_ifo
      REAL*4 R_ofo
      LOGICAL*1 L_ufo,L0_ako,L0_eko,L_iko,L0_oko,L0_uko,L0_alo
     &,L0_elo
      REAL*4 R_ilo,R0_olo
      LOGICAL*1 L0_ulo
      REAL*4 R_amo,R0_emo,R_imo,R_omo
      LOGICAL*1 L_umo
      REAL*4 R_apo
      LOGICAL*1 L_epo,L0_ipo,L0_opo,L_upo,L0_aro,L0_ero,L0_iro
     &,L0_oro
      REAL*4 R_uro,R0_aso
      LOGICAL*1 L0_eso
      REAL*4 R_iso,R0_oso,R_uso,R_ato
      LOGICAL*1 L_eto
      REAL*4 R_ito
      LOGICAL*1 L_oto,L0_uto,L0_avo,L_evo,L0_ivo,L0_ovo,L0_uvo
     &,L0_axo
      REAL*4 R_exo,R0_ixo
      LOGICAL*1 L0_oxo
      REAL*4 R_uxo,R0_abu,R_ebu,R_ibu
      LOGICAL*1 L_obu
      REAL*4 R_ubu
      LOGICAL*1 L_adu,L0_edu,L0_idu,L_odu,L0_udu,L0_afu,L0_efu
     &,L0_ifu
      REAL*4 R_ofu,R0_ufu
      LOGICAL*1 L0_aku
      REAL*4 R_eku,R0_iku,R_oku,R_uku
      LOGICAL*1 L_alu
      REAL*4 R_elu
      LOGICAL*1 L_ilu,L0_olu,L0_ulu,L0_amu,L0_emu,L0_imu,L_omu
     &,L_umu,L0_apu
      REAL*4 R_epu,R0_ipu
      LOGICAL*1 L0_opu
      REAL*4 R_upu,R0_aru
      LOGICAL*1 L0_eru
      REAL*4 R_iru
      LOGICAL*1 L0_oru
      REAL*4 R_uru
      LOGICAL*1 L0_asu
      REAL*4 R_esu,R_isu,R_osu
      LOGICAL*1 L_usu
      REAL*4 R_atu
      LOGICAL*1 L_etu,L0_itu,L0_otu,L0_utu,L0_avu,L0_evu,L_ivu
     &,L_ovu,L0_uvu
      REAL*4 R_axu,R0_exu
      LOGICAL*1 L0_ixu
      REAL*4 R_oxu,R0_uxu
      LOGICAL*1 L0_abad
      REAL*4 R_ebad
      LOGICAL*1 L0_ibad
      REAL*4 R_obad
      LOGICAL*1 L0_ubad
      REAL*4 R_adad,R_edad,R_idad
      LOGICAL*1 L_odad
      REAL*4 R_udad
      LOGICAL*1 L_afad,L0_efad,L0_ifad,L0_ofad,L0_ufad,L0_akad
     &,L_ekad,L_ikad,L0_okad
      REAL*4 R_ukad,R0_alad
      LOGICAL*1 L0_elad
      REAL*4 R_ilad,R0_olad
      LOGICAL*1 L0_ulad
      REAL*4 R_amad
      LOGICAL*1 L0_emad
      REAL*4 R_imad
      LOGICAL*1 L0_omad
      REAL*4 R_umad,R0_apad,R0_epad,R_ipad,R_opad
      LOGICAL*1 L_upad
      REAL*4 R_arad
      LOGICAL*1 L_erad,L0_irad,L0_orad,L0_urad,L0_asad,L0_esad
     &,L_isad,L_osad,L0_usad,L0_atad
      REAL*4 R_etad
      LOGICAL*1 L0_itad
      REAL*4 R_otad,R0_utad,R0_avad
      LOGICAL*1 L0_evad
      REAL*4 R_ivad
      LOGICAL*1 L0_ovad
      REAL*4 R_uvad
      LOGICAL*1 L0_axad
      REAL*4 R_exad,R0_ixad,R0_oxad,R_uxad,R_abed,R_ebed,R_ibed
     &,R_obed

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_e=R_i
C KONTEYNER_DOZ.fmg( 232, 523):pre: ������������  �� T,ftime
      L0_eko=.NOT.L_iv.and..not.L_ud
      L_ud=.NOT.L_iv
C KONTEYNER_DOZ.fmg( 227, 732):������������  �� 1 ���,i8-3
      L0_uko=L_iv.and..not.L_af
      L_af=L_iv
C KONTEYNER_DOZ.fmg( 227, 738):������������  �� 1 ���,i8-2
      L0_opo=.NOT.L_ev.and..not.L_ef
      L_ef=.NOT.L_ev
C KONTEYNER_DOZ.fmg( 227, 772):������������  �� 1 ���,i7-3
      L0_ero=L_ev.and..not.L_if
      L_if=L_ev
C KONTEYNER_DOZ.fmg( 227, 778):������������  �� 1 ���,i7-2
      L0_avo=.NOT.L_av.and..not.L_of
      L_of=.NOT.L_av
C KONTEYNER_DOZ.fmg( 227, 810):������������  �� 1 ���,i6-3
      L0_ovo=L_av.and..not.L_uf
      L_uf=L_av
C KONTEYNER_DOZ.fmg( 227, 816):������������  �� 1 ���,i6-2
      L0_idu=.NOT.L_ut.and..not.L_ak
      L_ak=.NOT.L_ut
C KONTEYNER_DOZ.fmg( 227, 848):������������  �� 1 ���,i5-3
      L0_afu=L_ut.and..not.L_ek
      L_ek=L_ut
C KONTEYNER_DOZ.fmg( 227, 854):������������  �� 1 ���,i5-2
      L0_ulu=.NOT.L_ot.and..not.L_ik
      L_ik=.NOT.L_ot
C KONTEYNER_DOZ.fmg(  46, 676):������������  �� 1 ���,i4-3
      L0_emu=L_ot.and..not.L_ok
      L_ok=L_ot
C KONTEYNER_DOZ.fmg(  46, 682):������������  �� 1 ���,i4-2
      L0_otu=.NOT.L_it.and..not.L_uk
      L_uk=.NOT.L_it
C KONTEYNER_DOZ.fmg(  46, 730):������������  �� 1 ���,i3-3
      L0_avu=L_it.and..not.L_al
      L_al=L_it
C KONTEYNER_DOZ.fmg(  46, 736):������������  �� 1 ���,i3-2
      L0_ifad=.NOT.L_et.and..not.L_el
      L_el=.NOT.L_et
C KONTEYNER_DOZ.fmg(  46, 784):������������  �� 1 ���,i2-3
      L0_ufad=L_et.and..not.L_il
      L_il=L_et
C KONTEYNER_DOZ.fmg(  46, 790):������������  �� 1 ���,i2-2
      L0_orad=.NOT.L_at.and..not.L_ol
      L_ol=.NOT.L_at
C KONTEYNER_DOZ.fmg(  46, 842):������������  �� 1 ���,i1-3
      L0_usad=L_at.and..not.L_ul
      L_ul=L_at
C KONTEYNER_DOZ.fmg(  46, 848):������������  �� 1 ���,i1-2
      I0_ibe = 1
C KONTEYNER_DOZ.fmg( 247, 518):��������� ������������� IN (�������)
      R0_ope = -20
C KONTEYNER_DOZ.fmg(  66, 512):��������� (RE4) (�������)
      R0_are = -20
C KONTEYNER_DOZ.fmg(  56, 514):��������� (RE4) (�������)
      R0_ire = 60
C KONTEYNER_DOZ.fmg(  37, 518):��������� (RE4) (�������)
      R0_use = -20
C KONTEYNER_DOZ.fmg(  47, 516):��������� (RE4) (�������)
      R0_ite = 20000
C KONTEYNER_DOZ.fmg(  44, 584):��������� (RE4) (�������)
      R0_idi = 200
C KONTEYNER_DOZ.fmg(  44, 523):��������� (RE4) (�������)
      R0_eki = 18000
C KONTEYNER_DOZ.fmg(  44, 528):��������� (RE4) (�������)
      R0_oki = 16000
C KONTEYNER_DOZ.fmg(  44, 534):��������� (RE4) (�������)
      R0_ali = 14000
C KONTEYNER_DOZ.fmg(  44, 540):��������� (RE4) (�������)
      R0_ili = 12000
C KONTEYNER_DOZ.fmg(  44, 546):��������� (RE4) (�������)
      R0_uli = 10000
C KONTEYNER_DOZ.fmg(  44, 552):��������� (RE4) (�������)
      R0_emi = 8000
C KONTEYNER_DOZ.fmg(  44, 558):��������� (RE4) (�������)
      R0_omi = 6000
C KONTEYNER_DOZ.fmg(  44, 564):��������� (RE4) (�������)
      R0_api = 4000
C KONTEYNER_DOZ.fmg(  44, 570):��������� (RE4) (�������)
      R0_opi = 2000
C KONTEYNER_DOZ.fmg(  44, 576):��������� (RE4) (�������)
      R0_oxad = R_obed + (-R_ibed)
C KONTEYNER_DOZ.fmg(  30, 864):��������
C label 61  try61=try61-1
      if(L_ile) then
         R_iri=R_oke
      endif
C KONTEYNER_DOZ.fmg( 290, 602):���� � ������������� �������
      R0_em = R_iri + (-R_oke)
C KONTEYNER_DOZ.fmg( 212, 607):��������
      R0_epe=abs(R0_em)
C KONTEYNER_DOZ.fmg( 220, 607):���������� ��������
      L0_ume=R0_epe.lt.R_ape
C KONTEYNER_DOZ.fmg( 228, 607):���������� <,c11-1
      R0_avad=abs(R0_oxad)
C KONTEYNER_DOZ.fmg(  38, 864):���������� ��������
      L0_ari=R0_avad.gt.R_eri
C KONTEYNER_DOZ.fmg(  47, 870):���������� >,c1-0
      L0_itad=R0_avad.lt.R_otad
C KONTEYNER_DOZ.fmg(  47, 864):���������� <,c1-1
      L0_asad = L0_ari.OR.L0_itad
C KONTEYNER_DOZ.fmg(  54, 867):���
      L0_us = L_ov.AND.(.NOT.L_uke)
C KONTEYNER_DOZ.fmg( 212, 694):�
      L0_uxi=.NOT.L0_us.and..not.L_id
      L_id=.NOT.L0_us
C KONTEYNER_DOZ.fmg( 227, 694):������������  �� 1 ���,i9-3
      if(L_isad) then
         R_obed=R_ibed
      endif
C KONTEYNER_DOZ.fmg( 104, 860):���� � ������������� �������
      if(L_ekad) then
         R_obed=R_opad
      endif
C KONTEYNER_DOZ.fmg( 110, 802):���� � ������������� �������
      R0_is = R_obed + (-R_idad)
C KONTEYNER_DOZ.fmg(  30, 752):��������
      R0_uxu=abs(R0_is)
C KONTEYNER_DOZ.fmg(  38, 752):���������� ��������
      L0_ixu=R0_uxu.lt.R_oxu
C KONTEYNER_DOZ.fmg(  47, 752):���������� <,c3-1
      if(L_ivu) then
         R_ebed=R_edad
      endif
C KONTEYNER_DOZ.fmg( 110, 740):���� � ������������� �������
      if(L_ile) then
         R_ebed=R_ike
      endif
C KONTEYNER_DOZ.fmg( 290, 594):���� � ������������� �������
      if(L_osi) then
         R_ebed=R_ovi
      endif
C KONTEYNER_DOZ.fmg( 290, 662):���� � ������������� �������
      if(L_abo) then
         R_ebed=R_afo
      endif
C KONTEYNER_DOZ.fmg( 290, 702):���� � ������������� �������
      if(L_iko) then
         R_ebed=R_imo
      endif
C KONTEYNER_DOZ.fmg( 290, 741):���� � ������������� �������
      if(L_upo) then
         R_ebed=R_uso
      endif
C KONTEYNER_DOZ.fmg( 290, 780):���� � ������������� �������
      if(L_evo) then
         R_ebed=R_ebu
      endif
C KONTEYNER_DOZ.fmg( 290, 818):���� � ������������� �������
      if(L_odu) then
         R_ebed=R_oku
      endif
C KONTEYNER_DOZ.fmg( 290, 858):���� � ������������� �������
      if(L_omu) then
         R_ebed=R_isu
      endif
C KONTEYNER_DOZ.fmg( 110, 685):���� � ������������� �������
      R0_es = R_ebed + (-R_edad)
C KONTEYNER_DOZ.fmg(  30, 744):��������
      R0_exu=abs(R0_es)
C KONTEYNER_DOZ.fmg(  38, 744):���������� ��������
      L0_uvu=R0_exu.lt.R_axu
C KONTEYNER_DOZ.fmg(  47, 744):���������� <,c3-3
      L0_evu = L0_ixu.AND.L0_uvu.AND.L0_avu
C KONTEYNER_DOZ.fmg(  56, 744):�
      L0_itu=L0_evu.and..not.L_usu
      L_usu=L0_evu
C KONTEYNER_DOZ.fmg(  62, 744):������������  �� 1 ���,i3-1
      L_ivu=L0_itu.or.(L_ivu.and..not.(L0_otu))
      L0_utu=.not.L_ivu
C KONTEYNER_DOZ.fmg(  70, 742):RS �������,tr3
      if(L_ivu) then
         R_obed=R_idad
      endif
C KONTEYNER_DOZ.fmg( 110, 748):���� � ������������� �������
      R0_as = R_obed + (-R_osu)
C KONTEYNER_DOZ.fmg(  30, 697):��������
      R0_aru=abs(R0_as)
C KONTEYNER_DOZ.fmg(  38, 697):���������� ��������
      L0_opu=R0_aru.lt.R_upu
C KONTEYNER_DOZ.fmg(  47, 697):���������� <,c4-1
      R0_ur = R_ebed + (-R_isu)
C KONTEYNER_DOZ.fmg(  30, 689):��������
      R0_ipu=abs(R0_ur)
C KONTEYNER_DOZ.fmg(  38, 689):���������� ��������
      L0_apu=R0_ipu.lt.R_epu
C KONTEYNER_DOZ.fmg(  47, 689):���������� <,c4-3
      L0_imu = L0_opu.AND.L0_apu.AND.L0_emu
C KONTEYNER_DOZ.fmg(  56, 689):�
      L0_olu=L0_imu.and..not.L_alu
      L_alu=L0_imu
C KONTEYNER_DOZ.fmg(  62, 689):������������  �� 1 ���,i4-1
      L_omu=L0_olu.or.(L_omu.and..not.(L0_ulu))
      L0_amu=.not.L_omu
C KONTEYNER_DOZ.fmg(  70, 687):RS �������,4
      if(L_omu) then
         R_obed=R_osu
      endif
C KONTEYNER_DOZ.fmg( 110, 694):���� � ������������� �������
      R0_or = R_obed + (-R_uku)
C KONTEYNER_DOZ.fmg( 212, 870):��������
      R0_iku=abs(R0_or)
C KONTEYNER_DOZ.fmg( 220, 870):���������� ��������
      L0_aku=R0_iku.lt.R_eku
C KONTEYNER_DOZ.fmg( 228, 870):���������� <,c5-1
      R0_ir = R_ebed + (-R_oku)
C KONTEYNER_DOZ.fmg( 212, 862):��������
      R0_ufu=abs(R0_ir)
C KONTEYNER_DOZ.fmg( 220, 862):���������� ��������
      L0_ifu=R0_ufu.lt.R_ofu
C KONTEYNER_DOZ.fmg( 228, 862):���������� <,c5-3
      L0_efu = L0_aku.AND.L0_ifu.AND.L0_afu
C KONTEYNER_DOZ.fmg( 237, 862):�
      L0_edu=L0_efu.and..not.L_obu
      L_obu=L0_efu
C KONTEYNER_DOZ.fmg( 243, 862):������������  �� 1 ���,i5-1
      L_odu=L0_edu.or.(L_odu.and..not.(L0_idu))
      L0_udu=.not.L_odu
C KONTEYNER_DOZ.fmg( 251, 860):RS �������,tr5
      if(L_odu) then
         R_obed=R_uku
      endif
C KONTEYNER_DOZ.fmg( 290, 866):���� � ������������� �������
      R0_er = R_obed + (-R_ibu)
C KONTEYNER_DOZ.fmg( 212, 830):��������
      R0_abu=abs(R0_er)
C KONTEYNER_DOZ.fmg( 220, 830):���������� ��������
      L0_oxo=R0_abu.lt.R_uxo
C KONTEYNER_DOZ.fmg( 228, 830):���������� <,c6-1
      R0_ar = R_ebed + (-R_ebu)
C KONTEYNER_DOZ.fmg( 212, 822):��������
      R0_ixo=abs(R0_ar)
C KONTEYNER_DOZ.fmg( 220, 822):���������� ��������
      L0_axo=R0_ixo.lt.R_exo
C KONTEYNER_DOZ.fmg( 228, 822):���������� <,c6-3
      L0_uvo = L0_oxo.AND.L0_axo.AND.L0_ovo
C KONTEYNER_DOZ.fmg( 237, 822):�
      L0_uto=L0_uvo.and..not.L_eto
      L_eto=L0_uvo
C KONTEYNER_DOZ.fmg( 243, 822):������������  �� 1 ���,i6-1
      L_evo=L0_uto.or.(L_evo.and..not.(L0_avo))
      L0_ivo=.not.L_evo
C KONTEYNER_DOZ.fmg( 251, 820):RS �������,tr6
      if(L_evo) then
         R_obed=R_ibu
      endif
C KONTEYNER_DOZ.fmg( 290, 827):���� � ������������� �������
      R0_up = R_obed + (-R_ato)
C KONTEYNER_DOZ.fmg( 212, 792):��������
      R0_oso=abs(R0_up)
C KONTEYNER_DOZ.fmg( 220, 792):���������� ��������
      L0_eso=R0_oso.lt.R_iso
C KONTEYNER_DOZ.fmg( 228, 792):���������� <,c7-1
      R0_op = R_ebed + (-R_uso)
C KONTEYNER_DOZ.fmg( 212, 784):��������
      R0_aso=abs(R0_op)
C KONTEYNER_DOZ.fmg( 220, 784):���������� ��������
      L0_oro=R0_aso.lt.R_uro
C KONTEYNER_DOZ.fmg( 228, 784):���������� <,c7-3
      L0_iro = L0_eso.AND.L0_oro.AND.L0_ero
C KONTEYNER_DOZ.fmg( 237, 784):�
      L0_ipo=L0_iro.and..not.L_umo
      L_umo=L0_iro
C KONTEYNER_DOZ.fmg( 243, 784):������������  �� 1 ���,i7-1
      L_upo=L0_ipo.or.(L_upo.and..not.(L0_opo))
      L0_aro=.not.L_upo
C KONTEYNER_DOZ.fmg( 251, 782):RS �������,tr7
      if(L_upo) then
         R_obed=R_ato
      endif
C KONTEYNER_DOZ.fmg( 290, 789):���� � ������������� �������
      R0_ip = R_obed + (-R_omo)
C KONTEYNER_DOZ.fmg( 212, 753):��������
      R0_emo=abs(R0_ip)
C KONTEYNER_DOZ.fmg( 220, 753):���������� ��������
      L0_ulo=R0_emo.lt.R_amo
C KONTEYNER_DOZ.fmg( 228, 753):���������� <,c8-1
      R0_ep = R_ebed + (-R_imo)
C KONTEYNER_DOZ.fmg( 212, 745):��������
      R0_olo=abs(R0_ep)
C KONTEYNER_DOZ.fmg( 220, 745):���������� ��������
      L0_elo=R0_olo.lt.R_ilo
C KONTEYNER_DOZ.fmg( 228, 745):���������� <,c8-3
      L0_alo = L0_ulo.AND.L0_elo.AND.L0_uko
C KONTEYNER_DOZ.fmg( 237, 745):�
      L0_ako=L0_alo.and..not.L_ifo
      L_ifo=L0_alo
C KONTEYNER_DOZ.fmg( 243, 745):������������  �� 1 ���,i8-1
      L_iko=L0_ako.or.(L_iko.and..not.(L0_eko))
      L0_oko=.not.L_iko
C KONTEYNER_DOZ.fmg( 251, 743):RS �������,tr8
      if(L_iko) then
         R_obed=R_omo
      endif
C KONTEYNER_DOZ.fmg( 290, 750):���� � ������������� �������
      R0_ap = R_obed + (-R_efo)
C KONTEYNER_DOZ.fmg( 212, 714):��������
      R0_udo=abs(R0_ap)
C KONTEYNER_DOZ.fmg( 220, 714):���������� ��������
      L0_ido=R0_udo.lt.R_odo
C KONTEYNER_DOZ.fmg( 228, 714):���������� <,c9-1
      R0_um = R_ebed + (-R_afo)
C KONTEYNER_DOZ.fmg( 212, 706):��������
      R0_edo=abs(R0_um)
C KONTEYNER_DOZ.fmg( 220, 706):���������� ��������
      L0_ubo=R0_edo.lt.R_ado
C KONTEYNER_DOZ.fmg( 228, 706):���������� <,c9-3
      L0_ibo=L0_us.and..not.L_od
      L_od=L0_us
C KONTEYNER_DOZ.fmg( 227, 700):������������  �� 1 ���,i9-2
      L0_obo = L0_ido.AND.L0_ubo.AND.L0_ibo
C KONTEYNER_DOZ.fmg( 237, 706):�
      L0_oxi=L0_obo.and..not.L_axi
      L_axi=L0_obo
C KONTEYNER_DOZ.fmg( 243, 706):������������  �� 1 ���,i9-1
      L_abo=L0_oxi.or.(L_abo.and..not.(L0_uxi))
      L0_ebo=.not.L_abo
C KONTEYNER_DOZ.fmg( 251, 704):RS �������,tr9
      if(L_abo) then
         R_obed=R_efo
      endif
C KONTEYNER_DOZ.fmg( 290, 711):���� � ������������� �������
      R0_epad = R_obed + (-R_opad)
C KONTEYNER_DOZ.fmg(  30, 805):��������
      R0_olad=abs(R0_epad)
C KONTEYNER_DOZ.fmg(  38, 805):���������� ��������
      L0_elad=R0_olad.lt.R_ilad
C KONTEYNER_DOZ.fmg(  47, 805):���������� <,c2-1
      R0_apad = R_ebed + (-R_ipad)
C KONTEYNER_DOZ.fmg(  30, 797):��������
      R0_alad=abs(R0_apad)
C KONTEYNER_DOZ.fmg(  38, 797):���������� ��������
      L0_okad=R0_alad.lt.R_ukad
C KONTEYNER_DOZ.fmg(  47, 797):���������� <,c2-3
      L0_akad = L0_elad.AND.L0_okad.AND.L0_ufad
C KONTEYNER_DOZ.fmg(  56, 797):�
      L0_efad=L0_akad.and..not.L_odad
      L_odad=L0_akad
C KONTEYNER_DOZ.fmg(  62, 797):������������  �� 1 ���,i2-1
      L_ekad=L0_efad.or.(L_ekad.and..not.(L0_ifad))
      L0_ofad=.not.L_ekad
C KONTEYNER_DOZ.fmg(  70, 795):RS �������,tr2
      if(L_ekad) then
         R_ebed=R_ipad
      endif
C KONTEYNER_DOZ.fmg( 110, 793):���� � ������������� �������
      R0_ixad = R_ebed + (-R_abed)
C KONTEYNER_DOZ.fmg(  30, 856):��������
      R0_utad=abs(R0_ixad)
C KONTEYNER_DOZ.fmg(  38, 856):���������� ��������
      L0_atad=R0_utad.lt.R_etad
C KONTEYNER_DOZ.fmg(  47, 856):���������� <,c1-3
      L0_esad = L0_asad.AND.L0_atad.AND.L0_usad
C KONTEYNER_DOZ.fmg(  56, 856):�
      L0_irad=L0_esad.and..not.L_upad
      L_upad=L0_esad
C KONTEYNER_DOZ.fmg(  62, 856):������������  �� 1 ���,i1-1
      L_isad=L0_irad.or.(L_isad.and..not.(L0_orad))
      L0_urad=.not.L_isad
C KONTEYNER_DOZ.fmg(  70, 854):RS �������,tr1
      if(L_isad) then
         R_ebed=R_abed
      endif
C KONTEYNER_DOZ.fmg( 104, 852):���� � ������������� �������
C sav1=R0_apad
      R0_apad = R_ebed + (-R_ipad)
C KONTEYNER_DOZ.fmg(  30, 797):recalc:��������
C if(sav1.ne.R0_apad .and. try416.gt.0) goto 416
C sav1=R0_um
      R0_um = R_ebed + (-R_afo)
C KONTEYNER_DOZ.fmg( 212, 706):recalc:��������
C if(sav1.ne.R0_um .and. try391.gt.0) goto 391
C sav1=R0_ir
      R0_ir = R_ebed + (-R_oku)
C KONTEYNER_DOZ.fmg( 212, 862):recalc:��������
C if(sav1.ne.R0_ir .and. try299.gt.0) goto 299
C sav1=R0_op
      R0_op = R_ebed + (-R_uso)
C KONTEYNER_DOZ.fmg( 212, 784):recalc:��������
C if(sav1.ne.R0_op .and. try345.gt.0) goto 345
C sav1=R0_es
      R0_es = R_ebed + (-R_edad)
C KONTEYNER_DOZ.fmg(  30, 744):recalc:��������
C if(sav1.ne.R0_es .and. try251.gt.0) goto 251
C sav1=R0_ur
      R0_ur = R_ebed + (-R_isu)
C KONTEYNER_DOZ.fmg(  30, 689):recalc:��������
C if(sav1.ne.R0_ur .and. try275.gt.0) goto 275
C sav1=R0_ep
      R0_ep = R_ebed + (-R_imo)
C KONTEYNER_DOZ.fmg( 212, 745):recalc:��������
C if(sav1.ne.R0_ep .and. try368.gt.0) goto 368
C sav1=R0_ar
      R0_ar = R_ebed + (-R_ebu)
C KONTEYNER_DOZ.fmg( 212, 822):recalc:��������
C if(sav1.ne.R0_ar .and. try322.gt.0) goto 322
C sav1=R0_ixad
      R0_ixad = R_ebed + (-R_abed)
C KONTEYNER_DOZ.fmg(  30, 856):recalc:��������
C if(sav1.ne.R0_ixad .and. try434.gt.0) goto 434
      R0_am = R_ebed + (-R_ike)
C KONTEYNER_DOZ.fmg( 212, 599):��������
      R0_ome=abs(R0_am)
C KONTEYNER_DOZ.fmg( 220, 599):���������� ��������
      L0_eme=R0_ome.lt.R_ime
C KONTEYNER_DOZ.fmg( 228, 599):���������� <,c11-3
      R0_iki = R_obed + (-R0_eki)
C KONTEYNER_DOZ.fmg(  47, 529):��������
      R0_odi=abs(R0_iki)
C KONTEYNER_DOZ.fmg(  56, 529):���������� ��������
      L_ixe=R0_odi.lt.R0_idi
C KONTEYNER_DOZ.fmg(  67, 528):���������� <
      L0_ame = L0_ume.AND.L0_eme.AND.L_ixe.AND.L_ule
C KONTEYNER_DOZ.fmg( 237, 598):�
      L0_ele=L0_ame.and..not.L_ake
      L_ake=L0_ame
C KONTEYNER_DOZ.fmg( 243, 598):������������  �� 1 ���,i11-1
      L_ile=L0_ele.or.(L_ile.and..not.(.NOT.L_ule))
      L0_ole=.not.L_ile
C KONTEYNER_DOZ.fmg( 251, 596):RS �������,tr11
      L_uke=L_ile
C KONTEYNER_DOZ.fmg( 280, 608):������,TAKE11
C sav1=L0_us
      L0_us = L_ov.AND.(.NOT.L_uke)
C KONTEYNER_DOZ.fmg( 212, 694):recalc:�
C if(sav1.ne.L0_us .and. try86.gt.0) goto 86
      L0_os = L_uv.AND.(.NOT.L_uke)
C KONTEYNER_DOZ.fmg( 212, 653):�
      L0_isi=.NOT.L0_os.and..not.L_ad
      L_ad=.NOT.L0_os
C KONTEYNER_DOZ.fmg( 227, 653):������������  �� 1 ���,i10-3
      R0_om = R_obed + (-R_uvi)
C KONTEYNER_DOZ.fmg( 212, 674):��������
      R0_ivi=abs(R0_om)
C KONTEYNER_DOZ.fmg( 220, 674):���������� ��������
      L0_avi=R0_ivi.lt.R_evi
C KONTEYNER_DOZ.fmg( 228, 674):���������� <,c10-1
      R0_im = R_ebed + (-R_ovi)
C KONTEYNER_DOZ.fmg( 212, 666):��������
      R0_uti=abs(R0_im)
C KONTEYNER_DOZ.fmg( 220, 666):���������� ��������
      L0_iti=R0_uti.lt.R_oti
C KONTEYNER_DOZ.fmg( 228, 666):���������� <,c10-3
      L0_ati=L0_os.and..not.L_ed
      L_ed=L0_os
C KONTEYNER_DOZ.fmg( 227, 659):������������  �� 1 ���,i10-2
      L0_eti = L0_avi.AND.L0_iti.AND.L0_ati
C KONTEYNER_DOZ.fmg( 237, 666):�
      L0_esi=L0_eti.and..not.L_ori
      L_ori=L0_eti
C KONTEYNER_DOZ.fmg( 243, 666):������������  �� 1 ���,i10-1
      L_osi=L0_esi.or.(L_osi.and..not.(L0_isi))
      L0_usi=.not.L_osi
C KONTEYNER_DOZ.fmg( 251, 664):RS �������,tr10
      if(L_osi) then
         R_obed=R_uvi
      endif
C KONTEYNER_DOZ.fmg( 290, 670):���� � ������������� �������
C sav1=R0_om
      R0_om = R_obed + (-R_uvi)
C KONTEYNER_DOZ.fmg( 212, 674):recalc:��������
C if(sav1.ne.R0_om .and. try487.gt.0) goto 487
C sav1=R0_epad
      R0_epad = R_obed + (-R_opad)
C KONTEYNER_DOZ.fmg(  30, 805):recalc:��������
C if(sav1.ne.R0_epad .and. try410.gt.0) goto 410
C sav1=R0_ap
      R0_ap = R_obed + (-R_efo)
C KONTEYNER_DOZ.fmg( 212, 714):recalc:��������
C if(sav1.ne.R0_ap .and. try385.gt.0) goto 385
C sav1=R0_ip
      R0_ip = R_obed + (-R_omo)
C KONTEYNER_DOZ.fmg( 212, 753):recalc:��������
C if(sav1.ne.R0_ip .and. try362.gt.0) goto 362
C sav1=R0_iki
      R0_iki = R_obed + (-R0_eki)
C KONTEYNER_DOZ.fmg(  47, 529):recalc:��������
C if(sav1.ne.R0_iki .and. try458.gt.0) goto 458
C sav1=R0_up
      R0_up = R_obed + (-R_ato)
C KONTEYNER_DOZ.fmg( 212, 792):recalc:��������
C if(sav1.ne.R0_up .and. try339.gt.0) goto 339
C sav1=R0_er
      R0_er = R_obed + (-R_ibu)
C KONTEYNER_DOZ.fmg( 212, 830):recalc:��������
C if(sav1.ne.R0_er .and. try316.gt.0) goto 316
C sav1=R0_is
      R0_is = R_obed + (-R_idad)
C KONTEYNER_DOZ.fmg(  30, 752):recalc:��������
C if(sav1.ne.R0_is .and. try167.gt.0) goto 167
C sav1=R0_or
      R0_or = R_obed + (-R_uku)
C KONTEYNER_DOZ.fmg( 212, 870):recalc:��������
C if(sav1.ne.R0_or .and. try293.gt.0) goto 293
C sav1=R0_as
      R0_as = R_obed + (-R_osu)
C KONTEYNER_DOZ.fmg(  30, 697):recalc:��������
C if(sav1.ne.R0_as .and. try269.gt.0) goto 269
C sav1=R0_oxad
      R0_oxad = R_obed + (-R_ibed)
C KONTEYNER_DOZ.fmg(  30, 864):recalc:��������
C if(sav1.ne.R0_oxad .and. try61.gt.0) goto 61
      L0_axad=R_obed.gt.R_exad
C KONTEYNER_DOZ.fmg(  47, 836):���������� >,c1-6
      L0_asu=R_obed.gt.R_esu
C KONTEYNER_DOZ.fmg(  47, 670):���������� >,c4-6
      L0_ubad=R_obed.gt.R_adad
C KONTEYNER_DOZ.fmg(  47, 724):���������� >,c3-6
      L0_edi=R_obed.lt.R0_idi
C KONTEYNER_DOZ.fmg(  67, 582):���������� <
      L0_ete=R_obed.gt.R0_ite
C KONTEYNER_DOZ.fmg(  67, 586):���������� >
      L_exe = L0_ete.OR.L0_edi
C KONTEYNER_DOZ.fmg(  74, 583):���
      R0_upi = R_obed + (-R0_opi)
C KONTEYNER_DOZ.fmg(  47, 577):��������
      R0_ipi=abs(R0_upi)
C KONTEYNER_DOZ.fmg(  56, 577):���������� ��������
      L_adi=R0_ipi.lt.R0_idi
C KONTEYNER_DOZ.fmg(  67, 576):���������� <
      R0_epi = R_obed + (-R0_api)
C KONTEYNER_DOZ.fmg(  47, 571):��������
      R0_aki=abs(R0_epi)
C KONTEYNER_DOZ.fmg(  56, 571):���������� ��������
      L_ubi=R0_aki.lt.R0_idi
C KONTEYNER_DOZ.fmg(  67, 570):���������� <
      R0_umi = R_obed + (-R0_omi)
C KONTEYNER_DOZ.fmg(  47, 565):��������
      R0_ufi=abs(R0_umi)
C KONTEYNER_DOZ.fmg(  56, 565):���������� ��������
      L_obi=R0_ufi.lt.R0_idi
C KONTEYNER_DOZ.fmg(  67, 564):���������� <
      R0_imi = R_obed + (-R0_emi)
C KONTEYNER_DOZ.fmg(  47, 559):��������
      R0_ofi=abs(R0_imi)
C KONTEYNER_DOZ.fmg(  56, 559):���������� ��������
      L_ibi=R0_ofi.lt.R0_idi
C KONTEYNER_DOZ.fmg(  67, 558):���������� <
      L0_ute = L_exe.OR.L_ibi
C KONTEYNER_DOZ.fmg(  62, 472):���
      R0_ami = R_obed + (-R0_uli)
C KONTEYNER_DOZ.fmg(  47, 553):��������
      R0_ifi=abs(R0_ami)
C KONTEYNER_DOZ.fmg(  56, 553):���������� ��������
      L_ebi=R0_ifi.lt.R0_idi
C KONTEYNER_DOZ.fmg(  67, 552):���������� <
      if(L_ebi) then
         R_ere=R0_ire
      endif
      R0_ese=R_ere
C KONTEYNER_DOZ.fmg(  41, 518):�������-��������,add_t1
      R0_oli = R_obed + (-R0_ili)
C KONTEYNER_DOZ.fmg(  47, 547):��������
      R0_efi=abs(R0_oli)
C KONTEYNER_DOZ.fmg(  56, 547):���������� ��������
      L_abi=R0_efi.lt.R0_idi
C KONTEYNER_DOZ.fmg(  67, 546):���������� <
      if(L_abi) then
         R_ise=R0_use
      endif
      R0_ose=R_ise
C KONTEYNER_DOZ.fmg(  51, 516):�������-��������,add_t2
      R0_eli = R_obed + (-R0_ali)
C KONTEYNER_DOZ.fmg(  47, 541):��������
      R0_afi=abs(R0_eli)
C KONTEYNER_DOZ.fmg(  56, 541):���������� ��������
      L_uxe=R0_afi.lt.R0_idi
C KONTEYNER_DOZ.fmg(  67, 540):���������� <
      if(L_uxe) then
         R_upe=R0_are
      endif
      R0_ase=R_upe
C KONTEYNER_DOZ.fmg(  60, 514):�������-��������,add_t3
      R0_uki = R_obed + (-R0_oki)
C KONTEYNER_DOZ.fmg(  47, 535):��������
      R0_udi=abs(R0_uki)
C KONTEYNER_DOZ.fmg(  56, 535):���������� ��������
      L_oxe=R0_udi.lt.R0_idi
C KONTEYNER_DOZ.fmg(  67, 534):���������� <
      if(L_oxe) then
         R_ipe=R0_ope
      endif
      R0_ure=R_ipe
C KONTEYNER_DOZ.fmg(  70, 512):�������-��������,add_t4
      R_ate = R0_ese + R0_ose + R0_ase + R0_ure + R_ore
C KONTEYNER_DOZ.fmg(  90, 515):��������
      L0_omad=R_obed.gt.R_umad
C KONTEYNER_DOZ.fmg(  47, 778):���������� >,c2-6
      L_asi=L_osi
C KONTEYNER_DOZ.fmg( 280, 676):������,TAKE10
      if(L_osi) then
         R_uxad=R_uri
      endif
C KONTEYNER_DOZ.fmg( 290, 654):���� � ������������� �������
      L0_ovad=R_ebed.gt.R_uvad
C KONTEYNER_DOZ.fmg(  47, 830):���������� >,c1-7
      L0_efe=R_ebed.lt.R_ife
C KONTEYNER_DOZ.fmg( 224, 564):���������� <,c11-7
      L0_emad=R_ebed.gt.R_imad
C KONTEYNER_DOZ.fmg(  47, 772):���������� >,c2-7
      L0_oru=R_ebed.gt.R_uru
C KONTEYNER_DOZ.fmg(  47, 664):���������� >,c4-7
      L0_ibad=R_ebed.gt.R_obad
C KONTEYNER_DOZ.fmg(  47, 718):���������� >,c3-7
      L_erad=L_isad
C KONTEYNER_DOZ.fmg(  95, 866):������,TAKE1
      if(L_isad) then
         R_uxad=R_arad
      endif
C KONTEYNER_DOZ.fmg( 104, 844):���� � ������������� �������
      L_afad=L_ekad
C KONTEYNER_DOZ.fmg( 100, 807):������,TAKE2
      if(L_ekad) then
         R_uxad=R_udad
      endif
C KONTEYNER_DOZ.fmg( 110, 784):���� � ������������� �������
      L_ixi=L_abo
C KONTEYNER_DOZ.fmg( 280, 716):������,TAKE9
      if(L_abo) then
         R_uxad=R_exi
      endif
C KONTEYNER_DOZ.fmg( 290, 694):���� � ������������� �������
      L_ufo=L_iko
C KONTEYNER_DOZ.fmg( 280, 755):������,TAKE8
      if(L_iko) then
         R_uxad=R_ofo
      endif
C KONTEYNER_DOZ.fmg( 290, 733):���� � ������������� �������
      L_epo=L_upo
C KONTEYNER_DOZ.fmg( 280, 794):������,TAKE7
      if(L_upo) then
         R_uxad=R_apo
      endif
C KONTEYNER_DOZ.fmg( 290, 772):���� � ������������� �������
      L_oto=L_evo
C KONTEYNER_DOZ.fmg( 280, 832):������,TAKE6
      if(L_evo) then
         R_uxad=R_ito
      endif
C KONTEYNER_DOZ.fmg( 290, 810):���� � ������������� �������
      L_adu=L_odu
C KONTEYNER_DOZ.fmg( 280, 872):������,TAKE5
      if(L_odu) then
         R_uxad=R_ubu
      endif
C KONTEYNER_DOZ.fmg( 290, 850):���� � ������������� �������
      L_ilu=L_omu
C KONTEYNER_DOZ.fmg( 100, 699):������,TAKE4
      if(L_omu) then
         R_uxad=R_elu
      endif
C KONTEYNER_DOZ.fmg( 110, 677):���� � ������������� �������
      L_etu=L_ivu
C KONTEYNER_DOZ.fmg( 100, 754):������,TAKE3
      if(L_ivu) then
         R_uxad=R_atu
      endif
C KONTEYNER_DOZ.fmg( 110, 732):���� � ������������� �������
      L0_ofe=R_iri.gt.R_ufe
C KONTEYNER_DOZ.fmg( 224, 570):���������� >,c11-6
      L0_ale = L_ide.AND.L0_ofe.AND.L0_ofe
C KONTEYNER_DOZ.fmg( 259, 572):�
      if(L0_ale) then
         R_uxad=R_eke
      endif
C KONTEYNER_DOZ.fmg( 290, 580):���� � ������������� �������
      L0_ude=R_uxad.gt.R_afe
C KONTEYNER_DOZ.fmg( 224, 558):���������� >,c11-8
      L0_ode = L_ide.AND.L0_ofe.AND.L0_efe.AND.L0_ude.AND.
     &(.NOT.L_ede)
C KONTEYNER_DOZ.fmg( 259, 564):�
      L0_eru=R_uxad.gt.R_iru
C KONTEYNER_DOZ.fmg(  47, 658):���������� >,c4-8
      L_umu = L_omu.AND.L0_asu.AND.L0_oru.AND.L0_eru
C KONTEYNER_DOZ.fmg(  78, 664):�
      L0_abad=R_uxad.gt.R_ebad
C KONTEYNER_DOZ.fmg(  47, 712):���������� >,c3-8
      L_ovu = L_ivu.AND.L0_ubad.AND.L0_ibad.AND.L0_abad
C KONTEYNER_DOZ.fmg(  78, 720):�
      L0_ulad=R_uxad.gt.R_amad
C KONTEYNER_DOZ.fmg(  47, 766):���������� >,c2-8
      L_ikad = L_ekad.AND.L0_omad.AND.L0_emad.AND.L0_ulad
C KONTEYNER_DOZ.fmg(  78, 772):�
      L0_evad=R_uxad.gt.R_ivad
C KONTEYNER_DOZ.fmg(  47, 824):���������� >,c1-8
      L_osad = L_isad.AND.L0_axad.AND.L0_ovad.AND.L0_evad
C KONTEYNER_DOZ.fmg(  78, 831):�
      L0_ax = L_osad.OR.L_ikad.OR.L_ovu.OR.L_umu
C KONTEYNER_DOZ.fmg( 216, 517):���
      L0_ex = L_ix.AND.L0_ax
C KONTEYNER_DOZ.fmg( 222, 523):�
      if(L0_ex.and..not.L_u) then
         R_i=R_o
      else
         R_i=max(R0_e-deltat,0.0)
      endif
      L0_ube=R_i.gt.0.0
      L_u=L0_ex
C KONTEYNER_DOZ.fmg( 232, 523):������������  �� T,ftime
      R_axe=R_axe
C KONTEYNER_DOZ.fmg( 269, 502):������,R1VW01
C label 735  try735=try735-1
C sav1=R_axe
C KONTEYNER_DOZ.fmg( 269, 502):recalc:������,R1VW01
C if(sav1.ne.R_axe .and. try735.gt.0) goto 735
      if(L_exe) then
         R_eve=R_axe
      endif
      R_uve=R_eve
C KONTEYNER_DOZ.fmg(  70, 485):�������-��������,wtng_c
      if(L_exe) then
         R_ote=R_axe
      endif
      R_ive=R_ote
C KONTEYNER_DOZ.fmg(  70, 468):�������-��������,wtng_u
      if(L0_ute) then
         R_ave=R_axe
      endif
      R_ove=R_ave
C KONTEYNER_DOZ.fmg(  70, 476):�������-��������,wtng_f
      if(I0_ibe.ne.0) then
        iv1=I0_ibe
        if(0) then
          if(L0_ube.and..not.L_obe) then
            R_abe=R_ux
            R_axe=R_axe+1
          elseif(L0_ode.and..not.L_ade) then
            R_abe=R_ux
            R_axe=R_axe-1
          elseif(L0_ube) then
            R_abe=R_abe-deltat
            if(R_abe.lt.0.0) then
              R_abe=R_ux
              R_axe=R_axe+1
            endif
          elseif(L0_ode) then
            R_abe=R_abe-deltat
            if(R_abe.lt.0.0) then
              R_abe=R_ux
              R_axe=R_axe-1
            endif
          endif
          L_obe=L0_ube
          L_ade=L0_ode
        else
            R_abe=R_ox-R_ebe
            if(L0_ube.and..not.L0_ode) then
               R_axe=R_axe+R_abe*deltat/max(deltat,R_ux)
            elseif(L0_ode.and..not.L0_ube) then
               R_axe=R_axe-R_abe*deltat/max(deltat,R_ux)
            endif   
         endif
         if(0) then
            if(R_axe.gt.R_ox) then
               R_axe=R_ebe
            elseif(R_axe.lt.R_ebe) then
               R_axe=R_ox
            endif
         else
            if(R_axe.gt.R_ox) then
               R_axe=R_ox
            elseif(R_axe.lt.R_ebe) then
               R_axe=R_ebe
            endif
         endif
      endif
C KONTEYNER_DOZ.fmg( 246, 526):��������� ���������,weight
      End
