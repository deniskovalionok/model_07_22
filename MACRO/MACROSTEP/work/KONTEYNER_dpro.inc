      Interface
      Subroutine KONTEYNER_dpro(ext_deltat,R_i,R_o,L_u,L_ad
     &,L_ed,L_id,L_od,L_ud,L_af,L_ef,L_if,L_of,L_uf,L_ak,L_ek
     &,L_ik,L_ok,L_uk,L_al,L_el,L_il,L_ol,L_ul,L_at,L_et,L_it
     &,L_ot,L_ut,L_av,L_ev,L_iv,L_ov,L_uv,L_ix,R_ox,R_ux,R_abe
     &,R_ebe,L_obe,L_ade,L_ede,R_ude,R_efe,R_ofe,L_ufe,R_ake
     &,R_eke,R_ike,L_oke,L_ale,L_ile,R_ame,R_ome,R_ape,R_ipe
     &,R_upe,R_ere,R_ase,R_ose,R_ete,R_ote,R_ute,R_ave,R_eve
     &,R_ive,R_ove,L_uve,L_axe,L_exe,L_ixe,L_oxe,L_uxe,L_abi
     &,L_ebi,L_ibi,L_obi,R_upi,R_ari,L_eri,R_iri,L_ori,L_esi
     &,R_eti,R_uti,R_evi,R_ivi,L_ovi,R_uvi,L_axi,L_oxi,R_obo
     &,R_edo,R_odo,R_udo,L_afo,R_efo,L_ifo,L_ako,R_alo,R_olo
     &,R_amo,R_emo,L_imo,R_omo,L_umo,L_ipo,R_iro,R_aso,R_iso
     &,R_oso,L_uso,R_ato,L_eto,L_uto,R_uvo,R_ixo,R_uxo,R_abu
     &,L_ebu,R_ibu,L_obu,L_edu,R_efu,R_ufu,R_eku,R_iku,L_oku
     &,R_uku,L_alu,L_emu,L_imu,R_umu,R_ipu,R_aru,R_iru,R_uru
     &,R_asu,R_esu,L_isu,R_osu,L_usu,L_avu,L_evu,R_ovu,R_exu
     &,R_uxu,R_ebad,R_obad,R_ubad,R_adad,L_edad,R_idad,L_odad
     &,L_ufad,L_akad,R_ikad,R_alad,R_olad,R_amad,R_imad,R_apad
     &,R_epad,L_ipad,R_opad,L_upad,L_asad,L_esad,R_usad,R_etad
     &,R_avad,R_ivad,R_uvad,R_ixad,R_oxad,R_uxad,R_abed,R_ebed
     &)
C |R_i           |4 4 S|_simpftime*     |[���]���������� ��������� ������������� |0.0|
C |R_o           |4 4 K|_timpftime      |[���]������������ �������� �������������|5|
C |L_u           |1 1 S|_limpftime*     |[TF]���������� ��������� ������������� |F|
C |L_ad          |1 1 S|_splsi10-3*     |[TF]���������� ��������� ������������� |F|
C |L_ed          |1 1 S|_splsi10-2*     |[TF]���������� ��������� ������������� |F|
C |L_id          |1 1 S|_splsi9-3*      |[TF]���������� ��������� ������������� |F|
C |L_od          |1 1 S|_splsi9-2*      |[TF]���������� ��������� ������������� |F|
C |L_ud          |1 1 S|_splsi8-3*      |[TF]���������� ��������� ������������� |F|
C |L_af          |1 1 S|_splsi8-2*      |[TF]���������� ��������� ������������� |F|
C |L_ef          |1 1 S|_splsi7-3*      |[TF]���������� ��������� ������������� |F|
C |L_if          |1 1 S|_splsi7-2*      |[TF]���������� ��������� ������������� |F|
C |L_of          |1 1 S|_splsi6-3*      |[TF]���������� ��������� ������������� |F|
C |L_uf          |1 1 S|_splsi6-2*      |[TF]���������� ��������� ������������� |F|
C |L_ak          |1 1 S|_splsi5-3*      |[TF]���������� ��������� ������������� |F|
C |L_ek          |1 1 S|_splsi5-2*      |[TF]���������� ��������� ������������� |F|
C |L_ik          |1 1 S|_splsi4-3*      |[TF]���������� ��������� ������������� |F|
C |L_ok          |1 1 S|_splsi4-2*      |[TF]���������� ��������� ������������� |F|
C |L_uk          |1 1 S|_splsi3-3*      |[TF]���������� ��������� ������������� |F|
C |L_al          |1 1 S|_splsi3-2*      |[TF]���������� ��������� ������������� |F|
C |L_el          |1 1 S|_splsi2-3*      |[TF]���������� ��������� ������������� |F|
C |L_il          |1 1 S|_splsi2-2*      |[TF]���������� ��������� ������������� |F|
C |L_ol          |1 1 S|_splsi1-3*      |[TF]���������� ��������� ������������� |F|
C |L_ul          |1 1 S|_splsi1-2*      |[TF]���������� ��������� ������������� |F|
C |L_at          |1 1 I|FDA20TRAN01_C2_CBC|������ ������||
C |L_et          |1 1 I|FDA20TRAN02_C2_CBC|������ ������||
C |L_it          |1 1 I|FDA20TRAN03_C2_CBC|������ ������||
C |L_ot          |1 1 I|FDA20TRAN04_C2_CBC|������ ������||
C |L_ut          |1 1 I|FDA20TRAN05_C2_CBC|������ ������||
C |L_av          |1 1 I|FDA20TRAN06_C2_CBC|������ ������||
C |L_ev          |1 1 I|FDA20TRAN07_C2_CBC|������ ������||
C |L_iv          |1 1 I|FDA20TRAN08_C2_CBC|������ ������||
C |L_ov          |1 1 I|FDA20TRAN09_C2_CBC|������ ������||
C |L_uv          |1 1 I|FDA20TRAN10_C2_CBC|������ ������||
C |L_ix          |1 1 I|FDA20UNL01      |�������||
C |R_ox          |4 4 K|_utunweight     |����������� ������ ��������� ������|11.0|
C |R_ux          |4 4 K|_ttunweight     |������ ����� ��������� �������|21|
C |R_abe         |4 4 S|_stunweight*    |��������� ����������  ||
C |R_ebe         |4 4 K|_ltunweight     |����������� ������ ��������� �����|7.0|
C |L_obe         |1 1 S|_uc0tunweight*  |��������� ����� ���������� |F|
C |L_ade         |1 1 S|_lc0tunweight*  |��������� ����� ���������� |F|
C |L_ede         |1 1 I|FDA20KANT01_C10XH54|��������� ����������||
C |R_ude         |4 4 K|_lcmpc11-8      |[]�������� ������ �����������|0.1|
C |R_efe         |4 4 K|_lcmpc11-7      |[]�������� ������ �����������|600|
C |R_ofe         |4 4 K|_lcmpc11-6      |[]�������� ������ �����������|1950|
C |L_ufe         |1 1 S|_splsi11-1*     |[TF]���������� ��������� ������������� |F|
C |R_ake         |4 4 I|FDA20KANT01_C1_MFVAL|��������� ����� ����������||
C |R_eke         |4 4 I|FDA20KANT01VZ01 |���������� ����������� �� OZ||
C |R_ike         |4 4 I|FDA20KANT01VX01 |���������� ����������� �� OX||
C |L_oke         |1 1 O|TAKE11          |��������� �������� � �����������||
C |L_ale         |1 1 S|_qfftr11*       |�������� ������ Q RS-��������  |F|
C |L_ile         |1 1 I|FDA20KANT01_C2_CBC|������ ������||
C |R_ame         |4 4 K|_lcmpc11-3      |[]�������� ������ �����������|5|
C |R_ome         |4 4 K|_lcmpc11-1      |[]�������� ������ �����������|5|
C |R_ape         |4 4 S|_memadd_t4*     |����������� �������� ������� ||
C |R_ipe         |4 4 S|_memadd_t3*     |����������� �������� ������� ||
C |R_upe         |4 4 S|_memadd_t1*     |����������� �������� ������� ||
C |R_ere         |4 4 I|START_T         |||
C |R_ase         |4 4 S|_memadd_t2*     |����������� �������� ������� ||
C |R_ose         |4 4 O|R1VT01          |����������� ����������||
C |R_ete         |4 4 S|_memwtng_u*     |����������� �������� ������� ||
C |R_ote         |4 4 S|_memwtng_f*     |����������� �������� ������� ||
C |R_ute         |4 4 S|_memwtng_c*     |����������� �������� ������� ||
C |R_ave         |4 4 O|R1VW01_u        |����� ���������� ����� �������||
C |R_eve         |4 4 O|R1VW01_f        |����� ���������� ����� �������||
C |R_ive         |4 4 O|R1VW01_c        |����� ���������� �������||
C |R_ove         |4 4 O|R1VW01          |����� ����������|`START_W`|
C |L_uve         |1 1 O|INABOX01        |� ����������� �����||
C |L_axe         |1 1 O|INABOX10        |� ������������� �����||
C |L_exe         |1 1 O|INABOX09        |� ����� ���������� 3||
C |L_ixe         |1 1 O|INABOX08        |� ����� ���������� 2||
C |L_oxe         |1 1 O|INABOX07        |� ����� ���������� 1||
C |L_uxe         |1 1 O|INABOX06        |� ����� ����������||
C |L_abi         |1 1 O|INABOX05        |� ����� �������� �������||
C |L_ebi         |1 1 O|INABOX04        |� ����� �������� ������||
C |L_ibi         |1 1 O|INABOX03        |� ����� �������� ��������||
C |L_obi         |1 1 O|INABOX02        |� ����� �������� �������� ����� ���������||
C |R_upi         |4 4 K|_lcmpc1-0       |[]�������� ������ �����������|20000|
C |R_ari         |4 4 O|VY01            |��������� ���������� �� OY|0|
C |L_eri         |1 1 S|_splsi10-1*     |[TF]���������� ��������� ������������� |F|
C |R_iri         |4 4 I|FDA20TRAN10_C1_MFVAL|��������� �����||
C |L_ori         |1 1 O|TAKE10          |��������� �������� � ����������� 10||
C |L_esi         |1 1 S|_qfftr10*       |�������� ������ Q RS-��������  |F|
C |R_eti         |4 4 K|_lcmpc10-3      |[]�������� ������ �����������|100|
C |R_uti         |4 4 K|_lcmpc10-1      |[]�������� ������ �����������|100|
C |R_evi         |4 4 I|FDA20TRAN10VZ01 |���������� ������������ �� OZ||
C |R_ivi         |4 4 I|FDA20TRAN10VX01 |���������� ������������ �� OX||
C |L_ovi         |1 1 S|_splsi9-1*      |[TF]���������� ��������� ������������� |F|
C |R_uvi         |4 4 I|FDA20TRAN09_C1_MFVAL|��������� �����||
C |L_axi         |1 1 O|TAKE9           |��������� �������� � ����������� 9||
C |L_oxi         |1 1 S|_qfftr9*        |�������� ������ Q RS-��������  |F|
C |R_obo         |4 4 K|_lcmpc9-3       |[]�������� ������ �����������|100|
C |R_edo         |4 4 K|_lcmpc9-1       |[]�������� ������ �����������|100|
C |R_odo         |4 4 I|FDA20TRAN09VZ01 |���������� ������������ �� OZ||
C |R_udo         |4 4 I|FDA20TRAN09VX01 |���������� ������������ �� OX||
C |L_afo         |1 1 S|_splsi8-1*      |[TF]���������� ��������� ������������� |F|
C |R_efo         |4 4 I|FDA20TRAN08_C1_MFVAL|��������� �����||
C |L_ifo         |1 1 O|TAKE8           |��������� �������� � ����������� 8||
C |L_ako         |1 1 S|_qfftr8*        |�������� ������ Q RS-��������  |F|
C |R_alo         |4 4 K|_lcmpc8-3       |[]�������� ������ �����������|100|
C |R_olo         |4 4 K|_lcmpc8-1       |[]�������� ������ �����������|100|
C |R_amo         |4 4 I|FDA20TRAN08VZ01 |���������� ������������ �� OZ||
C |R_emo         |4 4 I|FDA20TRAN08VX01 |���������� ������������ �� OX||
C |L_imo         |1 1 S|_splsi7-1*      |[TF]���������� ��������� ������������� |F|
C |R_omo         |4 4 I|FDA20TRAN07_C1_MFVAL|��������� �����||
C |L_umo         |1 1 O|TAKE7           |��������� �������� � ����������� 7||
C |L_ipo         |1 1 S|_qfftr7*        |�������� ������ Q RS-��������  |F|
C |R_iro         |4 4 K|_lcmpc7-3       |[]�������� ������ �����������|100|
C |R_aso         |4 4 K|_lcmpc7-1       |[]�������� ������ �����������|100|
C |R_iso         |4 4 I|FDA20TRAN07VZ01 |���������� ������������ �� OZ||
C |R_oso         |4 4 I|FDA20TRAN07VX01 |���������� ������������ �� OX||
C |L_uso         |1 1 S|_splsi6-1*      |[TF]���������� ��������� ������������� |F|
C |R_ato         |4 4 I|FDA20TRAN06_C1_MFVAL|��������� �����||
C |L_eto         |1 1 O|TAKE6           |��������� �������� � ����������� 6||
C |L_uto         |1 1 S|_qfftr6*        |�������� ������ Q RS-��������  |F|
C |R_uvo         |4 4 K|_lcmpc6-3       |[]�������� ������ �����������|100|
C |R_ixo         |4 4 K|_lcmpc6-1       |[]�������� ������ �����������|100|
C |R_uxo         |4 4 I|FDA20TRAN06VZ01 |���������� ������������ �� OZ||
C |R_abu         |4 4 I|FDA20TRAN06VX01 |���������� ������������ �� OX||
C |L_ebu         |1 1 S|_splsi5-1*      |[TF]���������� ��������� ������������� |F|
C |R_ibu         |4 4 I|FDA20TRAN05_C1_MFVAL|��������� �����||
C |L_obu         |1 1 O|TAKE5           |��������� �������� � ����������� 5||
C |L_edu         |1 1 S|_qfftr5*        |�������� ������ Q RS-��������  |F|
C |R_efu         |4 4 K|_lcmpc5-3       |[]�������� ������ �����������|100|
C |R_ufu         |4 4 K|_lcmpc5-1       |[]�������� ������ �����������|100|
C |R_eku         |4 4 I|FDA20TRAN05VZ01 |���������� ������������ �� OZ||
C |R_iku         |4 4 I|FDA20TRAN05VX01 |���������� ������������ �� OX||
C |L_oku         |1 1 S|_splsi4-1*      |[TF]���������� ��������� ������������� |F|
C |R_uku         |4 4 I|FDA20TRAN04_C1_MFVAL|��������� �����||
C |L_alu         |1 1 O|TAKE4           |��������� �������� � ����������� 4||
C |L_emu         |1 1 S|_qff4*          |�������� ������ Q RS-��������  |F|
C |L_imu         |1 1 O|FULL4           |��������� ����� � ������� PU||
C |R_umu         |4 4 K|_lcmpc4-3       |[]�������� ������ �����������|100|
C |R_ipu         |4 4 K|_lcmpc4-1       |[]�������� ������ �����������|100|
C |R_aru         |4 4 K|_lcmpc4-8       |[]�������� ������ �����������|0.1|
C |R_iru         |4 4 K|_lcmpc4-7       |[]�������� ������ �����������|480|
C |R_uru         |4 4 K|_lcmpc4-6       |[]�������� ������ �����������|7950|
C |R_asu         |4 4 I|FDA20TRAN04VZ01 |���������� ������������ �� OZ||
C |R_esu         |4 4 I|FDA20TRAN04VX01 |���������� ������������ �� OX||
C |L_isu         |1 1 S|_splsi3-1*      |[TF]���������� ��������� ������������� |F|
C |R_osu         |4 4 I|FDA20TRAN03_C1_MFVAL|��������� �����||
C |L_usu         |1 1 O|TAKE3           |��������� �������� � ����������� 3||
C |L_avu         |1 1 S|_qfftr3*        |�������� ������ Q RS-��������  |F|
C |L_evu         |1 1 O|FULL3           |��������� ����� � ������� ������||
C |R_ovu         |4 4 K|_lcmpc3-3       |[]�������� ������ �����������|100|
C |R_exu         |4 4 K|_lcmpc3-1       |[]�������� ������ �����������|100|
C |R_uxu         |4 4 K|_lcmpc3-8       |[]�������� ������ �����������|0.1|
C |R_ebad        |4 4 K|_lcmpc3-7       |[]�������� ������ �����������|480|
C |R_obad        |4 4 K|_lcmpc3-6       |[]�������� ������ �����������|5950|
C |R_ubad        |4 4 I|FDA20TRAN03VZ01 |���������� ������������ �� OZ||
C |R_adad        |4 4 I|FDA20TRAN03VX01 |���������� ������������ �� OX||
C |L_edad        |1 1 S|_splsi2-1*      |[TF]���������� ��������� ������������� |F|
C |R_idad        |4 4 I|FDA20TRAN02_C1_MFVAL|��������� �����||
C |L_odad        |1 1 O|TAKE2           |��������� �������� � ����������� 2||
C |L_ufad        |1 1 S|_qfftr2*        |�������� ������ Q RS-��������  |F|
C |L_akad        |1 1 O|FULL2           |��������� ����� � ������� ��������||
C |R_ikad        |4 4 K|_lcmpc2-3       |[]�������� ������ �����������|100|
C |R_alad        |4 4 K|_lcmpc2-1       |[]�������� ������ �����������|100|
C |R_olad        |4 4 K|_lcmpc2-8       |[]�������� ������ �����������|0.1|
C |R_amad        |4 4 K|_lcmpc2-7       |[]�������� ������ �����������|480|
C |R_imad        |4 4 K|_lcmpc2-6       |[]�������� ������ �����������|3950|
C |R_apad        |4 4 I|FDA20TRAN02VZ01 |���������� ������������ �� OZ||
C |R_epad        |4 4 I|FDA20TRAN02VX01 |���������� ������������ �� OX||
C |L_ipad        |1 1 S|_splsi1-1*      |[TF]���������� ��������� ������������� |F|
C |R_opad        |4 4 I|FDA20TRAN01_C1_MFVAL|��������� �����||
C |L_upad        |1 1 O|TAKE1           |��������� �������� � ����������� 1||
C |L_asad        |1 1 S|_qfftr1*        |�������� ������ Q RS-��������  |F|
C |L_esad        |1 1 O|FULL1           |��������� ����� � ������� �������� �����||
C |R_usad        |4 4 K|_lcmpc1-3       |[]�������� ������ �����������|100|
C |R_etad        |4 4 K|_lcmpc1-1       |[]�������� ������ �����������|100|
C |R_avad        |4 4 K|_lcmpc1-8       |[]�������� ������ �����������|0.1|
C |R_ivad        |4 4 K|_lcmpc1-7       |[]�������� ������ �����������|480|
C |R_uvad        |4 4 K|_lcmpc1-6       |[]�������� ������ �����������|1950|
C |R_ixad        |4 4 O|VV01            |��������� ����� ����������||
C |R_oxad        |4 4 I|FDA20TRAN01VZ01 |���������� ������������ �� OZ||
C |R_uxad        |4 4 O|VZ01            |��������� ���������� �� OZ|`START_Z`|
C |R_abed        |4 4 I|FDA20TRAN01VX01 |���������� ������������ �� OX||
C |R_ebed        |4 4 O|VX01            |��������� ���������� �� OX|`START_X`|

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_i,R_o
      LOGICAL*1 L_u,L_ad,L_ed,L_id,L_od,L_ud,L_af,L_ef,L_if
     &,L_of,L_uf,L_ak,L_ek,L_ik,L_ok,L_uk,L_al,L_el,L_il,L_ol
     &,L_ul,L_at
      LOGICAL*1 L_et,L_it,L_ot,L_ut,L_av,L_ev,L_iv,L_ov,L_uv
     &,L_ix
      REAL*4 R_ox,R_ux,R_abe,R_ebe
      LOGICAL*1 L_obe,L_ade,L_ede
      REAL*4 R_ude,R_efe,R_ofe
      LOGICAL*1 L_ufe
      REAL*4 R_ake,R_eke,R_ike
      LOGICAL*1 L_oke,L_ale,L_ile
      REAL*4 R_ame,R_ome,R_ape,R_ipe,R_upe,R_ere,R_ase,R_ose
     &,R_ete,R_ote,R_ute,R_ave,R_eve,R_ive,R_ove
      LOGICAL*1 L_uve,L_axe,L_exe,L_ixe,L_oxe,L_uxe,L_abi
     &,L_ebi,L_ibi,L_obi
      REAL*4 R_upi,R_ari
      LOGICAL*1 L_eri
      REAL*4 R_iri
      LOGICAL*1 L_ori,L_esi
      REAL*4 R_eti,R_uti,R_evi,R_ivi
      LOGICAL*1 L_ovi
      REAL*4 R_uvi
      LOGICAL*1 L_axi,L_oxi
      REAL*4 R_obo,R_edo,R_odo,R_udo
      LOGICAL*1 L_afo
      REAL*4 R_efo
      LOGICAL*1 L_ifo,L_ako
      REAL*4 R_alo,R_olo,R_amo,R_emo
      LOGICAL*1 L_imo
      REAL*4 R_omo
      LOGICAL*1 L_umo,L_ipo
      REAL*4 R_iro,R_aso,R_iso,R_oso
      LOGICAL*1 L_uso
      REAL*4 R_ato
      LOGICAL*1 L_eto,L_uto
      REAL*4 R_uvo,R_ixo,R_uxo,R_abu
      LOGICAL*1 L_ebu
      REAL*4 R_ibu
      LOGICAL*1 L_obu,L_edu
      REAL*4 R_efu,R_ufu,R_eku,R_iku
      LOGICAL*1 L_oku
      REAL*4 R_uku
      LOGICAL*1 L_alu,L_emu,L_imu
      REAL*4 R_umu,R_ipu,R_aru,R_iru,R_uru,R_asu,R_esu
      LOGICAL*1 L_isu
      REAL*4 R_osu
      LOGICAL*1 L_usu,L_avu,L_evu
      REAL*4 R_ovu,R_exu,R_uxu,R_ebad,R_obad,R_ubad,R_adad
      LOGICAL*1 L_edad
      REAL*4 R_idad
      LOGICAL*1 L_odad,L_ufad,L_akad
      REAL*4 R_ikad,R_alad,R_olad,R_amad,R_imad,R_apad,R_epad
      LOGICAL*1 L_ipad
      REAL*4 R_opad
      LOGICAL*1 L_upad,L_asad,L_esad
      REAL*4 R_usad,R_etad,R_avad,R_ivad,R_uvad,R_ixad,R_oxad
     &,R_uxad,R_abed,R_ebed
      End subroutine KONTEYNER_dpro
      End interface
