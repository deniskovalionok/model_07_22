      Interface
      Subroutine DAT_SAS_2_HANDLER(ext_deltat,R_ed,R_id,R_af
     &,R_ef,R_ek,R_ik,I_il,R_ul,L_am,R_im,L_om,L_um,R_ep,R_ip
     &,R_up,L_er)
C |R_ed          |4 4 I|koeff_units     |����������� �����������||
C |R_id          |4 4 I|input           |����||
C |R_af          |4 4 I|LOWER_warning_setpoint|||
C |R_ef          |4 4 I|LOWER_alarm_setpoint|||
C |R_ek          |4 4 I|UPPER_alarm_setpoint|||
C |R_ik          |4 4 I|UPPER_warning_setpoint|||
C |I_il          |2 4 O|dat_color       |���� ��������||
C |R_ul          |4 4 I|GM05V           |���������� ����������� �������||
C |L_am          |1 1 I|GM05            |���������� ����������� �������||
C |R_im          |4 4 I|GM04V           |������ �������� ���������� ��������||
C |L_om          |1 1 I|GM04            |������ �������� ���������� ��������||
C |L_um          |1 1 I|GM02            |����� �� ������������ ������ ���������||
C |R_ep          |4 4 O|KKS             |�����||
C |R_ip          |4 4 I|MAXIMUM         |||
C |R_up          |4 4 I|MINIMUM         |||
C |L_er          |1 1 I|GM01            |����� �� ����������� ������ ���������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      REAL*4 R_ed,R_id,R_af,R_ef,R_ek,R_ik
      INTEGER*4 I_il
      REAL*4 R_ul
      LOGICAL*1 L_am
      REAL*4 R_im
      LOGICAL*1 L_om,L_um
      REAL*4 R_ep,R_ip,R_up
      LOGICAL*1 L_er
      End subroutine DAT_SAS_2_HANDLER
      End interface
