      Subroutine REG_RAS_MAN(ext_deltat,L_e,L_u,R_of,R_ik
     &,R_al,R_el,R_il,R_ol,R_um,R_ip,R_op,R_up,R_ar,R_er,R_ir
     &,L_ur,R_as,R_es,L_os,L_at,L_et,I_iv,R_uv,R_ax,R_ex,R8_ix
     &,R_ox,R_ebe,R_ibe,R_obe,R_ube,R_ade)
C |L_e           |1 1 I|feedback        |||
C |L_u           |1 1 I|flagmod         |����� ������� �����||
C |R_of          |4 4 I|k4              |�-� ����������� ��|0|
C |R_ik          |4 4 I|t3              |���������� ������� ���������������|1.0|
C |R_al          |4 4 S|_sdif1*         |���������� ��������� |0.0|
C |R_el          |4 4 I|u1              |����������� �����������|1.0|
C |R_il          |4 4 O|dlt             |������� ��������� ����������||
C |R_ol          |4 4 O|_odif1*         |�������� ������ |0.0|
C |R_um          |4 4 S|_oint1*         |����� ����������� |0.0|
C |R_ip          |4 4 S|turn_on_ST*     |��������� ������ "--" |0.0|
C |R_op          |4 4 I|turn_on         |������� ������ ������ "--" |0.0|
C |R_up          |4 4 S|vmopen_ST*      |��������� ������ "--" |0.0|
C |R_ar          |4 4 I|vmopen          |������� ������ ������ "--" |0.0|
C |R_er          |4 4 S|vmclose_ST*     |��������� ������ "--" |0.0|
C |R_ir          |4 4 I|vmclose         |������� ������ ������ "--" |0.0|
C |L_ur          |1 1 O|turn_on_CMD*    |[TF]����� ������ --|F|
C |R_as          |4 4 K|_cJ898          |�������� ��������� ����������|1.0|
C |R_es          |4 4 K|_cJ897          |�������� ��������� ����������|0.0|
C |L_os          |1 1 S|_qffJ901*       |�������� ������ Q RS-��������  |F|
C |L_at          |1 1 O|vmopen_CMD*     |[TF]����� ������ --|F|
C |L_et          |1 1 O|vmclose_CMD*    |[TF]����� ������ --|F|
C |I_iv          |2 4 O|LS              |||
C |R_uv          |4 4 K|_cJ733          |�������� ��������� ����������|10.0|
C |R_ax          |4 4 O|out             |����� ��������� ��������||
C |R_ex          |4 4 O|_oaprJ896*      |�������� ������ ���������. ����� |0.0|
C |R8_ix         |4 8 O|pos             |||
C |R_ox          |4 4 I|norma           |||
C |R_ebe         |4 4 I|CP              |||
C |R_ibe         |4 4 I|setpoint        |||
C |R_obe         |4 4 I|Kd              |||
C |R_ube         |4 4 I|Ki              ||0.1|
C |R_ade         |4 4 I|Kp              ||1.0|

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      LOGICAL*1 L_e
      REAL*4 R0_i,R0_o
      LOGICAL*1 L_u,L0_ad
      REAL*4 R0_ed
      LOGICAL*1 L0_id
      REAL*4 R0_od,R0_ud,R0_af,R0_ef,R0_if,R_of,R0_uf,R0_ak
     &,R0_ek,R_ik
      LOGICAL*1 L0_ok
      REAL*4 R0_uk,R_al,R_el,R_il,R_ol,R0_ul,R0_am,R0_em,R0_im
      LOGICAL*1 L0_om
      REAL*4 R_um,R0_ap,R0_ep,R_ip,R_op,R_up,R_ar,R_er,R_ir
      LOGICAL*1 L0_or,L_ur
      REAL*4 R_as,R_es,R0_is
      LOGICAL*1 L_os
      REAL*4 R0_us
      LOGICAL*1 L_at,L_et
      REAL*4 R0_it
      LOGICAL*1 L0_ot,L0_ut
      INTEGER*4 I0_av,I0_ev,I_iv
      REAL*4 R0_ov,R_uv,R_ax,R_ex
      REAL*8 R8_ix
      REAL*4 R_ox,R0_ux,R0_abe,R_ebe,R_ibe,R_obe,R_ube,R_ade

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_uk=R_al
C REG_RAS_MAN.fmg( 144, 232):pre: ���������������� ����� ,1
      R0_us=R_ex
C REG_RAS_MAN.fmg( 206, 212):pre: �������������� ����� (�����������)
      R0_i = (-R_ibe) + R_ebe
C REG_RAS_MAN.fmg(  42, 278):��������
      R0_o = R_ibe + (-R_ebe)
C REG_RAS_MAN.fmg(  42, 271):��������
      if(L_e) then
         R0_ux=R0_o
      else
         R0_ux=R0_i
      endif
C REG_RAS_MAN.fmg(  49, 274):���� RE IN LO CH7
      if(R_ox.ge.0.0) then
         R0_abe=R0_ux/max(R_ox,1.0e-10)
      else
         R0_abe=R0_ux/min(R_ox,-1.0e-10)
      endif
C REG_RAS_MAN.fmg(  58, 262):�������� ����������
      R0_em = 100
C REG_RAS_MAN.fmg( 134, 279):��������� (RE4) (�������)
      R0_ap = -(R0_em)
C REG_RAS_MAN.fmg( 146, 272):��������
      R0_ed = 0.001
C REG_RAS_MAN.fmg( 174, 247):��������� (RE4) (�������)
      R0_od = 0.999
C REG_RAS_MAN.fmg( 174, 251):��������� (RE4) (�������)
      R0_ud = 0.000001
C REG_RAS_MAN.fmg( 101, 269):��������� (RE4) (�������)
      L0_ok=.false.
C REG_RAS_MAN.fmg( 138, 228):��������� ���������� (�������)
      R0_af = 1
C REG_RAS_MAN.fmg( 162, 262):��������� (RE4) (�������)
      R0_ef = 0
C REG_RAS_MAN.fmg( 162, 256):��������� (RE4) (�������)
      R0_im = 1
C REG_RAS_MAN.fmg( 136, 267):��������� (RE4) (�������)
      L_ur=R_op.ne.R_ip
      R_ip=R_op
C REG_RAS_MAN.fmg( 152, 166):���������� ������������� ������
      L_at=R_ar.ne.R_up
      R_up=R_ar
C REG_RAS_MAN.fmg( 152, 178):���������� ������������� ������
      L_et=R_ir.ne.R_er
      R_er=R_ir
C REG_RAS_MAN.fmg( 152, 189):���������� ������������� ������
      L0_or = L_et.OR.L_at
C REG_RAS_MAN.fmg( 223, 188):���
      L_os=L0_or.or.(L_os.and..not.(L_ur))
      L0_ut=.not.L_os
C REG_RAS_MAN.fmg( 236, 186):RS �������
      L0_ot =.NOT.(L0_ut)
C REG_RAS_MAN.fmg( 270, 184):���
      !��������� R0_is = REG_RAS_MANC?? /1.0/
      R0_is=R_as
C REG_RAS_MAN.fmg( 204, 220):���������
      !��������� R0_it = REG_RAS_MANC?? /0.0/
      R0_it=R_es
C REG_RAS_MAN.fmg( 202, 220):���������
      I0_av = z'00C0C0C0'
C REG_RAS_MAN.fmg( 252, 190):��������� ������������� IN (�������)
      I0_ev = z'0000FF00'
C REG_RAS_MAN.fmg( 252, 188):��������� ������������� IN (�������)
      if(L0_ut) then
         I_iv=I0_ev
      else
         I_iv=I0_av
      endif
C REG_RAS_MAN.fmg( 258, 188):���� RE IN LO CH7
      !��������� R0_ov = REG_RAS_MANC?? /10.0/
      R0_ov=R_uv
C REG_RAS_MAN.fmg( 200, 220):���������
      R0_ul = R_ade * R_il
C REG_RAS_MAN.fmg( 102, 288):����������
C label 56  try56=try56-1
      if(L_at) then
         R_ex=R_ex+deltat/R0_ov
      else if(L_et) then
         R_ex=R_ex-deltat/R0_ov
      else if(.not.L_os) then
         R_ex=(R0_ov*R0_us+deltat*R_ax)/(R0_ov+deltat)
      endif
      if(R_ex.gt.R0_is) then
         R_ex=R0_is
      endif
      if(R_ex.lt.R0_it) then
         R_ex=R0_it
      endif
C REG_RAS_MAN.fmg( 206, 212):�������������� ����� (�����������)
      if(R_il.gt.R0_ud) then
         R0_ek=R_il-R0_ud
      elseif(R_il.le.-R0_ud) then
         R0_ek=R_il+R0_ud
      else
         R0_ek=0.0
      endif
C REG_RAS_MAN.fmg( 104, 262):���� ������������������
      R0_ep = R_ube * R0_ek
C REG_RAS_MAN.fmg( 122, 260):����������
      if(L0_ok) then
         R_ol=0.0
         R_al=R_il
      else
         R_ol=(R_obe*(R_il-R0_uk)+R_ik*R_ol)/(R_ik+deltat
     &)
         R_al=R_il
      endif
C REG_RAS_MAN.fmg( 144, 232):���������������� ����� ,1
      R0_am = R0_ul + R_um + R_ol
C REG_RAS_MAN.fmg( 158, 260):��������
      R0_if=MIN(R0_af,R0_am)
C REG_RAS_MAN.fmg( 167, 262):�������
      R0_uf=MAX(R0_if,R0_ef)
C REG_RAS_MAN.fmg( 167, 258):��������
      R0_ak = R_of * R0_uf
C REG_RAS_MAN.fmg(  64, 254):����������
      R_il = R0_abe + (-R0_ak)
C REG_RAS_MAN.fmg(  70, 262):��������
C sav1=R_ol
      if(L0_ok) then
         R_ol=0.0
         R_al=R_il
      else
         R_ol=(R_obe*(R_il-R0_uk)+R_ik*R_ol)/(R_ik+deltat
     &)
         R_al=R_il
      endif
C REG_RAS_MAN.fmg( 144, 232):recalc:���������������� ����� ,1
C if(sav1.ne.R_ol .and. try68.gt.0) goto 68
C sav1=R0_ul
      R0_ul = R_ade * R_il
C REG_RAS_MAN.fmg( 102, 288):recalc:����������
C if(sav1.ne.R0_ul .and. try56.gt.0) goto 56
      if(L_u) then
         R_ax=R_ax
      else
         R_ax=R0_uf
      endif
C REG_RAS_MAN.fmg( 208, 256):���� RE IN LO CH7
C sav1=R_ex
      if(L_at) then
         R_ex=R_ex+deltat/R0_ov
      else if(L_et) then
         R_ex=R_ex-deltat/R0_ov
      else if(.not.L_os) then
         R_ex=(R0_ov*R0_us+deltat*R_ax)/(R0_ov+deltat)
      endif
      if(R_ex.gt.R0_is) then
         R_ex=R0_is
      endif
      if(R_ex.lt.R0_it) then
         R_ex=R0_it
      endif
C REG_RAS_MAN.fmg( 206, 212):recalc:�������������� ����� (�����������)
C if(sav1.ne.R_ex .and. try58.gt.0) goto 58
      L0_id=R0_uf.gt.R0_od
C REG_RAS_MAN.fmg( 178, 252):���������� >
      L0_ad=R0_uf.lt.R0_ed
C REG_RAS_MAN.fmg( 178, 248):���������� <
      L0_om = L0_id.OR.L0_ad
C REG_RAS_MAN.fmg( 184, 250):���
      if(L0_ot) then
         R_um=R_ex
      elseif(L0_om) then
         R_um=R_um
      else
         R_um=R_um+deltat/R0_im*R0_ep
      endif
      if(R_um.gt.R0_em) then
         R_um=R0_em
      elseif(R_um.lt.R0_ap) then
         R_um=R0_ap
      endif
C REG_RAS_MAN.fmg( 144, 260):����������,1
C sav1=R0_am
      R0_am = R0_ul + R_um + R_ol
C REG_RAS_MAN.fmg( 158, 260):recalc:��������
C if(sav1.ne.R0_am .and. try70.gt.0) goto 70
      R8_ix = R_ex
C REG_RAS_MAN.fmg( 232, 212):��������
      End
