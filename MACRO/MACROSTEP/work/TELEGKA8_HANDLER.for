      Subroutine TELEGKA8_HANDLER(ext_deltat,R_ubi,R_i,R_o
     &,L_u,R_ad,R_af,R_ak,R_ik,R_ok,R_uk,R_al,L_or,R_us,R_uv
     &,R_ax,L_ex,R_ox,R_ux,L_abe,R_ibe,L_obe,R_ade,L_ede,L_ife
     &,L_ake,R_ame,R_ume,L_epe,R_ope,L_are,L_ire,L_ise,L_use
     &,L_ate,L_ite,L_ave,L_ove,L_axe,R_adi,L_odi,L_afi,L_ifi
     &,L_ofi,L_eki,L_iki,L_uli,L_emi,C20_api,L_epi,L_opi,L_upi
     &,L_iri,L_ori,L_uri,L_asi,L_esi,L_isi,L_osi,L_usi,L_ati
     &,L_eti,L_iti,L_oti,L_uti,L_avi,L_evi,L_ivi,R_axi,R_exi
     &,L_odo,R_udo,R_afo,R_efo,R_ifo,R_ofo,R_ufo,R_oko,R_emo
     &,R_imo,R_omo,R_umo,L_apo,R_epo,R_ipo,L_ero,L_iro,L_oro
     &,L_uro,L_aso,L_eso,L_oso,L_uso,L_ato,L_eto,L_uto,L_avo
     &,L_exo,L_abu,L_ibu)
C |R_ubi         |4 4 I|11 mlfpar19     ||0.6|
C |R_i           |4 4 S|_simpJ3895*     |[���]���������� ��������� ������������� |0.0|
C |R_o           |4 4 K|_timpJ3895      |[���]������������ �������� �������������|1.0|
C |L_u           |1 1 S|_limpJ3895*     |[TF]���������� ��������� ������������� |F|
C |R_ad          |4 4 S|_slpbJ3888*     |���������� ��������� ||
C |R_af          |4 4 I|VX01            |||
C |R_ak          |4 4 I|max_c           |||
C |R_ik          |4 4 S|_slpbJ3841*     |���������� ��������� ||
C |R_ok          |4 4 S|_slpbJ3840*     |���������� ��������� ||
C |R_uk          |4 4 S|_slpbJ3839*     |���������� ��������� ||
C |R_al          |4 4 S|_slpbJ3833*     |���������� ��������� ||
C |L_or          |1 1 S|_qffJ3789*      |�������� ������ Q RS-��������  |F|
C |R_us          |4 4 I|mlfpar19        |��������� ������������|0.6|
C |R_uv          |4 4 S|_simpJ3631*     |[���]���������� ��������� ������������� |0.0|
C |R_ax          |4 4 K|_timpJ3631      |[���]������������ �������� �������������|0.4|
C |L_ex          |1 1 S|_limpJ3631*     |[TF]���������� ��������� ������������� |F|
C |R_ox          |4 4 S|_simpJ3629*     |[���]���������� ��������� ������������� |0.0|
C |R_ux          |4 4 K|_timpJ3629      |[���]������������ �������� �������������|0.4|
C |L_abe         |1 1 S|_limpJ3629*     |[TF]���������� ��������� ������������� |F|
C |R_ibe         |4 4 S|_sdelnv2dyn$100Dasha*|[���]���������� ��������� |0|
C |L_obe         |1 1 S|_idelnv2dyn$100Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ade         |4 4 S|_sdelnv2dyn$99Dasha*|[���]���������� ��������� |0|
C |L_ede         |1 1 S|_idelnv2dyn$99Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_ife         |1 1 S|_qff156*        |�������� ������ Q RS-��������  |F|
C |L_ake         |1 1 S|_qff155*        |�������� ������ Q RS-��������  |F|
C |R_ame         |4 4 I|value_pos       |��� �������� ��������|0.5|
C |R_ume         |4 4 S|_sdelnv2dyn$83Dasha*|[���]���������� ��������� |0|
C |L_epe         |1 1 S|_idelnv2dyn$83Dasha*|[TF]���������� ��������� �������� ������ |f|
C |R_ope         |4 4 S|_sdelnv2dyn$82Dasha*|[���]���������� ��������� |0|
C |L_are         |1 1 S|_idelnv2dyn$82Dasha*|[TF]���������� ��������� �������� ������ |f|
C |L_ire         |1 1 S|_qffnv2dyn$106Dasha*|�������� ������ Q RS-��������  |F|
C |L_ise         |1 1 S|_qffnv2dyn$104Dasha*|�������� ������ Q RS-��������  |F|
C |L_use         |1 1 S|_qffnv2dyn$105Dasha*|�������� ������ Q RS-��������  |F|
C |L_ate         |1 1 I|OUTC            |�����||
C |L_ite         |1 1 S|_qffnv2dyn$103Dasha*|�������� ������ Q RS-��������  |F|
C |L_ave         |1 1 S|_qffnv2dyn$102Dasha*|�������� ������ Q RS-��������  |F|
C |L_ove         |1 1 S|_qffnv2dyn$101Dasha*|�������� ������ Q RS-��������  |F|
C |L_axe         |1 1 I|OUTO            |������||
C |R_adi         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |L_odi         |1 1 S|_qffnv2dyn$87Dasha*|�������� ������ Q RS-��������  |F|
C |L_afi         |1 1 S|_qffnv2dyn$86Dasha*|�������� ������ Q RS-��������  |F|
C |L_ifi         |1 1 S|_qffnv2dyn$96Dasha*|�������� ������ Q RS-��������  |F|
C |L_ofi         |1 1 S|_qffnv2dyn$95Dasha*|�������� ������ Q RS-��������  |F|
C |L_eki         |1 1 O|limit_switch_error|||
C |L_iki         |1 1 O|flag_mlf19      |||
C |L_uli         |1 1 O|XH53            |�� ������ (���)|F|
C |L_emi         |1 1 O|XH54            |�� ����� (���)|F|
C |C20_api       |3 20 O|task_state      |���������||
C |L_epi         |1 1 I|vlv_kvit        |||
C |L_opi         |1 1 I|instr_fault     |||
C |L_upi         |1 1 S|_qffJ3361*      |�������� ������ Q RS-��������  |F|
C |L_iri         |1 1 O|fault           |�������������||
C |L_ori         |1 1 O|norm            |�����||
C |L_uri         |1 1 I|mlf17           |������ ���������������� ����� �����||
C |L_asi         |1 1 I|mlf16           |������ ������������ ����� �����||
C |L_esi         |1 1 I|mlf15           |������ ���������������� ����� ������||
C |L_isi         |1 1 I|mlf14           |������ ������������ ����� ������||
C |L_osi         |1 1 I|mlf07           |���������� ���� �������||
C |L_usi         |1 1 I|mlf28           |������ ���������������� ��������� �����||
C |L_ati         |1 1 I|mlf09           |����� ��������� �����||
C |L_eti         |1 1 I|mlf08           |����� ��������� ������||
C |L_iti         |1 1 I|mlf26           |������ ���������������� ��������� ������||
C |L_oti         |1 1 I|mlf27           |������ ������������ ��������� �����||
C |L_uti         |1 1 I|mlf25           |������ ������������ ��������� ������||
C |L_avi         |1 1 I|mlf23           |������� ������� �����||
C |L_evi         |1 1 I|mlf22           |����� ����� ��������||
C |L_ivi         |1 1 I|mlf19           |���� ������������||
C |R_axi         |4 4 K|_cJ3173         |�������� ��������� ����������|`max_c`|
C |R_exi         |4 4 K|_cJ3172         |�������� ��������� ����������|`min_c`|
C |L_odo         |1 1 I|YA27            |������� ���� �� ����������|F|
C |R_udo         |4 4 K|_uintV_INT_PU   |����������� ������ ����������� ������|`max_c`|
C |R_afo         |4 4 K|_tintV_INT_PU   |[���]�������� T �����������|1|
C |R_efo         |4 4 O|_ointV_INT_PU*  |�������� ������ ����������� |`state_c`|
C |R_ifo         |4 4 K|_lintV_INT_PU   |����������� ������ ����������� �����|`min_c`|
C |R_ofo         |4 4 I|vel             |����� ����|20.0|
C |R_ufo         |4 4 O|POS             |��������� ��||
C |R_oko         |4 4 O|VZ01            |�������� ����������� ��||
C |R_emo         |4 4 S|vmdown_ST*      |��������� ������ "������� ����� �� ���������" |0.0|
C |R_imo         |4 4 I|vmdown          |������� ������ ������ "������� ����� �� ���������" |0.0|
C |R_omo         |4 4 S|vmup_ST*        |��������� ������ "������� ������ �� ���������" |0.0|
C |R_umo         |4 4 I|vmup            |������� ������ ������ "������� ������ �� ���������" |0.0|
C |L_apo         |1 1 O|vmstop_CMD*     |[TF]����� ������ ������� ���� �� ���������|F|
C |R_epo         |4 4 S|vmstop_ST*      |��������� ������ "������� ���� �� ���������" |0.0|
C |R_ipo         |4 4 I|vmstop          |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_ero         |1 1 I|mlf06           |������������� ������� "�����"||
C |L_iro         |1 1 I|mlf05           |������������� ������� "������"||
C |L_oro         |1 1 O|vmup_CMD*       |[TF]����� ������ ������� ������ �� ���������|F|
C |L_uro         |1 1 I|ulubup          |���������� ������ �� ����������|F|
C |L_aso         |1 1 I|ulubdown        |���������� ���� �� ����������|F|
C |L_eso         |1 1 O|vmdown_CMD*     |[TF]����� ������ ������� ����� �� ���������|F|
C |L_oso         |1 1 O|stop_inner      |�������||
C |L_uso         |1 1 S|_qffJ3174*      |�������� ������ Q RS-��������  |F|
C |L_ato         |1 1 I|mlf04           |���������������� �������� �����||
C |L_eto         |1 1 I|YA25            |������� ����� �� ����������|F|
C |L_uto         |1 1 I|mlf03           |���������������� �������� ������||
C |L_avo         |1 1 I|YA26            |������� ������ �� ����������|F|
C |L_exo         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_abu         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ibu         |1 1 I|mlf24           |��� ���������� � ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R0_e,R_i,R_o
      LOGICAL*1 L_u
      REAL*4 R_ad,R0_ed,R0_id,R0_od,R0_ud,R_af
      LOGICAL*1 L0_ef,L0_if,L0_of,L0_uf
      REAL*4 R_ak,R0_ek,R_ik,R_ok,R_uk,R_al,R0_el,R0_il,R0_ol
     &,R0_ul,R0_am,R0_em,R0_im,R0_om,R0_um,R0_ap
      LOGICAL*1 L0_ep
      REAL*4 R0_ip,R0_op,R0_up,R0_ar,R0_er
      LOGICAL*1 L0_ir,L_or
      REAL*4 R0_ur,R0_as,R0_es,R0_is,R0_os,R_us,R0_at,R0_et
     &,R0_it,R0_ot
      LOGICAL*1 L0_ut,L0_av,L0_ev,L0_iv
      REAL*4 R0_ov,R_uv,R_ax
      LOGICAL*1 L_ex
      REAL*4 R0_ix,R_ox,R_ux
      LOGICAL*1 L_abe
      REAL*4 R0_ebe,R_ibe
      LOGICAL*1 L_obe
      REAL*4 R0_ube,R_ade
      LOGICAL*1 L_ede,L0_ide,L0_ode,L0_ude,L0_afe,L0_efe,L_ife
     &,L0_ofe,L0_ufe,L_ake,L0_eke,L0_ike,L0_oke
      REAL*4 R0_uke,R0_ale
      LOGICAL*1 L0_ele,L0_ile
      REAL*4 R0_ole,R0_ule,R_ame
      LOGICAL*1 L0_eme,L0_ime
      REAL*4 R0_ome,R_ume
      LOGICAL*1 L0_ape,L_epe
      REAL*4 R0_ipe,R_ope
      LOGICAL*1 L0_upe,L_are,L0_ere,L_ire,L0_ore,L0_ure,L0_ase
     &,L0_ese,L_ise,L0_ose,L_use,L_ate,L0_ete,L_ite,L0_ote
     &,L0_ute,L_ave,L0_eve,L0_ive,L_ove,L0_uve,L_axe
      LOGICAL*1 L0_exe
      REAL*4 R0_ixe,R0_oxe,R0_uxe
      LOGICAL*1 L0_abi
      REAL*4 R0_ebi
      LOGICAL*1 L0_ibi,L0_obi
      REAL*4 R_ubi,R_adi
      LOGICAL*1 L0_edi,L0_idi,L_odi,L0_udi,L_afi,L0_efi,L_ifi
     &,L_ofi,L0_ufi,L0_aki,L_eki,L_iki,L0_oki,L0_uki,L0_ali
     &,L0_eli,L0_ili,L0_oli,L_uli
      CHARACTER*20 C20_ami
      LOGICAL*1 L_emi
      CHARACTER*20 C20_imi,C20_omi,C20_umi,C20_api
      LOGICAL*1 L_epi,L0_ipi,L_opi,L_upi,L0_ari,L0_eri,L_iri
     &,L_ori,L_uri,L_asi,L_esi,L_isi,L_osi,L_usi,L_ati,L_eti
     &,L_iti,L_oti,L_uti,L_avi,L_evi,L_ivi
      LOGICAL*1 L0_ovi,L0_uvi
      REAL*4 R_axi,R_exi
      LOGICAL*1 L0_ixi
      REAL*4 R0_oxi,R0_uxi,R0_abo
      LOGICAL*1 L0_ebo
      REAL*4 R0_ibo,R0_obo,R0_ubo
      LOGICAL*1 L0_ado,L0_edo,L0_ido,L_odo
      REAL*4 R_udo,R_afo,R_efo,R_ifo,R_ofo,R_ufo
      LOGICAL*1 L0_ako,L0_eko
      REAL*4 R0_iko,R_oko,R0_uko,R0_alo,R0_elo,R0_ilo,R0_olo
      LOGICAL*1 L0_ulo,L0_amo
      REAL*4 R_emo,R_imo,R_omo,R_umo
      LOGICAL*1 L_apo
      REAL*4 R_epo,R_ipo
      LOGICAL*1 L0_opo,L0_upo,L0_aro,L_ero,L_iro,L_oro,L_uro
     &,L_aso,L_eso,L0_iso,L_oso,L_uso,L_ato,L_eto,L0_ito,L0_oto
     &,L_uto,L_avo,L0_evo,L0_ivo,L0_ovo,L0_uvo
      LOGICAL*1 L0_axo,L_exo,L0_ixo,L0_oxo,L0_uxo,L_abu,L0_ebu
     &,L_ibu

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_es=R_uk
C TELEGKA8_HANDLER.fmg( 340, 378):pre: ����������� ��
      R0_ar=R_al
C TELEGKA8_HANDLER.fmg( 310, 367):pre: ����������� ��
      R0_ul=R_ok
C TELEGKA8_HANDLER.fmg( 384, 370):pre: ����������� ��
      R0_em=R_ik
C TELEGKA8_HANDLER.fmg( 376, 376):pre: ����������� ��
      R0_ot=R_ad
C TELEGKA8_HANDLER.fmg( 340, 444):pre: ����������� ��
      R0_e=R_i
C TELEGKA8_HANDLER.fmg(  84, 424):pre: ������������  �� T
      R0_ov=R_uv
C TELEGKA8_HANDLER.fmg( 434, 408):pre: ������������  �� T
      R0_ix=R_ox
C TELEGKA8_HANDLER.fmg( 434, 426):pre: ������������  �� T
      R0_ebe=R_ibe
C TELEGKA8_HANDLER.fmg( 228, 334):pre: �������� ��������� ������,nv2dyn$100Dasha
      R0_ube=R_ade
C TELEGKA8_HANDLER.fmg( 228, 346):pre: �������� ��������� ������,nv2dyn$99Dasha
      R0_ipe=R_ope
C TELEGKA8_HANDLER.fmg( 213, 310):pre: �������� ��������� ������,nv2dyn$82Dasha
      R0_ome=R_ume
C TELEGKA8_HANDLER.fmg( 213, 296):pre: �������� ��������� ������,nv2dyn$83Dasha
      R0_ed = 1.0
C TELEGKA8_HANDLER.fmg( 356, 404):��������� (RE4) (�������)
      R0_id = 1.0
C TELEGKA8_HANDLER.fmg( 356, 410):��������� (RE4) (�������)
      R0_ek = R_us * R_ak
C TELEGKA8_HANDLER.fmg( 350, 424):����������
      R0_od = R0_ek + (-R0_ed)
C TELEGKA8_HANDLER.fmg( 360, 406):��������
      L0_ef=R_af.gt.R0_od
C TELEGKA8_HANDLER.fmg( 340, 406):���������� >
      R0_ud = R0_ek + R0_id
C TELEGKA8_HANDLER.fmg( 360, 412):��������
      L0_if=R_af.lt.R0_ud
C TELEGKA8_HANDLER.fmg( 340, 412):���������� <
      L0_of = L0_if.AND.L0_ef
C TELEGKA8_HANDLER.fmg( 346, 410):�
      L0_uf = L_ivi.AND.L0_of
C TELEGKA8_HANDLER.fmg( 350, 416):�
      R0_el = 0.0
C TELEGKA8_HANDLER.fmg( 418, 378):��������� (RE4) (�������),nv2dyn$57Dasha
      R0_ol = 0.99
C TELEGKA8_HANDLER.fmg( 400, 380):��������� (RE4) (�������),nv2dyn$60Dasha
      if(.NOT.L_avi) then
         R0_ap=R0_ul
      endif
C TELEGKA8_HANDLER.fmg( 389, 376):���� � ������������� �������
      R0_im = 1.0
C TELEGKA8_HANDLER.fmg( 354, 376):��������� (RE4) (�������)
      R0_om = 0.0
C TELEGKA8_HANDLER.fmg( 346, 376):��������� (RE4) (�������)
      R0_op = 1.0e-10
C TELEGKA8_HANDLER.fmg( 324, 354):��������� (RE4) (�������)
      R0_ur = 0.0
C TELEGKA8_HANDLER.fmg( 334, 368):��������� (RE4) (�������)
      R0_er = R0_ar + (-R_us)
C TELEGKA8_HANDLER.fmg( 313, 360):��������
      R0_at = 0.33
C TELEGKA8_HANDLER.fmg( 270, 456):��������� (RE4) (�������),nv2dyn$59Dasha
      R0_et = R_ofo * R0_at
C TELEGKA8_HANDLER.fmg( 276, 458):����������
      if(L_osi) then
         R0_uko=R0_et
      else
         R0_uko=R_ofo
      endif
C TELEGKA8_HANDLER.fmg( 282, 459):���� RE IN LO CH7,nv2dyn$40Dasha
      R0_uke = 0.000001
C TELEGKA8_HANDLER.fmg( 186, 258):��������� (RE4) (�������),nv2dyn$61Dasha
      R0_ale = 0.999999
C TELEGKA8_HANDLER.fmg( 185, 264):��������� (RE4) (�������),nv2dyn$60Dasha
      R0_ole = 0.000001
C TELEGKA8_HANDLER.fmg( 186, 276):��������� (RE4) (�������),nv2dyn$61Dasha
      L0_eme=R_ame.lt.R0_ole
C TELEGKA8_HANDLER.fmg( 190, 277):���������� <,nv2dyn$46Dasha
      L0_oki = L_usi.AND.L0_eme
C TELEGKA8_HANDLER.fmg( 222, 275):�
      R0_ule = 0.999999
C TELEGKA8_HANDLER.fmg( 184, 282):��������� (RE4) (�������),nv2dyn$60Dasha
      L0_ime=R_ame.gt.R0_ule
C TELEGKA8_HANDLER.fmg( 190, 283):���������� >,nv2dyn$45Dasha
      L0_uki = L_iti.AND.L0_ime
C TELEGKA8_HANDLER.fmg( 222, 286):�
      if(.not.L_osi) then
         R_ume=0.0
      elseif(.not.L_epe) then
         R_ume=R_ofo
      else
         R_ume=max(R0_ome-deltat,0.0)
      endif
      L0_ape=L_osi.and.R_ume.le.0.0
      L_epe=L_osi
C TELEGKA8_HANDLER.fmg( 213, 296):�������� ��������� ������,nv2dyn$83Dasha
      if(.not.L_osi) then
         R_ope=0.0
      elseif(.not.L_are) then
         R_ope=R_ofo
      else
         R_ope=max(R0_ipe-deltat,0.0)
      endif
      L0_upe=L_osi.and.R_ope.le.0.0
      L_are=L_osi
C TELEGKA8_HANDLER.fmg( 213, 310):�������� ��������� ������,nv2dyn$82Dasha
      R0_ixe = 0.01
C TELEGKA8_HANDLER.fmg(  32, 294):��������� (RE4) (�������),nv2dyn$74Dasha
      R0_oxe = R0_ixe + R_ubi
C TELEGKA8_HANDLER.fmg(  36, 294):��������
      R0_uxe = 0.01
C TELEGKA8_HANDLER.fmg(  32, 302):��������� (RE4) (�������),nv2dyn$74Dasha
      R0_ebi = (-R0_uxe) + R_ubi
C TELEGKA8_HANDLER.fmg(  36, 301):��������
      C20_omi = '��������� 2'
C TELEGKA8_HANDLER.fmg(  24, 324):��������� ���������� CH20 (CH30) (�������)
      C20_umi = '������� ���'
C TELEGKA8_HANDLER.fmg(  24, 326):��������� ���������� CH20 (CH30) (�������)
      C20_ami = '��������� 1'
C TELEGKA8_HANDLER.fmg(  39, 324):��������� ���������� CH20 (CH30) (�������)
      L_upi=(L_opi.or.L_upi).and..not.(L_epi)
      L0_ipi=.not.L_upi
C TELEGKA8_HANDLER.fmg( 264, 274):RS �������
      !��������� R0_uxi = TELEGKA8_HANDLERC?? /`max_c`/
      R0_uxi=R_axi
C TELEGKA8_HANDLER.fmg( 374, 408):���������
      !��������� R0_obo = TELEGKA8_HANDLERC?? /`min_c`/
      R0_obo=R_exi
C TELEGKA8_HANDLER.fmg( 374, 428):���������
      R0_oxi = 0.01
C TELEGKA8_HANDLER.fmg( 374, 406):��������� (RE4) (�������)
      R0_abo = R0_uxi + (-R0_oxi)
C TELEGKA8_HANDLER.fmg( 377, 408):��������
      R0_ibo = 0.01
C TELEGKA8_HANDLER.fmg( 374, 425):��������� (RE4) (�������)
      R0_ubo = R0_obo + R0_ibo
C TELEGKA8_HANDLER.fmg( 377, 426):��������
      R0_iko = 0.0
C TELEGKA8_HANDLER.fmg( 308, 450):��������� (RE4) (�������)
      R0_olo = 0.0
C TELEGKA8_HANDLER.fmg( 292, 450):��������� (RE4) (�������)
      L_eso=R_imo.ne.R_emo
      R_emo=R_imo
C TELEGKA8_HANDLER.fmg(  32, 408):���������� ������������� ������
      L0_ito = L_aso.AND.L_eso.AND.(.NOT.L_ero)
C TELEGKA8_HANDLER.fmg(  81, 408):�
      L0_oto = L0_ito.OR.L_eto.OR.L_ato
C TELEGKA8_HANDLER.fmg(  88, 406):���
      L0_ike = L_ero.AND.L_eso
C TELEGKA8_HANDLER.fmg( 208, 334):�
      L0_ufe = (.NOT.L_iro).OR.L_eso
C TELEGKA8_HANDLER.fmg( 214, 342):���
      L_oro=R_umo.ne.R_omo
      R_omo=R_umo
C TELEGKA8_HANDLER.fmg(  42, 464):���������� ������������� ������
      L0_evo = L_oro.AND.L_uro.AND.(.NOT.L_iro)
C TELEGKA8_HANDLER.fmg(  74, 454):�
      L0_ivo = L0_evo.OR.L_avo.OR.L_uto
C TELEGKA8_HANDLER.fmg(  86, 452):���
      L0_iso = L0_ivo.OR.L0_oto
C TELEGKA8_HANDLER.fmg(  92, 420):���
      L0_oke = L_iro.AND.L_oro
C TELEGKA8_HANDLER.fmg( 208, 346):�
      L_ake=(L0_oke.or.L_ake).and..not.(L0_ufe)
      L0_eke=.not.L_ake
C TELEGKA8_HANDLER.fmg( 219, 344):RS �������,155
      if(.not.L_ake) then
         R_ade=0.0
      elseif(.not.L_ede) then
         R_ade=R_ofo
      else
         R_ade=max(R0_ube-deltat,0.0)
      endif
      L0_ude=L_ake.and.R_ade.le.0.0
      L_ede=L_ake
C TELEGKA8_HANDLER.fmg( 228, 346):�������� ��������� ������,nv2dyn$99Dasha
      L_ofi=(L0_ude.or.L_ofi).and..not.(L_ate)
      L0_afe=.not.L_ofi
C TELEGKA8_HANDLER.fmg( 244, 344):RS �������,nv2dyn$95Dasha
      L0_efe = L_oro.OR.(.NOT.L_ero)
C TELEGKA8_HANDLER.fmg( 212, 330):���
      L_ife=(L0_ike.or.L_ife).and..not.(L0_efe)
      L0_ofe=.not.L_ife
C TELEGKA8_HANDLER.fmg( 219, 332):RS �������,156
      if(.not.L_ife) then
         R_ibe=0.0
      elseif(.not.L_obe) then
         R_ibe=R_ofo
      else
         R_ibe=max(R0_ebe-deltat,0.0)
      endif
      L0_ide=L_ife.and.R_ibe.le.0.0
      L_obe=L_ife
C TELEGKA8_HANDLER.fmg( 228, 334):�������� ��������� ������,nv2dyn$100Dasha
      L_ifi=(L0_ide.or.L_ifi).and..not.(L_axe)
      L0_ode=.not.L_ifi
C TELEGKA8_HANDLER.fmg( 244, 332):RS �������,nv2dyn$96Dasha
      L_apo=R_ipo.ne.R_epo
      R_epo=R_ipo
C TELEGKA8_HANDLER.fmg(  31, 431):���������� ������������� ������
      L0_av = L_emi.AND.L_asi
C TELEGKA8_HANDLER.fmg(  99, 387):�
C label 147  try147=try147-1
      L0_ebo=R_ufo.lt.R0_ubo
C TELEGKA8_HANDLER.fmg( 386, 428):���������� <
      L0_ev = L0_ebo.AND.(.NOT.L_usi)
C TELEGKA8_HANDLER.fmg( 422, 426):�
      L_uli = L_ati.OR.L0_ev.OR.L_oti
C TELEGKA8_HANDLER.fmg( 426, 426):���
      if(L_uli.and..not.L_abe) then
         R_ox=R_ux
      else
         R_ox=max(R0_ix-deltat,0.0)
      endif
      L0_edo=R_ox.gt.0.0
      L_abe=L_uli
C TELEGKA8_HANDLER.fmg( 434, 426):������������  �� T
      if(L_emi.and..not.L_ex) then
         R_uv=R_ax
      else
         R_uv=max(R0_ov-deltat,0.0)
      endif
      L0_ado=R_uv.gt.0.0
      L_ex=L_emi
C TELEGKA8_HANDLER.fmg( 434, 408):������������  �� T
      L0_ido = L_apo.OR.L_odo.OR.L0_edo.OR.L0_ado
C TELEGKA8_HANDLER.fmg(  76, 424):���
      if(L0_ido.and..not.L_u) then
         R_i=R_o
      else
         R_i=max(R0_e-deltat,0.0)
      endif
      L0_ovi=R_i.gt.0.0
      L_u=L0_ido
C TELEGKA8_HANDLER.fmg(  84, 424):������������  �� T
      L_uso=L0_ovi.or.(L_uso.and..not.(L0_iso))
      L0_uvi=.not.L_uso
C TELEGKA8_HANDLER.fmg( 123, 422):RS �������
      L0_ovo = L_uso.OR.L_ibu
C TELEGKA8_HANDLER.fmg( 114, 432):���
      L0_ut = L_uli.AND.L_isi
C TELEGKA8_HANDLER.fmg(  99, 375):�
      L0_aro = L0_av.OR.L0_ut
C TELEGKA8_HANDLER.fmg( 107, 386):���
      L0_upo = L_emi.AND.(.NOT.L_eti)
C TELEGKA8_HANDLER.fmg( 106, 462):�
      L0_uxo = L_ibu.OR.L0_ovo.OR.L0_oto.OR.L0_aro.OR.L0_upo
C TELEGKA8_HANDLER.fmg( 120, 446):���
      L0_oxo = (.NOT.L0_upo).AND.L0_ivo
C TELEGKA8_HANDLER.fmg( 115, 452):�
      L_abu=(L0_oxo.or.L_abu).and..not.(L0_uxo)
      L0_ebu=.not.L_abu
C TELEGKA8_HANDLER.fmg( 128, 450):RS �������,1
      L0_eko = L_abu.AND.(.NOT.L_ato)
C TELEGKA8_HANDLER.fmg( 258, 448):�
      L0_amo = L0_eko.OR.L_uto
C TELEGKA8_HANDLER.fmg( 271, 448):���
      if(L0_amo) then
         R0_elo=R0_uko
      else
         R0_elo=R0_olo
      endif
C TELEGKA8_HANDLER.fmg( 297, 460):���� RE IN LO CH7
      L0_opo = L_uli.AND.(.NOT.L_ati)
C TELEGKA8_HANDLER.fmg( 104, 414):�
      L0_axo = L0_ovo.OR.L0_ivo.OR.L0_aro.OR.L0_opo
C TELEGKA8_HANDLER.fmg( 120, 396):���
      L0_uvo = L0_oto.AND.(.NOT.L0_opo)
C TELEGKA8_HANDLER.fmg( 120, 404):�
      L_exo=(L0_uvo.or.L_exo).and..not.(L0_axo)
      L0_ixo=.not.L_exo
C TELEGKA8_HANDLER.fmg( 128, 402):RS �������,2
      L0_ako = L_exo.AND.(.NOT.L_uto)
C TELEGKA8_HANDLER.fmg( 258, 434):�
      L0_ulo = L0_ako.OR.L_ato
C TELEGKA8_HANDLER.fmg( 271, 434):���
      if(L0_ulo) then
         R0_alo=R0_uko
      else
         R0_alo=R0_olo
      endif
C TELEGKA8_HANDLER.fmg( 297, 442):���� RE IN LO CH7
      R0_ilo = R0_elo + (-R0_alo)
C TELEGKA8_HANDLER.fmg( 303, 452):��������
      if(L_ibu) then
         R_oko=R0_iko
      else
         R_oko=R0_ilo
      endif
C TELEGKA8_HANDLER.fmg( 312, 451):���� RE IN LO CH7
      R_efo=R_efo+deltat/R_afo*R_oko
      if(R_efo.gt.R_udo) then
         R_efo=R_udo
      elseif(R_efo.lt.R_ifo) then
         R_efo=R_ifo
      endif
C TELEGKA8_HANDLER.fmg( 330, 437):����������,V_INT_PU
      if(L_avi) then
         R0_it=R0_ot
      else
         R0_it=R_efo
      endif
C TELEGKA8_HANDLER.fmg( 340, 437):���� RE IN LO CH7
      if(L0_uf) then
         R_ufo=R0_ek
      else
         R_ufo=R0_it
      endif
C TELEGKA8_HANDLER.fmg( 355, 436):���� RE IN LO CH7
      L0_ixi=R_ufo.gt.R0_abo
C TELEGKA8_HANDLER.fmg( 386, 408):���������� >
      L0_iv = L0_ixi.AND.(.NOT.L_iti)
C TELEGKA8_HANDLER.fmg( 420, 408):�
      L_emi = L_eti.OR.L0_iv.OR.L_uti
C TELEGKA8_HANDLER.fmg( 426, 408):���
C sav1=L0_upo
      L0_upo = L_emi.AND.(.NOT.L_eti)
C TELEGKA8_HANDLER.fmg( 106, 462):recalc:�
C if(sav1.ne.L0_upo .and. try195.gt.0) goto 195
C sav1=L0_ado
      if(L_emi.and..not.L_ex) then
         R_uv=R_ax
      else
         R_uv=max(R0_ov-deltat,0.0)
      endif
      L0_ado=R_uv.gt.0.0
      L_ex=L_emi
C TELEGKA8_HANDLER.fmg( 434, 408):recalc:������������  �� T
C if(sav1.ne.L0_ado .and. try175.gt.0) goto 175
C sav1=L0_av
      L0_av = L_emi.AND.L_asi
C TELEGKA8_HANDLER.fmg(  99, 387):recalc:�
C if(sav1.ne.L0_av .and. try147.gt.0) goto 147
      if(L_emi) then
         C20_imi=C20_omi
      else
         C20_imi=C20_umi
      endif
C TELEGKA8_HANDLER.fmg(  28, 325):���� RE IN LO CH20
      if(L_uli) then
         C20_api=C20_ami
      else
         C20_api=C20_imi
      endif
C TELEGKA8_HANDLER.fmg(  44, 324):���� RE IN LO CH20
      L0_ili = L_ati.AND.L_emi
C TELEGKA8_HANDLER.fmg( 230, 316):�
      L0_eli = L0_upe.AND.(.NOT.L_emi)
C TELEGKA8_HANDLER.fmg( 222, 309):�
      L_ise=(L_ate.or.L_ise).and..not.(L_emi)
      L0_ese=.not.L_ise
C TELEGKA8_HANDLER.fmg(  65, 278):RS �������,nv2dyn$104Dasha
      L_odi=(L_uli.or.L_odi).and..not.(L_emi)
      L0_edi=.not.L_odi
C TELEGKA8_HANDLER.fmg( 112, 306):RS �������,nv2dyn$87Dasha
      L0_udi = L_oti.AND.(.NOT.L_odi)
C TELEGKA8_HANDLER.fmg( 121, 311):�
      L_afi=(L_emi.or.L_afi).and..not.(L_uli)
      L0_idi=.not.L_afi
C TELEGKA8_HANDLER.fmg( 112, 317):RS �������,nv2dyn$86Dasha
      L0_efi = L_uti.AND.(.NOT.L_afi)
C TELEGKA8_HANDLER.fmg( 122, 320):�
      L_eki = L0_efi.OR.L0_udi
C TELEGKA8_HANDLER.fmg( 128, 319):���
      R_ad=R0_it
C TELEGKA8_HANDLER.fmg( 340, 444):����������� ��
      R0_ip = R_oko + R0_er
C TELEGKA8_HANDLER.fmg( 319, 361):��������
      R0_up = R0_ip * R0_er
C TELEGKA8_HANDLER.fmg( 323, 360):����������
      L0_ir=R0_up.lt.R0_op
C TELEGKA8_HANDLER.fmg( 328, 359):���������� <
      L_or=(L0_ir.or.L_or).and..not.(.NOT.L_ivi)
      L0_ep=.not.L_or
C TELEGKA8_HANDLER.fmg( 334, 357):RS �������
      if(L_or) then
         R0_as=R0_ur
      else
         R0_as=R_oko
      endif
C TELEGKA8_HANDLER.fmg( 337, 368):���� RE IN LO CH7
      R0_is = R0_es + R0_as
C TELEGKA8_HANDLER.fmg( 343, 370):��������
      R0_um=MAX(R0_om,R0_is)
C TELEGKA8_HANDLER.fmg( 350, 371):��������
      R0_os=MIN(R0_im,R0_um)
C TELEGKA8_HANDLER.fmg( 358, 372):�������
      if(L_avi) then
         R0_am=R0_em
      else
         R0_am=R0_os
      endif
C TELEGKA8_HANDLER.fmg( 378, 370):���� RE IN LO CH7
      R_ik=R0_am
C TELEGKA8_HANDLER.fmg( 376, 376):����������� ��
      R_ok=R0_am
C TELEGKA8_HANDLER.fmg( 384, 370):����������� ��
      if(L_avi) then
         R0_ap=R0_os
      endif
C TELEGKA8_HANDLER.fmg( 371, 376):���� � ������������� �������
      if(L_or) then
         R0_ap=R_us
      endif
C TELEGKA8_HANDLER.fmg( 329, 376):���� � ������������� �������
      R_al=R0_ap
C TELEGKA8_HANDLER.fmg( 310, 367):����������� ��
      R_uk=R0_ap
C TELEGKA8_HANDLER.fmg( 340, 378):����������� ��
      if(L_isi) then
         R0_il=R0_ol
      else
         R0_il=R0_ap
      endif
C TELEGKA8_HANDLER.fmg( 404, 380):���� RE IN LO CH7,nv2dyn$56Dasha
      if(L_asi) then
         R_adi=R0_el
      else
         R_adi=R0_il
      endif
C TELEGKA8_HANDLER.fmg( 423, 380):���� RE IN LO CH7,nv2dyn$56Dasha
      L0_abi=R_adi.gt.R0_ebi
C TELEGKA8_HANDLER.fmg(  40, 305):���������� >,nv2dyn$84Dasha
      L0_exe=R_adi.lt.R0_oxe
C TELEGKA8_HANDLER.fmg(  42, 299):���������� <,nv2dyn$85Dasha
      L0_ibi = L0_abi.AND.L0_exe
C TELEGKA8_HANDLER.fmg(  48, 304):�
      L0_obi = L0_ibi.AND.L_ivi
C TELEGKA8_HANDLER.fmg(  60, 296):�
      L0_ose = L0_obi.AND.L_ise
C TELEGKA8_HANDLER.fmg(  78, 281):�
      L0_ele=R_adi.lt.R0_uke
C TELEGKA8_HANDLER.fmg( 190, 260):���������� <,nv2dyn$46Dasha
      L0_ufi = L_uri.AND.L0_ele
C TELEGKA8_HANDLER.fmg( 222, 258):�
      L0_ile=R_adi.gt.R0_ale
C TELEGKA8_HANDLER.fmg( 190, 266):���������� >,nv2dyn$45Dasha
      L0_aki = L_esi.AND.L0_ile
C TELEGKA8_HANDLER.fmg( 222, 268):�
      L_oso=L_uso
C TELEGKA8_HANDLER.fmg( 150, 424):������,stop_inner
      L_ove=(L_axe.or.L_ove).and..not.(L_uli)
      L0_uve=.not.L_ove
C TELEGKA8_HANDLER.fmg(  60, 289):RS �������,nv2dyn$101Dasha
      L0_ive = L0_obi.AND.L_ove
C TELEGKA8_HANDLER.fmg(  78, 295):�
      L0_ali = L0_ape.AND.(.NOT.L_uli)
C TELEGKA8_HANDLER.fmg( 218, 296):�
      L0_oli = L_eti.AND.L_uli
C TELEGKA8_HANDLER.fmg( 230, 321):�
      L0_ote = (.NOT.L_ivi).AND.L_ite
C TELEGKA8_HANDLER.fmg( 106, 290):�
C label 372  try372=try372-1
      L0_ute = L_ate.AND.L0_ote
C TELEGKA8_HANDLER.fmg( 110, 291):�
      L_ite=(L_ivi.or.L_ite).and..not.(L0_ute)
      L0_ete=.not.L_ite
C TELEGKA8_HANDLER.fmg( 102, 287):RS �������,nv2dyn$103Dasha
C sav1=L0_ote
      L0_ote = (.NOT.L_ivi).AND.L_ite
C TELEGKA8_HANDLER.fmg( 106, 290):recalc:�
C if(sav1.ne.L0_ote .and. try372.gt.0) goto 372
      L_ave=(L0_ive.or.L_ave).and..not.(L0_ute)
      L0_eve=.not.L_ave
C TELEGKA8_HANDLER.fmg( 116, 293):RS �������,nv2dyn$102Dasha
      L0_ore = (.NOT.L_ivi).AND.L_ire
C TELEGKA8_HANDLER.fmg( 112, 276):�
C label 378  try378=try378-1
      L0_ure = L_axe.AND.L0_ore
C TELEGKA8_HANDLER.fmg( 116, 277):�
      L_ire=(L_ivi.or.L_ire).and..not.(L0_ure)
      L0_ere=.not.L_ire
C TELEGKA8_HANDLER.fmg( 107, 273):RS �������,nv2dyn$106Dasha
C sav1=L0_ore
      L0_ore = (.NOT.L_ivi).AND.L_ire
C TELEGKA8_HANDLER.fmg( 112, 276):recalc:�
C if(sav1.ne.L0_ore .and. try378.gt.0) goto 378
      L_use=(L0_ose.or.L_use).and..not.(L0_ure)
      L0_ase=.not.L_use
C TELEGKA8_HANDLER.fmg( 124, 279):RS �������,nv2dyn$105Dasha
      L_iki = L_ave.OR.L_use
C TELEGKA8_HANDLER.fmg( 138, 294):���
      L0_ari =.NOT.(L_uto.OR.L_ato.OR.L0_oli.OR.L0_ili.OR.L0_eli.OR.L0_a
     &li.OR.L0_uki.OR.L0_oki.OR.L_avi.OR.L_isi.OR.L_asi.OR.L_iki.OR.L_ek
     &i.OR.L0_aki.OR.L0_ufi.OR.L_ofi.OR.L_ifi)
C TELEGKA8_HANDLER.fmg( 256, 309):���
      L0_eri =.NOT.(L0_ari)
C TELEGKA8_HANDLER.fmg( 266, 281):���
      L_iri = L0_eri.OR.L_upi
C TELEGKA8_HANDLER.fmg( 270, 280):���
      L_ori = L0_ari.OR.L_opi
C TELEGKA8_HANDLER.fmg( 266, 286):���
      End
