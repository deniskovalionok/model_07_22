      Interface
      Subroutine SOLENOIDVALVE_MAN(ext_deltat,R_ofi,L_obi
     &,L_ibi,R8_exe,C30_o,L_ed,L_af,L_ef,I_ak,I_ek,R_uk,R_al
     &,R_el,R_il,R_ol,R_ul,I_am,I_om,I_ep,I_ar,L_er,L_ir,L_or
     &,L_ur,L_os,L_us,L_it,L_ot,L_ut,L_ev,L_ade,L_efe,L_ake
     &,L_ike,L_oke,L_ele,L_ile,L_ule,R8_eme,R_ape,R8_ope,L_ove
     &,R_oxe,R_abi,L_ufi,L_aki,L_eki,L_iki,L_oki,L_uki)
C |R_ofi         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |L_obi         |1 1 O|13 cbo          |�� ������� (���)|F|
C |L_ibi         |1 1 O|14 cbc          |�� ������� (���)|F|
C |R8_exe        |4 8 O|16 value        |��� �������� ��������|0.5|
C |C30_o         |3 30 O|state           |||
C |L_ed          |1 1 I|tech_mode       |||
C |L_af          |1 1 I|ruch_mode       |||
C |L_ef          |1 1 I|avt_mode        |||
C |I_ak          |2 4 O|LM              |�����||
C |I_ek          |2 4 O|LF              |����� �������������||
C |R_uk          |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_al          |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_el          |4 4 S|vmclose_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_il          |4 4 I|vmclose_button  |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_ol          |4 4 S|vmopen_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ul          |4 4 I|vmopen_button   |������� ������ ������ "������� ������� �� ���������" |0.0|
C |I_am          |2 4 O|LST             |����� ����||
C |I_om          |2 4 O|LCL             |����� �������||
C |I_ep          |2 4 O|LOP             |����� �������||
C |I_ar          |2 4 O|LS              |��������� �������||
C |L_er          |1 1 O|vmclose_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ir          |1 1 O|vmopen_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_or          |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_ur          |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_os          |1 1 O|block           |||
C |L_us          |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_it          |1 1 O|STOP            |�������||
C |L_ot          |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_ut          |1 1 I|uluclose        |������� ������� �� ����������|F|
C |L_ev          |1 1 I|uluopen         |������� ������� �� ����������|F|
C |L_ade         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_efe         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ake         |1 1 I|vlv_kvit        |||
C |L_ike         |1 1 I|instr_fault     |||
C |L_oke         |1 1 S|_qffJ453*       |�������� ������ Q RS-��������  |F|
C |L_ele         |1 1 O|fault           |�������������||
C |L_ile         |1 1 O|norm            |�����||
C |L_ule         |1 1 O|nopower         |��� ����������||
C |R8_eme        |4 8 I|voltage         |[��]���������� �� ������||
C |R_ape         |4 4 I|power           |�������� ��������||
C |R8_ope        |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_ove         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_oxe         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_abi         |4 4 I|tcl_top         |����� ����|30.0|
C |L_ufi         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_aki         |1 1 I|mlf23           |������� ������� �����||
C |L_eki         |1 1 I|mlf22           |����� ����� ��������||
C |L_iki         |1 1 I|mlf04           |�������� �� ��������||
C |L_oki         |1 1 I|mlf03           |�������� �� ��������||
C |L_uki         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      CHARACTER*30 C30_o
      LOGICAL*1 L_ed,L_af,L_ef
      INTEGER*4 I_ak,I_ek
      REAL*4 R_uk,R_al,R_el,R_il,R_ol,R_ul
      INTEGER*4 I_am,I_om,I_ep,I_ar
      LOGICAL*1 L_er,L_ir,L_or,L_ur,L_os,L_us,L_it,L_ot,L_ut
     &,L_ev,L_ade,L_efe,L_ake,L_ike,L_oke,L_ele,L_ile,L_ule
      REAL*8 R8_eme
      REAL*4 R_ape
      REAL*8 R8_ope
      LOGICAL*1 L_ove
      REAL*8 R8_exe
      REAL*4 R_oxe,R_abi
      LOGICAL*1 L_ibi,L_obi
      REAL*4 R_ofi
      LOGICAL*1 L_ufi,L_aki,L_eki,L_iki,L_oki,L_uki
      End subroutine SOLENOIDVALVE_MAN
      End interface
