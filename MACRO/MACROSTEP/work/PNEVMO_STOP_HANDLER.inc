      Interface
      Subroutine PNEVMO_STOP_HANDLER(ext_deltat,R_ote,R8_ebe
     &,C30_ed,R_af,R_ef,R_if,R_of,R_uf,R_ak,R_ik,R_uk,L_al
     &,L_il,L_ol,L_ul,R_am,L_im,L_om,L_um,L_ap,L_ep,L_up,L_ar
     &,L_ir,L_or,L_as,R8_is,R_et,R8_ut,L_av,L_ev,L_ov,L_uv
     &,L_ufe,R_oke,R_ale,L_are,L_ire,L_ose,L_ete,L_ute,L_ave
     &,L_eve,L_ive,L_ove,L_uve)
C |R_ote         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |R8_ebe        |4 8 O|16 value        |��� �������� ��������|0.5|
C |C30_ed        |3 30 O|state           |||
C |R_af          |4 4 S|vmclose_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ef          |4 4 I|vmclose_button  |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_if          |4 4 S|vmopen_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_of          |4 4 I|vmopen_button   |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_uf          |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ak          |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_ik          |4 4 O|POS_CL          |��������||
C |R_uk          |4 4 O|POS_OP          |��������||
C |L_al          |1 1 I|vlv_kvit        |||
C |L_il          |1 1 I|instr_fault     |||
C |L_ol          |1 1 S|_qffJ1618*      |�������� ������ Q RS-��������  |F|
C |L_ul          |1 1 O|vmclose_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |R_am          |4 4 I|tclose          |����� ���� �������|30.0|
C |L_im          |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_om          |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_um          |1 1 O|block           |||
C |L_ap          |1 1 O|vmopen_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ep          |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_up          |1 1 O|STOP            |�������||
C |L_ar          |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_ir          |1 1 O|fault           |�������������||
C |L_or          |1 1 O|norm            |�����||
C |L_as          |1 1 O|nopower         |��� ����������||
C |R8_is         |4 8 I|voltage         |[��]���������� �� ������||
C |R_et          |4 4 I|power           |�������� ��������||
C |R8_ut         |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_av          |1 1 I|YA22            |������� ������� �������|F|
C |L_ev          |1 1 I|YA12            |������� ������� �� ����������|F|
C |L_ov          |1 1 I|YA21            |������� ������� �������|F|
C |L_uv          |1 1 I|YA11            |������� ������� �� ����������|F|
C |L_ufe         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_oke         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_ale         |4 4 I|topen           |����� ���� �������|30.0|
C |L_are         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_ire         |1 1 O|YV12            |�� ������� (���)|F|
C |L_ose         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ete         |1 1 O|YV11            |�� ������� (���)|F|
C |L_ute         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_ave         |1 1 I|mlf23           |������� ������� �����||
C |L_eve         |1 1 I|mlf22           |����� ����� ��������||
C |L_ive         |1 1 I|mlf04           |�������� �� ��������||
C |L_ove         |1 1 I|mlf03           |�������� �� ��������||
C |L_uve         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      CHARACTER*30 C30_ed
      REAL*4 R_af,R_ef,R_if,R_of,R_uf,R_ak,R_ik,R_uk
      LOGICAL*1 L_al,L_il,L_ol,L_ul
      REAL*4 R_am
      LOGICAL*1 L_im,L_om,L_um,L_ap,L_ep,L_up,L_ar,L_ir,L_or
     &,L_as
      REAL*8 R8_is
      REAL*4 R_et
      REAL*8 R8_ut
      LOGICAL*1 L_av,L_ev,L_ov,L_uv
      REAL*8 R8_ebe
      LOGICAL*1 L_ufe
      REAL*4 R_oke,R_ale
      LOGICAL*1 L_are,L_ire,L_ose,L_ete
      REAL*4 R_ote
      LOGICAL*1 L_ute,L_ave,L_eve,L_ive,L_ove,L_uve
      End subroutine PNEVMO_STOP_HANDLER
      End interface
