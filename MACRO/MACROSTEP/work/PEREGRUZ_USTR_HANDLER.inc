      Interface
      Subroutine PEREGRUZ_USTR_HANDLER(ext_deltat,L_e,L_i
     &,L_o,L_u,R_ed,R_id,R_ud,R_af,L_ef,R_of,R_uf,L_ak,L_ul
     &,L_om,L_um,R_ap,R_ep,R_ip,R_op,R_up,L_er,L_or,R_as,R_es
     &,R_ot,R_ut,R_av,R_ev,L_iv,R_ov,R_uv,L_ax,L_ex,L_ix,L_ux
     &,L_abe,L_ebe,L_obe,L_ode,L_ife,L_ufe)
C |L_e           |1 1 O|UNCATCH         |��������� �������|F|
C |L_i           |1 1 I|YA31            |������� ��������� �������|F|
C |L_o           |1 1 O|CATCH           |������ �������|F|
C |L_u           |1 1 I|YA30            |������� ������ �������|F|
C |R_ed          |4 4 K|_cJ3237         |�������� ��������� ����������|`max_c`|
C |R_id          |4 4 K|_cJ3236         |�������� ��������� ����������|`min_c`|
C |R_ud          |4 4 S|_simpJ3201*     |[���]���������� ��������� ������������� |0.0|
C |R_af          |4 4 K|_timpJ3201      |[���]������������ �������� �������������|0.4|
C |L_ef          |1 1 S|_limpJ3201*     |[TF]���������� ��������� ������������� |F|
C |R_of          |4 4 S|_simpJ3198*     |[���]���������� ��������� ������������� |0.0|
C |R_uf          |4 4 K|_timpJ3198      |[���]������������ �������� �������������|0.4|
C |L_ak          |1 1 S|_limpJ3198*     |[TF]���������� ��������� ������������� |F|
C |L_ul          |1 1 I|stop            |������� �� ��������� ����||
C |L_om          |1 1 I|YA27            |������� ���� �� ����������|F|
C |L_um          |1 1 I|down            |������� �� ��������� ����||
C |R_ap          |4 4 K|_uintV_INT_PU   |����������� ������ ����������� ������|`max_c`|
C |R_ep          |4 4 K|_tintV_INT_PU   |[���]�������� T �����������|1|
C |R_ip          |4 4 K|_lintV_INT_PU   |����������� ������ ����������� �����|`min_c`|
C |R_op          |4 4 O|POS             |��������� ��||
C |R_up          |4 4 O|_ointV_INT_PU*  |�������� ������ ����������� |`state_c`|
C |L_er          |1 1 I|mlf03           |�������� �� ��������||
C |L_or          |1 1 I|mlf04           |�������� �� ��������||
C |R_as          |4 4 O|VZ01            |�������� ����������� ��||
C |R_es          |4 4 I|vel             |����� ����|20.0|
C |R_ot          |4 4 S|USER_DOWN_ST*   |��������� ������ "������� ���� �� ���������" |0.0|
C |R_ut          |4 4 I|USER_DOWN       |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_av          |4 4 S|USER_UP_ST*     |��������� ������ "������� ����� �� ���������" |0.0|
C |R_ev          |4 4 I|USER_UP         |������� ������ ������ "������� ����� �� ���������" |0.0|
C |L_iv          |1 1 O|USER_STOP_CMD*  |[TF]����� ������ ������� ���� �� ���������|F|
C |R_ov          |4 4 S|USER_STOP_ST*   |��������� ������ "������� ���� �� ���������" |0.0|
C |R_uv          |4 4 I|USER_STOP       |������� ������ ������ "������� ���� �� ���������" |0.0|
C |L_ax          |1 1 O|USER_UP_CMD*    |[TF]����� ������ ������� ����� �� ���������|F|
C |L_ex          |1 1 I|up              |������� �� ��������� �����||
C |L_ix          |1 1 O|USER_DOWN_CMD*  |[TF]����� ������ ������� ���� �� ���������|F|
C |L_ux          |1 1 O|stop_inner      |�������||
C |L_abe         |1 1 S|_qffJ3299*      |�������� ������ Q RS-��������  |F|
C |L_ebe         |1 1 I|YA25            |������� ���� �� ����������|F|
C |L_obe         |1 1 I|YA26            |������� ����� �� ����������|F|
C |L_ode         |1 1 S|_qff2*          |�������� ������ Q RS-��������  |F|
C |L_ife         |1 1 S|_qff1*          |�������� ������ Q RS-��������  |F|
C |L_ufe         |1 1 I|mlf24           |��� ���������� � ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat
      LOGICAL*1 L_e,L_i,L_o,L_u
      REAL*4 R_ed,R_id,R_ud,R_af
      LOGICAL*1 L_ef
      REAL*4 R_of,R_uf
      LOGICAL*1 L_ak,L_ul,L_om,L_um
      REAL*4 R_ap,R_ep,R_ip,R_op,R_up
      LOGICAL*1 L_er,L_or
      REAL*4 R_as,R_es,R_ot,R_ut,R_av,R_ev
      LOGICAL*1 L_iv
      REAL*4 R_ov,R_uv
      LOGICAL*1 L_ax,L_ex,L_ix,L_ux,L_abe,L_ebe,L_obe,L_ode
     &,L_ife,L_ufe
      End subroutine PEREGRUZ_USTR_HANDLER
      End interface
