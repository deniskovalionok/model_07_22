      Subroutine DAT_ANA_2_HANDLER(ext_deltat,R_e,R_i,R_ed
     &,R_id,I_al,R_im,R_um,R_ap,R_ep,R_os,L_us,R_et,L_it,L_ot
     &,R_av,R_ux,R_ebe,L_obe)
C |R_e           |4 4 O|nom             |||
C |R_i           |4 4 K|_cJ1505         |�������� ��������� ����������|0.3|
C |R_ed          |4 4 I|koeff_units     |����������� �����������||
C |R_id          |4 4 I|input           |����||
C |I_al          |2 4 O|dat_color       |���� ��������||
C |R_im          |4 4 I|LOWER_warning_setpoint|||
C |R_um          |4 4 I|LOWER_alarm_setpoint|||
C |R_ap          |4 4 I|UPPER_alarm_setpoint|||
C |R_ep          |4 4 I|UPPER_warning_setpoint|||
C |R_os          |4 4 I|GM05V           |���������� ����������� �������||
C |L_us          |1 1 I|GM05            |���������� ����������� �������||
C |R_et          |4 4 I|GM04V           |������ �������� ���������� ��������||
C |L_it          |1 1 I|GM04            |������ �������� ���������� ��������||
C |L_ot          |1 1 I|GM02            |����� �� ������������ ������ ���������||
C |R_av          |4 4 O|KKS             |�����||
C |R_ux          |4 4 I|MAXIMUM         |||
C |R_ebe         |4 4 I|MINIMUM         |||
C |L_obe         |1 1 I|GM01            |����� �� ����������� ������ ���������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R_e,R_i,R0_o,R0_u,R0_ad,R_ed,R_id
      LOGICAL*1 L0_od,L0_ud,L0_af
      INTEGER*4 I0_ef
      LOGICAL*1 L0_if,L0_of,L0_uf
      INTEGER*4 I0_ak,I0_ek,I0_ik,I0_ok,I0_uk,I_al
      LOGICAL*1 L0_el,L0_il,L0_ol,L0_ul,L0_am,L0_em
      REAL*4 R_im
      LOGICAL*1 L0_om
      REAL*4 R_um,R_ap,R_ep
      LOGICAL*1 L0_ip,L0_op,L0_up,L0_ar,L0_er,L0_ir,L0_or
     &,L0_ur,L0_as,L0_es
      REAL*4 R0_is,R_os
      LOGICAL*1 L_us
      REAL*4 R0_at,R_et
      LOGICAL*1 L_it,L_ot
      REAL*4 R0_ut,R_av,R0_ev,R0_iv,R0_ov
      LOGICAL*1 L0_uv,L0_ax
      REAL*4 R0_ex
      LOGICAL*1 L0_ix
      REAL*4 R0_ox,R_ux,R0_abe,R_ebe,R0_ibe
      LOGICAL*1 L_obe

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      !��������� R0_o = DAT_ANA_2_HANDLERC?? /0.3/
      R0_o=R_i
C DAT_ANA_2_HANDLER.fmg( 329, 207):���������
      R0_ad = R_ux + (-R_ebe)
C DAT_ANA_2_HANDLER.fmg( 322, 209):��������
      R0_u = R0_ad * R0_o
C DAT_ANA_2_HANDLER.fmg( 332, 208):����������
      R_e = R0_u + R_ebe
C DAT_ANA_2_HANDLER.fmg( 344, 207):��������
      R0_ibe = R_id * R_ed
C DAT_ANA_2_HANDLER.fmg(  46, 256):����������
      if(L_obe) then
         R0_abe=R_ebe
      else
         R0_abe=R0_ibe
      endif
C DAT_ANA_2_HANDLER.fmg( 120, 255):���� RE IN LO CH7
      if(L_ot) then
         R0_ut=R_ux
      else
         R0_ut=R0_abe
      endif
C DAT_ANA_2_HANDLER.fmg( 131, 254):���� RE IN LO CH7
      if(L_it) then
         R0_at=R_et
      else
         R0_at=R0_ut
      endif
C DAT_ANA_2_HANDLER.fmg( 141, 253):���� RE IN LO CH7
      L0_of=R0_ibe.gt.R_ep
C DAT_ANA_2_HANDLER.fmg( 320, 250):���������� >
      L0_od=R0_ibe.lt.R_im
C DAT_ANA_2_HANDLER.fmg( 320, 244):���������� <
      L0_uf = L0_of.OR.L0_od
C DAT_ANA_2_HANDLER.fmg( 329, 250):���
      L0_ud=R0_ibe.lt.R_um
C DAT_ANA_2_HANDLER.fmg( 353, 240):���������� <
      L0_if=R0_ibe.gt.R_ap
C DAT_ANA_2_HANDLER.fmg( 352, 246):���������� >
      I0_ef = z'008080FF'
C DAT_ANA_2_HANDLER.fmg( 367, 260):��������� ������������� IN (�������)
      I0_ek = z'0080FFFF'
C DAT_ANA_2_HANDLER.fmg( 330, 260):��������� ������������� IN (�������)
      I0_ok = z'00FFC0C0'
C DAT_ANA_2_HANDLER.fmg( 308, 262):��������� ������������� IN (�������)
      I0_uk = z'00989898'
C DAT_ANA_2_HANDLER.fmg( 308, 264):��������� ������������� IN (�������)
      L0_ip=R_et.gt.R_im
C DAT_ANA_2_HANDLER.fmg( 265, 205):���������� >
      L0_op=R_et.gt.R_um
C DAT_ANA_2_HANDLER.fmg( 265, 210):���������� >
      L0_up=R_et.lt.R_ap
C DAT_ANA_2_HANDLER.fmg( 265, 215):���������� <
      L0_ar=R_et.lt.R_ep
C DAT_ANA_2_HANDLER.fmg( 265, 220):���������� <
      L0_er=R_et.lt.R_ux
C DAT_ANA_2_HANDLER.fmg( 265, 228):���������� <
      L0_or=R_et.gt.R_ebe
C DAT_ANA_2_HANDLER.fmg( 265, 238):���������� >
      L0_ir = L0_or.AND.L0_er.AND.L0_ar.AND.L0_up.AND.L0_op.AND.L0_ip
C DAT_ANA_2_HANDLER.fmg( 282, 232):�
      L0_as = L_it.AND.L0_ir
C DAT_ANA_2_HANDLER.fmg( 184, 234):�
      R0_ev = 0.01
C DAT_ANA_2_HANDLER.fmg(  82, 263):��������� (RE4) (�������)
      R0_ov = R_ux + (-R_ebe)
C DAT_ANA_2_HANDLER.fmg(  76, 266):��������
      R0_iv = R0_ov * R0_ev
C DAT_ANA_2_HANDLER.fmg(  85, 264):����������
      R0_ox = R_ux + R0_iv
C DAT_ANA_2_HANDLER.fmg(  91, 276):��������
      L0_ix=R0_ibe.gt.R0_ox
C DAT_ANA_2_HANDLER.fmg(  98, 278):���������� >
      R0_ex = R_ebe + (-R0_iv)
C DAT_ANA_2_HANDLER.fmg(  91, 270):��������
      L0_ax=R0_ibe.lt.R0_ex
C DAT_ANA_2_HANDLER.fmg(  98, 272):���������� <
      L0_uv = L0_ix.OR.L0_ax
C DAT_ANA_2_HANDLER.fmg( 106, 272):���
      L0_af = L0_if.OR.L0_ud.OR.L_obe.OR.L_ot.OR.L0_uv
C DAT_ANA_2_HANDLER.fmg( 368, 242):���
      R0_is = R_os + R_av
C DAT_ANA_2_HANDLER.fmg( 138, 218):��������
C label 81  try81=try81-1
      if(L_us) then
         R_av=R0_is
      else
         R_av=R0_at
      endif
C DAT_ANA_2_HANDLER.fmg( 148, 252):���� RE IN LO CH7
C sav1=R0_is
      R0_is = R_os + R_av
C DAT_ANA_2_HANDLER.fmg( 138, 218):recalc:��������
C if(sav1.ne.R0_is .and. try81.gt.0) goto 81
      L0_em=R_av.gt.R_ebe
C DAT_ANA_2_HANDLER.fmg( 265, 194):���������� >
      L0_am=R_av.lt.R_ux
C DAT_ANA_2_HANDLER.fmg( 265, 185):���������� <
      L0_ol=R_av.lt.R_ap
C DAT_ANA_2_HANDLER.fmg( 265, 172):���������� <
      L0_el=R_av.gt.R_im
C DAT_ANA_2_HANDLER.fmg( 265, 162):���������� >
      L0_il=R_av.gt.R_um
C DAT_ANA_2_HANDLER.fmg( 265, 167):���������� >
      L0_ul=R_av.lt.R_ep
C DAT_ANA_2_HANDLER.fmg( 265, 178):���������� <
      L0_om = L0_em.AND.L0_am.AND.L0_ul.AND.L0_ol.AND.L0_il.AND.L0_el
C DAT_ANA_2_HANDLER.fmg( 282, 190):�
      L0_ur = L_us.AND.L0_om
C DAT_ANA_2_HANDLER.fmg( 184, 227):�
      L0_es = L0_as.OR.L0_ur
C DAT_ANA_2_HANDLER.fmg( 193, 232):���
      if(L0_es) then
         I0_ik=I0_ok
      else
         I0_ik=I0_uk
      endif
C DAT_ANA_2_HANDLER.fmg( 311, 262):���� RE IN LO CH7
      if(L0_uf) then
         I0_ak=I0_ek
      else
         I0_ak=I0_ik
      endif
C DAT_ANA_2_HANDLER.fmg( 334, 261):���� RE IN LO CH7
      if(L0_af) then
         I_al=I0_ef
      else
         I_al=I0_ak
      endif
C DAT_ANA_2_HANDLER.fmg( 370, 260):���� RE IN LO CH7
      End
