      Subroutine PIDmod(ext_deltat,L_i,R_ad,L_ed,R8_od,L_ud
     &,R8_ak,R_uk,R_ul,R_im,R_om,R_um,R_ap,R_ep,R_ip,R_up
     &,L_ur,R_es,R_at)
C |L_i           |1 1 I|fladd           |����� �������||
C |R_ad          |4 4 I|addstp          |��������||
C |L_ed          |1 1 O|tracmod1        |����� ��������||
C |R8_od         |4 8 I|posin           |���� ��������� ������� (��������)||
C |L_ud          |1 1 I|flagmod         |����� ������� �����||
C |R8_ak         |4 8 O|out             |����� ��������� ��������||
C |R_uk          |4 4 I|k4              |�-� ����������� ��||
C |R_ul          |4 4 I|t3              |���������� ������� ���������������||
C |R_im          |4 4 S|_sdif1*         |���������� ��������� |0.0|
C |R_om          |4 4 I|k3              |�-� ���������������||
C |R_um          |4 4 I|u1              |����������� �����������||
C |R_ap          |4 4 I|k2              |�-� ������������||
C |R_ep          |4 4 O|dlt             |������� ��������� ����������||
C |R_ip          |4 4 I|k1              |�-� ����������������||
C |R_up          |4 4 O|_odif1*         |�������� ������ |0.0|
C |L_ur          |1 1 I|tracmod         |����� ��������||
C |R_es          |4 4 S|_oint1*         |����� ����������� |0.0|
C |R_at          |4 4 I|delta           |������� ��������� ����������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R0_e
      LOGICAL*1 L_i
      REAL*4 R0_o,R0_u,R_ad
      LOGICAL*1 L_ed
      REAL*4 R0_id
      REAL*8 R8_od
      LOGICAL*1 L_ud,L0_af
      REAL*4 R0_ef
      LOGICAL*1 L0_if
      REAL*4 R0_of,R0_uf
      REAL*8 R8_ak
      REAL*4 R0_ek,R0_ik,R0_ok,R_uk,R0_al,R0_el,R0_il,R0_ol
     &,R_ul
      LOGICAL*1 L0_am
      REAL*4 R0_em,R_im,R_om,R_um,R_ap,R_ep,R_ip,R0_op,R_up
     &,R0_ar,R0_er,R0_ir,R0_or
      LOGICAL*1 L_ur,L0_as
      REAL*4 R_es,R0_is,R0_os,R0_us,R_at

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_em=R_im
C PIDmod.fmg( 243, 174):pre: ���������������� ����� ,1
      R0_e = deltat
C PIDmod.fmg( 145, 161):��������� (RE4) (�������)
      R0_u = 0.0
C PIDmod.fmg( 133, 164):��������� (RE4) (�������)
      if(L_i) then
         R0_o=R_ad
      else
         R0_o=R0_u
      endif
C PIDmod.fmg( 136, 163):���� RE IN LO CH7
      R0_op = R0_o * R0_e
C PIDmod.fmg( 148, 163):����������
      L_ed=L_ur
C PIDmod.fmg( 134, 186):������,tracmod1
      R0_id = 0.0
C PIDmod.fmg( 135, 202):��������� (RE4) (�������)
      if(L_ur) then
         R0_il=R0_id
      else
         R0_il=R_at
      endif
C PIDmod.fmg( 137, 203):���� RE IN LO CH7
      R0_us = R8_od
C PIDmod.fmg( 160, 214):��������
      R0_ir = 100
C PIDmod.fmg( 233, 218):��������� (RE4) (�������)
      R0_is = -(R0_ir)
C PIDmod.fmg( 244, 213):��������
      R0_ef = 0.001
C PIDmod.fmg( 276, 188):��������� (RE4) (�������)
      R0_of = 0.999
C PIDmod.fmg( 276, 192):��������� (RE4) (�������)
      R0_uf = 0.000001
C PIDmod.fmg( 200, 210):��������� (RE4) (�������)
      L0_am=.false.
C PIDmod.fmg( 237, 169):��������� ���������� (�������)
      R0_ek = 1
C PIDmod.fmg( 264, 203):��������� (RE4) (�������)
      R0_ik = 0
C PIDmod.fmg( 264, 197):��������� (RE4) (�������)
      R0_or = 1
C PIDmod.fmg( 235, 208):��������� (RE4) (�������)
      R0_ar = R_ip * R_ep
C PIDmod.fmg( 200, 224):����������
C label 29  try29=try29-1
      if(R_ep.gt.R0_uf) then
         R0_ol=R_ep-R0_uf
      elseif(R_ep.le.-R0_uf) then
         R0_ol=R_ep+R0_uf
      else
         R0_ol=0.0
      endif
C PIDmod.fmg( 203, 203):���� ������������������
      R0_os = R_ap * R0_ol
C PIDmod.fmg( 221, 202):����������
      if(L0_am) then
         R_up=0.0
         R_im=R_ep
      else
         R_up=(R_om*(R_ep-R0_em)+R_ul*R_up)/(R_ul+deltat)
         R_im=R_ep
      endif
C PIDmod.fmg( 243, 174):���������������� ����� ,1
      R0_er = R0_ar + R_es + R_up + R0_op
C PIDmod.fmg( 259, 201):��������
      R0_ok=MIN(R0_ek,R0_er)
C PIDmod.fmg( 268, 203):�������
      R0_al=MAX(R0_ok,R0_ik)
C PIDmod.fmg( 268, 199):��������
      R0_el = R_uk * R0_al
C PIDmod.fmg( 160, 195):����������
      R_ep = R0_il + (-R0_el)
C PIDmod.fmg( 168, 203):��������
C sav1=R_up
      if(L0_am) then
         R_up=0.0
         R_im=R_ep
      else
         R_up=(R_om*(R_ep-R0_em)+R_ul*R_up)/(R_ul+deltat)
         R_im=R_ep
      endif
C PIDmod.fmg( 243, 174):recalc:���������������� ����� ,1
C if(sav1.ne.R_up .and. try35.gt.0) goto 35
C sav1=R0_ar
      R0_ar = R_ip * R_ep
C PIDmod.fmg( 200, 224):recalc:����������
C if(sav1.ne.R0_ar .and. try29.gt.0) goto 29
      L0_if=R0_al.gt.R0_of
C PIDmod.fmg( 280, 194):���������� >
      L0_af=R0_al.lt.R0_ef
C PIDmod.fmg( 280, 190):���������� <
      L0_as = L0_if.OR.L0_af
C PIDmod.fmg( 286, 191):���
      if(L_ur) then
         R_es=R0_us
      elseif(L0_as) then
         R_es=R_es
      else
         R_es=R_es+deltat/R0_or*R0_os
      endif
      if(R_es.gt.R0_ir) then
         R_es=R0_ir
      elseif(R_es.lt.R0_is) then
         R_es=R0_is
      endif
C PIDmod.fmg( 243, 202):����������,1
C sav1=R0_er
      R0_er = R0_ar + R_es + R_up + R0_op
C PIDmod.fmg( 259, 201):recalc:��������
C if(sav1.ne.R0_er .and. try37.gt.0) goto 37
      if(L_ud) then
         R8_ak=R8_ak
      else
         R8_ak=R0_al
      endif
C PIDmod.fmg( 309, 197):���� RE IN LO CH7
C label 62  try62=try62-1
      End
