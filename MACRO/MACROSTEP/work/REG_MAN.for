      Subroutine REG_MAN(ext_deltat,R_ixe,R8_ime,R_i,R_o,L_u
     &,R_id,R_od,L_ud,R_if,R_uf,R_ak,R_ek,R_ik,R_ok,R_uk,R_al
     &,I_el,L_ol,L_am,L_em,L_im,L_om,L_ap,L_ep,L_ar,L_er,L_ur
     &,L_as,L_is,L_us,R8_et,R_av,R8_ov,L_uv,L_ex,L_obe,I_ube
     &,L_ule,R_ume,R_epe,L_ite,L_axe,L_oxe,L_uxe,L_abi,L_ebi
     &,L_ibi,L_obi)
C |R_ixe         |4 4 I|11 mlfpar19     |��������� ������������|0.6|
C |R8_ime        |4 8 O|16 value        |��� �������� ��������|0.5|
C |R_i           |4 4 S|_simpJ1231*     |[���]���������� ��������� ������������� |0.0|
C |R_o           |4 4 K|_timpJ1231      |[���]������������ �������� �������������|0.5|
C |L_u           |1 1 S|_limpJ1231*     |[TF]���������� ��������� ������������� |F|
C |R_id          |4 4 S|_simpJ1227*     |[���]���������� ��������� ������������� |0.0|
C |R_od          |4 4 K|_timpJ1227      |[���]������������ �������� �������������|0.5|
C |L_ud          |1 1 S|_limpJ1227*     |[TF]���������� ��������� ������������� |F|
C |R_if          |4 4 O|POS_CL          |��������||
C |R_uf          |4 4 O|POS_OP          |��������||
C |R_ak          |4 4 S|vmstop_button_ST*|��������� ������ "������� ���� �� ���������" |0.0|
C |R_ek          |4 4 I|vmstop_button   |������� ������ ������ "������� ���� �� ���������" |0.0|
C |R_ik          |4 4 S|vmclose_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_ok          |4 4 I|vmclose_button  |������� ������ ������ "������� ������� �� ���������" |0.0|
C |R_uk          |4 4 S|vmopen_button_ST*|��������� ������ "������� ������� �� ���������" |0.0|
C |R_al          |4 4 I|vmopen_button   |������� ������ ������ "������� ������� �� ���������" |0.0|
C |I_el          |2 4 O|LZM             |������ "�������"||
C |L_ol          |1 1 I|vlv_kvit        |||
C |L_am          |1 1 I|instr_fault     |||
C |L_em          |1 1 S|_qffJ453*       |�������� ������ Q RS-��������  |F|
C |L_im          |1 1 O|vmclose_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_om          |1 1 O|vmopen_button_CMD*|[TF]����� ������ ������� ������� �� ���������|F|
C |L_ap          |1 1 I|ulubclose       |���������� ������� �� ���������|F|
C |L_ep          |1 1 I|ulubopen        |���������� ������� �� ���������|F|
C |L_ar          |1 1 O|block           |||
C |L_er          |1 1 O|vmstop_button_CMD*|[TF]����� ������ ������� ���� �� ���������|F|
C |L_ur          |1 1 O|STOP            |�������||
C |L_as          |1 1 S|_qff10*         |�������� ������ Q RS-��������  |F|
C |L_is          |1 1 O|norm            |�����||
C |L_us          |1 1 O|nopower         |��� ����������||
C |R8_et         |4 8 I|voltage         |[��]���������� �� ������||
C |R_av          |4 4 I|power           |�������� ��������||
C |R8_ov         |4 8 O|sn_power        |�������� ���� ��|3.0|
C |L_uv          |1 1 I|YA22            |������� ������� �� ����������|F|
C |L_ex          |1 1 I|YA21            |������� ������� �� ����������|F|
C |L_obe         |1 1 O|fault           |�������������||
C |I_ube         |2 4 O|LAM             |������ "�������"||
C |L_ule         |1 1 S|_qff6*          |�������� ������ Q RS-��������  |F|
C |R_ume         |4 4 O|mf_value        |��� ��������� �����|0.5|
C |R_epe         |4 4 I|tcl_top         |����� ����|30.0|
C |L_ite         |1 1 O|XH52            |�� ������� (���)|F|
C |L_axe         |1 1 O|XH51            |�� ������� (���)|F|
C |L_oxe         |1 1 I|mlf24           |��� ���������� � ����������||
C |L_uxe         |1 1 I|mlf23           |������� ������� �����||
C |L_abi         |1 1 I|mlf22           |����� ����� ��������||
C |L_ebi         |1 1 I|mlf04           |�������� �� ��������||
C |L_ibi         |1 1 I|mlf03           |�������� �� ��������||
C |L_obi         |1 1 I|mlf19           |���� ������������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R0_e,R_i,R_o
      LOGICAL*1 L_u,L0_ad
      REAL*4 R0_ed,R_id,R_od
      LOGICAL*1 L_ud,L0_af
      REAL*4 R0_ef,R_if,R0_of,R_uf,R_ak,R_ek,R_ik,R_ok,R_uk
     &,R_al
      INTEGER*4 I_el,I0_il
      LOGICAL*1 L_ol,L0_ul,L_am,L_em,L_im,L_om
      REAL*4 R0_um
      LOGICAL*1 L_ap,L_ep,L0_ip,L0_op,L0_up,L_ar,L_er,L0_ir
     &,L0_or,L_ur,L_as,L0_es,L_is,L0_os,L_us
      REAL*4 R0_at
      REAL*8 R8_et
      LOGICAL*1 L0_it,L0_ot
      REAL*4 R0_ut,R_av,R0_ev,R0_iv
      REAL*8 R8_ov
      LOGICAL*1 L_uv,L0_ax,L_ex,L0_ix
      INTEGER*4 I0_ox,I0_ux,I0_abe,I0_ebe,I0_ibe
      LOGICAL*1 L_obe
      INTEGER*4 I_ube,I0_ade,I0_ede
      LOGICAL*1 L0_ide,L0_ode
      REAL*4 R0_ude,R0_afe
      LOGICAL*1 L0_efe
      REAL*4 R0_ife,R0_ofe,R0_ufe,R0_ake,R0_eke,R0_ike
      LOGICAL*1 L0_oke
      REAL*4 R0_uke,R0_ale,R0_ele,R0_ile
      LOGICAL*1 L0_ole,L_ule
      REAL*4 R0_ame,R0_eme
      REAL*8 R8_ime
      REAL*4 R0_ome,R_ume,R0_ape,R_epe,R0_ipe,R0_ope,R0_upe
     &,R0_are,R0_ere,R0_ire,R0_ore,R0_ure
      LOGICAL*1 L0_ase,L0_ese,L0_ise,L0_ose,L0_use,L0_ate
     &,L0_ete,L_ite,L0_ote,L0_ute,L0_ave,L0_eve,L0_ive,L0_ove
     &,L0_uve,L_axe,L0_exe
      REAL*4 R_ixe
      LOGICAL*1 L_oxe,L_uxe,L_abi,L_ebi,L_ibi,L_obi

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_e=R_i
C REG_MAN.fmg( 115, 190):pre: ������������  �� T
      R0_ed=R_id
C REG_MAN.fmg( 115, 234):pre: ������������  �� T
      I0_ox = z'01000009'
C REG_MAN.fmg( 152, 207):��������� ������������� IN (�������)
      I0_ux = z'01000003'
C REG_MAN.fmg( 152, 209):��������� ������������� IN (�������)
      I0_il = z'0100000E'
C REG_MAN.fmg( 188, 201):��������� ������������� IN (�������)
      R0_ef = 100
C REG_MAN.fmg( 378, 273):��������� (RE4) (�������)
      R0_of = 100
C REG_MAN.fmg( 365, 267):��������� (RE4) (�������)
      L_er=R_ek.ne.R_ak
      R_ak=R_ek
C REG_MAN.fmg(  22, 208):���������� ������������� ������
      L_im=R_ok.ne.R_ik
      R_ik=R_ok
C REG_MAN.fmg(  21, 194):���������� ������������� ������
      L0_ax = (.NOT.L_ap).AND.L_im
C REG_MAN.fmg(  65, 194):�
      L0_ote = L0_ax.OR.L_uv
C REG_MAN.fmg(  69, 192):���
      L_om=R_al.ne.R_uk
      R_uk=R_al
C REG_MAN.fmg(  22, 238):���������� ������������� ������
      L0_ix = L_om.AND.(.NOT.L_ep)
C REG_MAN.fmg(  65, 236):�
      L0_uve = L0_ix.OR.L_ex
C REG_MAN.fmg(  69, 234):���
      L0_ir = L0_uve.OR.L0_ote
C REG_MAN.fmg(  74, 203):���
      L_as=(L_er.or.L_as).and..not.(L0_ir)
      L0_or=.not.L_as
C REG_MAN.fmg( 104, 205):RS �������,10
      L_ur=L_as
C REG_MAN.fmg( 132, 207):������,STOP
      L0_ise = L_as.OR.L_oxe
C REG_MAN.fmg(  96, 214):���
      I0_ibe = z'01000009'
C REG_MAN.fmg( 152, 264):��������� ������������� IN (�������)
      I0_ebe = z'01000003'
C REG_MAN.fmg( 152, 262):��������� ������������� IN (�������)
      I0_ade = z'0100000E'
C REG_MAN.fmg( 166, 256):��������� ������������� IN (�������)
      L_em=(L_am.or.L_em).and..not.(L_ol)
      L0_ul=.not.L_em
C REG_MAN.fmg( 326, 178):RS �������
      L0_up=.false.
C REG_MAN.fmg(  63, 217):��������� ���������� (�������)
      L0_op=.false.
C REG_MAN.fmg(  63, 215):��������� ���������� (�������)
      L0_ip=.false.
C REG_MAN.fmg(  63, 213):��������� ���������� (�������)
      L_ar = L0_up.OR.L0_op.OR.L0_ip.OR.L_ep.OR.L_ap
C REG_MAN.fmg(  67, 213):���
      R0_um = DeltaT
C REG_MAN.fmg( 250, 254):��������� (RE4) (�������)
      if(R_epe.ge.0.0) then
         R0_upe=R0_um/max(R_epe,1.0e-10)
      else
         R0_upe=R0_um/min(R_epe,-1.0e-10)
      endif
C REG_MAN.fmg( 259, 252):�������� ����������
      L0_os =.NOT.(L_ibi.OR.L_ebi.OR.L_obi.OR.L_abi.OR.L_uxe.OR.L_oxe
     &)
C REG_MAN.fmg( 319, 191):���
      L0_es =.NOT.(L0_os)
C REG_MAN.fmg( 328, 185):���
      L_obe = L0_es.OR.L_em
C REG_MAN.fmg( 332, 184):���
      L_is = L0_os.OR.L_am
C REG_MAN.fmg( 328, 190):���
      R0_at = 0.1
C REG_MAN.fmg( 254, 160):��������� (RE4) (�������)
      L_us=R8_et.lt.R0_at
C REG_MAN.fmg( 259, 162):���������� <
      R0_ut = 0.0
C REG_MAN.fmg( 266, 182):��������� (RE4) (�������)
      R0_eke = 0.000001
C REG_MAN.fmg( 354, 208):��������� (RE4) (�������)
      R0_ike = 0.999999
C REG_MAN.fmg( 354, 224):��������� (RE4) (�������)
      R0_ude = 0.0
C REG_MAN.fmg( 296, 244):��������� (RE4) (�������)
      R0_afe = 0.0
C REG_MAN.fmg( 370, 242):��������� (RE4) (�������)
      L0_efe = L_uxe.OR.L_abi
C REG_MAN.fmg( 363, 237):���
      R0_ofe = 1
C REG_MAN.fmg( 348, 252):��������� (RE4) (�������)
      R0_ufe = 0.0
C REG_MAN.fmg( 340, 252):��������� (RE4) (�������)
      R0_ale = 1.0e-10
C REG_MAN.fmg( 318, 228):��������� (RE4) (�������)
      R0_ame = 0.0
C REG_MAN.fmg( 328, 242):��������� (RE4) (�������)
      R0_ipe = DeltaT
C REG_MAN.fmg( 250, 264):��������� (RE4) (�������)
      if(R_epe.ge.0.0) then
         R0_are=R0_ipe/max(R_epe,1.0e-10)
      else
         R0_are=R0_ipe/min(R_epe,-1.0e-10)
      endif
C REG_MAN.fmg( 259, 262):�������� ����������
      R0_ure = 0.0
C REG_MAN.fmg( 282, 244):��������� (RE4) (�������)
      L0_ose = (.NOT.L_ur).AND.L_ite
C REG_MAN.fmg( 102, 166):�
C label 104  try104=try104-1
      if(L_abi) then
         R0_ife=R0_afe
      else
         R0_ife=R8_ime
      endif
C REG_MAN.fmg( 373, 242):���� RE IN LO CH7
      if(.NOT.L0_efe) then
         R_ume=R8_ime
      endif
C REG_MAN.fmg( 384, 252):���� � ������������� �������
      L0_eve = (.NOT.L_ur).AND.L_axe
C REG_MAN.fmg( 104, 260):�
      L0_exe = (.NOT.L_axe).AND.L0_uve
C REG_MAN.fmg( 102, 235):�
      L0_ove = L_axe.OR.L0_ise.OR.L0_ote
C REG_MAN.fmg( 102, 229):���
      L0_af = L0_exe.AND.(.NOT.L0_ove)
C REG_MAN.fmg( 108, 234):�
      if(L0_af.and..not.L_ud) then
         R_id=R_od
      else
         R_id=max(R0_ed-deltat,0.0)
      endif
      L0_ave=R_id.gt.0.0
      L_ud=L0_af
C REG_MAN.fmg( 115, 234):������������  �� T
      L0_ive = (.NOT.L0_eve).AND.L0_ave
C REG_MAN.fmg( 128, 235):�
      L0_ode = L0_ive.AND.(.NOT.L_ebi)
C REG_MAN.fmg( 252, 242):�
      L0_ese = L0_ode.OR.L_ibi
C REG_MAN.fmg( 265, 241):���
      if(L0_ese) then
         R0_ire=R0_are
      else
         R0_ire=R0_ure
      endif
C REG_MAN.fmg( 287, 254):���� RE IN LO CH7
      L0_ute = L0_ote.AND.(.NOT.L_ite)
C REG_MAN.fmg( 102, 191):�
      L0_ate = L0_ise.OR.L_ite.OR.L0_uve
C REG_MAN.fmg( 102, 185):���
      L0_ad = L0_ute.AND.(.NOT.L0_ate)
C REG_MAN.fmg( 108, 190):�
      if(L0_ad.and..not.L_u) then
         R_i=R_o
      else
         R_i=max(R0_e-deltat,0.0)
      endif
      L0_ete=R_i.gt.0.0
      L_u=L0_ad
C REG_MAN.fmg( 115, 190):������������  �� T
      L0_use = L0_ete.AND.(.NOT.L0_ose)
C REG_MAN.fmg( 128, 189):�
      L0_ide = L0_use.AND.(.NOT.L_ibi)
C REG_MAN.fmg( 252, 228):�
      L0_ase = L0_ide.OR.L_ebi
C REG_MAN.fmg( 265, 227):���
      if(L0_ase) then
         R0_ere=R0_upe
      else
         R0_ere=R0_ure
      endif
C REG_MAN.fmg( 287, 236):���� RE IN LO CH7
      R0_ore = R0_ire + (-R0_ere)
C REG_MAN.fmg( 293, 246):��������
      if(L_oxe) then
         R0_eme=R0_ude
      else
         R0_eme=R0_ore
      endif
C REG_MAN.fmg( 300, 244):���� RE IN LO CH7
      R0_ile = R_ume + (-R_ixe)
C REG_MAN.fmg( 308, 235):��������
      R0_uke = R0_eme + R0_ile
C REG_MAN.fmg( 314, 236):��������
      R0_ele = R0_uke * R0_ile
C REG_MAN.fmg( 318, 235):����������
      L0_ole=R0_ele.lt.R0_ale
C REG_MAN.fmg( 323, 234):���������� <
      L_ule=(L0_ole.or.L_ule).and..not.(.NOT.L_obi)
      L0_oke=.not.L_ule
C REG_MAN.fmg( 330, 232):RS �������,6
      if(L_ule) then
         R_ume=R_ixe
      endif
C REG_MAN.fmg( 324, 252):���� � ������������� �������
      if(L_ule) then
         R0_ome=R0_ame
      else
         R0_ome=R0_eme
      endif
C REG_MAN.fmg( 332, 244):���� RE IN LO CH7
      R0_ape = R_ume + R0_ome
C REG_MAN.fmg( 338, 245):��������
      R0_ake=MAX(R0_ufe,R0_ape)
C REG_MAN.fmg( 346, 246):��������
      R0_ope=MIN(R0_ofe,R0_ake)
C REG_MAN.fmg( 354, 247):�������
      L_ite=R0_ope.lt.R0_eke
C REG_MAN.fmg( 363, 210):���������� <
C sav1=L0_ate
      L0_ate = L0_ise.OR.L_ite.OR.L0_uve
C REG_MAN.fmg( 102, 185):recalc:���
C if(sav1.ne.L0_ate .and. try147.gt.0) goto 147
C sav1=L0_ute
      L0_ute = L0_ote.AND.(.NOT.L_ite)
C REG_MAN.fmg( 102, 191):recalc:�
C if(sav1.ne.L0_ute .and. try145.gt.0) goto 145
C sav1=L0_ose
      L0_ose = (.NOT.L_ur).AND.L_ite
C REG_MAN.fmg( 102, 166):recalc:�
C if(sav1.ne.L0_ose .and. try104.gt.0) goto 104
      if(L0_efe) then
         R8_ime=R0_ife
      else
         R8_ime=R0_ope
      endif
C REG_MAN.fmg( 377, 246):���� RE IN LO CH7
      R_uf = R0_of * R8_ime
C REG_MAN.fmg( 368, 266):����������
      R_if = R0_ef + (-R_uf)
C REG_MAN.fmg( 382, 272):��������
      L_axe=R0_ope.gt.R0_ike
C REG_MAN.fmg( 363, 225):���������� >
C sav1=L0_ove
      L0_ove = L_axe.OR.L0_ise.OR.L0_ote
C REG_MAN.fmg( 102, 229):recalc:���
C if(sav1.ne.L0_ove .and. try126.gt.0) goto 126
C sav1=L0_exe
      L0_exe = (.NOT.L_axe).AND.L0_uve
C REG_MAN.fmg( 102, 235):recalc:�
C if(sav1.ne.L0_exe .and. try124.gt.0) goto 124
C sav1=L0_eve
      L0_eve = (.NOT.L_ur).AND.L_axe
C REG_MAN.fmg( 104, 260):recalc:�
C if(sav1.ne.L0_eve .and. try119.gt.0) goto 119
      if(L0_efe) then
         R_ume=R0_ope
      endif
C REG_MAN.fmg( 366, 252):���� � ������������� �������
C sav1=R0_ile
      R0_ile = R_ume + (-R_ixe)
C REG_MAN.fmg( 308, 235):recalc:��������
C if(sav1.ne.R0_ile .and. try171.gt.0) goto 171
C sav1=R0_ape
      R0_ape = R_ume + R0_ome
C REG_MAN.fmg( 338, 245):recalc:��������
C if(sav1.ne.R0_ape .and. try188.gt.0) goto 188
      L0_it = L0_ive.OR.L0_use
C REG_MAN.fmg( 251, 174):���
      L0_ot = L0_it.AND.(.NOT.L_us)
C REG_MAN.fmg( 266, 173):�
      if(L0_ot) then
         R0_iv=R_av
      else
         R0_iv=R0_ut
      endif
C REG_MAN.fmg( 269, 180):���� RE IN LO CH7
      if(L0_eve) then
         I0_abe=I0_ox
      else
         I0_abe=I0_ux
      endif
C REG_MAN.fmg( 155, 208):���� RE IN LO CH7
      if(L_obe) then
         I_el=I0_il
      else
         I_el=I0_abe
      endif
C REG_MAN.fmg( 191, 206):���� RE IN LO CH7
      if(L0_ose) then
         I0_ede=I0_ebe
      else
         I0_ede=I0_ibe
      endif
C REG_MAN.fmg( 155, 262):���� RE IN LO CH7
      if(L_obe) then
         I_ube=I0_ade
      else
         I_ube=I0_ede
      endif
C REG_MAN.fmg( 170, 262):���� RE IN LO CH7
      R0_ev = R8_ov
C REG_MAN.fmg( 264, 190):��������
C label 247  try247=try247-1
      R8_ov = R0_iv + R0_ev
C REG_MAN.fmg( 275, 189):��������
C sav1=R0_ev
      R0_ev = R8_ov
C REG_MAN.fmg( 264, 190):recalc:��������
C if(sav1.ne.R0_ev .and. try247.gt.0) goto 247
      End
