      Subroutine DAT_DISCR_EMERG_HANDLER(ext_deltat,L_e,R_i
     &,R_o,R_ed,R_id,L_ud,L_ek,L_ik,L_al,I_am)
C |L_e           |1 1 O|input_button_CMD*|[TF]����� ������ ������ ����|F|
C |R_i           |4 4 S|input_button_ST*|��������� ������ "������ ����" |0.0|
C |R_o           |4 4 I|input_button    |������� ������ ������ "������ ����" |0.0|
C |R_ed          |4 4 S|kvit_button_ST* |��������� ������ "������������" |0.0|
C |R_id          |4 4 I|kvit_button     |������� ������ ������ "������������" |0.0|
C |L_ud          |1 1 S|_qffJ764*       |�������� ������ Q RS-��������  |F|
C |L_ek          |1 1 O|kvit_button_CMD*|[TF]����� ������ ������������|F|
C |L_ik          |1 1 S|_qffJ745*       |�������� ������ Q RS-��������  |F|
C |L_al          |1 1 I|input           |����||
C |I_am          |2 4 O|LS              |��������� �������||

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      LOGICAL*1 L_e
      REAL*4 R_i,R_o
      LOGICAL*1 L0_u
      INTEGER*4 I0_ad
      REAL*4 R_ed,R_id
      LOGICAL*1 L0_od,L_ud,L0_af
      INTEGER*4 I0_ef,I0_if,I0_of
      LOGICAL*1 L0_uf,L0_ak,L_ek,L_ik,L0_ok
      INTEGER*4 I0_uk
      LOGICAL*1 L_al,L0_el
      INTEGER*4 I0_il,I0_ol,I0_ul,I_am

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I0_ad = z'0100000A'
C DAT_DISCR_EMERG_HANDLER.fmg( 230, 177):��������� ������������� IN (�������)
      I0_ol = z'0100000A'
C DAT_DISCR_EMERG_HANDLER.fmg( 176, 182):��������� ������������� IN (�������)
      L_e=R_o.ne.R_i
      R_i=R_o
C DAT_DISCR_EMERG_HANDLER.fmg( 144, 180):���������� ������������� ������
      L0_ak = L_e.OR.L_al
C DAT_DISCR_EMERG_HANDLER.fmg( 153, 174):���
      L_ek=R_id.ne.R_ed
      R_ed=R_id
C DAT_DISCR_EMERG_HANDLER.fmg( 132, 170):���������� ������������� ������
      L_ik=(L0_ak.or.L_ik).and..not.(L_ek)
      L0_ok=.not.L_ik
C DAT_DISCR_EMERG_HANDLER.fmg( 188, 172):RS �������
      L0_uf = (.NOT.L0_ak).AND.L_ik
C DAT_DISCR_EMERG_HANDLER.fmg( 196, 175):�
      L_ud=(L_ek.or.L_ud).and..not.(.NOT.L0_ak)
      L0_od=.not.L_ud
C DAT_DISCR_EMERG_HANDLER.fmg( 168, 163):RS �������
      L0_el = (.NOT.L_ud).AND.L0_ak
C DAT_DISCR_EMERG_HANDLER.fmg( 177, 164):�
      L0_af = L_al.AND.L_ud
C DAT_DISCR_EMERG_HANDLER.fmg( 216, 170):�
      L0_u = (.NOT.L_al).AND.L_ud
C DAT_DISCR_EMERG_HANDLER.fmg( 232, 166):�
      I0_if = z'01000007'
C DAT_DISCR_EMERG_HANDLER.fmg( 214, 178):��������� ������������� IN (�������)
      I0_uk = z'01000097'
C DAT_DISCR_EMERG_HANDLER.fmg( 198, 179):��������� ������������� IN (�������)
      I0_ul = z'01000096'
C DAT_DISCR_EMERG_HANDLER.fmg( 176, 180):��������� ������������� IN (�������)
      if(L0_el) then
         I0_il=I0_ul
      else
         I0_il=I0_ol
      endif
C DAT_DISCR_EMERG_HANDLER.fmg( 179, 180):���� RE IN LO CH7
      if(L0_uf) then
         I0_of=I0_uk
      else
         I0_of=I0_il
      endif
C DAT_DISCR_EMERG_HANDLER.fmg( 202, 180):���� RE IN LO CH7
      if(L0_af) then
         I0_ef=I0_if
      else
         I0_ef=I0_of
      endif
C DAT_DISCR_EMERG_HANDLER.fmg( 218, 178):���� RE IN LO CH7
      if(L0_u) then
         I_am=I0_ad
      else
         I_am=I0_ef
      endif
C DAT_DISCR_EMERG_HANDLER.fmg( 234, 178):���� RE IN LO CH7
      End
