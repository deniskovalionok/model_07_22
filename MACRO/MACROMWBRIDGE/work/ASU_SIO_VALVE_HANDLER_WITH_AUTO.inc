      Interface
      Subroutine ASU_SIO_VALVE_HANDLER_WITH_AUTO(ext_deltat
     &,L_id,I_af,I_ef,L_uf,L_ak,I_al,I_ul,I_am,I_im,I_om,L_ar
     &,L_ur,R8_os,R_ot,R_av,I_uv,R_ex,I_ux,R_ebe,R_ibe,R_obe
     &,R_ube,R_ade)
C |L_id          |1 1 S|_qffJ309*       |�������� ������ Q RS-��������  |F|
C |I_af          |2 4 I|AUTO_HAND       |����/����||
C |I_ef          |2 4 O|XA41            |���������� ������ ���||
C |L_uf          |1 1 S|_splsJ278*      |[TF]���������� ��������� ������������� |F|
C |L_ak          |1 1 S|_splsJ273*      |[TF]���������� ��������� ������������� |F|
C |I_al          |2 4 I|USER_STOP       |������ ����. ������� �� ���������||
C |I_ul          |2 4 I|USER_CLOSE      |������� �������. ������� �� ���������||
C |I_am          |2 4 I|YH22            |������� �������. ������� �� ����������||
C |I_im          |2 4 I|USER_OPEN       |������� �������. ������� �� ���������||
C |I_om          |2 4 I|YH21            |������� �������. ������� �� ����������||
C |L_ar          |1 1 S|_qffJ217*       |�������� ������ Q RS-��������  |F|
C |L_ur          |1 1 S|_qffJ216*       |�������� ������ Q RS-��������  |F|
C |R8_os         |4 8 O|POS             |��������� ��������||
C |R_ot          |4 4 K|_lcmpJ49        |[]�������� ������ �����������|0.99999|
C |R_av          |4 4 K|_lcmpJ48        |[]�������� ������ �����������|0.000001|
C |I_uv          |2 4 O|XH52            |�������� �������||
C |R_ex          |4 4 K|_lcmpJ22        |[]�������� ������ �����������|0.000001|
C |I_ux          |2 4 O|XH51            |�������� �������||
C |R_ebe         |4 4 K|_lcmpJ19        |[]�������� ������ �����������|0.99999|
C |R_ibe         |4 4 K|_uintJ2         |����������� ������ ����������� ������|1.0|
C |R_obe         |4 4 K|_tintJ2         |[���]�������� T �����������|20.0|
C |R_ube         |4 4 O|_ointJ2*        |�������� ������ ����������� |0.0|
C |R_ade         |4 4 K|_lintJ2         |����������� ������ ����������� �����|0.0|

      IMPLICIT NONE
      REAL*4 ext_deltat
      LOGICAL*1 L_id
      INTEGER*4 I_af,I_ef
      LOGICAL*1 L_uf,L_ak
      INTEGER*4 I_al,I_ul,I_am,I_im,I_om
      LOGICAL*1 L_ar,L_ur
      REAL*8 R8_os
      REAL*4 R_ot,R_av
      INTEGER*4 I_uv
      REAL*4 R_ex
      INTEGER*4 I_ux
      REAL*4 R_ebe,R_ibe,R_obe,R_ube,R_ade
      End subroutine ASU_SIO_VALVE_HANDLER_WITH_AUTO
      End interface
