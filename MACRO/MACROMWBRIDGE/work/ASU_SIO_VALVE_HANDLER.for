      Subroutine ASU_SIO_VALVE_HANDLER(ext_deltat,R_e,R_i
     &,R_o,R_u,I_id,L_ek,I_ik,I_al,L_ol,L_ul,I_um,L_ap,I_up
     &,I_ar,I_ir,I_or,L_at,L_ut,R8_ov,I_obe,I_ide,R_ode,R_ude
     &,R_afe,R_efe)
C |R_e           |4 4 K|_lcmpJ389       |[]�������� ������ �����������|0.000001|
C |R_i           |4 4 K|_lcmpJ388       |[]�������� ������ �����������|0.99999|
C |R_o           |4 4 K|_lcmpJ387       |[]�������� ������ �����������|0.99999|
C |R_u           |4 4 K|_lcmpJ386       |[]�������� ������ �����������|0.000001|
C |I_id          |2 4 O|XH54            |�������||
C |L_ek          |1 1 S|_qffJ309*       |�������� ������ Q RS-��������  |F|
C |I_ik          |2 4 O|XA41            |���������� ������ ���||
C |I_al          |2 4 I|AUTO_HAND       |����/����||
C |L_ol          |1 1 S|_splsJ278*      |[TF]���������� ��������� ������������� |F|
C |L_ul          |1 1 S|_splsJ273*      |[TF]���������� ��������� ������������� |F|
C |I_um          |2 4 I|USER_STOP       |������ ����. ������� �� ���������||
C |L_ap          |1 1 I|mlf01           |��� �������||
C |I_up          |2 4 I|USER_CLOSE      |������� �������. ������� �� ���������||
C |I_ar          |2 4 I|YH22            |������� �������. ������� �� ����������||
C |I_ir          |2 4 I|USER_OPEN       |������� �������. ������� �� ���������||
C |I_or          |2 4 I|YH21            |������� �������. ������� �� ����������||
C |L_at          |1 1 S|_qffJ217*       |�������� ������ Q RS-��������  |F|
C |L_ut          |1 1 S|_qffJ216*       |�������� ������ Q RS-��������  |F|
C |R8_ov         |4 8 O|POS             |��������� ��������||
C |I_obe         |2 4 O|XH52            |�������� �������||
C |I_ide         |2 4 O|XH51            |�������� �������||
C |R_ode         |4 4 K|_uintJ2         |����������� ������ ����������� ������|1.0|
C |R_ude         |4 4 K|_tintJ2         |[���]�������� T �����������|20.0|
C |R_afe         |4 4 O|_ointJ2*        |�������� ������ ����������� |0.0|
C |R_efe         |4 4 K|_lintJ2         |����������� ������ ����������� �����|0.0|

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      REAL*4 R_e,R_i,R_o,R_u
      INTEGER*4 I0_ad,I0_ed,I_id
      LOGICAL*1 L0_od,L0_ud,L0_af,L0_ef,L0_if,L0_of,L0_uf
     &,L0_ak,L_ek
      INTEGER*4 I_ik,I0_ok,I0_uk,I_al
      LOGICAL*1 L0_el,L0_il,L_ol,L_ul,L0_am,L0_em,L0_im,L0_om
      INTEGER*4 I_um
      LOGICAL*1 L_ap,L0_ep,L0_ip,L0_op
      INTEGER*4 I_up,I_ar
      LOGICAL*1 L0_er
      INTEGER*4 I_ir,I_or
      LOGICAL*1 L0_ur,L0_as,L0_es,L0_is,L0_os,L0_us,L_at
      REAL*4 R0_et,R0_it,R0_ot
      LOGICAL*1 L_ut
      REAL*4 R0_av,R0_ev,R0_iv
      REAL*8 R8_ov
      LOGICAL*1 L0_uv,L0_ax,L0_ex
      INTEGER*4 I0_ix,I0_ox
      LOGICAL*1 L0_ux,L0_abe
      INTEGER*4 I0_ebe,I0_ibe,I_obe
      LOGICAL*1 L0_ube
      INTEGER*4 I0_ade,I0_ede,I_ide
      REAL*4 R_ode,R_ude,R_afe,R_efe,R0_ife

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I0_ed = 1
C ASU_SIO_VALVE_HANDLER.fmg( 173, 267):��������� ������������� IN (�������)
      I0_ad = 0
C ASU_SIO_VALVE_HANDLER.fmg( 173, 264):��������� ������������� IN (�������)
      if(L_ap) then
         I_id=I0_ad
      else
         I_id=I0_ed
      endif
C ASU_SIO_VALVE_HANDLER.fmg( 179, 264):���� RE IN LO CH7
      L0_od=.false.
C ASU_SIO_VALVE_HANDLER.fmg( 158, 224):��������� ���������� (�������)
      L0_of=I_al.ne.0
C ASU_SIO_VALVE_HANDLER.fmg( 108, 176):��������� 1->LO
      I0_ok = 0
C ASU_SIO_VALVE_HANDLER.fmg( 135, 184):��������� ������������� IN (�������)
      I0_uk = 1
C ASU_SIO_VALVE_HANDLER.fmg( 135, 186):��������� ������������� IN (�������)
      L0_el=I_ar.ne.0
C ASU_SIO_VALVE_HANDLER.fmg( 154, 215):��������� 1->LO
      L0_il=I_or.ne.0
C ASU_SIO_VALVE_HANDLER.fmg( 154, 248):��������� 1->LO
      L0_om=I_um.ne.0
C ASU_SIO_VALVE_HANDLER.fmg( 154, 222):��������� 1->LO
      L0_op=I_up.ne.0
C ASU_SIO_VALVE_HANDLER.fmg( 154, 203):��������� 1->LO
      L0_er=I_ir.ne.0
C ASU_SIO_VALVE_HANDLER.fmg( 154, 235):��������� 1->LO
      R0_ot = 0.0
C ASU_SIO_VALVE_HANDLER.fmg( 212, 239):��������� (RE4) (�������)
      R0_it = -1.0
C ASU_SIO_VALVE_HANDLER.fmg( 212, 237):��������� (RE4) (�������)
      R0_iv = 0.0
C ASU_SIO_VALVE_HANDLER.fmg( 212, 252):��������� (RE4) (�������)
      R0_ev = 1.0
C ASU_SIO_VALVE_HANDLER.fmg( 212, 250):��������� (RE4) (�������)
      I0_ox = 0
C ASU_SIO_VALVE_HANDLER.fmg( 264, 263):��������� ������������� IN (�������)
      I0_ix = 1
C ASU_SIO_VALVE_HANDLER.fmg( 264, 261):��������� ������������� IN (�������)
      I0_ibe = 0
C ASU_SIO_VALVE_HANDLER.fmg( 264, 245):��������� ������������� IN (�������)
      I0_ebe = 1
C ASU_SIO_VALVE_HANDLER.fmg( 264, 243):��������� ������������� IN (�������)
      I0_ade = 2
C ASU_SIO_VALVE_HANDLER.fmg( 264, 251):��������� ������������� IN (�������)
      L0_if=I_ik.ne.0
C ASU_SIO_VALVE_HANDLER.fmg( 108, 171):��������� 1->LO
C label 39  try39=try39-1
      L0_uf = L0_of.AND.(.NOT.L0_if)
C ASU_SIO_VALVE_HANDLER.fmg( 119, 172):�
      L_ek=(L0_of.or.L_ek).and..not.(L0_uf)
      L0_ak=.not.L_ek
C ASU_SIO_VALVE_HANDLER.fmg( 129, 174):RS �������
      if(L_ek) then
         I_ik=I0_ok
      else
         I_ik=I0_uk
      endif
C ASU_SIO_VALVE_HANDLER.fmg( 140, 184):���� RE IN LO CH7
      L0_ef=I_ik.ne.0
C ASU_SIO_VALVE_HANDLER.fmg( 154, 195):��������� 1->LO
      if(.NOT.L0_ef) then
         L0_ud=L0_er
      else
         L0_ud=L0_il
      endif
C ASU_SIO_VALVE_HANDLER.fmg( 163, 244):���� RE IN LO CH7
      L0_is = L0_ud.AND.(.NOT.L_ap)
C ASU_SIO_VALVE_HANDLER.fmg( 177, 244):�
      if(.NOT.L0_ef) then
         L0_af=L0_op
      else
         L0_af=L0_el
      endif
C ASU_SIO_VALVE_HANDLER.fmg( 163, 213):���� RE IN LO CH7
      L0_ur = L0_af.AND.(.NOT.L_ap)
C ASU_SIO_VALVE_HANDLER.fmg( 177, 213):�
      if(.NOT.L0_ef) then
         L0_ep=L0_om
      else
         L0_ep=L0_od
      endif
C ASU_SIO_VALVE_HANDLER.fmg( 163, 222):���� RE IN LO CH7
      L0_am=L_at.and..not.L_ol
      L_ol=L_at
C ASU_SIO_VALVE_HANDLER.fmg( 185, 239):������������  �� 1 ���
C label 66  try66=try66-1
      if(L_ut) then
         R0_av=R0_ev
      else
         R0_av=R0_iv
      endif
C ASU_SIO_VALVE_HANDLER.fmg( 216, 251):���� RE IN LO CH7
      if(L_at) then
         R0_et=R0_it
      else
         R0_et=R0_ot
      endif
C ASU_SIO_VALVE_HANDLER.fmg( 216, 238):���� RE IN LO CH7
      R0_ife = R0_av + R0_et
C ASU_SIO_VALVE_HANDLER.fmg( 224, 244):��������
      R_afe=R_afe+deltat/R_ude*R0_ife
      if(R_afe.gt.R_ode) then
         R_afe=R_ode
      elseif(R_afe.lt.R_efe) then
         R_afe=R_efe
      endif
C ASU_SIO_VALVE_HANDLER.fmg( 235, 242):����������
      L0_abe=R_afe.lt.R_e
C ASU_SIO_VALVE_HANDLER.fmg( 256, 240):���������� <
      L0_ube=R_afe.gt.R_i
C ASU_SIO_VALVE_HANDLER.fmg( 256, 248):���������� >
      L0_ux = L0_abe.OR.L0_ube
C ASU_SIO_VALVE_HANDLER.fmg( 244, 196):���
      L0_ip = L0_ep.OR.L_ap.OR.L0_ux
C ASU_SIO_VALVE_HANDLER.fmg( 177, 230):���
      L0_em = L_ut.AND.L_at
C ASU_SIO_VALVE_HANDLER.fmg( 205, 227):�
      L0_os = L0_ip.OR.L0_am.OR.L0_em
C ASU_SIO_VALVE_HANDLER.fmg( 191, 239):���
      L_ut=L0_is.or.(L_ut.and..not.(L0_os))
      L0_us=.not.L_ut
C ASU_SIO_VALVE_HANDLER.fmg( 198, 242):RS �������
      L0_im=L_ut.and..not.L_ul
      L_ul=L_ut
C ASU_SIO_VALVE_HANDLER.fmg( 185, 210):������������  �� 1 ���
      L0_as = L0_im.OR.L0_ip.OR.L0_em
C ASU_SIO_VALVE_HANDLER.fmg( 191, 208):���
      L_at=L0_ur.or.(L_at.and..not.(L0_as))
      L0_es=.not.L_at
C ASU_SIO_VALVE_HANDLER.fmg( 198, 211):RS �������
C sav1=L0_em
      L0_em = L_ut.AND.L_at
C ASU_SIO_VALVE_HANDLER.fmg( 205, 227):recalc:�
C if(sav1.ne.L0_em .and. try92.gt.0) goto 92
      if(L0_abe) then
         I_obe=I0_ebe
      else
         I_obe=I0_ibe
      endif
C ASU_SIO_VALVE_HANDLER.fmg( 267, 243):���� RE IN LO CH7
      R8_ov = R_afe
C ASU_SIO_VALVE_HANDLER.fmg( 258, 274):��������
      L0_uv=R_afe.lt.R_o
C ASU_SIO_VALVE_HANDLER.fmg( 256, 254):���������� <
      L0_ax=R_afe.gt.R_u
C ASU_SIO_VALVE_HANDLER.fmg( 256, 260):���������� >
      L0_ex = L0_ax.AND.L0_uv
C ASU_SIO_VALVE_HANDLER.fmg( 261, 257):�
      if(L0_ex) then
         I0_ede=I0_ix
      else
         I0_ede=I0_ox
      endif
C ASU_SIO_VALVE_HANDLER.fmg( 267, 261):���� RE IN LO CH7
      if(L0_ube) then
         I_ide=I0_ade
      else
         I_ide=I0_ede
      endif
C ASU_SIO_VALVE_HANDLER.fmg( 267, 251):���� RE IN LO CH7
      End
