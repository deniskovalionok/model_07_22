      Subroutine ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION(ext_deltat
     &,I_o,L_if,I_ek,L_uk,L_al,I_am,L_em,I_ap,I_ep,I_op,I_up
     &,L_es,L_at,R8_ut,R_uv,R_ex,I_abe,R_ibe,I_ade,R_ide,R_ode
     &,R_ude,R_afe,R_efe)
C |I_o           |2 4 O|XH54            |�������||
C |L_if          |1 1 S|_qffJ309*       |�������� ������ Q RS-��������  |F|
C |I_ek          |2 4 I|AUTO_HAND       |����/����||
C |L_uk          |1 1 S|_splsJ278*      |[TF]���������� ��������� ������������� |F|
C |L_al          |1 1 S|_splsJ273*      |[TF]���������� ��������� ������������� |F|
C |I_am          |2 4 I|USER_STOP       |������ ����. ������� �� ���������||
C |L_em          |1 1 I|mlf01           |��� �������||
C |I_ap          |2 4 I|USER_CLOSE      |������� �������. ������� �� ���������||
C |I_ep          |2 4 I|YH22            |������� �������. ������� �� ����������||
C |I_op          |2 4 I|USER_OPEN       |������� �������. ������� �� ���������||
C |I_up          |2 4 I|YH21            |������� �������. ������� �� ����������||
C |L_es          |1 1 S|_qffJ217*       |�������� ������ Q RS-��������  |F|
C |L_at          |1 1 S|_qffJ216*       |�������� ������ Q RS-��������  |F|
C |R8_ut         |4 8 O|POS             |��������� ��������||
C |R_uv          |4 4 K|_lcmpJ49        |[]�������� ������ �����������|0.99999|
C |R_ex          |4 4 K|_lcmpJ48        |[]�������� ������ �����������|0.000001|
C |I_abe         |2 4 O|XH52            |�������� �������||
C |R_ibe         |4 4 K|_lcmpJ22        |[]�������� ������ �����������|0.000001|
C |I_ade         |2 4 O|XH51            |�������� �������||
C |R_ide         |4 4 K|_lcmpJ19        |[]�������� ������ �����������|0.99999|
C |R_ode         |4 4 K|_uintJ2         |����������� ������ ����������� ������|1.0|
C |R_ude         |4 4 K|_tintJ2         |[���]�������� T �����������|20.0|
C |R_afe         |4 4 O|_ointJ2*        |�������� ������ ����������� |0.0|
C |R_efe         |4 4 K|_lintJ2         |����������� ������ ����������� �����|0.0|

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      INTEGER*4 I0_e,I0_i,I_o
      LOGICAL*1 L0_u,L0_ad,L0_ed,L0_id,L0_od,L0_ud,L0_af,L0_ef
     &,L_if
      INTEGER*4 I0_of,I0_uf,I0_ak,I_ek
      LOGICAL*1 L0_ik,L0_ok,L_uk,L_al,L0_el,L0_il,L0_ol,L0_ul
      INTEGER*4 I_am
      LOGICAL*1 L_em,L0_im,L0_om,L0_um
      INTEGER*4 I_ap,I_ep
      LOGICAL*1 L0_ip
      INTEGER*4 I_op,I_up
      LOGICAL*1 L0_ar,L0_er,L0_ir,L0_or,L0_ur,L0_as,L_es
      REAL*4 R0_is,R0_os,R0_us
      LOGICAL*1 L_at
      REAL*4 R0_et,R0_it,R0_ot
      REAL*8 R8_ut
      LOGICAL*1 L0_av
      INTEGER*4 I0_ev,I0_iv
      LOGICAL*1 L0_ov
      REAL*4 R_uv
      LOGICAL*1 L0_ax
      REAL*4 R_ex
      LOGICAL*1 L0_ix
      INTEGER*4 I0_ox,I0_ux,I_abe
      LOGICAL*1 L0_ebe
      REAL*4 R_ibe
      INTEGER*4 I0_obe,I0_ube,I_ade
      LOGICAL*1 L0_ede
      REAL*4 R_ide,R_ode,R_ude,R_afe,R_efe,R0_ife

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      I0_i = 1
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 173, 267):��������� ������������� IN (�������)
      I0_e = 0
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 173, 264):��������� ������������� IN (�������)
      if(L_em) then
         I_o=I0_e
      else
         I_o=I0_i
      endif
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 179, 264):���� RE IN LO CH7
      L0_u=.false.
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 158, 224):��������� ���������� (�������)
      L0_ud=I_ek.ne.0
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 108, 176):��������� 1->LO
      I0_uf = 0
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 135, 184):��������� ������������� IN (�������)
      I0_ak = 1
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 135, 186):��������� ������������� IN (�������)
      L0_ik=I_ep.ne.0
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 154, 215):��������� 1->LO
      L0_ok=I_up.ne.0
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 154, 248):��������� 1->LO
      L0_ul=I_am.ne.0
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 154, 222):��������� 1->LO
      L0_um=I_ap.ne.0
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 154, 203):��������� 1->LO
      L0_ip=I_op.ne.0
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 154, 235):��������� 1->LO
      R0_us = 0.0
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 212, 239):��������� (RE4) (�������)
      R0_os = -1.0
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 212, 237):��������� (RE4) (�������)
      R0_ot = 0.0
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 212, 252):��������� (RE4) (�������)
      R0_it = 1.0
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 212, 250):��������� (RE4) (�������)
      I0_iv = 0
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 264, 263):��������� ������������� IN (�������)
      I0_ev = 1
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 264, 261):��������� ������������� IN (�������)
      I0_ux = 0
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 264, 245):��������� ������������� IN (�������)
      I0_ox = 1
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 264, 243):��������� ������������� IN (�������)
      I0_obe = 2
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 264, 251):��������� ������������� IN (�������)
      if(L_if) then
         I0_of=I0_uf
      else
         I0_of=I0_ak
      endif
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 140, 184):���� RE IN LO CH7
C label 39  try39=try39-1
      L0_od=I0_of.ne.0
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 108, 171):��������� 1->LO
      L0_af = L0_ud.AND.(.NOT.L0_od)
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 119, 172):�
      L_if=(L0_ud.or.L_if).and..not.(L0_af)
      L0_ef=.not.L_if
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 129, 174):RS �������
      L0_id=I0_of.ne.0
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 154, 195):��������� 1->LO
      if(.NOT.L0_id) then
         L0_ad=L0_ip
      else
         L0_ad=L0_ok
      endif
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 163, 244):���� RE IN LO CH7
      L0_or = L0_ad.AND.(.NOT.L_em)
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 177, 244):�
      if(.NOT.L0_id) then
         L0_ed=L0_um
      else
         L0_ed=L0_ik
      endif
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 163, 213):���� RE IN LO CH7
      L0_ar = L0_ed.AND.(.NOT.L_em)
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 177, 213):�
      if(.NOT.L0_id) then
         L0_im=L0_ul
      else
         L0_im=L0_u
      endif
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 163, 222):���� RE IN LO CH7
      L0_el=L_es.and..not.L_uk
      L_uk=L_es
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 185, 239):������������  �� 1 ���
C label 64  try64=try64-1
      if(L_at) then
         R0_et=R0_it
      else
         R0_et=R0_ot
      endif
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 216, 251):���� RE IN LO CH7
      if(L_es) then
         R0_is=R0_os
      else
         R0_is=R0_us
      endif
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 216, 238):���� RE IN LO CH7
      R0_ife = R0_et + R0_is
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 224, 244):��������
      R_afe=R_afe+deltat/R_ude*R0_ife
      if(R_afe.gt.R_ode) then
         R_afe=R_ode
      elseif(R_afe.lt.R_efe) then
         R_afe=R_efe
      endif
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 235, 242):����������
      L0_ebe=R_afe.lt.R_ibe
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 256, 240):���������� <
      L0_ede=R_afe.gt.R_ide
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 256, 248):���������� >
      L0_ix = L0_ebe.OR.L0_ede
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 244, 196):���
      L0_om = L0_im.OR.L_em.OR.L0_ix
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 177, 230):���
      L0_il = L_at.AND.L_es
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 205, 227):�
      L0_ur = L0_om.OR.L0_el.OR.L0_il
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 191, 239):���
      L_at=L0_or.or.(L_at.and..not.(L0_ur))
      L0_as=.not.L_at
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 198, 242):RS �������
      L0_ol=L_at.and..not.L_al
      L_al=L_at
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 185, 210):������������  �� 1 ���
      L0_er = L0_ol.OR.L0_om.OR.L0_il
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 191, 208):���
      L_es=L0_ar.or.(L_es.and..not.(L0_er))
      L0_ir=.not.L_es
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 198, 211):RS �������
C sav1=L0_il
      L0_il = L_at.AND.L_es
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 205, 227):recalc:�
C if(sav1.ne.L0_il .and. try90.gt.0) goto 90
      if(L0_ebe) then
         I_abe=I0_ox
      else
         I_abe=I0_ux
      endif
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 267, 243):���� RE IN LO CH7
      R8_ut = R_afe
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 258, 274):��������
      L0_ov=R_afe.lt.R_uv
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 256, 254):���������� <
      L0_ax=R_afe.gt.R_ex
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 256, 260):���������� >
      L0_av = L0_ax.AND.L0_ov
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 261, 257):�
      if(L0_av) then
         I0_ube=I0_ev
      else
         I0_ube=I0_iv
      endif
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 267, 261):���� RE IN LO CH7
      if(L0_ede) then
         I_ade=I0_obe
      else
         I_ade=I0_ube
      endif
C ASU_SIO_VALVE_HANDLER_WITH_MALFUNCTION.fmg( 267, 251):���� RE IN LO CH7
      End
