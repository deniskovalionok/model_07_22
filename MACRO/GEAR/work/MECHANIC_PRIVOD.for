      Subroutine MECHANIC_PRIVOD(ext_deltat,L_e,R_od,L_ef
     &,L_if,R_uk,L_il,L_ol,L_ul,L_am,R_ip,L_ar,R_is,L_at,R_iv
     &,L_ax,L_ex,R_obe,L_ede,L_ide,R_ufe,L_ike,L_oke,L_uke
     &,R_eme,L_ume,R_ere,L_ure,L_ase,L_ese,L_ise,L_ose,L_use
     &,L_ate,L_ete,L_ite,L_ote,L_ute,L_ave,L_eve,L_ive,L_ove
     &,L_uve,L_axe,L_exe,L_ixe,L_oxe,L_uxe,L_abi,L_ebi,L_ibi
     &,L_obi,L_ubi,L_adi,L_edi,R_afi,L_ifi,L_ufi,L_aki,L_uki
     &,L_ali,L_eli,L_ili,L_oli,L_uli,R_ami,L_emi,L_imi,R_ipi
     &,L_opi,L_upi,L_ari,L_eri,L_iri,L_ori,I_esi,L_isi,I_iti
     &,L_oti,I_uti,L_avi,R_evi,R_oxi,R_ibo,R_ofo,L_ufo,L_ako
     &,L_iko,L_elo)
C |L_e           |1 1 O|POS10_READY     |�������� ��������� 5||
C |R_od          |4 4 I|pos10_x         |���������� ��������� 10||
C |L_ef          |1 1 I|POS10C          |������� ������� � ��������� 5||
C |L_if          |1 1 O|POS9_READY      |�������� ��������� 4||
C |R_uk          |4 4 I|pos9_x          |���������� ��������� 9||
C |L_il          |1 1 I|POS9C           |������� ������� � ��������� 4||
C |L_ol          |1 1 O|POS8_READY      |�������� ��������� 3||
C |L_ul          |1 1 O|POS7_READY      |�������� ��������� 2||
C |L_am          |1 1 O|POS6_READY      |�������� ��������� 1||
C |R_ip          |4 4 I|pos8_x          |���������� ��������� 8||
C |L_ar          |1 1 I|POS8C           |������� ������� � ��������� 3||
C |R_is          |4 4 I|pos7_x          |���������� ��������� 7||
C |L_at          |1 1 I|POS7C           |������� ������� � ��������� 2||
C |R_iv          |4 4 I|pos6_x          |���������� ��������� 6||
C |L_ax          |1 1 I|POS6C           |������� ������� � ��������� 1||
C |L_ex          |1 1 O|POS5_READY      |�������� ��������� 5||
C |R_obe         |4 4 I|pos5_x          |���������� ��������� 5||
C |L_ede         |1 1 I|POS5C           |������� ������� � ��������� 5||
C |L_ide         |1 1 O|POS4_READY      |�������� ��������� 4||
C |R_ufe         |4 4 I|pos4_x          |���������� ��������� 4||
C |L_ike         |1 1 I|POS4C           |������� ������� � ��������� 4||
C |L_oke         |1 1 O|POS3_READY      |�������� ��������� 3||
C |L_uke         |1 1 O|POS2_READY      |�������� ��������� 2||
C |R_eme         |4 4 I|pos3_x          |���������� ��������� 3||
C |L_ume         |1 1 I|POS3C           |������� ������� � ��������� 3||
C |R_ere         |4 4 I|pos2_x          |���������� ��������� 2||
C |L_ure         |1 1 I|POS2C           |������� ������� � ��������� 2||
C |L_ase         |1 1 O|POS10_UP        |������� ������||
C |L_ese         |1 1 O|POS9_UP         |������� ������||
C |L_ise         |1 1 O|POS8_UP         |������� ������||
C |L_ose         |1 1 O|POS7_UP         |������� ������||
C |L_use         |1 1 O|POS6_UP         |������� ������||
C |L_ate         |1 1 O|POS10_DOWN      |������� �����||
C |L_ete         |1 1 O|POS9_DOWN       |������� �����||
C |L_ite         |1 1 O|POS8_DOWN       |������� �����||
C |L_ote         |1 1 O|POS7_DOWN       |������� �����||
C |L_ute         |1 1 O|POS6_DOWN       |������� �����||
C |L_ave         |1 1 O|POS10_STOP      |������� ����||
C |L_eve         |1 1 O|POS9_STOP       |������� ����||
C |L_ive         |1 1 O|POS8_STOP       |������� ����||
C |L_ove         |1 1 O|POS7_STOP       |������� ����||
C |L_uve         |1 1 O|POS6_STOP       |������� ����||
C |L_axe         |1 1 O|POS5_UP         |������� ������||
C |L_exe         |1 1 O|POS5_DOWN       |������� �����||
C |L_ixe         |1 1 O|POS5_STOP       |������� ����||
C |L_oxe         |1 1 O|POS4_UP         |������� ������||
C |L_uxe         |1 1 O|POS3_UP         |������� ������||
C |L_abi         |1 1 O|POS2_UP         |������� ������||
C |L_ebi         |1 1 O|POS4_DOWN       |������� �����||
C |L_ibi         |1 1 O|POS3_DOWN       |������� �����||
C |L_obi         |1 1 O|POS2_DOWN       |������� �����||
C |L_ubi         |1 1 O|POS4_STOP       |������� ����||
C |L_adi         |1 1 O|POS3_STOP       |������� ����||
C |L_edi         |1 1 O|POS2_STOP       |������� ����||
C |R_afi         |4 4 I|pos1_x          |���������� ��������� 1||
C |L_ifi         |1 1 O|POS1_STOP       |������� ����||
C |L_ufi         |1 1 O|POS1_UP         |������� ������||
C |L_aki         |1 1 O|POS1_DOWN       |������� �����||
C |L_uki         |1 1 O|POS1_READY      |�������� ��������� 1||
C |L_ali         |1 1 I|POS1C           |������� ������� � ��������� 1||
C |L_eli         |1 1 O|UNCATCH         |��������� �������|F|
C |L_ili         |1 1 I|YA31            |������� ��������� �������|F|
C |L_oli         |1 1 O|CATCH           |������ �������|F|
C |L_uli         |1 1 I|YA30            |������� ������ �������|F|
C |R_ami         |4 4 O|SPEED           |�������� ������� [��/�]||
C |L_emi         |1 1 S|_splsJ202*      |[TF]���������� ��������� ������������� |F|
C |L_imi         |1 1 S|_splsJ197*      |[TF]���������� ��������� ������������� |F|
C |R_ipi         |4 4 I|vel             |�������� ������� [��/�]||
C |L_opi         |1 1 O|POS_DOWN_GLOB   |||
C |L_upi         |1 1 I|DOWNC           |������ �����. ���������� �������||
C |L_ari         |1 1 O|POS_UP_GLOB     |||
C |L_eri         |1 1 O|POS_STOP_GLOB   |||
C |L_iri         |1 1 I|STOPC           |������ ����. ���������� �������||
C |L_ori         |1 1 I|UPC             |������ ������. ���������� �������||
C |I_esi         |2 4 I|STOP_USER       |������ ����. ������� �� ���������||
C |L_isi         |1 1 I|STOP            |������ ����. ������� �� ����������||
C |I_iti         |2 4 I|DOWN_USER       |������ �����. ������� �� ���������||
C |L_oti         |1 1 I|DOWN            |������ �����. ������� �� ����������||
C |I_uti         |2 4 I|UP_USER         |������ ������. ������� �� ���������||
C |L_avi         |1 1 I|UP              |������ ������. ������� �� ����������||
C |R_evi         |4 4 O|POS             |��������� ������� [��]||
C |R_oxi         |4 4 I|max             |����. ��������� ������� [��]||
C |R_ibo         |4 4 I|min             |���. ��������� ������� [��]||
C |R_ofo         |4 4 S|_ointJ59*       |����� ����������� |0.0|
C |L_ufo         |1 1 O|XH52            |������ � MIN||
C |L_ako         |1 1 O|XH51            |������ � MAX||
C |L_iko         |1 1 S|_qffJ43*        |�������� ������ Q RS-��������  |F|
C |L_elo         |1 1 S|_qffJ42*        |�������� ������ Q RS-��������  |F|

      IMPLICIT NONE
      REAL*4 ext_deltat,deltat
      Integer*4 iv1,iv2,iv3
      Logical work
      Logical t,f
      parameter (t=.true.)
      parameter (f=.false.)
      LOGICAL*1 L_e,L0_i
      REAL*4 R0_o
      LOGICAL*1 L0_u
      REAL*4 R0_ad
      LOGICAL*1 L0_ed
      REAL*4 R0_id,R_od
      LOGICAL*1 L0_ud
      REAL*4 R0_af
      LOGICAL*1 L_ef,L_if,L0_of
      REAL*4 R0_uf
      LOGICAL*1 L0_ak
      REAL*4 R0_ek
      LOGICAL*1 L0_ik
      REAL*4 R0_ok,R_uk
      LOGICAL*1 L0_al
      REAL*4 R0_el
      LOGICAL*1 L_il,L_ol,L_ul,L_am,L0_em
      REAL*4 R0_im
      LOGICAL*1 L0_om
      REAL*4 R0_um
      LOGICAL*1 L0_ap
      REAL*4 R0_ep,R_ip
      LOGICAL*1 L0_op
      REAL*4 R0_up
      LOGICAL*1 L_ar,L0_er
      REAL*4 R0_ir
      LOGICAL*1 L0_or
      REAL*4 R0_ur
      LOGICAL*1 L0_as
      REAL*4 R0_es,R_is
      LOGICAL*1 L0_os
      REAL*4 R0_us
      LOGICAL*1 L_at,L0_et
      REAL*4 R0_it
      LOGICAL*1 L0_ot
      REAL*4 R0_ut
      LOGICAL*1 L0_av
      REAL*4 R0_ev,R_iv
      LOGICAL*1 L0_ov
      REAL*4 R0_uv
      LOGICAL*1 L_ax,L_ex,L0_ix
      REAL*4 R0_ox
      LOGICAL*1 L0_ux
      REAL*4 R0_abe
      LOGICAL*1 L0_ebe
      REAL*4 R0_ibe,R_obe
      LOGICAL*1 L0_ube
      REAL*4 R0_ade
      LOGICAL*1 L_ede,L_ide,L0_ode
      REAL*4 R0_ude
      LOGICAL*1 L0_afe
      REAL*4 R0_efe
      LOGICAL*1 L0_ife
      REAL*4 R0_ofe,R_ufe
      LOGICAL*1 L0_ake
      REAL*4 R0_eke
      LOGICAL*1 L_ike,L_oke,L_uke,L0_ale
      REAL*4 R0_ele
      LOGICAL*1 L0_ile
      REAL*4 R0_ole
      LOGICAL*1 L0_ule
      REAL*4 R0_ame,R_eme
      LOGICAL*1 L0_ime
      REAL*4 R0_ome
      LOGICAL*1 L_ume,L0_ape
      REAL*4 R0_epe
      LOGICAL*1 L0_ipe
      REAL*4 R0_ope
      LOGICAL*1 L0_upe
      REAL*4 R0_are,R_ere
      LOGICAL*1 L0_ire
      REAL*4 R0_ore
      LOGICAL*1 L_ure,L_ase,L_ese,L_ise,L_ose,L_use,L_ate
     &,L_ete,L_ite,L_ote,L_ute,L_ave,L_eve,L_ive,L_ove,L_uve
     &,L_axe,L_exe,L_ixe,L_oxe,L_uxe,L_abi
      LOGICAL*1 L_ebi,L_ibi,L_obi,L_ubi,L_adi,L_edi
      REAL*4 R0_idi,R0_odi,R0_udi,R_afi,R0_efi
      LOGICAL*1 L_ifi,L0_ofi,L_ufi,L_aki,L0_eki,L0_iki,L0_oki
     &,L_uki,L_ali,L_eli,L_ili,L_oli,L_uli
      REAL*4 R_ami
      LOGICAL*1 L_emi,L_imi,L0_omi,L0_umi,L0_api
      REAL*4 R0_epi,R_ipi
      LOGICAL*1 L_opi,L_upi,L_ari,L_eri,L_iri,L_ori,L0_uri
     &,L0_asi
      INTEGER*4 I_esi
      LOGICAL*1 L_isi,L0_osi,L0_usi,L0_ati,L0_eti
      INTEGER*4 I_iti
      LOGICAL*1 L_oti
      INTEGER*4 I_uti
      LOGICAL*1 L_avi
      REAL*4 R_evi,R0_ivi,R0_ovi,R0_uvi,R0_axi
      LOGICAL*1 L0_exi,L0_ixi
      REAL*4 R_oxi,R0_uxi
      LOGICAL*1 L0_abo,L0_ebo
      REAL*4 R_ibo,R0_obo
      LOGICAL*1 L0_ubo,L0_ado,L0_edo,L0_ido,L0_odo,L0_udo
     &,L0_afo,L0_efo,L0_ifo
      REAL*4 R_ofo
      LOGICAL*1 L_ufo,L_ako
      REAL*4 R0_eko
      LOGICAL*1 L_iko
      REAL*4 R0_oko,R0_uko,R0_alo
      LOGICAL*1 L_elo
      REAL*4 R0_ilo,R0_olo,R0_ulo

      iv1 = 0
      iv2 = 0
      iv3 = 0
      work = .false.
      deltat = (iv1+iv2+iv3)+ext_deltat

      R0_o = 10.0
C MECHANIC_PRIVOD.fmg( 451, 210):��������� (RE4) (�������)
      R0_ad = R_od + (-R0_o)
C MECHANIC_PRIVOD.fmg( 454, 212):��������
      R0_id = 10.0
C MECHANIC_PRIVOD.fmg( 457, 221):��������� (RE4) (�������)
      R0_af = R_od + R0_id
C MECHANIC_PRIVOD.fmg( 460, 223):��������
      R0_uf = 10.0
C MECHANIC_PRIVOD.fmg( 451, 250):��������� (RE4) (�������)
      R0_ek = R_uk + (-R0_uf)
C MECHANIC_PRIVOD.fmg( 454, 252):��������
      R0_ok = 10.0
C MECHANIC_PRIVOD.fmg( 457, 261):��������� (RE4) (�������)
      R0_el = R_uk + R0_ok
C MECHANIC_PRIVOD.fmg( 460, 263):��������
      R0_im = 10.0
C MECHANIC_PRIVOD.fmg( 451, 289):��������� (RE4) (�������)
      R0_um = R_ip + (-R0_im)
C MECHANIC_PRIVOD.fmg( 454, 291):��������
      R0_ep = 10.0
C MECHANIC_PRIVOD.fmg( 457, 300):��������� (RE4) (�������)
      R0_up = R_ip + R0_ep
C MECHANIC_PRIVOD.fmg( 460, 302):��������
      R0_ir = 10.0
C MECHANIC_PRIVOD.fmg( 451, 327):��������� (RE4) (�������)
      R0_ur = R_is + (-R0_ir)
C MECHANIC_PRIVOD.fmg( 454, 329):��������
      R0_es = 10.0
C MECHANIC_PRIVOD.fmg( 457, 338):��������� (RE4) (�������)
      R0_us = R_is + R0_es
C MECHANIC_PRIVOD.fmg( 460, 340):��������
      R0_it = 10.0
C MECHANIC_PRIVOD.fmg( 451, 366):��������� (RE4) (�������)
      R0_ut = R_iv + (-R0_it)
C MECHANIC_PRIVOD.fmg( 454, 368):��������
      R0_ev = 10.0
C MECHANIC_PRIVOD.fmg( 457, 377):��������� (RE4) (�������)
      R0_uv = R_iv + R0_ev
C MECHANIC_PRIVOD.fmg( 460, 379):��������
      R0_ox = 10.0
C MECHANIC_PRIVOD.fmg( 326, 210):��������� (RE4) (�������)
      R0_abe = R_obe + (-R0_ox)
C MECHANIC_PRIVOD.fmg( 329, 212):��������
      R0_ibe = 10.0
C MECHANIC_PRIVOD.fmg( 332, 221):��������� (RE4) (�������)
      R0_ade = R_obe + R0_ibe
C MECHANIC_PRIVOD.fmg( 335, 223):��������
      R0_ude = 10.0
C MECHANIC_PRIVOD.fmg( 326, 250):��������� (RE4) (�������)
      R0_efe = R_ufe + (-R0_ude)
C MECHANIC_PRIVOD.fmg( 329, 252):��������
      R0_ofe = 10.0
C MECHANIC_PRIVOD.fmg( 332, 261):��������� (RE4) (�������)
      R0_eke = R_ufe + R0_ofe
C MECHANIC_PRIVOD.fmg( 335, 263):��������
      R0_ele = 10.0
C MECHANIC_PRIVOD.fmg( 326, 289):��������� (RE4) (�������)
      R0_ole = R_eme + (-R0_ele)
C MECHANIC_PRIVOD.fmg( 329, 291):��������
      R0_ame = 10.0
C MECHANIC_PRIVOD.fmg( 332, 300):��������� (RE4) (�������)
      R0_ome = R_eme + R0_ame
C MECHANIC_PRIVOD.fmg( 335, 302):��������
      R0_epe = 10.0
C MECHANIC_PRIVOD.fmg( 326, 327):��������� (RE4) (�������)
      R0_ope = R_ere + (-R0_epe)
C MECHANIC_PRIVOD.fmg( 329, 329):��������
      R0_are = 10.0
C MECHANIC_PRIVOD.fmg( 332, 338):��������� (RE4) (�������)
      R0_ore = R_ere + R0_are
C MECHANIC_PRIVOD.fmg( 335, 340):��������
      R0_idi = 10.0
C MECHANIC_PRIVOD.fmg( 326, 366):��������� (RE4) (�������)
      R0_odi = R_afi + (-R0_idi)
C MECHANIC_PRIVOD.fmg( 329, 368):��������
      R0_udi = 10.0
C MECHANIC_PRIVOD.fmg( 332, 377):��������� (RE4) (�������)
      R0_efi = R_afi + R0_udi
C MECHANIC_PRIVOD.fmg( 335, 379):��������
      L_eli=L_ili
C MECHANIC_PRIVOD.fmg(  43, 340):������,UNCATCH
      L_oli=L_uli
C MECHANIC_PRIVOD.fmg(  43, 345):������,CATCH
      R0_epi = 1.0
C MECHANIC_PRIVOD.fmg( 143, 280):��������� (RE4) (�������)
      if(R_ipi.ge.0.0) then
         R0_uxi=R0_epi/max(R_ipi,1.0e-10)
      else
         R0_uxi=R0_epi/min(R_ipi,-1.0e-10)
      endif
C MECHANIC_PRIVOD.fmg( 141, 275):�������� ����������
      R0_obo = 0
C MECHANIC_PRIVOD.fmg( 143, 269):��������� (RE4) (�������)
      L0_ati=I_esi.ne.0
C MECHANIC_PRIVOD.fmg(  63, 236):��������� 1->LO
      L0_exi=I_iti.ne.0
C MECHANIC_PRIVOD.fmg(  63, 216):��������� 1->LO
      L0_ubo=I_uti.ne.0
C MECHANIC_PRIVOD.fmg(  63, 254):��������� 1->LO
      R0_ivi = 0.01
C MECHANIC_PRIVOD.fmg( 207, 273):��������� (RE4) (�������)
      R0_ovi = (-R0_ivi) + R_oxi
C MECHANIC_PRIVOD.fmg( 210, 273):��������
      R0_uvi = 0.01
C MECHANIC_PRIVOD.fmg( 207, 250):��������� (RE4) (�������)
      R0_axi = R0_uvi + R_ibo
C MECHANIC_PRIVOD.fmg( 210, 250):��������
      L0_ebo=.false.
C MECHANIC_PRIVOD.fmg( 143, 256):��������� ���������� (�������)
      L0_abo=.false.
C MECHANIC_PRIVOD.fmg( 141, 256):��������� ���������� (�������)
      R0_alo = 0.0
C MECHANIC_PRIVOD.fmg( 121, 258):��������� (RE4) (�������)
      R0_uko = -1.0
C MECHANIC_PRIVOD.fmg( 121, 256):��������� (RE4) (�������)
      R0_ulo = 0.0
C MECHANIC_PRIVOD.fmg( 121, 271):��������� (RE4) (�������)
      R0_olo = 1.0
C MECHANIC_PRIVOD.fmg( 121, 269):��������� (RE4) (�������)
      L0_eti = L_avi.OR.L_ori.OR.L_ari
C MECHANIC_PRIVOD.fmg(  47, 267):���
C label 113  try113=try113-1
      L0_oki=R_evi.lt.R0_efi
C MECHANIC_PRIVOD.fmg( 357, 377):���������� <
      L0_iki=R_evi.gt.R0_odi
C MECHANIC_PRIVOD.fmg( 357, 369):���������� >
      L_uki = L0_oki.AND.L0_iki
C MECHANIC_PRIVOD.fmg( 368, 373):�
      L_ifi = L_uki.AND.L_ali
C MECHANIC_PRIVOD.fmg( 378, 369):�
      L0_upe=R_evi.lt.R0_ore
C MECHANIC_PRIVOD.fmg( 357, 338):���������� <
      L0_ipe=R_evi.gt.R0_ope
C MECHANIC_PRIVOD.fmg( 357, 330):���������� >
      L_uke = L0_upe.AND.L0_ipe
C MECHANIC_PRIVOD.fmg( 368, 334):�
      L_edi = L_uke.AND.L_ure
C MECHANIC_PRIVOD.fmg( 378, 330):�
      L0_ule=R_evi.lt.R0_ome
C MECHANIC_PRIVOD.fmg( 357, 300):���������� <
      L0_ile=R_evi.gt.R0_ole
C MECHANIC_PRIVOD.fmg( 357, 292):���������� >
      L_oke = L0_ule.AND.L0_ile
C MECHANIC_PRIVOD.fmg( 368, 296):�
      L_adi = L_oke.AND.L_ume
C MECHANIC_PRIVOD.fmg( 378, 292):�
      L0_ife=R_evi.lt.R0_eke
C MECHANIC_PRIVOD.fmg( 357, 261):���������� <
      L0_afe=R_evi.gt.R0_efe
C MECHANIC_PRIVOD.fmg( 357, 253):���������� >
      L_ide = L0_ife.AND.L0_afe
C MECHANIC_PRIVOD.fmg( 368, 257):�
      L_ubi = L_ide.AND.L_ike
C MECHANIC_PRIVOD.fmg( 378, 253):�
      L0_ebe=R_evi.lt.R0_ade
C MECHANIC_PRIVOD.fmg( 357, 221):���������� <
      L0_ux=R_evi.gt.R0_abe
C MECHANIC_PRIVOD.fmg( 357, 213):���������� >
      L_ex = L0_ebe.AND.L0_ux
C MECHANIC_PRIVOD.fmg( 368, 217):�
      L_ixe = L_ex.AND.L_ede
C MECHANIC_PRIVOD.fmg( 378, 213):�
      L0_av=R_evi.lt.R0_uv
C MECHANIC_PRIVOD.fmg( 482, 377):���������� <
      L0_ot=R_evi.gt.R0_ut
C MECHANIC_PRIVOD.fmg( 482, 369):���������� >
      L_am = L0_av.AND.L0_ot
C MECHANIC_PRIVOD.fmg( 493, 373):�
      L_uve = L_am.AND.L_ax
C MECHANIC_PRIVOD.fmg( 503, 369):�
      L0_as=R_evi.lt.R0_us
C MECHANIC_PRIVOD.fmg( 482, 338):���������� <
      L0_or=R_evi.gt.R0_ur
C MECHANIC_PRIVOD.fmg( 482, 330):���������� >
      L_ul = L0_as.AND.L0_or
C MECHANIC_PRIVOD.fmg( 493, 334):�
      L_ove = L_ul.AND.L_at
C MECHANIC_PRIVOD.fmg( 503, 330):�
      L0_ap=R_evi.lt.R0_up
C MECHANIC_PRIVOD.fmg( 482, 300):���������� <
      L0_om=R_evi.gt.R0_um
C MECHANIC_PRIVOD.fmg( 482, 292):���������� >
      L_ol = L0_ap.AND.L0_om
C MECHANIC_PRIVOD.fmg( 493, 296):�
      L_ive = L_ol.AND.L_ar
C MECHANIC_PRIVOD.fmg( 503, 292):�
      L0_ik=R_evi.lt.R0_el
C MECHANIC_PRIVOD.fmg( 482, 261):���������� <
      L0_ak=R_evi.gt.R0_ek
C MECHANIC_PRIVOD.fmg( 482, 253):���������� >
      L_if = L0_ik.AND.L0_ak
C MECHANIC_PRIVOD.fmg( 493, 257):�
      L_eve = L_if.AND.L_il
C MECHANIC_PRIVOD.fmg( 503, 253):�
      L0_ed=R_evi.lt.R0_af
C MECHANIC_PRIVOD.fmg( 482, 221):���������� <
      L0_u=R_evi.gt.R0_ad
C MECHANIC_PRIVOD.fmg( 482, 213):���������� >
      L_e = L0_ed.AND.L0_u
C MECHANIC_PRIVOD.fmg( 493, 217):�
      L_ave = L_e.AND.L_ef
C MECHANIC_PRIVOD.fmg( 503, 213):�
      L_eri = L_ifi.OR.L_edi.OR.L_adi.OR.L_ubi.OR.L_ixe.OR.L_uve.OR.L_ov
     &e.OR.L_ive.OR.L_eve.OR.L_ave
C MECHANIC_PRIVOD.fmg( 238, 375):���
      L0_asi = L_isi.OR.L_iri.OR.L_eri
C MECHANIC_PRIVOD.fmg(  47, 248):���
      L0_usi = (.NOT.L0_ubo).AND.L0_asi.AND.(.NOT.L0_exi)
C MECHANIC_PRIVOD.fmg(  73, 248):�
      L_ufo=R_ofo.lt.R0_axi
C MECHANIC_PRIVOD.fmg( 215, 251):���������� <
      L_ako=R_ofo.gt.R0_ovi
C MECHANIC_PRIVOD.fmg( 215, 274):���������� >
      L0_edo = L_ufo.OR.L_ako
C MECHANIC_PRIVOD.fmg( 165, 220):���
      L0_osi = L0_usi.OR.L0_ati.OR.L0_edo
C MECHANIC_PRIVOD.fmg(  86, 250):���
      L0_api=L_elo.and..not.L_imi
      L_imi=L_elo
C MECHANIC_PRIVOD.fmg(  94, 229):������������  �� 1 ���
      L0_umi = L_elo.AND.L_iko
C MECHANIC_PRIVOD.fmg( 114, 246):�
      L0_odo = L0_api.OR.L0_osi.OR.L0_umi
C MECHANIC_PRIVOD.fmg( 100, 227):���
      L0_eki=R_evi.gt.R0_efi
C MECHANIC_PRIVOD.fmg( 357, 383):���������� >
      L_aki = L0_eki.AND.L_ali
C MECHANIC_PRIVOD.fmg( 368, 382):�
      L0_ire=R_evi.gt.R0_ore
C MECHANIC_PRIVOD.fmg( 357, 344):���������� >
      L_obi = L0_ire.AND.L_ure
C MECHANIC_PRIVOD.fmg( 368, 343):�
      L0_ime=R_evi.gt.R0_ome
C MECHANIC_PRIVOD.fmg( 357, 306):���������� >
      L_ibi = L0_ime.AND.L_ume
C MECHANIC_PRIVOD.fmg( 368, 305):�
      L0_ake=R_evi.gt.R0_eke
C MECHANIC_PRIVOD.fmg( 357, 267):���������� >
      L_ebi = L0_ake.AND.L_ike
C MECHANIC_PRIVOD.fmg( 368, 266):�
      L0_ube=R_evi.gt.R0_ade
C MECHANIC_PRIVOD.fmg( 357, 227):���������� >
      L_exe = L0_ube.AND.L_ede
C MECHANIC_PRIVOD.fmg( 368, 226):�
      L0_ov=R_evi.gt.R0_uv
C MECHANIC_PRIVOD.fmg( 482, 383):���������� >
      L_ute = L0_ov.AND.L_ax
C MECHANIC_PRIVOD.fmg( 493, 382):�
      L0_os=R_evi.gt.R0_us
C MECHANIC_PRIVOD.fmg( 482, 344):���������� >
      L_ote = L0_os.AND.L_at
C MECHANIC_PRIVOD.fmg( 493, 343):�
      L0_op=R_evi.gt.R0_up
C MECHANIC_PRIVOD.fmg( 482, 306):���������� >
      L_ite = L0_op.AND.L_ar
C MECHANIC_PRIVOD.fmg( 493, 305):�
      L0_al=R_evi.gt.R0_el
C MECHANIC_PRIVOD.fmg( 482, 267):���������� >
      L_ete = L0_al.AND.L_il
C MECHANIC_PRIVOD.fmg( 493, 266):�
      L0_ud=R_evi.gt.R0_af
C MECHANIC_PRIVOD.fmg( 482, 227):���������� >
      L_ate = L0_ud.AND.L_ef
C MECHANIC_PRIVOD.fmg( 493, 226):�
      L_opi = L_aki.OR.L_obi.OR.L_ibi.OR.L_ebi.OR.L_exe.OR.L_ute.OR.L_ot
     &e.OR.L_ite.OR.L_ete.OR.L_ate
C MECHANIC_PRIVOD.fmg( 238, 348):���
      L0_uri = L_oti.OR.L_upi.OR.L_opi
C MECHANIC_PRIVOD.fmg(  47, 228):���
      L0_ixi = (.NOT.L0_usi).AND.(.NOT.L0_ati).AND.(.NOT.L0_ubo
     &).AND.L0_uri
C MECHANIC_PRIVOD.fmg(  73, 231):�
      L0_ido = L0_ixi.OR.L0_exi
C MECHANIC_PRIVOD.fmg(  86, 232):���
      L_iko=L0_ido.or.(L_iko.and..not.(L0_odo))
      L0_udo=.not.L_iko
C MECHANIC_PRIVOD.fmg( 107, 230):RS �������
      L0_omi=L_iko.and..not.L_emi
      L_emi=L_iko
C MECHANIC_PRIVOD.fmg(  94, 258):������������  �� 1 ���
      L0_efo = L0_osi.OR.L0_omi.OR.L0_umi
C MECHANIC_PRIVOD.fmg( 100, 258):���
      L0_ado = L0_eti.AND.(.NOT.L0_ati).AND.(.NOT.L0_exi).AND.
     &(.NOT.L0_usi)
C MECHANIC_PRIVOD.fmg(  73, 264):�
      L0_afo = L0_ado.OR.L0_ubo
C MECHANIC_PRIVOD.fmg(  86, 263):���
      L_elo=L0_afo.or.(L_elo.and..not.(L0_efo))
      L0_ifo=.not.L_elo
C MECHANIC_PRIVOD.fmg( 107, 261):RS �������
C sav1=L0_umi
      L0_umi = L_elo.AND.L_iko
C MECHANIC_PRIVOD.fmg( 114, 246):recalc:�
C if(sav1.ne.L0_umi .and. try275.gt.0) goto 275
      if(L_elo) then
         R0_ilo=R0_olo
      else
         R0_ilo=R0_ulo
      endif
C MECHANIC_PRIVOD.fmg( 125, 270):���� RE IN LO CH7
      if(L_iko) then
         R0_oko=R0_uko
      else
         R0_oko=R0_alo
      endif
C MECHANIC_PRIVOD.fmg( 125, 257):���� RE IN LO CH7
      R0_eko = R0_ilo + R0_oko
C MECHANIC_PRIVOD.fmg( 133, 263):��������
      if(L0_abo) then
         R_ofo=R0_obo
      elseif(L0_ebo) then
         R_ofo=R_ofo
      else
         R_ofo=R_ofo+deltat/R0_uxi*R0_eko
      endif
      if(R_ofo.gt.R_oxi) then
         R_ofo=R_oxi
      elseif(R_ofo.lt.R_ibo) then
         R_ofo=R_ibo
      endif
C MECHANIC_PRIVOD.fmg( 146, 263):����������
      R_evi=R_ofo
C MECHANIC_PRIVOD.fmg( 242, 263):������,POS
      L0_ofi=R_evi.lt.R0_odi
C MECHANIC_PRIVOD.fmg( 357, 362):���������� <
      L_ufi = L0_ofi.AND.L_ali
C MECHANIC_PRIVOD.fmg( 368, 361):�
      L0_ape=R_evi.lt.R0_ope
C MECHANIC_PRIVOD.fmg( 357, 323):���������� <
      L_abi = L0_ape.AND.L_ure
C MECHANIC_PRIVOD.fmg( 368, 322):�
      L0_ale=R_evi.lt.R0_ole
C MECHANIC_PRIVOD.fmg( 357, 285):���������� <
      L_uxe = L0_ale.AND.L_ume
C MECHANIC_PRIVOD.fmg( 368, 284):�
      L0_ode=R_evi.lt.R0_efe
C MECHANIC_PRIVOD.fmg( 357, 246):���������� <
      L_oxe = L0_ode.AND.L_ike
C MECHANIC_PRIVOD.fmg( 368, 245):�
      L0_ix=R_evi.lt.R0_abe
C MECHANIC_PRIVOD.fmg( 357, 206):���������� <
      L_axe = L0_ix.AND.L_ede
C MECHANIC_PRIVOD.fmg( 368, 205):�
      L0_et=R_evi.lt.R0_ut
C MECHANIC_PRIVOD.fmg( 482, 362):���������� <
      L_use = L0_et.AND.L_ax
C MECHANIC_PRIVOD.fmg( 493, 361):�
      L0_er=R_evi.lt.R0_ur
C MECHANIC_PRIVOD.fmg( 482, 323):���������� <
      L_ose = L0_er.AND.L_at
C MECHANIC_PRIVOD.fmg( 493, 322):�
      L0_em=R_evi.lt.R0_um
C MECHANIC_PRIVOD.fmg( 482, 285):���������� <
      L_ise = L0_em.AND.L_ar
C MECHANIC_PRIVOD.fmg( 493, 284):�
      L0_of=R_evi.lt.R0_ek
C MECHANIC_PRIVOD.fmg( 482, 246):���������� <
      L_ese = L0_of.AND.L_il
C MECHANIC_PRIVOD.fmg( 493, 245):�
      L0_i=R_evi.lt.R0_ad
C MECHANIC_PRIVOD.fmg( 482, 206):���������� <
      L_ase = L0_i.AND.L_ef
C MECHANIC_PRIVOD.fmg( 493, 205):�
      L_ari = L_ufi.OR.L_abi.OR.L_uxe.OR.L_oxe.OR.L_axe.OR.L_use.OR.L_os
     &e.OR.L_ise.OR.L_ese.OR.L_ase
C MECHANIC_PRIVOD.fmg( 238, 319):���
C sav1=L0_eti
      L0_eti = L_avi.OR.L_ori.OR.L_ari
C MECHANIC_PRIVOD.fmg(  47, 267):recalc:���
C if(sav1.ne.L0_eti .and. try113.gt.0) goto 113
      R_ami = R0_eko * R_ipi
C MECHANIC_PRIVOD.fmg( 129, 286):����������
      End
